From diaframe Require Import proofmode_base.
From iris.proofmode Require Import proofmode.

Section test.
  Context {PROP : bi}.
  Context (P : nat → PROP).

  Lemma test_istep_as : ⊢ ∀ n, P n -∗ P n.
  Proof.
    iStep 2 as (n) "HP". Show. Undo 1.
    iStep 2 as ([|n]) "HP". Show.
    (* Note that we do not have to supply two IPM names! *)
    all: iSteps.
  Qed.

  Lemma test_istep_intro_disj : ⊢ ∀ n, P n ∨ P (S n) -∗ P n.
  Proof.
    iStep 2 as (n) "HP" / as (n) "HP" / --safe. Show.
  Abort.

  Lemma test_istep_intro_complain : ⊢ ∀ n, P n -∗ P n.
  Proof.
    iStep 2 as (n). Undo 1.
    iStep 2 as (n) / --silent. Undo 1.

(* this should change the error reporting behavior *)
Ltac2 Set diaframe_default_rename_error_verbosity := SilenceRenameErrors.

    iStep 2 as (n). Undo 1.
    iStep 2 as (n) / --verbose. Undo 1.

(* change it back to normal *)
Ltac2 Set diaframe_default_rename_error_verbosity := VerboseRenameErrors.

    iStep 2 as (n). Undo 1.
    iStep 2 as (n) / --silent.
    iSteps.
  Qed.

  Lemma test_isteps_until : ⊢ ∀ b,
    ((⌜b = true⌝ ∗ P 1) ∨ (⌜b = false⌝ ∗ P 2)) -∗
    if b then emp ∗ True ∗ P 1 else True ∗ P 2.
  Proof.
    iSteps --until goal-matches (True ∗ _)%I / --print-goals / as "? HP" / as_anon. Show.
    (* note that the order is correct! *)
  Abort.

  Lemma test_isteps_until_multi : ⊢ ∀ b,
    ((⌜b = true⌝ ∗ P 1) ∨ (⌜b = false⌝ ∗ P 2)) -∗
    if b then emp ∗ P 1 ∗ P 1 else True ∗ P 2 ∗ P 2.
  Proof.
    iSteps --until goal-matches (P 1 ∗ _)%I / --until goal-matches (P 2 ∗ _)%I. Show.
  Abort.

  Lemma test_isteps_until_custom : ⊢ ∀ b,
    ((⌜b = true⌝ ∗ P 1) ∨ (⌜b = false⌝ ∗ P 2)) -∗
    if b then emp ∗ P 1 ∗ P 1 else True ∗ P 2 ∗ P 2.
  Proof.
    iSteps --until ltac:(idtac;
      lazymatch goal with
      | |- environments.envs_entails (environments.Envs _ ?Γp _) _ =>
        lazymatch Γp with
        | environments.Esnoc _ _ (P _) =>
          idtac
        end
      end
    ).
  Abort.

  Lemma test_mask_matching `{!BiFUpd PROP} : ⊢@{PROP} True ={∅}=∗ P 0 ={⊤}=∗ P 1.
  Proof.
    iSteps --until goal-matches (|={⊤}=> _)%I.
    (* .. bloody hell *)
    goal-matches (|={⊤}=> _)%I. (* one would expect this to fail :/ *)
  Abort.
End test.
















