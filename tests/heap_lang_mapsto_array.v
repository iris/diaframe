From diaframe.heap_lang Require Import proof_automation.

Section list_functions.
  Context `{!heapGS Σ}.

  Lemma test l (z2 z3 : Z) : (l +ₗ 1) ↦∗ [ #z2; #z3 ] ⊢ (l +ₗ 2) ↦ #z3 ∗ (l +ₗ 1) ↦ #z2.
  Proof. iSteps. Qed.

  Lemma test2 l (z2 z3 : Z) : (l +ₗ 2) ↦ #z3 ∗ (l +ₗ 1) ↦ #z2 ⊢ (l +ₗ 1) ↦∗ [ #z2; #z3 ].
  Proof. iSteps. Qed.

  Lemma test3 l (z1 z2 : Z) : l ↦∗ [ #z1; #z2] ⊢ l ↦ #z1 ∗ (l +ₗ 1) ↦ #z2.
  Proof. iSteps. Qed.

  Lemma test4 l (z1 z2 : Z) : l ↦ #z1 ∗ (l +ₗ 1) ↦ #z2 ⊢ l ↦∗ [ #z1; #z2].
  Proof. iSteps. Qed.

  Lemma test5 l (z1 z2 : Z) : l ↦∗ [ #z1; #z2] ⊢ (l +ₗ 1) ↦ #z2 ∗ l ↦ #z1.
  Proof. iSteps. Qed.

  Lemma test6 l (z1 z2 z3 : Z) : l ↦∗ [ #z1; #z2; #z3] ⊢ l ↦∗ [ #z1; #z2] ∗ (l +ₗ 2) ↦ #z3.
  Proof. iSteps. Qed.

  Lemma test7 l (z1 z2 z3 : Z) : l ↦∗ [ #z1; #z2] ∗ (l +ₗ 2) ↦ #z3 ⊢ l ↦∗ [ #z1; #z2; #z3].
  Proof. iSteps. Qed.

  Lemma test8 l : (l +ₗ 1) ↦∗ [ #1; #2; #3 ] ∗ (l +ₗ 4) ↦ #4 ⊢ (l +ₗ 2) ↦∗ [ #2; #3; #4 ] ∗ (l +ₗ 1) ↦ #1.
  Proof. iSteps. Qed.

  Lemma test9 l : l ↦∗ [ #0; #1 ] ∗ (l +ₗ 2) ↦∗ [ #2; #3; #4 ] ⊢ (l +ₗ 1) ↦∗ [ #1; #2; #3 ] ∗ (l +ₗ 4) ↦ #4 ∗ l ↦ #0.
  Proof. iSteps. Qed.

  Lemma test10 l : (l +ₗ 1) ↦∗ [ #1; #2; #3; #4 ] ⊢ (l +ₗ 2) ↦∗ [ #2; #3 ] ∗ (l +ₗ 4) ↦ #4 ∗ (l +ₗ 1) ↦ #1.
  Proof. iSteps. Qed.

  Lemma test11 l : (l +ₗ 4) ↦∗ [ #4; #5] ∗ l ↦∗ [ #0; #1 ] ∗ (l +ₗ 2) ↦∗ [ #2; #3 ] ⊢ (l +ₗ 1) ↦∗ [ #1; #2; #3; #4 ] ∗ l ↦ #0 ∗ (l +ₗ5) ↦ #5.
  Proof. iSteps. Qed.

  Lemma test12 l x : (l +ₗ 0%nat) ↦ x ∗ (l +ₗ 1%nat) ↦ x ⊢ l ↦∗ [x; x].
  Proof. iSteps. Qed.

  Lemma test13 l P : (∃ x, l ↦∗ [x] ∗ P x) ⊢ (∃ y, l ↦ y ∗ P y).
  Proof.
    iIntros "H". iSteps.
  Qed.

  Lemma test14 l P : (∃ x, l ↦ x ∗ P x) ⊢ (∃ y, l ↦∗ [y] ∗ P y).
  Proof.
    iIntros "H". iSteps. (* the (l +ₗ 1) ↦∗ [] is not pure, so Diaframe stops here *)
    iSplitR. { iSteps. }
    iSteps.
  Qed.

  Lemma test15 l P : (∃ x, l ↦∗ [x; x] ∗ P x) ⊢ (∃ y, l ↦∗ [y] ∗ P y).
  Proof.
    iIntros "H". iSteps. (* the (l +ₗ 1) ↦∗ [] is not pure, so Diaframe stops here *)
    (* fails since the length computation on the RHS contains an evar,
      which SolveSepSideCondition then refuses to deal with *)
  Abort.
End list_functions.
