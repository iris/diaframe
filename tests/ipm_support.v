From diaframe Require Import proofmode_base.
From iris.proofmode Require Import proofmode.


Section test.
  Context {PROP : bi} `{!BiFUpd PROP} `{!BiAffine PROP}.

  Lemma test_isplit_solve_sep (P : nat → PROP) (H : ⊢ |={⊤}=> P 3) : P 3 ⊢ |={⊤}=> ∃ x, P x ∗ P x ∗ P x.
  Proof. iSteps. iSplitL; iApply H. (* both sides get a |={⊤}=>! *) Qed.

  Lemma test_isplit_solve_sep_nested (P : nat → PROP) (H : ⊢ |={⊤}=> P 3) : P 3 ⊢ |={⊤}=> ∃ x, (P x ∗ P x ∗ P x) ∗ P x.
  Proof.
    iSteps. iSplitL; first iApply H. 
    (* The goal should be a SolveSep, instead of a modality around a SolveSep *)
    Show.
    iSplitL; iApply H.
  Qed.

  Lemma test_ileft_solve_sep_nfoc (P : nat → PROP) : P 3 ⊢ |={⊤}=> ∃ x, (P x ∨ ⌜x = 3⌝) ∗ P x ∗ P x.
  Proof.
    iStep.
    iStepDebug.
    solveStep. iLeft. Undo 1.
    solveStep. iLeft.
    iStep --safest.
  Abort.

  Lemma test_iexists_solve_sep_nfoc (P : nat → PROP) : P 3 ⊢ |={⊤}=> ∃ x, (P x ∨ ⌜x = 3⌝) ∗ P x ∗ P x.
  Proof.
    iStep.
    iStepDebug.
    iExists 3. Undo 1.
    solveStep. iExists 3. Undo 1.
    solveStep. iExists 3.
    iStep.
  Abort.

  Lemma test_solve_sep_nfoc_imod (P : nat → PROP) E : (|={⊤,E}=> |={E,⊤}=> P 3) ⊢ |={⊤}=> ∃ x, (P x ∨ ⌜x = 3⌝) ∗ P x ∗ P x.
  Proof.
    iStep as "HP".
    iStepDebug. solveStep.
    iMod "HP" as "HP".
    solveStep.
    iMod "HP" as "HP".
    iStep.
  Abort.

  Lemma test_solve_one_nfoc_iexists_imod_ileft (P : nat → PROP) E : (|={⊤,E}=> |={E,⊤}=> P 3) ⊢ |={⊤}=> ∃ x, (P x ∨ ⌜x = 3⌝).
  Proof.
    iStep as "HP".
    iStepDebug. iLeft. Undo 1.
    iMod "HP".
    solveStep. iLeft. Undo 1.
    iExists 3.
    iMod "HP".
    iSteps.
  Qed.

  Lemma test_iintros (P : nat → PROP) : ⊢ (∀ x, P x ∗ P x -∗ P x ∗ P x).
  Proof.
    iStepDebug.
    iIntros (y).
    solveStep.
    iStepDebug.
    iIntros "HP".
    iSteps.
  Qed.
End test.