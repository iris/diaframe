From diaframe.hint_search Require Import reify_prop.
From diaframe Require Import proofmode_base lib.except_zero.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.



Class CanReify {PROP : bi} (P : PROP) :=
  can_reify : True.


Global Hint Extern 4 (CanReify ?P) =>
  let f := ltac2:(c1 |-
    let result := reify_prop (Option.get (Ltac1.to_constr c1)) in
    printf "Reified: %a" (fun () => bi_prop_to_string false) result;
    exact I
  ) in
  f P : typeclass_instances.


Section test.
  Context {PROP : bi}.
  Context (P Q R : bool → Z → PROP) (S : PROP).
  Context `{!BiBUpd PROP, !BiFUpd PROP, !BiBUpdFUpd PROP}.
  Context (HRS : ∀ b n, R b n ⊢ S).
  Context (T'' : bool → Z → PROP).

  Implicit Types A B C D : PROP.


  Definition test10' l : CanReify (∀ b : bool, T'' b l)%I := ltac:(apply _).

  Definition T1 := fun z b => (|==> Q b z ∨ R b z)%I.

  Definition test_prop1 : PROP := ∀ z b, P b z ∗ T1 z b.

  #[local] Instance atom_into_test b n : AtomIntoConnective (R b n) S.
  Proof. apply HRS. Qed.

  Definition test1 : CanReify test_prop1 := ltac:(apply _).

  Definition T2 := fun z b => (Q b z ∨ R b z)%I.

  Definition test_prop2 : PROP := ∀ z b, P b z ∗ |==> T2 z b.

  Definition test1_b : CanReify test_prop2 := ltac:(apply _).

  Typeclasses Opaque test_prop1.

  Definition test2 : CanReify test_prop1 := ltac:(apply _).

  Global Instance test_prop_into_forall : AtomIntoForall test_prop1 (λ z, ∀ b, P b z ∗ T1 z b)%I.
  Proof. rewrite /AtomIntoForall /IntoForallCareful /test_prop1. reflexivity. Qed.

  Definition test3 : CanReify test_prop1 := ltac:(apply _).

  Definition test4 : CanReify (∃ b z, P b z -∗ (Q b z ∨ False)) := ltac:(apply _).

  Definition test5 : CanReify (∃ b z, P b z ∗ test_prop1) := ltac:(apply _).

  Definition test6 : CanReify (∀ b z, P b z ={⊤}=∗ (Q b z ∗ True) ∨ False) := ltac:(apply _).

  Context {A : Type} {Q' : A → PROP}.

  Definition Q2 : PROP := ∀ QS, (∀ b, Q' b -∗ QS b) -∗ ∀ a, QS a.

  Definition test7 : CanReify Q2 := ltac:(apply _).

  Definition Q3 : PROP := (∀ (w : Z) (a : A), P false w -∗ Q' a ==∗ Q false w ∗ P false w).

  Definition test8 : CanReify Q3 := ltac:(apply _).

  Definition Q4 : PROP := ∃ z, P false z ∗ Q false z.
  Global Instance q4_access : AtomIntoExist Q4 (λ z, P false z ∗ ◇ (P false z -∗ Q4))%I.
  Proof.
    rewrite /AtomIntoExist /IntoExistCareful2 /IntoExistCareful /= /Q4.
    apply bi.exist_mono => z. apply bi.sep_mono_r.
    rewrite -bi.except_0_intro.
    apply bi.wand_intro_l. by eapply bi.exist_intro'.
  Qed.
  Typeclasses Opaque Q4.

  Definition test9 : CanReify Q4 := ltac:(apply _).


  (* We need to simplify away [bi_texist], since they occur inside atomic updates *)
  Definition test10 : CanReify (∃.. (tt : [tele]), ∃ a, Q' a)%I := ltac:(apply _).

  Definition test13 n : CanReify (▷^n P false 0) := ltac:(apply _).
  Definition test14 : CanReify (▷ P false 0) := ltac:(apply _).

  Definition test15 (T : PROP) (HT : AtomIntoExist T (λ (q : Qp), ⌜q < q⌝%Qp%I)) : CanReify T.
  Proof.
    (* this tests whether backtracking can kill the information queried with typeclass search 
    ltac2:(
    let resultc := { contents := None } in
    Control.once (fun _ =>
    Control.plus (fun _ =>
        let result := List.nth (tc_query_under_binders [] (fun egen => 
          safe_apply '@AtomIntoConnective ['PROP; 'T; egen '(bi_car PROP)]
        ) true ) 0 in
        resultc.(contents) := Some result;
        let rp := (Option.get (resultc.(contents))) in
        printf "found equivalent: %t" rp;
        Control.zero Assertion_failure
    ) (fun _ => ()));
    let rp := (Option.get (resultc.(contents))) in
    printf "found equivalent: %t" rp
    ). *)
    ltac2:(
      let rp_store := { contents := None } in
      lazy_match! goal with
      | [|- CanReify ?c ] =>
        plus_once_list
          [let result := 

              reify_prop c (*
              AtomProp '(token P γ) (Some (
               QuantProp BiQExist None 'Qp (AtomProp '(P 1%Qp) None))) *)

          in
           printf "Reified: %a" (fun () => bi_prop_to_string true) result;
           rp_store.(contents) := (*Some {field := 'Qp; next := None } *) Some result;
           Control.zero Assertion_failure
          | ()];

        let the_rp := (Option.get (rp_store.(contents))) in

        printf "Reified backtrack: %a" (fun () => bi_prop_to_string true) the_rp;
(*         printf "Reified backtrack: %t" ((Option.get (rp_store.(contents))).(field)); *)
        match the_rp with
        | AtomProp _ mp =>
          let the_rp' := Option.get mp in
          printf "Reified backtrack: %a" (fun () => bi_prop_to_string true) the_rp';
          match the_rp' with
          | QuantProp _ _ ctype _  =>
            printf "ctype: %t" ctype; assert ($ctype = $ctype)> [ | exact I ]
          | _ => Control.zero Assertion_failure
          end
        | _ => Control.zero Assertion_failure
        end
      end
    ).
    Show.
    reflexivity.
  Qed.

  (* this happened in gpfsl, with [@{V} P] being turned into [⊒ V -∗ P] *)
  Definition test16 (T : PROP) (M : PROP → PROP → PROP) (HT : ∀ Mr Q, AtomIntoConnective (M Mr Q) (Mr -∗ Q)) : CanReify (M T (∃ (n : nat), M T T))%I.
  Proof.
    (* this tests whether backtracking can kill the information queried with typeclass search 
    ltac2:(
    let resultc := { contents := None } in
    Control.once (fun _ =>
    Control.plus (fun _ =>
        let result := List.nth (tc_query_under_binders [] (fun egen => 
          safe_apply '@AtomIntoConnective ['PROP; 'T; egen '(bi_car PROP)]
        ) true ) 0 in
        resultc.(contents) := Some result;
        let rp := (Option.get (resultc.(contents))) in
        printf "found equivalent: %t" rp;
        Control.zero Assertion_failure
    ) (fun _ => ()));
    let rp := (Option.get (resultc.(contents))) in
    printf "found equivalent: %t" rp
    ). *)
    ltac2:(
      let rp_store := { contents := None } in
      lazy_match! goal with
      | [|- CanReify ?c ] =>
        plus_once_list
          [let result := 

              reify_prop c (*
              AtomProp '(token P γ) (Some (
               QuantProp BiQExist None 'Qp (AtomProp '(P 1%Qp) None))) *)

          in
           printf "Reified: %a" (fun () => bi_prop_to_string true) result;
           rp_store.(contents) := (*Some {field := 'Qp; next := None } *) Some result;
           Control.zero Assertion_failure
          | ()];

        let the_rp := (Option.get (rp_store.(contents))) in

        match the_rp with
        | AtomProp _ mp =>
          let the_rp' := Option.get mp in
          match the_rp' with
          | UnProp _ the_rp''  =>
            match the_rp'' with
            | QuantProp _ _ _ the_rp'''  =>
              match the_rp''' with
              | AtomProp _ mp =>
                let the_rp'''' := Option.get mp in
                match the_rp'''' with
                | UnProp _ the_rp''''' =>
                  match the_rp''''' with
                  | AtomProp c _ =>
                    assert ($c = $c) > [ | exact I]
                  | _ => Control.zero Assertion_failure
                  end
                | _ => Control.zero Assertion_failure
                end
              | _ => Control.zero Assertion_failure
              end
            | _ => Control.zero Assertion_failure
            end
          | _ => Control.zero Assertion_failure
          end
        | _ => Control.zero Assertion_failure
        end
      end
    ).
    Show.
    reflexivity.
  Qed.
End test.



From diaframe.lib Require Import iris_hints.
From iris.base_logic Require Import invariants.
From stdpp Require Import pretty.


Section clh_reification_test.
  Context `{!invGS Σ}.
  Let PROP := iPropI Σ.
  Context (loc : Type) (mapstoB : loc → Qp → bool → PROP) (mapstoL : loc → loc → PROP).
  Context (gname : Type) (ownE : gname → PROP) (ownA : gname → loc → Qp → PROP).
  Context `{!Inhabited gname} `{!Inhabited loc} `{_ : ∀ g, Timeless $ ownE g} 
    `{_ : ∀ l q b, Timeless $ mapstoB l q b} `{_ : ∀ l l', Timeless $ mapstoL l l'}.
  Context (T'' : bool → loc → PROP).
  Notation "l ↦{# q } b" := (mapstoB l q b) (at level 20) : bi_scope.
  Notation "l ↦ l'" := (mapstoL l l') (at level 20) : bi_scope.


  Definition queued_loc γe l γ R : PROP := 
    ownA γ l 1%Qp ∨ (∃ (b : bool), l ↦{#1/2} b ∗ (⌜b = true⌝ ∨ (⌜b = false⌝ ∗ R ∗ ownE γe ∗ ownA γ l (1/2)%Qp ∗ l ↦{#1/2} b))).

  Let N_node := nroot.@ "test_clh_node".
  Let N := nroot.@"test_clh_lock".

  Definition lock_inv γe lk R : PROP :=
    ∃ (l : loc), lk ↦ l ∗ ∃ γ, ownA γ l (1/2)%Qp ∗ 
      inv N_node $ queued_loc γe l γ R.

  Definition T' b l : PROP := l ↦{#1/2} b.


  Definition test10_2 l : CanReify (∀ b : bool, T'' b l) := ltac:(apply _).

  Definition test11 γe l γ R : CanReify (queued_loc γe l γ R) := ltac:(apply _).

  Definition test12_1 : CanReify (inv N True) := ltac:(apply _).

  Definition test12 γe lk R : CanReify (inv N (lock_inv γe lk R)) := ltac:(apply _).
End clh_reification_test.














