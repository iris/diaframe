From diaframe Require Import proofmode_base.
From iris.proofmode Require Import proofmode.


Section test.
  Context {PROP : bi}.
  Implicit Types P Q R S T : PROP.

  Lemma test_split_delete P Q R S `{!BiAffine PROP} :
    (P -∗ Q ∨ R) ∗ P ∗ (emp -∗ S ∗ S) ⊢ ((Q ∗ S) ∨ R) ∗ emp.
  Proof.
    iStep as "HQR HP HS".
    iStep as "HS'" / as "".
    all: iSteps.
  Qed.

  Lemma test_rename_when_stuck P Q R : P ⊢ (∃ x : nat, ⌜x < 0⌝ ∗ P).
  Proof.
    iSteps as "HP". Show.
  Abort.

  Lemma test_istep_anonymous_clear_frame P Q R S `{!BiAffine PROP} :
      P ∗ Q ∗ R ⊢ Q ∗ ((R -∗ S) -∗ S).
  Proof.
    iStep as "_ $ ?". Show.
    iSteps.
  Abort.
End test.