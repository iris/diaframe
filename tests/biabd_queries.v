From diaframe.hint_search Require Import biabd_disj_query instances_base.
From diaframe Require Import utils_ltac2 util_instances.
From iris.bi Require Import bi.
From stdpp Require Import telescopes.







Class CanFindBiAbd {PROP : bi} (P : PROP) (TTr : tele) (Q : TTr -t> PROP) :=
  can_find_biabd : True.

Global Hint Extern 4 (@CanFindBiAbd _ ?P ?TTr ?Q) =>
  let f := ltac2:(p1 ttr1 q1 |-
    let p := Option.get (Ltac1.to_constr p1) in
    let ttr := Option.get (Ltac1.to_constr ttr1) in
    let q := Option.get (Ltac1.to_constr q1) in
    let path := biabd_disj_reify_n_query_on 'false p ttr q in
    printf "path: %a" (fun () => path_list_to_string) path;
    exact I
  ) in
  f P TTr Q : typeclass_instances.


Section test.
  Context {PROP : bi}.
  Implicit Types A B C D : PROP.
  Context (P Q R : bool → Z → PROP) (S : PROP) (P' Q' R' : Z → PROP).
  Context `{!BiBUpd PROP, !BiFUpd PROP, !BiBUpdFUpd PROP}.
  Context (HRS : ∀ b z, R b z ⊢ S).

  Definition T := fun z b => (|==> Q b z ∨ R b z)%I.

  Definition test_prop : PROP := ∃ z b, P b z ∗ T z b.

  #[local] Instance atom_into_test b n : AtomIntoConnective (R b n) (emp ∗ S). 
  Proof. rewrite /AtomIntoConnective. rewrite left_id. apply HRS. Qed.

  Definition test1 : CanFindBiAbd test_prop TeleO S := ltac:(apply _).
  Definition test2 : CanFindBiAbd test_prop [tele_pair bool; Z] Q := ltac:(apply _).

  Typeclasses Opaque test_prop.
  Definition test3 : TCIf (CanFindBiAbd test_prop TeleO S) TCFalse TCTrue := ltac:(apply _).

  Definition test_hyp1 : PROP := ∀ z, P' z ∗ Q' z.

  Definition test4 : CanFindBiAbd test_hyp1 [tele_pair Z] P' := ltac:(apply _).
  Definition test5 : CanFindBiAbd test_hyp1 [tele_pair Z] Q' := ltac:(apply _).

  Typeclasses Opaque test_hyp1.
  Definition test6 : TCIf (CanFindBiAbd test_hyp1 [tele_pair Z] P') TCFalse TCTrue := ltac:(apply _).

  Global Instance test_hyp1_into_forall :
    AtomIntoForall test_hyp1 (λ z, P' z ∗ Q' z)%I.
  Proof. by rewrite /test_hyp1 /AtomIntoForall /IntoForallCareful. Qed.

  Definition test7 : CanFindBiAbd test_hyp1 [tele_pair Z] P' := ltac:(apply _).
  Definition test8 : CanFindBiAbd test_hyp1 [tele_pair Z] Q' := ltac:(apply _).

  Definition test_hyp2 : PROP := ∃ z, P' z -∗ (Q' z ∨ False).

  Definition test9 : CanFindBiAbd test_hyp2 [tele_pair Z] Q' := ltac:(apply _).

  Definition test_hyp3 : PROP := ∀ y, P' y ={⊤}=∗ (∃ z, Q' z ∗ ⌜z = (2 * y)%Z⌝) ∨ False.

  Definition test10 : CanFindBiAbd test_hyp3 [tele_pair Z] Q' := ltac:(apply _).

  Typeclasses Transparent test_prop.

  Goal True.
  Proof.
    (* tests the timing impact of reification *)

(*     time ltac2:(do 300 (
      let result := reify_prop 'test_prop in 
  (*     printf "result: %a" (fun () => bi_prop_to_string true) result ; *)
      let path := query_bi_prop result (fun _ => true) (fun p =>
        safe_apply '@ExBiAbdDisj ['PROP; '[tele_pair bool; Z]; 'false; p; '(Q)]
      ) tc_solve in
      ()
    )).
    time ltac2:(
      let test_prop' := eval unfold test_prop in test_prop in
      let result := reify_prop test_prop' in
      do 300 (
      let path := query_bi_prop result (fun _ => true) (fun p =>
        safe_apply '@ExBiAbdDisj ['PROP; 'TeleO; 'false; p; '(S)]
      ) tc_solve in
      ())
    ). *)

    exact I.
  Qed.

  (*
  Lemma test_speed :
    ∃ TT M1 M2 S T V,
    BiAbdDisjMod (TTl := TT) (TTr := [tele_pair Z]) false (test_hyp1) (λ z', R z')%I id M1 M2 S T V ∧ S = S ∧ T = T ∧ V = V.
  Proof.
    do 6 eexists. split.
    (* time *)
    (do 20 (try (ltac1:(find_biabd_disj)))).
    split.
    (* time *)
    (do 50 (try (lazy_match! goal with
    | [ |- @BiAbdDisj _ _ ?ttr _ ?prop ?goal _ _ _ _ ] =>
      let (dirlist, _) := find_biabd_disj_leaf 'false prop ttr goal in
      let strlist := List.map skel_direction_to_string dirlist in
      let msglist := List.map Message.of_string strlist in
      Message.print (messageconcat (Message.of_string "; ") msglist)
    end))).
  Abort.

  Lemma retest_1 :
    ∃ TT M1 M2 S T V,
    BiAbdDisjMod (TTl := TT) (TTr := [tele_pair Z]) false (test_hyp1) P id M1 M2 S T V ∧ S = S ∧ T = T ∧ V = V.
  Proof.
    do 6 eexists. split.
    - find_biabd_disj_ltac2 ().
    - simpl. repeat ltac1:(split; first reflexivity); reflexivity. 
  Qed.

  Lemma retest_2 :
    ∃ TT M1 M2 S T V,
    BiAbdDisjMod (TTl := TT) (TTr := [tele_pair Z]) false (test_hyp1) Q id M1 M2 S T V ∧ S = S ∧ T = T ∧ V = V.
  Proof.
    do 6 eexists. split.
    - find_biabd_disj_ltac2 ().
    - simpl. repeat ltac1:(split; first reflexivity); reflexivity. 
  Qed.

  Lemma retest_3 :
    ∃ TT M1 M2 S T V,
    BiAbdDisjMod (TTl := TT) (TTr := [tele]) false (test_hyp1) test_hyp1 id M1 M2 S T V ∧ S = S ∧ T = T ∧ V = V.
  Proof.
    do 6 eexists. split.
    - find_biabd_disj_ltac2 ().
    - simpl. repeat ltac1:(split; first reflexivity); reflexivity. 
  Qed.

  Lemma retest_4 :
    ∃ TT M1 M2 S T V,
    BiAbdDisjMod (TTl := TT) (TTr := [tele_pair Z]) false (test_hyp2) Q id M1 M2 S T V ∧ S = S ∧ T = T ∧ V = V.
  Proof.
    do 6 eexists. split.
    - find_biabd_disj_ltac2 ().
    - simpl. repeat ltac1:(split; first reflexivity); reflexivity. 
  Qed.

  Lemma retest_5 :
    ∃ TT M1 M2 S T V,
    BiAbdDisjMod (TTl := TT) (TTr := [tele_pair Z]) false (test_hyp3) Q (fupd ⊤ ⊤) M1 M2 S T V ∧ S = S ∧ T = T ∧ V = V.
  Proof.
    do 6 eexists. split.
    - find_biabd_disj_ltac2 ().
    - simpl. repeat ltac1:(split; first reflexivity); reflexivity. 
  Qed. *)

End test.



