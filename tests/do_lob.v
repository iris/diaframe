From diaframe Require Import lib.do_lob tele_utils proofmode_base.
From iris.proofmode Require Import proofmode.

Section instance_tests.

  Lemma test1 (n : nat) : ∃ f, AsFunOfOnly (TT := [tele_pair nat]) (n + 2) [tele_arg3 n] f ∧ f = (λ m, m + 2).
  Proof. eexists. split. tc_solve. done. Qed.

  Lemma test2 (n m : nat) : ∃ TT2 targ f, AsFunOfAmongOthers (TT := [tele_pair nat]) (TT2 := TT2) (n + m) [tele_arg3 n] targ f ∧ f = f.
  Proof. do 3 eexists. split. tc_solve. Show. done. Qed.

End instance_tests.


Section generalization_tests.
  Context `{!BiLöb PROP}.

  Lemma test3 `(a : A) `(b : B) (f : A → B → PROP) g : 
    (□ (∀ c, g c ∗ (∃ c2, f c c2) ∗ 
        (* desired löb IH below *)
        ▷ (∀ (x1 : A) (x2 : B), ((emp ∗ g x1) ∗ f x1 x2) ∗ emp ∗ <affine> True -∗ g x1) ∗ True -∗ 
        post_löb (g c))) ∗ 
      f a b ∗ g a ⊢ do_löb [tele_pair A] [tele_arg3 a] (λ _, emp) (λ c, g c).
  Proof.
    assert (NoLobGen f) by by constructor.
    assert (NoLobGen g) by by constructor.
    assert (NoLobGen A) by by constructor.
    iStep.
    iStep.
    iStep.
    iStep.
    iStep.
    iStep.
    iStep.
    iStep.
    iStep. iFrame "#". (* this frames the IH away *)
  Qed.

End generalization_tests.