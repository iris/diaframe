From diaframe Require Import proofmode_base.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.

Section test.
Context `{!BiAffine PROP}.
Implicit Types A B C D : PROP.

Context (P : nat → PROP).

Fixpoint P_or (n : nat) : PROP :=
  match n with
  | O => P 0
  | S n => P_or n ∨ P (S n)
  end.

Fixpoint P_sep (n : nat) : PROP :=
  match n with
  | O => P 0
  | S n => P_sep n ∗ P (S n)
  end.

Definition P_prob (n : nat) : PROP := P_sep (S n) -∗ (P_or (S n) ∗ P_sep n) ∧ (P_or (S n) ∗ P_sep n).

Lemma P_prob_taut : emp ⊢ ∀ n, P_prob n.
Proof. iSmash. Qed.

Lemma test_sep_speed : P_sep 10 ⊢ P_sep 10.
Proof.
  cbn.
  iStep 2. iSteps.
(* These were measured just after implementing the reified prop store: *)
(* time iStep 2 | iStep. time iStep. *)
(* --------------------------------  *)
(* 10: 0.4 s    | 0.3 s *)
(* 20: 1.2 s    | 0.9 s *)
(* 30: 2.4 s    | 1.8 s *)
(* 40: 4.2 s    | 3.3 s *)
(* 50: 6.9 s    | 6.4 s *)
(* 60: 10.7 s   | 8.2 s *)
(* 70: 15.2 s   | 11.2 s *)
(* 80: 21.7 s   | 15.9 s *)
(* 90: 29.7 s   | 22.2 s *)
(* 100: 40.1 s  | 27.9 s *)

(* The following were measured on Diaframe pre-reified prop store: *)
(* time iStep 2. *)
(* 10: 0.7 s *)
(* 20: 2.9 s *)
(* 30: 5.7 s *)
(* 40: 10.7 s *)
(* 50: 17.1 s *)
(* 60: 26.4 s *)
(* 70: 36.8 s *)
(* 80: 51.1 s *)
(* 90: 67.5 s *)
(* 100: 87.8 s *)
Qed.

(* The problem was that the ∧ caused backtracking across the and branches, dramatically worsening performance.
   This should no longer be the case: time (iStep; iSmash) and time (iSmash) should give comparable timings. *)
Let n := 4%nat.

Lemma test_and_4 : P_sep (S n) ⊢ (P_or (S n) ∗ P_sep n) ∧ (P_or (S n) ∗ P_sep n).
Proof. 
  simpl. iStep.
  time (iStep; iSmash). (* solving it twice, in parallel *)
  Undo 1.
  time iSmash. (* solving it twice, should also be in parallel *)
Qed.

Lemma test_fail_4 : P_sep (S n) ⊢ (P_or (S n) ∗ True) ∧ (P_or (S n) ∗ P_sep n ∗ P (S (S n))).
Proof. 
  simpl. iStep.
  Time (iStep; first iSmash).
  Time Fail iSmash. (* try proving second goal, failure time should be comparable to total failure time *)
  Undo 2.
  Time Fail (iStep; iSmash). (* trying to solve in parallel. first goal is provable in different ways, 
        second goal unprovable so causes failure. *)
  Time Fail iSmash. (* solving it twice, should also be in parallel *)
Abort.

End test.