From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.heap_lang Require Import proofmode.

(* This file can be used to test the overhead of Diaframe's automation on relatively simple goals. *)

Section stress_test1.
  Context `{!heapGS Σ}.

  Definition sum_rec : val :=
    rec: "sum_until" "l" "x" "i_max" :=
      if: ("x" ≤ "i_max") then
        "l" <- !"l" + "x" ;; 
        "sum_until" "l" ("x" + #1) "i_max"
      else #().

  Definition sum_until : val :=
    λ: "i_max",
      let: "l" := ref #0 in
      sum_rec "l" #1 "i_max" ;; 
      ! "l".

  Proposition sum_until_20_direct : {{{ True }}} sum_until #20 {{{ RET #210; True }}}.
  Proof.
    iSteps.
    time repeat (try (iIntros "!>"); wp_lam; iSteps). (* 24.8 seconds *)
  Time Qed. (* 19.5 seconds *)

  Instance sum_rec_spec (l : loc) (n m s : Z) : 
    SPEC {{ l ↦ #s }}
      sum_rec #l #n #m 
    {{ (z : Z), RET #(); l ↦ #z ∗ (⌜z = (s + ((n + m - 1) * (m - n) + 2 * m) / 2)%Z⌝ ∗ ⌜n ≤ m⌝%Z ∨ ⌜z = s⌝ ∗ ⌜m < n⌝%Z) }}.
  Proof.
    iSteps.
    iPureIntro. assert (x2 = x3) as ->; lia.
  Qed.

  Instance sum_until_spec (n : Z) : 
    SPEC {{ ⌜(0 ≤ n)%Z⌝ }} sum_until #n {{ RET #(n * (n + 1) / 2)%Z; True }}.
  Proof. iSteps. Qed.

  Proposition sum_until_20_from_spec : {{{ True }}} sum_until #20 {{{ RET #210; True }}}.
  Proof. iSteps. Qed.

End stress_test1.

Section stress_test2.
  Context (PROP : bi).

  (** Discussed during Iris Workshop 2023, see also
      https://gitlab.mpi-sws.org/iris/refinedc/-/blob/master/theories/lithium/benchmarks/liWand.v

    Format: time during workshop => time w/o fast-short circuiting => time with only for solvesep => time with on solveone, solvesep .. => time without double TC solving?
  *)

  Goal ∀ (P : nat → PROP),
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 20, P i)%I in exact x) -∗
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 20, P i)%I in exact x).
  Proof.
    time iSteps. (* 2.5 seconds => 0.9 seconds => 0.8 seconds => 0.7 seconds => 0.63 secs*)
  Qed.

  Goal ∀ (P : nat → PROP),
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 40, P i)%I in exact x) -∗
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 40, P i)%I in exact x).
  Proof.
    time iSteps. (* 11.4 seconds => 3.0 seconds => 2.7 seconds => 2.4 seconds => 2.3 => 2.0 secs *)
  Qed.

  Goal ∀ (P : nat → PROP),
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 60, P i)%I in exact x) -∗
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 60, P i)%I in exact x).
  Proof.
    time iSteps. (* 30 seconds => 7.5 seconds => 6.7 seconds => 6.0 seconds => 5.4 s => 5.183 secs *)
  Qed.

  Goal ∀ (P : nat → PROP),
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 80, P i)%I in exact x) -∗
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 80, P i)%I in exact x).
  Proof.
    time iSteps. (* 63 seconds => 15.7 seconds => 13.3 seconds => 12.0 seconds => 11.5 s => 10.6 secs *)
  Qed.

  Goal ∀ (P : nat → PROP),
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 100, P i)%I in exact x) -∗
      ltac:(let x := eval simpl in ([∗ list] i ∈ seq 0 100, P i)%I in exact x).
  Proof.
    time iSteps. (* 115 seconds => 29.3 seconds => 24 seconds => 22.1 seconds => 20.0 s => 18.978 secs *)
  Qed.

  (** The shape of this is bad for Diaframe: we start searching from the 'tail'
     of the context (i.e. [P n]), while we first need ([P 0]). So initially,
     we will start with a failing hint search for every [P i] with [0 < i ≤ n].
     There is also a scaling issue with introducing hypotheses, but that is 
     mainly due to environments [Δ] being repeated in the proof term. *)
End stress_test2.


Section stress_test_old.
  Context `{!heapGS Σ}.

  Proposition sum_until_20_old : {{{ True }}} sum_until #20 {{{ RET #210; True }}}.
  Proof.
    iIntros (Φ) "_ Post".
    wp_rec.
    wp_alloc l as "Hl".
    do 2 wp_pure _.
    wp_lam.
    time "Old implementation of wp_tacs" repeat (
      wp_load; wp_store; wp_pures; wp_lam); wp_load. (* 5.181 secs *)
    by iApply "Post".
  Time Qed.

End stress_test_old.



















