From diaframe.hint_search Require Import biabd_disj_query instances_base biabd_disj_from_path.
From diaframe Require Import utils_ltac2 solve_defs.
From iris.bi Require Import bi.
From stdpp Require Import telescopes.





Inductive CanConstructBiAbd {PROP : bi} (P : PROP) (M : PROP → PROP) (TTr : tele) (Q : TTr -t> PROP) : Prop :=
  construct_biabd_disj_mod (TTl : tele) R S D M1 M2 : 
    BiAbdDisjMod (TTl := TTl) false P Q M M1 M2 R S D →
    CanConstructBiAbd P M TTr Q.

Existing Class CanConstructBiAbd.
Global Existing Instance construct_biabd_disj_mod.


Ltac2 reify_biabd_hyp (_ : unit) : bi_prop :=
  lazy_match! goal with
  | [|- @BiAbdDisjMod _ _ ?ttr ?b ?p ?q _ _ _ _ _ _ ] =>
    reify_prop p
  | [|- CanConstructBiAbd ?p _ _ _ ] =>
    reify_prop p
  | [|- CanConstructBiAbd ?p _ _ _ ∧ _ ] =>
    reify_prop p
  end.


Section test.
  Context {PROP : bi}.
  Implicit Types A B C D : PROP.
  Context (P Q R : bool → Z → PROP) (S : PROP) (P' Q' R' : Z → PROP).
  Context `{!BiBUpd PROP, !BiFUpd PROP, !BiBUpdFUpd PROP}.
  Context (HRS : ∀ b z, R b z ⊢ S).

  Definition T := fun z b => (Q b z ∨ R b z)%I.

  Definition test_prop : PROP := ∃ z b, P b z ∗ |==> T z b.

  #[local] Instance atom_into_test b n : AtomIntoConnective (R b n) (emp ∗ S). 
  Proof. rewrite /AtomIntoConnective. rewrite left_id. apply HRS. Qed.

  Definition test1 : CanConstructBiAbd test_prop bupd TeleO S := ltac:(apply _).
  Definition test2 : CanConstructBiAbd test_prop bupd [tele_pair bool; Z] Q := ltac:(apply _).

  Typeclasses Opaque test_prop.
  Definition test3 : TCIf (CanConstructBiAbd test_prop bupd TeleO S) TCFalse TCTrue := ltac:(apply _).

  Definition test_hyp1 : PROP := ∀ z, P' z ∗ Q' z.

  Definition test4 : CanConstructBiAbd test_hyp1 id [tele_pair Z] P' := ltac:(apply _).
  Definition test5 : CanConstructBiAbd test_hyp1 id [tele_pair Z] Q' := ltac:(apply _).

  Typeclasses Opaque test_hyp1.
  Definition test6 : TCIf (CanConstructBiAbd test_hyp1 id [tele_pair Z] P') TCFalse TCTrue := ltac:(apply _).

  Global Instance test_hyp1_into_forall :
    AtomIntoForall test_hyp1 (λ z, P' z ∗ Q' z)%I.
  Proof. by rewrite /test_hyp1 /AtomIntoForall /IntoForallCareful. Qed.

  Definition test7 : CanConstructBiAbd test_hyp1 id [tele_pair Z] P' := ltac:(apply _).
  Definition test8 : CanConstructBiAbd test_hyp1 id [tele_pair Z] Q' := ltac:(apply _).

  Definition test_hyp2 : PROP := ∃ z, P' z -∗ (Q' z ∨ False).

  Definition test9 : CanConstructBiAbd test_hyp2 id [tele_pair Z] Q' := ltac:(apply _).

  Definition test_hyp3 : PROP := ∀ y, P' y ={⊤}=∗ (∃ z, Q' z ∗ ⌜z = (2 * y)%Z⌝) ∨ False.

  Definition test10 : CanConstructBiAbd test_hyp3 (fupd ⊤ ⊤) [tele_pair Z] Q' := ltac:(apply _).

  Tactic Notation "dotime" tactic(tac) := tac.
  Ltac2 Notation "dotime" tac(thunk(tactic)) := (tac ()).

  Typeclasses Opaque test_hyp1.
  Lemma test_speed1 : CanConstructBiAbd test_hyp1 id [tele_pair Z] R'. (* not possible! *)
  Proof.
    dotime
    (do 20 try (
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      find_biabd_disj
    )).

    dotime
    (do 100 try (
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      construct_biabd_disj_from_path; tc_solve
    )).

    ltac2:(
      let rp := reify_biabd_hyp () in
      dotime (do 150 (try (
        ltac1:(autoapply @construct_biabd_disj_mod with typeclass_instances);
        construct_biabd_disj_pre_reified_on_matched_goal rp; tc_solve ()
      )))
    ).
  Abort.

  Lemma test_speed2 : CanConstructBiAbd test_hyp1 id [tele_pair Z] P' ∧ False.
  Proof.
    dotime 
    (do 20 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      find_biabd_disj | ]]).

    dotime
    (do 30 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      construct_biabd_disj_from_path; tc_solve | ]]).

    ltac2:(
      let rp := reify_biabd_hyp () in
      dotime (do 35 (try ((
          split > [ ltac1:(autoapply @construct_biabd_disj_mod with typeclass_instances);
            construct_biabd_disj_pre_reified_on_matched_goal rp; tc_solve () | ]; exact I))
      ))
    ).
  Abort.

  Lemma test_speed3 : CanConstructBiAbd test_hyp1 id [tele_pair Z] Q' ∧ False.
  Proof.
    dotime
    (do 20 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      find_biabd_disj | ]]).

    dotime
    (do 30 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      construct_biabd_disj_from_path; tc_solve | ]]).

    ltac2:(
      let rp := reify_biabd_hyp () in
      dotime (do 32 (try ((
          split > [ ltac1:(autoapply @construct_biabd_disj_mod with typeclass_instances);
            construct_biabd_disj_pre_reified_on_matched_goal rp; tc_solve () | ]; exact I))
      ))
    ).
  Abort.

  Lemma test_speed4 : CanConstructBiAbd test_hyp1 id TeleO test_hyp1 ∧ False.
  Proof.
    dotime
    (do 20 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      find_biabd_disj | ]]).

    dotime
    (do 65 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      construct_biabd_disj_from_path; tc_solve | ]]).

    ltac2:(
      let rp := reify_biabd_hyp () in
      dotime (do 95 (try ((
          split > [ ltac1:(autoapply @construct_biabd_disj_mod with typeclass_instances);
            construct_biabd_disj_pre_reified_on_matched_goal rp; tc_solve () | ]; exact I))
      ))
    ).
  Abort.

  Lemma test_speed5 : CanConstructBiAbd test_hyp2 id [tele_pair Z] Q' ∧ False.
  Proof.
    dotime
    (do 20 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      find_biabd_disj | ]]).

    dotime
    (do 30
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      construct_biabd_disj_from_path; tc_solve | ]]).

    ltac2:(
      let rp := reify_biabd_hyp () in
      dotime (do 32 (try ((
          split > [ ltac1:(autoapply @construct_biabd_disj_mod with typeclass_instances);
            construct_biabd_disj_pre_reified_on_matched_goal rp; tc_solve () | ]; exact I))
      ))
    ).
  Abort.

  Lemma test_speed6 : CanConstructBiAbd test_hyp3 (fupd ⊤ ⊤) [tele_pair Z] Q' ∧ False.
  Proof.
    dotime
    (do 20 
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      find_biabd_disj | ]]).

    dotime
    (do 33
    try solve [split; [
      autoapply @construct_biabd_disj_mod with typeclass_instances;
      construct_biabd_disj_from_path; tc_solve | ]]).

    ltac2:(
      let rp := reify_biabd_hyp () in
      dotime (do 37 (try ((
          split > [ ltac1:(autoapply @construct_biabd_disj_mod with typeclass_instances);
            construct_biabd_disj_pre_reified_on_matched_goal rp; tc_solve () | ]; exact I))
      ))
    ).
  Abort.
End test.



