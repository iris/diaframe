From diaframe Require Import proofmode_base.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.

Section test.
  Context {PROP : bi}.

  Context {A : Type} {Q' : A → PROP}.

  Definition Q : PROP := ∀ a, Q' a.

  Instance Q_as_forall : IntoForallCareful Q Q'.
  Proof. unfold Q. tc_solve. Qed.

  Lemma test1 : Q ⊢ Q ∗ emp.
    (* TODO: this should actually fail! that it succeeds indicates that other biabds are tried after detecting that forall does not work *)
  Proof. iSteps. Abort.

  Definition Q2 : PROP := ∀ QS, (∀ b, Q' b -∗ QS b) -∗ ∀ a, QS a.

  Lemma test2 (a : A) : Q2 ⊢ ∃ a, Q' a ∗ True.
  Proof. Fail solve [iSteps]. Abort. (* produces extra goal, to instantiate QS *)

  Instance Q2_atom_and_connective : AtomIntoForall Q2 Q'.
  Proof. rewrite /AtomIntoForall /IntoForallCareful. iSteps. Qed.
  (* tells Diaframe to only see Q2 as ∀ a, Q' a *)
  Typeclasses Opaque Q2.

  Lemma test3 (a : A) : Q2 ⊢ ∃ a, Q' a ∗ True.
  Proof. iSteps. apply a. Qed. (* now goal gets solved *)

  Lemma test4 (a : A) : Q2 ⊢ ∃ a, Q' a.
  Proof. iStep. iStep. Show. iStep. Qed. (* show tests that it does not mention an instantiation of QS *)

End test.