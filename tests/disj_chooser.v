From diaframe Require Import proofmode_base.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.

Section test.
Context {PROP : bi}.
Implicit Types A B C D : PROP.

Section test_isteps.
  Lemma lem_test_0 A B C :
    (A ∨ (B ∗ (B -∗ A))) ∗ C ⊢ A ∗ C.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_1 A B C :
    □ (B -∗ A) ∗ (A ∨ B) ∗ C ⊢ A ∗ C.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_1a A B C:
    (A ∨ B) ∗ □ (B -∗ A) ∗ C ⊢ A ∗ C.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_2 A B C D :
    □ (C -∗ B) ∗ (A -∗ B ∨ C) ∗ D ∗ A ⊢ B ∗ D.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_2a A B C D :
    (A -∗ B ∨ C) ∗ D ∗ □ (C -∗ B) ∗ A ⊢ B ∗ D.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_2c A B C D :
    (A -∗ B ∨ (C ∗ (C -∗ B))) ∗ D ∗ A ⊢ D ∗ B.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_0_one A B C :
    (A ∨ (B ∗ (B -∗ A))) ⊢ A.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_1_one A B C :
    □ (B -∗ A) ∗ (A ∨ B) ⊢ A.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_1a_one A B C :
    (A ∨ B) ∗ □ (B -∗ A) ⊢ A.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_2_one A B C D :
    □ (C -∗ B) ∗ (A -∗ B ∨ C) ∗ A ⊢ B.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_2a_one A B C D :
    (A -∗ B ∨ C) ∗ D ∗ □ (C -∗ B) ∗ A ⊢ B ∗ D.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_2c_one A B C :
    (A -∗ B ∨ (C ∗ (C -∗ B))) ∗ A ⊢ B.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_inleft A B C  :
    A ∗ C ⊢ (A ∨ B) ∗ C.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_inright A B C :
    B ∗ C ⊢ (A ∨ B) ∗ C.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_3 A B C D :
    (C -∗ A ∨ B) ∗ C ∗ D ⊢ (B ∨ A) ∗ D.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_4 A B C D :
    (C -∗ A ∨ B) ∗ C ∗ D ⊢ D ∗ (B ∨ A).
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_inleft_one A B C :
    A ⊢ (A ∨ B).
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_inright_one A B C :
    B ⊢ (A ∨ B).
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_3_one A B C :
    (C -∗ A ∨ B) ∗ C ⊢ B ∨ A.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_inner_sep A B C :
    ⊢ ((True ∗ True) ∨ B) ∨ C.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_unfocus_ex (A : nat → PROP) B : B ⊢ (∃ x, A x) ∨ B.
  Proof. iSteps. Qed.

  Lemma lem_test_choose_pure_left_when_next_wand (φ : nat → Prop) A B : 
    <affine>⌜φ 3⌝ ∗ (A -∗ B) ⊢ (∃ n, (⌜φ n⌝ ∨ B ∗ ⌜n = 0⌝)) ∗ (A -∗ B).
  Proof. iSteps. Qed.

  Lemma lem_test_choose_pure_left_when_next_later_wand (φ : nat → Prop) A B : 
    <affine>⌜φ 3⌝ ∗ (A -∗ B) ⊢ (∃ n, (⌜φ n⌝ ∨ B ∗ ⌜n = 0⌝)) ∗ (▷ (A -∗ B)).
  Proof. iSteps. Qed.

  Lemma lem_test_choose_pure_left_when_next_forall (φ : nat → Prop) A B : 
    <affine>⌜φ 3⌝ ∗ (A -∗ B) ⊢ (∃ n, (⌜φ n⌝ ∨ B ∗ ⌜n = 0⌝)) ∗ (∀ (tt : ()), A -∗ B).
  Proof. iSteps. Qed.

  Lemma lem_test_choose_pure_left_when_next_later_forall (φ : nat → Prop) A B : 
    <affine>⌜φ 3⌝ ∗ (A -∗ B) ⊢ (∃ n, (⌜φ n⌝ ∨ B ∗ ⌜n = 0⌝)) ∗ (▷ ∀ (tt : ()), A -∗ B).
  Proof. iSteps. Qed.

  Lemma lem_test_paper (α β φ ζ ψ : PROP) `{!BiAffine PROP} :
    (α -∗ ((β ∗ φ) ∨ ζ)) ∗ α ∗ (ζ -∗ ψ) ⊢ (β ∨ ψ) ∗ emp.
  Proof. iSteps. Qed.

  Lemma lem_test_paper2 (α β φ ζ ψ ξ : PROP) `{!BiAffine PROP} :
    (ξ -∗ α -∗ ((β ∗ φ) ∨ (ζ ∗ ψ))) ∗ α ∗ ξ ⊢ (β ∨ ψ).
  Proof. iSteps. Qed.

  Lemma lem_test_interactive0 A B C (H : ⊢ C) :
    B ⊢ (A ∨ B) ∗ C.
  Proof. iSteps. iApply H. Qed.

  Lemma lem_test_interactive1 A B C D (H : ⊢ D) :
    (A -∗ B ∨ C) ∗ A ⊢ (B ∨ C) ∗ D.
  Proof. iSteps. all: iApply H. Qed.

  Lemma lem_test_interactive2 A B C D E :
    ((A ∗ B) -∗ C) ∗ (E -∗ A ∗ D) ∗ E ⊢ (C ∨ E) ∗ True.
  Proof.
    iStep. iStep. unseal_diaframe => /=.
    Undo 2. iStep --safe. iRight. iSteps.
    Undo 3. iSmash.
  Qed.

  Lemma keep_alternative_test_atom A B C D :
    AtomIntoWand false D C (A ∨ B) → (* about true for invariants *)
    D ∗ C ⊢ A ∨ B.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_disj_wand A B C D E :
    D ∗ (□(C -∗ B)) ∗ A ∗ E ∗ (E -∗ (A -∗ B) ∨ (A -∗ C)) ⊢ B ∗ D.
  Proof.
  (* shows we can handle wands beneath disjunctions. but crucially relies on the fact that
      □ (C -∗ B) is tried after the spatial hypothesis (E -∗ ...) *)
    iSteps.
  Qed.

  Lemma lem_test_disj_irrelevant_existentials `{BiFUpd PROP} A B (P Q : nat → PROP) : 
     Q 3 ∗ A ⊢ |={⊤}=> ∃ x, (P x ∨ A) ∗ Q x.
  Proof.
    iSteps. (* Relevant existentials are now rebound when a branch of disjunction does not use them! *)
  Qed.

  Lemma lem_test_tricky1 (α α' β β' γ : PROP) `{BiAffine PROP} :
    (α' -∗ α) ∗ (β' -∗ β) ∗ γ ∗ (γ -∗ (α' ∨ β')) ⊢ (α ∨ β) ∗ emp.
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_tricky2 (α α' β β' γ : PROP) `{BiAffine PROP} :
    (α' -∗ α) ∗ (β' -∗ β) ∗ γ ∗ (γ -∗ (α' ∨ β')) ⊢ (α ∨ β).
  Proof.
    iSteps.
  Qed.

  Lemma lem_test_no_later_abort (A B C D G : PROP) `{BiAffine PROP} : 
      (D -∗ A) ∗ (B -∗ ((A ∗ G) ∨ (C ∗ D))) ∗ B ⊢ ▷ A ∗ (G ∨ C).
  Proof. (* encountering this goal in the wild is implausible, but it would break if
            laters would commute better with wands... *)
    iStep as "HDA HAG HB".
    iPoseProof (bi.later_intro with "HAG") as "HAG".
    rewrite bi.later_wand bi.later_or bi.later_sep.
    iStep. (* both goals are now unprovable! *)
    Undo 3.
    iSteps.
  Qed.

  Lemma lem_test_no_commit_to_first_pure_disj (P : Prop) B :
    B ⊢ ⌜¬¬P → P⌝ ∨ B.
  Proof. iSteps. Qed.

  Lemma lem_test_normalize_choose (B : nat → PROP) :
    B 0 ⊢ (∃ x b, (B x ∗ ⌜0 = 1⌝ ∧ ⌜b = false⌝) ∨ (B x ∗ ⌜b = true⌝ )) ∗ True.
  Proof. iSteps. Qed.
End test_isteps.


Section test_disj_chooser.
  Lemma test_istepsafe_waits A B C D :
     A ∗ B ∗ (B -∗ D) ⊢ (B ∨ A) ∗ D.
  Proof.
    iStep.
    Fail solve [iSteps].
    iStep --safe. iRight.
    iSteps.
  Qed.

  Lemma test_istepsafest A B C D :
     (A ⊢ B) (* to simulate missing hint *) →
     A ∗ D ⊢ (B ∨ C) ∗ D.
  Proof.
    move => HAB.
    iStep.
    Fail solve [iSteps].
    iStep --safest. iLeft. rewrite -HAB.
    iSteps.
  Qed.

  Lemma test_ismash A B C D :
     A ∗ B ∗ (B -∗ D) ⊢ (B ∨ A) ∗ D.
  Proof. iSmash. Qed.

  Lemma test_ismash_and_rhs A B C D `{!BiAffine PROP} :
    (C -∗ A) ∗ C ∗ B ⊢ (A ∨ B) ∗ ((C ∗ emp) ∧ (C ∗ emp)).
  Proof. iSmash. Qed.

  Lemma test4 A B C D `{!BiAffine PROP} :
    C ∗ (B -∗ A ∗ D) ⊢ (((A ∨ B) ∨ C)).
  Proof. iSteps. Qed.
End test_disj_chooser.

End test.
