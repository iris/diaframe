From diaframe Require Import utils_ltac2 steps.ipm_hyp_renamer.
From iris.proofmode Require Import proofmode.
From Coq.micromega Require Import Psatz.


Module test_reintro.
  Local Tactic Notation "reintro_after" tactic(the_tac) "as" ne_simple_intropattern_list(xs) :=
    let f := ltac2:(xs tac |- 
      let probs := no_problems () in
      rename_multi_after_closure probs (Ltac1.of_list [xs]) (fun _ => Ltac1.run tac);
      print_problems probs
    ) in
    f xs the_tac.

  Local Tactic Notation "reintro_after_multi" tactic(the_tac) "as" simple_intropattern_list_list_sep(xs, "|") :=
    let f := ltac2:(xs tac |- 
      let probs := no_problems () in
      rename_multi_after_closure probs xs (fun _ => Ltac1.run tac);
      print_problems probs
    ) in
    f xs the_tac.

  Lemma test (A : Type) (x : A) (B : Type) : forall (a' : A), x = a' -> forall (x : B), x = x -> True.
  Proof.
    set (checkme := 1 + 1).
    (* Maximally adverse environment: ident [x] will be reused when introducing the second forall,
      and we have a local [checkme] parameter with the same name as 
      the one internal to [find_fresh_idents_between]. *)
    reintro_after (do 2 intro; subst; intro x; intro) as a b Hb. Undo 1.
    reintro_after (do 2 intro; subst; intro x; intro; exact I) as a b Hb. Undo 1.
    reintro_after_multi (do 2 intro; subst; intro x; intro) as a b Hb. exact I.
  Qed.

  Lemma test2 : (forall (b : bool), b = b) /\ (forall (n : nat) (m : nat), n = n).
  Proof.
    reintro_after_multi (split; intros) as b | n m. Undo 1.
    split;
    ltac2:(Control.enter (fun _ =>
      ltac1:(reintro_after_multi (intros; reflexivity) as xb m)
    )).
  Qed.

  Lemma test3 : (1 = 1) -> forall (m : nat), m = m -> m = m /\ m = m + 1.
  Proof.
    intros H1.
    reintro_after_multi (intros; clear H1) as m Hm. Undo 1.
    reintro_after_multi (unshelve (intros; clear H1)) as m Hm. Undo 1.
    (* check that the unshelve - clear interaction does not unshelve our evar *)
    reintro_after_multi (unshelve (intros; split; [lia|])) as m Hm.
    (* check that [lia], by virtue of [zify] messing with our hypothesis,
       does not break our approach *)
  Abort.

  Lemma test_dep_replace (m : nat) : forall (eq : 3 + m = 5), eq_sym (eq_sym eq) = eq -> forall (n k : nat), m = n -> m = 2.
  Proof.
    intros Hm1 Hm2.
    reintro_after_multi (intros; subst) as n k. lia.
  Qed.
End test_reintro.

Local Tactic Notation "reintro_ipm_after" tactic(the_tac) "as" constr(Hs) :=
  let f := ltac2:(hs_raw tac |- 
    let probs := no_problems () in
    rename_ipm_hyps_after_closure probs hs_raw (fun d => Ltac1.run tac);
    print_problems probs
  ) in
  f Hs the_tac.

Local Tactic Notation "reintro_ipm_after_thunk" tactic(the_tac) "as" constr(Hs) :=
  let f := ltac2:(hs_raw tac |- 
    let probs := no_problems () in
    rename_ipm_hyps_after_closure probs hs_raw (fun _ => ltac1:(tac |- tac tt) tac);
    print_problems probs
  ) in
  f Hs the_tac.

Lemma test {PROP : bi} `{!BiAffine PROP} (P Q R S : PROP) : P ⊢ Q -∗ R -∗ S -∗ Q.
Proof.
  iIntros "HP".

  reintro_ipm_after (iIntros "H1 H2 H3") as "HQ HR HS". Undo 1.

  reintro_ipm_after (iIntros "H1 H2"; iClear "HP"; iIntros "HP") as "HQ HR". Undo 1.

  (* new names _must_ be fresh wrt old names! i.e., closures in between should use anon_namer *)
  reintro_ipm_after_thunk (fun _ => iIntros "H1 H2"; iClear "HP"; iIntros "HP") as "HQ HR". Undo 1.

  reintro_ipm_after_thunk (fun _ => iIntros "H1 H2 H3") as "HQ HR HS". Undo 1.

  reintro_ipm_after_thunk (fun _ => iIntros "H1 H2 H3") as "HQ HR HS". Undo 1.

  reintro_ipm_after (iIntros "???") as "HQ HR HS".
Abort.

Lemma test_order_pers {PROP : bi} (P Q R S : PROP) : ⊢ P -∗ □ Q -∗ □ R -∗ S -∗ False.
Proof.
  reintro_ipm_after (iIntros "H1 #H2 #H3 ?") as "HQ HR HP HS".
Abort.

Local Tactic Notation "reintro_ipm_after_multi" tactic(the_tac) "as" constr_list_sep(Hs, "|") :=
  let f := ltac2:(hs_raw tac |- 
    let probs := no_problems () in
    rename_multi_ipm_hyps_after_closure probs hs_raw (fun d => Ltac1.run tac);
    print_problems probs
  ) in
  f Hs the_tac.

Lemma test_multi {PROP : bi} `{!BiAffine PROP} (P Q R S : PROP) : P ⊢ Q -∗ (R -∗ S -∗ Q) ∧ (S -∗ False).
Proof.
  iIntros "HP".

  reintro_ipm_after_multi (iIntros "H1"; iSplit; iIntros "H2"; [iIntros "H3"| ]) as "HQ HR HS" | "HQ HS". Undo 1.
  reintro_ipm_after_multi (iIntros "H1"; iSplit; iIntros "H2"; [iIntros "H3"| ]) as "HQ HR HS" | "HQ HS". Undo 1.
Abort.

(*
Lemma test_me (A B : PROP) : ⊢ ∀ (b : bool), A ∧ B -∗ True.
Proof.
  iStep 2 as ([|]) "HAB".
Abort.
*)


