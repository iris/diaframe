From diaframe Require Import proofmode_base.
From diaframe.lib Require Import max_prefix_list_hints.
From iris.bi Require Import bi updates.
From iris.proofmode Require Import proofmode.
From iris.algebra Require Import csum.

Section test.
  Context {PROP : bi}.
  Context (A : Type) (f : A → boolO).

  Lemma test_bool_ineq (a : A) : ⌜f a = true⌝ ⊢@{PROP} ⌜f a ≠ false⌝.
  Proof. iSteps. Qed.

  Lemma test_bool_negb_eq (a : A) : ⌜f a = true⌝ ⊢@{PROP} ⌜negb $ f a = false⌝.
  Proof. iSteps. Qed.

  Lemma test_bool_negb_eq2 (a : A) : ⌜negb (f a) = true⌝ ⊢@{PROP} ⌜f a ≠ true⌝.
  Proof. iSteps. Qed.

  Lemma test_bool_negb_eq3 (a : A) : ⌜negb (f a) && true = true⌝ ⊢@{PROP} ⌜f a || false ≠ true⌝.
  Proof. iSteps. Qed.

  Lemma test_bool_negb_eq4 (a : A) : ⌜negb (f a) = true⌝ ⊢@{PROP} ⌜f a ≠ true⌝.
  Proof. iSteps. Qed.

  Lemma test_bool_negb_eq5 (a : A) : ⌜f a = true⌝ ⊢@{PROP} ⌜@eq (ofe_car boolO) (negb $ f a) false⌝.
  Proof. iSteps. Qed.

  Lemma test_pure_intro_false_pure_forall (P : Prop) `{!BiPureForall PROP} : ⊢@{PROP} ⌜False → P⌝.
  Proof. iSteps. Qed.

  Context (g : A → Type → boolO). 
  (* if boolO is not changed into bool everywhere, bool_solver might choke on destructing syntactically-inequal, yet convertible terms *)
  Lemma test_bool_negb_eq6 (a : A) : ⌜@eq (ofe_car boolO) (g a boolO) true⌝ ⊢@{PROP} ⌜@eq (ofe_car boolO) (negb $ g a boolO) false⌝.
  Proof. iSteps. Qed.

  (* equalities on pairs *)
  Lemma test_pair_eq1 : ⊢@{PROP} ∃ (pn : nat * nat), ⌜pn.1 = 0⌝ ∗ ⌜pn.2 = 1⌝.
  Proof. iSteps. Qed.

  Lemma test_pair_eq2 : ⊢@{PROP} ∃ (pn : nat * nat), ⌜0 = pn.1⌝ ∗ ⌜1 = pn.2⌝.
  Proof. iSteps. Qed.

  Lemma test_fupd_mask_is_evar (P : PROP) `{!BiFUpd PROP} : ∃ E, P ⊢ |={⊤,E}=> True ∗ P.
  Proof. eexists. iSteps. Qed.

  Lemma test_destruct_nonnil_list (ns : list nat) : ⊢@{PROP} ⌜ns ≠ []⌝ -∗ True ∗ ∃ n ns', ⌜ns = n::ns'⌝ ∗ True.
  Proof. iSteps. Qed.

  Lemma test_prefix_destruct_no_clear (ls rs : list nat) (H : 1 + 1 = 2) 
    : ⊢@{PROP} ⌜0 :: ls `prefix_of` 0 :: rs⌝ -∗ True.
  (* Diaframe was clearing [H] in unfortunate situations *)
  Proof. iStep. Check H. iStep. Qed.

  Lemma test_can_simplify_some_contradictory_pair (a b c d : nat)
    : ⊢@{PROP} ⌜((Cinl a), to_agree b) ≡ ((Cinr c), to_agree d)⌝ -∗ False.
  Proof. iStep. Qed.

  Lemma test_prove_pair_eq (a b : nat) : ⊢@{PROP} ∃ (pr : nat * nat), ⌜(a, b) = pr⌝ ∗ True.
  Proof. iSteps. Qed.

  Lemma test_failing_simplify_eq (a b c d : nat) (H: a ≠ c) : ⊢@{PROP} ⌜(a, b) = (c, d)⌝ -∗ True.
  Proof.
    iStepDebug. solveStep.
    (* This example used to rely on the simplify_eq call in solveStep.
        It is solved directly now, by the solveStep call above. *)
  Qed.

  Lemma test_bool_decide_simpl (x1 : Z) (H : (x1 ≠ 1)%Z) P Q : 
      P ⊢@{PROP} ⌜bool_decide (x1 = 1) = true⌝ ∗ Q ∗ True ∨ ⌜bool_decide (x1 = 1) = false⌝ ∗ P ∗ True.
  Proof. iSteps. Qed.

  Lemma test_pure_existential_not_mentioned `{!BiFUpd PROP} :
    ⊢@{PROP} |={⊤}=> ∃ (n : nat), ∀ (m : nat), |={⊤}=> True.
  Proof. iSteps. exact 0. Qed.

  Lemma test_intro_nonsense_eq p (Hp : p ≠ 1) P : ⌜p = 1⌝ ⊢@{PROP} P.
  Proof. iSteps. Qed.

  Lemma test_prove_prefix_as (zs1 zs2 : list Z) (Hpref : zs1 `prefix_of` zs2) :
    ⊢@{PROP} ⌜zs1 `prefix_of` zs2 ++ [Z0]⌝ ∗ True.
  Proof. iStep as (Hpref) "". (* TODO: max_prefix_list solver breaks this by calling [clear] *) iStep. Qed.

  Lemma test_integer_equalities_1 : ⊢@{PROP} ∃ z, ⌜(z + 3)%Z = 4⌝ ∗ (emp -∗ True).
  Proof. iSteps. Qed.

  Lemma test_integer_equalities_2 : ⊢@{PROP} ∃ z, ⌜(3 + z)%Z = 4⌝ ∗ (emp -∗ True).
  Proof. iSteps. Qed.

  Lemma test_integer_equalities_3 : ⊢@{PROP} ∃ z, ⌜(3 * z)%Z = 9⌝ ∗ (emp -∗ True).
  Proof. iSteps. Qed.

  Lemma test_integer_equalities_4 : ⊢@{PROP} ∃ z, ⌜(z * 3)%Z = 9⌝ ∗ (emp -∗ True).
  Proof. iSteps. Qed.

  Lemma test_integer_equalities_5 : ⊢@{PROP} ∃ z, ⌜(z * 0)%Z = 0⌝ ∗ (emp -∗ True).
  Proof. iSteps. exact 1. Qed.

  Lemma test_integer_equalities_6 : ⊢@{PROP} ∃ z, ⌜(z * 3 + 5)%Z = 8⌝ ∗ (emp -∗ ⌜z = 1⌝).
  Proof. iSteps. Qed.
End test.