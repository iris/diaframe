From diaframe Require Import proofmode_base.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.
From iris.heap_lang Require Import proofmode.

(* Perennial relies on <. Diaframe used to enforce = *)
Succeed Constraint bi.Logic = bi.Quant.
Succeed Constraint bi.Logic < bi.Quant.

Section test.
  Context `{!heapGS Σ}.
  Implicit Types P : iProp Σ.
  (* This is some tricky stuff to do with universes.
     Basically, introduce_var has been explicitly annotated with universe names at various points,
     since otherwise Coq can come and kneecap you with Universe problems,
     preventing Diaframe from quantifying on propositions. I am not precisely sure why and how,
     but this test case fails without the added annotations. *)


  (* Apparently prod.u0 is a player in the universe hierarchy. This may have to do with
     using the pair type to Extract Dependencies between telescopes. In any case,
     we usually want to/will turn out to set prod.u0 equal to bi.Quant *)

  (* If we set the constraint after test_pt2, Coq 8.14.1 will choke for some obscure reason. *)
  Constraint bi.Quant = prod.u0.

  Set Universe Polymorphism.
  Section pt1.
  Universe test1 test2 test3.

  Lemma test_pt1 (TT : tele@{test1}) (P : TT -t> iProp Σ) : ⊢ ∀ (tt : TT), tele_app@{test1 test2 test3} P tt -∗ ∃ tt', tele_app P tt' ∗ ⌜tt' = tt'⌝.
  Proof. iSteps. Qed.

  End pt1.
  Unset Universe Polymorphism.

  Lemma test_pt2 (TT : tele) (P : TT -t> iProp Σ) : ⊢ ∀ (tt : TT), tele_app P tt -∗ ∃ tt', tele_app P tt' ∗ ⌜tt' = tt'⌝.
  Proof. apply test_pt1. Qed.

  Lemma test_pt3 : ⊢ ∀ (P : iProp Σ), P -∗ P.
  Proof. iSteps. Qed.
End test.

Section test2.
  Context `{!heapGS Σ}.
  (* this all works now we use our own product type in tele_utils *)

  (* Below enforces bi.Quant = prod.u1 *)
  Definition test_prop1 : iProp Σ := ∀ (A B : Type@{prod.u1}), ∀ (pr : A * B), ⌜pr = (pr.1, pr.2)⌝.

  Lemma test_lem1 (Φ Ψ : iProp Σ → iProp Σ) : (∀ R : iProp Σ, Φ R -∗ Ψ R) ∗ Φ emp ⊢ ∃ R, Ψ R.
  Proof. iSteps. Qed.

  (* it should now still be possible to quantify over pairs, but this works independently of the universe constraint *)
  Lemma test_lem2 (Φ Ψ : (nat * nat) → iProp Σ) : (∀ np, Φ np -∗ Ψ np) ∗ Φ (0, 0) ⊢ ∃ a b, Ψ (a, b).
  Proof. iSteps. Qed. 

  Lemma test_lem3 (Φ Ψ : (nat * nat) → iProp Σ) : (∀ np, Φ np -∗ Ψ np) ∗ Φ (0, 0) ⊢ ∃ pr, Ψ pr.
  Proof. iSteps. Qed.
End test2.

From iris.bi Require Import monpred.

Section test3.
  Context (view : biIndex).
  Definition vProp Σ := monPred view (uPredI (iResUR Σ)).
  Definition vPropO Σ := monPredO view (uPredI (iResUR Σ)).
  Definition vPropI Σ := monPredI view (uPredI (iResUR Σ)).

  Context {Σ : gFunctors}.
  Implicit Types P : vProp Σ.

  Lemma test_extract_dep1 P Φ : P ∗ Φ 0 ⊢ ∃ n : nat, P ∗ Φ n.
  Proof. iSteps. Qed.

  Lemma test_extract_dep2 P (Φ : nat → vProp Σ) : P ∗ Φ 0 ⊢ ∃ n : nat, Φ n ∗ P.
  Proof. iSteps. Qed.
End test3.
