From diaframe Require Import proofmode_base.
From iris.bi Require Import bi lib.atomic.
From iris.base_logic Require Import lib.invariants.
From iris.proofmode Require Import proofmode.
From diaframe.lib Require Import iris_hints atomic.

Section later_problems.
  Context {PROP : bi}.
  Implicit Types P Q G : PROP.

  Lemma test_local_later P Q G :
    (P -∗ Q) ∗ ▷ P ∗ G ⊢ ▷ Q ∗ G.
  Proof.
    iStep.
    iSteps. (* Problem: Have ▷ A, need to prove A since Diaframe eagerly introduces ▷ on B *)
    Undo 1.
    iFrame. (* Now G gets removed, so we will introduce the later and everything is fine *)
    iSteps.
  Qed.

  Lemma test_local_later_hard {A : Type} (P Q R : A → PROP) c G :
    Affine (PROP := PROP) True%I → (∀ a, Affine (R a)) →
    ((∀ a, P a -∗ (Q a ∗ R a)) ∗ ▷ P c ∗ G ⊢ (∃ b, ▷ Q b) ∗ G).
  Proof.
    move => HTrue HR.
    iStep. 
    iSteps. 
    Undo 1.
    iFrame.
    iSteps. (* Still a problem now! Works if the ▷ stays outside the existential *)
    Undo 1.
    assert (Inhabited A) by exact (populate c).
    erewrite <-bi.later_exist.
    iSteps.
  Qed.

  Lemma test3_local_later_hard_abd {A : Type} (P Q : A → PROP) c : ▷ P c ∗ (∀ a, P a -∗ Q a) ⊢ ∃ a, ▷ Q a.
  Proof.
    iStep as "HP HPQ".
    iStep. (* abduct could be relatively easily fixed with the new abduct stuff, but BiAbd is not ready.. *)
    Undo 1. 
    iSpecialize ("HPQ" with "HP").
    iSteps. 
  Qed.
End later_problems.


Section fupd_problems.
  Context `{!invGS Σ}.

  Variable P Q R : iProp Σ.

  Section HR.
  Variable HR : HINT ε₁ ✱ [- ; R] ⊫ [id]; P -∗ Q ✱ [emp].

  Lemma test_inv_biabd :
    inv nroot (P -∗ Q) ∗ P ∗ R ⊢ |={⊤}=> ▷ Q ∗ emp.
  Proof. iSteps. Qed.

  Lemma test_inv_abd :
    inv nroot (P -∗ Q) ∗ P ∗ R ⊢ |={⊤}=> ▷ Q.
  Proof.
    iSteps.
  Qed.
    (* now automatic! :) *)
  End HR.

  Lemma test_wand_mod1 (E1 E2 : coPset) :
    (P -∗ |={E1, E2}=> Q) ∗ (True -∗ P ∗ |={E2, E1}=> emp) ⊢ |={E1}=> Q.
  Proof. iSteps. Qed.

  Lemma test_wand_mod2 (E1 E2 : coPset) :
    (P -∗ |={E1, E2}=> Q) ∗ (|={E1}=> P ∗ |={E2, E1}=> emp) ⊢ |={E1}=> Q.
  Proof. iSteps. Qed.

  Lemma test_wand_mod3 (E1 E2 E3 : coPset) :
    (P -∗ |={E3, E2}=> Q) ∗ (|={E1, E3}=> P ∗ |={E2, E1}=> emp) ⊢ |={E1}=> Q.
  Proof. iSteps. Qed.

  Lemma test_forall_mod1 E1 E2 `(S : A → iProp Σ) :
    (∀ a, S a -∗ |={E1, E2}=> Q) ∗ (|={E1}=> ∃ x, S x ∗ |={E2,E1}=> emp) ⊢ |={E1}=> Q.
  Proof. iSteps. Qed.

  Lemma test_forall_mod_equiv E1 E2 `(S : A → iProp Σ) :
    ((∃ a, S a) -∗ |={E1, E2}=> Q) ∗ (|={E1}=> ∃ x, S x ∗ |={E2,E1}=> emp) ⊢ |={E1}=> Q.
  Proof. iSteps. Qed.

  Lemma test_inv_loop_problem S : 
    ExclusiveProp R → Timeless R →
    Q ∗ inv nroot (P ∗ R ∨ S) ∗ R ⊢ |={⊤}=> ▷ P ∨ (Q ∗ R).
  Proof.
    move => HR1 HR2.
    iStep.
    do 6 iStep. (* this is almost the same goal as before, except that we have now assumed 'IsEasyGoal' to break the loop *)
    do 5 iStep. (* the (emp -∗ emp) forces the invariant to be kept open, preventing the invariant hint from being found again *)
    iStep. (* meaning we will try the second case of the disjunction, and then close the invariant to conclude *)
    iSteps.
  Qed.

  Lemma test_no_hangs :
    P ∗ (P ={∅,⊤}=∗ Q) ∗ (Q ={⊤,∅}=∗ R ∗ R) ∗ (|={⊤,∅}=> (True ∗ |={∅,⊤}=> True)) ⊢ |={⊤}=> (R ∗ emp) ∗ (emp -∗ R).
  Proof. 
    timeout 10 iSmash. (* takes 0.5 seconds locally *)
    (* there was a bug where this would hang, caused by asSolveGoalStep being idempotent,
        caused by bad instances for AsSolveGoal M (IntroVars) with M not introducable *)
  Qed.

  Lemma lem_test_cancel_open_inv_for_alloc1 :
    inv nroot (P ∨ Q) ∗ Q ⊢ |={⊤}=> ((▷ inv nroot P) ∨ Q) ∗ emp.
  Proof.
    iSteps. (* this one relies on ε₀ and ε₁ not being explicitly put in -∗ after possible hint application *)
    (* additionally, it uses combined_fupd_fupd_last_top *)
  Qed.

  Lemma lem_test_cancel_open_inv_for_alloc2 :
    inv nroot (P ∨ Q) ∗ Q ⊢ |={⊤ ∖ ∅}=> ((▷ inv nroot P) ∨ Q) ∗ emp.
  Proof.
    iSteps. (* this one relies on ε₀ and ε₁ not being explicitly put in -∗ after possible hint application *)
    (* additionally, this relies on combined_fupd_fupd_subseteq *)
  Qed.

  Lemma lem_test_cancel_open_inv_for_alloc3 :
    inv nroot (P ∨ Q) ∗ Q ⊢ |={⊤ ∖ ∅}=> ((▷ inv nroot (P ∨ R)) ∨ Q) ∗ emp.
  Proof.
    iSteps. (* this relies on the above, and on the fact that we check not introducable only in the case of 
      a lone atom on the left-hand side! *)
  Qed.

  Lemma lem_test_maybe_open_inv_hot_swap : 
    ExclusiveProp Q → Timeless Q →
    inv nroot (P ∨ Q) ∗ Q ⊢ |={⊤}=> ▷ P ∨ R.
  Proof.
    move => HQ1 HQ2. iSteps.
  Qed.

  Lemma lem_test_maybe_open_inv_hot_swap2 : 
    ExclusiveProp Q → Timeless Q →
    inv nroot (P ∨ Q) ∗ Q ⊢ |={⊤}=> (▷ P ∨ R) ∗ emp.
  Proof.
    move => HQ1 HQ2. iSteps.
  Qed.

  Lemma lem_test_maybe_open_inv_cancel : 
    □ R ∗ inv nroot (P ∨ Q) ⊢ |={⊤}=> ▷ P ∨ R.
  Proof.
    Fail solve [iSteps].
    iSmash.
  Qed.

  Lemma lem_test_maybe_open_inv_cancel_2 : 
    □ R ∗ inv nroot (P ∨ Q) ⊢ |={⊤}=> (▷ P ∨ R) ∗ emp.
  Proof.
    iStep.
    Fail solve [iSteps].
    iSmash.
  Qed.

  Lemma lem_test_inv_hot_swap_leftover_2 S :
    HINT Q ✱ [-; emp] ⊫ [id]; P ✱ [R] →
    ExclusiveProp S → Timeless S →
    S ∗ inv nroot (Q ∨ (S ∗ R)) ⊢ |={⊤}=> ▷ P ∗ emp.
  Proof.
    move => HQP HS1 HS2.
    iStep.
    iStep.
    iStep.
    iStep.
  Qed.

  Lemma lem_test_inv_hot_swap_leftover_timeless S :
    HINT Q ✱ [-; emp] ⊫ [id]; P ✱ [R] →
    ExclusiveProp S → Timeless S → Timeless Q →
    S ∗ inv nroot (Q ∨ (S ∗ R)) ⊢ |={⊤}=> P.
  Proof.
    move => HQP HS1 HS2.
    iSteps.
  Qed.

  Lemma lem_test_inv_hot_swap_leftover_later S :
    HINT Q ✱ [-; emp] ⊫ [id]; P ✱ [R] →
    ExclusiveProp S → Timeless S →
    S ∗ inv nroot (Q ∨ (S ∗ R)) ⊢ |={⊤}=> ▷ P.
  Proof. (* TODO: the later-mono hint together with biabd causes trouble here, FIXME *)
    move => HQP HS1 HS2.
    iSteps.
  Abort.

  Lemma lem_test_inv_hot_swap_leftover_indirect2 S :
    HINT Q ✱ [-; emp] ⊫ [id]; P ✱ [R] →
    ExclusiveProp S → Timeless S →
    S ∗ inv nroot ((P ∗ False) ∨ Q ∨ (S ∗ R)) ⊢ |={⊤}=> ▷ P ∗ emp.
  Proof.
    move => HQP HS1 HS2.
    iStep.
    iStep.
    iStep.
    iStep.
  Qed.

  Lemma lem_test_inv_hot_swap_leftover_indirect S :
    HINT Q ✱ [-; emp] ⊫ [id]; P ✱ [R] →
    ExclusiveProp S → Timeless S →
    S ∗ inv nroot ((P ∗ False) ∨ Q ∨ (S ∗ R)) ⊢ |={⊤}=> ▷ P.
  Proof.
    move => HQP HS1 HS2.
    iSteps.
  Qed.

  Lemma lem_test_cancel_open_inv S :
    HINT P ✱ [-; Q] ⊫ [id]; R ✱ [emp] →
    inv (nroot.@"test") P ∗ S ⊢ |={⊤ ∖ ↑nroot.@"test2"}=> (▷ R ∨ S) ∗ emp.
  Proof.
    move => HPQR.
    iSteps.
  Qed.

  Lemma lem_test_mask_condition :
    ∃ E,
    inv (nroot.@"inv") P ⊢ |={⊤, E}=> ▷P ∗ ⌜↑nroot.@"other" ⊆ E⌝ ∗ ▷(P ={E,⊤}=∗ emp).
  Proof.
    eexists. iSteps.
  Qed.

  Lemma test_iInv : inv nroot P ⊢ |={⊤}=> ((Q ∨ Q) ∗ R) ∨ R.
  Proof.
    iStep as "HI".
    iStepDebug.
    iInv "HI" as "HP". Undo 1.
    solveStep.
    iInv "HI" as "HP". Undo 1.
    iLeft.
    solveStep.
    iInv "HI" as "HP". Undo 1.
    solveStep.
    iInv "HI" as "HP".
    iIntros "Hcl". Show.
  Abort.

  Lemma test_au_in_inv (Φ : nat → nat → iProp Σ) : 
    inv nroot (∃ z1, AU <{ ∃∃ z2, Φ z1 z2 }> @ ⊤, ∅ <{ Φ 0 0, COMM True }>) ⊢
    ∃ E, |={⊤, E}=> ∃ z1 z2, Φ z1 z2 ∗ True.
  Proof.
    iStep as "HI". iExists _. iStepDebug.
    solveStep.
    Fail solveStep. 
    (* one would need a later credit to make this work.
      additionally, this used to fail horribly, by trying to cbn away
      a bi_texist when an UNBOUND_REL was present in the term *)
  Abort.
End fupd_problems.


Section alloc_disj_problems.
  Context `{!BiBUpd PROP}.

  Context (O : gname → PROP).
  Context {HO : HINT ε₁ ✱ [-; emp] ⊫ [bupd] γ; O γ ✱ [emp] }.

  Lemma alloc_test : ⊢ |==> ∃ γ1 γ2, O γ1 ∗ O γ2.
  Proof. iSteps. Qed.

  Context (I : PROP → PROP).
  Context {HI : ∀ P, HINT ε₁ ✱ [-; P] ⊫ [bupd]; I P ✱ [I P] }.

  Lemma alloc_inv_test : ⊢ |==> ∃ γ, I (O γ) ∗ I (O γ) ∗ True.
  Proof. iSteps. Qed.

  Lemma alloc_disj_inv_test P : P ⊢ |==> ∃ γ, I (P ∨ O γ) ∗ O γ ∗ True.
  Proof.
    iSteps. (* this is actually a fine goal to stop at! *)
    iAssert (|==> ∃ x, O x)%I as ">HN"; first iSteps. iDecompose "HN" as (x) "HO".
    iExists x. iSteps.
  Qed.

  Context (O1 O2 : gname → PROP).
  Context {HOs1 : HINT ε₁ ✱ [- ; emp] ⊫ [bupd] γ; O1 γ ✱ [O2 γ] }.
  Context {HOs2 : HINT ε₁ ✱ [- ; emp] ⊫ [bupd] γ; O2 γ ✱ [O1 γ] }.
  Context {HO1E : ∀ γ, ExclusiveProp (O1 γ)}.
  Context {HO2E : ∀ γ, ExclusiveProp (O2 γ)}.

  Instance HOs2_Coq_8_14 : BiAbd (TTl := [tele]) (TTr := [tele_pair gname]) false (ε₁)%I O2 bupd emp%I (λ γ, O1 γ).
  Proof. apply _ || ((* this branch & instance is necessary since Coq 8.14.1s unification is not as smart *) exact HOs2). Qed.

  Lemma alloc_disj_test_2 P `{!BiAffine PROP} : P ⊢ |==> ∃ γ, ((P ∨ O1 γ) ∗ O2 γ).
  Proof. iSteps. Qed.

  Lemma alloc_disj_inv_test_2 P : P ⊢ |==> ∃ γ, I ((P ∨ O1 γ) ∗ O2 γ) ∗ O1 γ ∗ True.
  Proof. iSteps. Qed.

  Lemma alloc_disj_inv_test_3 P : P ⊢ |==> ∃ γ, I (((P ∨ O1 γ) ∗ True) ∗ O2 γ) ∗ O1 γ ∗ True.
  Proof. iSteps. Qed.

  Lemma alloc_disj_inv_test_4 P Q : P ⊢ |==> (∃ γ, I ((P ∨ O1 γ) ∗ O2 γ) ∗ O1 γ ∗ True) ∨ Q.
  Proof. iSteps. Qed.

  Lemma alloc_disj_inv_test_5 P Q : P ⊢ |==> (∃ γ, I (((P ∨ O1 γ) ∗ True) ∗ O2 γ) ∗ O1 γ ∗ True) ∨ Q.
  Proof. iSteps. Qed.
End alloc_disj_problems.



















