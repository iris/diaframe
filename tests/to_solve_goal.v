From diaframe Require Import proofmode_base.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.

Section test.
  Context {PROP : bi}.
  Implicit Types P : PROP.

  Lemma tforall_simplifies P : ⊢ ∀.. (tn : [tele_pair nat]), tele_app (TT := [tele_pair nat]) (λ n : nat, ⌜n ≤ S n⌝ ∗ P)%I tn.
  Proof.
    iStep. (* should introduce an (n : nat), NOT a (tn : [tele_pair nat]) *)
    Show.
  Abort.

  (* We may want this to work also in non affine logics. Proper investigation into such logics still needs to be done. *)
  Lemma inspect_sep_affine (φ1 φ2 : Prop) `{!BiAffine PROP} : <affine> (⌜φ1 ∧ φ2⌝) ⊢@{PROP} True.
  Proof.
    iStep.
    assert φ1 as _ by assumption.
    assert φ2 as _ by assumption.
  Abort.
End test.