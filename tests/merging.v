From diaframe Require Import proofmode_base.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.


Section test.
  Context {PROP : bi}.
  Context (P : Z → PROP).
  Open Scope Z_scope.

  Global Instance mergable_persist_P n m :
    MergablePersist (P m) (λ p Pin Pout,
      TCAnd (TCEq Pin (P n)) $
          TCEq Pout ⌜n ≠ m⌝)%I.
  Proof. Admitted.

  Global Instance mergable_persist_P_first m :
    MergablePersist (P m) (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₀)) $
          TCEq Pout ⌜m ≠ 725⌝)%I.
  Proof. Admitted.

  Global Instance mergable_persist_P_last m :
    MergablePersist (P m) (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₁)) $
          TCEq Pout ⌜m ≠ 1337⌝)%I.
  Proof. Admitted.

  Lemma get_all_ineqs x y z `{!BiAffine PROP}
      (* TODO: fix that the BiAffine condition is no longer needed ?*)
    : ⊢ P x -∗ P y -∗ P z -∗ ⌜x ≠ y⌝ ∗ ⌜x ≠ z⌝ ∗ ⌜y ≠ z⌝ ∗ ⌜x ≠ 725⌝ ∗ ⌜y ≠ 1337⌝ ∗ ⌜z ≠ 725⌝.
  Proof. iSteps. Qed.
End test.














