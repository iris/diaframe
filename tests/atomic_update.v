From diaframe Require Import proofmode_base.
From diaframe.lib Require Import atomic except_zero.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.

Section test.
  Context `{!BiFUpd PROP} `{!BiAffine PROP}.
  Implicit Type P Q R : PROP.

  Lemma test_aupd_no_mask P Q R :
    P ∗ Q ⊢ atomic_update (TA := [tele]) (TB := [tele]) ⊤ ∅ (λ _, P) (λ _ _, Q -∗ R) (λ _ _, R).
  Proof. iSteps. Qed.

  Lemma test_aupd_mask_chg1 P Q R :
    Q ∗ (|={⊤,∅}=> (P ∗ ((P ∨ Q) ={∅,⊤}=∗ emp ∗ χ ∗ False))) ⊢ atomic_update (TA := [tele]) (TB := [tele]) ⊤ ∅ (λ _, P) (λ _ _, R) (λ _ _, R).
  Proof. iSteps. Qed.

  Lemma test_aupd_mask_chg2 P1 P2 Q1 Q2 R :
    Q2 ∗ atomic_update (TA := [tele]) (TB := [tele]) ⊤ ∅ (λ _, P1 ∗ P2) (λ _ _, P2) (λ _ _, Q1)
    ⊢ atomic_update (TA := [tele]) (TB := [tele]) ⊤ ∅ (λ _, P1) (λ _ _, Q1 -∗ Q2 -∗ R) (λ _ _, R).
  Proof. iSmash. Qed.
End test.