From diaframe.heap_lang Require Import proof_automation.

Section test.
  Context {PROP : bi}.

  Lemma test_pairV_eq1 : ⊢@{PROP} ∃ (pn : Z * Z), ⌜(#pn.1, #pn.2)%V = (#3, #4)%V⌝ ∗ (emp -∗ emp).
  Proof. iSteps. Qed.

  Lemma test_pairV_eq2 : ⊢@{PROP} ∃ (pn : Z * Z), ⌜(#3, #4)%V = (#pn.1, #pn.2)%V⌝ ∗ (emp -∗ emp).
  Proof. iSteps. Qed.

  Lemma test_rewrite_hint (P Q : Z → PROP) (z1 z2 z3 z4 : Z)
    (H : ∀ z, HINT ε₁ ✱ [-; P z] ⊫ [id]; Q z ✱ [emp]) :
    P z1 ∗ P z2 ⊢ Q z2 ∗ (⌜(#z1, #z2)%V = (#z3, #z4)%V⌝ -∗ P z3 ∗ True).
  Proof. iSteps. Qed.
End test.