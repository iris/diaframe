From diaframe Require Import tele_utils util_classes util_instances.
From iris Require Import prelude bi.bi.
From stdpp Require Import telescopes.




Lemma test1 : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun n : nat => TeleS (fun nv : Vector.t nat n =>
            TeleS (fun b : bool => [tele_pair b = b])))))
    (fun n nv _ _ => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test2 : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun n : nat => TeleS (fun b : bool => [tele_pair Vector.t nat n; b = b]))))
    (fun n _ nv _ => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test3 : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun n : nat => TeleS (fun b : bool => [tele_pair b = b; Vector.t nat n]))))
    (fun n _ _ nv => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test4 : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun b : bool => TeleS (fun n : nat => [tele_pair Vector.t nat n; b = b]))))
    (fun _ n nv _ => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test5 : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun b : bool => TeleS (fun n : nat => [tele_pair b = b; Vector.t nat n]))))
    (fun _ n _ nv => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test6 : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun b : bool => TeleS (fun Hb : b = b => 
        TeleS (fun n : nat => [tele_pair Vector.t nat n])))))
    (fun _ _ n nv => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test1' : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun n : nat => TeleS (fun nv : Vector.t nat n =>
            TeleS (fun b : bool => [tele_pair b = Nat.eqb n 3])))))
    (fun n nv _ _ => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test2' : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun n : nat => TeleS (fun b : bool => [tele_pair Vector.t nat n; b = Nat.eqb n 3]))))
    (fun n _ nv _ => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.

Lemma test3' : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun n : nat => TeleS (fun b : bool => [tele_pair b = Nat.eqb n 3; Vector.t nat n]))))
    (fun n _ _ nv => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.

Lemma test4' : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun b : bool => TeleS (fun n : nat => [tele_pair Vector.t nat n; b = Nat.eqb n 3]))))
    (fun _ n nv _ => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test5' : ∃ TT1 g TT2 h, (ExtractDep (TT := (TeleS (fun b : bool => TeleS (fun n : nat => [tele_pair b = Nat.eqb n 3; Vector.t nat n]))))
    (fun _ n _ nv => nv = nv) TT1 g TT2 h).
Proof. do 4 eexists. tc_solve. Qed.


Lemma test_no_lambda2 (f4 : nat -> bool -> nat -> bool -> Prop) : 
  ∃ TT1 g TT2 h, ExtractDep (TT := [tele_pair nat; bool]) (fun n => f4 n) TT1 g TT2 h.
Proof. do 4 eexists. tc_solve. Qed.

Lemma test_no_lambda3 (f4 : nat -> bool -> nat -> bool -> Prop) : 
  ∃ TT1 g TT2 h, ExtractDep (TT := [tele_pair nat; bool; nat]) (fun n => f4 n) TT1 g TT2 h.
Proof. do 4 eexists. tc_solve. Abort.

Lemma test_no_lambda4 (f4 : nat -> bool -> nat -> bool -> Prop) : 
  ∃ TT1 g TT2 h, ExtractDep (TT := [tele_pair nat; bool; nat; bool]) (fun n => f4 n) TT1 g TT2 h.
Proof. do 4 eexists. tc_solve. Qed.

Lemma test_no_lambda3' (f4 : nat -> bool -> nat -> bool -> Prop) : 
  ∃ TT1 g TT2 h, ExtractDep (TT := [tele_pair nat; bool; nat]) (fun n b => f4 n b) TT1 g TT2 h.
Proof. do 4 eexists. tc_solve. Abort.

Lemma test_no_lambda4' (f4 : nat -> bool -> nat -> bool -> Prop) : 
  ∃ TT1 g TT2 h, ExtractDep (TT := [tele_pair nat; bool; nat; bool]) (fun n b => f4 n b) TT1 g TT2 h.
Proof. do 4 eexists. tc_solve. Qed.

Lemma test_no_lambda4'' (f4 : nat -> bool -> nat -> bool -> Prop) : 
  ∃ TT1 g TT2 h, ExtractDep (TT := [tele_pair nat; bool; nat; bool]) (fun n b m => f4 n b m) TT1 g TT2 h.
Proof. do 4 eexists. tc_solve. Qed.


Lemma test_texist `{!BiAffine PROP} {A B : Type} (D1 : A → Type) (D2 : B → Type) 
  (G : ∀ a, D1 a → PROP) (H : ∀ b, D2 b → PROP) :
  ∃ TT P, FromTExist (∃ (a : A) (b : B) (da : D1 a) (db : D2 b), G a da ∗ H b db) TT P.
Proof. do 2 eexists. tc_solve. Qed.






















