From diaframe Require Import proofmode_base.
From diaframe.steps Require Import automation_state ltac2_store.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.

Section test.
  Context {PROP : bi}.

  Lemma advanced_existentials {A B : Type} (f g : B → PROP) (m : A → B) :
    ((∀ b, g b -∗ f b) ∗ ∃ a, g (m a))%I ⊢ ∃ a, f (m a).
  Proof. iSteps. Qed.

  Context `{!BiBUpd PROP}.

  Lemma advanced_existentials2 {A B : Type} (f g : B → PROP) (m : A → B) :
    ((∀ b, g b -∗ f b) ∗ |==> ∃ a, g (m a))%I ⊢ |==> ∃ a, f (m a).
  Proof. iSteps. Qed.

  Lemma easy_existentials {A B : Type} (f : A → B → PROP) (g : B → PROP) (m : A → B) (z : A) :
    ((∀ a b, g b -∗ f a b) ∗ |==> ∃ a, g (m a))%I ⊢ |==> ∃ b, f z b.
  Proof. iSteps. Qed.

  Lemma very_advanced_existentials2 {A B : Type} (f g : B → PROP) (m : A → A → A → B) (n : A → A → A):
    ((∀ b, g b -∗ f b) ∗ |==> ∃ x y z, g (m (n y x) (n x y) z))%I ⊢ |==> ∃ x y z, f (m (n y x) (n x y) z).
  Proof. iSteps. Qed.

  Lemma test_linear_logic_pure (f : nat → PROP) : f 3 ⊢ ∃ n, f n ∗ ⌜n = 3⌝.
  Proof. iSteps. Qed.

  Lemma very_advanced_existentials2_biabd {A B : Type} (f g : B → PROP) (m : A → A → A → B) (n : A → A → A) a:
    ((∀ b, g b -∗ f b) ∗ |==> ∃ x y z, g (m (n y x) (n x y) z) ∗ ⌜z = a⌝)%I ⊢ |==> ∃ x y z, f (m (n y x) (n x y) z) ∗ ⌜z = a⌝ .
  Proof. iSteps. Qed.

  Lemma very_advanced_existentials3_biabd {A B : Type} (f g : B → B → PROP) (m : A → A → B) (n : A → A → A) a:
    ((∀ b c, g b c -∗ f b c) ∗ |==> ∃ x y z, g (m (n y x) (n x y)) z ∗ ⌜z = a⌝)%I ⊢ |==> ∃ x y z, f (m (n y x) (n x y)) z ∗ ⌜z = a⌝ .
  Proof. iSteps. Qed.

  Lemma advanced_existentials3 {A : Type} (f g : A → A) (G R : A → A → PROP) :
    ((∀ a b, R a b -∗ G (f a) b) ∗ |==> ∃ a y, R a (g y))%I ⊢ |==> ∃ x y, G x (g y).
  Proof. iSteps. Qed.

  Lemma advanced_existentials4 {A : Type} (f g : A → A) (G R : A → A → PROP) :
    ((∀ b a, R a b -∗ G (f a) b) ∗ |==> ∃ a y, R a (g y))%I ⊢ |==> ∃ x y, G x (g y).
  Proof. iSteps. Qed. 
  (* note that the order of these quantifiers has changed. this makes it actually suddenly very difficult,
     since there is no good order of regular forall/exist rules that is able to give the weakest side condition!
      for example, Actema (from https://dl.acm.org/doi/10.1145/3497775.3503692 ) cannot handle this:
        https://prover.dioxygen.io/?goal=RmF0aGVyOigpLT4oKSwgTW90aGVyOigpLT4oKSwgQ2hpbGQ6OigpJigpLCBEZXNjZW5kYW50OjooKSYoKTsgKGV4aXN0cyB5OigpLiBleGlzdHMgYTooKS4gQ2hpbGQoYSwgRmF0aGVyKHkpKSksIChmb3JhbGwgYjooKS5mb3JhbGwgYTooKS4gQ2hpbGQoYSwgYikgLT4gRGVzY2VuZGFudChNb3RoZXIoYSksIGIpKSB8LSBleGlzdHMgeDooKS4gZXhpc3RzIHk6KCkuIERlc2NlbmRhbnQoeCwgRmF0aGVyKHkpKQ%3D%3D
          vs
        https://prover.dioxygen.io/?goal=RmF0aGVyOigpLT4oKSwgTW90aGVyOigpLT4oKSwgQ2hpbGQ6OigpJigpLCBEZXNjZW5kYW50OjooKSYoKTsgKGV4aXN0cyB5OigpLiBleGlzdHMgYTooKS4gQ2hpbGQoYSwgRmF0aGVyKHkpKSksIChmb3JhbGwgYTooKS5mb3JhbGwgYjooKS4gQ2hpbGQoYSwgYikgLT4gRGVzY2VuZGFudChNb3RoZXIoYSksIGIpKSB8LSBleGlzdHMgeDooKS4gZXhpc3RzIHk6KCkuIERlc2NlbmRhbnQoeCwgRmF0aGVyKHkpKQ%3D%3D
      (yes these URLs are very slightly different)
  *)
  Lemma advanced_existentials5 {A B C : Type} (H : B → PROP) (f : C → B) (G : A → B → PROP) :
    (∀ b, H b -∗ ∃ a, G a b) ∗ (|==> ∃ c, H (f c)) ⊢ |==> ∃ a c, G a (f c).
  Proof. iSteps. Qed.

  Lemma advanced_existentials6 (𝔇 : (nat → PROP) → PROP) Ψ `{!BiAffine PROP} : 𝔇 Ψ ∗ Ψ 0 ⊢ ∃ Φ, 𝔇 Φ ∗ Φ 0.
  (* splitting this into a separating conjunction fast requires jumping through some hoops in the implementation *)
  Proof. iSteps. Qed.

  Lemma introduction_example (G H : nat → PROP) D :
    (∀ m, H m -∗ G (m + m) ∨ D) ∗ H 3 ⊢ ∃ n, D ∨ G n.
  Proof.
    iStep. (* FIXME: add extractdep to IntoOr for SolveOne *)
    iStep.
    iStep.
    iStep.
    iStep.
    iStep.
    iStep.
    Fail iStep.
  Abort.

  Lemma double_later_test (P : PROP) : ▷▷P ⊢ ▷▷P ∗ True.
  Proof. iSteps as "HP". Qed.

  Lemma dependent_existentials1 {A B : Type} (D1 : A → Type) (D2 : B → Type)
      (G : ∀ a, D1 a → PROP) (H : ∀ b, D2 b → PROP) a0 da0 b0 db0 :
    G a0 da0 ∗ H b0 db0 ⊢ |==> ∃ a b da db, G a da ∗ H b db ∗ True.
  Proof.
    iStep.
    iStep.
    Show. (* tests that subst is called appropriately to simplify dependent equalties *)
    iSteps.
  Qed.

  Lemma test_disj_existentials {A : Type} (P S : A → PROP) Q R b :
    R ∗ S b ⊢ |==> (∃ (a : A), ((Q ∗ P a) ∨ R) ∗ S a) ∗ (emp -∗ True).
  Proof. iSteps. Qed.

  Lemma test_disj_existentials_2 {A : Type} (P S : A → PROP) Q R b :
    R ∗ S b ⊢ |==> (∃ (a : A), (((Q ∗ P a) ∨ R) ∗ S a) ∨ (P a ∗ S a)) ∗ (emp -∗ True).
  Proof. iSteps. Qed.

  Lemma test_disj_existentials_3 {A : Type} (P S : A → PROP) Q R b :
    R ∗ S b ⊢ |==> (∃ (a : A), (((Q ∗ P a ∗ ⌜0 = 1⌝) ∨ R) ∗ S a) ) ∗ (emp -∗ True).
  Proof. iSteps. Qed.

  (* tests solveOne existential step, usually skipped because of AsSolveGoal shortcut *)
  Lemma test_disj_existentials_5 {A : Type} (P : A → PROP) b : P b ⊢ False ∨ ∃ a, P a.
  Proof. iSteps. Qed.

  Lemma test_nodecomposed_ex_works {A : Type} (P Q R : A → PROP) :
    (∃ a, P a ∗ Q a ∗ R a) ⊢ ∃ a, P a ∗ R a ∗ Q a.
  Proof.
    iIntros "HP". iSteps.
  Qed.

  Lemma test_nodecomposed_ex_and_works {A : Type} (P : A → PROP) (Q R : A → Prop) :
    (∃ a, P a ∧ ⌜Q a⌝ ∧ ⌜R a⌝) ⊢ ∃ a, P a ∗ ⌜R a⌝ ∗ ⌜Q a⌝.
  Proof. (* ∧ is not seen as ∗ here, even though that is valid. this used to work *)
    iIntros "HP". iSteps.
  Abort.

  Lemma test_linear_logic_1 {P Q R: PROP} :
    Persistent P →
    P ∧ Q ⊢ P ∗ Q .
  Proof. iSteps. Qed.

  Lemma test_linear_logic_2 {P Q R: PROP} :
    Persistent P →
    P ∗ Q ⊢ P ∗ Q .
  Proof. iSteps. Qed.

  Lemma test_linear_logic_3 {P Q R: PROP} :
    Persistent P →
    P ∗ Q ⊢ P ∗ P ∗ Q .
  Proof. iSteps. Qed.

  Lemma test_linear_logic_4 {P : PROP} :
    ε₀ ∗ P ⊢ P.
  Proof. iSteps. Qed.

  Lemma test_linear_logic_5 {P : PROP} :
    ε₁ ∗ P ⊢ P.
  Proof. iSteps. Qed.

  Lemma test_linear_pure_intro {P Q : PROP} :
    P ⊢ <affine> ⌜True⌝ -∗ P.
  Proof. iSteps. Qed.

  Lemma test_linear_pure_intro_split {P Q: PROP} :
    P ⊢ <affine> ⌜True = True ∧ True = True⌝ -∗ P.
  Proof. iSteps. Qed.

  Section side_cond_problems.
    Context `{!Inhabited A} {P Q R : A → PROP}.
    Context (the_hint : ∀ (a : A), HINT P a ✱ [- ; emp ∗ R a] ⊫ [id]; Q a ✱ [emp]).
    Local Existing Instance the_hint.

    Lemma test1 (x : A) (B : Type) : ⊢ ∀ (a'' : A), ((∀ a, P a) ∗ ⌜x = a''⌝ ∗  |==> ∃ a, R a)%I  -∗ ∀ (x' : B), |==> ∃ a, Q a ∗ True.
    Proof. iSteps. Qed.

    Lemma test_update_on_pair_equality (a b c d : A) :
        P a ∗ Q a ⊢ P a ∗ (⌜(b, a) = (d, c)⌝ -∗ (Q c ∗ True)).
    Proof. iSteps. Qed.

    Lemma test_update_on_pair_equality3 (a b c d e f : A) (P' : A → PROP) 
        (the_hint' : ∀ (a : A), HINT P a ✱ [- ; emp] ⊫ [id]; P' a ✱ [emp]) :
        P a ∗ Q a ⊢ P' a ∗ (⌜(b, (a, e)) = (d, (c, f))⌝ -∗ (Q c ∗ True)).
    Proof.
      iStartProof2.
      ltac2:(
        let (state, gid) := automation_state.new() in
        let opt := iStep_default_option_set () in
        let iStep_base := iStep_base_config_steps opt in
        let _ := iStep_base state gid in
        let _ := iStep_base state gid in
        let _ := iStep_base state gid in
        let _ := iStep_base state gid in
        printf "storesize: %i" (store.size (automation_state.get_rp_store_for state gid))
        (* should be zero! *)
      ). iStep.
    Qed.
  End side_cond_problems.

  Section later_exist_problems.
    Context `{!Inhabited A} {Q : A → PROP}.

    Lemma test2 c : Q c ⊢ |==> ∃ a, ▷ Q a ∗ True.
    Proof. iSteps. Qed.

    Lemma test3 c : Q c ⊢ |==> ∃ a, ▷ Q a.
    Proof. iSteps. Qed.

    Context {Qb : PROP}.
    Context (the_hint : HINT Qb ✱ [ - ; emp] ⊫ [bupd] a; Q a ✱ [emp]).

    Lemma test4 : Qb ⊢ |==> ∃ a, ▷ Q a.
    Proof. iSteps. Qed.

  End later_exist_problems.
End test.