From diaframe Require Import proofmode_base.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.

Section test.
Context {PROP : bi}.
Implicit Types A B C D : PROP.


Lemma non_simplified_env_1 : False ⊢@{PROP} False.
Proof.
  iIntros "H". iSteps. (* apparently eauto is smart enough to see this *)
Qed.

Lemma non_simplified_env_2 : False ⊢@{PROP} False ∗ emp.
Proof.
  iIntros "H". iSteps.
  Show.
  iDestruct "H" as %[].
Qed.

Lemma boolean_or `{!BiAffine PROP} A B1 B2 : A ∗ (emp -∗ (((A -∗ B1) ∗ B2) ∨ B2)) ⊢ ∃ (b : bool), (B1 ∗ ⌜b = false⌝ ∨ B2 ∗ ⌜b = true⌝) ∗ A.
Proof. iSmash. Qed.

Context (P : nat → PROP).
Definition exP : PROP := ∃ n, P n.
Typeclasses Opaque exP.

Lemma test_exP_not_unfolded : P 0 ⊢ exP ∗ True.
Proof.
  iStep. Fail solve [iSteps].
  unfold exP. iSteps.
Qed.

End test.