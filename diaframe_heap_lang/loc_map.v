From iris.heap_lang Require Import proofmode.
From diaframe.heap_lang Require Import proof_automation.
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import frac.
From iris.base_logic Require Import ghost_map.


(* version of ghost map where the global registry is 'invisible', and each mapsto can be updated like a physical mapsto.
   tricky part is guaranteeing that newly allocated locations are not yet in the registry. Uses meta tokens to do so,
   so we need the advanced spec of allocation.

   Mainly useful for binding extra 'logical' information to heap locations *)


Class loc_mapG Σ (V : Type) := LocMapG {
  #[local] loc_map_ghost_mapG :: ghost_mapG Σ positive V;
}.
Definition loc_mapΣ (V : Type) : gFunctors :=
  #[ ghost_mapΣ positive V ].

Global Instance subG_loc_mapΣ Σ (V : Type) :
  subG (loc_mapΣ V) Σ → loc_mapG Σ V.
Proof. solve_inG. Qed.

Section improved_alloc.
  Context `{!heapGS Σ}.

  Global Instance alloc_step_wp e v E1 E2 s:
    IntoVal e v →
    SPEC ⟨E1, E2⟩ {{ True }} Alloc e @ s {{ (l : loc), RET #l; l ↦ v ∗ meta_token l ⊤ }} | 18.
  Proof.
    move => <-.
    iStep.
    iApply wp_alloc => //.
    iSteps.
  Qed.

  Global Instance allocN_step_wp e v E1 E2 (n : Z) s:
    IntoVal e v →
    SPEC ⟨E1, E2⟩ {{ ⌜0 < n⌝%Z }} 
      AllocN #n e @ s 
    {{ (l : loc), RET #l; l ↦∗ replicate (Z.to_nat n) v ∗ meta_token l ⊤ ∗ ([∗ list] i ∈ seq 1 (Nat.pred $ Z.to_nat n), meta_token (l +ₗ (Z.of_nat i)) ⊤) }} | 28.
  Proof.
    move => <- /=.
    iStep.
    iApply wp_allocN => //.
    iSteps as (l) "Hmeta".
    destruct (Z.to_nat n) as [|n'] eqn:Heq => //. lia.
    simpl.
    iDecompose "Hmeta" as "Hmeta0 Hmeta".
    replace (l +ₗ 0%nat) with l; first iSteps.
    by destruct l as [[| |]].
  Qed.

  Global Instance meta_agree (l : loc) (N : namespace) `{Countable A} (x1 x2 : A) : 
    MergablePersist (meta l N x1) (λ p Pin Pout,
      TCAnd (TCEq Pin (meta l N x2)) $
            TCEq Pout ⌜x1 = x2⌝%I).
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    iStep as "HN1 HN2".
    iDestruct (meta_agree with "HN1 HN2") as %->.
    iSteps.
  Qed.

End improved_alloc.

Definition N_map := nroot .@ "ghostmap".

Section loc_map.
  Context {V : Type} `{!heapGS Σ, !loc_mapG Σ V}.

  Definition global_reg γm := inv N_map (∃ (m : gmap positive V), ghost_map_auth γm 1 m).

  (* by including global_reg in the definition, updates do not need to look for global_reg. 
      but it does mean this is no longer timeless. is that okay? *)
  Definition loc_map_elem (γm : gname) (l : loc) (dq : dfrac) (v : V) : iProp Σ :=
    ∃ (p : positive), meta l N_map p ∗ p ↪[ γm ]{dq} (v) ∗ global_reg γm.

  (* TODO: loc_map can currently only associate heap information for a location to a single gname,
      since one loses meta_token l ↑N_map. Can we fix this somehow? having a single, globally known
      γm_upper (or something), which does tricks so that it allocates a new key in all the maps for 
      each of the used gnames *)

  Lemma loc_map_elem_alloc (v : V) (γm : gname) (l : loc) E :
    (* TODO: require meta_token N_map instead of meta_token ⊤ *)
    ↑N_map ⊆ E →
    meta_token l ⊤ -∗ global_reg γm -∗ |={E}=> loc_map_elem γm l (DfracOwn 1) v.
  Proof.
    iStep 3 as (HE) "Hγm Hmeta".
    iInv N_map as ">[%m Hm]".
    assert (exists (p : positive), m !! p = None) as [p Hp].
    enough (∃ p, p ∉ dom (D := gset positive) m).
    destruct H as [p Hp].
    exists p.
    refine (iffLR (not_elem_of_dom _ _) Hp).
    apply exist_fresh.
    iMod (ghost_map_insert p v Hp with "Hm") as "[Hm Hγ]".
    iStep.
    iExists p.
    iMod (meta_set ⊤ l p N_map with "Hmeta"). done.
    iSteps.
  Qed.

  Lemma loc_map_elem_meta_token_incompat (γm : gname) (l : loc) (dq : dfrac) (v : V) :
    meta_token l ⊤ -∗ loc_map_elem γm l dq v -∗ ∀ E, ⌜↑N_map ⊆ E⌝ ={E}=∗ False. (* this will be okay since we will never open N_map *)
  Proof.
    iStep 4 as (p E HE) "HN Hγm Hmeta Hp".
    iMod (loc_map_elem_alloc v _ _ _ HE with "Hmeta Hγm") as "Hl".
    iDecompose "Hl" as "HN2 Hγm2 Hp2".
    iDestruct (ghost_map_elem_valid_2 with "Hp Hp2") as %[?Hdq _].
    revert Hdq. rewrite comm. move => /dfracown1_invalid_op //.
  Qed.

  (* not global, since it can interact quite badly with (⋄R = ▷ False ∨ R) and the recursive or rules! *)
  Instance biabd_ex_falso {PROP : bi} (P : PROP) :
    HINT False ✱ [- ; emp] ⊫ [id]; P ✱ [False] | 52.
  Proof. iStep. Qed.

  Lemma loc_map_elem_combine (γm : gname) (l : loc) (dq dq1 dq2 : dfrac) (v : V) :
    proofmode_classes.IsOp dq dq1 dq2 →
    loc_map_elem γm l dq1 v ∗ loc_map_elem γm l dq2 v ⊢ loc_map_elem γm l dq v.
  Proof.
    move => ->.
    iStep 2 as (p) "HN Hγm ?? Hp1 Hp2".
    iSplit; last done.
    iDestruct (ghost_map_elem_combine with "Hp1 Hp2") as "[$ _]".
  Qed.

  Lemma loc_map_elem_agree (γm : gname) (l : loc) (dq1 dq2 : dfrac) (v1 v2 : V) :
    loc_map_elem γm l dq1 v1 -∗ loc_map_elem γm l dq2 v2 -∗ ⌜✓ (dq1 ⋅ dq2) ∧ v1 = v2⌝.
  Proof.
    iStep 2 as (p) "HN Hγm ?? Hp1 Hp2".
    iDestruct (ghost_map_elem_valid_2 with "Hp1 Hp2") as %[? ->].
    iSteps.
  Qed.

  Lemma loc_map_elem_agree_later_l (γm : gname) (l : loc) (dq1 dq2 : dfrac) (v1 v2 : V) :
    ▷ loc_map_elem γm l dq1 v1 -∗ loc_map_elem γm l dq2 v2 -∗ ◇ ⌜✓ (dq1 ⋅ dq2) ∧ v1 = v2⌝.
  Proof.
    iStep 2 as (p) "HN ?? Hγm Hp1 Hp2".
    iDestruct (ghost_map_elem_valid_2 with "Hp1 Hp2") as %[? ->].
    iSteps.
  Qed.

  Lemma loc_map_update (v2 v1 : V) γm (l : loc) E :
    ↑N_map ⊆ E →
    loc_map_elem γm l (DfracOwn 1) v1 ⊢ |={E}=> loc_map_elem γm l (DfracOwn 1) v2.
  Proof.
    iStep 2 as (HE p) "HN Hγm Hp".
    iInv N_map as ">[%m Hm]".
    iMod (ghost_map_update v2 with "Hm Hp") as "[Hm Hp]".
    iSteps.
  Qed.

  Lemma loc_map_discard γm (l : loc) dq (v : V) :
    loc_map_elem γm l dq v ⊢ |==> loc_map_elem γm l (DfracDiscarded) v.
  Proof.
    iStep as (p) "HN Hγm Hp".
    iMod (ghost_map_elem_persist with "Hp") as "Hp".
    iSteps.
  Qed.

  Lemma alloc_global_reg E : 
    ⊢ |={E}=> ∃ γm, global_reg γm.
  Proof.
    iStep.
    iMod (ghost_map_alloc_empty) as "[%γm Hm]".
    iSteps.
  Qed.

  Global Instance loc_map_elem_fractional γm l v : fractional.Fractional (λ q, loc_map_elem γm l (DfracOwn q) v).
  Proof.
    move => q1 q2.
    apply (anti_symm _).
    - iStep as (p) "HN Hγm Hp".
      iDestruct "Hp" as "[Hp1 Hp2]".
      iSteps.
    - iStep as (p) "HN Hγm ?? Hp1 Hp2".
      iCombine "Hp1 Hp2" as "Hp".
      iSteps.
  Qed.

  Instance loc_map_elem_as_fractional γm l v q : fractional.AsFractional (loc_map_elem γm l (DfracOwn q) v) (λ q, loc_map_elem γm l (DfracOwn q) v) q.
  Proof.
    split => //. tc_solve.
  Qed.

  Global Instance loc_map_discarded_persistent γm l v : Persistent (loc_map_elem γm l DfracDiscarded v).
  Proof. tc_solve. Qed.

  Global Instance global_reg_persistent γm : Persistent (global_reg γm).
  Proof. tc_solve. Qed.

  Global Opaque loc_map_elem global_reg.

  Global Instance alloc_global_reg_biabd E :
    HINT ε₁ ✱ [- ; emp] ⊫ [fupd E E] γ; global_reg γ ✱ [global_reg γ].
  Proof.
    iStep.
    iMod (alloc_global_reg) as "[%γ #Hγ]".
    iSteps.
  Qed.

  Global Instance alloc_global_reg_biabd_direct E : (* λ term does not always match *)
    BiAbd (TTl := [tele]) (TTr := [tele_pair gname]) false (ε₁)%I global_reg (fupd E E) emp%I (λ γ, global_reg γ).
  Proof. iSteps. Qed.

  Section mergable.
    Global Instance mergable_consume_mapsto_persist γm l v1 v2 :
      MergableConsume (loc_map_elem γm l DfracDiscarded v1)%I true (λ p Pin Pout, 
        TCAnd (TCEq Pin (loc_map_elem γm l DfracDiscarded v2)) 
              (TCEq Pout (loc_map_elem γm l DfracDiscarded v1 ∗ ⌜v1 = v2⌝)))%I | 40.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl1 Hl2".
      iDestruct (loc_map_elem_agree with "Hl1 Hl2") as "#[_ ->]".
      iSteps.
    Qed.

    Global Instance mergable_consume_mapsto_own γm q1 q2 q l v1 v2 :
      MergableConsume (loc_map_elem γm l (DfracOwn q1) v1)%I true (λ p Pin Pout, 
        TCAnd (TCEq Pin (loc_map_elem γm l (DfracOwn q2) v2)) $ 
        TCAnd (proofmode_classes.IsOp (A := fracR) q q1 q2) $
              (TCEq Pout (loc_map_elem γm l (DfracOwn q) v1 ∗ ⌜v1 = v2⌝)))%I | 30.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> [+ ->]].
      rewrite bi.intuitionistically_if_elim => Hq.
      iStep as "Hl1 Hl2".
      iDestruct (loc_map_elem_agree with "Hl1 Hl2") as "#[_ ->]".
      rewrite Hq. by iCombine "Hl1 Hl2" as "$".
    Qed.

    Global Instance mergable_persist_mapsto_dfrac_own γm q1 dq2 l v1 v2 :
      MergablePersist (loc_map_elem γm l (DfracOwn q1) v1)%I (λ p Pin Pout, 
        TCAnd (TCEq Pin (loc_map_elem γm l dq2 v2)) 
              (TCEq Pout ⌜v1 = v2 ∧ q1 < 1⌝%Qp))%I | 50.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl1 Hl2".
      iDestruct (loc_map_elem_agree with "Hl1 Hl2") as "#[% ->]".
      iSteps. iPureIntro.
      by eapply dfrac_valid_own_l.
    Qed.

    Global Instance mergable_persist_mapsto_dfrac_own2 γm q1 dq2 l v1 v2 :
      MergablePersist (loc_map_elem γm l dq2 v1)%I (λ p Pin Pout, 
        TCAnd (TCEq Pin (loc_map_elem γm l (DfracOwn q1) v2)) 
              (TCEq Pout ⌜v1 = v2 ∧ q1 < 1⌝%Qp))%I | 50.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iSteps.
    Qed.

    (* this last instance is necessary for opaque dq1 and dq2 *)
    Global Instance mergable_persist_mapsto_last_resort γm dq1 dq2 l v1 v2 :
      MergablePersist (loc_map_elem γm l dq1 v1)%I (λ p Pin Pout, 
        TCAnd (TCEq Pin (loc_map_elem γm l dq2 v2)) 
              (TCEq Pout ⌜v1 = v2⌝))%I | 99.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl1 Hl2".
      iDestruct (loc_map_elem_agree with "Hl1 Hl2") as "#[H ->]".
      iSteps.
    Qed.

    Global Instance mergable_persist_mapsto_last_resort_later_l γm dq1 dq2 l v1 v2 :
      MergablePersist (▷ loc_map_elem γm l dq1 v1)%I (λ p Pin Pout, 
        TCAnd (TCEq Pin (loc_map_elem γm l dq2 v2)) 
              (TCEq Pout $ ◇ ⌜v1 = v2⌝))%I | 99.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl1 Hl2".
      iDestruct (loc_map_elem_agree_later_l with "Hl1 Hl2") as "#[Hp1 Hp2]". eauto.
    Qed.

    Global Instance mergable_persist_mapsto_last_resort_later_r γm dq1 dq2 l v1 v2 :
      MergablePersist (loc_map_elem γm l dq1 v1)%I (λ p Pin Pout, 
        TCAnd (TCEq Pin (▷ loc_map_elem γm l dq2 v2)) 
              (TCEq Pout $ ◇ ⌜v1 = v2⌝))%I | 99.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hv Hl1 Hl2".
      iIntros "!>".
      iMod "Hv" as %->.
      iSteps.
    Qed.


    Global Instance mergable_map_elem_meta_token γm l dq1 v :
      MergableConsume (loc_map_elem γm l dq1 v) true (λ p Pin Pout,
        TCAnd (TCEq Pin (meta_token l ⊤))
              (TCEq Pout (∀ E, ⌜↑N_map ⊆ E⌝ ={E}=∗ False)%I)).
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl Hmeta".
      iDestruct (loc_map_elem_meta_token_incompat with "Hmeta Hl") as "$".
    Qed.

    Global Instance mergable_meta_token_map_elem γm l dq1 v :
      MergableConsume (meta_token l ⊤) true (λ p Pin Pout,
        TCAnd (TCEq Pin (loc_map_elem γm l dq1 v))
              (TCEq Pout (∀ E, ⌜↑N_map ⊆ E⌝ ={E}=∗ False)%I)).
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      by iStep.
    Qed.
  End mergable.


  Global Instance loc_map_elem_update_discard γm l v1 v2 E : 
    HINT loc_map_elem γm l (DfracOwn 1) v1 ✱ [- ; ⌜↑N_map ⊆ E⌝] ⊫ [cfupd (↑N_map) E E]; 
         loc_map_elem γm l DfracDiscarded v2 ✱ [loc_map_elem γm l DfracDiscarded v2] | 30.
  Proof.
    iStep as (HE) "Hl". rewrite cfupd_eq.
    iMod (loc_map_update v2 with "Hl") as "Hl" => //.
    iMod (loc_map_discard with "Hl") as "#$".
    iSteps.
  Qed.

  Global Instance loc_map_elem_update γm l q1 mq2 v1 v2 E : 
    FracSub 1 q1 mq2 →
    HINT loc_map_elem γm l (DfracOwn 1) v1 ✱ [- ; ⌜↑N_map ⊆ E⌝] ⊫ [cfupd (↑N_map) E E]; 
         loc_map_elem γm l (DfracOwn q1) v2 ✱ [match mq2 with None => emp | Some q2 => loc_map_elem γm l (DfracOwn q2) v2 end] | 30.
  Proof.
    rewrite /FracSub => Hq.
    iStep as (HE) "Hl". rewrite cfupd_eq.
    iMod (loc_map_update v2 with "Hl") as "Hl" => //.
    destruct mq2.
    - rewrite -Hq.
      iDestruct "Hl" as "[$ $]" => //.
    - rewrite Hq. iSteps.
  Qed.

  Global Instance biabd_loc_map_discard γm dq l v1 v2 :
    HINT loc_map_elem γm l dq v1 ✱ [- ; ⌜v1 = v2⌝] ⊫ [bupd]; 
         loc_map_elem γm l DfracDiscarded v2 ✱ [loc_map_elem γm l DfracDiscarded v2 ∗ ⌜v1 = v2⌝] | 40.
  Proof.
    iStep as "Hl".
    iMod (loc_map_discard with "Hl") as "#$".
    iSteps.
  Qed.

  Global Instance loc_map_frac_need_more γm q1 q2 mq q3 l v1 v2 :
    FracSub q2 q1 mq →
    TCEq mq (Some q3) →
    HINT loc_map_elem γm l (DfracOwn q1) v1 ✱ [v3; ⌜v1 = v2⌝ ∗ loc_map_elem γm l (DfracOwn q3) v3] ⊫ [id]; 
         loc_map_elem γm l (DfracOwn q2) v2 ✱ [⌜v3 = v1⌝ ∗ ⌜v1 = v2⌝] | 55.
  Proof.
    move => <- ->. iStep as (v).
    iSteps.
  Qed.

  Global Instance loc_map_frac_have_enough γm q1 q2 mq3 l v1 v2 :
    FracSub q1 q2 mq3 →
    HINT loc_map_elem γm l (DfracOwn q1) v1 ✱ [- ; ⌜v1 = v2⌝] ⊫ [id]; 
         loc_map_elem γm l (DfracOwn q2) v2 ✱ [⌜v1 = v2⌝ ∗ match mq3 with | Some q => (loc_map_elem γm l (DfracOwn q) v1) | _ => True end] | 54.
  Proof.
    move => <-; iStep as "Hl".
    destruct mq3.
    - iDestruct "Hl" as "[Hl1 Hl2]".
      iSteps.
    - iSteps.
  Qed.

  Global Instance alloc_loc_elem γm l v E q1 mq2 :
    FracSub 1 q1 mq2 →
    HINT meta_token l ⊤ ✱ [- ; global_reg γm ∗ ⌜↑ N_map ⊆ E⌝] ⊫ [cfupd (↑N_map) E E]; 
         loc_map_elem γm l (DfracOwn q1) v ✱ [match mq2 with | None => True | Some q2 => loc_map_elem γm l (DfracOwn q2) v end].
  Proof.
    move => Hq; iStep as (HE) "Hγm Hl".
    rewrite cfupd_eq.
    iMod (loc_map_elem_alloc v with "Hl Hγm") as "H" => //.
    iSteps.
  Qed.

  Global Instance alloc_loc_elem_discard γm l v E :
    HINT meta_token l ⊤ ✱ [- ; global_reg γm ∗ ⌜↑ N_map ⊆ E⌝] ⊫ [cfupd (↑N_map) E E]; 
         loc_map_elem γm l DfracDiscarded v ✱ [loc_map_elem γm l DfracDiscarded v].
  Proof.
    iStep as (HE) "Hγm Hl". rewrite cfupd_eq.
    iMod (loc_map_elem_alloc v with "Hl Hγm") as "H" => //.
    iSteps.
  Qed.

End loc_map.























