From iris.bi Require Export bi derived_laws lib.fractional lib.atomic updates.
From iris.algebra Require Import frac.
From iris.proofmode Require Import proofmode. 
From iris.program_logic Require Import weakestpre.

From diaframe Require Import proofmode_base.
From diaframe.lib Require Export atomic.
From diaframe.lib Require Import persistently intuitionistically.

From diaframe.heap_lang Require Import specs.
From diaframe.symb_exec Require Import defs weakestpre spec_notation.
From diaframe.symb_exec Require Export weakestpre_logatom atom_spec_notation.
From iris.heap_lang Require Import proofmode notation atomic_heap.

Import bi notation.

(* Adds logically atomic specifications for operations in Iris's atomic_heap abstraction *)

Section execution_instances.
  Context  `{!atomic_heap, !heapGS Σ,!atomic_heapGS Σ}.

  Global Instance load_step_atomic (l : loc) :
         (* this extra later is an equivalent specification, but easier to use for difficult situations *)
    SPEC v q, << ▷ l ↦{q} v >> load #l << RET v ; l ↦{q} v >>.
  Proof.
    iSteps as (Φ) "HAU".
    iApply load_spec.
    iApply (atomic_update_mono with "HAU").
    iSteps as (v dq) "Hl" / as (v dq).
    - by iMod "Hl" as ">$".
    - iExists id. simpl. iSteps.
  Qed.

  Global Instance alloc_step_wp e v :
    IntoVal e v →
    SPEC {{ True }} alloc e {{ (l : loc), RET #l; l ↦ v }} | 20.
  Proof.
    move => <-.
    iSteps.
    iApply alloc_spec => //. iSteps.
  Qed.

  Global Instance store_step_atomic (l : loc) (w : val) :
    SPEC v, << ▷ l ↦ v >> store #l w << RET #(); l ↦ w >>.
  Proof.
    iSteps as (Φ) "HAU".
    iApply store_spec.
    iApply (atomic_update_mono with "HAU").
    iSteps as (v) "Hl" / as (v).
    - by iMod "Hl" as ">$".
    - iExists id. simpl. iSteps.
  Qed.

  Global Instance free_step_wp (l : loc) :
    SPEC v, {{ l ↦ v }} free #l {{ (b : base_lit), RET #b; True }}.
  Proof.
    iSteps as (v) "Hl".
    iApply (free_spec with "Hl").
    iSteps.
  Qed.

  Opaque vals_compare_safe.

  Global Instance cmpxchg_step_atomic (l : loc) (v' w : val) :
    SolveSepSideCondition (val_is_unboxed v') →
    SPEC v, << ▷ l ↦ v >> cmpxchg #l v' w << (b : bool), RET (v, #b)%V; ⌜b = true⌝ ∗ ⌜v = v'⌝ ∗ l ↦ w ∨ ⌜b = false⌝ ∗ ⌜v ≠ v'⌝ ∗ l ↦ v >>.
  Proof.
    rewrite /SolveSepSideCondition => Hv.
    iSteps as (Φ) "HAU".
    iApply cmpxchg_spec; first done.
    iApply (atomic_update_mono with "HAU").
    iSteps as (v) "Hl" / as (v).
    - by iMod "Hl" as ">$".
    - iExists (λ _, [tele_arg bool_decide (v = v')]) => /=.
      (* Used to be: iSplit; iSteps; last first. 
        but this broke since our goal now features raw decide goals, 
        which dont simplify how we want them to
      *)
      iSplit; last first.
      { iStep as "HΦ". rewrite bool_decide_decide. destruct (decide (v = v')); iSteps. }
      rewrite bool_decide_decide.
      destruct (decide (v = v')); iSteps.
  Qed.

End execution_instances.

Section biabd_instances.
  Context  {aheap: atomic_heap} `{!heapGS Σ,!atomic_heapGS Σ}.

  Section mergable.
    Global Instance mergable_consume_mapsto_persist l v1 v2 :
      MergableConsume (l ↦□ v1)%I true (λ p Pin Pout, TCAnd (TCEq Pin (l ↦□ v2)) (TCEq Pout (l ↦□ v1 ∗ ⌜v1 = v2⌝)))%I | 40.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl1 Hl2".
      iDestruct (pointsto_agree with "Hl1 Hl2") as "->".
      iSteps.
    Qed.

    Global Instance mergable_consume_mapsto_own q1 q2 q l v1 v2 :
      MergableConsume (l ↦{#q1} v1)%I true (λ p Pin Pout, 
          TCAnd (TCEq Pin (l ↦{#q2} v2)) $ 
          TCAnd (proofmode_classes.IsOp (A := fracR) q q1 q2) $
                (TCEq Pout (l ↦{#q} v1 ∗ ⌜v1 = v2⌝)))%I | 30. (* this does not include q ≤ 1! *)
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> [+ ->]].
      rewrite bi.intuitionistically_if_elim => Hq.
      iStep as "Hl1 Hl2".
      iDestruct (pointsto_agree with "Hl1 Hl2") as "#->".
      iCombine "Hl1 Hl2" as "H".
      rewrite Hq frac_op.
      iSteps.
    Qed.

    Global Instance mergable_persist_mapsto_dfrac_own q1 dq2 l v1 v2 : (* TODO: atomic_heap does not expose that q1 < 1 *)
      MergablePersist (l ↦{#q1} v1)%I (λ p Pin Pout, TCAnd (TCEq Pin (l ↦{dq2} v2)) (TCEq Pout ⌜v1 = v2⌝%Qp))%I | 50.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl1 Hl2".
      iDestruct (pointsto_agree with "Hl1 Hl2") as "#->".
      iSteps.
    Qed.

    Global Instance mergable_persist_mapsto_dfrac_own2 q1 dq2 l v1 v2 : (* TODO: atomic_heap does not expose that q1 < 1 *)
      MergablePersist (l ↦{dq2} v1)%I (λ p Pin Pout, TCAnd (TCEq Pin (l ↦{#q1} v2)) (TCEq Pout ⌜v1 = v2⌝%Qp))%I | 50.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iSteps.
    Qed.

    (* this last instance is necessary for opaque dq1 and dq2 *)
    Global Instance mergable_persist_mapsto_last_resort dq1 dq2 l v1 v2 :
      MergablePersist (l ↦{dq1} v1)%I (λ p Pin Pout, TCAnd (TCEq Pin (l ↦{dq2} v2)) (TCEq Pout ⌜v1 = v2⌝))%I | 99.
    Proof.
      rewrite /MergableConsume => p Pin Pout [-> ->].
      rewrite bi.intuitionistically_if_elim.
      iStep as "Hl1 Hl2".
      iDestruct (pointsto_agree with "Hl1 Hl2") as "#->".
      iSteps.
    Qed.
  End mergable.

  Section biabds.
    Global Instance mapsto_val_may_need_more (l : loc) (v1 v2 : val) (q1 q2 : Qp) mq q :
      FracSub q2 q1 mq →
      TCEq mq (Some q) →
      HINT l ↦{#q1} v1 ✱ [v'; ⌜v1 = v2⌝ ∗ l ↦{#q} v'] ⊫ [id];
           l ↦{#q2} v2 ✱ [⌜v1 = v2⌝ ∗ ⌜v' = v1⌝] | 55.
    Proof. move => <- ->. iSteps. Qed.

    Global Instance mapsto_val_have_enough (l : loc) (v1 v2 : val) (q1 q2 : Qp) mq :
      FracSub q1 q2 mq →
      HINT l ↦{#q1} v1 ✱ [- ; ⌜v1 = v2⌝] ⊫ [id]; l ↦{#q2} v2 ✱ [⌜v1 = v2⌝ ∗ match mq with Some q => l ↦{#q} v1 | _ => True end] | 54.
    Proof.
      move => <-.
      destruct mq; iSteps as "Hl".
      iDestruct "Hl" as "[Hl1 Hl2]".
      iSteps.
    Qed.

    Global Instance as_persistent_mapsto l q v :
      HINT l ↦{q} v ✱ [- ; emp] ⊫ [bupd]; l ↦□v ✱ [l ↦□ v] | 100.
    Proof.
      iStep as "Hl".
      iMod (pointsto_persist with "Hl") as "#Hl".
      iSteps.
    Qed.
  End biabds.

End biabd_instances.




















