From iris.heap_lang Require Import proofmode.
From diaframe.heap_lang Require Import proof_automation.

(* Hints for the 'invariant locations' resources  l ↦_I v  and l ↦_I □ *)

Section hints.
  Context `{!heapGS Σ}.

  Global Instance inv_mapsto_box_access E l (P : val → Prop) : 
    HINT □⟨true⟩ l ↦_P □ ✱ [- ; inv_heap_inv ∗ ⌜↑inv_heapN ⊆ E⌝] ⊫ 
      [cfupd (↑inv_heapN) E (E ∖ ↑inv_heapN)] (v : val) (dq : dfrac); l ↦{dq} v ✱ [⌜dq = DfracOwn 1⌝ ∗ ⌜P v⌝ ∗ (l ↦ v ={E ∖ ↑inv_heapN, E}=∗ True)].
  Proof.
    iStep as (HE) "Hl□ HI". rewrite cfupd_eq.
    iMod (inv_pointsto_acc with "HI Hl□") as "Hl"; first done.
    iDecompose "Hl" as (v HPv) "Hl Hcl". iSteps.
  Qed.

  Global Instance inv_mapsto_val_access E l (P : val → Prop) v :
    HINT l ↦_P v ✱ [- ; inv_heap_inv ∗ ⌜↑ inv_heapN ⊆ E⌝ ] ⊫ 
      [cfupd (↑inv_heapN) E (E ∖ ↑inv_heapN)]; l ↦ v ✱ [⌜P v⌝ ∗ (∀ w, l ↦ w ∗ ⌜P w⌝ ==∗ l ↦_P w ∗ (|={E ∖ ↑inv_heapN, E}=> True))].
  Proof.
    iStep as (HE) "HI HlP". rewrite cfupd_eq.
    iMod (inv_pointsto_own_acc_strong with "HI HlP") as "Hl"; first done.
    iDecompose "Hl" as (HPv) "Hl Hcl". iSteps.
  Qed.

  Global Instance inv_mapsto_alloc l I :
    HINT ε₁ ✱ [v; l ↦_I v] ⊫ [id]; l ↦_I □ ✱ [l ↦_I v ∗ l ↦_I □].
  Proof.
    iStep 2 as (v) "HI".
    iDestruct (inv_pointsto_own_inv with "HI") as "#H".
    iSteps.
  Qed.
End hints.