From iris.bi Require Export bi telescopes derived_laws.
From iris.proofmode Require Import proofmode.
From diaframe.symb_exec Require Import defs weakestpre.
From diaframe Require Export spec_notation.
From diaframe Require Import proofmode_base.
From diaframe.lib Require Import iris_hints.
From iris.heap_lang Require Import proofmode notation lib.lock.

Import bi.

(* This file registers the specifications of heap_langs native (atomic) instructions, like 
   CmpXchg, load, store and FAA instructions. These are written as SPEC, just like the specifications
   we prove in the examples. The main difference here is that to prove these specifications, we use
   lemma's provided by Iris's heap_lang. These are proven using the semantics of the language.

   We can use Diaframe's automation here, to partially 'bootstrap' ourselves. This is because
   Diaframe's automation will just stop when it does not know how to proceed.
 *)

Class PureExecNoRec φ n e1 e2 :=
  is_pure_exec : PureExec (Λ := heap_lang) φ n e1 e2.

Unset Universe Polymorphism.

Section heap_lang_instances.
  Context `{!heapGS Σ}.

  Open Scope expr_scope.

  Global Instance pure_wp_step_exec_inst1 `(e : expr) φ n e' E s:
    PureExecNoRec φ n e e' → (* TODO: prevent unfolding explicit recs *)
    ReductionTemplateStep wp_red_cond (qprod TeleO TeleO) (ε₀)%I [tele_arg3 E;s] e 
      (λ pr, tele_app (TT := [tele]) (tele_app (TT := [tele]) e' $ qfst pr) $ qsnd pr) 
      (template_M n id id TeleO TeleO ⌜φ⌝%I emp%I) | 80.
      (* used when φ is an equality on a new evar: this will cause SolveSepSideCondition to fail *)
      (* this is a ReductionTemplateStep: if it were a ReductionStep, the priority of as_template_step would be considered, not that of this instance *)
  Proof.
    intros.
    refine (pure_wp_step_exec _ _ _ _ _ _ _ _ _). exact H.
  Qed.

  Global Instance pure_wp_step_exec_inst2 (e : expr) φ n e' E s:
    PureExecNoRec φ n e e' →
    SolveSepSideCondition φ →
    ReductionTemplateStep wp_red_cond [tele] (ε₀)%I [tele_arg3 E; s] e (tele_app (TT := [tele]) e') (template_I n (fupd E E))%I | 8.
  Proof. 
    intros. eapply pure_wp_step_exec2 => //. tc_solve.
  Qed.

  Global Instance load_step_wp l E1 E2 s :
    SPEC ⟨E1, E2⟩ v q, {{ ▷ l ↦{q} v }} ! #l @ s {{ RET v; l ↦{q} v }}.
  Proof.
    iSteps as (v q) "Hl".
    iApply (wp_load with "Hl").
    iSteps.
  Qed.

  Global Instance alloc_step_wp e v s:
    IntoVal e v →
    SPEC {{ True }} ref e @ s {{ l, RET #l; l ↦ v }} | 20.
  Proof.
    move => <-.
    iSteps.
    iApply wp_alloc => //.
    iSteps.
  Qed.

  Global Instance allocN_step_wp e v E1 E2 n s:
    IntoVal e v →
    SPEC ⟨E1, E2⟩ {{ ⌜0 < n⌝%Z }} AllocN #n e @ s {{ l, RET #l; l ↦∗ replicate (Z.to_nat n) v }} | 30.
  Proof.
    move => <- /=.
    iSteps.
    iApply wp_allocN => //.
    iSteps.
  Qed.

  Global Instance store_step_wp l v' E1 E2 s :
    SPEC ⟨E1, E2⟩ v, {{ ▷ l ↦ v }} #l <- v' @ s {{ RET #(); l ↦ v' }}.
  Proof.
    iSteps as (v) "Hl".
    iApply (wp_store with "Hl").
    iSteps.
  Qed.

  Global Instance free_step_wp l E1 E2 s :
    SPEC ⟨E1, E2⟩ v, {{ ▷ l ↦ v }} Free #l @ s {{ RET #(); True }}.
  Proof.
    iSteps as (v) "Hl".
    iApply (wp_free with "Hl").
    iSteps.
  Qed.

  Global Instance new_proph_step s :
    SPEC {{ True }} NewProph @ s {{ pvs (p : proph_id), RET #p; proph p pvs }}.
  Proof.
    iSteps.
    iApply wp_new_proph; iSteps.
  Qed.

  Global Instance abduct_resolve_atomic_spec K (e e_in : expr) (p : proph_id) (v : val) s (Φ : val → iProp Σ) pre n E1 E2 (TT1 TT2 : tele)
      L e' v' U M1 M2 :
    ReshapeExprAnd expr e_in K (Resolve e #p v) (TCAnd (LanguageCtx K) $ 
                                                 TCAnd (Atomic StronglyAtomic e) $
                                                 TCAnd (Atomic WeaklyAtomic e) $ (SolveSepSideCondition (to_val e = None))) →
    ReductionStep' wp_red_cond pre n M1 M2 TT1 TT2 L U e e' [tele_arg3 E2; s] → (* does not work for pure since that is a ReductionTemplateStep *)
    IntroducableModality M1 → IntroducableModality M2 →
    (TC∀.. ttl, TC∀.. ttr, IntoVal (tele_app (tele_app e' ttl) ttr) (tele_app (tele_app v' ttl) ttr)) →
    HINT1 pre ✱ [|={E1, E2}=> ∃ pvs, proph p pvs ∗ ∃.. ttl, tele_app L ttl ∗ 
      ▷^n (∀ pvs', ∀.. ttr, ⌜pvs = (pair (tele_app (tele_app v' ttl) ttr) v)::pvs'⌝ ∗ proph p pvs' ∗ tele_app (tele_app U ttl) ttr ={E2,E1}=∗ 
            WP K $ tele_app (tele_app e' ttl) ttr @ s ; E1 {{ Φ }} ) ] 
          ⊫ [id]; WP e_in @ s ; E1 {{ Φ }} | 45.
  Proof.
    case => -> [HK [He1 [He3 He2]]] HLU HM1 HM2 Hev'.
    iStep as "Hpre HL". iApply wp_bind. iMod "HL" as (pvs) "[Hp Hwp]".
    { apply resolve_atomic. destruct s; try tc_solve. }
    iApply (wp_resolve with "Hp"). apply He2. simpl.
    iDestruct "Hwp" as (ttl) "[Hl HΦ]".
    rewrite /ReductionStep' /ReductionTemplateStep in HLU.
    iPoseProof (HLU with "Hpre") as "HWP". simpl.
    iApply "HWP". iApply HM1 => /=.
    iExists ttl. iFrame. iIntros "!>" (tt2) "HU". iApply HM2 => /=.
    revert Hev'. rewrite /TCTForall /IntoVal => /(dep_eval_tele ttl) /(dep_eval_tele tt2) => Hev'.
    rewrite -Hev'.
    iApply wp_value. iIntros (pvs').
    iStep 2 as "Hpost Hproph".
    iSpecialize ("Hpost" $! pvs' tt2). rewrite -Hev'. iApply "Hpost".
    iSteps.
  Qed.

  Global Instance abduct_resolve_skip K (e_in : expr) (p : proph_id) (v : val) s E1 E2 (Φ : val → iProp Σ) :
    ReshapeExprAnd expr e_in K (Resolve Skip #p v) (LanguageCtx K) →
    HINT1 ε₀ ✱ [|={E1, E2}=> ∃ pvs, proph p pvs ∗
      ▷ (∀ pvs', ⌜pvs = (pair (#()) v)::pvs'⌝ ∗ proph p pvs' ={E2,E1}=∗ 
            WP K $ #() @ s ; E1 {{ Φ }} ) ] 
          ⊫ [id]; WP e_in @ s ; E1 {{ Φ }} | 45.
  Proof.
    case => -> HK.
    iStep as "H". iApply wp_bind. iMod "H". iDecompose "H" as (ps) "Hproph Hpost".
    iApply (wp_resolve with "Hproph"). done.
    wp_pures. iStep 3 as (ps') "Hpost Hproph". 
    iMod ("Hpost" with "[Hproph]"); iSteps.
  Qed.

  Opaque vals_compare_safe.


  Global Instance cmpxchg_step_wp_stronger l v1 v2 E1 E2 s :
    SPEC ⟨E1, E2⟩ (v : val) (q : dfrac), 
      {{ ▷ l ↦{q} v ∗ ⌜vals_compare_safe v v1⌝ ∗ ⌜q = DfracOwn 1 ∨ v1 ≠ v⌝ }} 
        CmpXchg #l v1 v2 @ s 
      {{ (b : bool), RET (v, #b)%V; 
          ⌜b = true⌝ ∗ ⌜v = v1⌝ ∗ l ↦{q} v2   ∨
          ⌜b = false⌝ ∗ ⌜v ≠ v1⌝ ∗ l ↦{q} v }}.
  Proof.
    iStep 3 as (v Hv1) "Hl" / as (v q Hv1 Hv2) "Hl".
    - destruct (decide (v = v1)) as [->|Hneq].
      * iApply (wp_cmpxchg_suc with "Hl") => //.
        iSteps.
      * iApply (wp_cmpxchg_fail with "Hl") => //.
        iSteps.
    - iApply (wp_cmpxchg_fail with "Hl") => //.
      iSteps.
  Qed.

  Global Instance xchg_step_wp l v E1 E2 s :
    SPEC ⟨E1, E2⟩ (v' : val), {{ ▷ l ↦ v' }} 
      Xchg #l v @ s 
    {{ RET v'; l ↦ v }}.
  Proof.
    iSteps as (v') "Hl".
    iApply (wp_xchg with "Hl").
    iSteps.
  Qed.

  Global Instance faa_step_wp l i E1 E2 s :
    SPEC ⟨E1, E2⟩ (z : Z), {{ ▷ l ↦ #z }} FAA #l #i @ s {{ RET #z; l ↦ #(z + i) }}.
  Proof.
    iSteps as (z) "Hl".
    iApply (wp_faa with "Hl").
    iSteps.
  Qed.

  (* There is no PureExec for an If statement with an abstract boolean. We create a reduction step for 
      the case where this boolean is a bool_decide. *)

  Global Instance if_step_bool_decide P `{Decision P} e1 e2 E s :
    ReductionStep (wp_red_cond, [tele_arg3 E; s]) if: #(bool_decide P) then e1 else e2 ⊣ ⟨id⟩ emp; ε₀ =[▷^1]=>
      ∃ b : bool, ⟨id⟩ (if b then e1 else e2)%V ⊣ ⌜b = true⌝ ∗ ⌜P⌝ ∨ ⌜b = false⌝ ∗ ⌜¬P⌝| 50.
  Proof.
    (* texan_to_red_cond does not work here, since (if b then e1 else e2) is not a value! *)
    rewrite /ReductionStep' /=.
    apply forall_intro => Φ.
    iIntros "_ [_ H]".
    case_bool_decide; wp_pures => /=.
    - iApply ("H" $! true). eauto.
    - iApply ("H" $! false). eauto.
  Qed.

  Global Instance if_step_bool_decide_neg P `{Decision P} e1 e2 E s :
    ReductionStep (wp_red_cond, [tele_arg3 E; s]) if: #(bool_decide (¬P)) then e1 else e2 ⊣ ⟨id⟩ emp; ε₀ =[▷^1]=>
      ∃ b : bool, ⟨id⟩ (if b then e1 else e2)%V ⊣ ⌜b = true⌝ ∗ ⌜¬P⌝ ∨ ⌜b = false⌝ ∗ ⌜P⌝ | 49.
  Proof.
    rewrite /ReductionStep' /=.
    apply forall_intro => Φ.
    iIntros "_ [_ H]".
    case_bool_decide => /=.
    - wp_pures.
      iApply ("H" $! true). eauto.
    - wp_pures.
      iApply ("H" $! false). eauto.
  Qed.

  Global Instance if_step_negb_bool_decide P `{Decision P} e1 e2 E s :
    ReductionStep (wp_red_cond, [tele_arg3 E; s]) if: #(negb $ bool_decide P) then e1 else e2 ⊣ ⟨id⟩ emp; ε₀ =[▷^1]=>
      ∃ b : bool, ⟨id⟩ (if b then e1 else e2)%V ⊣ ⌜b = true⌝ ∗ ⌜¬P⌝ ∨ ⌜b = false⌝ ∗ ⌜P⌝ | 49.
  Proof.
    rewrite /ReductionStep' /=.
    apply forall_intro => Φ.
    iIntros "_ [_ H]".
    case_bool_decide => /=.
    - wp_pures.
      iApply ("H" $! false). eauto.
    - wp_pures.
      iApply ("H" $! true). eauto.
  Qed.

End heap_lang_instances.

Section extra_lock_step_instances.
  Context (L : lock.lock) `{!heapGS Σ,!lockG Σ}.

  Global Instance acquire_step_wp lk γ R :
    SPEC {{ is_lock γ lk R }} acquire lk {{ RET #(); locked γ ∗ R }}.
  Proof.
    iSteps as "HL".
    iApply acquire_spec; iSteps.
  Qed.

  Global Instance release_step_wp lk γ R :
    SPEC {{ is_lock γ lk R ∗ locked γ ∗ R }} release lk {{ RET #(); True }}.
  Proof.
    iSteps as "HL Hlocked HR".
    iApply (release_spec with "[Hlocked HR]"); iSteps.
  Qed.

End extra_lock_step_instances.

Section unfold_functions.
  Context `{!heapGS Σ}.

  Fixpoint occurs_in (s : string) (body : expr) : bool :=
    match body with
    | Val _ => false
    | Var s' => if decide (s = s') then true else false
    | Rec b x e => if decide (BNamed s ≠ b ∧ BNamed s ≠ x) then occurs_in s e else false
    | App f a => (occurs_in s f) || (occurs_in s a)
    | UnOp _ e => occurs_in s e
    | BinOp _ l r => (occurs_in s l) || (occurs_in s r)
    | If c t e => (occurs_in s c) || (occurs_in s t) || (occurs_in s e)
    | Pair l r => (occurs_in s l) || (occurs_in s r)
    | Fst e => (occurs_in s e)
    | Snd e => (occurs_in s e)
    | InjL e => (occurs_in s e)
    | InjR e => (occurs_in s e)
    | Case c l r => (occurs_in s c) || (occurs_in s l) || (occurs_in s r)
    | Fork e => (occurs_in s e)
    | AllocN n e => (occurs_in s n) || (occurs_in s e)
    | Free e => (occurs_in s e)
    | Load e => (occurs_in s e)
    | Store l e => (occurs_in s l) || (occurs_in s e)
    | CmpXchg l e1 e2 => (occurs_in s l) || (occurs_in s e1) || (occurs_in s e2)
    | Xchg l e1 => (occurs_in s l) || (occurs_in s e1)
    | FAA l n => (occurs_in s l) || (occurs_in s n)
    | NewProph => false
    | Resolve a1 a2 a3 => (occurs_in s a1) || (occurs_in s a2) || (occurs_in s a3)
    end.

  Definition is_recursive_fun (v : val) :=
    match v with
    | RecV (BNamed f) x e => occurs_in f e
    | _ => false
    end.

  Global Instance pure_wp_step_exec_inst_last `(e : expr) φ n e' E s :
    ((∀ f x e, SolveSepSideCondition (is_recursive_fun (RecV f x e) = false) → 
                AsRecV (RecV f x e) f x e) → 
      PureExec φ n e e') →
    SolveSepSideCondition φ →
    ReductionTemplateStep wp_red_cond [tele] (ε₁)%I [tele_arg3 E; s] e (tele_app (TT := [tele]) e') (template_I n (fupd E E)).
  Proof.
    intros. eapply pure_wp_step_exec2 => //. tc_solve.
    apply H. intros. exact eq_refl.
  Qed.

End unfold_functions.

Ltac find_reshape e K e' TC :=
  lazymatch e with
  | fill ?Kabs ?e_inner =>
    reshape_expr e_inner ltac:(fun K' e'' => 
      unify K (fill Kabs ∘ fill K'); unify e' e'';
      notypeclasses refine (ConstructReshape e (fill Kabs ∘ fill K') e'' _ (eq_refl) _); tc_solve )
  | _ =>
    reshape_expr e ltac:(fun K' e'' => 
      unify K (fill K'); unify e' e''; 
      notypeclasses refine (ConstructReshape e (fill K') e'' _ (eq_refl) _); tc_solve )
  end.

Global Hint Extern 4 (ReshapeExprAnd expr ?e ?K ?e' ?TC) => 
  find_reshape e K e' TC : typeclass_instances.

Global Hint Extern 4 (ReshapeExprAnd (language.expr ?L) ?e ?K ?e' ?TC) =>
  unify L heap_lang;
  find_reshape e K e' TC : typeclass_instances.

Global Arguments heap_lang : simpl never.
  (* If not, cbn unfolds heap_lang, and this term is quite large, causing slowdowns in various places *)

From iris.heap_lang.lib Require Import par.

Section par.
  Context `{!heapGS Σ, spawnG Σ}.

  (* i guess it might be sufficient to find only one of the two specs, and execute the other as 'usual'..? *)
  (* that would also make this instance drastically more readable *)
  (* however, how do we encode that you only get the U after executing the other expression? *)
  Global Instance par_both_have_specs e1 e2 n1 n2 TT1_1 TT1_2 TT2_1 TT2_2 e1' v1 e2' v2 L1 L2 U1 U2 :
    ReductionStep' wp_red_cond (ε₀)%I n1 (fupd ⊤ ⊤) (fupd ⊤ ⊤) TT1_1 TT1_2 L1 U1 e1 e1' [tele_arg ⊤; NotStuck] →
    ReductionStep' wp_red_cond (ε₀)%I n2 (fupd ⊤ ⊤) (fupd ⊤ ⊤) TT2_1 TT2_2 L2 U2 e2 e2' [tele_arg ⊤; NotStuck] →
    (TC∀.. tt1 tt2, IntoVal (tele_app (tele_app e1' tt1) tt2) (tele_app (tele_app v1 tt1) tt2)) →
    (TC∀.. tt1 tt2, IntoVal (tele_app (tele_app e2' tt1) tt2) (tele_app (tele_app v2 tt1) tt2)) →
    ReductionStep' wp_red_cond (ε₀)%I 1 (fupd ⊤ ⊤) (fupd ⊤ ⊤) (TelePairType TT1_1 TT2_1) (TelePairType TT1_2 TT2_2) 
      (* and now come the telescopic hoops we have to jump through.. *)
      (* the precondition: ∃.. (tt1 : TT1_1) (tt2 : TT2_1), L1 tt1 ∗ L2 tt1 *)
      (tele_curry $ tele_dep_bind (λ tt1, tele_map (λ P, P ∗ tele_app L1 tt1)%I $ as_dependent_fun TT1_1 TT2_1 L2 tt1))
      (* the post condition: (λ.. tt1 tt2 tt1' tt2', U1 tt1 tt1' ∗ U2 tt2 tt2' *)
      (tele_curry $ tele_dep_bind (λ tt1, as_dependent_fun TT1_1 TT2_1 (tele_bind (λ tt2, 
          tele_curry $ tele_dep_bind (λ tt1', tele_map (λ Ur, tele_app (tele_app U1 tt1) tt1' ∗ Ur)%I $ as_dependent_fun TT1_2 TT2_2 (tele_app U2 tt2) tt1' ))) tt1))
    (par (λ: <>, e1)%V (λ: <>, e2)%V) 
    (* the new expression: (λ.. tt1 tt2 tt1' tt2', (v1 tt1 tt1', v2 tt2 tt2') *)
    (tele_curry $ tele_dep_bind (λ tt1, as_dependent_fun TT1_1 TT2_1 (tele_bind (λ tt2, 
        tele_curry $ tele_dep_bind (λ tt1', tele_map (λ vr : val, of_val $ (tele_app (tele_app v1 tt1) tt1', vr)%V) $ as_dependent_fun TT1_2 TT2_2 (tele_app v2 tt2) tt1' ))) tt1))
    [tele_arg ⊤; NotStuck].
  Proof.
    intros He1 He2 Hev1 Hev2.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iIntros "_" (Φ) ">H".
    iDestruct "H" as (ttp) "[Hpre Hpost]".
    rewrite (tele_arg_concat_inv_eq ttp).
    destruct (tele_arg_concat_inv_strong_hd ttp) as [tt1 tt2'].
    rewrite -(dependent_arg_eq _ _ tt1 tt2').
    generalize (as_non_dependent_arg _ _ tt1 tt2') => tt2 {tt2'}.
    rewrite tele_curry_uncurry_relation.
    rewrite -tele_uncurry_curry_compose.
    rewrite tele_dep_appd_bind.
    rewrite tele_map_app.
    rewrite -dependent_fun_eq.
    iDestruct "Hpre" as "[Hpre1 Hpre2]".
    iApply wp_fupd.
    wp_apply (wp_par (λ v, ∃.. (tt1' : TT1_2), ⌜v = tele_app (tele_app v1 tt1) tt1'⌝ ∗ tele_app (tele_app U1 tt1) tt1')%I
      (λ v, ∃.. (tt2' : TT2_2), ⌜v = tele_app (tele_app v2 tt2) tt2'⌝ ∗ tele_app (tele_app U2 tt2) tt2')%I with "[Hpre2][Hpre1]").
    - iStep. iIntros "!>". iExists _. iStep.
      iIntros "!> !>" (tt1') "HU".
      revert Hev1 => /(dep_eval_tele tt1) /(dep_eval_tele tt1') Hev1'. iStep.
      iExists _. iSteps.
    - iStep. iIntros "!>". iExists _. iStep.
      iIntros "!> !>" (tt2') "HU".
      revert Hev2 => /(dep_eval_tele tt2) /(dep_eval_tele tt2') Hev2'. iStep.
      iExists _. iSteps.
    - iSteps as (v1' v2') "HU1 HU2 HΦ".
      iDestruct "HU1" as (tt1') "[-> HU1]".
      iDestruct "HU2" as (tt2') "[-> HU2]".
      iSpecialize ("HΦ" $! (tele_pair_arg tt1' tt2')).
      rewrite !tele_curry_uncurry_relation.
      rewrite -!tele_uncurry_curry_compose.
      rewrite !tele_dep_appd_bind.
      rewrite -!dependent_fun_eq.
      rewrite -!tele_curry_uncurry_relation.
      rewrite !tele_app_bind.
      rewrite !tele_curry_uncurry_relation.
      rewrite -!tele_uncurry_curry_compose.
      rewrite !tele_dep_appd_bind.
      rewrite !tele_map_app.
      rewrite -!dependent_fun_eq.
      iApply fupd_idemp.
      rewrite -(wp_value_fupd NotStuck ⊤).
      iApply "HΦ". iFrame.
  Qed.

  Lemma fork_have_specs P e n TT1 TT2 e' v L U E s w :
    TCEq (tele_app (TT := [tele_pair coPset; stuckness]) (λ E' _, E') w) E →
    TCEq (tele_app (TT := [tele_pair coPset; stuckness]) (λ _ s, s) w) s →
    ReductionStep' wp_red_cond P n (fupd ⊤ ⊤) (fupd ⊤ ⊤) TT1 TT2 L U e e' [tele_arg3 ⊤; s] →
    (TC∀.. tt1 tt2, IntoVal (tele_app (tele_app e' tt1) tt2) (tele_app (tele_app v tt1) tt2)) →
    ReductionStep' wp_red_cond P 1 (fupd E E) (fupd E E) TT1 [tele] (tele_map bi_later L) (tele_bind (λ _, emp%I)) (Fork e) (tele_bind (λ _, of_val #())) w.
  Proof.
    drop_telescope w as E' s' => /= -> ->.
    intros He Hev.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iIntros "HP" (Φ) ">H".
    iDestruct "H" as (ttp) "[Hpre Hpost]".
    rewrite !tele_app_bind tele_map_app.
    iApply wp_fupd.
    iApply (wp_fork with "[Hpre HP]").
    - iSteps as "HL".
      iExists _. iFrame.
      iIntros "!> !> !>" (tt2) "HU". simpl.
      revert Hev => /(dep_eval_tele ttp) /(dep_eval_tele tt2) <-.
      iSteps.
    - iSteps as "HΦ".
      rewrite left_id.
      iMod "HΦ".
      by iApply wp_value_fupd.
  Qed.
  Global Existing Instance fork_have_specs.

End par.

Unset Universe Polymorphism.

Global Hint Extern 4 (PureExecNoRec _ _ ?e1 _) =>
  lazymatch e1 with
  | (App (Val ?v1) (Val ?v2)) =>
    assert_fails (assert (∃ f x erec, 
      TCAnd (AsRecV v1 f x erec) $
      TCAnd (TCIf (TCEq f BAnon) False TCTrue)
            (SolveSepSideCondition (is_recursive_fun (RecV f x erec) = true))) by (do 3 eexists; tc_solve));
    unfold PureExecNoRec; tc_solve
  | _ => unfold PureExecNoRec; tc_solve
  end : typeclass_instances.


(* Can be used with Diaframe's '--until' option.
FIXME: this tactic makes sense for other languages,
and we could include it in symb_exec/weakstepre.v.
But then we would need to Export that file, and not
sure if that's desirable. Exporting just the Tactic Notation
is currently broken (coq#18697)
*)
Tactic Notation "program-matches" open_constr(pat) :=
  lazymatch goal with
  | |- environments.envs_entails _ (wp _ _ ?e _) =>
    eunify e pat
  | |- environments.envs_entails _ (fupd _ _ (wp _ _ ?e _)) =>
    eunify e pat
  end.




















