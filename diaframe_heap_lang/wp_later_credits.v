From diaframe.symb_exec Require Import defs weakestpre.
From iris.program_logic Require Import weakestpre lifting.
From diaframe.lib Require Export later_credits.
From diaframe Require Import tele_utils.
From iris.proofmode Require Import proofmode.

Import bi.


(* This file overrides the default SPEC for pure execution to get later credits.
    Still very much WIP, awkward to use in combination with ReLoC. *)


From diaframe.heap_lang Require Import proof_automation.


Section wp_later_credits.
  Context `{!heapGS Σ}.

  Global Instance pure_wp_step_exec_inst_lc (e : expr) φ n e' E s:
    PureExecNoRec φ n e e' →
    SolveSepSideCondition φ →
    ReductionTemplateStep wp_red_cond (qprod TeleO TeleO) (ε₀)%I [tele_arg3 E; s] e 
      (λ pr, tele_app (TT := [tele]) (tele_app (TT := [tele]) e' $ qfst pr) $ qsnd pr)
      (template_M n (fupd E E) (fupd E E) TeleO TeleO ⌜φ⌝ (£ n))%I | 3.
  Proof.
    intros. refine (pure_wp_step_exec_lc_fupd _ _ _ _ _ _ _ _ _). exact H.
  Qed.

  Global Instance pure_wp_step_exec_inst_last_lc `(e : expr) φ n e' E s :
    ((∀ f x e, SolveSepSideCondition (is_recursive_fun (RecV f x e) = false) → 
                AsRecV (RecV f x e) f x e) → 
      PureExec φ n e e') →
    SolveSepSideCondition φ →
    ReductionTemplateStep wp_red_cond (qprod TeleO TeleO) (ε₁)%I [tele_arg3 E; s] e
      (λ pr, tele_app (TT := [tele]) (tele_app (TT := [tele]) e' $ qfst pr) $ qsnd pr)
      (template_M n (fupd E E) (fupd E E) TeleO TeleO ⌜φ⌝ (£ n))%I | 3.
  Proof.
    intros. refine (pure_wp_step_exec_lc_fupd _ _ _ _ _ _ _ _ _).
  Qed.
End wp_later_credits.
