From iris.heap_lang Require Export proofmode lib.nondet_bool.
From diaframe.heap_lang Require Import proof_automation.

(** The clairvoyant coin, taken from iris.lib. Used to test prophecy variable automation. *)

Definition new_coin: val :=
  λ: <>, (ref (nondet_bool #()), NewProph).

Definition read_coin : val := λ: "cp", !(Fst "cp").

Definition toss_coin : val :=
  λ: "cp",
  let: "c" := Fst "cp" in
  let: "p" := Snd "cp" in
  let: "r" := nondet_bool #() in
  "c" <- "r";; resolve_proph: "p" to: "r";; #().

Section proof.
  Context `{!heapGS Σ}.

  Definition prophecy_to_list_bool (vs : list (val * val)) : list bool :=
    (λ v, bool_decide (v = #true)) ∘ snd <$> vs.

  Definition coin (cp : val) (bs : list bool) : iProp Σ :=
    ∃ (c : loc) (p : proph_id) (vs : list (val * val)),
       proph p vs ∗
       ⌜cp = (#c, #p)%V⌝ ∗
       ∃ (b : bool) , c ↦ #b ∗ ⌜bs = b :: prophecy_to_list_bool vs⌝.

  Global Instance nondet_bool_spec :
    SPEC {{ True }} nondet_bool #() {{ (b : bool), RET #b; True }}.
  Proof. iStep. iApply nondet_bool_spec; iSteps. Qed.

  Lemma new_coin_spec : {{{ True }}} new_coin #() {{{ c bs, RET c; coin c bs }}}.
  Proof. iSteps. Qed.

  Lemma read_coin_spec cp bs :
    {{{ coin cp bs }}}
      read_coin cp
    {{{b bs', RET #b; ⌜bs = b :: bs'⌝ ∗ coin cp bs }}}.
  Proof. iSteps. Qed.

  Lemma toss_coin_spec cp bs :
    {{{ coin cp bs }}}
      toss_coin cp
    {{{b bs', RET #(); ⌜bs = b :: bs'⌝ ∗ coin cp bs' }}}.
  Proof. iSteps as (l p _ b vs Φ). destruct b; eauto. Qed.
End proof.
