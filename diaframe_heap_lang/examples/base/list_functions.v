From diaframe.heap_lang Require Import proof_automation wp_auto_lob.

Section list_functions.
  Context `{!heapGS Σ}.

  Fixpoint is_list (l: val) (ls : list Z) : iProp Σ :=
    match ls with
    | nil => ⌜l = InjLV #()⌝
    | x :: xs => ∃ (hd:loc) l', ⌜l = InjRV #hd⌝ ∗ hd ↦ (#x, l') ∗ is_list l' xs
    end%I.

  Definition list_inc : val := rec: "inc" "x" :=
    match: "x" with
      NONE => #()
    | SOME "x2" => let: "h" := Fst (! "x2") in let: "t" := Snd (! "x2") in 
                 "x2" <- (("h" + #1), "t") ;; "inc" "t"
    end.

  Global Instance match_node (nd : val) ls e1 e2 :
    SPEC [is_list nd ls]
     {{ True }}
       Case nd e1 e2
     {{ [▷^0] RET Case nd e1 e2; (⌜ls = []⌝ ∨ (∃ (x : Z) (xs : list Z), ⌜ls = x :: xs⌝)) ∗ is_list nd ls }} | 50.
  Proof. iSteps. Qed.

  Obligation Tactic := program_verify.

  Instance list_inc_correct (xs : list Z) (l : val) : 
    SPEC {{ is_list l xs }} 
      list_inc l 
    {{ RET #(); is_list l (map Z.succ xs) }}.
  Proof. iSteps. Qed.

  Definition list_append : val := rec: "append" "l" "l'" :=
    match: "l" with
      NONE => "l'"
    | SOME "x2" => let: "p" := ! "x2" in let: "r" := "append" (Snd "p") "l'" in 
                   "x2" <- (Fst "p", "r");; InjR "x2"
    end.

  Global Instance list_append_spec (xs ys: list Z) (l l': val) : 
    SPEC {{ is_list l xs ∗ is_list l' ys }}
      list_append l l' 
    {{ (l_res : val), RET l_res; is_list l_res (xs ++ ys) }}.
  Proof. iSteps. Qed.

  Definition list_append_pre : val := rec: "f" "h" "p" "l'" :=
    match: "h" with
      NONE => "p" <- (Fst (! "p"), "l'")
    | SOME "x2" => "f" (Snd (! "x2")) "x2" "l'"
    end.

  Definition list_append2 : val := λ: "l" "l'",
    match: "l" with
      NONE => "l'"
    | SOME "x2" => list_append_pre (Snd (! "x2")) "x2" "l'" ;; "l"
    end.

  Global Instance list_append_pre_step (l' h : val) (p : loc) (xs ys : list Z) (v : val) :
    SPEC {{ is_list h xs ∗ is_list l' ys ∗ p ↦ (v, h) }}
       list_append_pre h #p l'
     {{ RET #(); ∃ h0 : val, p ↦ (v, h0) ∗ is_list h0 (xs ++ ys) }}.
  Proof. iSteps. Qed.

  Global Instance list_append2_correct (xs ys: list Z) (l l': val) : 
    SPEC {{ is_list l xs ∗ is_list l' ys }} 
      list_append2 l l' 
    {{ (l_res : val), RET l_res; is_list l_res (xs ++ ys) }}.
  Proof. iSteps. Qed.

  Definition list_count : val := rec: "f" "l" "n" :=
    match: "l" with
      NONE => "n"
    | SOME "x2" => "f" (Snd (! "x2")) ("n" + #1)
    end.

  Global Instance list_count_spec (xs : list Z) (l : val) (n : Z) :
    SPEC {{ is_list l xs }}
      list_count l #n 
    {{ (z : Z), RET #z; ⌜(n + List.length xs)%Z = z⌝ ∗ is_list l xs }}.
  Proof. iSteps. Qed.

  Definition list_length : val := 
    λ: "l", list_count "l" #0.

  Global Instance list_length_correct (xs: list Z) (l : val) :
    SPEC {{ is_list l xs }}
      list_length l 
    {{ (z : Z), RET #z; ⌜z = List.length xs⌝ ∗ is_list l xs }}.
  Proof. iSteps. Qed.

  Definition list_length_direct : val :=
    λ: "l",
      let: "count" := rec: "countf" "l" "n" :=
        match: "l" with
          NONE => "n"
        | SOME "x2" => "countf" (Snd (! "x2")) ("n" + #1)
        end in
      "count" "l" #0.

  Global Instance list_length_direct_correct (xs: list Z) (l : val) :
    SPEC {{ is_list l xs }}
      list_length_direct l 
    {{ (z : Z), RET #z; ⌜z = List.length xs⌝ ∗ is_list l xs }}.
  Proof.
    iSteps. (* does not work since it is unclear how to generalize over #0 *)
  Abort.

End list_functions.



