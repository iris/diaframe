From diaframe.heap_lang Require Import proof_automation.

Definition swap: val := λ: "x" "y",
  let: "tmp" := ! "x" in
  "x" <- ! "y";;
  "y" <- "tmp".

Section proof.
  Context `{!heapGS Σ}.

  Lemma swap_spec x y v1 v2 :
    {{{ x ↦ v1 ∗ y ↦ v2 }}}
      swap #x #y
    {{{ RET #(); x ↦ v2 ∗ y ↦ v1 }}}.
  Proof. iSteps. Qed.
End proof.