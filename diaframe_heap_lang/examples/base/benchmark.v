From diaframe.heap_lang Require Import proof_automation.

From iris.algebra Require Import lib.frac_auth numbers. 
From iris.heap_lang Require Import proofmode.
From iris.heap_lang Require Import lib.par lib.counter lib.lock.

Import bi.

From diaframe.lib Require Import own_hints.


Section lock_increment.
  Context (L : lock).
  Context `{!heapGS Σ, !spawnG Σ, !ccounterG Σ, !lockG Σ}.
  Local Existing Instance ccounter_inG.

  Definition parallel_add : val := λ: "s",
    let: "l" := ref "s" in
    let: "lk" := newlock #() in
    ((acquire "lk";; "l" <- #1 + ! "l" ;; release "lk") ||| 
     (acquire "lk";; "l" <- #1 + ! "l" ;; release "lk")) ;;
    acquire "lk" ;;
    ! "l".

  Global Opaque par.

  Proposition parallel_add_spec (s : nat) :
    {{{ True }}} parallel_add #s {{{ RET #(2 + s); True }}}.
  Proof.
    iSteps as (Φ l) "HΦ Hl".
    iAssert (|==> ∃ γ, (∃ (m : nat), l ↦ #(m + s) ∗ own γ (●F m)) ∗ own γ (◯F 0))%I with "[Hl]" 
      as ">[%γ [HR Hγ']]"; first iSteps. iIntros "!>".
    wp_apply (newlock_spec (∃ m : nat, l ↦ #(m + s) ∗ own γ (●F m)) with "HR").
    iSteps as (lk γlk) "HL".
    iDestruct "Hγ'" as "[Hγ1 Hγ2]". iIntros "!>".
    wp_apply (wp_par (λ _, own γ (◯F{1/2} 1)) (λ _, own γ (◯F{1/2} 1)) with "[Hγ1] [Hγ2]");
    iSteps.
  Qed.

  Proposition test (z1 z2 : Z) : {{{ True }}} if: #z1 ≠ #z2 then #() else #() {{{ RET #(); True }}}.
  Proof. iSteps. Qed.

  Definition parallel_add_fg : val := λ: "s",
    let: "l" := ref "s" in
    ((FAA "l" #1) ||| 
     (FAA "l" #1)) ;;
    ! "l".

  Proposition parallel_add_fg_spec (s : nat) :
    {{{ True }}} parallel_add_fg #s {{{ RET #(2%nat + s); True }}}.
  Proof.
    iSteps --until program-matches (par (λ: <>, FAA _ _)%V (λ: <>, _)%V;; _)%E /
          as (Φ l) "HΦ Hl". (* par will see that it knows a spec for FAA, and shortcircuit. We need to prevent that *)
    pose proof (nroot .@ "N") as N.
    iAssert (|={⊤}=> ∃ γ, inv N (∃ m : nat, l ↦ #(m + s) ∗ own γ (●F m)) ∗ own γ (◯F 0))%I with "[Hl]"
      as ">[%γ [#HN Hγ']]"; first iSteps.
    iDestruct "Hγ'" as "[Hγ1 Hγ2]". iIntros "!>".
    wp_apply (wp_par (λ _, own γ (◯F{1/2} 1)) (λ _, own γ (◯F{1/2} 1)) with "[Hγ1] [Hγ2]");
    iSteps.
  Qed.

  Definition parallel_store : val := λ: <>,
    let: "l" := ref #5 in
    (("l" <- #3) ||| ("l" <- #7));;
    ! "l".

  Proposition parallel_store_spec : {{{ True }}} parallel_store #() {{{ (n : Z), RET #n; True }}}.
  Proof.
    iSteps --until goal-matches (|={_}=> WP par (λ: <>, _)%V (λ: <>, _)%V ;; _ {{ _ }})%I /
        as (Φ l) "HΦ Hl".
    pose proof (nroot .@ "N") as N.
    iAssert (|={⊤}=> inv N (∃ n : Z, l ↦ #n))%I with "[Hl]" as ">#HN"; first iSteps.
    iIntros "!>". wp_apply (wp_par (λ _, True) (λ _, True))%I;
    iSteps.
  Qed.

  Proposition free_inv_loop P : {{{ P }}} let: "l" := ref #2 in Free "l" {{{ RET #(); P }}}.
  Proof.
    iStep 8 as (Φ l) "HP HΦ Hl".
    iMod (inv_alloc nroot _ (l ↦ #2 ∗ P) with "[HP Hl]") as "#HN". { iSteps. }
    iSteps. (* this tests that this terminates *)
  Abort.

End lock_increment.


From iris.algebra Require Import csum agree excl.

Definition stateR := csumR (exclR unitO) (agreeR unitO).
Class stateG Σ := { #[local] state_inG :: inG Σ stateR}.
Definition stateΣ : gFunctors := #[GFunctor stateR].
Local Instance sub_stateΣ Σ : subG stateΣ Σ → stateG Σ.
Proof. solve_inG. Qed.

Definition Start : stateR := Cinl (Excl ()).
Definition Finish : stateR := Cinr (to_agree ()).

Class fracG Σ := { #[local] frac_inG :: inG Σ fracR }.
Definition fracΣ : gFunctors := #[GFunctor fracR].
Local Instance subG_fracΣ {Σ} : subG fracΣ Σ → fracG Σ.
Proof. solve_inG. Qed.


Section stateG_automation.
  Context `{!stateG Σ}.

  Global Instance biabd_start_finish γ :
    HINT own γ Start ✱ [- ; emp] ⊫ [bupd]; own γ Finish ✱ [own γ Finish].
  Proof.
    iStep as "H".
    rewrite own_update; last by apply (cmra_update_exclusive Finish).
    (* TODO: iStep does not solve this, due to the different sum_inr_subtract_n/s instances *)
    iMod "H" as "#H".
    iSteps.
  Qed.

  Global Instance finish_dup_inst γ : Persistent (own γ Finish).
  Proof.
    apply own_core_persistent.
    rewrite /CoreId. done.
  Qed.

  Global Instance state_valid_op_both_finish :
    IsValidOp Finish Finish Finish Σ True%I.
  Proof. split; eauto. iSteps. rewrite /Finish -Cinr_op agree_idemp //. Qed.
  Global Instance state_valid_op_left_start (s : stateR) :
    IsValidOp (Start ⋅ s) Start s Σ False%I.
  Proof. split; destruct s; eauto. Qed.
  Global Instance state_valid_op_right_start (s : stateR) :
    IsValidOp (s ⋅ Start) s Start Σ False%I.
  Proof. split; destruct s; eauto. Qed.
End stateG_automation.


Definition slow_incr : val := 
  λ: "l",
    "l" <- ! "l" + # 1.

Definition slow_incr_par : val :=
  λ: "l",
    (slow_incr "l" ||| slow_incr "l");;
    ! "l".


Section exercises.
  Context `{!heapGS Σ, !stateG Σ, !fracG Σ, !spawnG Σ}.

  Let N := nroot.

  Definition double_incr_inv (γ1 γ2 : gname) (l : loc) (n : Z) : iProp Σ :=
    ∃ m : Z, l ↦ # m ∗ ((⌜m = n⌝ ∗ own γ1 Start)
                        ∨ (⌜m = (n + 1)%Z⌝ ∗ own γ1 Finish ∗ own γ2 (1/2)%Qp)
                        ∨ (⌜m = (n + 2)%Z⌝ ∗ own γ1 Finish ∗ own γ2 1%Qp)).

  Instance slow_incr_spec (n : Z) (l : loc) :
    SPEC γ1 γ2, {{ inv N (double_incr_inv γ1 γ2 l n) ∗ own γ2 (1/2)%Qp }}
      slow_incr #l
    {{ RET #(); own γ1 Finish }}.
  Proof. iSteps. Qed.

  Instance slow_incr_par_spec (n : Z) (l : loc) :
    SPEC {{ l ↦ #n }}
      slow_incr_par #l
    {{ (z : Z), RET #z; ⌜z = (n + 1)%Z⌝ ∨ ⌜z = (n + 2)%Z⌝ }}.
  Proof.
    iSteps as (γ1).
    (* allocation is manual since γ2 is not mentioned in the first case of invariant.
       could be fixed with a (1/3) mention. but alright *)
    iMod (own_alloc 1%Qp) as (γ2) "Hγ2"; first done.
    iExists γ2. iSteps.
  Qed.
End exercises.

Section exercises_old.
  Context `{!heapGS Σ, !stateG Σ, !fracG Σ, !spawnG Σ}.

  Let N := nroot.

  Definition double_incr_inv_old (γ1 γ2 : gname) (l : loc) (n : Z) : iProp Σ :=
    (l ↦ #n ∗ own γ1 Start) ∨
    (l ↦ #(n + 1) ∗ own γ1 Finish ∗ own γ2 (1/2)%Qp) ∨
    (l ↦ #(n + 2) ∗ own γ1 Finish ∗ own γ2 1%Qp).

  Instance slow_incr_spec_old_inv (n : Z) (l : loc) :
    SPEC γ1 γ2, {{ inv N (double_incr_inv_old γ1 γ2 l n) ∗ own γ2 (1/2)%Qp }}
      slow_incr #l
    {{ RET #(); own γ1 Finish }}.
  Proof. iSteps. Qed.

  Instance slow_incr_par_spec2 (n : Z) (l : loc) :
    SPEC {{ l ↦ #n }}
      slow_incr_par #l
    {{ (z : Z), RET #z; ⌜z = (n + 1)%Z⌝ ∨ ⌜z = (n + 2)%Z⌝ }}.
  Proof.
    iSteps as (γ1).
    (* allocation is manual since γ2 is not mentioned in the first case of invariant.
       could be fixed with a (1/3) mention. but alright *)
    iMod (own_alloc 1%Qp) as (γ2) "Hγ2"; first done.
    iExists γ2. iSteps.
  Qed.
End exercises_old.


































