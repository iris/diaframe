From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes tele_utils solve_defs.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file contains all base (bi)abduction hints, needed for minimal functionality of Diaframe.
*)

Unset Universe Polymorphism.

Section biabd_instances.
  Context {PROP : bi}.

  Implicit Types P : PROP.
  Implicit Types M : PROP → PROP.

  Global Instance bi_abd_from_pers_assumption P :
    HINT □⟨true⟩ P ✱ [- ; emp] ⊫ [id]; P ✱ [emp] | 49.
  Proof. rewrite /BiAbd /=. by rewrite bi.intuitionistically_elim. Qed.

  Global Instance bi_abd_from_spat_assumption P P' :
    (* this requirement will remember newly proven persistent things *)
    TCIf (Persistent P) (TCEq P' (<affine> P)%I) (TCEq P' emp%I) →
    HINT P ✱ [- ; emp] ⊫ [id]; P ✱ [P'] | 50.
  Proof.
    rewrite /BiAbd /= !right_id //.
    case => [HP -> /= | ->].
    - rewrite -{1}(idemp bi_and P).
      by apply persistent_and_affinely_sep_r_1.
    - by rewrite right_id.
  Qed.

  Global Instance bi_abd_from_assumption_intuit P :
    HINT □⟨true⟩ P ✱ [- ; emp] ⊫ [id]; □ P ✱ [emp].
  Proof. by rewrite /BiAbd /= !right_id. Qed.

  Global Instance bi_abd_empty_goal_from_emp :
    HINT emp ✱ [- ; emp] ⊫ [@id PROP]; χ ✱ [emp] | 50.
  Proof. by rewrite /BiAbd /= empty_goal_eq. Qed.

  (* Whether this is hint is definitely required is debatable. but it is useful *)
  Lemma normalize_disj (TT : tele) (P P1 P2 P' : TT -t> PROP) teq :
    (TC∀.. (tt : TT), FromOr (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    SimplTeleEq TT teq →
    (TC∀.. (tt : TT), NormalizedProp (tele_app P tt) (tele_app P' tt) true) →
    BiAbd (TTl := TT) (TTr := TT) false (ε₀)%I P id P' (tele_map (tele_map (bi_pure)) (teq))%I. (* | 40.*)
  Proof.
    rewrite /NormalizedProp /NormalizeProp /BiAbd /SimplTeleEq /= empty_hyp_first_eq => _ /tforall_forall Hteq /tforall_forall HP.
    apply tforall_forall => ttl.
    rewrite left_id.
    iIntros "HP".
    iExists (ttl).
    rewrite HP !tele_map_app.
    iFrame; iPureIntro.
    specialize (Hteq ttl).
    apply (dep_eval_tele ttl) in Hteq.
    apply Hteq.
    apply tele_eq_app_both.
  Qed.

  Global Instance abd_if_bool_decide `{Decision φ} (L R : PROP) :
    (* cannot be a biabd, since the forall would not be introducable! *)
    HINT1 ε₀ ✱ [∀ (b : bool), (⌜b = true⌝ ∗ ⌜φ⌝ ∨ ⌜b = false⌝ ∗ ⌜¬φ⌝ )-∗ if b then L else R] 
        ⊫ [id]; if bool_decide φ then L else R.
  Proof.
    rewrite /Abduct /=.
    case_bool_decide.
    - rewrite (forall_elim true) empty_hyp_first_eq left_id.
      iIntros "H".
      iApply "H". by iLeft.
    - rewrite (forall_elim false) empty_hyp_first_eq left_id.
      iIntros "H".
      iApply "H". by iRight.
  Qed.

  Global Instance biabd_emp_valid {TTl TTr} p P Q M R S :
    AsEmpValid (BiAbd (TTl := TTl) (TTr := TTr) p P Q M R S) (∀.. (ttl : TTl), □?p P ∗ (tele_app R ttl) -∗ M $ ∃.. (ttr : TTr), tele_app Q ttr ∗ tele_app (tele_app S ttl) ttr).
  Proof.
    split.
    - rewrite /BiAbd /= => /tforall_forall HPQ.
      iIntros (ttl) "HPR". rewrite HPQ //.
    - rewrite /BiAbd tforall_forall /= => HPQ ttl.
      iApply HPQ.
  Qed.
End biabd_instances.

Section abduction_instances.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma abd_from_biabd {TTl TTr : tele} p P Q M R S :
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M R S →
    ModalityMono M →
    (TC∀.. (ttr : TTr), TCOr (∀.. ttl : TTl, Affine (tele_app (tele_app S ttl) ttr)) (Absorbing (tele_app Q ttr))) →
    Abduct (TT := TTr) p P Q M (∃.. ttl, tele_app R ttl)%I. 
    (* priority of this instance has a significant performance impact! I think low = better *)
  Proof.
    rewrite /BiAbd /Abduct => /tforall_forall HPR HM /tforall_forall Hdrop.
    apply wand_elim_r', bi_texist_elim => ttl.
    apply wand_intro_l.
    rewrite HPR.
    apply HM, bi_texist_mono => ttr.
    case: (Hdrop ttr) => [/(dep_eval_tele ttl) HS | HQ ].
    - by rewrite HS right_id.
    - by apply sep_elim_l, _.
  Qed.

  Lemma abd_from_biabd2 {TTl TTr : tele} p P Q M R S :
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M R S →
    ModalityStrongMono M →
    Abduct (TT := TTr) p P Q M (∃.. ttl, tele_app R ttl ∗ ∀.. ttr, tele_app (tele_app S ttl) ttr -∗ emp)%I. 
    (* so this looks better, but 'just' emp will be useless, in practice, we will want a modality before emp. *)
  Proof.
    rewrite /BiAbd /Abduct => /tforall_forall HPR HM.
    apply wand_elim_r', bi_texist_elim => ttl.
    apply wand_intro_l.
    rewrite assoc HPR modality_strong_frame_l.
    apply HM, wand_elim_l', bi_texist_elim => ttr.
    apply wand_intro_r. rewrite -assoc (bi_tforall_elim ttr) wand_elim_r right_id bi_texist_exist -(exist_intro _) //.
  Qed.

  Global Instance abduct_from_assumption (P : PROP) :
    HINT1 P ✱ [ emp ] ⊫ [id]; P | 50.
  Proof. by rewrite /Abduct /= right_id. Qed.

  Global Instance abduct_emp_valid {TT} p P Q M R :
    AsEmpValid (Abduct (TT := TT) p P Q M R) (□?p P ∗ R -∗ M $ ∃.. (tt : TT), tele_app Q tt)%I.
  Proof.
    split; rewrite /Abduct /=.
    - move => -> //. iIntros "$".
    - move => HPQ. iApply HPQ.
  Qed.
End abduction_instances.

Section disj_instances.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Global Instance biabd_disj_from_biabd {TTl TTr : tele} p P Q M R S :
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M R S →
    ModalityMono M →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M R S (tele_bind (λ _, None2)).
  Proof.
    rewrite /BiAbdDisj /BiAbd /= !tforall_forall => + HM ttl.
    move => ->. rewrite tele_app_bind.
    apply HM. by rewrite right_id.
  Qed.

  Lemma abd_from_biabd_disj {TTl TTr : tele} p P Q M R S D R' :
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M R S D →
    ModalityStrongMono M →
    TCIf (TC∀.. (ttl : TTl),  TCEq (tele_app D ttl) None2) 
      (TCAnd (TCEq R' $ ∃.. ttl, tele_app R ttl)%I 
             (TC∀.. (ttr : TTr), TCOr (∀.. ttl : TTl, Affine (tele_app (tele_app S ttl) ttr)) (Absorbing (tele_app Q ttr))))
      (TCEq R' $ ∃.. ttl, tele_app R ttl ∗ (((∀.. ttr : TTr, tele_app (tele_app S ttl) ttr -∗ emp)) ∧ (match tele_app D ttl with None2 => False | Some2 D' => D' end -∗ ∃.. ttr, tele_app Q ttr)))%I →
    Abduct (TT := TTr) p P Q M R'.
  Proof.
    rewrite /BiAbdDisj /Abduct /TCTForall /= !tforall_forall => HPQ HM.
    case => [/tforall_forall HD [-> /tforall_forall Hdrop] | ->];
    apply wand_elim_r', bi_texist_elim => ttl;
    apply wand_intro_l; [|rewrite assoc]; rewrite HPQ; [ | rewrite modality_strong_frame_l]; apply HM; 
    [ | apply wand_elim_l']; apply or_elim.
    - apply bi_texist_mono => ttr.
      case: (Hdrop ttr) => [/(dep_eval_tele ttl) HS | HQ ].
      * by rewrite HS right_id.
      * by apply sep_elim_l, _.
    - rewrite HD. apply pure_elim' => [[]].
    - rewrite and_elim_l. apply bi_texist_elim => ttr.
      rewrite (bi_tforall_elim ttr). apply bi.wand_intro_r.
      rewrite -assoc wand_elim_r right_id.
      rewrite bi_texist_exist. apply exist_intro.
    - destruct (tele_app D ttl) as [D'| ]; last first.
      { apply pure_elim' => [[]]. }
      rewrite and_elim_r. apply bi.wand_intro_r.
      by rewrite wand_elim_r.
  Qed.

  Global Instance biabd_disj_pure_decidable φ :
    IsEvarFree φ → (* if we dont guard against this, this hint might be applied under binders, which is usually a bad idea *)
        (* for example: suppose our goal is ⌜∃ x, v = InjRV x⌝. Then SolveSepFoc will look for hints for
           λ v, ⌜v = InjRV x⌝. This is decidable for each x, even if an evar, but the case distinction is useless.
          what we'd want to do instead, is `decide (∃ x, v = InjRV x)`. Maybe that is possible? See below.. *)
    Decision φ → (* if we know we can determine the truth of φ *)
    TCIf (SolveSepSideCondition (¬ φ)) False TCTrue → (* and we do not know it is currently false *)
    TCIf (SolveSepSideCondition (φ)) False TCTrue →   (*             nor that it is true *)
    BiAbdDisj (PROP := PROP) (TTl := [tele]) (TTr := [tele]) false (ε₀)%I ⌜φ⌝%I id emp%I ⌜φ⌝%I (Some2 $ ⌜¬φ⌝)%I | 20.
  Proof.
    rewrite /BiAbdDisj /= /Decision => _ [Hφ|Hnφ] _ _.
    - rewrite -or_intro_l empty_hyp_first_eq. rewrite -bi.pure_intro //.
    - rewrite -or_intro_r. rewrite -bi.pure_intro //.
  Qed.

  Lemma biabd_disj_pure_decidable_exist (TTr : tele) P φ :
    (TC∀.. (tt : TTr), TCEq (tele_app P tt) (⌜tele_app φ tt⌝%I)) →
    IsEvarFree φ → (* if we dont guard against this, this hint might be applied under binders, which is usually a bad idea *)
        (* This lemma might be useful if our goal is ⌜∃ x, v = InjRV x⌝. Not sure if Decision can be used, 
           might not simplify or no instances might be found. *)
    (∃.. x, tele_app φ x) ∨ (∀.. x, ¬ tele_app φ x)  → (* if we know we can determine the truth of φ *)
    TCIf (SolveSepSideCondition (∀.. x, ¬ tele_app φ x)) False TCTrue → (* and we do not know it is currently false *)
    TCIf (SolveSepSideCondition (∃.. x, tele_app φ x)) False TCTrue →   (*             nor that it is true *)
    BiAbdDisj (PROP := PROP) (TTl := [tele]) (TTr := TTr) false (ε₀)%I P id emp%I P (Some2 $ ⌜∀.. x, ¬ tele_app φ x⌝)%I.
    (* TODO: presence of this hint would make verification of ARC with free automatic whatever the order of disjuncts! *)
  Proof.
    rewrite /BiAbdDisj /TCTForall /= => /tforall_forall HP _ [/texist_exist [tt Hφ]|Hnφ] _ _.
    - rewrite -or_intro_l empty_hyp_first_eq. by rewrite bi_texist_exist -(exist_intro tt) HP -bi.pure_intro.
    - rewrite -or_intro_r. by rewrite -bi.pure_intro.
  Qed.
End disj_instances.

Unset Universe Polymorphism.









































