From iris.proofmode Require Import base coq_tactics reduction tactics classes_make.
From diaframe Require Import util_classes tele_utils solve_defs.
From iris.bi Require Import bi telescopes.

Import bi.

Unset Universe Polymorphism.

Section lemmas_disj.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.
  Implicit Types P : PROP.

  Lemma biabd_disj_goal_exist {A : Type} {C : tele} {TTf : A → tele} TTl (g : C -t> A) S T T' P (Q : TeleS TTf -t> PROP) p M D :
    (* the telescope C should be seen as the collection of all unsolved variables/evars *)
    (∀ (c : C), BiAbdDisj (TTl := tele_app TTl c) (TTr := TTf $ tele_app g c) p P (Q $ tele_app g c) M (tele_appd_dep (tele_uncurry S) c) (T c) (D c)) →
    (∀.. (c : C) (ttl : tele_app TTl c), 
      TransportTeleProp (tele_app g c) (tele_app (T c) ttl) (tele_app (tele_appd_dep (tele_uncurry T') c) ttl)
    ) →
    ModalityMono M →
    BiAbdDisj (TTr := TeleS TTf) (TTl := TeleConcatType C TTl) p P Q M S T' (tele_curry $ tele_dep_bind D).
  Proof.
    rewrite /BiAbdDisj => HPQS HT HM.
    apply tforall_forall => ml.
    rewrite (tele_arg_concat_inv_eq ml).
    destruct (tele_arg_concat_inv_strong_hd ml) as [tt1 tt2].
    rewrite tele_curry_uncurry_relation.
    revert HPQS => /(dep_eval tt1) /(dep_eval_tele tt2) ->.
    apply HM, or_mono; last first.
    { rewrite tele_curry_uncurry_relation
         -tele_uncurry_curry_compose tele_dep_appd_bind //. }
    apply bi_texist_elim => tt.
    rewrite bi_texist_exist -(bi.exist_intro $ TargS (tele_app g tt1) tt) /=.
    apply bi.sep_mono_r.
    rewrite tele_curry_uncurry_relation.

    revert HT => /(dep_eval_tele tt1) /(dep_eval_tele tt2) ->.
    rewrite -(bi.exist_intro eq_refl) => //=.
  Qed.

  Lemma biabd_disj_hyp_exist p P `(R : A → PROP) TTl TT2 Q S T D M (D' : option2 PROP) :
    IntoExistCareful2 P R →
    (∀ (a : A),
      BiAbdDisj (TTr := TT2) (TTl := TTl a) p (R a) Q M (S a) (T a) (D a)
    ) →
    (* we could add something like:
    [(∀ (a : A), ∀.. (ttl : TTl a), tele_app (S a) ttl ⊢ <pers> tele_app (Spers a) ttl) →]
    to obtain [□ tele_app (Spers a) ttl] in T *)
    TCIf (∀ (a : A), ∀.. (ttl : TTl a),  (TCEq (tele_app (D a) ttl) None2)) (TCEq D' None2)
        (TCEq D' (Some2 $ ∃ a, ∃.. (ttl : TTl a), match tele_app (D a) ttl with Some2 D'' => D'' | None2 => False end)%I) →
    ModalityStrongMono M →
    BiAbdDisj (TTr := TT2) (TTl := [tele]) p P Q M (∀ a, ∃.. (ttl : TTl a), tele_app (S a) ttl)%I
        (tele_bind (λ tt2, ∃ a, ∃.. (ttl : TTl a), tele_app (tele_app (T a) ttl) tt2))%I
        D'.
  Proof.
    rewrite /IntoExistCareful2 /IntoExistCareful /BiAbdDisj /= => -> HRS HD HM.
    rewrite intuitionistically_if_exist.
    apply bi.wand_elim_l', bi.exist_elim => a.
    apply bi.wand_intro_r.
    rewrite (bi.forall_elim a).
    apply bi.wand_elim_r', bi_texist_elim => ttl.
    apply bi.wand_intro_l. revert HRS => /(dep_eval a) /(dep_eval_tele ttl) ->.
    apply HM, or_mono; last first.
    { case: HD => [ | ->].
      - move => /(dep_eval a) /(dep_eval_tele ttl) -> -> //.
      - rewrite -(bi.exist_intro a) bi_texist_exist -(bi.exist_intro ttl) //. }
    apply bi_texist_mono => ttr.
    apply bi.sep_mono_r.
    rewrite tele_app_bind.
    rewrite -(bi.exist_intro a) bi_texist_exist -(bi.exist_intro ttl) //.
  Qed.

  Lemma biabd_disj_hyp_forall {A : Type} {C : tele} {TTr : tele} TTl (g : C -t> A) S T P (P' : A → PROP) (Q : TTr -t> PROP) p M D :
    (* the telescope C should be seen as the collection of all unsolved variables/evars *)
    IntoForallCareful P P' →
    (∀ (c : C), 
      BiAbdDisj (TTl := tele_app TTl c) (TTr := TTr) p (P' $ tele_app g c) Q M 
        (tele_appd_dep (tele_uncurry S) c) 
        (tele_appd_dep (tele_uncurry T) c)
        (tele_appd_dep (tele_uncurry D) c)
    ) →
    ModalityMono M →
    BiAbdDisj (TTr := TTr) (TTl := TeleConcatType C TTl) p P Q M S T D.
  Proof.
    rewrite /BiAbdDisj /IntoForallCareful => HPP HPQS HM.
    apply tforall_forall => ml.
    rewrite (tele_arg_concat_inv_eq ml).
    destruct (tele_arg_concat_inv_strong_hd ml) as [tt1 tt2].
    rewrite tele_curry_uncurry_relation HPP (bi.forall_elim $ tele_app g tt1).
    revert HPQS => /(dep_eval tt1) /(dep_eval_tele tt2) ->.
    apply HM, or_mono; last first.
    { rewrite tele_curry_uncurry_relation //. }
    apply bi_texist_mono => tt.
    apply bi.sep_mono_r.
    rewrite tele_curry_uncurry_relation //.
  Qed.

  Lemma biabd_disj_hyp_wand p P R V {TTl} {TTr} Q M S T D :
    IntoWand2 p P R V →
    BiAbdDisj (TTl := TTl) (TTr := TTr) false V Q M S T D →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M (tele_map (flip bi_sep R) S) T D.
  Proof.
    rewrite /BiAbdDisj /IntoWand2 !tforall_forall => /= HPRV + ttl.
    by rewrite HPRV tele_map_app /= sep_comm -sep_assoc wand_elim_r sep_comm => ->.
  Qed.

  Lemma biabd_disj_goal_later_intro p P {TTr TTm} Q M Q' S1 T D n :
    (TC∀.. tt, FromLaterNMax (tele_app Q tt) n (tele_app Q' tt)) →
    BiAbdDisj (TTl := TTm) (TTr := TTr) p P Q' M S1 T D →
    ModalityMono M →
    BiAbdDisj (TTl := TTm) (TTr := TTr) p P Q M S1 T D.
  Proof.
    rewrite /TCTForall /BiAbdDisj /FromLaterNMax !tforall_forall => HQ + HM ttm.
    move => ->.
    apply HM, or_mono => //.
    apply bi_texist_mono => ttr.
    apply sep_mono_l.
    rewrite -HQ. apply bi.laterN_intro.
  Qed.

  Lemma biabd_disj_hyp_mod p P M1 P' {TTl TTr} Q S T D M M' :
    IntoModal p P M1 P' →
    ModalityStrongMono M1 →
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P' Q M S T D →
    ModalityCompat3 M' M1 M → (* this (hopefully) normalizes M, so that we don't have M1 ∘ M2 *)
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M' S T D.
  Proof.
    rewrite /IntoModal /BiAbdDisj /= !tforall_forall => + HM1 HPS HM1' ttl.
    move => ->. rewrite modality_strong_frame_l -HM1'. apply HM1.
    rewrite HPS //.
  Qed.

  Lemma biabd_disj_hyp_sepl P P1 P2 {TTl TTr} Q M S T D :
    IntoSepCareful P P1 P2 →
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P1 Q M S T D →
    ModalityStrongMono M →
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P Q M S (tele_map (tele_map (bi_sep P2)) T) 
      (tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 (D' ∗ P2) end)%I).
  Proof.
    rewrite /BiAbdDisj /IntoSepCareful !tforall_forall /= => HP + HM ttl.
    rewrite HP sep_comm sep_assoc (sep_comm (tele_app S _)) => ->.
    rewrite modality_strong_frame_l.
    apply HM.
    iDestruct 1 as "[[[%tt [HQ HT]]|HD] HP]"; last first.
    { iRight. rewrite tele_app_bind. destruct (tele_app D ttl) as [D'|]; first iFrame. iDestruct "HD" as "[]". }
    iLeft. iExists tt.
    rewrite !tele_map_app.
    by iFrame.
  Qed.

  Lemma biabd_disj_hyp_sepr P P1 P2 {TTl TTr} Q M S T D :
    IntoSepCareful P P1 P2 →
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P2 Q M S T D →
    ModalityStrongMono M →
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P Q M S (tele_map (tele_map (bi_sep P1)) T) 
      (tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 (D' ∗ P1) end)%I).
  Proof.
    intros. eapply biabd_disj_hyp_sepl => //.
    revert H; rewrite /IntoSepCareful => ->.
    by rewrite comm.
  Qed.

  (* ofcourse, this trick with the conditional sideconditions can always be done.
     Consider, for example, doing it to separating conjunction ∗. The problem is that,
     when the sidecondition S can indeed not be proven, you just receive back the same (P1 ∗ P2)
     and so the hint search will find and apply the same thing again.
     The situation is different for 'regular' conjunction ∧. Usually used as 'internal choice',
     it makes sense to try if we can prove the sideconditions of the left-hand side. If we can't,
     then (since we tried, and it didn't work), we should try if we can make it work if we 'choose' differently,
     and use it as P2 *)
(* Allowing bi.Logic < bi.Quant breaks this for some obscure reason.
  We do not actually use this, so skip for now 
  Lemma biabd_disj_hyp_andl P1 P2 {TTl TTr} Q M S T D M' tteq :
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P1 Q M S T D →
    ModalityStrongMono M →
    CoIntroducable M M' →
    SimplTeleEq TTl tteq →
    BiAbdDisj (TTl := TeleS (λ b : bool, if b then TTl else TeleO)) (TTr := TTr) false (P1 ∧ P2)%I Q M 
      (λ (b : bool),
          tele_bind (λ tif : if b then TTl else TeleO, (∃.. (ttl : TTl), tele_app S ttl ∗ ⌜b = true⌝ ∧ match b, tif return PROP with | false, _ => False%I | true, ttl' => ⌜tele_app (tele_app tteq ttl) ttl'⌝ end) ∨ ⌜b = false⌝))%I 
      (λ (b : bool), 
            match b return (if b then TTl else TeleO) -t> TTr -t> PROP  with
            | true => tele_map (tele_map (flip bi_sep True)) T
            | false => tele_bind (λ _ : TTr, False) 
            end)%I
      (λ (b : bool), 
            match b return (if b then TTl else TeleO) -t> option2 PROP  with
            | true => tele_map (λ op, match op with |Some2 D' => Some2 (D' ∗ True) | _ => None2 end) D
            | false => Some2 (P2 ∗ M' True)
            end)%I.
  Proof. (* However, this cannot currently (always) work for two reasons: 
            1. Diaframe relies on telescopes that are constructors
            2. CoIntroducable is not true in the relevant cases, like closing atomic updates
            3. Diaframe also (implicitly) relies on non-dependent telescopes

           These issues could possibly be resolved as follows:
            - introduce a separate datastructure tele_cond b TTl TTr, so that
              TeleS (λ b : bool, if b then TTl else TeleO) is isomorphic to 
              TeleS (λ b : bool, TeleS (λ _ : tele_cond b TTl TTl, TeleO)). 
              This fixes 1. We then need to improve telescope decomposition code to fix 3.
              Additionally, we need some new magic to be able to compare elements 
              arg : tele_cond true TTl TTr   with bare   arg' : TTl  
            - Additionally, we need to handle 2. For this, we may need a separate rule,
              coming down to the following: if M is not CoIntroducable (so, invariant-closing),
              then we need to immediately find whether P2 can also close the invariant. if not,
              drop it, if so, present it as an alternative 
         *)
    rewrite /BiAbdDisj => /tforall_forall HPS HM HMM' HTTeq b.
    apply tforall_forall => ttl /=.
    rewrite !tele_app_bind /=.
    apply bi.wand_elim_r', or_elim; apply bi.wand_intro_l.
    - rewrite bi.and_elim_l.
      apply bi.wand_elim_r', bi_texist_elim => ttl'.
      apply bi.wand_elim_r', impl_elim_l', pure_elim' => Hb. subst.
      apply bi.impl_intro_r. rewrite left_id.
      apply bi.pure_elim' => Httleq. revert HTTeq. 
      rewrite /SimplTeleEq => /(dep_eval_tele ttl') /(dep_eval_tele ttl) => 
        /proj1 /(dep_eval Httleq) /tele_eq_eq_1 Httleq'. subst => {Httleq}.
      do 2 apply bi.wand_intro_l. rewrite assoc HPS modality_strong_frame_l. apply HM.
      apply bi.wand_elim_l', or_elim.
      * apply bi_texist_elim => ttr.
        rewrite -or_intro_l bi_texist_exist -(exist_intro ttr).
        rewrite !tele_map_app /=. apply bi.wand_intro_r. by rewrite assoc.
      * rewrite -or_intro_r. rewrite tele_map_app. destruct (tele_app D ttl) as [D'|].
        + by apply bi.wand_intro_r.
        + by apply bi.pure_elim'.
    - rewrite bi.and_elim_r. apply bi.wand_elim_r', pure_elim' => Hb. subst.
      apply bi.wand_intro_l. specialize (HMM' True%I). rewrite {1}HMM'.
      rewrite modality_strong_frame_r. apply HM. by rewrite -or_intro_r comm.
  Qed.

  Lemma biabd_disj_hyp_andr P1 P2 {TTl TTr} Q M S T D M' tteq :
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P2 Q M S T D →
    ModalityStrongMono M →
    CoIntroducable M M' →
    SimplTeleEq TTl tteq →
    BiAbdDisj (TTl := TeleS (λ b : bool, if b then TTl else TeleO)) (TTr := TTr) false (P1 ∧ P2)%I Q M 
      (λ (b : bool),
          tele_bind (λ tif : if b then TTl else TeleO, (∃.. (ttl : TTl), tele_app S ttl ∗ ⌜b = true⌝ ∧ match b, tif return PROP with | false, _ => False%I | true, ttl' => ⌜tele_app (tele_app tteq ttl) ttl'⌝ end) ∨ ⌜b = false⌝))%I 
      (λ (b : bool), 
            match b return (if b then TTl else TeleO) -t> TTr -t> PROP  with
            | true => tele_map (tele_map (flip bi_sep True)) T
            | false => tele_bind (λ _ : TTr, False) 
            end)%I
      (λ (b : bool), 
            match b return (if b then TTl else TeleO) -t> option2 PROP  with
            | true => tele_map (λ op, match op with |Some2 D' => Some2 (D' ∗ True) | _ => None2 end) D
            | false => Some2 (P1 ∗ M' True)
            end)%I.
  Proof.
    move => HPS HM HMM' HTTeq.
    rewrite /BiAbdDisj.
    apply tforall_forall => ttl /=. rewrite and_comm. revert ttl. apply tforall_forall.
    eapply biabd_disj_hyp_andl; done.
  Qed. *)

  Lemma biabd_disj_later_mono p P n P' {TTl TTr : tele} Q Q' R S D :
    IntoLaterNMax P n P' → (* we can't use IntoLaterN since the n is an input there, not an output. *)
    (∀.. tt, FromLaterN (tele_app Q tt) n (tele_app Q' tt)) →
    Inhabited TTr →
    BiAbdDisj (TTl := TTl) (TTr := TTr) false P' Q' id R S D →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q id (tele_map (bi_laterN n) R) (tele_map (tele_map (bi_laterN n)) S) 
      (tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 $ (▷^n D') end)%I).
  Proof.
    rewrite /BiAbdDisj /= => HP /tforall_forall HQ HTT.
    rewrite !tforall_forall => + ttl.
    rewrite HP tele_map_app intuitionistically_if_elim -laterN_sep => ->.
    rewrite !tele_map_app.
    rewrite tele_app_bind.
    destruct (tele_app D ttl) as [D' | ] => //.
    - rewrite laterN_or.
      apply or_mono; last done.
      rewrite !bi_texist_exist laterN_exist.
      apply exist_mono => ttr.
      rewrite -HQ laterN_sep !tele_map_app //.
    - rewrite !right_id.
      rewrite !bi_texist_exist laterN_exist.
      apply exist_mono => ttr.
      rewrite -HQ laterN_sep !tele_map_app //.
  Qed.

  Lemma biabd_disj_hyp_orl P P1 P2 {TTl TTr} p Q M M' S U D :
    IntoOr P P1 P2 →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P1 Q M S U D →
    ModalityStrongMono M →
    CoIntroducable M M' →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M S U 
      (tele_bind (λ ttl, Some2 $ match tele_app D ttl with
        | Some2 D' => D' ∨ □?p P2 ∗ tele_app S ttl ∗ M' emp
        | None2 => □?p P2 ∗ tele_app S ttl ∗ M' emp
        end))%I.
  Proof.
    rewrite /BiAbdDisj /= !tforall_forall /IntoOr => HP HPQ HM HM' ttl.
    rewrite HP intuitionistically_if_or sep_or_r HPQ.
    apply or_elim.
    - apply HM, or_mono => //. rewrite tele_app_bind. 
      destruct (tele_app D ttl) as [D' | ]; first apply or_intro_l.
      by apply bi.pure_elim'.
    - rewrite tele_app_bind.
      rewrite -{1}(right_id _ bi_sep (tele_app S ttl)).
      rewrite {1}(HM' emp)%I.
      do 2 rewrite modality_strong_frame_r.
      apply HM.
      rewrite -or_intro_r.
      rewrite comm (comm _ (M' emp)%I).
      destruct (tele_app D ttl) as [D' | ]; last done.
      by rewrite -or_intro_r.
  Qed.

  Lemma biabd_disj_hyp_orr P P1 P2 {TTl TTr} p Q M M' S U D :
    IntoOr P P1 P2 →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P2 Q M S U D →
    ModalityStrongMono M →
    CoIntroducable M M' →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M S U 
      (tele_bind (λ ttl, Some2 $ match tele_app D ttl with
        | Some2 D' => D' ∨ □?p P1 ∗ tele_app S ttl ∗ M' emp
        | None2 => □?p P1 ∗ tele_app S ttl ∗ M' emp
        end))%I.
  Proof.
    intros. eapply biabd_disj_hyp_orl => //.
    revert H; rewrite /IntoOr => ->.
    by rewrite comm.
  Qed.

  Lemma biabd_disj_persistency_change {TTl TTr} p P Q M S U D :
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M S U D →
    BiAbdDisj (TTl := TTl) (TTr := TTr) true P Q M S U D.
  Proof.
    rewrite /BiAbdDisj !tforall_forall => + ttl => /(dep_eval ttl) <- /=.
    destruct p => /=; first done.
    by rewrite bi.intuitionistically_elim.
  Qed.
End lemmas_disj.

Unset Universe Polymorphism.