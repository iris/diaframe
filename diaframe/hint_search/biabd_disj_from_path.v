From diaframe.hint_search Require Export search_biabd_disj biabd_disj_query.
From diaframe Require Import solve_defs util_classes util_instances.
From diaframe.steps Require Import ltac2_store ipm_hyp_utils.
From iris.bi Require Import bi.
From iris.proofmode Require Import environments reduction.
From stdpp Require Import base telescopes.


(* make everything that takes an Ltac1 continuation more easily available in Ltac2 *)
Ltac2 biabd_disj_step_orl (cont : unit -> unit) : unit :=
  let f := ltac1:(c |- 
    biabd_disj_step_orl ltac:(idtac; c tt)
  ) in
  f (as_ltac1_thunk cont).

Ltac2 biabd_disj_step_orr (cont : unit -> unit) : unit :=
  let f := ltac1:(c |-
    biabd_disj_step_orr ltac:(idtac; c tt)
  ) in
  f (as_ltac1_thunk cont).

Ltac2 biabd_disj_step_hyp_exist (cont : unit -> unit) : unit :=
  let f := ltac1:(c |-
    biabd_disj_step_hyp_exist ltac:(idtac; c tt)
  ) in
  f (as_ltac1_thunk cont).

Ltac2 biabd_disj_step_hyp_mod (cont : unit -> unit) : unit :=
  let f := ltac1:(c |-
    biabd_disj_step_hyp_mod ltac:(idtac; c tt)
  ) in
  f (as_ltac1_thunk cont).

(* mutable, so that later credit support can override this *)
Ltac2 mutable biabd_disj_step_hyp_later (cont : unit -> unit) : unit :=
  ltac1:(biabd_disj_step_later_mono); 
  cont ().

Ltac2 biabd_disj_step_forall (cont : unit -> unit) : unit :=
  let f := ltac1:(c |-
    biabd_disj_step_forall ltac:(idtac; c tt)
  ) in
  f (as_ltac1_thunk cont).

Ltac2 biabd_disj_step_goal_exist (cont : unit -> unit) : unit :=
  let f := ltac1:(c |-
    biabd_disj_step_goal_exist ltac:(idtac; c tt)
  ) in
  f (as_ltac1_thunk cont).



Lemma biabddisjhyp_from_atom_into_connective {PROP : bi} {TTl TTr : tele} p P P' Q (M M1 M2 : PROP → PROP) R S D :
  AtomIntoConnective P P' →
  BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P' Q M M1 M2 R S D →
  BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D.
Proof.
  rewrite /AtomIntoConnective => HPP'. 
  case; case => HP HM. split; split; last done.
  revert HP. rewrite /BiAbdDisj !tforall_forall => + ttl => /(dep_eval ttl) /=. 
  by rewrite HPP'.
Qed.


Ltac biabd_disj_step_atom_into_connective := 
  autoapply @biabddisjhyp_from_atom_into_connective with typeclass_instances; 
  [tc_solve | ].

Ltac solve_biabd_disj_goal_now :=
  let p := 
    lazymatch goal with
    | |- BiAbdDisjUnfoldGoalMod false _ _ _ _ _ _ _ _ => false
    | |- BiAbdDisjUnfoldGoalMod true _ _ _ _ _ _ _ _ => true
    end
  in
  biabd_disj_step_goal_from_base p.

Ltac2 solve_biabd_disj_goal_try_exists (cont : unit -> unit) :=
  plus_once (fun _ => 
    ltac1:(solve_biabd_disj_goal_now)
  ) (fun _ =>
    biabd_disj_step_goal_exist cont
  ).

Ltac2 rec apply_biabd_disj_goal_rules_go (_ : unit) : unit :=
  plus_once (fun _ => 
    ltac1:(solve_biabd_disj_goal_now)
  ) (fun _ =>
    let cont := plus_once (fun _ =>
      ltac1:(biabd_disj_step_goal_mod);
      (fun c => solve_biabd_disj_goal_try_exists c)
    ) (fun _ =>
      (fun c => solve_biabd_disj_goal_try_exists c)
    ) in
    cont apply_biabd_disj_goal_rules_go
  ).

Ltac2 apply_biabd_disj_goal_rules (_ : unit) : unit :=
  ltac1:(autoapply @biabddisjhyp_from_unfold_goal with typeclass_instances);
  apply_biabd_disj_goal_rules_go ().

Ltac2 print_goal (_ : unit) : unit :=
  lazy_match! goal with
  | [|- ?g] => printf "goal: %t" g
  end.

Ltac2 rec construct_biabd_disj_from_path (path : path_el list) (p : bi_prop) : unit :=
  (* assumes proof obligation is a BiAbdDisjMod *)
  match path with
  | [] => apply_biabd_disj_goal_rules ()
  | el :: path =>
    apply_single_path_rule el p (fun p' =>
      construct_biabd_disj_from_path path p'
    )
  end
with
  apply_single_path_rule (el : path_el) (p : bi_prop) (cont : bi_prop -> unit) : unit :=
    match el with
    | PathElEnterAtom =>
      match p with
      | AtomProp _ mp =>
        match mp with
        | None => Control.throw Not_found
        | Some p' => 
          ltac1:(biabd_disj_step_atom_into_connective);
          cont p'
        end
      | _ => Control.throw Not_found
      end
    | PathElDontEnterAtom => cont p
    | PathElBinL =>
      match p with
      | BinProp bo l _ =>
        match bo with
        | BiBSep =>
          ltac1:(biabd_disj_step_sepl); cont l
        | BiBOr =>
          biabd_disj_step_orl (fun _ => cont l)
        end
      | _ => Control.throw Not_found
      end
    | PathElBinR =>
      match p with
      | BinProp bo _ r =>
        match bo with
        | BiBSep =>
          ltac1:(biabd_disj_step_sepr); cont r
        | BiBOr =>
          biabd_disj_step_orr (fun _ => cont r)
        end
      | _ => Control.throw Not_found
      end
    | PathElWand => 
      match p with
      | UnProp uo p' =>
        ltac1:(biabd_disj_step_wand); cont p'
      | _ => Control.throw Not_found
      end
    | PathElMod m => 
      match p with
      | UnProp uo p' =>
        biabd_disj_step_hyp_mod (fun _ => cont p')
      | _ => Control.throw Not_found
      end
    | PathElLater =>
      match p with
      | UnProp uo p' =>
        biabd_disj_step_hyp_later (fun _ => cont p')
      | _ => Control.throw Not_found
      end
    | PathElForall => 
      match p with
      | QuantProp qo _ _ p' =>
        biabd_disj_step_forall (fun _ => cont p')
      | _ => Control.throw Not_found
      end
    | PathElExist => 
      match p with
      | QuantProp qo _ _ p' =>
        biabd_disj_step_hyp_exist (fun _ => cont p')
      | _ => Control.throw Not_found
      end
    end
.


Ltac2 construct_biabd_disj_pre_reified (rp : bi_prop)
  (b : constr) (p : constr) (ttr : constr) (q : constr) : unit :=
  let bi := get_prop_type p in
  let path := biabd_disj_query_on bi b rp ttr q in
(*   printf "path: %a" (fun () => path_list_to_string) path; *)
  construct_biabd_disj_from_path (List.rev path) rp.


Ltac2 construct_biabd_disj_from_goal_matched_tac1s 
  (b1 : Ltac1.t) (p1 : Ltac1.t) (ttr1 : Ltac1.t) (q1 : Ltac1.t) : unit :=
  let b := Option.get (Ltac1.to_constr b1) in
  let p := Option.get (Ltac1.to_constr p1) in
  let ttr := Option.get (Ltac1.to_constr ttr1) in
  let q := Option.get (Ltac1.to_constr q1) in
  let rp := reify_prop p in
(*   printf "rp: %a" (fun () => bi_prop_to_string true) rp; *)
  construct_biabd_disj_pre_reified rp b p ttr q.


Global Hint Extern 1 (@BiAbdDisjMod _ _ ?TTr ?p ?P ?Q ?M _ _ _ _ _) =>
  find_biabd_disj_initial_step;
  let f := ltac2:(b1 p1 ttr1 q1 |-
    construct_biabd_disj_from_goal_matched_tac1s b1 p1 ttr1 q1
  ) in
  f p P TTr Q : typeclass_instances.

Ltac construct_biabd_disj_from_path :=
  lazymatch goal with
  | |- @BiAbdDisjMod _ _ ?TTr ?p ?P ?Q ?M _ _ _ _ _ =>
    find_biabd_disj_initial_step;
    let f := ltac2:(b1 p1 ttr1 q1 |-
      construct_biabd_disj_from_goal_matched_tac1s b1 p1 ttr1 q1
    ) in
    f p P TTr Q
  end.

(* for debugging, this can be quite handy: *)
Ltac debug_construct_biabd_disj_from_path :=
  lazymatch goal with
  | |- @BiAbdDisjMod _ _ ?TTr ?p ?P ?Q ?M _ _ _ _ _ =>
    find_biabd_disj_initial_step;
    let f := ltac2:(b1 p1 ttr1 q1 |-
      let b := Option.get (Ltac1.to_constr b1) in
      let p := Option.get (Ltac1.to_constr p1) in
      let ttr := Option.get (Ltac1.to_constr ttr1) in
      let q := Option.get (Ltac1.to_constr q1) in
      let bi := get_prop_type p in
      let rp := reify_prop p in
      printf "rp: %a" (fun () => bi_prop_to_string true) rp;
      let path := biabd_disj_query_on bi b rp ttr q in
      printf "path: %a" (fun () => path_list_to_string) path;
      construct_biabd_disj_from_path (List.rev path) rp
    ) in
    f p P TTr Q
  end.


Ltac2 construct_biabd_disj_pre_reified_on_matched_goal (rp : bi_prop) : unit :=
  lazy_match! goal with
  | [|- @BiAbdDisjMod _ _ ?ttr ?b ?p ?q _ _ _ _ _ _ ] =>
    ltac1:(find_biabd_disj_initial_step);
    construct_biabd_disj_pre_reified rp b p ttr q
  end.


(* We will usually be looking for the following goal during the automation:
  [FindInExtendedContext Δ (λ p P, BiAbdDisjMod (TTl := TTl) (TTr := TTr) p P P2 M' M1 M2 S T D) mi p P1]
  and we have available an Ltac2 [(ipm_ident, bi_prop) store.t] object in which we can store
  reified propositions. We now define a way to construct instances of this typeclass using the store. *)

Ltac2 reified_first_hyp (bi : constr) : bi_prop :=
  AtomProp '(@empty_hyp_first $bi) None.
Ltac2 reified_middle_hyp (bi : constr) : bi_prop :=
  AtomProp '(@empty_hyp_last_spatial $bi) None.
Ltac2 reified_last_hyp (bi : constr) : bi_prop :=
  AtomProp '(@empty_hyp_last $bi) None.

(* Optimization: whenever syntactically equal props exist in our context,
  shortcircuit directly to that one. *)
Ltac2 rec find_syntactically_equal_to_in (Γ : constr) (q : constr) : constr * constr :=
  lazy_match! Γ with
  | Esnoc ?env' ?i ?p =>
    if Constr.equal p q then
      (i, p)
    else
      find_syntactically_equal_to_in env' q
  end.

Ltac2 construct_biabd_disj_in_env_except_tc
  (i : constr) (p : constr) (pers : constr) (ttr : constr) (q : constr) 
    (rp_store : (ipm_ident, bi_prop) store.t) 
    (chosen_mi : constr) (chosen_p : constr) : unit :=
  let rp := store.get_or_set rp_store (ipm_ident_of_constr i)
                    (fun _ => reify_prop p) in
  let bi := get_prop_type p in
  let path := biabd_disj_query_on bi pers rp ttr q in
  unify $chosen_p $p; unify $chosen_mi (Some $i);
  ltac1:(find_biabd_disj_initial_step);
  construct_biabd_disj_from_path (List.rev path) rp.

Ltac2 rec find_biabd_disj_in_env_rec
    (env : constr) (pers : constr) (ttr : constr) (q : constr) 
    (rp_store : (ipm_ident, bi_prop) store.t) 
    (chosen_mi : constr) (chosen_p : constr) : unit :=
  (* assumes the goal is [BiAbdDisjMod (TTl := TTl) (TTr := TTr) p ?P P2 M' M1 M2 S T D] *)
  lazy_match! env with
  | Esnoc ?env' ?i ?p =>
    let cont := plus_once_list
      [ construct_biabd_disj_in_env_except_tc 
          i p pers ttr q 
          rp_store chosen_mi chosen_p;
        (fun _ => tc_solve ())
      | fun _ => find_biabd_disj_in_env_rec env' pers ttr q rp_store chosen_mi chosen_p] in
    cont ()
  end.

Ltac2 find_biabd_disj_in_env
    (env : constr) (pers : constr) (ttr : constr) (q : constr) 
    (rp_store : (ipm_ident, bi_prop) store.t) 
    (chosen_mi : constr) (chosen_p : constr) : unit :=
  (* In some cases, this directly shortcircuits to a syntactically equal hyp in env.
    Otherwise, calls find_biabd_disj_in_env_rec *)
  let cont := plus_once_list 
    [ lazy_match! ttr with
      | TeleO =>
        let (i, p) := find_syntactically_equal_to_in env q in
        construct_biabd_disj_in_env_except_tc i p pers ttr q rp_store chosen_mi chosen_p;
        (fun _ => tc_solve ())
      end
    | (fun _ => find_biabd_disj_in_env_rec env pers ttr q rp_store chosen_mi chosen_p)]
  in
  cont ().

Ltac2 find_biabd_disj_first_hyp 
    (ttr : constr) (q : constr) (bi : constr) : unit :=
  let rp := reified_first_hyp bi in
  let path := biabd_disj_query_on bi 'false rp ttr q in
  eapply fic_ext_lookup_empty_first; cbn beta; 
  ltac1:(find_biabd_disj_initial_step);
  construct_biabd_disj_from_path (List.rev path) rp; tc_solve ().

Ltac2 find_biabd_disj_middle_hyp 
    (ttr : constr) (q : constr) (bi : constr) : unit :=
  let rp := reified_middle_hyp bi in
  let path := biabd_disj_query_on bi 'false rp ttr q in
  eapply fic_ext_lookup_empty_last_spatial; cbn beta; 
  ltac1:(find_biabd_disj_initial_step);
  construct_biabd_disj_from_path (List.rev path) rp; tc_solve ().

Ltac2 find_biabd_disj_last_hyp 
    (ttr : constr) (q : constr) (bi : constr) : unit :=
  let rp := reified_last_hyp bi in
  let path := biabd_disj_query_on bi 'false rp ttr q in
  eapply fic_ext_lookup_empty_last; cbn beta; 
  ltac1:(find_biabd_disj_initial_step);
  construct_biabd_disj_from_path (List.rev path) rp; tc_solve ().

Ltac2 find_biabd_disj_in_envs
    (envs : constr) (ttr : constr) (q : constr) 
    (rp_store : (ipm_ident, bi_prop) store.t) 
    (chosen_mi : constr) (chosen_p : constr) : unit :=
  let (bi, Γs, Γp) :=
    lazy_match! envs with
    | @Envs ?bi ?gp ?gs _ => (bi, gs, gp)
    end
  in
  first 
    [ find_biabd_disj_first_hyp ttr q bi
    | eapply findinextcontext_from_spat; 
      split > [ | cbn beta; find_biabd_disj_in_env Γs 'false ttr q rp_store chosen_mi chosen_p]; ltac1:(pm_reflexivity)
    | find_biabd_disj_middle_hyp ttr q bi
    | eapply findinextcontext_from_pers; 
      split > [ | cbn beta; find_biabd_disj_in_env Γp 'true ttr q rp_store chosen_mi chosen_p]; ltac1:(pm_reflexivity)
    | find_biabd_disj_last_hyp ttr q bi ].


Ltac2 find_biabd_disj_in_envs_on_goal (rp_store : (ipm_ident, bi_prop) store.t) : unit :=
  lazy_match! goal with
  | [|- FindInExtendedContext ?Δ (λ p P, BiAbdDisjMod (TTr := ?ttr) p P ?q _ _ _ _ _ _) ?mi _ ?chosen_p] =>
    find_biabd_disj_in_envs Δ ttr q rp_store mi chosen_p
  end.











