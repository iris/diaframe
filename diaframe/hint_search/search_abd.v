From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes tele_utils solve_defs util_instances.
From diaframe.hint_search Require Import lemmas_abd search_biabd lemmas_biabd_disj.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file uses the recursive rules for constructing abduction hints 
   to build a procedure which finds hints using these rules.
*)

Unset Universe Polymorphism.

Section witness_postpone_optimization.
  Context {PROP : bi}.
  Implicit Types (M : PROP → PROP).

  Lemma abd_rec_mod_forall_unif p P' `(P : A → PROP) {C : tele} TT Q M M1 M2 Mh R (g : C -t> A) :
    IntoForallCareful P' P →
    SplitLeftModality3_Save M M1 M2 →
    (∀ (c : C), ∃ a' R',
      AbductModRec (TT := TT) p (P a') Q M2 Mh R' ∧
      TCAnd (GatherEvarsEq a' (tele_app g c)) $ (* if g : C → A maybe the tactic can be simpler..? *)
      TCEq R' (R c)
    ) →
    IntroducableModality Mh →
    ModalityMono Mh →
    AbductModRec (TT := TT) p P' Q M M1 (∃.. c, R c).
  Proof.
    rewrite /IntoForallCareful => HP HMs Habd HMh HMh'.
    (* need to do some work before we can use abd_forall *)
    rewrite /AbductModRec. etrans.
    apply HMs. rewrite HP.
    assert ((∃.. c, R c) ⊢ (∃.. c, tele_app (tele_bind R) c)) as ->.
    { apply bi_texist_mono => tt. by rewrite tele_app_bind. }
    done.
    eapply (abd_rec_mod_forall) => // c.
    destruct (Habd c) as [a' [R' [HPR [Ha' HR]]]].
    revert Ha' HPR => /eq_from_gather_evars_eq ->.
    rewrite /AbductModRec => <-. apply HMh'. 
    apply bi.sep_mono_r. rewrite tele_app_bind HR //.
  Qed.

  Lemma abd_rec_mod_witness_unif {A : Type} {C : tele} {TTf : A → tele} (g : C -t> A) M M1 M2 Mh R P (Q : TeleS TTf -t> PROP) p :
    SplitLeftModality3 M M1 M2 →
    (∀ (c : C), ∃ a' R''',
      AbductModRec (TT := TTf a') p P (Q a') M2 Mh R''' ∧ (* R''': PROP! so stuff is not as difficult as in BiAbd *)
      TCAnd (GatherEvarsEq a' (tele_app g c)) $
      TCEq (R''') (R c)
    ) →
    IntroducableModality Mh →
    AbductModRec (TT := TeleS TTf) p P Q M M1 (∃.. c, R c).
  Proof.
    move => HMs HPR HM.
    rewrite /AbductModRec.  etrans.
    apply HMs.
    assert ((∃.. c, R c) ⊢ (∃.. c, tele_app (tele_bind R) c)) as -> => //.
    { apply bi_texist_mono => tt. by rewrite tele_app_bind. }
    eapply abd_rec_mod_goal_exist => //.
    move => c.
    destruct (HPR c) as [a' [R' [HPR2 [Ha' ->]]]].
    revert Ha' HPR2 => /eq_from_gather_evars_eq ->.
    rewrite /AbductModRec => HPR2.
    erewrite <-HPR2. rewrite tele_app_bind //.
  Qed.

End witness_postpone_optimization.

Section stages_rec_mod.
  Context {PROP : bi}.
  Implicit Types (M : PROP → PROP).

  Definition AbductGoalModRec_def := @AbductModRec.
  Definition AbductGoalModRec_aux : seal (@AbductGoalModRec_def). Proof. by eexists. Qed.
  Definition AbductGoalModRec := AbductGoalModRec_aux.(unseal).
  Global Opaque AbductGoalModRec.
  Global Arguments AbductGoalModRec {PROP} TT p P Q M M1 S : rename, simpl never.
  Lemma AbductGoalModRec_eq : @AbductGoalModRec = @AbductModRec.
  Proof. trans AbductGoalModRec_def; last done. rewrite -AbductGoalModRec_aux.(seal_eq) //. Qed.

  Lemma abd_goal_mod_rec_from_base p P {TTr} Q M M1 M2 S :
    TCAnd (Abduct (TT := TTr) p P Q M2 S) (SplitModality3 M M1 M2) →
    AbductGoalModRec TTr p P Q M M1 S.
  Proof.
    case => HPQ HMs. rewrite AbductGoalModRec_eq.
    eapply abduct_mod_rec_from_abduct => //.
    apply HMs. apply HMs.
  Qed.

  Lemma abd_goal_mod_rec_from_base_pers p P {TTr} Q M M1 M2 S :
    TCAnd (Abduct (TT := TTr) p P Q M2 S) (SplitModality3 M M1 M2) →
    AbductGoalModRec TTr true P Q M M1 S.
  Proof.
    intros [H HMs]. eapply abd_goal_mod_rec_from_base; split; last done.
    by eapply abd_persistency_change.
  Qed.

  Lemma abd_goal_mod_rec_from_base_biabd p P {TTl TTr} Q M M1 M2 M' Mr S T :
    SplitLeftModality3 M M' Mr →
    TCAnd (BiAbd (TTl := TTl) (TTr := TTr) p P Q M2 S T) (SplitModality3 M' M1 M2) →
    AbductGoalModRec TTr p P Q M M1 (∃.. ttl, tele_app S ttl ∗ (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mr emp))%I.
  Proof.
    move => HM. rewrite AbductGoalModRec_eq.
    case => HPQ HMs.
    by eapply abduct_mod_rec_from_biabd.
  Qed.

  Lemma abd_goal_mod_rec_from_base_biabd_disj p P {TTl TTr} Q M M1 M2 M' Mr S T D Mc :
    SplitLeftModality3 M M' Mr →
    TCAnd (BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 S T D) (SplitModality3 M' M1 M2) →
    StripSaved Mr Mc →
    AbductGoalModRec TTr p P Q M M1 (∃.. ttl, tele_app S ttl ∗
        match tele_app D ttl with
        | None2 => (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mc emp)
        | Some2 D' => (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mc emp) ∧ (D' -∗ Mc $ ∃.. ttr, tele_app Q ttr)
        end)%I.
  Proof.
    move => HM. rewrite AbductGoalModRec_eq.
    case => HPQ HMs HMc.
    by eapply abduct_mod_rec_from_biabd_disj.
  Qed.

  Lemma abd_goal_mod_rec_from_base_biabd_disj_pers p P {TTl TTr} Q M M1 M2 M' Mr S T D Mc :
    SplitLeftModality3 M M' Mr →
    TCAnd (BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 S T D) (SplitModality3 M' M1 M2) →
    StripSaved Mr Mc →
    AbductGoalModRec TTr true P Q M M1 (∃.. ttl, tele_app S ttl ∗
        match tele_app D ttl with
        | None2 => (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mc emp)
        | Some2 D' => (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mc emp) ∧ (D' -∗ Mc $ ∃.. ttr, tele_app Q ttr)
        end)%I.
  Proof.
    move => HM1 [H HM2] HM3. eapply abd_goal_mod_rec_from_base_biabd_disj; [ | split | ]; try done.
    by eapply biabd_disj_persistency_change.
  Qed.

  Section hyp.
    Lemma abdhyp_rec_mod_mod_intro_l p P M1 P' {TTr} Q M2 S M Mh Ml :
      IntoModal p P M1 P' →
      SplitModality4 M Ml M1 M2 → (* it is valid and sometimes desired to keep a part of Ml around *)
      AbductModRec (TT := TTr) false P' Q M2 Mh S →
      ModalityStrongMono Mh →
      AbductModRec (TT := TTr) p P Q M Ml (Mh S).
    Proof.
      intros.
      eapply (abd_modrec_id_swap _ _ _ _ _ (M1 ∘ M2)); try apply H0.
      eapply abd_rec_mod_mod_intro_l; try done; try apply H0.
      move => ? //.
    Qed.

    Lemma abdhyp_rec_from_goal_rec TTr p P Q M M1 S :
      AbductGoalModRec TTr p P Q M M1 S →
      AbductModRec (TT := TTr) p P Q M M1 S.
    Proof. by rewrite AbductGoalModRec_eq. Qed.
  End hyp.

  Section goal.
    Lemma abd_goal_rec_mod_goal_exist {A : Type} {C : tele} {TTf : A → tele} (g : C -t> A) M M1 M2 Mh R P (Q : TeleS TTf -t> PROP) p :
      SplitLeftModality3_Save M M1 M2 →
      (∀ (c : C), ∃ a' R''',
        AbductGoalModRec (TTf a') p P (Q a') M2 Mh R''' ∧ (* R''': PROP! so stuff is not as difficult as in BiAbd *)
        TCAnd (GatherEvarsEq a' (tele_app g c)) $
        TCEq (R''') (R c)
      ) →
      IntroducableModality Mh →
      AbductGoalModRec (TeleS TTf) p P Q M M1 (∃.. c, R c).
    Proof. rewrite AbductGoalModRec_eq. apply abd_rec_mod_witness_unif. Qed.

    Lemma abd_goal_mod_later_r p P {TTr : tele} Q Q' n M M1 S :
      (TC∀.. tt, FromLaterNMax (tele_app Q tt) n (tele_app Q' tt)) →
      Inhabited TTr → (* or if TTr is trivial.. *)
      ModalityMono M →
      AbductGoalModRec TTr p P Q' (M ∘ bi_laterN n) M1 S →
      AbductGoalModRec TTr p P Q M M1 S.
    Proof. rewrite AbductGoalModRec_eq. apply abd_mod_later_r. Qed.

    Lemma abd_goal_rec_mod_goal_foc_disj_ex p P M M1 {TT : tele} Q D R :
      AbductGoalModRec TT p P Q M M1 R →
      ModalityMono M →
      AbductGoalModRec [tele] p P (FocDisjEx (TT := TT) Q D) M M1 R.
    Proof. rewrite AbductGoalModRec_eq. apply abd_rec_mod_goal_foc_disj_ex. Qed.
  End goal.
End stages_rec_mod.


Ltac find_abd_mod_input_initial := 
  autoapply @abd_mod_input_from_mod_rec with typeclass_instances; [
  lazymatch goal with
  | |- AbductModRec _ _ (FocDisjEx _ _) _ _ _ =>
    autoapply @abd_rec_mod_start_foc_disj_ex with typeclass_instances; [tc_solve | ..]
  | |- _ => idtac
  end | ].

Ltac abd_mod_rec_later_mono cont :=
  autoapply @abd_rec_mod_later_mono with typeclass_instances; [tc_solve .. | cont | | ].

Ltac abd_mod_rec_sepl cont := autoapply @abd_rec_mod_sepl with typeclass_instances; [tc_solve| cont | ].
Ltac abd_mod_rec_sepr cont := autoapply @abd_rec_mod_sepr with typeclass_instances; [tc_solve| cont | ].
Ltac abd_mod_rec_sep P cont := 
  assert_succeeds (assert (∃ P1 P2, IntoSepCareful P P1 P2) as _ by (eexists; eexists; tc_solve)); 
    ((abd_mod_rec_sepl cont) + (abd_mod_rec_sepr cont)).

Ltac abd_mod_rec_orl cont := autoapply @abd_rec_mod_orl with typeclass_instances; [ cont | ..].
Ltac abd_mod_rec_orr cont := autoapply @abd_rec_mod_orr with typeclass_instances; [ cont | ..].
Ltac abd_mod_rec_or cont := (abd_mod_rec_orl cont) + (abd_mod_rec_orr cont).

Ltac abd_mod_rec_hyp_exist cont := 
  autoapply @abd_rec_mod_exist with typeclass_instances; [tc_solve | 
        cbn [tforall tele_fold tele_bind tele_app tele_appd_dep]; intros; cont | ..].

Ltac abd_mod_rec_hyp_mod cont := 
  autoapply @abdhyp_rec_mod_mod_intro_l with typeclass_instances; [tc_solve | tc_solve | cont | ..].

Ltac abd_mod_rec_wand cont := 
  autoapply @abd_rec_mod_wand with typeclass_instances; [tc_solve | cont | ..].

Ltac abd_mod_rec_forall cont := 
  autoapply @abd_rec_mod_forall_unif with typeclass_instances; [tc_solve | tc_solve | 
      intros ?; do 2 eexists; split; [cont | ] 
      | | ].


Ltac abd_mod_rec_from_goal :=
  autoapply @abdhyp_rec_from_goal_rec with typeclass_instances.

Ltac abd_mod_rec_atom_shortcut p P :=
  assert_succeeds (assert (AtomAndConnective p P) by tc_solve);
  abd_mod_rec_from_goal.

Ltac abd_mod_rec_step_hyp_naive p P cont := 
  (abd_mod_rec_atom_shortcut p P; cont) + (
  (abd_mod_rec_later_mono cont) ||
  (abd_mod_rec_sep P cont) ||
  (abd_mod_rec_or cont) ||
  (abd_mod_rec_hyp_exist cont) ||
  (abd_mod_rec_hyp_mod cont) ||
  (abd_mod_rec_wand cont) ||
  (abd_mod_rec_forall cont) ||
  (abd_mod_rec_from_goal; cont)
  ).

Ltac abd_connective_as_atom_shortcut := fail.
(* this keeps open the option of seeing, for example, all wands also as atoms.
  Redefine as:
Ltac abd_connective_as_atom_shortcut ::= abd_mod_rec_atom_shortcut.
  to find more hints *)

Ltac abd_mod_rec_step_hyp p P cont :=
  (abd_connective_as_atom_shortcut p P; cont) +
  lazymatch P with
       (* The (||) is to support some tricky laterable stuff. These ensure we don't quit too early when 
          looking for hints of the shape  Abduct _ (▷ P) Q _ _ _  -> so no matching later in goal *)
  | bi_later _ => (abd_mod_rec_later_mono cont) || (abd_mod_rec_from_goal; cont)
  | bi_laterN _ _ => (abd_mod_rec_later_mono cont) || (abd_mod_rec_from_goal; cont)
  | bi_sep _ _ => abd_mod_rec_sep P cont
  | bi_or _ _ => abd_mod_rec_or cont
  | bi_exist _ => abd_mod_rec_hyp_exist cont
  | fupd _ _ _ => abd_mod_rec_hyp_mod cont
  | bupd _ => abd_mod_rec_hyp_mod cont
  | bi_wand _ _ => abd_mod_rec_wand cont
  | bi_forall _ => abd_mod_rec_forall cont
  | _ => abd_mod_rec_step_hyp_naive p P cont
  end.


Ltac abd_mod_rec_goal_mod := 
  autoapply @abd_goal_mod_later_r with typeclass_instances; [tc_solve.. | ].

Ltac abd_mod_rec_goal_exist cont :=
  autoapply @abd_goal_rec_mod_goal_exist with typeclass_instances; [tc_solve | 
      intros ?; do 2 (notypeclasses refine (ex_intro _ _ _)); split; [cont | ] 
    | ].

Ltac abd_mod_rec_goal_foc_disj_ex cont :=
  autoapply @abd_goal_rec_mod_goal_foc_disj_ex with typeclass_instances; [cont | ].

Ltac abd_mod_rec_step_goal_from_base p :=
  lazymatch p with
  | false =>
    (solve [autoapply @abd_goal_mod_rec_from_base with typeclass_instances; diaframe_hint_search]) || 
    (solve [autoapply @abd_goal_mod_rec_from_base_biabd_disj with typeclass_instances; 
                  [tc_solve | diaframe_hint_search | tc_solve] ])
  | true =>
    (solve [autoapply @abd_goal_mod_rec_from_base_pers with typeclass_instances; diaframe_hint_search]) || 
    (solve [autoapply @abd_goal_mod_rec_from_base_biabd_disj_pers with typeclass_instances; 
                  [tc_solve | diaframe_hint_search | tc_solve] ])
  end.

Ltac abd_mod_rec_step_goal p cont :=
  abd_mod_rec_step_goal_from_base p || (
    tryif abd_mod_rec_goal_mod then cont else
    (abd_mod_rec_goal_foc_disj_ex cont) ||
    (abd_mod_rec_goal_exist cont)
  ).

Ltac abd_mod_rec_step_with cont :=
  lazymatch goal with
  | |- AbductModRec ?p ?P _ _ _ _ => abd_mod_rec_step_hyp p P cont
  | |- AbductGoalModRec _ ?p _ _ _ _ _ => abd_mod_rec_step_goal p cont
  end.

Ltac abd_mod_rec_step_test := abd_mod_rec_step_with ltac:(idtac).

Ltac find_abd_mod_rec_steps :=
  let cont := ltac:(pre_step; find_abd_mod_rec_steps) in
  abd_mod_rec_step_with cont.

Ltac find_abd_mod_input := find_abd_mod_input_initial; [find_abd_mod_rec_steps|.. ]; solve [tc_solve].

Global Hint Extern 4 (AbductModInput _ _ _ _ _ _) =>
  find_abd_mod_input : typeclass_instances.








