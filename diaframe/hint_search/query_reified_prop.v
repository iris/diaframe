From diaframe.hint_search Require Export reify_prop.
From stdpp Require Import base.




Definition forall_operator [A : Type] (P : A → Prop) : Prop := ∀ a, P a.


Ltac2 refine_ex_intro (u : unit) := 
  (* should behave the same as ltac1:(refine (ex_intro _ _ _)) *)
  refine '(ex_intro _ _ _); Control.focus 1 1 Control.shelve; cbn beta.
  (* TODO: replace with ltac1:(autoapply @ex_intro)..?*)


Ltac2 Type path_el := [
  | PathElEnterAtom | PathElDontEnterAtom
  | PathElBinL | PathElBinR 
  | PathElWand | PathElMod (constr) | PathElLater
  | PathElForall | PathElExist ].

Ltac2 path_el_to_string (pe : path_el) : message :=
  match pe with
  | PathElEnterAtom =>
    fprintf "E"
  | PathElDontEnterAtom =>
    fprintf "A"
  | PathElBinL =>
    fprintf "BL"
  | PathElBinR =>
    fprintf "BR"
  | PathElWand =>
    fprintf "UW"
  | PathElMod m =>
    fprintf "UM"
  | PathElLater =>
    fprintf "U▷"
  | PathElForall =>
    fprintf "∀"
  | PathElExist =>
    fprintf "∃"
  end.

Ltac2 rec path_list_to_string (ps : path_el list) : message :=
  match ps with
  | [] => fprintf ""
  | p :: ps => 
    fprintf "%a ++ %a"
      (fun () => path_list_to_string) ps
      (fun () => path_el_to_string) p
  end.



Ltac2 rec query_bi_prop_int
  (p : bi_prop)
  (path_checker : path_el list -> bool)
  (query : path_el list -> constr -> constr) (* PROP → Prop *)
  (querysolver : unit -> unit)
  (quantifier : constr -> constr) (* appropriately quantify the query *)
  (quantifysolver : unit -> unit) (* and how to introduce those quantifiers *)
  (cur_path : path_el list)
    : path_el list :=
  if Bool.neg (path_checker cur_path) then
    Control.zero Assertion_failure
  else
  match p with
  | AtomProp c mp =>
    let atom_query (_ : unit) := 
      let query_here := query cur_path c in
      let quant_query := quantifier query_here in
      assert_succeeds (fun _ => assert $quant_query by (quantifysolver(); querysolver())); 
      PathElDontEnterAtom :: cur_path
    in
    match mp with
    | None =>
      atom_query ()
    | Some p' =>
      plus_once atom_query (fun _ => 
        query_bi_prop_int p' path_checker query querysolver quantifier quantifysolver (PathElEnterAtom :: cur_path)
      )
    end
  | BinProp bo l r =>
    plus_once (fun _ =>
      query_bi_prop_int l path_checker query querysolver quantifier quantifysolver (PathElBinL :: cur_path)
    ) (fun _ =>
      query_bi_prop_int r path_checker query querysolver quantifier quantifysolver (PathElBinR :: cur_path)
    )
  | UnProp uo p' =>
    let path_head := 
      match uo with
      | BiUWand _ => PathElWand
      | BiUMod c => PathElMod c
      | BiULater c => PathElLater
      end in
    query_bi_prop_int p' path_checker query querysolver quantifier quantifysolver (path_head :: cur_path)
  | QuantProp qo bindname bindtype p' => 
    let (quant_op, quant_tac, path_el) :=
      match qo with
      | BiQForall => '@ex, refine_ex_intro, PathElForall
      | BiQExist => '@forall_operator, (fun _ => intros ?), PathElExist
      end in     (* we could use Constr.Unsafe.Prof instead of forall_operator..?*)
    query_bi_prop_int p' path_checker query querysolver 
      (fun c =>
        let bound_query := make_lambda (Constr.Binder.make bindname bindtype) c in
        quantifier (safe_apply quant_op [bindtype; bound_query])
      )
      (fun u => quantifysolver u; quant_tac ())
      (path_el :: cur_path)
  end.

Ltac2 query_bi_prop
  (p : bi_prop)
  (path_checker : path_el list -> bool)
  (query : path_el list -> constr -> constr) (* PROP → Prop *)
  (querysolver : unit -> unit)
    : path_el list :=
  query_bi_prop_int p path_checker query querysolver (fun c => c) (fun c => c) [].








