From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes tele_utils solve_defs util_instances.
From diaframe.hint_search Require Import lemmas_biabd.
From iris.bi Require Import bi telescopes.

Import bi.


(* This file uses the recursive rules for constructing biabduction hints 
   to build a procedure which finds hints using these rules.
*)

(* This is some infrastructure to determine C, the telescope of generalizable evars *)

Inductive TCEqFunApp {A} (x : A) : A → Prop := TCEqFunApp_refl : TCEqFunApp x x.
Existing Class TCEqFunApp.

Definition eq_from_eq_fun_app {A : Type} {a b : A} :
  TCEqFunApp a b → a = b.
Proof. by case. Defined.

Lemma TCEq_to_eq {A : Type} {a b : A} : TCEq a b → a = b.
Proof. by case. Defined.
  
Inductive GatherEvarsEq {A} (x : A) : A → Prop := GatherEvarsEq_refl : GatherEvarsEq x x.
Existing Class GatherEvarsEq.

Definition eq_from_gather_evars_eq {A : Type} {a b : A} :
  GatherEvarsEq a b → a = b.
Proof. by case. Defined.

Section evar_retcon_optimization.
  Context {PROP : bi}. 
  (* so biabd_goal_exist and biabd_forall are really nice, it's just that the unification engine
    seems to properly hate them. so we have to jump through some hoops to make it usable *)
  Implicit Types (M : PROP → PROP).

  Lemma biabd_forall_unif p P' `(P : A → PROP) {C : tele} TTr Q M TTl (g : C -t> A) S T :
    IntoForallCareful P' P → 
    (∀ (c : C), ∃ a' TTla S'' T'', (* S'' : TTla -t> PROP, T'' : TTla -t> TTr -t> PROP *)
      BiAbd (TTr := TTr) (TTl := TTla) p (P a') Q M S'' T'' ∧
      TCAnd (GatherEvarsEq a' (tele_app g c)) $ ∃ (HTTeq : TCEq TTla (TTl c)), 
        (* yes, a lot of fun, we need eq_rect here *)
      TCAnd (TCEq (eq_rect TTla (λ T, T -t> PROP) S'' (TTl c) (TCEq_to_eq HTTeq)) (S c)) $ (* S : ∀ c, TTl c -t> PROP *)
      TCEq (eq_rect TTla (λ T, T -t> TTr -t> PROP) T'' (TTl c) (TCEq_to_eq HTTeq)) (T c)
    ) →
    ModalityMono M →
    BiAbd (TTr := TTr) (TTl := TeleConcatType C (tele_bind TTl)) p P' Q M 
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) 
              (λ c, tele_app_bind_type_fix (S c)))
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind 
              (λ c, tele_app_bind_type_fix (T c))).
  Proof.
    rewrite /IntoForallCareful => HPs HRQ HM. rewrite HPs {HPs}.
    eapply biabd_forall; last done.
    move => c. 
    destruct (HRQ c) as [a' [TTla [S'' [T'' [HPQRS [Hceq [HTTeq [HSEq HTEq]]]]]]]].
    revert HTTeq HPQRS HSEq HTEq.
    move => /TCEq_to_eq => HTeq. subst.
    revert Hceq. 
    move => /eq_from_gather_evars_eq Hceq. subst.
    move => /= HPQRS HS HT.
    revert HPQRS. rewrite HS HT. (* cant use proper since the telescopes are tele_app_bind rewritten *)

    rewrite /BiAbd => HRaw.
    apply tforall_forall => tx.
    revert HRaw => /(dep_eval_tele $ tele_app_bind_arg_fix tx) // HP.
    rewrite -tele_uncurry_curry_compose.
    rewrite tele_dep_appd_bind.
    rewrite tele_app_bind_fix_eq.
    erewrite HP.
    apply HM, bi_texist_mono => tt.
    apply sep_mono_r.
    rewrite -tele_uncurry_curry_compose.
    rewrite tele_dep_appd_bind.
    rewrite tele_app_bind_fix_eq //.
  Qed.

  Lemma biabd_goal_exist_unif {A : Type} {C : tele} {TTf : A → tele} TTl (g : C -t> A) S T (T' T'' : C -td> tele_bind TTl -c> TeleS TTf -t> PROP)
    P (Q : TeleS TTf -t> PROP) p M :
    (∀ (c : C), ∃ a' TTla S'' T''', 
      BiAbd (TTl := TTla) (TTr := TTf a') p P (Q a') M S'' T''' ∧
      ∃ (Haceq : GatherEvarsEq a' (tele_app g c)) (HTTeq : TCEq TTla (TTl c)), 
      TCAnd (TCEq (eq_rect TTla (λ T, T -t> PROP) S'' (TTl c) (TCEq_to_eq HTTeq)) (S c)) $ (* S : ∀ c, TTl c -t> PROP *)
      TCEq 
        (eq_rect a' (λ b, TTl c -t> TTf b -t> PROP)
          (eq_rect TTla (λ T, T -t> TTf a' -t> PROP) T''' (TTl c) (TCEq_to_eq HTTeq))
        (tele_app g c) (eq_from_gather_evars_eq Haceq)
      )
      (T c)
    ) →
    ModalityMono M → 
    (TC∀.. (c : C) (ttl : tele_app (tele_bind TTl) c), TransportTeleProp (tele_app g c) (tele_app (T c) $ tele_app_bind_arg_fix ttl) 
      (tele_app (tele_appd_dep T' c) ttl)
    ) →
    (TC∀.. (c : C) (ttl : tele_app (tele_bind TTl) c), TCEq (tele_app (tele_appd_dep T' c) ttl) (tele_app (tele_appd_dep T'' c) ttl)) →
    BiAbd (TTr := TeleS TTf) (TTl := TeleConcatType C (tele_bind TTl)) p P Q M 
        (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (S c)))
        (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ T'').
  Proof.
    move => HPs HM HRQ HTT.
    eapply (biabd_goal_exist _ _ _ (λ c, tele_app_bind_type_fix (T c))); [ | | done ].
    - intros.
      destruct (HPs c) as [a' [TTla [S'' [T''' [HPQRS [Hceq [HTTeq [HSEq HTEq]]]]]]]].
      revert HTTeq HPQRS HSEq HTEq.
      move => /TCEq_to_eq => HTeq. subst.
      revert Hceq. 
      move => /eq_from_gather_evars_eq Hceq. subst.
      move => HPQRS HS HT. simpl in HS, HT.
      revert HPQRS.
      rewrite HS HT. (* cant use proper since the telescopes are tele_app_bind rewritten *)

      rewrite /BiAbd => HRaw.
      apply tforall_forall => tx.
      revert HRaw => /(dep_eval_tele $ tele_app_bind_arg_fix tx) // HP.
      rewrite -tele_uncurry_curry_compose.
      rewrite tele_dep_appd_bind.
      rewrite tele_app_bind_fix_eq. 
      erewrite HP.
      apply HM, bi_texist_mono => tt.
      apply sep_mono_r.
      rewrite -tele_app_bind_fix_eq //.
    - apply tforall_forall => c.
      apply tforall_forall => tt.
      revert HTT => /(dep_eval_tele c) /(dep_eval_tele tt).
      rewrite -tele_uncurry_curry_compose => <-.
      rewrite tele_app_bind_fix_eq.
      revert HRQ => /(dep_eval_tele c) /(dep_eval_tele tt) //.
  Qed.

  Lemma biabd_exist_cut p (P : PROP) `(R : A → PROP) TT2 Q M TTl S T :
    IntoExistCareful2 P R →
    (∀ (a : A), ∃ TTl' S'' T'',
      BiAbd (TTr := TT2) (TTl := TTl') p (R a) Q M S'' T'' ∧
      ∃ (HTTeq : TCEqFunApp TTl' (TTl a)),
      TCAnd (TCEqFunApp (eq_rect TTl' (λ T, T -t> PROP) S'' (TTl a) (eq_from_eq_fun_app HTTeq)) (S a)) $
            (TCEqFunApp (eq_rect TTl' (λ T, T -t> TT2 -t> PROP) T'' (TTl a) (eq_from_eq_fun_app HTTeq)) (T a))
    ) →
    ModalityStrongMono M →
    BiAbd (TTr := TT2) (TTl := [tele]) p P Q M (∀ a, ∃.. (ttl : TTl a), tele_app (S a) ttl)%I 
        (tele_bind (λ tt2, ∃ a, ∃.. (ttl : TTl a), tele_app (tele_app (T a) ttl) tt2))%I.
  Proof.
    rewrite /BiAbd /= => -> HRS HM.
    rewrite intuitionistically_if_exist.
    apply wand_elim_l', exist_elim => a.
    apply wand_intro_r.
    rewrite (forall_elim a).
    apply wand_elim_r', bi_texist_elim => ttl.
    apply wand_intro_l.
    revert HRS => /(dep_eval a) [TTl' [S' [T' [Habd [+ +]]]]].
    move => /eq_from_eq_fun_app HTTeq. subst. simpl.
    case => <- HT.
    revert Habd => /(dep_eval_tele ttl) ->.
    apply HM, bi_texist_mono => tt2.
    rewrite tele_app_bind.
    apply sep_mono_r.
    rewrite -(bi.exist_intro a) bi_texist_exist -(bi.exist_intro ttl) HT //.
  Qed.

End evar_retcon_optimization.

(* this can be obtained from the hide_evars tactic in steps.small_steps..? *)
Ltac generalize2_int the_var :=
  first 
  [ match goal with
    | |- context [?arg] =>
      is_evar arg;
      let evar_name := fresh "x" in
      set (evar_name := arg);
      generalize2_int the_var;
      unfold evar_name; clear evar_name
    end
  | generalize the_var ].

Ltac generalize2 the_var :=
  let the_var' := fresh "x" in
  set (the_var' := the_var);
  generalize2_int the_var';
  unfold the_var'; clear the_var'.

Global Hint Extern 4 (TCEqFunApp ?L (?f ?arg)) =>
  let L' := eval simpl in L in
  match L' with
  | context [arg] => 
    change L with L';
(* fun stuff: generalize also generalizes on the possible use of the variable inside an evar.
   we don't want to do that. So here is generalize2, defined above, which guards all the evars,
   making them behave more like regular variables *)
    generalize2 arg;
    lazymatch goal with
    | |- ∀ x, TCEqFunApp (@?L'' x) _ =>
      unify f L'';
      refine (λ x, TCEqFunApp_refl _); shelve
    end
      (* the shelve is necessary when the current result mentions uninstantiated evars. those may then sometimes
         suddenly be a goal, but they will be unified later. Note usually there are no goals left after refine,
         in which case shelve is a no-op *)
  | _ => let arg_type := type of arg in
        unify f (λ _ : arg_type, L'); refine (TCEqFunApp_refl L')
  end : typeclass_instances.

Global Hint Extern 4 (GatherEvarsEq ?leftpart (@tele_app ?TT _ ?g ?c)) =>
  let rec retcon_tele free_arg :=
    match leftpart with
    | context [?term] => 
      is_evar term; 
      let X := type of term in
      lazymatch X with
      | tele => fail
      | _ => idtac
      end;
      let TT := match type of free_arg with | tele_arg ?TT => TT end in
      let TT1 := open_constr:((_ : tele)) in
      unify TT (TeleS (λ _: X, TT1));
      unify term (free_arg .t1);
      rewrite unfold_tele_app_S;
      retcon_tele (free_arg .t2)
    | _ => 
      let TT := match type of free_arg with | tele_arg ?TT => TT end in
      unify TT TeleO;
      rewrite (unfold_tele_app_O _ free_arg)
    end
  in
  retcon_tele c;
  generalize2 c; (* this leaves g untouched *)
  apply reducing_tforall_forall; cbn; (* simplifies all telescope stuff to regular *)
  done : typeclass_instances.

Section stages.
  Context {PROP : bi}.
  Implicit Types (M : PROP → PROP).

  Class BiAbdUnfoldHypMod {TTl TTr : tele} p P Q M M1 M2 R S := Build_BiAbdUnfoldHyp {
    bi_abd_unfold_hyp : BiAbdMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S
  }.

  Class BiAbdUnfoldGoalMod {TTl TTr : tele} p P Q M M1 M2 R S := Build_BiAbdUnfoldGoal {
    bi_abd_unfold_goal : BiAbdMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S
  }.

  Lemma biabd_from_unfold_hyp {TTl TTr : tele} p P Q M M1 M2 R S :
    BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S →
    BiAbdMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S.
  Proof. by case. Qed.

  Lemma biabdhyp_from_unfold_goal {TTl TTr : tele} p P Q M M1 M2 R S :
    BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S →
    BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S.
  Proof. by case. Qed.

  Lemma biabdhyp_atom_connective_from_unfold_goal {TTl TTr : tele} p P Q M M1 M2 R S :
    AtomAndConnective p P →
    BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S →
    BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S.
  Proof. move => _. apply: biabdhyp_from_unfold_goal. Qed.

  Lemma unfoldhyp_from_base {TTl TTr : tele} p P Q M M1 M2 R S :
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M2 R S →
    SplitModality3 M M1 M2 →
    BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S.
  Proof. intros. by eapply Build_BiAbdUnfoldHyp, Build_BiAbdMod. Qed.

  Lemma unfoldgoal_from_base {TTl TTr : tele} p P Q M M1 M2 R S :
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M2 R S →
    SplitModality3 M M1 M2 →
    BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S.
  Proof. intros. by eapply Build_BiAbdUnfoldGoal, Build_BiAbdMod. Qed.

  Lemma unfoldgoal_from_base_tcand {TTl TTr : tele} p P Q M M1 M2 R S :
    TCAnd (BiAbd (TTl := TTl) (TTr := TTr) p P Q M2 R S) (SplitModality3 M M1 M2) →
    BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S.
  Proof. case. apply unfoldgoal_from_base. Qed.

  Section hyp_steps.
    Lemma biabdhyp_later_mono p P n P' {TTl TTr : tele} Q Q' R S M M' :
      IntoLaterNMax P n P' → (* we can't use IntoLaterN since the n is an input there, not an output. *)
      (TC∀.. tt, FromLaterN (tele_app Q tt) n (tele_app Q' tt)) →
      Inhabited TTr →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) false P' Q' M M' id R S →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M' id (tele_map (bi_laterN n) R) (tele_map (tele_map (bi_laterN n)) S).
    Proof. move => ? ? ? [[? ?]]. eapply unfoldhyp_from_base => //. eapply biabd_later_mono => //. Qed.

    Lemma biabdhyp_sepl P P1 P2 {TTl TTr} Q M M1 M2 S T :
      IntoSepCareful P P1 P2 →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) false P1 Q M M1 M2 S T →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) false P Q M M1 M2 S (tele_map (tele_map (bi_sep P2)) T).
    Proof. move => ? [[? HMs]]. eapply unfoldhyp_from_base => //. eapply biabd_sepl, HMs => //. Qed.
    Lemma biabdhyp_sepr P P1 P2 {TTl TTr} Q M M1 M2 S T :
      IntoSepCareful P P1 P2 →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) false P2 Q M M1 M2 S T →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) false P Q M M1 M2 S (tele_map (tele_map (bi_sep P1)) T).
    Proof. move => ? [[? HMs]]. eapply unfoldhyp_from_base, HMs. eapply biabd_sepr, HMs => //. Qed.

    Lemma biabdhyp_exist' p (P : PROP) `(R : A → PROP) TT2 Q M M1 M2 TTl S T :
      IntoExistCareful2 P R →
      (∀ (a : A), ∃ TTl' S'' T'',
        BiAbdUnfoldHypMod (TTr := TT2) (TTl := TTl') p (R a) Q M M1 M2 S'' T'' ∧
        ∃ (HTTeq : TCEqFunApp TTl' (TTl a)),
        TCAnd (TCEqFunApp (eq_rect TTl' (λ T, T -t> PROP) S'' (TTl a) (eq_from_eq_fun_app HTTeq)) (S a)) $
              (TCEqFunApp (eq_rect TTl' (λ T, T -t> TT2 -t> PROP) T'' (TTl a) (eq_from_eq_fun_app HTTeq)) (T a))
      ) →
      SplitModality3 M M1 M2 → (* incase TT1 is not inhabited, we don't actually get this *)
      BiAbdUnfoldHypMod (TTr := TT2) (TTl := [tele]) p P Q M M1 M2 (∀ a, ∃.. (ttl : TTl a), tele_app (S a) ttl)%I 
          (tele_bind (λ tt2, ∃ a, ∃.. (ttl : TTl a), tele_app (tele_app (T a) ttl) tt2))%I.
    Proof.
      intros.
      eapply unfoldhyp_from_base; last exact H1.
      eapply biabd_exist_cut; [exact H| | eapply H1].
      move => a.
      destruct (H0 a) as (?&?&?& Hbiabd & Heq).
      do 3 eexists; split; last exact Heq. apply bi_abd_unfold_hyp.
    Qed.

    Lemma biabdhyp_forall_postpone p P' `(P : A → PROP) {C : tele} TTr Q M M1 M2 TTl g S T :
      IntoForallCareful P' P →
      (∀ (c : C), ∃ a' TTla S'' T'', (* S'' : TTla -t> PROP, T'' : TTla -t> TTr -t> PROP *)
        BiAbdUnfoldHypMod (TTl := TTla) (TTr := TTr) p (P a') Q M M1 M2 S'' T'' ∧
        TCAnd (GatherEvarsEq a' (tele_app g c)) $ ∃ (HTTeq : TCEq TTla (TTl c)), 
        TCAnd (TCEq (eq_rect TTla (λ T, T -t> PROP) S'' (TTl c) (TCEq_to_eq HTTeq)) (S c)) $ (* S : ∀ c, TTl c -t> PROP *)
        TCEq (eq_rect TTla (λ T, T -t> TTr -t> PROP) T'' (TTl c) (TCEq_to_eq HTTeq)) (T c)
      ) →
      SplitModality3 M M1 M2 →
      BiAbdUnfoldHypMod (TTr := TTr) (TTl := TeleConcatType C (tele_bind TTl)) p P' Q M M1 M2
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (S c)))
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (λ c, tele_app_bind_type_fix (T c))).
    Proof.
      intros. eapply unfoldhyp_from_base => //. eapply biabd_forall_unif => // [a | ]; [ | apply H1].
      specialize (H0 a); move: H0. do 5 (case => ?); intros. 
      do 4 eexists; split => //. by eapply bi_abd_unfold_hyp. 
    Qed.

    Lemma biabdhyp_mod_intro_l p P M M1 M2 M1' M2' Mb P' {TTl TTr} Q S T :
      IntoModal p P M2 P' →
      SplitModality4 M M1 M2 Mb → (* suppose we open invariant. 
            if we are not able to do this, we don't want to open and search if it would give what we want, then later fail anyway.
            so we make a SplitModality3 twice..? that would be too simple unfortunately.
            we keep some Mb which is left over, which P' can consume. But it is import that 
            what is left over is introducable *)
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) false P' Q Mb M1' M2' S T →
      IntroducableModality M1' →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 (M2 ∘ M2') S T.
    Proof. 
      move => ? HMs [[HP HMs']] HM1'. eapply unfoldhyp_from_base.
      - eapply biabd_mod_intro_l => //.
        * eapply HMs. 
        * rewrite /ModalityCompat3 => R //=.
      - destruct HMs as [HMs1 HMs2 HMs3].
        destruct HMs' as [HMs1' HMs2' HMs3'].
        split.
        * rewrite /ModalityCompat3 => R //=.
          rewrite -HMs1.
          apply HMs2, HMs3.
          rewrite -HMs1' -HM1' //=.
        * apply HMs2.
        * tc_solve.
    Qed.

    Lemma biabdhyp_wand p P R V {TTl} {TTr} Q M M1 M2 S T :
      IntoWand2 p P R V →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) false V Q M M1 M2 S T →
      BiAbdUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 (tele_map (flip bi_sep R) S) T.
    Proof. move => ? [[? ?]]. eapply unfoldhyp_from_base => //. eapply biabd_wand => //. Qed.
  End hyp_steps.

  Section goal_steps.
    Lemma biabdgoal_mod_intro_r p P {TTl TTr} φ Q MI Q' `(sel : TTr -t> A) M M1 M2 S T :
      (TC∀.. tt, TCAnd (FromModal (tele_app φ tt) MI (tele_app sel tt) (tele_app Q tt) (tele_app Q' tt)) (SolveSepSideCondition (tele_app φ tt)) ) →
      IntroducableModality MI →
      BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q' M M1 M2 S T →
      BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 S T.
    Proof. move => ? ? [[? HMs]]. eapply unfoldgoal_from_base => //. eapply biabd_mod_intro_r => //. eapply HMs. Qed.

    Lemma biabdgoal_later_intro_r p P {TTl TTr} Q Q' n M M1 M2 S T :
      (TC∀.. tt, FromLaterNMax (tele_app Q tt) n (tele_app Q' tt)) →
      BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q' M M1 M2 S T →
      BiAbdUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 S T.
    Proof.
      rewrite /FromLaterNMax => HQ [[HPQR HM]]. (* can be show from above but causes univers problems..? *)
      eapply unfoldgoal_from_base; last done.
      destruct HM.
      eapply biabd_proper_ent; last (done); try done. 
      - apply tforall_forall => ttr.
        revert HQ => /(dep_eval_tele ttr) <- //.
        apply bi.laterN_intro.
      - split; [done | left; tc_solve].
    Qed.

    Lemma biabdgoal_witness_both_v3 {A : Type} {TTf : A → tele} {C : tele} TTl (g : C -t> A) S T (T' T'' : C -td> tele_bind TTl -c> TeleS TTf -t> PROP)
      P (Q : TeleS TTf -t> PROP) p M M1 M2 :
      (∀ (c : C), ∃ a' TTla S'' T''', 
        BiAbdUnfoldGoalMod (TTl := TTla) (TTr := TTf a') p P (Q a') M M1 M2 S'' T''' ∧
        ∃ (Haceq : GatherEvarsEq a' (tele_app g c)) (HTTeq : TCEq TTla (TTl c)), 
        TCAnd (TCEq (eq_rect TTla (λ T, T -t> PROP) S'' (TTl c) (TCEq_to_eq HTTeq)) (S c)) $ (* S : ∀ c, TTl c -t> PROP *)
        TCEq 
          (eq_rect a' (λ b, TTl c -t> TTf b -t> PROP)
            (eq_rect TTla (λ T, T -t> TTf a' -t> PROP) T''' (TTl c) (TCEq_to_eq HTTeq))
          (tele_app g c) (eq_from_gather_evars_eq Haceq)
        )
        (T c)
      ) →
      SplitModality3 M M1 M2 → (* TCOr inhabited..? *)
      (TC∀.. (c : C), ∀.. ttl : tele_app (tele_bind TTl) c, TransportTeleProp (tele_app g c) (tele_app (T c) $ tele_app_bind_arg_fix ttl) 
        (tele_app (tele_appd_dep T' c) ttl)
      ) →
      (TC∀.. (c : C) (ttl : tele_app (tele_bind TTl) c), TCEq (tele_app (tele_appd_dep T' c) ttl) (tele_app (tele_appd_dep T'' c) ttl)) →
      BiAbdUnfoldGoalMod (TTr := TeleS TTf) (TTl := TeleConcatType C (tele_bind TTl)) p P Q M M1 M2 
          (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (S c)))
          (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ T'').
    Proof.
      intros. eapply unfoldgoal_from_base => //. eapply biabd_goal_exist_unif => //; last apply H0. 
      move => a. specialize (H a); move: H. do 5 (case => ?); intros. 
      do 4 eexists; split => //. eapply bi_abd_unfold_goal.
    Qed.
  End goal_steps.

End stages.


Ltac biabd_step_atom_shortcut := 
  notypeclasses refine (biabdhyp_atom_connective_from_unfold_goal _ _ _ _ _ _ _ _ _ _); [tc_solve | ].

Ltac biabd_step_later_mono := 
  notypeclasses refine (biabdhyp_later_mono _ _ _ _ _ _ _ _ _ _ _ _ _ _); [tc_solve..| ].

Ltac biabd_step_sepl := 
  notypeclasses refine (biabdhyp_sepl _ _ _ _ _ _ _ _ _ _ _); [tc_solve| ].

Ltac biabd_step_sepr :=
  notypeclasses refine (biabdhyp_sepr _ _ _ _ _ _ _ _ _ _ _); [tc_solve| ].

Ltac biabd_step_sep P := 
  (* TODO: this could be improved, by generating the proof once, then passing it to both *)
  assert_succeeds (assert (∃ P1 P2, IntoSepCareful P P1 P2) as _ by (eexists; eexists; tc_solve)); 
    ((biabd_step_sepl) + (biabd_step_sepr)).

Ltac biabd_step_hyp_exist cont := 
  notypeclasses refine (biabdhyp_exist' _ _ _ _ _ _ _ _ _ _ _ _ _ _); [tc_solve | 
      intros?; do 3 eexists; split; [ cont | simple notypeclasses refine (ex_intro _ _ _); last cbn beta ] | ].

Ltac biabd_step_hyp_mod cont :=
  notypeclasses refine (biabdhyp_mod_intro_l _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _); [tc_solve | tc_solve | cont | ].

Ltac biabd_step_wand :=
  notypeclasses refine (biabdhyp_wand _ _ _ _ _ _ _ _ _ _ _ _); [tc_solve| ].

Ltac biabd_step_forall cont :=
  notypeclasses refine (biabdhyp_forall_postpone _ _ _ _ _ _ _ _ _ _ _ _ _ _ _); [ tc_solve |
    intros ?; do 4 eexists; split; [ cont | split; [ | simple notypeclasses refine (ex_intro _ _ _); last cbn beta ] ] | ].

Ltac biabd_step_from_goal p P :=
  assert_fails (assert (AtomAndConnective p P) as _ by (tc_solve));
    simple eapply biabdhyp_from_unfold_goal.

Ltac biabd_step_hyp_naive p P cont :=
  (biabd_step_atom_shortcut; cont) + (
  tryif biabd_step_later_mono then cont else (* ensures ⊳ (P ∗ Q) is not also tried as ⊳P ∗ ⊳ Q *)
  tryif biabd_step_sep P then cont else
  (biabd_step_hyp_exist cont) || (*TODO rewrite this so that the single tactics like <- make progress/don't skip if a hint could not be found *)
  (biabd_step_hyp_mod cont) ||
  (biabd_step_wand; cont) ||
  (biabd_step_forall cont) ||
  (biabd_step_from_goal p P; cont)
  ).

Ltac connective_as_atom_shortcut := fail.
(* this keeps open the option of seeing, for example, all wands also as atoms.
  Redefine as:
Ltac connective_as_atom_shortcut ::= biabd_step_atom_shortcut.
  to find more hints
*)

Ltac biabd_step_hyp p P cont :=
  (connective_as_atom_shortcut; cont) +
  lazymatch P with
       (* we have tryifs here to support some tricky Laterable stuff. These ensure we don't quit too early when 
          looking for hints of the shape  BiAbd _ (▷ P) Q _ _ _  -> so no matching later in goal *)
  | bi_later _ => tryif biabd_step_later_mono then cont else (biabd_step_from_goal p P; cont)
  | bi_laterN _ _ => tryif biabd_step_later_mono then cont else (biabd_step_from_goal p P; cont)
  | bi_sep _ _ => biabd_step_sep P; cont
  | bi_exist _ => biabd_step_hyp_exist cont
  | fupd _ _ _ => biabd_step_hyp_mod cont
  | bupd _ => biabd_step_hyp_mod cont
  | bi_wand _ _ => biabd_step_wand; cont
  | bi_forall _ => biabd_step_forall cont
  | _ => biabd_step_hyp_naive p P cont
  end.

Ltac biabd_step_goal_mod := 
  notypeclasses refine (biabdgoal_later_intro_r _ _ _ _ _ _ _ _ _ _ _ _); [tc_solve | ].

Ltac biabd_step_goal_exist cont :=
  notypeclasses refine (biabdgoal_witness_both_v3 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _);
  [ intros; do 4 (notypeclasses refine (ex_intro _ _ _)); split; 
    [ cont
    | simple notypeclasses refine (ex_intro _ _ _); 
      [ 
      | simpl; simple notypeclasses refine (ex_intro _ _ _); last simpl ] ] 
  | idtac..].

Ltac diaframe_hint_search := tc_solve.

Ltac biabd_step_goal cont :=
  (solve [eapply unfoldgoal_from_base_tcand; diaframe_hint_search]) || (
    tryif biabd_step_goal_mod then cont else
    biabd_step_goal_exist cont
  ).

Ltac pre_step := idtac.

Ltac biabd_step_with cont :=
  lazymatch goal with
  | |- BiAbdUnfoldHypMod ?p ?P _ _ _ _ _ _ => biabd_step_hyp p P cont
  | |- BiAbdUnfoldGoalMod _ _ _ _ _ _ _ _ => biabd_step_goal cont
  end.

Ltac biabd_step_test := biabd_step_with ltac:(idtac).

Ltac find_biabd_steps :=
  let cont := ltac:(pre_step; find_biabd_steps) in 
  biabd_step_with cont.
  (* if you want to inspect the goal after applying all recursive rules, use
    cont := ltac:(pre_step; try find_biabd_steps) *)
  (* this CPS version ensures we don't start (possibly costly) TC search until we are sure all recursive rules have been applied. 
     Note that find_biabd_steps intentionally FAILS if it cannot find a hint. 
      this means it fails before initiating TC search, as desired. If it does succeed, it only leaves sideconditions for TC search
  *)

Ltac find_biabd_initial_step := 
  notypeclasses refine (biabd_from_unfold_hyp _ _ _ _ _ _ _ _ _).

(* fail succeeds only on empty goals :) *)
Ltac find_biabd := find_biabd_initial_step; find_biabd_steps; solve [tc_solve].

Global Hint Extern 4 (BiAbdMod _ ?P _ ?M _ _ _ _) =>
  find_biabd : typeclass_instances.















  