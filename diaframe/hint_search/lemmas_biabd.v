From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes tele_utils solve_defs.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file proves the recursive rules for constructing biabduction hints sound. *)

Set Universe Polymorphism.

Section lemmas.
  Context {PROP : bi}.
  Implicit Types P : PROP.
  Implicit Types M : PROP → PROP.

  Lemma biabd_goal_exist {A : Type} {C : tele} {TTf : A → tele} TTl (g : C -t> A) S T T' P (Q : TeleS TTf -t> PROP) p M :
    (* the telescope C should be seen as the collection of all unsolved variables/evars *)
    (∀ (c : C), BiAbd (TTl := tele_app TTl c) (TTr := TTf $ tele_app g c) p P (Q $ tele_app g c) M (tele_appd_dep (tele_uncurry S) c) (T c)) →
    (∀.. (c : C) (ttl : tele_app TTl c), 
      TransportTeleProp (tele_app g c) (tele_app (T c) ttl) (tele_app (tele_appd_dep (tele_uncurry T') c) ttl)
    ) →
    ModalityMono M →
    BiAbd (TTr := TeleS TTf) (TTl := TeleConcatType C TTl) p P Q M S T'.
  Proof.
    move => HPQS HT HM.
    rewrite /BiAbd.
    apply tforall_forall => ml.
    rewrite (tele_arg_concat_inv_eq ml).
    destruct (tele_arg_concat_inv_strong_hd ml) as [tt1 tt2].
    rewrite tele_curry_uncurry_relation.
    revert HPQS => /(dep_eval tt1) /(dep_eval_tele tt2) ->.

    apply HM, bi_texist_elim => tt.
    rewrite bi_texist_exist -(bi.exist_intro $ TargS (tele_app g tt1) tt) /=.
    apply bi.sep_mono_r.
    rewrite tele_curry_uncurry_relation.

    revert HT => /(dep_eval_tele tt1) /(dep_eval_tele tt2) ->.
    rewrite -(bi.exist_intro eq_refl) => //=.
  Qed.

  Lemma biabd_witness `{TTf : A → tele} {TTl : tele}
    P (Q : TeleS TTf -t> PROP) (a : A) p R S M S' :
    BiAbd (TTr := TTf a) (TTl := TTl) p P (Q a) M R S →
    ModalityMono M →
    (∀.. ttl : TTl, TransportTeleProp a (tele_app S ttl) (tele_app S' ttl)) →
    BiAbd (TTr := TeleS TTf) (TTl := TTl) p P Q M R S'.
  Proof. (* easy to show directly, but somewhat subtle to show that it follows from biabd_goal_exist by setting C to the empty telescope *)
    intros.
    eapply (biabd_goal_exist (C := [tele]) _ _ _ (elim_dep_teleo_fun _)); last done.
    - apply tforall_forall => /=. cbn. done.
    - cbn. exact H1.
  Qed.

  Lemma biabd_witness_postpone {A : Type} {TTf TTl : A → tele}
    P (Q : TeleS TTf -t> PROP) p R S M S' :
    (∀ a : A, BiAbd (TTr := TTf a) (TTl := TTl a) p P (Q a) M (R a) (S a)) →
    ModalityMono M →
    (∀ a, ∀.. ttl : TTl a, TransportTeleProp a (tele_app (S a) ttl) (tele_app S' (TargS a ttl))) →
    BiAbd (TTr := TeleS TTf) (TTl := TeleS TTl) p P Q M R S'.
  Proof.
    intros.
    unshelve eapply (biabd_goal_exist (C := [tele_pair A])); last done.
    - simpl. exact id.
    - move => ta. destruct ta as [a []] => /=. exact (S a).
    - case => a [] /=. apply H.
    - simpl => a. apply H1.
  Qed.

  Lemma biabd_exist p (P : PROP) TT1 R TT2 Q M TTl S S' T T':
    IntoTExistExact P TT1 R →
    (∀.. tt1, BiAbd (TTr := TT2) (TTl := tele_app TTl tt1) p (tele_app R tt1) Q M (tele_appd_dep S tt1) (tele_appd_dep T tt1)) →
    (∀.. tt1, S' ⊢ bi_texist $ tele_app $ tele_appd_dep S tt1) →
    (* S' := ∀.. tt1 : TT1, ∃.. ttl : tele_app TTl tt1, tele_app (tele_appd_dep S tt1) ttl *)
    (∀.. (tt : TeleConcatType TT1 TTl), ∀.. tt2, tele_app (tele_app (tele_curry T) tt) tt2 ⊢ tele_app T' tt2) →
    (* T' := λ tt2, ∃.. (tt : TeleConcatType TT1 TTl), tele_app (tele_curry T) tt *)
    ModalityStrongMono M →
    BiAbd (TTr := TT2) (TTl := [tele]) p P Q M S' T'.
  Proof.
    rewrite /IntoTExistExact /IntoTExist /BiAbd /= !bi_texist_exist => HP /tforall_forall HRS /tforall_forall HS /tforall_forall HT HM.
    rewrite HP intuitionistically_if_exist.
    apply wand_elim_l', exist_elim => tt1.
    apply wand_intro_r.
    rewrite (HS tt1).
    apply wand_elim_r', bi_texist_elim => ttl.
    apply wand_intro_l.
    revert HRS => /(dep_eval tt1) /(dep_eval_tele ttl) ->.
    apply HM, bi_texist_mono => tt2.
    specialize (HT (TeleConcatArg tt1 _ ttl)); revert HT.
    rewrite /tele_pointwise => /(dep_eval_tele tt2) <-.
    rewrite tele_curry_uncurry_relation.
    by rewrite -tele_uncurry_curry_compose.
  Qed.

  Lemma biabd_forall {C : tele} p `(P : A → PROP) TTr Q M TTl (g : C -t> A) S T :
    (∀ (c : C), (* as before, C will be the telescope of generalizable evars *)
      BiAbd (TTr := TTr) (TTl := tele_app TTl c) p (P $ tele_app g c) Q M 
          (tele_appd_dep (tele_uncurry S) c) 
          (tele_appd_dep (tele_uncurry T) c)
    ) →
    ModalityMono M →
    BiAbd (TTr := TTr) (TTl := TeleConcatType C TTl) p (∀ a, P a) Q M S T.
  Proof.
    move => HPs HM.
    rewrite /BiAbd /=.
    apply tforall_forall => ml.
    rewrite (tele_arg_concat_inv_eq ml).
    destruct (tele_arg_concat_inv_strong_hd ml) as [tt1 tt2].
    rewrite tele_curry_uncurry_relation.
    rewrite (bi.forall_elim (tele_app g tt1)).
    revert HPs => /(dep_eval tt1) /(dep_eval_tele tt2) ->.

    apply HM, bi_texist_mono => ttr.
    apply sep_mono_r.
    rewrite tele_curry_uncurry_relation //.
  Qed.

  (* above supersedes the case where the BiAbd can only be found for a specific witness, although we do require the extra ModalityMono *)
  Lemma biabd_forall_one p `(P : A → PROP) a TT2 TTl Q M S T :
    BiAbd (TTr := TT2) (TTl := TTl) p (P a) Q M S T →
    ModalityMono M →
    BiAbd (TTr := TT2) (TTl := TTl) p (∀ a, P a) Q M S T.
  Proof.
    intros. eapply (biabd_forall (C := [tele])); last done.
    apply tforall_forall => //=.
  Qed.

  (* above also supersedes the case where precisely the argument is the generalizable evar *)
  Lemma biabd_forall_postpone p `(P : A → PROP) TT2 Q M TTl S T :
    (∀ a, BiAbd (TTr := TT2) (TTl := TTl a) p (P a) Q M (S a) (T a)) →
    ModalityMono M →
    BiAbd (TTr := TT2) (TTl := TeleS TTl) p (∀ a, P a) Q M S T.
  Proof.
    intros.
    eapply (biabd_forall (C := [tele_pair A])); last done.
    apply tforall_forall => /= a.
    apply H.
  Qed.

  (* IntoWand is too aggressive *)
  Lemma biabd_wand p P R V {TTl} {TTr} Q M S T :
    IntoWand2 p P R V →
    BiAbd (TTl := TTl) (TTr := TTr) false V Q M S T →
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M (tele_map (flip bi_sep R) S) T.
  Proof.
    rewrite /BiAbd /IntoWand2 !tforall_forall => /= HPRV + ttl.
    by rewrite HPRV tele_map_app /= sep_comm -sep_assoc wand_elim_r sep_comm => ->.
  Qed.

  (* this is not strong enough: we want to condition here on M2
    if M2 commutes with later, then we want to add a later to S and T *)
  Lemma biabd_mod_intro_r p P {TTl TTr} φ Q MI Q' `(sel : TTr -t> A) M2 S T :
    (∀.. tt, TCAnd (FromModal (tele_app φ tt) MI (tele_app sel tt) (tele_app Q tt) (tele_app Q' tt)) (SolveSepSideCondition (tele_app φ tt))) →
    IntroducableModality MI →
    BiAbd (TTl := TTl) (TTr := TTr) p P Q' M2 S T →
    ModalityStrongMono M2 →
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M2 S T.
  Proof.
    rewrite /BiAbd !tforall_forall => HQ HMI + HM2 ttl.
    move => ->.
    apply HM2, bi_texist_mono => ttr.
    apply sep_mono_l.
    specialize (HQ ttr) as [HQ' Hφ].
    rewrite -HQ' //.
    apply HMI.
  Qed.

  (* for when P = |={⊤∖ ↑N, ⊤}=> P'*)
  Lemma biabd_mod_intro_l p P M1 P' {TTl TTr} Q M2 S T M :
    IntoModal p P M1 P' →
    ModalityStrongMono M1 →
    BiAbd (TTl := TTl) (TTr := TTr) false P' Q M2 S T →
    ModalityCompat3 M M1 M2 → (* this (hopefully) normalizes M, so that we don't have M1 ∘ M2 *)
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M S T.
  Proof.
    rewrite /IntoModal /BiAbd /= !tforall_forall => HPP' HM HPS HM' ttl.
    rewrite HPP' modality_strong_frame_l -HM'.
    by apply HM.
  Qed.

  Lemma biabd_sepl P P1 P2 {TTl TTr} Q M S T :
    IntoSepCareful P P1 P2 →
    BiAbd (TTl := TTl) (TTr := TTr) false P1 Q M S T →
    ModalityStrongMono M →
    BiAbd (TTl := TTl) (TTr := TTr) false P Q M S (tele_map (tele_map (bi_sep P2)) T).
  Proof.
    rewrite /BiAbd /IntoSepCareful !tforall_forall /= => HP + HM ttl.
    rewrite HP sep_comm sep_assoc (sep_comm (tele_app S _)) => ->.
    rewrite modality_strong_frame_l.
    apply HM.
    iDestruct 1 as "[HQT HP]".
    iDestruct "HQT" as (tt) "[HQ HT]".
    iExists tt.
    rewrite !tele_map_app.
    by iFrame.
  Qed.

  Lemma biabd_sepr P P1 P2 {TTl TTr} Q M S T :
    IntoSepCareful P P1 P2 →
    BiAbd (TTl := TTl) (TTr := TTr) false P2 Q M S T →
    ModalityStrongMono M →
    BiAbd (TTl := TTl) (TTr := TTr) false P Q M S (tele_map (tele_map (bi_sep P1)) T).
  Proof.
    intros.
    (apply: biabd_sepl); last first. 
    - exact H0.
    - revert H. rewrite /IntoSepCareful sep_comm => -> //.
  Qed.

  Lemma biabd_later_mono p P n P' {TTl TTr : tele} Q Q' R S :
    IntoLaterNMax P n P' → (* we can't use IntoLaterN since the n is an input there, not an output. *)
    (∀.. tt, FromLaterN (tele_app Q tt) n (tele_app Q' tt)) →
    Inhabited TTr →
    BiAbd (TTl := TTl) (TTr := TTr) false P' Q' id R S →
    BiAbd (TTl := TTl) (TTr := TTr) p P Q id (tele_map (bi_laterN n) R) (tele_map (tele_map (bi_laterN n)) S).
  Proof.
    rewrite /BiAbd /= => HP /tforall_forall HQ HTT.
    rewrite !tforall_forall => + ttl.
    rewrite HP tele_map_app intuitionistically_if_elim -laterN_sep => ->.
    rewrite !bi_texist_exist laterN_exist.
    apply exist_mono => ttr.
    by rewrite -HQ laterN_sep !tele_map_app.
  Qed.

End lemmas.










