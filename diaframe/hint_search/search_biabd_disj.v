From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes tele_utils solve_defs util_instances.
From diaframe.hint_search Require Import lemmas_biabd_disj search_biabd.
From iris.bi Require Import bi telescopes.

Import bi.



Section witness_postpone.
  Context {PROP : bi}.
  Implicit Types (M : PROP → PROP).

  Lemma biabd_disj_forall_unif p P' `(P : A → PROP) {C : tele} TTr Q M TTl (g : C -t> A) S T D :
    IntoForallCareful P' P →
    (∀ (c : C), ∃ a' TTla S'' T'' D'', (* S'' : TTla -t> PROP, T'' : TTla -t> TTr -t> PROP *)
      BiAbdDisj (TTr := TTr) (TTl := TTla) p (P a') Q M S'' T'' D'' ∧
      TCAnd (GatherEvarsEq a' (tele_app g c)) $ ∃ (HTTeq : TCEq TTla (TTl c)), 
        (* yes, a lot of fun, we need eq_rect here *)
      TCAnd (TCEq (eq_rect TTla (λ T, T -t> PROP) S'' (TTl c) (TCEq_to_eq HTTeq)) (S c)) $ (* S : ∀ c, TTl c -t> PROP *)
      TCAnd (TCEq (eq_rect TTla (λ T, T -t> TTr -t> PROP) T'' (TTl c) (TCEq_to_eq HTTeq)) (T c)) $
             TCEq (eq_rect TTla (λ T, T -t> option2 PROP) D'' (TTl c) (TCEq_to_eq HTTeq)) (D c)
    ) →
    ModalityMono M →
    BiAbdDisj (TTr := TTr) (TTl := TeleConcatType C (tele_bind TTl)) p P' Q M 
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (S c)))
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (λ c, tele_app_bind_type_fix (T c)))
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (λ c, tele_app_bind_type_fix (D c))).
  Proof.
    move => HPs HRQ HM.
    eapply (biabd_disj_hyp_forall _ g); [ done | | done].
    move => c. 
    destruct (HRQ c) as [a' [TTla [S'' [T'' [D'' [HPQRS [Hceq [HTTeq [HSEq [HTEq HDEq]]]]]]]]]].
    revert HTTeq HPQRS HSEq HTEq HDEq.
    move => /TCEq_to_eq => HTeq {HRQ}. subst.
    revert Hceq. 
    move => /eq_from_gather_evars_eq Hceq. subst.
    move => /= HPQRS HS HT HD.
    revert HPQRS. rewrite HS HT HD {HS HT HD D'' S'' T''}. (* cant use proper since the telescopes are tele_app_bind rewritten *)

    rewrite /BiAbdDisj !tforall_forall => + tt.
    rewrite -!tele_uncurry_curry_compose !tele_dep_appd_bind !tele_app_bind_fix_eq //.
  Qed.

  Lemma biabd_disj_goal_exist_unif {A : Type} {C : tele} {TTf : A → tele} TTl (g : C -t> A) S T (T' : C -td> tele_bind TTl -c> TeleS TTf -t> PROP)
    P (Q : TeleS TTf -t> PROP) p M D :
    (∀ (c : C), ∃ a' TTla S'' T''' D''', 
      BiAbdDisj (TTl := TTla) (TTr := TTf a') p P (Q a') M S'' T''' D''' ∧
      ∃ (Haceq : GatherEvarsEq a' (tele_app g c)) (HTTeq : TCEq TTla (TTl c)), 
      TCAnd (TCEq (eq_rect TTla (λ T, T -t> PROP) S'' (TTl c) (TCEq_to_eq HTTeq)) (S c)) $ (* S : ∀ c, TTl c -t> PROP *)
      TCAnd (TCEq 
        (eq_rect a' (λ b, TTl c -t> TTf b -t> PROP)
          (eq_rect TTla (λ T, T -t> TTf a' -t> PROP) T''' (TTl c) (TCEq_to_eq HTTeq))
        (tele_app g c) (eq_from_gather_evars_eq Haceq)
        )
        (T c)
      ) $ TCEq 
      (eq_rect TTla (λ T, T -t> option2 PROP) D'''(TTl c) (TCEq_to_eq HTTeq)) (D c)
    ) →
    ModalityMono M → 
    (TC∀.. (c : C) (ttl : tele_app (tele_bind TTl) c), TransportTeleProp (tele_app g c) (tele_app (T c) $ tele_app_bind_arg_fix ttl) 
      (tele_app (tele_appd_dep T' c) ttl)
    ) →
    BiAbdDisj (TTr := TeleS TTf) (TTl := TeleConcatType C (tele_bind TTl)) p P Q M 
        (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (S c)))
        (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ T')
        (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (D c))).
  Proof.
    move => HPs HM HRQ.
    eapply (biabd_disj_goal_exist _ g _ (λ c, tele_app_bind_type_fix (T c))); last done.
    - intros.
      destruct (HPs c) as [a' [TTla [S'' [T''' [D''' [HPQRS [Hceq [HTTeq [HSEq [HTEq HDEq]]]]]]]]]].
      revert HTTeq HPQRS HSEq HTEq HDEq.
      move => /TCEq_to_eq => HTeq. subst.
      revert Hceq. 
      move => /eq_from_gather_evars_eq Hceq. subst.
      move => HPQRS HS HT HD. simpl in HS, HT, HD.
      revert HPQRS.
      rewrite HS HT HD. (* cant use proper since the telescopes are tele_app_bind rewritten *)

      rewrite /BiAbdDisj !tforall_forall => + ttl.
      rewrite -!tele_uncurry_curry_compose !tele_dep_appd_bind !tele_app_bind_fix_eq => -> //.
    - apply tforall_forall => c.
      apply tforall_forall => tx.
      revert HRQ.
      rewrite /TCTForall => /(dep_eval_tele c).
      move=> /(dep_eval_tele $ tx).
      rewrite -tele_uncurry_curry_compose.
      rewrite tele_app_bind_fix_eq //.
  Qed.

  Lemma biabd_disj_hyp_exist_cut p (P : PROP) `(R : A → PROP) TT2 Q M TTl S T D D' :
    IntoExistCareful2 P R →
    (∀ (a : A), ∃ TTl' S'' T'' D'',
      BiAbdDisj (TTr := TT2) (TTl := TTl') p (R a) Q M S'' T'' D'' ∧
      ∃ (HTTeq : TCEqFunApp TTl' (TTl a)),
      TCAnd (TCEqFunApp (eq_rect TTl' (λ T, T -t> PROP) S'' (TTl a) (eq_from_eq_fun_app HTTeq)) (S a)) $
      TCAnd (TCEqFunApp (eq_rect TTl' (λ T, T -t> TT2 -t> PROP) T'' (TTl a) (eq_from_eq_fun_app HTTeq)) (T a)) $
            (TCEqFunApp (eq_rect TTl' (λ T, T -t> option2 PROP) D'' (TTl a) (eq_from_eq_fun_app HTTeq)) (D a))
    ) →
    ModalityStrongMono M →
    TCIf (∀ (a : A), ∀.. (ttl : TTl a),  (TCEq (tele_app (D a) ttl) None2)) (TCEq D' None2)
        (TCEq D' (Some2 $ ∃ a, ∃.. (ttl : TTl a), match tele_app (D a) ttl with Some2 D'' => D'' | None2 => False end)%I) → 
    BiAbdDisj (TTr := TT2) (TTl := [tele]) p P Q M (∀ a, ∃.. (ttl : TTl a), tele_app (S a) ttl)%I 
        (tele_bind (λ tt2, ∃ a, ∃.. (ttl : TTl a), tele_app (tele_app (T a) ttl) tt2))%I
        D'.
  Proof.
    intros.
    eapply biabd_disj_hyp_exist; [done | | done |done].
    move => a.
    revert H0 => /(dep_eval a) [TTl' [S' [T' [D'' [Habd [+ +]]]]]].
    move => /eq_from_eq_fun_app HTTeq. subst. simpl.
    case => <- [<- <-] //.
  Qed.

End witness_postpone.


Section stages.
  Context {PROP : bi}.
  Implicit Types (M : PROP → PROP).

  Class BiAbdDisjUnfoldHypMod {TTl TTr : tele} p P Q M M1 M2 R S D := Build_BiAbdDisjUnfoldHyp {
    bi_abd_disj_unfold_hyp : BiAbdDisjMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D
  }.

  Class BiAbdDisjUnfoldGoalMod {TTl TTr : tele} p P Q M M1 M2 R S D := Build_BiAbdDisjUnfoldGoal {
    bi_abd_disj_unfold_goal : BiAbdDisjMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D
  }.

  Lemma biabddisj_from_unfold_hyp {TTl TTr : tele} p P Q M M1 M2 R S D :
    BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D →
    BiAbdDisjMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D.
  Proof. by case. Qed.

  Lemma biabddisjhyp_from_unfold_goal {TTl TTr : tele} p P Q M M1 M2 R S D :
    BiAbdDisjUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D →
    BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D.
  Proof. by case. Qed.

  Lemma biabddisjhyp_atom_connective_from_unfold_goal {TTl TTr : tele} p P Q M M1 M2 R S D :
    AtomAndConnective p P →
    BiAbdDisjUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D →
    BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D.
  Proof. move => _. apply: biabddisjhyp_from_unfold_goal. Qed.

  Lemma unfolddisjhyp_from_base {TTl TTr : tele} p P Q M M1 M2 R S D :
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 R S D →
    SplitModality3 M M1 M2 →
    BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D.
  Proof. intros. by eapply Build_BiAbdDisjUnfoldHyp, Build_BiAbdDisjMod. Qed.

  Lemma unfolddisjgoal_from_base {TTl TTr : tele} p P Q M M1 M2 R S D :
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 R S D →
    SplitModality3 M M1 M2 →
    BiAbdDisjUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D.
  Proof. intros. by eapply Build_BiAbdDisjUnfoldGoal, Build_BiAbdDisjMod. Qed.

  Lemma unfolddisjgoal_from_base_tcand {TTl TTr : tele} p P Q M M1 M2 R S D :
    TCAnd (BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 R S D) (SplitModality3 M M1 M2) →
    BiAbdDisjUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 R S D.
  Proof. case. apply unfolddisjgoal_from_base. Qed.

  Lemma unfolddisjgoal_from_base_tcand_pers {TTl TTr : tele} p P Q M M1 M2 R S D :
    TCAnd (BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 R S D) (SplitModality3 M M1 M2) →
    BiAbdDisjUnfoldGoalMod (TTl := TTl) (TTr := TTr) true P Q M M1 M2 R S D.
  Proof. move => [H HMs]. apply unfolddisjgoal_from_base; last done. by eapply biabd_disj_persistency_change. Qed.

  Section hyp_steps.
    Lemma biabddisjhyp_later_mono p P n P' {TTl TTr : tele} Q Q' R S M M' D :
      IntoLaterNMax P n P' → (* we can't use IntoLaterN since the n is an input there, not an output. *)
      (TC∀.. tt, FromLaterN (tele_app Q tt) n (tele_app Q' tt)) →
      Inhabited TTr →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) false P' Q' M M' id R S D →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M' id (tele_map (bi_laterN n) R) (tele_map (tele_map (bi_laterN n)) S)
        (tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 $ (▷^n D') end)%I).
    Proof. move => ? ? ? [[? ?]]. eapply unfolddisjhyp_from_base => //. eapply biabd_disj_later_mono => //. Qed.

    Lemma biabddisjhyp_sepl P P1 P2 {TTl TTr} Q M M1 M2 S T D :
      IntoSepCareful P P1 P2 →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) false P1 Q M M1 M2 S T D →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) false P Q M M1 M2 S (tele_map (tele_map (bi_sep P2)) T)
        (tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 (D' ∗ P2) end)%I).
    Proof. move => ? [[? HMs]]. eapply unfolddisjhyp_from_base => //. eapply biabd_disj_hyp_sepl, HMs => //. Qed.
    Lemma biabddisjhyp_sepr P P1 P2 {TTl TTr} Q M M1 M2 S T D :
      IntoSepCareful P P2 P1 →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) false P1 Q M M1 M2 S T D →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) false P Q M M1 M2 S (tele_map (tele_map (bi_sep P2)) T)
        (tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 (D' ∗ P2) end)%I).
    Proof. move => ? [[? HMs]]. eapply unfolddisjhyp_from_base, HMs. eapply biabd_disj_hyp_sepr, HMs => //. Qed.

    Record biabd_disj_hyp_exist_reqs TTl TTr p H G M M1 M2 S T D := PackageBiAbdDisjHypExist {
      hyp_ex_tele : tele@{Quant}; (* Coq 8.19 chokes on this when omitted *)
      hyp_ex_sidecond : hyp_ex_tele -t> PROP;
      hyp_ex_residue : hyp_ex_tele -t> TTr -t> PROP;
      hyp_ex_disjunct : hyp_ex_tele -t> option2 PROP;
      hyp_ex_biabddisj : BiAbdDisjUnfoldHypMod (TTr := TTr) (TTl := hyp_ex_tele) p H G M M1 M2 hyp_ex_sidecond hyp_ex_residue hyp_ex_disjunct;
      hyp_ex_tele_eq : TCEqFunApp hyp_ex_tele TTl;
      hyp_ex_sidecond_eq : TCEqFunApp (eq_rect hyp_ex_tele (λ T, T -t> PROP) hyp_ex_sidecond TTl (eq_from_eq_fun_app hyp_ex_tele_eq)) S;
      hyp_ex_residue_eq : TCEqFunApp (eq_rect hyp_ex_tele (λ T, T -t> TTr -t> PROP) hyp_ex_residue TTl (eq_from_eq_fun_app hyp_ex_tele_eq)) T;
      hyp_ex_disjunct_eq : TCEqFunApp (eq_rect hyp_ex_tele (λ T, T -t> option2 PROP) hyp_ex_disjunct TTl (eq_from_eq_fun_app hyp_ex_tele_eq)) D
    }.

    Lemma biabddisjhyp_exist' p (P : PROP) `(R : A → PROP) TT2 Q M M1 M2 TTl S T D D' :
      IntoExistCareful2 P R →
      (∀ (a : A),
        biabd_disj_hyp_exist_reqs (TTl a) TT2 p (R a) Q M M1 M2 (S a) (T a) (D a)
      ) →
      SplitModality3 M M1 M2 → (* incase TT1 is not inhabited, we don't actually get this *)
      TCIf (∀ (a : A), TC∀.. (ttl : TTl a),  (TCEq (tele_app (D a) ttl) None2)) (TCEq D' None2)
          (TCEq D' (Some2 $ ∃ a, ∃.. (ttl : TTl a), match tele_app (D a) ttl with Some2 D'' => D'' | None2 => False end)%I) → 
                (* the TC∀ above is crucial, if left out, apply _ and typeclasses eauto have different behaviour *)
      BiAbdDisjUnfoldHypMod (TTr := TT2) (TTl := [tele]) p P Q M M1 M2 (∀ a, ∃.. (ttl : TTl a), tele_app (S a) ttl)%I 
          (tele_bind (λ tt2, ∃ a, ∃.. (ttl : TTl a), tele_app (tele_app (T a) ttl) tt2))%I
          D'.
    Proof.
      intros HPR Hpackaged HMs HD.
      eapply unfolddisjhyp_from_base; last exact HMs.
      eapply biabd_disj_hyp_exist_cut; [exact HPR| | eapply HMs | exact HD].
      move => a.
      destruct (Hpackaged a) as [? ? ? ? Hbiabd HTTeq HSeq HTeq HDeq].
      repeat eexists; [ | eassumption..].
      by eapply bi_abd_disj_unfold_hyp.
    Qed.

    Record biabd_disj_hyp_forall_reqs TTl TTr p {A} (H : A → PROP) witness G M M1 M2 S T D := PackageBiAbdDisjHypForall {
      hyp_forall_witness : A;
      hyp_forall_tele : tele@{Quant}; (* Coq 8.19 chokes on this when omitted *)
      hyp_forall_sidecond : hyp_forall_tele -t> PROP;
      hyp_forall_residue : hyp_forall_tele -t> TTr -t> PROP;
      hyp_forall_disjunct : hyp_forall_tele -t> option2 PROP;
      hyp_forall_biabddisj : BiAbdDisjUnfoldHypMod (TTr := TTr) (TTl := hyp_forall_tele) p (H hyp_forall_witness) G M M1 M2 hyp_forall_sidecond hyp_forall_residue hyp_forall_disjunct;
      hyp_forall_witness_eq : GatherEvarsEq hyp_forall_witness witness;
      hyp_forall_tele_eq : TCEq hyp_forall_tele TTl;
      hyp_forall_sidecond_eq : TCEq (eq_rect hyp_forall_tele (λ T, T -t> PROP) hyp_forall_sidecond TTl (TCEq_to_eq hyp_forall_tele_eq)) S;
      hyp_forall_residue_eq : TCEq (eq_rect hyp_forall_tele (λ T, T -t> TTr -t> PROP) hyp_forall_residue TTl (TCEq_to_eq hyp_forall_tele_eq)) T;
      hyp_forall_disjunct_eq : TCEq (eq_rect hyp_forall_tele (λ T, T -t> option2 PROP) hyp_forall_disjunct TTl (TCEq_to_eq hyp_forall_tele_eq)) D
    }.

    Lemma biabddisjhyp_forall_postpone p P' `(P : A → PROP) {C : tele} TTr Q M M1 M2 TTl g S T D :
      IntoForallCareful P' P →
      (∀ (c : C),
        biabd_disj_hyp_forall_reqs (TTl c) TTr p P (tele_app g c) Q M M1 M2 (S c) (T c) (D c)
      ) →
      SplitModality3 M M1 M2 →
      BiAbdDisjUnfoldHypMod (TTr := TTr) (TTl := TeleConcatType C (tele_bind TTl)) p P' Q M M1 M2
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (S c)))
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (λ c, tele_app_bind_type_fix (T c)))
      (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (λ c, tele_app_bind_type_fix (D c))).
    Proof.
      intros HPR Hpackaged HMs. eapply unfolddisjhyp_from_base => //. eapply biabd_disj_forall_unif => // [a | ]; [ | apply HMs].
      destruct (Hpackaged a) as [wit ? ? ? ? Hbiabd Hwiteq HTTeq HSeq HTeq HDeq].
      do 5 eexists. split; [eapply Hbiabd | ].
      repeat eexists; eassumption.
    Qed.

    Lemma biabddisjhyp_mod_intro_l p P M M1 M2 M1' M2' Mb P' {TTl TTr} Q S T D Mc :
      IntoModal p P M2 P' →
      SplitModality4 M M1 M2 Mb → (* suppose we open invariant. 
            if we are not able to do this, we don't want to open and search if it would give what we want, then later fail anyway.
            so we make a SplitModality3 twice..? that would be too simple unfortunately.
            we keep some Mb which is left over, which P' can consume. But it is import that 
            what is left over is introducable *)
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) false P' Q Mb M1' M2' S T D →
      IntroducableModality M1' →
      CombinedModalitySafe M2 M2' Mc →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 Mc S T D.
    Proof. 
      move => ? HMs [[HP HMs']] HM1' HMc. eapply unfolddisjhyp_from_base.
      - eapply biabd_disj_hyp_mod => //.
        * eapply HMs. 
        * rewrite /ModalityCompat3 => R //=. by rewrite HMc.
      - destruct HMs as [HMs1 HMs2 HMs3].
        destruct HMs' as [HMs1' HMs2' HMs3'].
        split.
        * rewrite /ModalityCompat3 => R //=.
          rewrite -HMs1.
          apply HMs2. rewrite HMc. apply HMs3.
          rewrite -HMs1' -HM1' //=.
        * apply HMs2.
        * split.
          + move => V W HVW. rewrite !HMc. by apply HMs3, HMs3'.
          + move => V W. rewrite !HMc. rewrite modality_strong_frame_l. apply HMs3. apply HMs3'.
    Qed.

    Lemma biabddisjhyp_wand p P R V {TTl} {TTr} Q M M1 M2 S T D :
      IntoWand2 p P R V →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) false V Q M M1 M2 S T D →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 (tele_map (flip bi_sep R) S) T D.
    Proof. move => ? [[? ?]]. eapply unfolddisjhyp_from_base => //. eapply biabd_disj_hyp_wand => //. Qed.

    Lemma biabddisjhyp_orl P1 P2 {TTl TTr} p Q M M1 M2 M' S U D :
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P1 Q M M1 M2 S U D →
      CoIntroducable M2 M' →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p (P1 ∨ P2)%I Q M M1 M2 S U 
        (tele_bind (λ ttl, Some2 $ match tele_app D ttl with
          | Some2 D' => D' ∨ □?p P2 ∗ tele_app S ttl ∗ M' emp
          | None2 => □?p P2 ∗ tele_app S ttl ∗ M' emp
          end))%I.
    Proof. move => [[? HMs]] ?. eapply unfolddisjhyp_from_base => //. apply: biabd_disj_hyp_orl => //. apply HMs. Qed.

    Lemma biabddisjhyp_orr P1 P2 {TTl TTr} p Q M M1 M2 M' S U D :
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P1 Q M M1 M2 S U D →
      CoIntroducable M2 M' →
      BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p (P2 ∨ P1)%I Q M M1 M2 S U 
        (tele_bind (λ ttl, Some2 $ match tele_app D ttl with
          | Some2 D' => D' ∨ □?p P2 ∗ tele_app S ttl ∗ M' emp
          | None2 => □?p P2 ∗ tele_app S ttl ∗ M' emp
          end))%I.
    Proof. move => [[? HMs]] ?. eapply unfolddisjhyp_from_base => //. apply: biabd_disj_hyp_orr => //. apply HMs. Qed.
  End hyp_steps.

  Section goal_steps.
    Lemma biabddisjgoal_later_intro_r p P {TTl TTr} Q Q' n M M1 M2 S T D :
      (TC∀.. tt, FromLaterNMax (tele_app Q tt) n (tele_app Q' tt)) →
      BiAbdDisjUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q' M M1 M2 S T D →
      BiAbdDisjUnfoldGoalMod (TTl := TTl) (TTr := TTr) p P Q M M1 M2 S T D.
    Proof.
      move => HQ [[HPQR HM]]. (* can be show from above but causes univers problems..? *)
      eapply unfolddisjgoal_from_base; last done.
      destruct HM.
      eapply biabd_disj_goal_later_intro; (done || tc_solve).
    Qed.

    Record biabd_disj_goal_exist_reqs TTl {A} (TTr : A → tele) p H (G : TeleS (λ a, TTr a) -t> PROP) witness M M1 M2 S T D := PackageBiAbdDisjGoalExist {
      goal_ex_witness : A;
      goal_ex_tele : tele@{Quant}; (* Coq 8.19 chokes on this when omitted *)
      goal_ex_sidecond : goal_ex_tele -t> PROP;
      goal_ex_residue : goal_ex_tele -t> TTr goal_ex_witness -t> PROP;
      goal_ex_disjunct : goal_ex_tele -t> option2 PROP;
      goal_ex_biabddisj : BiAbdDisjUnfoldGoalMod (TTr := TTr goal_ex_witness) (TTl := goal_ex_tele) p H (G goal_ex_witness) M M1 M2 goal_ex_sidecond goal_ex_residue goal_ex_disjunct;
      goal_ex_witness_eq : GatherEvarsEq goal_ex_witness witness;
      goal_ex_tele_eq : TCEq goal_ex_tele TTl;
      goal_ex_sidecond_eq : TCEq (eq_rect goal_ex_tele (λ T, T -t> PROP) goal_ex_sidecond TTl (TCEq_to_eq goal_ex_tele_eq)) S;
      goal_ex_residue_eq : TCEq
        (eq_rect goal_ex_witness (λ b, TTl -t> TTr b -t> PROP)
          (eq_rect goal_ex_tele (λ T, T -t> TTr goal_ex_witness -t> PROP) goal_ex_residue TTl (TCEq_to_eq goal_ex_tele_eq))
          witness
          (eq_from_gather_evars_eq goal_ex_witness_eq)
        )
        T;
      goal_ex_disjunct_eq : TCEq (eq_rect goal_ex_tele (λ T, T -t> option2 PROP) goal_ex_disjunct TTl (TCEq_to_eq goal_ex_tele_eq)) D
    }.

    Lemma biabddisjgoal_witness_both_v3 {A : Type} {TTf : A → tele} {C : tele} TTl (g : C -t> A) S T D (T' : C -td> tele_bind TTl -c> TeleS TTf -t> PROP)
      P (Q : TeleS TTf -t> PROP) p M M1 M2 :
      (∀ (c : C),
        biabd_disj_goal_exist_reqs (TTl c) TTf p P Q (tele_app g c) M M1 M2 (S c) (T c) (D c)
      ) →
      SplitModality3 M M1 M2 → (* TCOr inhabited..? *)
      (TC∀.. (c : C), ∀.. ttl : tele_app (tele_bind TTl) c, TransportTeleProp (tele_app g c) (tele_app (T c) $ tele_app_bind_arg_fix ttl) 
        (tele_app (tele_appd_dep T' c) ttl)
      ) →
      BiAbdDisjUnfoldGoalMod (TTr := TeleS TTf) (TTl := TeleConcatType C (tele_bind TTl)) p P Q M M1 M2 
          (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (S c)))
          (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ T')
          (tele_curry (TT1 := C) (TT2 := tele_bind TTl) $ tele_dep_bind (TT2 := tele_bind TTl) (λ c, tele_app_bind_type_fix (D c))).
    Proof.
      intros Hpackaged HMs HT. eapply unfolddisjgoal_from_base => //. eapply biabd_disj_goal_exist_unif => //; last apply HMs.
      move => a.
      destruct (Hpackaged a) as [wit ? ? ? ? Hbiabd Hwiteq HTTeq HSeq HTeq HDeq].
      do 5 eexists. split; [eapply Hbiabd | ].
      repeat eexists; eassumption.
    Qed.
  End goal_steps.

End stages.





Ltac biabd_disj_step_atom_shortcut :=
  autoapply @biabddisjhyp_atom_connective_from_unfold_goal with typeclass_instances; [tc_solve | ].

Ltac biabd_disj_step_later_mono := 
  autoapply @biabddisjhyp_later_mono with typeclass_instances; [tc_solve..| ].

Ltac biabd_disj_step_sepl := 
  autoapply @biabddisjhyp_sepl with typeclass_instances; [tc_solve| ].

Ltac biabd_disj_step_sepr :=
  autoapply @biabddisjhyp_sepr with typeclass_instances; [tc_solve| ].

Ltac biabd_disj_step_sep P := 
  (* TODO: this could be improved, by generating the proof once, then passing it to both *)
  assert_succeeds (assert (∃ P1 P2, IntoSepCareful P P1 P2) as _ by (eexists; eexists; tc_solve)); 
    ((biabd_disj_step_sepl) + (biabd_disj_step_sepr)).

Ltac biabd_disj_step_orl cont :=
  autoapply @biabddisjhyp_orl with typeclass_instances; [cont | ].

Ltac biabd_disj_step_orr cont :=
  autoapply @biabddisjhyp_orr with typeclass_instances; [cont | ].

Ltac biabd_disj_step_or P cont :=
  assert_succeeds (assert (∃ P1 P2, IntoOr P P1 P2) as _ by (eexists; eexists; tc_solve));
    ((biabd_disj_step_orl cont) + (biabd_disj_step_orr cont)).

Ltac biabd_disj_step_hyp_exist cont :=
  autoapply @biabddisjhyp_exist' with typeclass_instances; [tc_solve |
    intros ?;
    unshelve (autoapply @PackageBiAbdDisjHypExist with typeclass_instances);
    [ shelve | shelve | shelve | shelve | | cont | ..]
  | ..].

Ltac biabd_disj_step_hyp_mod cont :=
  autoapply @biabddisjhyp_mod_intro_l with typeclass_instances; [tc_solve | tc_solve | cont | | ].

Ltac biabd_disj_step_wand :=
  autoapply @biabddisjhyp_wand with typeclass_instances; [tc_solve| ].

Ltac biabd_disj_step_forall cont :=
  autoapply @biabddisjhyp_forall_postpone with typeclass_instances; [ tc_solve |
    intros ?;
    unshelve (autoapply @PackageBiAbdDisjHypForall with typeclass_instances);
    [ shelve | shelve | shelve | shelve | shelve | | cont | ..]
  | ..].

Ltac biabd_disj_step_from_goal p P :=
  assert_fails (assert (AtomAndConnective p P) as _ by (tc_solve));
    autoapply @biabddisjhyp_from_unfold_goal with typeclass_instances.

Ltac biabd_disj_step_hyp_naive p P cont :=
  (biabd_disj_step_atom_shortcut; cont) + (
  tryif biabd_disj_step_later_mono then cont else (* ensures ⊳ (P ∗ Q) is not also tried as ⊳P ∗ ⊳ Q *)
  tryif biabd_disj_step_sep P then cont else
  (biabd_disj_step_or P cont) ||
  (biabd_disj_step_hyp_exist cont) || (*TODO rewrite this so that the single tactics like <- make progress/don't skip if a hint could not be found *)
  (biabd_disj_step_hyp_mod cont) ||
  (biabd_disj_step_wand; cont) ||
  (biabd_disj_step_forall cont) ||
  (biabd_disj_step_from_goal p P; cont)
  ).

Ltac connective_as_atom_shortcut := fail.
(* this keeps open the option of seeing, for example, all wands also as atoms.
  Redefine as:
Ltac connective_as_atom_shortcut ::= biabd_disj_step_atom_shortcut.
  to find more hints
*)

Ltac biabd_disj_step_hyp p P cont :=
  (connective_as_atom_shortcut; cont) +
  lazymatch P with
       (* we have tryifs here to support some tricky Laterable stuff. These ensure we don't quit too early when 
          looking for hints of the shape  BiAbd _ (▷ P) Q _ _ _  -> so no matching later in goal *)
  | bi_later _ => tryif biabd_disj_step_later_mono then cont else (biabd_disj_step_from_goal p P; cont)
  | bi_laterN _ _ => tryif biabd_disj_step_later_mono then cont else (biabd_disj_step_from_goal p P; cont)
  | bi_sep _ _ => biabd_disj_step_sep P; cont
  | bi_or _ _ => biabd_disj_step_or P cont
  | bi_exist _ => biabd_disj_step_hyp_exist cont
  | fupd _ _ _ => biabd_disj_step_hyp_mod cont
  | bupd _ => biabd_disj_step_hyp_mod cont
  | bi_wand _ _ => biabd_disj_step_wand; cont
  | bi_forall _ => biabd_disj_step_forall cont
  | _ => biabd_disj_step_hyp_naive p P cont
  end.

Ltac biabd_disj_step_goal_mod := 
  autoapply @biabddisjgoal_later_intro_r with typeclass_instances; [tc_solve | ].

Ltac biabd_disj_step_goal_exist cont :=
  autoapply @biabddisjgoal_witness_both_v3 with typeclass_instances;
  [ intros ?;
    unshelve (autoapply @PackageBiAbdDisjGoalExist with typeclass_instances);
    [ shelve | shelve | shelve | shelve | shelve | | | cont | ..]
  | ..].

Ltac diaframe_hint_search := tc_solve.

Ltac biabd_disj_step_goal_from_base p :=
  lazymatch p with
  | false => (solve [autoapply @unfolddisjgoal_from_base_tcand with typeclass_instances; diaframe_hint_search])
  | true => (solve [autoapply @unfolddisjgoal_from_base_tcand_pers with typeclass_instances; diaframe_hint_search])
  end.

Ltac biabd_disj_step_goal p cont :=
  biabd_disj_step_goal_from_base p || (
    tryif biabd_disj_step_goal_mod then cont else
    biabd_disj_step_goal_exist cont
  ).

Ltac pre_step := idtac.

Ltac biabd_disj_step_with cont :=
  lazymatch goal with
  | |- BiAbdDisjUnfoldHypMod ?p ?P _ _ _ _ _ _ _ => biabd_disj_step_hyp p P cont
  | |- BiAbdDisjUnfoldGoalMod ?p _ _ _ _ _ _ _ _ => biabd_disj_step_goal p cont
  end.

Ltac biabd_disj_step_test := biabd_disj_step_with ltac:(idtac).

Ltac find_biabd_disj_steps :=
  let cont := ltac:(pre_step; find_biabd_disj_steps) in 
  biabd_disj_step_with cont.
  (* if you want to inspect the goal after applying all recursive rules, use
    cont := ltac:(pre_step; try find_biabd_disj_steps) *)
  (* this CPS version ensures we don't start (possibly costly) TC search until we are sure all recursive rules have been applied. 
     Note that find_biabd_disj_steps intentionally FAILS if it cannot find a hint. 
      this means it fails before initiating TC search, as desired. If it does succeed, it only leaves sideconditions for TC search
  *)

Ltac find_biabd_disj_initial_step := 
  autoapply @biabddisj_from_unfold_hyp with typeclass_instances.

(* fail succeeds only on empty goals :) *)
Ltac find_biabd_disj := find_biabd_disj_initial_step; find_biabd_disj_steps; solve [tc_solve].

(*
Global Hint Extern 4 (BiAbdDisjMod _ ?P _ ?M _ _ _ _ _) =>
  find_biabd_disj : typeclass_instances.
*)


