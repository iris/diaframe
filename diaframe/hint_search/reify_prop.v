From diaframe Require Export utils_ltac2 util_classes tele_utils.
From iris.bi Require Import bi telescopes.


Ltac2 Type bi_bin_op := [ BiBSep | BiBOr ].

Ltac2 Type bi_un_op := [
  | BiUWand (constr)
  | BiUMod (constr)
  | BiULater (constr)
].

Ltac2 Type bi_quant_op := [ 
  | BiQForall
  | BiQExist
].

Ltac2 Type rec bi_prop := [
  | AtomProp (constr, bi_prop option)
  | BinProp (bi_bin_op, bi_prop, bi_prop)
  | UnProp (bi_un_op, bi_prop)
  | QuantProp (bi_quant_op, ident option, constr, bi_prop)
].


Ltac2 uncast (c : constr) : constr :=
  match Constr.Unsafe.kind c with
  | Constr.Unsafe.Cast c' _ _ =>  c'
  | _ => c
  end.

(* If we directly pass on unified evars,
  _really_ subtle bugs can appear. This is because backtracking
  will undo evar unification! so those constrs get turned back to evars,
  and these evars might not have been created at all in your current proof state. *)
Ltac2 unevarify (e : constr) : constr :=
  let e' := Constr.Unsafe.kind e in
  Constr.Unsafe.make e'.

Ltac2 rec iter_prod (binders : binder list) (t : constr) :=
  match binders with
  | [] => t
  | binder :: binders' =>
    Constr.Unsafe.make (Constr.Unsafe.Prod binder (iter_prod binders' t))
  end.

Ltac2 rec tc_query_under_binders_int
  (binders : binder list) (* outermost binders first *)
  (allbinders : binder list)
  (rem_binders : int)
  (binder_depth : int)
  (quantify : constr -> constr) (* how to quantify the outer binders *)
  (querygen : (constr -> constr) -> constr) (* take an 'evar of type generator' to build the query *)
  (postprocess : (constr -> constr) option)
  (* result is well-typed in outerscope, but we usually want something usable in our inner scope *)
  : constr list :=
  match binders with
  | [] =>
    let generated_evars := { contents := [] } in
    let gen_evar (t : constr) := 
      let evar_type := iter_prod allbinders t in
      (* if we do not type this, Coq might get terribly confused and throw nasty errors *)
      let result := open_constr:((_ : $evar_type)) in
      generated_evars.(contents) := result :: generated_evars.(contents); 
        (* later calls come are first! *)
      result
    in
    let the_query := quantify (querygen gen_evar) in
    let _ := constr:(ltac:(tc_solve) : $the_query) in
    let postprocess' := Option.default (fun c => c) postprocess in
    (* entries are reversed to match user given order *)
    List.map postprocess' (
    List.map unevarify (
    List.map uncast (List.rev (generated_evars.(contents)))))
  | binder :: binders2 =>
    tc_query_under_binders_int binders2 allbinders (Int.sub rem_binders 1) (Int.add binder_depth 1)
      (fun c =>
        quantify (Constr.Unsafe.make (Constr.Unsafe.Prod binder c))
      )
      (fun cgen =>
        querygen (fun u => 
          safe_apply (cgen u) [Constr.Unsafe.make (Constr.Unsafe.Rel binder_depth)]
        )
      )
      (Option.map (fun p => 
        (fun c => safe_apply (p c) [Constr.Unsafe.make (Constr.Unsafe.Rel rem_binders)])
      ) postprocess)
  end.

Ltac2 rec get_deepest_lambda_body (c : constr) : int * constr :=
  match Constr.Unsafe.kind c with
  | Constr.Unsafe.Lambda bind body =>
    let (n, result) := get_deepest_lambda_body body in
    (Int.add n 1, result)
  | Constr.Unsafe.Cast c _ _ =>
    get_deepest_lambda_body c
  | _ =>
    (0, c)
  end.

Ltac2 safe_eval_beta (c : constr) : constr :=
  match Constr.Unsafe.kind c with
  | Constr.Unsafe.App f args =>
    let (n, body) := get_deepest_lambda_body f in
(*     printf "deep lambda body: %t" body;
    Array.iteri (fun i c' => printf "arg: %i %t" i c') args; *)
    safe_apply (Constr.Unsafe.substnl (List.rev (List.firstn n (Array.to_list args))) 0 body)
      (List.skipn n (Array.to_list args))
  | _ => c
  end.

Ltac2 tc_query_under_binders
  (binders : binder list) (* innermost binders first *)
  (querygen : (constr -> constr) -> constr) (* take an 'evar generator' to build the query *)
  (inner_scoped : bool) (* whether return values should live in inner or outer scope *)
  : constr list :=
  let result := tc_query_under_binders_int (List.rev binders) (List.rev binders) (List.length binders) 1 (fun c => c) querygen (
    if inner_scoped then Some (fun c => c) else None
  ) in
  if inner_scoped then
(*     List.iter (fun c' => printf "result pre-safe-beta: %t" c') result; *)
    List.map safe_eval_beta result
  else
    result.


Inductive IntoStrongModal {PROP : bi} (P : PROP) (M : PROP → PROP) (Pout : PROP) :=
  into_strong_modal : IntoModal false P M Pout → ModalityStrongMono M → IntoStrongModal P M Pout.
Existing Class IntoStrongModal.
Global Existing Instance into_strong_modal.

Global Hint Mode IntoStrongModal + ! - - : typeclass_instances.


Ltac2 get_constant_for_constr (u : constr) :=
  match Constr.Unsafe.kind u with
  | Constr.Unsafe.Constant const _ => const
  | _ => Control.throw Not_found
  end.


Local Ltac2 Notation "red_flags" s(strategy) := s.

Ltac2 bi_texist_tforall_flags (_ : unit) := { (red_flags beta match fix zeta) with
  Std.rConst := List.map (fun a => Std.ConstRef a) (List.map get_constant_for_constr ['(@bi_texist); '(@tele_fold); '(@tele_bind); '(@bi_tforall); '(@tele_app)])
}.

Ltac2 maybe_simplify_bi_texist_forall (prop : constr) (under_binders : bool) : constr :=
  if under_binders then
    (* we could do something better (like with Constr.is_closed) once we drop support for Coq 8.16 *)
    prop
  else
    lazy_match! prop with
    | context [bi_texist] =>
      (* Std.eval_cbn may fail horribly if the term features UNBOUND_REL/raw de bruijn indices *)
       Std.eval_cbn (bi_texist_tforall_flags ()) prop
    | context [bi_tforall] =>
       Std.eval_cbn (bi_texist_tforall_flags ()) prop
    | _ => prop
    end.

Ltac2 is_empty (ls : 'a list) : bool :=
  match ls with
  | [] => true
  | _ => false
  end.


Ltac2 rec reify_prop_int (c : constr) (bs : binder list) (bi : constr) : bi_prop :=
  let atom_reify (atom : constr) : bi_prop :=
    plus_once (fun _ =>
      let connective := List.nth (tc_query_under_binders bs (fun egen => safe_apply '@AtomIntoConnective [bi; atom; egen '(bi_car $bi)]) true) 0 in
      AtomProp (unevarify atom) (Some (reify_prop_int (maybe_simplify_bi_texist_forall connective (Bool.neg (is_empty bs))) bs bi))
    ) (fun _ =>
      AtomProp (unevarify atom) None
    )
  in
  match Constr.Unsafe.kind c with
  | Constr.Unsafe.Var name =>
    atom_reify c
  | Constr.Unsafe.App f args =>
    let default (_ : unit) := 
      lazy_match! f with
      | @bi_forall _ => 
        let thetype := (Array.get args 0) in
        let thelambda := (Array.get args 1) in
        match Constr.Unsafe.kind thelambda with
        | Constr.Unsafe.Lambda bind_fa body_fa => 
          QuantProp BiQForall (Constr.Binder.name bind_fa) (unevarify thetype) (reify_prop_int body_fa (bind_fa :: bs) bi)
        | _ => Control.throw Not_found
        end
      | @bi_exist _ =>
        let thetype := (Array.get args 0) in
        let thelambda := (Array.get args 1) in
        match Constr.Unsafe.kind thelambda with
        | Constr.Unsafe.Lambda bind_ex body_ex => 
          QuantProp BiQExist (Constr.Binder.name bind_ex) (unevarify thetype) (reify_prop_int body_ex (bind_ex :: bs) bi)
        | _ => Control.throw Not_found
        end
      | @bi_sep _ =>
        BinProp BiBSep (reify_prop_int (Array.get args 0) bs bi) (reify_prop_int (Array.get args 1) bs bi)
      | @bi_or _ =>
        BinProp BiBOr (reify_prop_int (Array.get args 0) bs bi) (reify_prop_int (Array.get args 1) bs bi)
      | @bi_wand _ =>
        UnProp (BiUWand (Array.get args 0)) (reify_prop_int (Array.get args 1) bs bi)
      | @bi_later _ =>
        UnProp (BiULater '(1%nat)) (reify_prop_int (Array.get args 0) bs bi)
      | @bi_laterN =>
        (* not a projection, so no implicit argument, I think *)
        UnProp (BiULater (Array.get args 1)) (reify_prop_int (Array.get args 2) bs bi)
      | _ =>
        (* 1. check if function is modality
           2. unfold head, check if that returns anything non-trivial
           3. otherwise, return atom_reify *)
        let cont := plus_once (fun _ =>
          let as_modal_list := tc_query_under_binders bs (fun egen =>

            let modality_raw := egen '(bi_car $bi → bi_car $bi) in
            let modalprop_raw := egen '(bi_car $bi) in
            safe_apply '@IntoStrongModal [bi; c; modality_raw; modalprop_raw]

          ) true in
          let modality := List.nth as_modal_list 0 in
          let modalprop := maybe_simplify_bi_texist_forall (List.nth as_modal_list 1) (Bool.neg (is_empty bs)) in
          (fun _ => UnProp (BiUMod modality) (reify_prop_int modalprop bs bi))
        ) (fun _ =>
          match Constr.is_const f with
          | false => (fun _ => atom_reify c)
          | true =>
            plus_once (fun _ =>
              let uf := tc_unfold_in f c in
              (fun _ =>
                let result := reify_prop_int uf bs bi in
                match result with
                | AtomProp ac mp =>
                  match mp with
                  | None => (* unfolding was useless *)
                    atom_reify c
                  | Some p' => 
                    AtomProp c mp
                  end
                | _ => result
                end
              )
            ) (fun _ _ => atom_reify c)
          end
        ) in
        cont ()
      end
    in
    match Constr.Unsafe.kind f with
    | Constr.Unsafe.Lambda bind body =>
      (* do beta substitution.. *)
      reify_prop_int (safe_eval_beta c) bs bi
    | _ => default ()
    end
  | Constr.Unsafe.Constant _ _ =>
    (* try to unfold c, if possible. *)
    let cont := plus_once (fun _ =>
      let uprop := tc_unfold_const c in
      (fun _ =>
        let result := reify_prop_int uprop bs bi in
        match result with
        | AtomProp ac mp =>
          match mp with
          | None => (* unfolding was useless *)
            atom_reify c
          | Some _ => result
          end
        | _ => result
        end
      )
    ) (fun _ => 
      (fun _ => atom_reify c)
    ) in
    cont ()
  | _ =>
    atom_reify c
  end.


Ltac2 get_prop_type (prop : constr) : constr :=
  let get_prop_type_fun := constr:(λ {PROP : bi} (P : PROP), PROP) in
  Std.eval_cbn (red_flags beta) '($get_prop_type_fun _ $prop).


Ltac2 reify_prop (c : constr) : bi_prop :=
  reify_prop_int (maybe_simplify_bi_texist_forall c false) [] (get_prop_type c).


Ltac2 bi_bin_op_to_string (bo : bi_bin_op) : message :=
  match bo with
  | BiBSep => fprintf "∗"
  | BiBOr => fprintf "∨"
  end.

Ltac2 bi_un_op_to_string (print_atoms : bool) (uo : bi_un_op) : message :=
  match uo with
  | BiUWand c => if print_atoms then fprintf "%t -∗" c else fprintf "? -∗"
  | BiUMod c => fprintf "‖ %t ‖ " c
  | BiULater c => fprintf "▷^%t " c
  end.

Ltac2 bi_quant_op_to_string (qo : bi_quant_op) (bindname : ident option) (bindtype : constr) : message :=
  match qo with
  | BiQForall => fprintf "∀ (? : %t) " bindtype
  | BiQExist => fprintf "∃ (? : %t) " bindtype
  end.


Ltac2 rec bi_prop_to_string (print_atoms : bool) (p : bi_prop) : message :=
  match p with
  | AtomProp c mp => 
    match mp with
    | None =>
      if print_atoms then fprintf "%t" c else fprintf "Atom"
    | Some p' =>
      if print_atoms then 
        fprintf "%t (%a)" c (fun () => bi_prop_to_string print_atoms) p'
      else
        fprintf "Atom (%a)" (fun () => bi_prop_to_string print_atoms) p'
    end
  | BinProp bo l r =>
     fprintf "%a  %a  %a" 
        (fun () => bi_prop_to_string print_atoms) l
        (fun () => bi_bin_op_to_string) bo
        (fun () => bi_prop_to_string print_atoms) r
  | UnProp uo p' =>
     fprintf "%a  %a" 
        (fun () => bi_un_op_to_string print_atoms) uo
        (fun () => bi_prop_to_string print_atoms) p'
  | QuantProp qo bn bt p' =>
     fprintf "%a  %a" 
        (fun () () => bi_quant_op_to_string qo bn bt) ()
        (fun () => bi_prop_to_string print_atoms) p'
  end.




