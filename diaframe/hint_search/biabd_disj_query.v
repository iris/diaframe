From diaframe.hint_search Require Export reify_prop query_reified_prop lemmas_biabd_disj.
From diaframe Require Import solve_defs.
From iris.bi Require Import bi.
From stdpp Require Import telescopes.


(* Typeclasses for applying goal rules *)
Class ExBiAbdDisjW {PROP : bi} {TTr : tele} (p : bool) (P : PROP) (Q : TTr -t> PROP) : Prop := {
  ex_biabd_disj_w_unfold : True
}.

Global Instance ex_biabd_disj_w_spat {PROP : bi} {TTr : tele} (P : PROP) (Q : TTr -t> PROP) M TTl R S D :
  BiAbdDisj (TTl := TTl) false P Q M R S D →
  ExBiAbdDisjW false P Q | 0.
Proof. by constructor. Qed.

Global Instance ex_biabd_disj_w_pers {PROP : bi} {TTr : tele} (p : bool) (P : PROP) (Q : TTr -t> PROP) M TTl R S D :
  BiAbdDisj (TTl := TTl) p P Q M R S D →
  ExBiAbdDisjW true P Q | 0.
Proof. by constructor. Qed.

Global Instance ex_biabd_disj_w_wit {PROP : bi} `{TTf : A → tele} (p : bool) (P : PROP) (Q : TeleS TTf -t> PROP) a :
  ExBiAbdDisjW p P (Q a) → ExBiAbdDisjW p P Q | 1.
Proof. by constructor. Qed.

Class ExBiAbdDisj {PROP : bi} {TTr : tele} (p : bool) (P : PROP) (Q : TTr -t> PROP) : Prop := {
  ex_biabd_disj_unfold : True
}.

Global Instance ex_biabd_disj_direct_spat {PROP : bi} {TTr : tele} (P : PROP) (Q : TTr -t> PROP) M TTl R S D :
  BiAbdDisj (TTl := TTl) false P Q M R S D →
  ExBiAbdDisj false P Q | 0.
Proof. by constructor. Qed.

Global Instance ex_biabd_disj_direct_pers {PROP : bi} {TTr : tele} (p : bool) (P : PROP) (Q : TTr -t> PROP) M TTl R S D :
  BiAbdDisj (TTl := TTl) p P Q M R S D →
  ExBiAbdDisj true P Q | 0.
Proof. by constructor. Qed.

Global Instance ex_biabd_disj_from_later {PROP : bi} {TTr : tele} (p : bool) (P : PROP) (Q Q' : TTr -t> PROP) n :
  (TC∀.. tt, FromLaterNMax (tele_app Q tt) n (tele_app Q' tt)) →
  ExBiAbdDisj p P Q' →
  ExBiAbdDisj p P Q | 10.
Proof. by constructor. Qed.

Global Instance ex_biabd_disj_from_exist {PROP : bi} `{TTf : A → tele} (p : bool) (P : PROP) (Q : TeleS TTf -t> PROP) a :
  ExBiAbdDisjW p P (Q a) →
  ExBiAbdDisj p P Q | 20.
Proof. by constructor. Qed.


Ltac2 Type ModBeforeLaterStages := [ NoMod | FoundMod | FoundModBeforeLater ].

Ltac2 find_mod_before_later (path : path_el list) : bool :=
  let result := fold_left (fun mod_stage pathel => 
      match mod_stage with 
      | FoundModBeforeLater => FoundModBeforeLater
      | FoundMod => 
        match pathel with
        | PathElLater => FoundModBeforeLater 
        | _ => FoundMod 
        end
      | NoMod => 
        match pathel with 
        | PathElMod _ => FoundMod 
        | _ => NoMod 
        end
      end) NoMod path in
  match result with
  | FoundModBeforeLater => true
  | _ => false
  end.

Ltac2 mutable biabd_path_checker (path : path_el list) : bool :=
  Bool.neg (find_mod_before_later path).

Ltac2 gen_pers_bool (pers_bool : constr) : (path_el list) -> constr :=
  lazy_match! pers_bool with
  | false => (fun _ => pers_bool)
  | true => fun path =>
    if List.exist (fun e =>
      match e with
      | PathElWand => true
      | PathElMod _ => true
      | PathElEnterAtom => true
      | _ => false
      end
    ) path then 'false else 'true
  end.

Ltac2 biabd_disj_query_on (bi : constr) (pers_bool : constr) (p : bi_prop) (ttr : constr) (goal : constr) : path_el list :=
  let bool_gen := gen_pers_bool pers_bool in
  query_bi_prop p biabd_path_checker (fun p a =>
    safe_apply '@ExBiAbdDisj [bi; ttr; bool_gen p; a; goal]
  ) tc_solve.

Ltac2 biabd_disj_reify_n_query_on (pers_bool : constr) (p : constr) (ttr : constr) (goal : constr) : path_el list :=
  let bi := get_prop_type p in
  let rp := reify_prop p in
  biabd_disj_query_on bi pers_bool rp ttr goal.













