From diaframe.hint_search Require Import search_biabd instances_base.
From diaframe Require Import solve_defs.
From iris.proofmode Require Import ltac_tactics.


(* This file, when imported, functionally removes all BiAbd hints.
  By explicitly registering hints with Hint Resolve (as below),
  one can add hints one by one. This makes it possible to count the number of 
  distinct hints used by a particular file. *)

Create HintDb biabd_custom discriminated.

Ltac iSolveTCH hintdb :=
  solve [once (typeclasses eauto with hintdb)].

Tactic Notation "iSolveTCH" "with" ident(arg) := iSolveTCH arg.


Ltac diaframe_hint_search ::= iSolveTCH with biabd_custom.


Global Hint Extern 4 =>
  lazymatch goal with
  | |- BiAbd _ _ _ _ _ _ => fail
  | |- Abduct _ _ _ _ _ => fail
  | |- _ => tc_solve
  end : biabd_custom.

Global Hint Resolve TCAnd_intro : biabd_custom.
Global Hint Resolve instances_base.bi_abd_from_pers_assumption | 49 : biabd_custom.
Global Hint Resolve instances_base.bi_abd_from_spat_assumption | 50 : biabd_custom.
Global Hint Resolve instances_base.bi_abd_empty_goal_from_emp | 50 : biabd_custom.
Global Hint Resolve instances_base.abd_from_biabd: biabd_custom.

(* To enable symbolic execution, one also needs: 

Global Hint Resolve weakestpre.abduct_from_execution : biabd_custom.
Global Hint Resolve weakestpre.collect_modal_wp_value : biabd_custom.
*)
