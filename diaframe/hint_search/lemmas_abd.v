From iris.proofmode Require Import base coq_tactics reduction tactics classes_make.
From diaframe Require Import util_classes tele_utils solve_defs.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file proves the recursive rules for constructing abduction hints sound. *)

Set Universe Polymorphism.

Section lemmas.
  Context {PROP : bi}.
  Implicit Types P : PROP.
  Implicit Types M : PROP → PROP.

  Lemma abd_mod_input_from_mod_rec p P {TTr} Q M M1 S :
    AbductModRec p P Q M M1 S →
    ModalityStrongMono M1 →
    AbductModInput TTr p P Q M (M1 S). 
  (* [AbductModInput] is what a [SolveOne] goal will look for. 
    These are always constructed from [AbductModRec], for which we will build recursive rules *)
  Proof.
    rewrite /AbductModRec /AbductModInput /Abduct => <- HM.
    rewrite modality_strong_frame_r. apply HM. 
    rewrite comm //.
  Qed.

  Lemma abduct_mod_rec_from_abduct p P {TTr} Q M M1 M2 S :
    Abduct (TT := TTr) p P Q M2 S →
    ModalityCompat3 M M1 M2 →
    ModalityMono M1 →
    AbductModRec (TT := TTr) p P Q M M1 S.
  (* The base instance for [AbductModRec] *)
  Proof.
    rewrite /AbductModRec /Abduct => HPS <- HM.
    by apply HM.
  Qed.

  Lemma abduct_mod_rec_from_biabd p P {TTl TTr} Q M M' Mr M1 M2 S T : 
    (* can take into account that it's bad to throw away resources at mask change *)
    SplitLeftModality3 M M' Mr →
    BiAbd (TTl := TTl) (TTr := TTr) p P Q M2 S T →
    SplitModality3 M' M1 M2 →
    AbductModRec (TT := TTr) p P Q M M1 (∃.. ttl, tele_app S ttl ∗ (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mr emp))%I.
  Proof.
    rewrite /BiAbd /AbductModRec /TCTForall => [[HMs HM' HMr]] /tforall_forall HPS [HMs' HM1m HM2m].
    rewrite -HMs -HMs'. apply HM1m.
    apply bi.wand_elim_r', bi_texist_elim => ttl.
    apply bi.wand_intro_l. rewrite assoc HPS modality_strong_frame_l. apply HM2m.
    apply bi.wand_elim_l', bi_texist_elim => ttr.
    apply bi.wand_intro_r.
    rewrite -assoc (bi_tforall_elim ttr) wand_elim_r modality_strong_frame_r.
    apply HMr.
    by rewrite left_id bi_texist_exist -(exist_intro _).
  Qed.

  Lemma abduct_mod_rec_from_biabd_disj p P {TTl TTr} Q M M' Mr M1 M2 S T D Mc : 
    (* can take into account that it's bad to throw away resources at mask change *)
    SplitLeftModality3 M M' Mr →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 S T D →
    SplitModality3 M' M1 M2 →
    StripSaved Mr Mc →
    AbductModRec (TT := TTr) p P Q M M1 (∃.. ttl, tele_app S ttl ∗
        match tele_app D ttl with
        | None2 => (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mc emp)
        | Some2 D' => (∀.. ttr, tele_app (tele_app T ttl) ttr -∗ Mc emp) ∧ (D' -∗ Mc $ ∃.. ttr, tele_app Q ttr)
        end)%I.
  Proof.
    rewrite /BiAbdDisj /AbductModRec /TCTForall => [[HMs HM' HMr]] /tforall_forall HPS [HMs' HM1m HM2m] HMc.
    rewrite -HMs -HMs'. apply HM1m.
    apply bi.wand_elim_r', bi_texist_elim => ttl.
    apply bi.wand_intro_l. rewrite assoc HPS modality_strong_frame_l. apply HM2m.
    apply bi.wand_elim_l', or_elim.
    - apply bi_texist_elim => ttr. destruct (tele_app D ttl); first rewrite and_elim_l;
      apply bi.wand_intro_r;
      rewrite -assoc (bi_tforall_elim ttr) wand_elim_r -HMc modality_strong_frame_r;
      apply HMr;
      by rewrite left_id bi_texist_exist -(exist_intro _).
    - destruct (tele_app D ttl) as [D' | ]; last apply pure_elim' => [[]].
      rewrite and_elim_r. apply bi.wand_intro_r. by rewrite wand_elim_r -HMc.
  Qed.

  (* useful lemmas that are not recursive instances *)
  Lemma abd_modrec_introducable_swap p P {TTr} Q M M1 M2 Mh S :
    ModalityCompat3 M M1 M2 →
    AbductModRec (TT := TTr) p P Q M2 Mh S →
    ModalityStrongMono M1 →
    IntroducableModality Mh →
    AbductModRec (TT := TTr) p P Q M M1 S.
  Proof.
    rewrite /AbductModRec /= => <- HPQ HM1 HMh.
    apply HM1. rewrite -HPQ -HMh //. 
  Qed.

  Lemma abd_modrec_id_swap p P {TTr} Q M M1 M2 S :
    ModalityCompat3 M M1 M2 →
    AbductModRec (TT := TTr) p P Q M2 id S →
    ModalityStrongMono M1 →
    AbductModRec (TT := TTr) p P Q M M1 S.
  Proof.
    intros. eapply abd_modrec_introducable_swap => //.
    by intros ?.
  Qed.

  Section abduct_rules.
    (* To give an idea of the available recursive rules, we first prove some recursive rules for regular [Abduct]s. *)

    (* this may look weird, how will we know C? We can actually infer C after finding the inner [Abduct] *)
    Lemma abd_goal_exist {A : Type} {C : tele} {TTf : A → tele} (g : C -t> A) M R P (Q : TeleS TTf -t> PROP) p :
      (∀ (c : C), (* C is the telescope containing all generalizable evars *)
        Abduct (TT := TTf $ tele_app g c) p P (Q $ tele_app g c) M (tele_app R c)
      ) →
      ModalityMono M →
      Abduct (TT := TeleS TTf) p P Q M (∃.. c, tele_app R c).
    Proof.
      rewrite /Abduct => HPR HM.
      apply bi.wand_elim_r', bi_texist_elim => c.
      apply bi.wand_intro_l.
      rewrite HPR.
      apply HM, bi_texist_elim => att.
      rewrite bi_texist_exist.
      rewrite -(bi.exist_intro $ TargS (tele_app g c) att) //.
    Qed.

    (* the following two rules (which one would want) follow from [abd_goal_exist] *)
    Lemma abd_witness `{TTf : A → tele}
      P (Q : TeleS TTf -t> PROP) (a : A) p R M :
      Abduct (TT := TTf a) p P (Q a) M R →
      ModalityMono M →
      Abduct (TT := TeleS TTf) p P Q M R.
    Proof.
      intros. eapply (abd_goal_exist (C := [tele])); last done.
      apply tforall_forall => /=. exact H.
    Qed.
    Lemma abd_witness_postpone {A : Type} {TTf : A → tele}
      P (Q : TeleS TTf -t> PROP) p (R : A → PROP) M :
      (∀ a : A, Abduct (TT := TTf a) p P (Q a) M (R a)) →
      ModalityMono M →
      Abduct (TT := TeleS TTf) p P Q M (∃ a, R a)%I.
    Proof.
      intros. eapply (abd_goal_exist (C := [tele_pair A])); last done.
      apply tforall_forall => /=. exact H.
    Qed.

    Lemma abd_exist p (P : PROP) TT1 R TT2 Q M S :
      IntoTExistExact P TT1 R →
      (∀.. tt1, Abduct (TT := TT2) p (tele_app R tt1) Q M (tele_app S tt1)) →
      Abduct (TT := TT2) p P Q M (∀.. tt1, tele_app S tt1)%I.
    Proof.
      rewrite /Abduct /IntoTExistExact /IntoTExist /= !bi_texist_exist => -> /tforall_forall HRS.
      rewrite intuitionistically_if_exist.
      apply wand_elim_l', exist_elim => tt1.
      apply wand_intro_r.
      rewrite !bi_tforall_forall (bi.forall_elim tt1). apply HRS.
    Qed.

    (* We do the same trick as in [abd_goal_exist] *)
    Lemma abd_forall {C : tele} p `(P : A → PROP) TT Q M R (g : C -t> A) :
      (∀ (c : C), (* C is the telescope containing all generalizable evars *)
        Abduct (TT := TT) p (P $ tele_app g c) Q M (tele_app R c)
      ) →
      Abduct (TT := TT) p (∀ a, P a) Q M (∃.. c, tele_app R c).
    Proof.
      rewrite /Abduct => HP.
      apply bi.wand_elim_r', bi_texist_elim => c.
      apply bi.wand_intro_l.
      rewrite (bi.forall_elim $ tele_app g c) HP //.
    Qed.

    (* With these two consequences: *)
    Lemma abd_forall_one p `(P : A → PROP) a' TT2 Q M S:
      Abduct (TT := TT2) p (P a') Q M S →
      Abduct (TT := TT2) p (∀ a, P a) Q M S.
    Proof. 
      intros. eapply (abd_forall (C := [tele])).
      apply tforall_forall => /=. exact H.
    Qed.
    Lemma abd_forall_postpone p {A} (P S : A → PROP) TTl Q M:
      (∀ a, Abduct (TT := TTl) p (P a) Q M (S a)) →
      Abduct (TT := TTl) p (∀ a, P a) Q M (∃ a, S a)%I.
    Proof.
      intros. eapply (abd_forall (C := [tele_pair A])).
      apply tforall_forall => /=. exact H.
    Qed.

    (* IntoWand is too aggressive *)
    Lemma abduct_wand p P R V {TTr} Q M S RS:
      IntoWand2 p P R V → (* P ⊢ R -∗ V *)
      Abduct (TT := TTr) false V Q M S →
      MakeSep R S RS → (* this order guarantees that end goal is in grammar, as long as R ∈ L *)
      (* additionally, by using MakeSep we get rid of unsolvable (hardgoal ∗ emp) *)
      Abduct (TT := TTr) p P Q M RS. 
    Proof.
      rewrite /Abduct /IntoWand2 /IntoSep /MakeSep => -> /= HVS <-.
      rewrite sep_assoc wand_elim_l HVS //.
    Qed.

    (* this is not strong enough: we want to condition here on M2
      if M2 commutes with later, then we want to add a later to S
       note that is also not enough: consider hypothesis (A -∗ B) for goal ▷ B
       suggestion above would give us new goal ((▷ emp) ∗ A). 
      [AbductModRec] partially solves this problem *)
    Lemma abd_mod_intro_r p P {TTr} φ Q MI Q' `(sel : TTr -t> A) M2 S :
      (∀.. tt, TCAnd (FromModal (tele_app φ tt) MI (tele_app sel tt) (tele_app Q tt) (tele_app Q' tt)) (SolveSepSideCondition (tele_app φ tt))) →
      IntroducableModality MI →
      Abduct (TT := TTr) p P Q' M2 S →
      ModalityStrongMono M2 →
      Abduct (TT := TTr) p P Q M2 S.
    Proof.
      rewrite /Abduct !tforall_forall => HQ HMI -> HM2.
      apply HM2, bi_texist_mono => ttr.
      specialize (HQ ttr) as [HQ' Hφ].
      rewrite -HQ' //.
      apply HMI.
    Qed.

    (* for when P = |={⊤∖ ↑N, ⊤}=> P'*)
    Lemma abd_mod_intro_l p P M1 P' {TTr} Q M2 S M :
      IntoModal p P M1 P' →
      ModalityStrongMono M1 →
      Abduct (TT := TTr) false P' Q M2 S →
      ModalityCompat3 M M1 M2 → 
      Abduct (TT := TTr) p P Q M S.
    Proof.
      rewrite /IntoModal /Abduct /= => -> HM1 HPS HM.
      rewrite modality_strong_frame_l -HM.
      by apply HM1.
    Qed.

    Lemma abduct_sepl' P P1 P2 {TTr} Q M S :
      IntoSepCareful P P1 P2 →
      Abduct (TT := TTr) false P1 Q M S →
      Abduct (TT := TTr) false P Q M (P2 -∗ S).
    Proof.
      rewrite /Abduct /IntoSepCareful /= => -> <-.
      iIntros "[[$ HP2] HS]".
      by iApply "HS".
    Qed.

    Lemma abduct_sepr' P P1 P2 {TTr} Q M S :
      IntoSepCareful P P1 P2 →
      Abduct (TT := TTr) false P2 Q M S →
      Abduct (TT := TTr) false P Q M (P1 -∗ S).
    Proof.
      rewrite /Abduct /IntoSepCareful /= => -> <-.
      iIntros "[[HP1 $] HS]".
      by iApply "HS".
    Qed.

    Lemma abduct_orl P P1 P2 {TTr} Q M S :
      IntoOr P P1 P2 →
      Abduct (TT := TTr) false P1 Q M S →
      Abduct (TT := TTr) false P Q M (S ∧ (P2 -∗ M (∃.. tt, tele_app Q tt)))%I.
    Proof.
      rewrite /IntoOr /Abduct /= => -> HPS.
      apply bi.wand_elim_l', or_elim; apply bi.wand_intro_r.
      - by rewrite and_elim_l.
      - rewrite and_elim_r wand_elim_r //.
    Qed.

    Lemma abd_later_mono p P n P' {TTr : tele} Q Q' R :
      IntoLaterNMax P n P' → (* we can't use IntoLaterN since the n is an input there, not an output. *)
      (∀.. tt, FromLaterN (tele_app Q tt) n (tele_app Q' tt)) →
      Inhabited TTr →
      Abduct (TT := TTr) false P' Q' id R →
      Abduct (TT := TTr) p P Q id (▷^n R)%I.
    Proof.
      rewrite /Abduct /IntoLaterNMax /= => HPP' /tforall_forall HQ HTT.
      rewrite HPP' intuitionistically_if_elim -laterN_sep => ->.
      rewrite !bi_texist_exist laterN_exist.
      apply exist_mono => ttr.
      rewrite -HQ //.
    Qed.

    Lemma abd_persistency_change p P {TTr : tele} Q M R :
      Abduct (TT := TTr) p P Q M R →
      Abduct (TT := TTr) true P Q M R.
    Proof.
      rewrite /Abduct => <- /=.
      destruct p => /=; first done.
      by rewrite bi.intuitionistically_elim.
    Qed.
  End abduct_rules.

  Lemma abd_rec_mod_goal_exist {A : Type} {C : tele} {TTf : A → tele} (g : C -t> A) M M1 M2 Mh R P (Q : TeleS TTf -t> PROP) p :
    SplitLeftModality3 M M1 M2 →
    (∀ (c : C), (* C is the telescope containing all generalizable evars *)
      AbductModRec (TT := TTf $ tele_app g c) p P (Q $ tele_app g c) M2 Mh (tele_app R c)
    ) →
    IntroducableModality Mh → (* see [abd_rec_mod_forall'] below for something that may be a bit more careful with Mh *)
    AbductModRec (TT := TeleS TTf) p P Q M M1 (∃.. c, tele_app R c).
  Proof.
    rewrite /AbductModRec => [[HMs HM1 HM2]] HPR HMh. rewrite -HMs.
    apply HM1.
    apply bi.wand_elim_r', bi_texist_elim => c.
    apply bi.wand_intro_l. erewrite (HMh (□?p P ∗ tele_app R c)%I).
    rewrite HPR.
    apply HM2, bi_texist_elim => att.
    rewrite bi_texist_exist.
    rewrite -(bi.exist_intro $ TargS (tele_app g c) att) //.
  Qed.

  Lemma abd_rec_mod_exist p (P : PROP) TT1 R TT2 Q M M1 S :
    IntoTExistExact P TT1 R →
    (∀.. tt1, AbductModRec (TT := TT2) p (tele_app R tt1) Q M M1 (tele_app S tt1)) →
    ModalityStrongMono M1 →
    AbductModRec (TT := TT2) p P Q M id (∀.. tt1 : TT1, M1 (tele_app S tt1))%I.
  Proof.
    rewrite /AbductModRec /IntoTExistExact /IntoTExist /= !bi_texist_exist => -> /tforall_forall HRS HM1.
    rewrite intuitionistically_if_exist.
    apply wand_elim_l', exist_elim => tt1.
    apply wand_intro_r.
    rewrite !bi_tforall_forall (bi.forall_elim tt1).
    rewrite modality_strong_frame_r. rewrite -HRS. apply HM1.
    rewrite comm //.
  Qed.

  Lemma abd_rec_mod_forall {C : tele} p `(P : A → PROP) TT Q M M1 M2 Mh R (g : C -t> A) :
    SplitLeftModality3 M M1 M2 →
    (∀ (c : C), (* C is the telescope containing all generalizable evars *)
      AbductModRec (TT := TT) p (P $ tele_app g c) Q M2 Mh (tele_app R c)
    ) →
    IntroducableModality Mh →
    AbductModRec (TT := TT) p (∀ a, P a) Q M M1 (∃.. c, tele_app R c).
  Proof.
    rewrite /AbductModRec => [[<- HM1 HM2]] HPR HMh.
    apply HM1.
    apply bi.wand_elim_r', bi_texist_elim => c.
    apply bi.wand_intro_l.
    rewrite (bi.forall_elim $ tele_app g c).
    rewrite -(HPR c). apply HMh.
  Qed.

  Lemma abd_rec_mod_forall' {C : tele} p `(P : A → PROP) TT Q M M1 M2 Mh R (g : C -t> A) :
    SplitLeftModality3 M M1 M2 →
    (∀ (c : C), (* C is the telescope containing all generalizable evars *)
      AbductModRec (TT := TT) p (P $ tele_app g c) Q M2 Mh (tele_app R c)
    ) →
    ModalityStrongMono Mh → (* ▷ satisfies this, and shóuld be put in front of existentials, 
      while fancy updates cán't be put in front. To do this properly, we need a typeclass
      to determine commutation rules with existentials. For (C := [tele]), everything commutes.
      Later also commutes with other C. fupd needs to be introduced for other C, 
      which is only possible if masks are unifiable. *)
    AbductModRec (TT := TT) p (∀ a, P a) Q M M1 (∃.. c, Mh $ tele_app R c).
  Proof.
    rewrite /AbductModRec => [[<- HM1 HM2]] HPR HMh.
    apply HM1.
    apply bi.wand_elim_r', bi_texist_elim => c.
    apply bi.wand_intro_l.
    rewrite (bi.forall_elim $ tele_app g c).
    rewrite -(HPR c).
    rewrite modality_strong_frame_r. apply HMh.
    rewrite comm //.
  Qed.

  Lemma abd_rec_mod_wand p P R V {TTr} Q M M1 S RS:
    IntoWand2 p P R V → (* P ⊢ R -∗ V *)
    AbductModRec (TT := TTr) false V Q M M1 S →
    MakeSep R S RS → (* this order guarantees that end goal is in grammar, as long as R ∈ L *)
    (* additionally, by using MakeSep we get rid of unsolvable (hardgoal ∗ emp) *)
    ModalityMono M1 →
    AbductModRec (TT := TTr) p P Q M M1 RS. 
  Proof.
    rewrite /AbductModRec /Abduct /IntoWand2 /IntoSep /MakeSep => HPRV <- /= HRS HM.
    apply HM. rewrite HPRV -HRS.
    rewrite sep_assoc wand_elim_l //.
  Qed.

  Lemma abd_mod_later_r p P {TTr : tele} Q Q' n M M1 S :
    (TC∀.. tt, FromLaterNMax (tele_app Q tt) n (tele_app Q' tt)) →
    Inhabited TTr → (* or if TTr is trivial.. *)
    ModalityMono M →
    AbductModRec (TT := TTr) p P Q' (M ∘ bi_laterN n) M1 S →
    AbductModRec (TT := TTr) p P Q M M1 S.
  Proof.
    rewrite /AbductModRec => /tforall_forall HQQ' HTTr HM ->.
    apply HM. rewrite !bi_texist_exist. rewrite bi.laterN_exist.
    apply bi.exist_mono => a. rewrite HQQ' //.
  Qed.

  (* for when P = |={⊤∖ ↑N, ⊤}=> P'*)
  Lemma abd_rec_mod_mod_intro_l p P M1 P' {TTr} Q M2 S M Mh :
    IntoModal p P M1 P' →
    ModalityCompat3 M M1 M2 → 
    ModalityStrongMono M1 →
    AbductModRec (TT := TTr) false P' Q M2 Mh S →
    ModalityStrongMono Mh →
    AbductModRec (TT := TTr) p P Q M id (Mh S).
  Proof.
    rewrite /IntoModal /AbductModRec /= => -> HMs HM1 HPS HMh.
    rewrite modality_strong_frame_l -HMs.
    apply HM1. rewrite -HPS. rewrite modality_strong_frame_r. apply HMh.
    rewrite comm //.
  Qed.

  Lemma abd_rec_mod_sepl P P1 P2 {TTr} Q M Mh S :
    IntoSepCareful P P1 P2 →
    AbductModRec (TT := TTr) false P1 Q M Mh S →
    ModalityStrongMono Mh →
    AbductModRec (TT := TTr) false P Q M id (P2 -∗ Mh S).
  Proof.
    rewrite /AbductModRec /IntoSepCareful /= => -> <- HMh.
    rewrite -sep_assoc wand_elim_r.
    rewrite modality_strong_frame_r. apply HMh. rewrite comm //.
  Qed.

  Lemma abd_rec_mod_sepr P P1 P2 {TTr} Q M Mh S :
    IntoSepCareful P P1 P2 →
    AbductModRec (TT := TTr) false P2 Q M Mh S →
    ModalityStrongMono Mh →
    AbductModRec (TT := TTr) false P Q M id (P1 -∗ Mh S).
  Proof.
    intros. eapply abd_rec_mod_sepl; [ | done..].
    rewrite /IntoSepCareful. rewrite H comm //.
  Qed.

  Lemma abd_rec_mod_orl P1 P2 {TTr} Q M Mh S Q' Mc :
    AbductModRec (TT := TTr) false P1 Q M Mh S →
    ModalityStrongMono Mh →
    PrepareForRetry Q Q' →
    ModalityMono M →
    StripSaved M Mc →
    AbductModRec (TT := TTr) false (P1 ∨ P2)%I Q M id ((Mh S) ∧ (P2 -∗ <affine> ⌜TC∀.. ttr, IsEasyGoal (tele_app Q ttr)⌝ -∗ Mc $ ∃.. ttr, tele_app Q' ttr))%I.
  Proof.
    rewrite /IntoOr /AbductModRec /= => HPS HMh HQ HM HMc.
    apply bi.wand_elim_l', or_elim; apply bi.wand_intro_r.
    - rewrite and_elim_l modality_strong_frame_r -HPS. apply HMh. by rewrite comm.
    - rewrite and_elim_r wand_elim_r. iIntros "HM".
      iSpecialize ("HM" with "[]"); last iStopProof. 
      { iIntros "!>". iPureIntro. apply tforall_forall => ttr. exact I. }
      rewrite -HMc.
      apply HM, bi_texist_mono => ttr. apply HQ.
  Qed.

  Lemma abd_rec_mod_orr P1 P2 {TTr} Q M Mh S Q' Mc :
    AbductModRec (TT := TTr) false P2 Q M Mh S →
    ModalityStrongMono Mh →
    PrepareForRetry Q Q' →
    ModalityMono M →
    StripSaved M Mc →
    AbductModRec (TT := TTr) false (P1 ∨ P2)%I Q M id ((Mh S) ∧ (P1 -∗ <affine> ⌜TC∀.. ttr, IsEasyGoal (tele_app Q ttr)⌝ -∗ Mc $ ∃.. ttr, tele_app Q' ttr))%I.
  Proof.
    rewrite /IntoOr /AbductModRec /= => HPS HMh HQ HM HMc.
    apply bi.wand_elim_l', or_elim; apply bi.wand_intro_r.
    - rewrite and_elim_r wand_elim_r. iIntros "HM".
      iSpecialize ("HM" with "[]"); last iStopProof. 
      { iIntros "!>". iPureIntro. apply tforall_forall => ttr. exact I. }
      rewrite -HMc. apply HM, bi_texist_mono => ttr. apply HQ.
    - rewrite and_elim_l modality_strong_frame_r -HPS. apply HMh. by rewrite comm.
  Qed.

  Lemma abd_rec_mod_later_mono p P n P' {TTr : tele} Q Q' R M Mh Mc :
    IntoLaterNMax P n P' → (* we can't use IntoLaterN since the n is an input there, not an output. *)
    (∀.. tt, FromLaterN (tele_app Q tt) n (tele_app Q' tt)) →
    Inhabited TTr →
    ModalityMono M →
    AbductModRec (TT := TTr) p P' Q' id Mh R → (* Mh could theoretically be later! *)
    ModalityStrongMono Mh →
    StripSaved M Mc →
    AbductModRec (TT := TTr) p P Q M Mc (▷^n Mh R)%I.
  Proof.
    rewrite /AbductModRec /IntoLaterNMax /= => HPP' /tforall_forall HQ HTT HM HPQR HMh <-.
    apply HM.
    rewrite !bi_texist_exist. setoid_rewrite <-HQ.
    rewrite -laterN_exist.
    rewrite HPP' laterN_intuitionistically_if_2.
    rewrite -laterN_sep.
    apply laterN_mono.
    rewrite -bi_texist_exist. rewrite -HPQR.
    rewrite modality_strong_frame_r.
    apply HMh. rewrite comm //.
  Qed.

  Lemma abd_rec_mod_goal_foc_disj_ex p P M M1 {TT : tele} Q D R :
    AbductModRec (TT := TT) p P Q M M1 R →
    ModalityMono M →
    AbductModRec (TT := [tele]) p P (FocDisjEx (TT := TT) Q D) M M1 R.
  Proof.
    rewrite /AbductModRec => -> /= HM.
    rewrite FocDisjEx_eq /FocDisjEx_def.
    apply HM, or_intro_l.
  Qed.

  (* this rule should be applied at the start of a derivation for FocDisjEx, essentially
     saving the option of not proving R *)
  Lemma abd_rec_mod_start_foc_disj_ex p P M M1 M2 Mh {TT : tele} Q D R Mc :
    SplitLeftModality3_Save M M1 M2 →
    AbductModRec (TT := [tele]) p P (FocDisjEx (TT := TT) Q D) M2 Mh R →
    IntroducableModality Mh →
    StripSaved M2 Mc →
    AbductModRec (TT := [tele]) p P (FocDisjEx (TT := TT) Q D) M M1 (R ∨ (if p then Mc D else P -∗ Mc D))%I.
  Proof.
    rewrite /AbductModRec /= => [[HMs HM1 HM2]] HPR HMh HMc.
    rewrite -HMs. apply HM1, bi.wand_elim_r', bi.or_elim.
    - apply bi.wand_intro_l. rewrite -HPR. apply HMh.
    - apply bi.wand_intro_l. etrans; last first.
      { apply HM2. rewrite FocDisjEx_eq /FocDisjEx_def. apply bi.or_intro_r. }
      destruct p => /=.
      * by rewrite bi.intuitionistically_elim_emp left_id -HMc.
      * by rewrite bi.wand_elim_r HMc.
  Qed.
End lemmas.










