From diaframe Require Import solve_defs util_classes tele_utils.
From iris.proofmode Require Import proofmode classes.

Import bi.


Section iris_tactic_compatibility.
  Context {PROP : bi}.

  (* Makes iExist work on SolveSep goals *)
  Global Instance solve_sep_into_exist `{TTf : A → tele} M (P Q : TeleS TTf -t> PROP) :
    ModalityStrongMono M →
    FromExist (SolveSep (TT := TeleS TTf) M P Q) (λ a, SolveSep (TT := TTf a) M (P a) (Q a)).
  Proof.
    unseal_diaframe; rewrite /FromExist => HM.
    apply exist_elim => a.
    apply HM.
    rewrite !bi_texist_exist.
    apply exist_elim => tt.
    by rewrite -(exist_intro $ TargS a tt) /=.
  Qed.
  (* Makes iExist work on SolveSepFoc goals *)
  Global Instance solve_sep_foc_into_exist `{TTf : A → tele} M (P Q : TeleS TTf -t> PROP) R :
    ModalityStrongMono M →
    FromExist (SolveSepFoc (TT := TeleS TTf) M P Q R) (λ a, SolveSepFoc  (TT := TTf a) M (P a) (Q a) R).
  Proof.
    unseal_diaframe; rewrite /FromExist => HM.
    apply exist_elim => a.
    apply HM, bi.or_mono; last done.
    rewrite !bi_texist_exist.
    apply exist_elim => tt.
    by rewrite -(exist_intro $ TargS a tt) /=.
  Qed.
  (* Makes iExist work on SolveOne goals *)
  Global Instance solve_one_into_exist `{TTf : A → tele} M (P : TeleS TTf -t> PROP) :
    ModalityStrongMono M →
    FromExist (SolveOne (TT := TeleS TTf) M P) (λ a, SolveOne (TT := TTf a) M (P a)).
  Proof.
    unseal_diaframe; rewrite /FromExist => HM.
    apply exist_elim => a.
    apply HM.
    rewrite !bi_texist_exist.
    apply exist_elim => tt.
    by rewrite -(exist_intro $ TargS a tt) /=.
  Qed.
  (* Makes iExist work on SolveOneFoc goals *)
  Global Instance solve_one_foc_into_exist `{TTf : A → tele} M (P : TeleS TTf -t> PROP) R :
    ModalityStrongMono M →
    FromExist (SolveOneFoc (TT := TeleS TTf) M P R) (λ a, SolveOneFoc (TT := TTf a) M (P a) R).
  Proof.
    unseal_diaframe; rewrite /FromExist => HM.
    apply exist_elim => a.
    apply HM, or_mono; last done.
    rewrite !bi_texist_exist.
    apply exist_elim => tt.
    by rewrite -(exist_intro $ TargS a tt) /=.
  Qed.

  (* Makes iIntros "H" work on IntroduceHyp *)
  Global Instance introduce_hyp_into_wand (Q R : PROP) :
    FromWand (IntroduceHyp Q R) Q R. (* also makes AsSolveGoal work *)
  Proof. unseal_diaframe. tc_solve. Qed.

  (* Makes iIntros (x) work on IntroduceVars *)
  Global Instance introduce_vars_into_forall {A : Type} {b : A → tele} (Q : TeleS b -t> PROP) :
    FromForall (IntroduceVars Q) (λ a : A, IntroduceVars (TT := b a) (Q a)) (λ x, x).
  Proof. unseal_diaframe; tc_solve. Qed.
  (* Makes iSplitL/iSplitR work on SolveSep. *)
  Global Instance solvesep_fromsep (L G : PROP) M M1 M2 MG :
    SplitLeftModality3 M M1 M2 →
    AbsorbModal G M2 MG →
    FromSep (SolveSep (TT := TeleO) M L G) (M1 L) MG | 999. 
  Proof.
    rewrite /FromSep. unseal_diaframe => /= [[HM HM1 HM2]] ->.
    rewrite modality_strong_frame_l -HM. apply HM1.
    rewrite modality_strong_frame_r. apply HM2. by rewrite comm.
  Qed.

  (* Makes iLeft/iRight work on SolveSep *)
  Lemma solve_sep_first_from_or {TT : tele} (M : PROP → PROP) P P1 P2 R :
    (TC∀.. tt : TT, FromOr (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ModalityMono M →
    FromOr (SolveSep (TT := TT) M P R) (SolveSep (TT := TT) M P1 R) (SolveSep (TT := TT) M P2 R).
  Proof.
    unseal_diaframe; rewrite /FromOr => /tforall_forall HPs HM.
    apply bi.or_elim; apply HM; apply bi_texist_mono => tt.
    all: apply sep_mono_l; rewrite -HPs.
    - rewrite -bi.or_intro_l //.
    - rewrite -bi.or_intro_r //.
  Qed.
  (* Makes iLeft/iRight work on SolveOne *)
  Lemma solve_one_from_or {TT : tele} (M : PROP → PROP) P P1 P2 :
    (TC∀.. tt : TT, FromOr (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ModalityMono M →
    FromOr (SolveOne (TT := TT) M P) (SolveOne (TT := TT) M P1) (SolveOne (TT := TT) M P2).
  Proof.
    unseal_diaframe; rewrite /FromOr => /tforall_forall HPs HM.
    apply bi.or_elim; apply HM; apply bi_texist_mono => tt.
    all: rewrite -HPs.
    - rewrite -bi.or_intro_l //.
    - rewrite -bi.or_intro_r //.
  Qed.
  (* Makes iLeft/iRight work on SolveSepFoc *)
  Lemma solve_sep_foc_from_or {TT : tele} (M : PROP → PROP) L Q D :
    ModalityMono M →
    FromOr (SolveSepFoc (TT := TT) M L Q D) (SolveSep M L Q) (M D).
  Proof. unseal_diaframe; rewrite /FromOr => HM. apply or_elim; apply HM; eauto. Qed.
  (* Makes iLeft/iRight work on SolveOneFoc *)
  Lemma solve_one_foc_from_or {TT : tele} (M : PROP → PROP) Q D :
    ModalityMono M →
    FromOr (SolveOneFoc (TT := TT) M Q D) (SolveOne M Q) (M D).
  Proof. unseal_diaframe; rewrite /FromOr => HM. apply or_elim; apply HM; eauto. Qed.

  (* Makes iMod work on SolveSep *)
  Lemma solve_sep_elim_modal p (P : PROP) P' M M1 M2 {TT} R S :
    IntoModal p P M1 P' →
    SplitModality3 M M1 M2 →
    ElimModal True p false P P' (SolveSep M (TT := TT) R S) (SolveSep M2 (TT := TT) R S).
  Proof.
    unseal_diaframe; rewrite /IntoModal /ElimModal /= => -> [<- HM1 HM2] _.
    rewrite modality_strong_frame_l.
    apply HM1.
    by rewrite wand_elim_r.
  Qed.
  (* Makes iMod work on SolveSepFoc *)
  Lemma solve_sep_foc_elim_modal p (P : PROP) P' M M1 M2 {TT} R S D :
    IntoModal p P M1 P' →
    SplitModality3 M M1 M2 →
    ElimModal True p false P P' (SolveSepFoc M (TT := TT) R S D) (SolveSepFoc M2 (TT := TT) R S D).
  Proof.
    unseal_diaframe; rewrite /IntoModal /ElimModal /= => -> [<- HM1 HM2] _.
    rewrite modality_strong_frame_l.
    apply HM1.
    by rewrite wand_elim_r.
  Qed.
  (* Makes iMod work on SolveOne *)
  Lemma solve_one_elim_modal p (P : PROP) P' M M1 M2 {TT} R :
    IntoModal p P M1 P' →
    SplitModality3 M M1 M2 →
    ElimModal True p false P P' (SolveOne M (TT := TT) R) (SolveOne M2 (TT := TT) R).
  Proof.
    unseal_diaframe; rewrite /IntoModal /ElimModal /= => -> [<- HM1 HM2] _.
    rewrite modality_strong_frame_l.
    apply HM1.
    by rewrite wand_elim_r.
  Qed.
  (* Makes iMod work on SolveOneFoc *)
  Lemma solve_one_foc_elim_modal p (P : PROP) P' M M1 M2 {TT} R D :
    IntoModal p P M1 P' →
    SplitModality3 M M1 M2 →
    ElimModal True p false P P' (SolveOneFoc M (TT := TT) R D) (SolveOneFoc M2 (TT := TT) R D).
  Proof.
    unseal_diaframe; rewrite /IntoModal /ElimModal /= => -> [<- HM1 HM2] _.
    rewrite modality_strong_frame_l.
    apply HM1.
    by rewrite wand_elim_r.
  Qed.

  (* Makes iInv work on SolveSep, using the iMod support. Non-standard opening wrt
    default Iris usage, but this is what Diaframe fares best with *)
  Global Instance solvesep_elimacc Mo Mc Mi {X : Type} α β mγ {TT} (R : TT -t> PROP) Q Z :
    (∀ P, ElimModal True false false (Mo P) P (SolveSep (TT := TT) Mi R Q) Z) →
    ModalityMono Mc →
    ElimAcc (X := X) True Mo Mc α β mγ (SolveSep (TT := TT) Mi R Q) 
      (λ x, ((β x) -∗ Mc (χ ∗ (default emp (mγ x)))) -∗ Z)%I.
  Proof.
    unseal_diaframe; rewrite /ElimAcc /accessor => HZ HMc /= _.
    iIntros "Hx1 Hx2".
    iMod "Hx2" as (x) "[Hα Hβ]".
    iSpecialize ("Hx1" with "Hα").
    iApply "Hx1". iIntros "Hβ'". iSpecialize ("Hβ" with "Hβ'").
    iStopProof. apply HMc. by rewrite empty_goal_eq left_id.
  Qed.
  (* Makes iInv work on SolveSepFoc, using the iMod support. Non-standard opening wrt
    default Iris usage, but this is what Diaframe fares best with *)
  Global Instance solvesepfoc_elimacc Mo Mc Mi {X : Type} α β mγ {TT} (R : TT -t> PROP) Q D Z :
    (∀ P, ElimModal True false false (Mo P) P (SolveSepFoc (TT := TT) Mi R Q D) Z) →
    ModalityMono Mc →
    ElimAcc (X := X) True Mo Mc α β mγ (SolveSepFoc (TT := TT) Mi R Q D) 
      (λ x, ((β x) -∗ Mc (χ ∗ (default emp (mγ x)))) -∗ Z)%I.
  Proof.
    unseal_diaframe; rewrite /ElimAcc /accessor => HZ HMc /= _.
    iIntros "Hx1 Hx2".
    iMod "Hx2" as (x) "[Hα Hβ]".
    iSpecialize ("Hx1" with "Hα").
    iApply "Hx1". iIntros "Hβ'". iSpecialize ("Hβ" with "Hβ'").
    iStopProof. apply HMc. by rewrite empty_goal_eq left_id.
  Qed.
  (* Makes iInv work on SolveOne, using the iMod support. Non-standard opening wrt
    default Iris usage, but this is what Diaframe fares best with *)
  Global Instance solveone_elimacc Mo Mc Mi {X : Type} α β mγ {TT} (R : TT -t> PROP) Z :
    (∀ P, ElimModal True false false (Mo P) P (SolveOne (TT := TT) Mi R) Z) →
    ModalityMono Mc →
    ElimAcc (X := X) True Mo Mc α β mγ (SolveOne (TT := TT) Mi R) 
      (λ x, ((β x) -∗ Mc (χ ∗ (default emp (mγ x)))) -∗ Z)%I.
  Proof.
    unseal_diaframe; rewrite /ElimAcc /accessor => HZ HMc /= _.
    iIntros "Hx1 Hx2".
    iMod "Hx2" as (x) "[Hα Hβ]".
    iSpecialize ("Hx1" with "Hα").
    iApply "Hx1". iIntros "Hβ'". iSpecialize ("Hβ" with "Hβ'").
    iStopProof. apply HMc. by rewrite empty_goal_eq left_id.
  Qed.
  (* Makes iInv work on SolveOneFoc, using the iMod support. Non-standard opening wrt
    default Iris usage, but this is what Diaframe fares best with *)
  Global Instance solveonefoc_elimacc Mo Mc Mi {X : Type} α β mγ {TT} (R : TT -t> PROP) D Z :
    (∀ P, ElimModal True false false (Mo P) P (SolveOneFoc (TT := TT) Mi R D) Z) →
    ModalityMono Mc →
    ElimAcc (X := X) True Mo Mc α β mγ (SolveOneFoc (TT := TT) Mi R D) 
      (λ x, ((β x) -∗ Mc (χ ∗ (default emp (mγ x)))) -∗ Z)%I.
  Proof.
    unseal_diaframe; rewrite /ElimAcc /accessor => HZ HMc /= _.
    iIntros "Hx1 Hx2".
    iMod "Hx2" as (x) "[Hα Hβ]".
    iSpecialize ("Hx1" with "Hα").
    iApply "Hx1". iIntros "Hβ'". iSpecialize ("Hβ" with "Hβ'").
    iStopProof. apply HMc. by rewrite empty_goal_eq left_id.
  Qed.
End iris_tactic_compatibility.


(* bunch of evarconv/w_unify work arounds... *)
Global Hint Extern 4 (FromOr (SolveSep _ _ _) _ _) =>
    eapply solve_sep_first_from_or; solve [tc_solve] : typeclass_instances.

Global Hint Extern 4 (FromOr (SolveOne _ _) _ _) =>
    eapply solve_one_from_or; solve [tc_solve] : typeclass_instances.

Global Hint Extern 4 (FromOr (SolveSepFoc _ _ _ _) _ _) =>
    eapply solve_sep_foc_from_or; solve [tc_solve] : typeclass_instances.

Global Hint Extern 4 (FromOr (SolveOneFoc _ _ _) _ _) =>
    eapply solve_one_foc_from_or; solve [tc_solve] : typeclass_instances.


Global Hint Extern 4 (ElimModal _ _ _ _ _ (SolveSep _ _ _) _) =>
    eapply solve_sep_elim_modal; solve [tc_solve] : typeclass_instances.

Global Hint Extern 4 (ElimModal _ _ _ _ _ (SolveSepFoc _ _ _ _) _) =>
    eapply solve_sep_foc_elim_modal; solve [tc_solve] : typeclass_instances.

Global Hint Extern 4 (ElimModal _ _ _ _ _ (SolveOne _ _) _) =>
    eapply solve_one_elim_modal; solve [tc_solve] : typeclass_instances.

Global Hint Extern 4 (ElimModal _ _ _ _ _ (SolveOneFoc _ _ _) _) =>
    eapply solve_one_foc_elim_modal; solve [tc_solve] : typeclass_instances.

