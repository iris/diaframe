From iris.bi Require Import bi fractional.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import auth numbers.
From iris.base_logic Require Import base_logic own.

From diaframe Require Import proofmode_base.
From diaframe.lib Require Import own_hints except_zero.


(* Like frac_token, but not specifically focused at a fractional payload.
   Currently only used as a building block for frac_token.
*)

Section tokenizable_mixin.
  Context `{!BiBUpd PROP}.
  Context (token_req : PROP).
  Context (token : gname → PROP).
  Context (token_counter : gname → positive → PROP).
  Context (no_tokens : gname → Qp → PROP).

  Record TokenizableMixin := {
      mixin_allocate_none : ⊢ |==> ∃ γ, no_tokens γ 1 ;
      mixin_create_token_succ γ p : token_counter γ p ⊢ |==> token_counter γ (Pos.succ p) ∗ token γ;
      mixin_create_token_1 γ : no_tokens γ 1 ∗ token_req ⊢ |==> token_counter γ 1 ∗ token γ;
      mixin_delete_token_pred γ p : (1 < p)%positive → token_counter γ p ∗ token γ ⊢ |==> token_counter γ (Pos.pred p);
      mixin_delete_token_1 γ : token_counter γ 1 ∗ token γ ⊢ |==> no_tokens γ 1 ∗ token_req;
      (* note the later here: this makes sure we can check compatibility when 'hotswapping' no_tokens in an invariant
         we don't want to require token_counter to be timeless, since it will not be in general: in the
          fractional payload case, token_counter contains a P q, which may very well not be timeless *)
      mixin_token_not_no_tokens γ q : ▷ token γ ∗ no_tokens γ q ⊢ ◇ False; 
      mixin_token_counter_no_tokens_incompat γ q p : no_tokens γ q ∗ ▷ token_counter γ p ⊢ ◇ False;
      mixin_no_tokens_timeless γ q : Timeless (no_tokens γ q);
      mixin_no_tokens_valid_frac γ q : no_tokens γ q ⊢ ⌜q ≤ 1⌝%Qp; 
      mixin_no_tokens_fractional γ : Fractional (no_tokens γ);
  }.
End tokenizable_mixin.


Module tokenizable.

Record Tokenizable `{!BiBUpd PROP} := {
  token_req : PROP;
  token : gname → PROP;
  token_counter : gname → positive → PROP;
  no_tokens : gname → Qp → PROP;
  tokizable_mixin : TokenizableMixin token_req token token_counter no_tokens
}.

Global Arguments Tokenizable _ {_}.
Global Arguments Build_Tokenizable {_ _} _ _ _ _ _.

Section lemmas.
  Context `{!BiBUpd PROP}.
  Context (tc : Tokenizable PROP).

  Lemma allocate_none : ⊢ |==> ∃ γ, no_tokens tc γ 1.
  Proof. eapply mixin_allocate_none, tc. Qed.

  Lemma create_token_succ γ p : token_counter tc γ p ⊢ |==> token_counter tc γ (Pos.succ p) ∗ token tc γ.
  Proof. eapply mixin_create_token_succ, tc. Qed.

  Lemma create_token_1 γ : no_tokens tc γ 1 ∗ token_req tc ⊢ |==> token_counter tc γ 1 ∗ token tc γ.
  Proof. eapply mixin_create_token_1, tc. Qed.

  Lemma delete_token_pred γ p : (1 < p)%positive → token_counter tc γ p ∗ token tc γ ⊢ |==> token_counter tc γ (Pos.pred p).
  Proof. eapply mixin_delete_token_pred, tc. Qed.

  Lemma delete_token_1 γ : token_counter tc γ 1 ∗ token tc γ ⊢ |==> no_tokens tc γ 1 ∗ token_req tc.
  Proof. eapply mixin_delete_token_1, tc. Qed.

  Global Instance no_tokens_fractional γ : Fractional (no_tokens tc γ).
  Proof. eapply mixin_no_tokens_fractional, tc. Qed.

  Global Instance no_tokens_timeless γ q : Timeless (no_tokens tc γ q).
  Proof. eapply mixin_no_tokens_timeless, tc. Qed.

  Lemma allocate_some : token_req tc ⊢ |==> ∃ γ, token_counter tc γ 1 ∗ token tc γ.
  Proof. 
    iIntros "H". 
    iMod (allocate_none) as (γ) "Hγ". 
    iExists γ. iApply create_token_1; iFrame.
  Qed.

  Lemma token_not_no_tokens γ q : ▷ token tc γ ∗ no_tokens tc γ q ⊢ ◇ False.
  Proof. eapply mixin_token_not_no_tokens, tc. Qed.

  Lemma token_not_no_tokens_now γ q : token tc γ ∗ no_tokens tc γ q ⊢ ◇ False.
  Proof. erewrite <-token_not_no_tokens. iSteps. Qed.

  Lemma token_counter_no_tokens_incompat γ q p : no_tokens tc γ q ∗ ▷ token_counter tc γ p ⊢ ◇ False.
  Proof. eapply mixin_token_counter_no_tokens_incompat, tc. Qed.

  Lemma no_tokens_frac_valid γ q : no_tokens tc γ q ⊢ ⌜q ≤ 1⌝%Qp.
  Proof. eapply mixin_no_tokens_valid_frac, tc. Qed.

End lemmas.

Section iter_lemmas.
  (* even though the tokencounter lemmas do not directly mention interaction with iterated tokens, they are enough to show that
     having n instances of `token tc γ` and also `token_counter tc γ p` forces `n ≤ p` *)
  Context `{!BiBUpd PROP}.
  Context (tc : Tokenizable PROP).

  Fixpoint token_iter tc (n : nat) γ : PROP :=
    match n with 
    | O => emp
    | S n => token tc γ ∗ token_iter tc n γ
    end.

  Lemma token_iter_sum n1 n2 γ : token_iter tc (n1 + n2) γ ⊣⊢ token_iter tc n1 γ ∗ token_iter tc n2 γ.
  Proof.
    induction n1 => //=.
    - by rewrite left_id.
    - rewrite IHn1 -bi.sep_assoc //.
  Qed.

  Lemma token_iter_count_dealloc_lt n γ p :
    (Z.of_nat n < Z.pos p)%Z →
    (token_iter tc n γ ∗ token_counter tc γ p ⊢ |==> token_counter tc γ (Z.to_pos (Z.pos p - (Z.of_nat n)))).
  Proof.
    induction n as [|n].
    - simpl => _. rewrite left_id -bupd_intro //.
    - simpl => HSn.
      rewrite -bi.sep_assoc IHn {IHn}; last lia.
      iIntros "[Ht >Hc]".
      iCombine "Hc Ht" as "Hγ".
      rewrite delete_token_pred.
      * enough (Pos.pred (Z.to_pos (Z.pos p - n)) = Z.to_pos (Z.pos p - S n)) as -> => //.
        destruct n => //=.
        { rewrite Z2Pos.inj_sub; last lia. simpl. lia. }
        rewrite Z2Pos.inj_sub; last lia. simpl.
        rewrite Z2Pos.inj_sub; last lia. simpl. lia.
      * destruct n => //=.
        rewrite Z2Pos.inj_sub; last lia. simpl. lia.
  Qed.

  Lemma token_iter_count_dealloc_all n γ p :
    (Z.of_nat n = Z.pos p) →
    (token_iter tc n γ ∗ token_counter tc γ p ⊢ |==> no_tokens tc γ 1 ∗ token_req tc).
  Proof.
    destruct n => //= Hn.
    rewrite -bi.sep_assoc token_iter_count_dealloc_lt; last lia.
    iIntros "[Ht >Hc]".
    iCombine "Hc Ht" as "Hγ".
    rewrite -Hn.
    replace (Z.of_nat (S n) - Z.of_nat n)%Z with 1%Z by lia. simpl.
    by rewrite delete_token_1.
  Qed.

  Lemma token_iter_count_compat n γ p : 
    token_iter tc n γ ∗ token_counter tc γ p ⊢ |==> ◇ ⌜n ≤ Zpos p⌝%Z.
  Proof.
    destruct (decide (n ≤ Z.pos p)%Z); first eauto.
    apply Znot_le_gt in n0.
    replace n with (Pos.to_nat p + (n - Pos.to_nat p)) at 1; last lia.
    rewrite token_iter_sum (bi.sep_comm (token_iter _ (Pos.to_nat _) _)) -bi.sep_assoc.
    rewrite token_iter_count_dealloc_all; last lia.
    iIntros "[Ht >[Hc1 Hc2]]".
    destruct (n - Pos.to_nat p) eqn:Heq; try lia.
    simpl.
    iDestruct "Ht" as "[Ht1 Htr]".
    iCombine "Ht1 Hc1" as "Hγ".
    rewrite token_not_no_tokens_now. iIntros "!>". iMod "Hγ" as "%". contradiction.
  Qed.

  Lemma token_iter_count_compat_direct n γ p `{!BiPlainly PROP, !BiBUpdPlainly PROP} :
    token_iter tc n γ ∗ token_counter tc γ p ⊢ ◇ ⌜n ≤ Zpos p⌝%Z.
  Proof.
    rewrite token_iter_count_compat.
    by iIntros ">H".
  Qed.

End iter_lemmas.

Section automation.
  Context `{!BiBUpd PROP}.
  Context (tc : Tokenizable PROP).
  Context `{BiAffine PROP}.

  Instance no_tokens_AsFractional γ q : AsFractional (no_tokens tc γ q) (no_tokens tc γ) q.
  Proof. constructor. done. tc_solve. Qed.

  Lemma biabd_alloc_none q φ mq :
    CmraSubtract 1%Qp q φ mq →
    HINT ε₁ ✱ [- ; ⌜φ⌝] ⊫ [bupd] γ; no_tokens tc γ q ✱ [default emp (q' ← mq; Some $ no_tokens tc γ q')].
  Proof.
    rewrite /CmraSubtract => Hq. iStep as (Hwit).
    iMod (allocate_none tc) as (γ) "Hγ".
    rewrite -Hq //. destruct mq => /=; last iSteps.
    iDestruct "Hγ" as "[? ?]".
    iSteps.
  Qed.

  Lemma biabd_alloc_some :
    HINT ε₁ ✱ [- ; token_req tc] ⊫ [bupd] γ; token_counter tc γ 1 ✱ [token tc γ].
  Proof. iStep as "H1". iMod (allocate_some with "H1") as (γ) "Hγ". iSteps. Qed.

  Lemma biabd_create_token γ p1 p2 :
    SolveSepSideCondition (p2 = Pos.succ p1) →
    HINT token_counter tc γ p1 ✱ [- ; emp] ⊫ [bupd]; token_counter tc γ p2 ✱ [token tc γ].
  Proof.
    move => ->; iStep.
    by iApply create_token_succ.
  Qed.

  Lemma biabd_create_first_token γ p :
    SolveSepSideCondition (p = 1%positive) →
    HINT no_tokens tc γ 1 ✱ [- ; token_req tc] ⊫ [bupd]; token_counter tc γ p ✱ [token tc γ].
  Proof. move => ->. apply: create_token_1. Qed.

  Lemma biabd_delete_token γ p1 p2 :
    SolveSepSideCondition (1 < p1)%positive →
    SolveSepSideCondition (p2 = Pos.pred p1) →
    HINT token_counter tc γ p1 ✱ [- ; token tc γ] ⊫ [bupd]; token_counter tc γ p2 ✱ [emp].
  Proof.
    move => Hp ->. iStep. rewrite right_id. iApply delete_token_pred => //. iSteps.
  Qed.

  Instance biabd_delete_last_token γ q mq φ p :
    SolveSepSideCondition (p = 1)%positive →
    CmraSubtract 1%Qp q φ mq →
    HINT token_counter tc γ p ✱ [- ; token tc γ ∗ ⌜φ⌝] ⊫ [bupd]; no_tokens tc γ q ✱ [token_req tc ∗ default emp (q' ← mq; Some $ no_tokens tc γ q')].
  Proof.
    move => -> Hq.
    iStep as (Hwit) "Hcount Htok".
    iMod (delete_token_1 tc γ with "[Hcount Htok]") as "[Hnt Hr]"; first iSteps.
    specialize (Hq Hwit); revert Hq.
    destruct mq as [q'|] => /=.
    - rewrite frac_op => <-.
      by iDestruct "Hnt" as "[$ $]".
    - move => ->.
      iSteps.
  Qed.

  Lemma biabd_delete_last_token_to_req γ p :
    SolveSepSideCondition (p = 1%positive) →
    HINT token_counter tc γ p ✱ [- ; token tc γ] ⊫ [bupd]; token_req tc ✱ [no_tokens tc γ 1].
  Proof.
    move => ->. iStep as "Htok Hcnt". rewrite (bi.sep_comm (token_req _)).
    iSteps.
  Qed.

  Lemma no_tokens_split γ q1 q2 mq :
    FracSub q1 q2 mq →
    HINT no_tokens tc γ q1 ✱ [- ; True] ⊫ [id]; 
         no_tokens tc γ q2 ✱ [match mq with | Some q => no_tokens tc γ q | None => True end].
  Proof.
    move => <-.
    destruct mq; iSteps as "Hnt".
    iDestruct "Hnt" as "[Hnt1 Hnt2]".
    iSteps.
  Qed.

  Instance merge_token_no_tokens_now γ q :
    MergableConsume (token tc γ) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens tc γ q))
            (TCEq Pout (◇ False)))%I. (* | 11 *)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    apply token_not_no_tokens_now.
  Qed.

  Instance merge_token_no_tokens γ q :
    MergableConsume (▷ token tc γ) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens tc γ q))
            (TCEq Pout (◇ False)))%I. (* | 11 *)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    apply token_not_no_tokens.
  Qed.

  Lemma merge_no_tokens_token_now γ q :
    MergableConsume (no_tokens tc γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (token tc γ))
            (TCEq Pout (◇False)))%I. (* | 10. *)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    by iStep.
  Qed.

  Lemma merge_no_tokens_token γ q :
    MergableConsume (no_tokens tc γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (▷ token tc γ))
            (TCEq Pout (◇False)))%I. (* | 10. *)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    by iStep.
  Qed.

  Instance merge_no_tokens_token_later_counter γ q pos:
    MergableConsume (no_tokens tc γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (▷ token_counter tc γ pos))
            (TCEq Pout (◇ False)))%I. (* | 11.*)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    apply token_counter_no_tokens_incompat.
  Qed.

  Instance merge_no_tokens_token_counter γ q pos:
    MergableConsume (no_tokens tc γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (token_counter tc γ pos))
            (TCEq Pout (◇ False)))%I. (* | 12.*)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    rewrite (bi.later_intro (token_counter _ _ _)). iStep as "Hnt Htc".
    iRevert "Hnt"; by iStep.
  Qed.

  Lemma merge_no_tokens γ q q1 q2 :
    MergableConsume (no_tokens tc γ q1) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens tc γ q2)) $
      TCAnd (IsOp q q1 q2) 
            (TCEq Pout (no_tokens tc γ q ∗ ⌜q ≤ 1⌝%Qp)))%I. (* | 20.*)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> [-> ->]].
    rewrite bi.intuitionistically_if_elim.
    iIntros "[Hq1 Hq2]".
    iCombine "Hq1 Hq2" as "H".
    iDestruct (no_tokens_frac_valid with "H") as %?.
    iSteps.
  Qed.

  Lemma merge_token_counter_no_tokens γ q pos:
    MergableConsume (token_counter tc γ pos) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens tc γ q))
            (TCEq Pout (◇ False)))%I. (* | 11.*)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    by iStep.
  Qed.

  Lemma merge_token_counter_no_tokens_later γ q pos:
    MergableConsume (▷token_counter tc γ pos)%I true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens tc γ q))
            (TCEq Pout (◇ False)))%I. (* | 11.*)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    by iStep.
  Qed.

End automation.

End tokenizable.

Import tokenizable.

Section base_instance.
  Definition emptyTokenR := authUR $ optionUR positiveR.
  Class emptyTokenG Σ := { #[local] emptyTokenCounter_inG :: inG Σ emptyTokenR }.
  Definition emptyTokenΣ : gFunctors := #[GFunctor emptyTokenR].
  Global Instance subG_emptyTokenCounterΣ {Σ} : subG emptyTokenΣ Σ → emptyTokenG Σ.
  Proof. solve_inG. Qed.

  Context `{!emptyTokenG Σ}.

  Program Definition empty_token_counter : Tokenizable $ iPropI Σ :=
    Build_Tokenizable
      emp%I
      (λ γ, own γ (◯ (Some 1%positive)))
      (λ γ p, own γ (● (Some p)))
      (λ γ q, own γ (●{#q} None))
      _.
  Next Obligation.
    split; [iSteps.. |tc_solve | | ].
    - iStep 3 as (γ q) "Hq". 
      iDestruct (own_valid with "Hq") as %[Hq _]%auth_auth_dfrac_valid.
      revert Hq => /dfrac_valid_own Hq; iSteps.
    - intros γ p q. eapply (anti_symm _); first iStep as "Hq".
      * by iDestruct "Hq" as "[$ $]".
      * iStep. by rewrite Qp.add_comm.
  Qed.
End base_instance.


Section tokenizable_extra_resource.
  Context {PROP : bi}.
  Context `{!BiBUpd PROP}.
  Context (tc : Tokenizable PROP).
  Context (Q : PROP).

  Program Definition add_resource : Tokenizable PROP :=
    Build_Tokenizable
      (token_req tc ∗ Q)
      (token tc)
      (λ γ p, token_counter tc γ p ∗ Q)%I
      (no_tokens tc)
      _.
  Next Obligation.
    split.
    - apply allocate_none.
    - iIntros (γ p) "[Htc $]"; iStopProof.
      apply create_token_succ.
    - iIntros (γ) "(Htc1 & Htc2 & $)"; iStopProof.
      apply create_token_1.
    - iIntros (γ p Hp) "[[Htc1 $] Htc2]"; iStopProof.
      apply delete_token_pred, Hp.
    - iIntros (γ) "[[Htc1 $] Htc2]"; iStopProof.
      apply delete_token_1.
    - apply token_not_no_tokens.
    - iIntros (γ q p) "[Htc1 [Htc2 _]]"; iStopProof.
      apply token_counter_no_tokens_incompat.
    - tc_solve.
    - apply no_tokens_frac_valid.
    - tc_solve.
  Qed.
End tokenizable_extra_resource.



























