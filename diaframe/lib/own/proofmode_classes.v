From iris.algebra Require Import cmra auth frac_auth.
From iris.base_logic Require Import own.
From iris.proofmode Require Import proofmode.


(* TODO: Remove this file once https://gitlab.mpi-sws.org/iris/iris/-/merge_requests/771 is merged *)

Class IsValidOp {A : cmra} (a a1 a2 : A) Σ (P : iProp Σ) := {
  is_valid_merge : ✓ (a1 ⋅ a2) ⊢ □ P ;
  is_valid_op : ✓ (a1 ⋅ a2) ⊢@{iPropI Σ} a ≡ a1 ⋅ a2 ;
}.

Global Hint Mode IsValidOp ! - ! ! ! - : typeclass_instances.

Definition includedI {M : ucmra} {A : cmra} (a b : A) : uPred M
  := (∃ c, b ≡ a ⋅ c)%I.

Notation "a ≼ b" := (includedI a b) : bi_scope.

Class IsIncludedMerge {A : cmra} (a1 a2 : A) {Σ} (P : iPropI Σ) := 
  is_included_merge : ✓ a2 ⊢ a1 ≼ a2 ↔ □ P.

Global Hint Mode IsIncludedMerge ! ! ! ! - : typeclass_instances.

Class HasRightId {A : cmra} (a : A) :=
  has_right_id : a ≼ a.

Global Hint Mode HasRightId ! ! : typeclass_instances.

Class IsIncludedMergeUnital {A : cmra} (a1 a2 : A) {Σ} (P_lt P_le : iProp Σ) := {
  included_merge_from_unital : IsIncludedMerge a1 a2 P_lt;
  is_included_merge_unital : ✓ a2 ⊢ □ P_lt ∨ a1 ≡ a2 ↔ □ P_le;
}.

Global Hint Mode IsIncludedMergeUnital ! ! ! ! - - : typeclass_instances.


Lemma own_valid_op {A : cmra} (a a1 a2 : A) γ `{!inG Σ A} (P : iProp Σ) :
  IsValidOp a a1 a2 Σ P →
  own γ a1 -∗ own γ a2 -∗ own γ a ∗ □ P.
Proof.
  case => HP Ha.
  iIntros "Ha1 Ha2".
  iAssert (✓ (a1 ⋅ a2))%I as "#H✓".
  { iApply (own_valid_2 with "Ha1 Ha2"). }
  iSplit; last by iApply HP.
  iAssert (a ≡ a1 ⋅ a2)%I with "[Ha1 Ha2]" as "#Heq"; first by iApply Ha.
  iRewrite "Heq".
  rewrite own_op; iFrame.
Qed.

Lemma is_included_auth {A : ucmra} (a1 a2 : A) dq γ `{!inG Σ $ authUR A} (P : iProp Σ) :
  IsIncludedMerge a1 a2 P →
  own γ (●{dq} a2) -∗ own γ (◯ a1) -∗ □ P.
Proof.
  rewrite /IsIncludedMerge => HP.
  iIntros "Ha1 Ha2".
  iCombine "Ha1 Ha2" as "Ha".
  rewrite own_valid auth_both_dfrac_validI.
  iDestruct "Ha" as "(_ & [%c #Hc] & #H✓)".
  iApply (HP with "H✓").
  iExists c. iApply "Hc".
Qed.

Lemma bi_iff_trans {PROP : bi} (P Q R : PROP) : (P ↔ Q) ∧ (Q ↔ R) ⊢ P ↔ R.
Proof.
  iIntros "HPQR"; iSplit; iStopProof;
  eapply bi.impl_intro_r.
  - rewrite /bi_iff.
    rewrite (bi.and_elim_l (P → Q)).
    rewrite (bi.and_elim_l (Q → R)).
    rewrite bi.and_comm bi.and_assoc.
    rewrite !bi.impl_elim_r //.
  - rewrite /bi_iff.
    rewrite (bi.and_elim_r (P → Q)).
    rewrite (bi.and_elim_r (Q → R)).
    rewrite -bi.and_assoc.
    rewrite !bi.impl_elim_l //.
Qed.

Lemma bi_iff_wand_iff {PROP : bi} (P Q : PROP) :
  BiAffine PROP →
  Persistent P →
  Persistent Q →
  (P ↔ Q) ⊣⊢ (P ∗-∗ Q).
Proof.
  intros.
  apply (anti_symm _);
  iIntros "HPQ"; iSplit; iIntros "#H";
  by iApply "HPQ".
Qed.

Lemma bi_wand_iff_trans {PROP : bi} (P Q R : PROP) :
  (P ∗-∗ Q) -∗ (Q ∗-∗ R) -∗ (P ∗-∗ R).
Proof.
  iIntros "HPQ HQR"; iSplit; iIntros "H".
  - iApply "HQR". by iApply "HPQ".
  - iApply "HPQ". by iApply "HQR".
Qed.

Lemma bi_wand_iff_rew {PROP : bi} (P Q R : PROP) :
  (P ∗-∗ Q) -∗ (Q ∗-∗ R) ∗-∗ (P ∗-∗ R).
Proof.
  iIntros "HPQ"; iSplit; iIntros "H".
  - iApply (bi_wand_iff_trans with "HPQ H").
  - iApply (bi_wand_iff_trans with "[HPQ] H").
    by iApply bi.wand_iff_sym.
Qed.

Lemma persistent_dup {PROP : bi} (P : PROP) : 
  Persistent P →
  P ⊢ P ∗ P.
Proof.
  move => HP.
  assert (P ⊢ P ∧ P) as HPdup by eauto.
  rewrite {1}HPdup {1}HP.
  rewrite bi.persistently_and_sep_elim //.
Qed.

Lemma lift_iff_wand_iffs {PROP : bi} (P Q S : PROP) :
  Persistent P →
  (P ⊢ Q ∗-∗ S) →
  (P ⊢ Q) ↔ (P ⊢ S)%I.
Proof.
  move => HP HQS. split.
  - move => HQR.
    rewrite (persistent_dup P) {1}HQS HQR.
    iIntros "[HQS HQ]".
    by iApply "HQS".
  - move => HQR.
    rewrite (persistent_dup P) {1}HQS HQR.
    iIntros "[HQS HQ]".
    by iApply "HQS".
Qed.

Lemma included_merge_as_unital_1 {A : cmra} (a1 a2 : A) Σ (P_lt P_le : iProp Σ) :
  IsIncludedMerge (Some a1) (Some a2) P_le  →
  IsIncludedMerge a1 a2 P_lt →
  IsIncludedMergeUnital a1 a2 P_lt P_le.
Proof.
  move => HP_lt HP_le; split => //.
  revert HP_lt HP_le; rewrite /IsIncludedMerge option_validI !bi_iff_wand_iff => HP_lt HP_le.
  rewrite (persistent_dup (✓ a2)).
  rewrite {1}HP_lt HP_le.
  iIntros "[HP_lt HP_le]".
  iApply (bi_wand_iff_trans with "[HP_le] HP_lt").
  iSplit.
  * iIntros "[HP|Hr]".
    + iSpecialize ("HP_le" with "HP").
      iDestruct "HP_le" as "[%c Hc]". iExists (Some c).
      rewrite -Some_op. by iRewrite "Hc".
    + iExists None.
      rewrite Some_op_opM /=. by iRewrite "Hr".
  * iIntros "[%c Hc]".
    destruct c as [c|].
    + rewrite -Some_op option_equivI.
      iLeft. iApply "HP_le". by iExists c.
    + rewrite Some_op_opM /= option_equivI. 
      iRight.
      by iRewrite "Hc".
Qed.

Lemma included_merge_as_unital_2 {A : cmra} (a1 a2 : A) Σ (P_lt P_le : iProp Σ) :
  IsIncludedMergeUnital a1 a2 P_lt P_le →
  IsIncludedMerge (Some a1) (Some a2) P_le.
Proof.
  case.
  rewrite /IsIncludedMerge option_validI !bi_iff_wand_iff => HP_lt HP_le.
  rewrite (persistent_dup (✓ a2)).
  rewrite {1}HP_lt HP_le.
  iIntros "[HP_lt HP_le]".
  iApply (bi_wand_iff_trans with "[HP_lt] HP_le").
  iSplit.
  * iIntros "[%c Hc]".
    destruct c as [c|].
    + rewrite -Some_op option_equivI.
      iLeft. iApply "HP_lt". by iExists c.
    + rewrite Some_op_opM /= option_equivI. 
      iRight.
      by iRewrite "Hc".
  * iIntros "[HP|Hr]".
    + iSpecialize ("HP_lt" with "HP").
      iDestruct "HP_lt" as "[%c Hc]". iExists (Some c).
      rewrite -Some_op. by iRewrite "Hc".
    + iExists None.
      rewrite Some_op_opM /=. by iRewrite "Hr".
Qed.


Lemma is_included_frac_auth {A : cmra} (a1 a2 : A) q γ `{!inG Σ $ frac_authUR A} (P_lt P_le : iProp Σ) :
  IsIncludedMergeUnital (q, a1) (1%Qp, a2) P_lt P_le →
  own γ (●F a2) -∗ own γ (◯F{q} a1) -∗ □ P_le.
Proof.
  move => HP.
  apply is_included_auth. eapply included_merge_as_unital_2 => //.
Qed.


Section proper.
  Global Instance includedI_proper_1 {M : ucmra} {A : cmra} n : Proper ((dist n) ==> (dist n) ==> (dist n)) (includedI (M := M) (A := A)).
  Proof. solve_proper. Qed.
  Global Instance includedI_proper_2 {M : ucmra} {A : cmra} : Proper ((≡) ==> (≡) ==> (≡)) (includedI (M := M) (A := A)).
  Proof. solve_proper. Qed.

  Global Instance merge_proper {A : cmra} : Proper ((≡) ==> (≡) ==> forall_relation (λ Σ, (≡@{iPropI Σ}) ==> (iff))) (IsIncludedMerge (A := A)).
  Proof. solve_proper. Qed.

  Global Instance merge_unital_proper {A : cmra} : Proper ((≡) ==> (≡) ==> forall_relation (λ Σ, (≡@{iPropI Σ}) ==> (≡@{iPropI Σ}) ==> (iff))) (IsIncludedMergeUnital (A := A)).
  Proof.
    move => a1 a1' Ha1 a2 a2' Ha2 Σ P1 P1' HP1 P2 P2' HP2.
    split; case => H1 H2; split.
    - revert H1; apply merge_proper => //.
    - rewrite -Ha2 H2 HP1 Ha1 HP2 //.
    - revert H1; apply merge_proper => //.
    - rewrite Ha2 H2 HP1 Ha1 HP2 //.
  Qed.
End proper.

