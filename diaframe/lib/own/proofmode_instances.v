From iris.algebra Require Import cmra proofmode_classes.
From iris.base_logic Require Import own.
From iris.proofmode Require Import proofmode classes_make.

From diaframe.lib.own Require Import proofmode_classes.

(* TODO: Remove this file once https://gitlab.mpi-sws.org/iris/iris/-/merge_requests/771 is merged *)

Local Instance includedI_into_pure `{CmraDiscrete A} (a b : A) {Σ} : IntoPure (PROP := iPropI Σ) (a ≼ b)%I (a ≼ b).
Proof.
  rewrite /IntoPure. iDestruct 1 as (c) "%"; iPureIntro.
  by eexists.
Qed.


Section cmra_instances.
  Implicit Types Σ : gFunctors.
  Context {A : cmra}.

  Lemma from_isop (a a1 a2 : A) {Σ} :
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I.
  Proof. 
    rewrite /IsOp; split.
    - eauto.
    - rewrite H. eauto.
  Qed.

  Lemma is_valid_op_comm (a a1 a2 : A) {Σ} (P : iProp Σ) :
    IsValidOp a a1 a2 Σ P → IsValidOp a a2 a1 Σ P.
  Proof. case; split; rewrite comm //. Qed.

  Lemma is_included_merge' (a1 a2 : A) {Σ} (P : iProp Σ) :
    IsIncludedMerge a1 a2 P →
    a1 ≼ a2 ⊢ ✓ a2 -∗ □ P.
  Proof.
    rewrite /IsIncludedMerge => ->.
    iIntros "Ha1 HP".
    rewrite bi_iff_wand_iff.
    by iApply "HP".
  Qed.

  Global Instance unital_from_right_id (a1 a2 : A) {Σ} (P : iProp Σ) :
    HasRightId a2 →
    IsIncludedMerge a1 a2 P →
    IsIncludedMergeUnital a1 a2 P P | 100.
  Proof.
    rewrite /IsIncludedMerge => [[e He]] HP.
    split => //.
    rewrite HP.
    iIntros "HaP"; iSplit.
    - iIntros "[$|Ha]".
      iApply "HaP".
      iExists e.
      iRewrite "Ha".
      rewrite -He //.
    - iIntros "$".
  Qed.

  Global Instance merge_unital_last_resort (a1 a2 : A) {Σ} (P_lt P_le : iProp Σ):
    IsIncludedMerge a1 a2 P_lt →
    MakeOr P_lt (a1 ≡ a2)%I P_le →
    IsIncludedMergeUnital a1 a2 P_lt P_le | 999.
  Proof.
    rewrite /MakeOr => HP_lt HP_le.
    split => //. rewrite -HP_le.
    iIntros "_"; iSplit; iIntros "[#$|#$]".
  Qed.
End cmra_instances.


Section ucmra_instances.
  Implicit Types Σ : gFunctors.
  Context {A : ucmra}.

  Global Instance valid_op_unit_left (a : A) Σ :
    IsValidOp a ε a Σ True%I | 5.
  Proof. apply from_isop. rewrite /IsOp left_id //. Qed.

  Global Instance valid_op_unit_right (a : A) Σ :
    IsValidOp a a ε Σ True%I | 5.
  Proof. apply is_valid_op_comm, _. Qed.

  Global Instance has_right_id (a : A) :
    HasRightId a.
  Proof. exists ε. rewrite right_id //. Qed.
End ucmra_instances.


From iris.algebra Require Import numbers frac dfrac.

Section numbers.
  Implicit Types Σ : gFunctors.

  Global Instance nat_valid_op (a a1 a2 : nat) Σ: 
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
  Proof. apply from_isop. Qed.
  Global Instance nat_included_merge (a1 a2 : nat) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜a1 ≤ a2⌝%I.
  Proof.
    rewrite /IsIncludedMerge.
    iIntros "_"; iSplit. 
    - by iDestruct 1 as %?%nat_included.
    - iIntros "%". iExists (a2 - a1). iPureIntro. fold_leibniz. rewrite nat_op. lia.
  Qed.

  Global Instance nat_max_valid_op (a a1 a2 : max_nat) Σ :
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
  Proof. apply from_isop. Qed.
  Global Instance nat_max_included_merge (a1 a2 : nat) {Σ} : IsIncludedMerge (MaxNat a1) (MaxNat a2) (Σ := Σ) ⌜a1 ≤ a2⌝%I.
  Proof.
    rewrite /IsIncludedMerge. iIntros "_"; iSplit.
    - by iDestruct 1 as %?%max_nat_included.
    - iIntros "%". iExists (MaxNat a2). rewrite max_nat_op. iPureIntro. fold_leibniz. f_equal. lia.
  Qed.

  Global Instance nat_min_valid_op (a a1 a2 : min_nat) Σ :
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I.
  Proof. apply from_isop. Qed.
  Global Instance nat_min_included_merge (a1 a2 : nat) {Σ} : IsIncludedMerge (MinNat a1) (MinNat a2) (Σ := Σ) ⌜a2 ≤ a1⌝%I.
  Proof.
    rewrite /IsIncludedMerge. iIntros "_"; iSplit.
    - by iDestruct 1 as %?%min_nat_included.
    - iIntros "%". iExists (MinNat a2). rewrite min_nat_op_min. iPureIntro. fold_leibniz. f_equal. lia.
  Qed.
  Global Instance nat_min_has_right_id (a : nat) {Σ} : HasRightId (MinNat a).
  Proof. exists (MinNat a). rewrite min_nat_op_min. fold_leibniz. f_equal. lia.
  Qed.

  Global Instance positive_valid_op (a a1 a2 : positive) Σ :
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I.
  Proof. apply from_isop. Qed.
  Global Instance positive_included_merge (a1 a2 : positive) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜(a1 < a2)%positive⌝%I.
  Proof. 
    rewrite /IsIncludedMerge. iIntros "_"; iSplit.
    - by iDestruct 1 as %?%pos_included.
    - iIntros "%". iExists (a2 - a1)%positive. iPureIntro. fold_leibniz. rewrite pos_op_add. lia.
  Qed.
  Global Instance positive_included_merge_unital (a1 a2 : positive) {Σ} : 
    IsIncludedMergeUnital a1 a2 (Σ := Σ) ⌜a1 < a2⌝%positive%I ⌜(a1 ≤ a2)%positive⌝%I.
  Proof.
    apply: Build_IsIncludedMergeUnital.
    iIntros "_"; iSplit.
    - iIntros "[%|->]"; eauto with lia.
    - iIntros "%H".
      apply Positive_as_DT.le_lteq in H as [Hl | ->]; last by iRight.
      by iLeft.
  Qed.

  Global Instance frac_valid_op (q q1 q2 : Qp) Σ :
    IsOp q q1 q2 → IsValidOp q q1 q2 Σ ⌜q1 + q2 ≤ 1⌝%Qp%I.
  Proof.
    rewrite /IsOp; split; last first.
    { rewrite H; eauto. }
    by iDestruct 1 as %?%frac_valid.
  Qed.
  Global Instance frac_included_merge (q1 q2 : Qp) {Σ} : IsIncludedMerge q1 q2 (Σ := Σ) ⌜(q1 < q2)%Qp⌝%I.
  Proof. 
    rewrite /IsIncludedMerge. iIntros "_" ; iSplit.
    - by iDestruct 1 as %?%frac_included.
    - iIntros "%".
      apply Qp.lt_sum in H as [q' ->].
      by iExists q'.
  Qed.
  Global Instance frac_included_merge_unital (q1 q2 : Qp) {Σ} : IsIncludedMergeUnital q1 q2 (Σ := Σ) ⌜q1 < q2⌝%Qp%I ⌜q1 ≤ q2⌝%Qp%I.
  Proof.
    apply: Build_IsIncludedMergeUnital.
    iIntros "_"; iSplit.
    - iIntros "[%|->]"; eauto. iPureIntro. by apply Qp.lt_le_incl.
    - iIntros "%H".
      destruct (Qp.le_lteq q1 q2) as [[Hdl|Hdr] _]; eauto.
  Qed.

  Lemma dfrac_validI (dq : dfrac) {M : ucmra} : ✓ dq ⊢@{uPredI M} match dq with | DfracOwn q => ✓ q | DfracBoth q => ⌜q < 1⌝%Qp | _ => True end.
  Proof. rewrite uPred.discrete_valid /valid /cmra_valid /= /dfrac_valid_instance. destruct dq => //=. rewrite uPred.discrete_valid frac_valid //. Qed.

  Global Instance dfrac_valid_op_carry (q q1 q2 : Qp) Σ Pq :
    IsValidOp q q1 q2 Σ Pq → IsValidOp (DfracOwn q) (DfracOwn q1) (DfracOwn q2) Σ Pq.
  Proof.
    split.
    - rewrite /op /cmra_op /=. rewrite dfrac_validI.
      destruct H as [H _]. rewrite H //.
    - destruct H as [_ H].
      rewrite /op /cmra_op /= dfrac_validI H.
      iIntros "->" => //.
  Qed.

  Global Instance dfrac_valid_op_with_discarded_r (q : Qp) Σ :
    IsValidOp (DfracOwn q ⋅ DfracDiscarded) (DfracOwn q) DfracDiscarded Σ ⌜q < 1⌝%Qp%I.
  Proof.
    split; last by eauto.
    rewrite dfrac_validI /=. eauto.
  Qed.

  Global Instance dfrac_valid_op_with_discarded_l (q : Qp) Σ :
    IsValidOp (DfracOwn q ⋅ DfracDiscarded) DfracDiscarded (DfracOwn q) Σ ⌜q < 1⌝%Qp%I.
  Proof. apply is_valid_op_comm, _. Qed.

  Global Instance dfrac_valid_op_both_discarded (q : Qp) Σ :
    IsValidOp DfracDiscarded DfracDiscarded DfracDiscarded Σ True.
  Proof. split; eauto. Qed.

  Global Instance dfrac_own_included_merge (q1 q2 : Qp) {Σ} Pq : 
    IsIncludedMerge q1 q2 (Σ := Σ) Pq → 
    IsIncludedMerge (DfracOwn q1) (DfracOwn q2) (Σ := Σ) Pq.
  Proof. 
    rewrite /IsIncludedMerge dfrac_validI => ->.
    rewrite !bi_iff_wand_iff.
    iApply bi_wand_iff_trans. iSplit.
    - iDestruct 1 as %?%dfrac_own_included. iPureIntro. by apply frac_included.
    - iIntros "%".
      apply frac_included in H.
      apply Qp.lt_sum in H as [q' ->].
      by iExists (DfracOwn q').
  Qed.
  Global Instance dfrac_own_included_merge_unital (q1 q2 : Qp) {Σ} Pq Pq' : 
    IsIncludedMergeUnital q1 q2 (Σ := Σ) Pq Pq' → 
    IsIncludedMergeUnital (DfracOwn q1) (DfracOwn q2) (Σ := Σ) Pq Pq'.
  Proof.
    case => Hpq Hpq'. split; first apply _.
    rewrite dfrac_validI Hpq' !bi_iff_wand_iff.
    iApply bi_wand_iff_trans. iSplit.
    - iIntros "[Hpq|%]"; eauto. iRight. case: H => -> //.
    - iIntros "[Hpq|->]"; eauto.
  Qed.
  Global Instance dfrac_own_disc_included_merge (q : Qp) {Σ} :
    IsIncludedMerge (DfracOwn q) DfracDiscarded (Σ := Σ) False.
  Proof.
    rewrite /IsIncludedMerge.
    iIntros "_". iSplit => //.
    iIntros "[%dq %Hdq]". destruct dq => //=.
  Qed.
  Global Instance dfrac_disc_own_included_merge (q : Qp) {Σ} :
    IsIncludedMerge DfracDiscarded (DfracOwn q) (Σ := Σ) False.
  Proof.
    rewrite /IsIncludedMerge.
    iIntros "_". iSplit => //.
    iIntros "[%dq %Hdq]". destruct dq => //=.
  Qed.
  Global Instance dfrac_disc_disc_included_merge {Σ} :
    IsIncludedMerge DfracDiscarded DfracDiscarded (Σ := Σ) True.
  Proof.
    rewrite /IsIncludedMerge.
    iIntros "_". iSplit => //.
    iIntros "_". by iExists DfracDiscarded.
  Qed.
  Global Instance dfrac_disc_disc_included_merge_unital (q : Qp) {Σ} :
    IsIncludedMergeUnital DfracDiscarded DfracDiscarded (Σ := Σ) True True.
  Proof. apply: Build_IsIncludedMergeUnital. eauto. Qed.
  Global Instance dfrac_discarded_right_id : HasRightId DfracDiscarded.
  Proof. exists DfracDiscarded => //. Qed.

End numbers.


From iris.algebra Require Import gset.

Section sets.
  Context `{Countable K}.
  Global Instance set_is_op_emp_l (X : gset K) :
    IsOp X ∅ X | 10.
  Proof. rewrite /IsOp. set_solver. Qed.
  Global Instance set_is_op_emp_r (X : gset K) :
    IsOp X X ∅ | 10.
  Proof. rewrite /IsOp. set_solver. Qed.
  Global Instance set_is_op (X Y : gset K) :
    IsOp (X ∪ Y) X Y | 20.
  Proof. done. Qed.

  Global Instance set_is_valid_op (a a1 a2 : gset K) Σ :
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
  Proof. apply from_isop. Qed.
  Global Instance set_included_merge (a1 a2 : gset K) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜a1 ⊆ a2⌝%I.
  Proof. 
    rewrite /IsIncludedMerge. iIntros "_"; iSplit.
    - by iDestruct 1 as %?%gset_included. 
    - iIntros "%". iExists a2. iPureIntro. set_solver.
  Qed.

  Global Instance set_disj_is_valid_op (X Y : gset K) Σ :
    IsValidOp (GSet (X ∪ Y)) (GSet X) (GSet Y) Σ ⌜X ## Y⌝%I | 20.
  Proof.
    split.
    - by iDestruct 1 as %?%gset_disj_valid_op.
    - iDestruct 1 as %?%gset_disj_valid_op.
      by rewrite gset_disj_union.
  Qed.
  Global Instance set_disj_valid_op_emp_l (X Y : gset K) Σ :
    IsValidOp (GSet X) (GSet X) (GSet ∅) Σ True%I | 10.
  Proof. apply from_isop. rewrite /IsOp. rewrite gset_disj_union; [f_equal | ]; set_solver. Qed.
  Global Instance set_disj_valid_op_emp_r (X Y : gset K) Σ :
    IsValidOp (GSet X) (GSet ∅) (GSet X) Σ True%I | 10.
  Proof. apply from_isop. rewrite /IsOp. rewrite gset_disj_union; [f_equal | ]; set_solver. Qed.
  Global Instance disj_set_included_merge (a1 a2 : gset K) {Σ} : IsIncludedMerge (GSet a1) (GSet a2) (Σ := Σ) ⌜a1 ⊆ a2⌝%I.
  Proof. 
    rewrite /IsIncludedMerge. iIntros "_"; iSplit.
    - by iDestruct 1 as %?%gset_disj_included. 
    - iIntros "%".
      iExists (GSet (a2 ∖ a1)).
      iPureIntro. rewrite gset_disj_union; [|set_solver].
      fold_leibniz. f_equal. by apply union_difference_L.
  Qed.
End sets.


From iris.algebra Require Import gmultiset.

Section multisets.
  Context `{Countable K}.
  Global Instance multiset_is_op_emp_l (X : gmultiset K) :
    IsOp X ∅ X | 10.
  Proof. rewrite /IsOp. multiset_solver. Qed.
  Global Instance multiset_is_op_emp_r (X : gset K) :
    IsOp X X ∅ | 10.
  Proof. rewrite /IsOp. multiset_solver. Qed.
  Global Instance multiset_is_op (X Y : gmultiset K) :
    IsOp (X ⊎ Y) X Y.
  Proof. done. Qed.

  Global Instance multiset_is_valid_op (a a1 a2 : gmultiset K) Σ :
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
  Proof. apply from_isop. Qed.
  Global Instance multiset_included_merge (a1 a2 : gmultiset K) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜a1 ⊆ a2⌝%I.
  Proof. 
    rewrite /IsIncludedMerge. iIntros "_"; iSplit.
    - by iDestruct 1 as %?%gmultiset_included. 
    - iIntros "%". iExists (a2 ∖ a1). iPureIntro. fold_leibniz. rewrite gmultiset_op. multiset_solver.
  Qed.
End multisets.


From iris.algebra Require Import csum excl agree auth frac_auth.

Section recursive.
  Implicit Types A : cmra.

  Global Instance option_some_valid_op {A} (a a1 a2 : A) Σ P :
    IsValidOp a a1 a2 Σ P → IsValidOp (Some a) (Some a1) (Some a2) Σ P.
  Proof.
    case => HP Ha.
    split; rewrite -Some_op option_validI //.
    by rewrite Ha option_equivI.
  Qed.
  Global Instance option_included_merge {A} (a1 a2 : A) {Σ} (P_lt P_le : iProp Σ):
    IsIncludedMergeUnital a1 a2 P_lt P_le →
    IsIncludedMerge (Some a1) (Some a2) P_le | 100.
  Proof. by eapply included_merge_as_unital_2. Qed.
 (* we need below, even though it is trivial, to handle recursive cases *)
  Global Instance option_none_excl_included_merge {A} (a : optionUR A) {Σ} :
    IsIncludedMerge None a (Σ := Σ) (True)%I.
  Proof.
    rewrite /IsIncludedMerge. iIntros "_". iSplit; eauto.
    iIntros "_". iExists a. by rewrite left_id.
  Qed.
  Global Instance option_some_none_excl_included_merge {A} (a : A) {Σ} :
    IsIncludedMerge (Some a) None (Σ := Σ) (False)%I.
  Proof.
    rewrite /IsIncludedMerge. iIntros "_"; iSplit; last done.
    iDestruct 1 as (c) "Hc".
    iDestruct "Hc" as %?.
    iPureIntro.
    destruct c as [c|].
    - rewrite Some_op_opM /= in H.
      apply (Some_equiv_eq _ (a ⋅ c)) in H as [x [xH _]] => //.
    - rewrite Some_op_opM /= in H.
      apply (Some_equiv_eq _ a) in H as [x [xH _]] => //.
  Qed.

  Global Instance sum_inl_valid_op {A1 A2} (a a1 a2 : A1) Σ P :
    IsValidOp a a1 a2 Σ P → IsValidOp (Cinl a) (Cinl (B := A2) a1) (Cinl (B := A2) a2) Σ P.
  Proof.
    case => HP Ha. 
    split; rewrite -Cinl_op csum_validI //.
    rewrite Ha.
    iIntros "Ha".
    by iRewrite "Ha".
  Qed.
  Global Instance sum_inr_valid_op {A1 A2} (a a1 a2 : A1) Σ P :
    IsValidOp a a1 a2 Σ P → IsValidOp (Cinr a) (Cinr (A := A2) a1) (Cinr (A := A2) a2) Σ P.
  Proof.
    case => HP Ha. 
    split; rewrite -Cinr_op csum_validI //.
    rewrite Ha.
    iIntros "Ha".
    by iRewrite "Ha".
  Qed.
  Global Instance sum_inl_inr_invalid_op {A B : cmra} (a : A) (b : B) Σ :
    IsValidOp (CsumBot) (Cinl (B := B) a) (Cinr (A := A) b) Σ False.
  Proof. 
    split; rewrite /op /= /cmra_op //=; eauto. (* TODO: need proper access to unsealing lemma's *)
    uPred_primitive.unseal.
    rewrite /upred.uPred_cmra_valid_def /=.
    rewrite /validN /= /cmra_validN //=.
  Qed.
  Global Instance sum_inr_inl_invalid_op {A B : cmra} (a : A) (b : B) Σ :
    IsValidOp (CsumBot) (Cinr (A := B) a) (Cinl (B := A) b) Σ False.
  Proof. 
    split; rewrite /op /= /cmra_op //=; eauto.
    uPred_primitive.unseal.
    rewrite /upred.uPred_cmra_valid_def /=.
    rewrite /validN /= /cmra_validN //=.
  Qed.
  Global Instance sum_inl_included_merge {A1 A2} (a1 a2 : A1) {Σ} (P : iProp Σ):
    IsIncludedMerge a1 a2 P →
    IsIncludedMerge (Cinl (B := A2) a1) (Cinl (B := A2) a2) P | 100.
  Proof.
    rewrite /IsIncludedMerge => HaP.
    iIntros "#H✓"; iSplit.
    - iDestruct 1 as (c) "#Hc".
      rewrite csum_equivI csum_validI.
      destruct c=> //=.
      iApply HaP => //.
      by iExists _.
    - rewrite csum_validI HaP.
      iIntros "#HP".
      iSpecialize ("H✓" with "HP").
      iDestruct "H✓" as (c) "Hc".
      iRewrite "Hc".
      by iExists (Cinl c).
  Qed.
  Global Instance sum_inl_included_merge_unital {A1 A2} (a1 a2 : A1) {Σ} (P_lt P_le : iProp Σ):
    IsIncludedMergeUnital a1 a2 P_lt P_le →
    IsIncludedMergeUnital (Cinl (B := A2) a1) (Cinl (B := A2) a2) P_lt P_le | 100.
  Proof.
    case => HP_lt HP_le; split; first apply _.
    rewrite csum_validI HP_le.
    iIntros "HP_lt_le"; iSplit.
    * iIntros "[H|H]"; iApply "HP_lt_le"; first by eauto.
      rewrite csum_equivI. eauto.
    * iIntros "HP_le".
      iDestruct ("HP_lt_le" with "HP_le") as "[$|H]".
      iRewrite "H". eauto.
  Qed.
  Global Instance sum_inr_included_merge {A1 A2} (a1 a2 : A1) {Σ} (P : iProp Σ):
    IsIncludedMerge a1 a2 P →
    IsIncludedMerge (Cinr (A := A2) a1) (Cinr (A := A2) a2) (P)%I | 100.
  Proof.
    rewrite /IsIncludedMerge => HaP.
    iIntros "#H✓"; iSplit.
    - iDestruct 1 as (c) "#Hc".
      rewrite csum_equivI csum_validI.
      destruct c=> //=.
      iApply HaP => //.
      by iExists _.
    - rewrite csum_validI HaP.
      iIntros "#HP".
      iSpecialize ("H✓" with "HP").
      iDestruct "H✓" as (c) "Hc".
      iRewrite "Hc".
      by iExists (Cinr c).
  Qed.
  Global Instance sum_inr_included_merge_unital {A1 A2} (a1 a2 : A1) {Σ} (P_lt P_le : iProp Σ):
    IsIncludedMergeUnital a1 a2 P_lt P_le →
    IsIncludedMergeUnital (Cinr (A := A2) a1) (Cinr (A := A2) a2) P_lt P_le | 100.
  Proof.
    case => HP_lt HP_le; split; first apply _.
    rewrite csum_validI HP_le.
    iIntros "HP_lt_le"; iSplit.
    * iIntros "[H|H]"; iApply "HP_lt_le"; first by eauto.
      rewrite csum_equivI. eauto.
    * iIntros "HP_le".
      iDestruct ("HP_lt_le" with "HP_le") as "[$|H]".
      iRewrite "H". eauto.
  Qed.
  Global Instance sum_inl_inr_included_merge {A1 A2} (a1 : A1) (a2 : A2) {Σ} :
    IsIncludedMerge (Σ := Σ) (Cinl a1) (Cinr a2) False | 100.
  Proof.
    rewrite /IsIncludedMerge; iIntros "_"; iSplit; last done.
    iDestruct 1 as (c) "#Hc".
    rewrite csum_equivI.
    destruct c=> //=.
  Qed.
  Global Instance sum_inr_inl_included_merge {A1 A2} (a1 : A1) (a2 : A2) {Σ} :
    IsIncludedMerge (Σ := Σ) (Cinr a1) (Cinl a2) False | 100.
  Proof.
    rewrite /IsIncludedMerge; iIntros "_"; iSplit; last done.
    iDestruct 1 as (c) "#Hc".
    rewrite csum_equivI.
    destruct c=> //=.
  Qed.
  Global Instance csum_right_id_l {A B : cmra} (a : A) :
    HasRightId a → HasRightId (Cinl (B := B) a).
  Proof. 
    move => [r rH].
    exists (Cinl r).
    rewrite -Cinl_op -rH //.
  Qed.
  Global Instance csum_right_id_r {A B : cmra} (a : A) :
    HasRightId a → HasRightId (Cinr (A := B) a).
  Proof. 
    move => [r rH].
    exists (Cinr r).
    rewrite -Cinr_op -rH //.
  Qed.

  Global Instance prod_valid_op {A1 A2} (x x1 x2 : A1) (y y1 y2 : A2) Σ P1 P2 P :
    IsValidOp x x1 x2 Σ P1 → 
    IsValidOp y y1 y2 Σ P2 → 
    MakeAnd P1 P2 P →
    IsValidOp (x, y) (x1, y1) (x2, y2) Σ P.
  Proof.
    case => HP1 Hxs.
    case => HP2 Hys.
    rewrite /MakeAnd. split; rewrite -pair_op prod_validI /=.
    - rewrite -H bi.intuitionistically_and -HP1 -HP2 //.
    - rewrite prod_equivI /= -Hxs -Hys //.
  Qed.
  Global Instance prod_included_merge {A1 A2} (x1 x2 : A1) (y1 y2 : A2) {Σ} P1 P2 P :
    IsIncludedMerge x1 x2 (Σ := Σ) P1 →
    IsIncludedMerge y1 y2 (Σ := Σ) P2 →
    MakeAnd P1 P2 P →
    IsIncludedMerge (x1, y1) (x2, y2) (Σ := Σ) P.
  Proof.
    rewrite /IsIncludedMerge /MakeAnd => HP1 HP2 <-.
    rewrite bi.intuitionistically_and prod_validI /=.
    iIntros "[#Hx✓ #Hy✓]"; iSplit.
    - iDestruct 1 as (z) "#Hz".
      destruct z as [z1 z2].
      rewrite -pair_op.
      rewrite prod_equivI /=.
      iDestruct "Hz" as "[Hz1 Hz2]".
      iSplit.
      * iApply (HP1 with "[Hz1]") => //. 
        by iExists _.
      * iApply (HP2 with "[Hz2]") => //. 
        by iExists _.
    - iIntros "[#HP1 #HP2]".
      rewrite HP1 HP2.
      iSpecialize ("Hx✓" with "HP1").
      iSpecialize ("Hy✓" with "HP2").
      iDestruct "Hx✓" as (x3) "Hx3".
      iDestruct "Hy✓" as (y3) "Hy3".
      iRewrite "Hx3". iRewrite "Hy3".
      by iExists (_, _).
  Qed. (* extra instance because TC resolution gets confused for ucmras :( *)
  Global Instance prod_included_merge_ucmra {A1 A2 : ucmra} (x1 x2 : A1) (y1 y2 : A2) {Σ} P1 P2 P :
    IsIncludedMerge x1 x2 (Σ := Σ) P1 →
    IsIncludedMerge y1 y2 (Σ := Σ) P2 →
    MakeAnd P1 P2 P →
    IsIncludedMerge (x1, y1) (x2, y2) (Σ := Σ) P.
  Proof. simple eapply prod_included_merge. Qed.

  Lemma prod_includedI {A1 A2} (x1 x2 : A1) (y1 y2 : A2) Σ :
    (x1, y1) ≼ (x2, y2) ⊣⊢@{iPropI Σ} (x1 ≼ x2) ∧ (y1 ≼ y2).
  Proof.
    apply (anti_symm _).
    - iDestruct 1 as ([x y]) "Hc".
      rewrite -pair_op prod_equivI /=.
      iDestruct "Hc" as "[Hx Hy]"; iSplit; by iExists _.
    - iDestruct 1 as "[[%x Hx] [%y Hy]]".
      iRewrite "Hx"; iRewrite "Hy".
      by iExists (_, _).
  Qed.

  Global Instance make_and_false_l {PROP : bi} (P : PROP) : KnownLMakeAnd False P False.
  Proof.
    rewrite /KnownLMakeAnd /MakeAnd //.
    apply (anti_symm _).
    - iIntros "[$ _]".
    - iIntros "[]".
  Qed.

  Global Instance make_and_false_r {PROP : bi} (P : PROP) : KnownLMakeAnd P False False.
  Proof.
    rewrite /KnownLMakeAnd /MakeAnd.
    apply (anti_symm _).
    - iIntros "[_ $]".
    - iIntros "[]".
  Qed.

  Global Instance make_or_false_l {PROP : bi} (P : PROP) : KnownLMakeOr False P P.
  Proof.
    rewrite /KnownLMakeOr /MakeOr.
    apply (anti_symm _).
    - iIntros "[[] | $]".
    - rewrite left_id //.
  Qed.

  Global Instance make_or_false_r {PROP : bi} (P : PROP) : KnownLMakeOr P False P.
  Proof.
    rewrite /KnownLMakeOr /MakeOr.
    apply (anti_symm _).
    - iIntros "[$ | []]".
    - rewrite right_id //.
  Qed.

(* (x1, y1) ≼ (x2, y2) ∨ (x1, y1) ≡ (x2, y2)  (q, n) ≼? (1, 2)  gives q = 1 and n = 2 or q < 1 and n ≤ 2
     q ≤ 1 and n ≤ 2 and (q < 1 or q = 1 and n = 2) *)
   (** TODO: this proof is horrible *)
  Global Instance prod_included_merge_unital {A1 A2} (x1 x2 : A1) (y1 y2 : A2) {Σ} P1_lt P1_le P2_lt P2_le P_lt P_le P_le' P_lt_case P_lt_case' P_case :
    IsIncludedMergeUnital x1 x2 (Σ := Σ) P1_lt P1_le → 
    IsIncludedMergeUnital y1 y2 (Σ := Σ) P2_lt P2_le →
    MakeAnd P1_le P2_le P_le' →
    MakeAnd P1_lt P2_lt P_lt →
    TCIf (HasRightId x2) (TCEq P_lt_case' True%I) (TCEq P_lt_case' P1_lt) →
    TCIf (HasRightId y2) (TCEq P_lt_case P_lt_case') (MakeAnd P_lt_case' P2_lt P_lt_case) →
    MakeOr P_lt_case (x1 ≡ x2 ∧ y1 ≡ y2)%I P_case → (* MakeOr will simplify True ∨ P ⊣⊢ True *)
    MakeAnd P_le' P_case P_le →
    IsIncludedMergeUnital (x1, y1) (x2, y2) (Σ := Σ) P_lt P_le.
    (* for option $ Qp * positive, this will give:
        IsIncludedMergeUnital (q1, p1) (q2, p2) (q1 < q2 ∧ p1 < p2) (q1 ≤ q2 ∧ p1 ≤ p2 ∧ ((q1 < q2 ∧ p1 < p2) ∨ (q1 ≡ q2 ∧ p1 ≡ p2)))
    *)
  Proof.
    rewrite /MakeAnd /MakeOr /HasRightId => HP1 HP2 HP1P2 HP1P2' HTC1 HTC2 HPcase HPle.
    split.
    { apply: prod_included_merge.
      * apply HP1.
      * apply HP2.
      * done. }
    rewrite prod_validI /= -HPle {HPle} -HPcase {HPcase} -HP1P2 {HP1P2} -HP1P2' {HP1P2'}.
    iIntros "[#H✓x #H✓y]".
    rewrite (persistent_dup (✓ y2)).
    rewrite (persistent_dup (✓ x2)).
    case: HP1 => + HP1. rewrite {1}HP1.
    case: HP2 => + HP2. rewrite {1}HP2.
    rewrite /IsIncludedMerge => HP1' HP2'.
    rewrite !bi_iff_wand_iff.
    iDestruct "H✓y" as "[HP2_lt_le H✓y]".
    iDestruct "H✓x" as "[HP1_lt_le H✓x]".
    iSplit.
    - rewrite prod_equivI /=. iIntros "[#[Hc1 Hc2]|#[Hc1 Hc2]] !>"; iSplit; try iSplit.
      + iApply bi.intuitionistically_elim. iApply "HP1_lt_le". eauto.
      + iApply bi.intuitionistically_elim. iApply "HP2_lt_le". eauto.
      + iLeft. case: HTC1; case: HTC2.
        * move => Hy -> Hx -> //.
        * move => <- Hx ->. iFrame "#".
        * move => Hy -> -> //.
        * move => <- ->. iFrame "#".
      + iApply bi.intuitionistically_elim. iApply "HP1_lt_le". eauto.
      + iApply bi.intuitionistically_elim. iApply "HP2_lt_le". eauto.
      + iRight; by iSplit.
    - rewrite prod_equivI /=.
      iIntros "#[[HP1_le HP2_le] [Hlt|[Hx Hy]]]"; last by iRight; iSplit.
      case: HTC1; case: HTC2.
      * move => Hy -> Hx ->.
        iDestruct ("HP1_lt_le" with "HP1_le") as "#[#$|H]".
        + iLeft.
          iDestruct ("HP2_lt_le" with "HP2_le") as "#[#$|Hy]".
          iApply (HP1' with "H✓y"). iRewrite "Hy". done.
        + iFrame "H". iDestruct ("HP2_lt_le" with "HP2_le") as "#[#$|$]".
          iLeft. iApply (HP2' with "H✓x"). iRewrite "H". done.
      * move => <- Hx ->. rewrite left_id.
        iFrame "Hlt".
        iDestruct ("HP1_lt_le" with "HP1_le") as "[#$|H]".
        iLeft.
        iApply (HP2' with "H✓x"). iRewrite "H". done.
      * move => Hy -> ->.
        iFrame "Hlt".
        iDestruct ("HP2_lt_le" with "HP2_le") as "[#$|H]".
        iLeft.
        iApply (HP1' with "H✓y"). iRewrite "H". done.
      * move => <- ->. by iLeft.
  Qed.
  Global Instance prod_right_id_both {A1 A2} (x1 : A1) (x2 : A2) :
    HasRightId x1 → HasRightId x2 → HasRightId (x1, x2).
  Proof.
    rewrite /HasRightId.
    case => c Hx1.
    case => c' Hx2.
    exists (c, c').
    by rewrite -pair_op -Hx1 -Hx2.
  Qed.

  Global Instance excl_valid_op {O : ofe} (a1 a2 : excl O) Σ:
    IsValidOp ExclBot a1 a2 Σ False%I.
  Proof. split; rewrite excl_validI /=; eauto. Qed.
  Global Instance excl_included_merge {O : ofe} (o1 o2 : excl O) {Σ} :
    IsIncludedMerge o1 o2 (Σ := Σ) False%I.
  Proof.
    rewrite /IsIncludedMerge. rewrite excl_validI. destruct o2 as [o2|]; last eauto.
    iIntros "_". iSplit; last eauto. iDestruct 1 as (c) "Hc". 
    unfold op, cmra_op => /=; unfold excl_op_instance.
    by rewrite excl_equivI.
  Qed.
  Global Instance excl_included_merge_unital {O : ofe} (o1 o2 : O) {Σ} :
    IsIncludedMergeUnital (Excl o1) (Excl o2) (Σ := Σ) False%I (o1 ≡ o2)%I.
  Proof.
    apply: Build_IsIncludedMergeUnital.
    rewrite bi_iff_wand_iff.
    iIntros "_"; iSplit.
    - iIntros "[%|#H] !>" => //.
      rewrite excl_equivI //.
    - iIntros "#H". iRight. by iRewrite "H".
  Qed.

  Global Instance agree_valid_op {O : ofe} (a1 a2 : O) Σ :
    IsValidOp (to_agree a1) (to_agree a1) (to_agree a2) Σ (a1 ≡ a2)%I.
  Proof.
    split; rewrite agree_validI agree_equivI; first eauto.
    iIntros "H".
    iRewrite "H".
    by rewrite agree_idemp.
  Qed.
  Global Instance agree_has_right_id {O : ofe} (a : O) : HasRightId (to_agree a).
  Proof. exists (to_agree a). by rewrite agree_idemp. Qed.
  Global Instance agree_included_merge {O : ofe} Σ (o1 o2 : O) :
    IsIncludedMerge (Σ := Σ) (to_agree o1) (to_agree o2) (o1 ≡ o2).
  Proof.
    rewrite /IsIncludedMerge.
    iIntros "H✓".
    iSplit. 
    - iIntros "H". iDestruct "H" as (c) "#H2".
      iRevert "H✓".
      iRewrite "H2". iIntros "#H✓".
      iAssert (to_agree o1 ≡ c)%I as "Hx"; last iClear "H✓".
      { by iApply agree_validI. }
      iRevert "H2". iRewrite -"Hx". rewrite agree_idemp agree_equivI.
      iIntros "#H1". by iRewrite "H1".
    - iIntros "#H". 
      iExists (to_agree o1). iRewrite -"H".
      by rewrite agree_idemp.
  Qed.
  Lemma to_agree_equiv {O : ofe} (o1 o2 : O) : to_agree o1 ≡ to_agree o2 → o1 ≡ o2.
  Proof.
    intros.
    apply to_agree_included.
    exists (to_agree o1).
    rewrite agree_idemp. by rewrite H.
  Qed.

  Global Instance auth_frag_valid_op {A : ucmra} (a a1 a2 : A) Σ P :
    IsValidOp a a1 a2 Σ P →
    IsValidOp (◯ a) (◯ a1) (◯ a2) Σ P.
  Proof.
    case => HP Ha.
    split; rewrite -auth_frag_op auth_frag_validI //.
    rewrite Ha.
    iIntros "H".
    by iRewrite "H".
  Qed.
  Lemma auth_auth_dfrac_op_validI Σ {A : ucmra} dq1 dq2 (a1 a2 : A) : ✓ (●{dq1} a1 ⋅ ●{dq2} a2) ⊣⊢@{iPropI Σ} ⌜✓ (dq1 ⋅ dq2)%Qp⌝ ∧ ✓ a1 ∧ (a1 ≡ a2).
  Proof.
    eapply (anti_symm _); last first.
    - iIntros "(% & Ha1 & Ha)".
      iRewrite -"Ha"; iStopProof.
      split => n.
      uPred.unseal => x Hx.
      rewrite /uPred_holds /= => Ha.
      apply auth_auth_dfrac_op_validN => //.
    - split => n.
      uPred.unseal => x Hx.
      rewrite /uPred_holds /= => /auth_auth_dfrac_op_validN [Hdqs [Ha Ha1]].
      rewrite /uPred_holds /=.
      rewrite /uPred_holds //=.
  Qed.
  Global Instance auth_auth_dfrac_own_valid_op {A : ucmra} (a1 a2 : A) (q q1 q2 : Qp) Σ Pq :
    IsValidOp q q1 q2 Σ Pq →
    IsValidOp (●{#q} a1) (●{#q1} a1) (●{#q2} a2) Σ (Pq ∧ a1 ≡ a2)%I.
  Proof.
    intros.
    split.
    - rewrite auth_auth_dfrac_op_validI.
      iIntros "(% & _ & #Ha)".
      rewrite bi.intuitionistically_and; iSplit; last done.
      destruct H as [<- _]. iPureIntro.
      rewrite dfrac_op_own in H0; revert H0 => /dfrac_valid_own Hq.
      by apply frac_valid.
    - rewrite auth_auth_dfrac_op_validI.
      iIntros "(% & _ & #Ha)".
      iRewrite -"Ha".
      rewrite -auth_auth_dfrac_op dfrac_op_own.
      iAssert (q ≡ q1 + q2)%Qp%I as "->" => //.
      destruct H as [_ <-]. iPureIntro.
      rewrite dfrac_op_own in H0; revert H0 => /dfrac_valid_own Hq.
      by apply frac_valid.
  Qed.
  Global Instance auth_auth_discard_valid_op {A : ucmra} (a1 a2 : A) Σ :
    IsValidOp (●□ a1) (●□ a1) (●□ a2) Σ (a1 ≡ a2)%I.
  Proof.
    intros.
    split.
    - rewrite auth_auth_dfrac_op_validI.
      iIntros "(% & _ & #$)".
    - rewrite auth_auth_dfrac_op_validI.
      iIntros "(% & _ & #Ha)".
      iRewrite -"Ha".
      by rewrite -auth_auth_dfrac_op dfrac_op_discarded.
  Qed.

  Global Instance auth_frac_frag_valid_op {A : cmra} (a a1 a2 : A) (q q1 q2 : Qp) Σ P1 P2 :
    IsValidOp q q1 q2 Σ P1 → 
    IsValidOp a a1 a2 Σ P2 →
    IsValidOp (◯F{q} a) (◯F{q1} a1) (◯F{q2} a2) Σ (P1 ∧ P2)%I.
  Proof.
    intros.
    apply auth_frag_valid_op, option_some_valid_op.
    by eapply prod_valid_op.
  Qed.

  Lemma view_uninjI {A B} {rel : view_rel A B} (v : viewR rel) (M : ucmra) :
    ✓ v ⊢@{uPredI M} match view_auth_proj v with
          | Some (dq, a) =>
            ∃ a', a ≡ to_agree a' ∧ v ≡ ●V{dq} a' ⋅ ◯V (view_frag_proj v)
          | None => v ≡ ◯V (view_frag_proj v)
          end.
  Proof.
    destruct v as [[[dq a]|] b] => /=; last eauto.
    uPred.unseal.
    split=> n y Hy.
    rewrite /upred.uPred_cmra_valid_def /= /validN /cmra_validN /= /view_validN_instance /=.
    case => Hdq [a' [Ha1 Ha2]].
    exists a'.
    split => //=.
    split => //=.
    - rewrite Ha1 right_id //.
    - by rewrite left_id.
  Qed.

  Lemma view_equivI {A B} {rel : view_rel A B} (vl vr : viewR rel) (M : ucmra) :
    vl ≡ vr ⊣⊢@{uPredI M} view_auth_proj vl ≡ view_auth_proj vr ∧ view_frag_proj vl ≡ view_frag_proj vr.
  Proof. uPred.unseal; split => n y Hy //. Qed.

  Lemma view_includedI {A B} {rel : view_rel A B} (vu vl : viewR rel) (M : ucmra) :
    vl ≼ vu ⊢@{uPredI M} ✓ vu -∗ view_frag_proj vl ≼ view_frag_proj vu ∧ 
      match view_auth_proj vu, view_auth_proj vl with
      | (Some (dqu, au)), (Some (dql, al)) => (Some dql) ≼ (Some dqu) ∧ al ≼ au (* this only gives al ≡ au if ✓ au, which is usually dependent on rel *)
      | None, (Some (dql, al)) => False
      | _, _ => True
      end.
  Proof.
    iIntros "[%el #H] #Hvu".
    iAssert (✓ el)%I as "Hel".
    { iRevert "Hvu". iRewrite "H". rewrite comm uPred.cmra_valid_weaken. eauto. }
    rewrite (view_uninjI el).
    destruct el as [[[dq3 a3]|] b_frac] => /=.
    - iDestruct "Hel" as "[%a' [#Ha #Hel]]".
      iRevert "H". iRewrite "Hel". iClear "Hel Ha". clear a3.
      destruct vu as [vua vuf]; destruct vl as [vla vlf] => /=.
      rewrite view_equivI /=.
      iIntros "[#Hva #Hvf]". rewrite right_id left_id. iSplit; first by iExists b_frac.
      destruct vua as [[vuq vua]|]; destruct vla as [[vlq vla]|] => //.
      * rewrite -Some_op /= -pair_op.
        rewrite option_equivI prod_equivI /=.
        iDestruct "Hva" as "[Hvq Hva]".
        iSplit; last by iExists _.
        iExists (Some dq3). rewrite -Some_op. by iRewrite "Hvq".
      * rewrite -Some_op /= -pair_op.
        by rewrite option_equivI.
    - iClear "Hel".
      rewrite view_equivI /= right_id.
      iDestruct "H" as "[Ha Hf]".
      iSplit; first by iExists b_frac.
      destruct vu as [vua vuf]; destruct vl as [vla vlf] => //=.
      destruct vua as [[vuq vua]|]; destruct vla as [[vlq vla]|] => //=; rewrite option_equivI //.
      rewrite prod_equivI /=.
      iDestruct "Ha" as "[Hq Ha]".
      iRewrite "Hq". iSplit; first by iExists None.
      iExists vla.
      rewrite agree_idemp //.
  Qed.

  Global Instance auth_auth_included {A : ucmra} (a1 a2 : A) (dq1 dq2 : dfrac) Σ Pq Pq' :
    IsIncludedMerge (Σ := Σ) (Some dq1) (Some dq2) Pq →
    MakeAnd Pq (a1 ≡ a2) Pq' →
    IsIncludedMerge (Σ := Σ) (●{dq1} a1) (●{dq2} a2) Pq'.
  Proof.
    rewrite /IsIncludedMerge /MakeAnd => Hdq <-.
    iIntros "#Ha2".
    iSplit.
    - iIntros "#Hi".
      rewrite view_includedI /=.
      iDestruct ("Hi" with "Ha2") as "(_ & Hdq & Ha1)"; iClear "Hi".
      rewrite auth_auth_dfrac_validI. iDestruct "Ha2" as "[% Ha2]".
      rewrite bi.intuitionistically_and.
      iSplit; first by iApply Hdq => //.
      rewrite is_included_merge'.
      iApply "Ha1" => //.
    - iIntros "[#HPq #Ha]".
      iRewrite "Ha"; iClear "Ha".
      rewrite auth_auth_dfrac_validI. iDestruct "Ha2" as "[% _]".
      iAssert (Some dq1 ≼ Some dq2)%I as "#[%dq Hqs]".
      iApply Hdq => //.
      destruct dq as [q|].
      * iExists (●{q} a2).
        rewrite -auth_auth_dfrac_op -Some_op.
        rewrite option_equivI. by iRewrite "Hqs".
      * rewrite right_id option_equivI.
        iRewrite "Hqs".
        iExists ε. rewrite right_id //.
  Qed.

  Global Instance frag_auth_included {A : ucmra} (a1 a2 : A) (dq2 : dfrac) Σ Pq :
    IsIncludedMerge (Σ := Σ) a1 ε Pq →
    IsIncludedMerge (Σ := Σ) (◯ a1) (●{dq2} a2) Pq.
  Proof.
    rewrite /IsIncludedMerge => Hdq.
    iIntros "#Ha2".
    iSplit.
    - iIntros "#Hi".
      rewrite view_includedI /=.
      iDestruct ("Hi" with "Ha2") as "[Hi' _ ]".
      iApply Hdq => //.
      eauto using ucmra_unit_valid.
    - iIntros "#Hpq".
      iAssert (a1 ≼ ε)%I as "[%a' #Ha1]".
      { iApply Hdq => //. eauto using ucmra_unit_valid. }
      iExists (◯ a' ⋅ ●{dq2} a2).
      rewrite assoc -auth_frag_op.
      rewrite view_equivI /= left_id right_id.
      iSplit => //.
  Qed.

  (* i guess in practice we will always have a2 = a3? *)
  Global Instance frag_auth_frag_included {A : ucmra} (a1 a2 a3 : A) (dq2 : dfrac) Σ Pq :
    IsIncludedMerge (Σ := Σ) a1 a3 Pq →
    IsIncludedMerge (Σ := Σ) (◯ a1) (●{dq2} a2 ⋅ ◯ a3) Pq.
  Proof.
    rewrite /IsIncludedMerge => Hdq.
    iIntros "#Ha2".
    iSplit.
    - iIntros "#Hi".
      rewrite view_includedI /= left_id right_id.
      iSpecialize ("Hi" with "Ha2").
      iApply Hdq => //.
      rewrite auth_both_dfrac_validI.
      iDestruct "Ha2" as "(_ & [%a' Ha] & Ha2)".
      iApply (uPred.cmra_valid_weaken _ a'). iRevert "Ha2". iRewrite "Ha". eauto.
    - iIntros "#Hpq".
      iAssert (a1 ≼ a3)%I as "[%a' #Ha1]".
      { iApply Hdq => //. rewrite comm uPred.cmra_valid_weaken auth_frag_validI //. }
      iExists (◯ a' ⋅ ●{dq2} a2).
      rewrite assoc -auth_frag_op.
      rewrite view_equivI /= !left_id !right_id.
      iSplit => //.
  Qed.

  Global Instance auth_auth_frag_included {A : ucmra} (a1 a2 a3 : A) (dq1 dq2 : dfrac) Σ Pq Pq' :
    IsIncludedMerge (Σ := Σ) (Some dq1) (Some dq2) Pq →
    MakeAnd Pq (a1 ≡ a2) Pq' →
    IsIncludedMerge (Σ := Σ) (●{dq1} a1) (●{dq2} a2 ⋅ ◯ a3) Pq'.
  Proof.
    rewrite /IsIncludedMerge /MakeAnd => Hdq <-.
    iIntros "#Ha2".
    iSplit.
    - iIntros "#Hi".
      rewrite view_includedI /=.
      iDestruct ("Hi" with "Ha2") as "(_ & Hdq & Ha1)"; iClear "Hi".
      rewrite !is_included_merge'.
      rewrite bi.intuitionistically_and.
      rewrite auth_both_dfrac_validI.
      iDestruct "Ha2" as "(% & Hai & Ha2)".
      iSplit; first by iApply Hdq => //.
      iApply "Ha1" => //.
    - iIntros "[#Hpq #Has]". iRewrite "Has"; iClear "Has".
      rewrite auth_both_dfrac_validI. iDestruct "Ha2" as "(% & _ & Ha2)".
      iAssert (Some dq1 ≼ Some dq2)%I as "#[%dq Hqs]".
      { iApply Hdq => //. }
      destruct dq as [q|].
      * iExists (●{q} a2 ⋅ ◯ a3).
        rewrite view_equivI /= left_id left_id right_id right_id.
        (* FIXME: using !left_id and/or !right_id raises:
          Anomaly "conversion was given unreduced term (FLambda)."*)
        iSplit => //.
        rewrite -!Some_op -pair_op.
        rewrite !option_equivI prod_equivI /= agree_idemp. iSplit => //.
      * rewrite right_id option_equivI.
        iRewrite "Hqs".
        by iExists (◯ a3).
  Qed.

  Global Instance auth_frag_auth_frag_included {A : ucmra} (a1 a2 a3 a4 : A) (dq1 dq2 : dfrac) Σ Pq Pa Pr' Pr :
    IsIncludedMerge (Σ := Σ) (Some dq1) (Some dq2) Pq →
    IsIncludedMerge (Σ := Σ) a4 a3 Pa →
    MakeAnd Pq (a1 ≡ a2) Pr' →
    MakeAnd Pr' Pa Pr →
    IsIncludedMerge (Σ := Σ) (●{dq1} a1 ⋅ ◯ a4) (●{dq2} a2 ⋅ ◯ a3) Pr.
  Proof.
    rewrite /IsIncludedMerge /MakeAnd => HPq HPa <- <-.
    iIntros "#Ha2".
    iSplit.
    - iIntros "#Hi".
      rewrite view_includedI /=.
      iDestruct ("Hi" with "Ha2") as "(Ha4 & Hdq & Ha1)"; iClear "Hi".
      rewrite !is_included_merge' !left_id.
      rewrite !bi.intuitionistically_and.
      rewrite auth_both_dfrac_validI.
      iDestruct "Ha2" as "(% & [%c Hac] & Ha2)".
      iSplit; try iSplit.
      * iApply HPq => //.
      * iApply "Ha1" => //.
      * iApply HPa => //.
        iApply uPred.cmra_valid_weaken. iRevert "Ha2". iRewrite "Hac". eauto.
    - iIntros "#[[#Hpq #Has] HPa]". iRewrite "Has"; iClear "Has".
      rewrite auth_both_dfrac_validI. iDestruct "Ha2" as "(% & [%c Hac] & Ha2)".
      iAssert (Some dq1 ≼ Some dq2)%I as "#[%dq Hqs]"; last iClear "Hpq".
      { iApply HPq => //. }
      iAssert (a4 ≼ a3)%I as "#[%c' Hc]"; last iClear "HPa".
      { iApply HPa => //.
        iApply uPred.cmra_valid_weaken. iRevert "Ha2". iRewrite "Hac". eauto. }
      destruct dq as [q|].
      * iExists (●{q} a2 ⋅ ◯ c').
        rewrite view_equivI /= left_id left_id left_id right_id right_id right_id.
        (* FIXME: using !left_id and/or !right_id raises:
          Anomaly "conversion was given unreduced term (FLambda)."*)
        iSplit => //.
        rewrite -!Some_op -pair_op.
        rewrite !option_equivI prod_equivI /= agree_idemp. iSplit => //.
      * rewrite right_id option_equivI.
        iRewrite "Hqs".
        iExists (◯ c'). rewrite view_equivI /= right_id !left_id. eauto.
  Qed.

  Global Instance auth_frag_included {A : ucmra} (a1 a2 : A) (dq2 : dfrac) Σ :
    IsIncludedMerge (Σ := Σ) (●{dq2} a2) (◯ a1) False.
  Proof.
    rewrite /IsIncludedMerge.
    iIntros "#Ha2".
    iSplit => //.
    iIntros "#Hi".
    rewrite view_includedI /=.
    iDestruct ("Hi" with "Ha2") as "[_ $]".
  Qed.

  Lemma frag_frag_included {A : ucmra} (a1 a2 : A) Σ Pq :
    IsIncludedMerge (Σ := Σ) a1 a2 Pq →
    IsIncludedMerge (Σ := Σ) (◯ a1) (◯ a2) Pq.
  Proof.
    rewrite /IsIncludedMerge => HPq.
    iIntros "#Ha2".
    iSplit => //.
    - iIntros "#Hi".
      rewrite view_includedI /=.
      iDestruct ("Hi" with "Ha2") as "[Ha1a2 _]".
      iApply HPq => //.
      rewrite auth_frag_validI //.
    - iIntros "#HPq".
      iAssert (a1 ≼ a2)%I as "[%a Ha]".
      { iApply HPq => //. rewrite auth_frag_validI //. }
      iExists (◯ a).
      rewrite -auth_frag_op view_equivI /=.
      iSplit => //.
  Qed.

End recursive.