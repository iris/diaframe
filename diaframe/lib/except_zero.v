From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode environments.
From diaframe Require Import proofmode_base.


(* This file, when imported, makes Diaframe strip the except_0 modality
   of hypothesis when possible, as well as stripping ▷s of Timeless propositions
   whenever the goal is an except 0. *)

Section except_zero_drop_modal.
  Context {PROP : bi}.
  Implicit Types P Q R : PROP.

  Class IsRecursiveExcept0 P := 
    is_recursive_except_0 : ◇ P ⊢ P.

  Global Instance timeless_drop_later_if_except0 R :
    TCNoBackTrack (IsRecursiveExcept0 R) →
    DropModal (PROP := PROP) (bi_later)%I R Timeless.
  Proof.
    rewrite /DropModal /IsRecursiveExcept0 => [[HR]] P HP.
    rewrite -{2}HR.
    iIntros "HPR >HP !>".
    by iApply "HPR".
  Qed.

  Global Instance drop_except_0_if_except0 R :
    TCNoBackTrack (IsRecursiveExcept0 R) →
    DropModal (PROP := PROP) (bi_except_0)%I R (λ _, TCTrue).
  Proof.
    rewrite /DropModal /IsRecursiveExcept0 => [[HR]] P HP.
    rewrite -{2}HR.
    iIntros "HPR >HP !>".
    by iApply "HPR".
  Qed.

  Global Instance except_0_into_modal p P R :
    IntoExcept0 P R →
    IntoModal p P bi_except_0 R | 30. 
    (* necessary for stripping off ⋄ on introduction. TODO: dont reuse intomodal for this *)
  Proof.
    rewrite /IntoExcept0 /IntoModal bi.intuitionistically_if_elim => -> //.
  Qed.
End except_zero_drop_modal.

Section except_zero_instances.
  Context {PROP : bi}.
  Implicit Types P Q R : PROP.

  Global Instance bi_laterN_except_0 P m : 
    SolveSepSideCondition (m ≥ 1)%nat →
    IsExcept0 (Transform $ ▷^m P)%I.
  Proof.
    rewrite /SolveSepSideCondition /IsExcept0.
    unseal_diaframe => Hm.
    destruct m; first by contradict Hm; lia.
    simpl => {Hm}.
    by iMod 1 as "HP".
  Qed.

  Global Instance bi_later_except_0 P :
    IsExcept0 (Transform $ ▷ P)%I.
  Proof.
    change bi_later with (bi_laterN (PROP := PROP) 1)%I.
    eapply bi_laterN_except_0.
    rewrite /SolveSepSideCondition; lia.
  Qed.

  Global Instance solve_sep_is_except_0 {TT} (M : PROP → PROP) A G :
    (∀ P, IsExcept0 (M P)) →
    IsExcept0 (SolveSep (TT := TT) M A G).
  Proof. unseal_diaframe; eauto. Qed.

  Global Instance solve_sep_foc_is_except_0 {TT} (M : PROP → PROP) A G D :
    (∀ P, IsExcept0 (M P)) →
    IsExcept0 (SolveSepFoc (TT := TT) M A G D).
  Proof. unseal_diaframe; eauto. Qed.

  Global Instance solve_one_foc_is_except_0 {TT} (M : PROP → PROP) A G :
    (∀ P, IsExcept0 (M P)) →
    IsExcept0 (SolveOneFoc (TT := TT) M A G).
  Proof. unseal_diaframe; eauto. Qed.

  Global Instance solve_one_is_except_0 {TT} (M : PROP → PROP) A :
    (∀ P, IsExcept0 (M P)) →
    IsExcept0 (SolveOne (TT := TT) M A).
  Proof. unseal_diaframe; eauto. Qed.


  (* now the recursive instances: *)
  Global Instance recurse_except0_forall `(P : A → PROP) :
    (∀ a, IsRecursiveExcept0 (P a)) →
    IsRecursiveExcept0 (∀ a, P a).
  Proof.
    rewrite /IsRecursiveExcept0 => HP.
    iIntros "HP" (a).
    iApply HP. iMod "HP". iApply "HP".
  Qed.

  Global Instance recurse_except0_introvars `(R : TT -t> PROP) :
    (TC∀.. (tt : TT), IsRecursiveExcept0 (tele_app R tt)) →
    IsRecursiveExcept0 (IntroduceVars (TT := TT) R).
  Proof.
    rewrite /IsRecursiveExcept0 => HP.
    unseal_diaframe.
    iIntros "HP" (tt).
    iApply HP. iMod "HP". iApply "HP".
  Qed.

  Global Instance recurse_except0_wand P Q :
    IsRecursiveExcept0 Q →
    IsRecursiveExcept0 (P -∗ Q).
  Proof.
    rewrite /IsRecursiveExcept0 => -{2}<-. 
    iIntros ">HPQ HP".
    iSpecialize ("HPQ" with "HP").
    iApply "HPQ".
  Qed.

  Global Instance recurse_except0_introducehyp P Q :
    IsRecursiveExcept0 Q →
    IsRecursiveExcept0 (IntroduceHyp P Q).
  Proof. unseal_diaframe. tc_solve. Qed.

  Global Instance recurse_except0_and P Q :
    IsRecursiveExcept0 P →
    IsRecursiveExcept0 Q →
    IsRecursiveExcept0 (P ∧ Q).
  Proof.
    rewrite /IsRecursiveExcept0 => {2}<- {2}<-.
    apply bi.and_intro; [rewrite bi.and_elim_l | rewrite bi.and_elim_r ]; done.
  Qed.

  Global Instance recurse_except0_solve_and P Q :
    IsRecursiveExcept0 P →
    IsRecursiveExcept0 Q →
    IsRecursiveExcept0 (SolveAnd P Q).
  Proof. unseal_diaframe. tc_solve. Qed.


  (* and the base case *)
  Global Instance recurse_except0_basecase P :
    IsExcept0 P →
    IsRecursiveExcept0 P | 100.
  Proof. done. Qed.
End except_zero_instances.