From iris.bi Require Export bi telescopes lib.fixpoint.
From iris.proofmode Require Import proofmode environments classes_make.
From diaframe Require Import proofmode_base.

Set Default Proof Using "Type".

(* Defines a 'run_greatest_fixpoint' construct, which will automatically
   apply the introduction rule for greatest fixpoints.
   Useful since "AU = greatest_fixpoint AACC" *)


Global Instance unit_funs_ne {PROP : bi} (F : unit → PROP) : NonExpansive F.
Proof. move => n1 [] [] //. Qed.


Section greatest_fixpoint_lemmas.
  (* some lemmas that were elucidating to me, about how greatest_fixpoints behave logically *)
  Context {PROP : bi}.

  Section general_ofe.
    Context {A : ofe}.
    Implicit Types a : A.

    Lemma greatest_fixpoint_strong_mono' (F G : (A → PROP) → (A → PROP)) :
      BiMonoPred F →
      □ (∀ Φ x, F Φ x -∗ G Φ x) -∗
      ∀ x, bi_greatest_fixpoint F x -∗ bi_greatest_fixpoint G x.
    Proof.
      iIntros (HF) "#Hmon". iApply greatest_fixpoint_coiter.
      iIntros "!>" (y) "IH". rewrite greatest_fixpoint_unfold.
      by iApply "Hmon".
    Qed.

    Lemma bi_gfp_strong (F : (A → PROP) → (A → PROP)) R a :
      BiMonoPred F →
      R ∗ bi_greatest_fixpoint F a
        ⊢ bi_greatest_fixpoint (λ P u, R ∗ F (λ u', R -∗ P u') u) a.
    Proof.
      move => HF. iRevert (a).
      iApply greatest_fixpoint_coiter; first solve_proper.
      iIntros "!>" (a) "[$ HF]".
      rewrite {1}greatest_fixpoint_unfold.
      iRevert "HF".
      iRevert (a).
      destruct HF.
      iApply bi_mono_pred; first solve_proper.
      iIntros "!>" (x) "$ $".
    Qed.
  End general_ofe.

  Section unit_ofe.
    (* one would expect a similar result would hold for general A, but I got into trouble proving this:
       we seem to need that some functions of which we have too little information are NonExpansive.
       This follows trivially for A = unit, see unit_funs_ne *)
    Lemma bi_gfp_strong_subtract (F : (() → PROP) → (() → PROP)) R :
      BiMonoPred F →
      bi_greatest_fixpoint (λ P u, R -∗ F (λ u', R ∗ P u') u) ()
        ⊢ R -∗ bi_greatest_fixpoint F ().
    Proof.
      iIntros (HF) "HF HR".
      iCombine "HR HF" as "HF".
      erewrite bi_gfp_strong.
      iRevert "HF".
      generalize () => u.
      iRevert (u).
      iApply greatest_fixpoint_strong_mono'; last first.
      iIntros "!>" (Φ u) "[HR HFR]".
      iSpecialize ("HFR" with "HR").
      iRevert "HFR". iRevert (u).
      destruct HF.
      iApply bi_mono_pred.
      iIntros "!>" (x) "[HR HΦR]".
      by iApply "HΦR".
      all: split; try tc_solve.
      - move => Φ Ψ _ _.
        iIntros "#HΦΨ" (x) "[$ HF] HR".
        iSpecialize ("HF" with "HR").
        iRevert "HF". iRevert (x).
        destruct HF.
        iApply bi_mono_pred.
        iIntros "!>" (x) "[$ HF] HR".
        iApply "HΦΨ".
        by iApply "HF".
      - move => Φ Ψ _ _.
        iIntros "#HΦΨ" (x) "HF HR".
        iSpecialize ("HF" with "HR").
        iRevert "HF". iRevert (x).
        destruct HF.
        iApply bi_mono_pred.
        iIntros "!>" (x) "[$ HF]".
        by iApply "HΦΨ".
    Qed.
  End unit_ofe.

End greatest_fixpoint_lemmas.


Fixpoint big_star_no_emp {PROP : bi} (Ps : list PROP) : PROP :=
  match Ps with
  | [] => emp
  | [P] => P
  | P :: Ps => P ∗ big_star_no_emp Ps
  end.

Global Notation "'[`∗`]' Ps" := (big_star_no_emp Ps) (at level 20) : bi_scope.

Lemma big_star_no_emp_equiv {PROP : bi} (Ps : list PROP) :
  [∗] Ps ⊣⊢ [`∗`] Ps.
Proof.
  induction Ps => //.
  destruct Ps.
  - rewrite /= right_id //.
  - revert IHPs => /= <- //.
Qed.


Section glp_lemmas.
  Context {PROP : bi}.

  (* lazy sealing :o *)
  Definition run_greatest_fixpoint_def (F : PROP → PROP) : PROP :=
    bi_greatest_fixpoint (λ P _, F $ P ()) ().
  Definition run_greatest_fixpoint_aux : seal (@run_greatest_fixpoint_def). Proof. by eexists. Qed.
  Definition run_greatest_fixpoint := run_greatest_fixpoint_aux.(unseal).
  Global Opaque run_greatest_fixpoint.
  Global Arguments run_greatest_fixpoint F : simpl never.
  Lemma run_greatest_fixpoint_eq : @run_greatest_fixpoint = @run_greatest_fixpoint_def.
  Proof. by rewrite -run_greatest_fixpoint_aux.(seal_eq). Qed.

  Lemma run_greatest_fixpoint_wand F R :
    □ (R -∗ F R) ⊢ R -∗ run_greatest_fixpoint F.
  Proof.
    rewrite run_greatest_fixpoint_eq /run_greatest_fixpoint_def.
    iIntros "#HF HR".
    iApply (greatest_fixpoint_coiter _ (λ _, R)); last iFrame.
    by iIntros "!>" ([]).
  Qed.
  (* use MergeMod to accomplish this *)

  Global Instance gfp_as_solve_goal F M :
    IntroducableModality M →
    AsSolveGoal M (run_greatest_fixpoint F)%I (Transform $ run_greatest_fixpoint F).
  Proof. unseal_diaframe; rewrite /AsSolveGoal. eauto. Qed.

  Global Instance gfp_introduce_all_laterable Δ F :
    TRANSFORM Δ, run_greatest_fixpoint F =[ctx]=> F ([`∗`] env_spatial Δ)%I | 30.
  Proof.
    unseal_diaframe; rewrite /TransformCtx.
    rewrite envs_entails_unseal of_envs_alt.
    assert (⌜envs_wf Δ⌝ ∧ □ [∧] env_intuitionistic Δ ⊢ □ (⌜envs_wf Δ⌝ ∧ □ [∧] env_intuitionistic Δ)).
    { iIntros "[% #HΔ] !>". eauto. }
    rewrite bi.persistent_and_sep_assoc.
    revert H. generalize (⌜envs_wf Δ⌝ ∧ □ [∧] env_intuitionistic Δ)%I => R HR HRΔ.
    apply bi.wand_elim_l'.
    rewrite big_star_no_emp_equiv -run_greatest_fixpoint_wand {1}HR.
    apply bi.intuitionistically_mono'.
    rewrite -{1}big_star_no_emp_equiv.
    by apply bi.wand_intro_r.
  Qed.
End glp_lemmas.



