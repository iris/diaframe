From iris.bi Require Import bi fractional.
From iris.proofmode Require Import tactics.
From iris.algebra Require Import auth numbers.
From iris.base_logic Require Import base_logic own.

From diaframe Require Import proofmode_base.
From diaframe.lib Require Import own_hints.


(* Library for Diaframe to deal with the case where we want to represent 
   an integer as the difference between two (rising) natural numbers. 
   Since this is an equation with 1 constraint and two unknowns, Diaframe
   cannot handle this out of the box. Instead, we use some Ltac to find the desired solution *)


Section nat_diff.
  Context {PROP : bi}.

  Definition int_as_nat_diff' (z : Z) (n1 n2 : nat) : Prop := z = (n1 - n2)%Z.
  Definition int_as_nat_diff (z : Z) (n1 n2 : nat) : PROP := ⌜int_as_nat_diff' z n1 n2⌝.

  Global Instance consume_nat_diff z n1 n2 : MergableConsume (int_as_nat_diff z n1 n2) true (λ p Pin Pout,
    TCAnd (TCEq Pin (ε₀)%I) $
          TCEq Pout ⌜z = (n1 - n2)%Z⌝%I).
  Proof.
    rewrite /MergableConsume /= => p Pin Pout [-> ->].
    iSteps.
  Qed.

  Global Instance nat_diff_unfold z n1 n2 : 
    HINT ε₀ ✱ [- ; ⌜int_as_nat_diff' z n1 n2⌝] ⊫ [id]; int_as_nat_diff z n1 n2 ✱ [True].
  Proof. iSteps. Qed.

  Global Instance nat_diff_timeless z n1 n2 : Timeless (int_as_nat_diff z n1 n2).
  Proof. tc_solve. Qed.

End nat_diff.



Global Opaque int_as_nat_diff.

Notation NtoZ n := (Z.of_nat n).

Ltac solve_int_as_nat_diff := 
  lazymatch goal with
  | |- int_as_nat_diff' ?z ?n1 ?n2 =>
(*    is_evar n1; is_evar n2; *)
    match z with
    | Z0 => unify n1 O; unify n2 O; reflexivity
    | _ => 
    let base_vars :=
      match z with
      | (NtoZ ?oi - NtoZ ?od)%Z => constr:((oi, od, Z0))
      | (NtoZ ?oi - NtoZ ?od + ?k)%Z => constr:((oi, od, k))
      | (NtoZ ?oi - NtoZ ?od - ?k)%Z => constr:((oi, od, Z.opp k))
      end in
    match base_vars with
    | (?oi, ?od, ?k) =>
    let change_pair :=
      match k with
      | Z0 => constr:((true, O))
      | NtoZ ?k => constr:((true, k))
      | Zpos ?k => constr:((true, Pos.to_nat k))
      | Zneg ?k => constr:((false, Pos.to_nat k))
      | Z.opp (Zpos ?k) => constr:((false, Pos.to_nat k))
      | Z.opp (NtoZ ?k) => constr:((false, k))
      end in
    let unif_pair := 
      match goal with
      | _ : (NtoZ ?ni - NtoZ ?nd)%Z = (NtoZ ?oi - NtoZ ?od)%Z |- _ =>
        constr:((ni, nd))
      | |- _ =>
        constr:((oi, od))
      end in
    match unif_pair with
    | (?ni, ?nd) =>
    match change_pair with
    | (?b, ?k) =>
      let k' := eval cbn in k in
      match b with
      | true => 
        match k' with
        | O => unify n1 ni
        | _ => unify n1 (ni + k')
        end; unify n2 nd
      | false => unify n1 ni; unify n2 (nd + k')
      end;
      rewrite /int_as_nat_diff'; lia
    end
    end
    end
    end
  end.

Global Hint Extern 4 (int_as_nat_diff' ?z ?n1 ?n2) => solve_int_as_nat_diff : solve_pure_add.
















