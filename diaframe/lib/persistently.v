From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode environments.
From diaframe Require Import proofmode_base.


(* Makes Diaframe support the <pers> modality in goals. *)

Section persistently.
  Context {PROP : bi}.
  Implicit Type P : PROP.

  Global Instance persistently_as_solve_goal P M :
    IntroducableModality M →
    AsSolveGoal M (<pers> P)%I (Transform $ <pers> P)%I.
  Proof. unseal_diaframe; rewrite /AsSolveGoal //. eauto. Qed.


  Global Instance persistently_transform_hyp P R : 
    TRANSFORM □⟨false⟩ P, <pers> R =[hyp]=> <pers> R.
  Proof.
    rewrite /TransformHyp /=.
    iIntros "$". eauto.
  Qed.

  Global Instance persistently_transfrom_ctx Δ P :
    TCEq (env_spatial Δ) Enil → TRANSFORM Δ, <pers> P =[ctx]=> P | 30.
  Proof.
    rewrite /TransformCtx => HM.
    rewrite envs_entails_unseal.
    rewrite {2}env_spatial_is_nil_intuitionistically; last rewrite /env_spatial_is_nil HM //.
    move => ->. eauto.
  Qed.
End persistently.