From iris.algebra Require Import local_updates excl auth lib.frac_auth csum.

(* Diaframe hint library aimed at automatically deriving good hints for owned propositions.
   Supports most of the (recursive) building blocks for cmra's found in iris.algebra *)

Class FindLocalUpdate {A : cmra} (x x' y y' : A) (φ : Prop) :=
  find_local_update : φ → (x, y) ~l~> (x', y').
Global Hint Mode FindLocalUpdate ! ! ! - - - : typeclass_instances.

Section base_local_updates.
  Lemma find_local_update_incomparable {A : ucmra} (x x' : A) :
    Cancelable x →
    (* This is needed to find things like 
        (Some (Cinl x), Some (Cinl x)) ~l~> (Some (Cinr x'), Some (Cinr x')) 
      i.e. for ucmra's with incomparable elements *)
    FindLocalUpdate x x' x x' (✓ x').
  Proof.
    (* however, it is always applicable for cmras with Cancelable x. That's bad
       the current workaround is to add a custom instance for swapping Cinl to Cinr *)
    rewrite /FindLocalUpdate => Hx Hx'.
    etrans.
    rewrite -[x in (x, _)]right_id.
    by apply cancel_local_update_unit.
    apply local_update_unital => n z Heps.
    rewrite left_id => <-.
    rewrite right_id.
    split => //.
    by apply cmra_valid_validN.
  Qed. (*

  Global Instance find_local_update_unital {A : ucmra} (x : A ) :
    FindLocalUpdate x x ε ε True | 999. not necessary since we have the better UcmraSubtract instance..?
    This instance is counterproductive for cmras which have idempotent elements/joinlike ops: (MinNat, MaxNat, regular set)
    but its useful/fast for cmras which do not have that: nat, positive, GSet, GMultiset. how to improve..?
  Proof. done. Qed. *)

  Global Instance find_local_update_from_prod_both {A B : cmra} (x1 x1' y1 y1' : A) (x2 x2' y2 y2' : B) φ1 φ2 :
    FindLocalUpdate x1 x1' y1 y1' φ1 →
    FindLocalUpdate x2 x2' y2 y2' φ2 →
    FindLocalUpdate (x1, x2) (x1', x2') (y1, y2) (y1', y2') (φ1 ∧ φ2) | 150.
  Proof.
    rewrite /FindLocalUpdate => H1 H2 [/H1 H1' /H2 H2'].
    apply prod_local_update' => //.
  Qed.

  Global Instance find_local_update_from_prod_left {A B : cmra} (x1 x1' y1 y1' : A) (x2 y2 : B) φ :
    FindLocalUpdate x1 x1' y1 y1' φ →
    FindLocalUpdate (x1, x2) (x1', x2) (y1, y2) (y1', y2) φ | 160.
  Proof.
    rewrite /FindLocalUpdate => H1 /H1 H1'.
    apply prod_local_update_1 => //.
  Qed.

  Global Instance find_local_update_from_prod_right {A B : cmra} (x1 y1 : A) (x2 x2' y2 y2' : B) φ :
    FindLocalUpdate x2 x2' y2 y2' φ →
    FindLocalUpdate (x1, x2) (x1, x2') (y1, y2) (y1, y2') φ | 160.
  Proof.
    rewrite /FindLocalUpdate => H1 /H1 H1'.
    apply prod_local_update_2 => //.
  Qed.

  Global Instance find_local_update_some {A : cmra} (x x' y y' : A) φ :
    FindLocalUpdate x x' y y' φ →
    FindLocalUpdate (Some x) (Some x') (Some y) (Some y') φ | 210.
  Proof. move => H /H H'. by apply option_local_update. Qed.

  Global Instance find_local_update_exclusive {A : ofe} (x x' y : exclR A) :
    FindLocalUpdate x x' y x' (✓ x') | 150.
  Proof. move => Hx1 Hx2. by apply exclusive_local_update. Qed.

  Global Instance sum_inl_find_local_update {A1 A2 : cmra} (x x' y y' : A1) φ :
    FindLocalUpdate x x' y y' φ →
    FindLocalUpdate (Cinl (B:=A2) x) (Cinl x') (Cinl y) (Cinl y') φ | 150.
  Proof. move => H /H H'. by apply csum_local_update_l. Qed.

  Global Instance sum_inr_find_local_update {A1 A2 : cmra} (x x' y y' : A1) φ :
    FindLocalUpdate x x' y y' φ →
    FindLocalUpdate (Cinr (A:=A2) x) (Cinr x') (Cinr y) (Cinr y') φ | 150.
  Proof. move => H /H H'. by apply csum_local_update_r. Qed.

  Global Instance sum_inl_inr_swap_local_update {A1 A2 : cmra} (x : A1) (x' : A2) :
    Cancelable (Some $ Cinl (B :=A2) x) →
    FindLocalUpdate (Some $ Cinl (B:=A2) x) (Some $ Cinr (A:=A1) x') (Some $ Cinl (B:=A2) x)
         (Some $ Cinr (A:=A1) x') (✓ x') | 160.
  Proof. eapply (find_local_update_incomparable (A := optionUR $ csumR A1 A2)). Qed.

  Global Instance sum_inr_inl_swap_local_update {A1 A2 : cmra} (x : A1) (x' : A2) :
    Cancelable (Some $ Cinr (A :=A2) x) →
    FindLocalUpdate (Some $ Cinr (A:=A2) x) (Some $ Cinl (B:=A1) x') (Some $ Cinr (A:=A2) x)
         (Some $ Cinl (B:=A1) x') (✓ x') | 160.
  Proof. eapply (find_local_update_incomparable (A := optionUR $ csumR A2 A1)). Qed.

End base_local_updates.

Class AsCmraUnit {A : ucmra} (x : A) :=
  as_cmra_unit : x = ε.

Section base_as_unit.
  Implicit Types A : ucmra.

  Global Instance unit_as_unit {A} : AsCmraUnit (A := A) ε | 99.
  Proof. done. Qed.

  Global Instance as_unit_to_frag `(x : A) :
    AsCmraUnit x →
    AsCmraUnit (◯ x).
  Proof. by rewrite /AsCmraUnit => ->. Qed.

  Global Instance as_unit_to_prod `(a1 : A1) `(a2 : A2) :
    AsCmraUnit a1 → AsCmraUnit a2 →
    AsCmraUnit (a1, a2).
  Proof. by rewrite /AsCmraUnit => -> ->. Qed.

End base_as_unit.

From iris.algebra Require Import numbers.
From diaframe Require Import solve_defs util_classes.

Section number_updates.
  Global Instance natO_local_update (x x' y : nat) :
    FindLocalUpdate x x' y (x' + y - x) (x ≤ x' + y) | 150.
  Proof.
    move => Hx.
    apply nat_local_update.
    lia.
  Qed.
  Global Instance natO_unit n :
     SolveSepSideCondition (n = 0) →
     AsCmraUnit (A := natUR) n | 10.
  Proof. done. Qed.
  Global Instance natO_unit' :
     AsCmraUnit (A := natUR) 0 | 5. (* separate instance since this will instantiate evars *)
  Proof. apply _. Qed.

  Global Instance natmax_local_update (x x' y : nat) :
    FindLocalUpdate (MaxNat x) (MaxNat x') (MaxNat y) (MaxNat x') (x ≤ x') | 150.
  Proof.
    move => Hxx'.
    by apply max_nat_local_update.
  Qed.
  Global Instance natmax_unit n : 
    SolveSepSideCondition (n = 0) →
    AsCmraUnit (A := max_natUR) (MaxNat n).
  Proof. rewrite /SolveSepSideCondition => ->. done. Qed.
  Global Instance natmax_unit' : 
    AsCmraUnit (A := max_natUR) (MaxNat 0).
  Proof. apply _. Qed.

  Global Instance natmin_local_update (x x' y : nat) :
    FindLocalUpdate (MinNat x) (MinNat x') (MinNat y) (MinNat x') (x' ≤ x) | 150.
  Proof.
    move => Hxx'.
    by apply min_nat_local_update.
  Qed.

  Global Instance Z_find_local_update (x x' y : Z) :
    FindLocalUpdate x x' y (x' + y - x)%Z True | 150.
  Proof.
    move => _.
    apply Z_local_update.
    lia.
  Qed.

  Global Instance Z_unit : AsCmraUnit (A := ZUR) 0%Z.
  Proof. done. Qed.

  Lemma positive_local_update (x y x' y' : positive) :
    (x + y' = x' + y)%positive → (x,y) ~l~> (x',y').
  Proof.
    move => Heq ?.
    apply local_update_discrete => [[p|]] /= _; fold_leibniz.
    - rewrite !pos_op_add => Hyp.
      split => //. lia.
    - intros. split => //. lia.
  Qed.

  Global Instance positive_update (x y x' : positive) :
    FindLocalUpdate x x' y (x' + y - x)%positive (x < x' + y)%positive | 150.
  Proof. (* the strict equality stems from the fact that y' needs to be positive here *)
    move => Hx.
    apply positive_local_update.
    lia.
  Qed.

  Lemma Qp_add_eq_mono_l p q r : (p = q) ↔ (r + p = r + q)%Qp.
  Proof.
    split => [-> //|Hrpq].
    apply (anti_symm (≤)%Qp).
    + eapply Qp.add_le_mono_l.
      by erewrite Hrpq.
    + eapply Qp.add_le_mono_l.
      by erewrite Hrpq.
  Qed.

  Lemma frac_local_update (q r q' r' : Qp) :
    (q' ≤ 1)%Qp →
    (q + r' = q' + r)%Qp → (q, r) ~l~> (q', r').
  Proof.
    move => ? Heq ?.
    apply local_update_discrete => [[p|]] /= /frac_valid Hp; fold_leibniz.
    - rewrite !frac_op => Hyp. subst.
      assert (p + r' = q')%Qp as Hprq.
      { eapply Qp_add_eq_mono_l. rewrite assoc. erewrite Heq. by rewrite (Qp.add_comm q' r). }
      split => //.
      rewrite -Hprq.
      by rewrite (Qp.add_comm p r').
    - intros. subst. revert Heq.
      rewrite (Qp.add_comm q' r).
      move => /(Qp_add_eq_mono_l r' q' r) Heq. subst.
      split => //.
  Qed.

  (* note that we cannot really do subtraction in Qp! so above is not that useful in practice *)

  Global Instance frac_update_add_right (q r r' : Qp) :
    FindLocalUpdate q (q + r')%Qp r (r' + r)%Qp (q + r' ≤ 1)%Qp | 150.
  Proof.
    move => Hx.
    apply frac_local_update => //.
    by rewrite assoc.
  Qed.
  Global Instance frac_update_add_left (q r r' : Qp) :
    FindLocalUpdate q (r' + q)%Qp r (r' + r)%Qp (r' + q ≤ 1)%Qp | 150.
  Proof.
    move => Hx.
    apply frac_local_update => //.
    by rewrite assoc (Qp.add_comm r' q).
  Qed.
  (* subtraction does not work: results are in Qp, so must be > 0. Half subtraction would work *)

End number_updates.

From iris.algebra Require Import gset.

Section set_updates.
  Context `{Countable K}.
  Implicit Types X Y : gset K.

  Global Instance set_union_update X X':
    FindLocalUpdate X X' ε X' (X ⊆ X') | 150.
  Proof.
    move => HX.
    by apply gset_local_update.
  Qed.
  Global Instance set_unit : AsCmraUnit (A := gsetUR K) ∅.
  Proof. done. Qed.

  Lemma set_helper Z X Y : X ∩ Z = Y ∩ Z → X ∖ Z = Y ∖ Z → X = Y.
  Proof.
    intros.
    apply set_eq => x.
    destruct (decide (x ∈ Z)); set_solver.
  Qed.

  Lemma set_helper2 X Y : X ∖ Y ∩ Y = ∅.
  Proof. set_solver. Qed.

  Lemma set_helper3 Y Z : Y ∖ (Y ∖ Z) = Y ∩ Z.
  Proof.
    apply set_eq => x; split; last set_solver.
    move => /elem_of_difference [HxY +].
    move => /not_elem_of_difference [HnxY | HxZ]; set_solver.
  Qed.

  Lemma set_helper4 X Y Z : X ∖ (Y ∖ Z) = X ∖ Y ∪ X ∩ Z.
  Proof.
    apply set_eq => x; split; last set_solver.
    move => /elem_of_difference [HxY +].
    move => /not_elem_of_difference [HnxY | HxZ]; set_solver.
  Qed.

  Lemma set_local_update X X' Y Y' :
   X ∖ Y = X' ∖ Y' → Y' ⊆ X' → (GSet X, GSet Y) ~l~> (GSet X', GSet Y').
  Proof.
    intros.
    apply local_update_total_valid=> _ _ /gset_disj_included HYX.
    rewrite local_update_unital_discrete=> -[Z|] _ /leibniz_equiv_iff //=.
    rewrite {1}/op /=. unfold cmra_op =>/=.
    unfold ucmra_op => /=.
    destruct (decide _) as [HXf|]; [intros[= ->]|done].
    split => //.
    rewrite gset_disj_union ; last set_solver.
    f_equal.
    revert H0.
    replace ((Y ∪ Z) ∖ Y) with Z; last set_solver.
    move => ->.
    by rewrite -union_difference_L.
  Qed.

  Global Instance set_disj_union_update X X' Y :
    FindLocalUpdate (GSet X) (GSet X') (GSet Y) (GSet (X' ∖ X ∪ X' ∩ Y)) (X ∖ Y ⊆ X') | 150.
  Proof.
    move => HX.
    apply local_update_total_valid=> _ _ /gset_disj_included HYX.
    apply set_local_update; last set_solver.
    rewrite -set_helper4.
    rewrite set_helper4.
    set_solver.
  Qed.
  Global Instance set_disj_union_eq X :
    (* this one is not strictly necessary, but speeds things up and keeps them well readable. 
        perhaps make this a real instance for selected cmra's? precisely those who are not a lattice but have actual ownership..?
       or equate the extra resource to the core of the element...? *)
    FindLocalUpdate (GSet X) (GSet X) (GSet ∅) (GSet ∅) True | 149.
  Proof.
    move => HX.
    apply set_local_update; set_solver.
  Qed.
  Global Instance set_disj_unit : AsCmraUnit (A := gset_disjUR K) (GSet ∅).
  Proof. done. Qed.

End set_updates.

From iris.algebra Require Import gmultiset.

Section multiset_updates.
  Context `{Countable K}.
  Implicit Types X Y : gmultiset K.

  Global Instance multiset_update X X' Y :
    FindLocalUpdate X X' Y ((X' ⊎ Y) ∖ X) (X ⊆ X' ⊎ Y) | 150.
  Proof.
    move => HX.
    apply gmultiset_local_update.
    by rewrite -gmultiset_disj_union_difference.
  Qed.
  Global Instance multiset_unit : AsCmraUnit (A := gmultisetUR K) (∅).
  Proof. done. Qed.

End multiset_updates.

From iris.base_logic Require Import invariants.
From stdpp Require Import telescopes.

Section other.

  Global Instance excl_inhabited {A : ofe} : Inhabited (exclR A).
  Proof. split. by apply ExclBot. Qed.

  Global Arguments option_unit_instance /.

End other.

From diaframe.lib.own Require Export proofmode_classes proofmode_instances.

(*Class IsValidOp {A : cmra} (a a1 a2 : A) Σ (P : iPropI Σ) := {
  is_valid_merge : ✓ (a1 ⋅ a2) ⊢ □ P ;
  is_valid_op : ✓ (a1 ⋅ a2) ⊢@{iPropI Σ} a ≡ a1 ⋅ a2 ;
}.

Global Hint Mode IsValidOp ! - ! ! ! - : typeclass_instances.

Definition includedI {M : ucmra} {A : cmra} (a b : A) : uPred M
  := (∃ c, b ≡ a ⋅ c)%I.

Notation "a ≼ b" := (includedI a b) : bi_scope.

Class IsIncludedMerge {A : cmra} (a1 a2 : A) {Σ} (P : iPropI Σ) := 
  is_included_merge : ✓ a2 ⊢ a1 ≼ a2 ↔ □ P.

Global Hint Mode IsIncludedMerge ! ! ! ! - : typeclass_instances.

Class IsIncludedMergeUnital {A : cmra} (a1 a2 : A) {Σ} (P : iPropI Σ) := 
  is_included_merge_unital : ✓ a2 ⊢ a1 ≼ a2 ∨ a1 ≡ a2 ↔ □ P.

Global Hint Mode IsIncludedMergeUnital ! ! ! ! - : typeclass_instances.

Class NonUnital {A : cmra} (a : A) Σ :=
  non_unital_element b : a ≡ a ⋅ b ⊢@{iPropI Σ} False.

Class HasRightId {A : cmra} (a : A) :=
  has_right_id : ∃ c, a ≡ a ⋅ c. *)

Class CmraSubtract {A : cmra} (a b : A) (φ : Prop) (c : option A) :=
  cmra_subtract : φ → default b (c' ← c; Some $ b ⋅ c') ≡ a.

Global Hint Mode CmraSubtract ! ! ! - - : typeclass_instances.

Class UcmraSubtract {A : ucmra} (a b : A) (φ : Prop) (c : A) :=
  #[global] ucmra_subtract :: CmraSubtract a b φ (Some c).

Global Hint Mode UcmraSubtract ! ! ! - - : typeclass_instances.

Class CoAllocate {A : cmra} (a : A) (b : option A) := {
  coalloc_valid : ✓ a ↔ ✓ (a ⋅? b) ;
  coalloc_max : match b with 
                | Some b => ∀ c, b ≼ c → ✓ (a ⋅ c) → ∀ n, c ≼{n} b
                | None => ∀ c, ¬ ✓(a ⋅ c)
                end;
  (* this last one guarantees that the coallocated element b is `maximal` in some weak way.
     This is not strictly required, but is usually what we want and the recursive instances preserve this property.
     Note that c ≼ b ←/→ (∀ n, c ≼{n} b) does not necessarily hold  *)
}.

From iris.proofmode Require Import base classes reduction tactics.
From iris.algebra Require Import agree frac.

Section validity.
  Implicit Types Σ : gFunctors. (*
  Lemma from_isop {A : cmra} (a a1 a2 : A) {Σ} :
    IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I.
  Proof. 
    rewrite /IsOp; split.
    - eauto.
    - rewrite H. eauto.
  Qed.

  Instance includedI_into_pure `{CmraDiscrete A} (a b : A) {Σ} : IntoPure (PROP := iPropI Σ) (a ≼ b)%I (a ≼ b).
  Proof.
    rewrite /IntoPure. iDestruct 1 as (c) "%"; iPureIntro.
    by eexists.
  Qed.

  Section proper.
    Context {A : cmra}.
    Implicit Types a : A.

    Global Instance merge_proper a1 a2 Σ : Proper ((≡@{iPropI Σ}) ==> (iff)) (IsIncludedMerge a1 a2).
    Proof. solve_proper. Qed.

    Global Instance merge_unital_proper a1 a2 Σ : Proper ((≡@{iPropI Σ}) ==> (iff)) (IsIncludedMergeUnital a1 a2).
    Proof. solve_proper. Qed.
  End proper. *)

  Section base_instances_cmra.
    Context {A : cmra}.

    Global Instance cmra_subtract_None (a : A) : CmraSubtract a a True None | 50.
    Proof. by rewrite /CmraSubtract /=. Qed.

    Global Instance cmra_subtract_explicit_l (a b : A) : CmraSubtract (a ⋅ b) a True (Some b) | 500.
    Proof. by rewrite /CmraSubtract /=. Qed.

    Global Instance cmra_subtract_explicit_r (a b : A) : CmraSubtract (a ⋅ b) b True (Some a) | 500.
    Proof. by rewrite /CmraSubtract /= comm. Qed.

    Global Instance exclusive_none_coallocate (a : A) :
      Exclusive a →
      CoAllocate a None | 60.
    Proof.
      split =>//.
      move => c /cmra_valid_validN Hn.
      by eapply H.
    Qed. (*

    Lemma is_included_merge' (a1 a2 : A) {Σ} (P : iPropI Σ) :
      IsIncludedMerge a1 a2 P →
      a1 ≼ a2 ⊢ ✓ a2 -∗ □ P.
    Proof.
      rewrite /IsIncludedMerge => ->.
      iIntros "Ha1 HP".
      iDestruct "HP" as "[HP _ ]".
      by iApply "HP".
    Qed.

    Lemma unital_from_base (a1 a2 : A) {Σ} (P1 P2 : iProp Σ) :
      IsIncludedMerge a1 a2 P1 →
      (P1 ⊢ P2) →
      (a1 ≡ a2 ⊢ ✓ a2 -∗ □ P2) →
      (✓ a2 ⊢ □ P2 → a1 ≼ a2 ∨ a1 ≡ a2) →
      IsIncludedMergeUnital a1 a2 P2.
    Proof.
      rewrite /IsIncludedMerge /IsIncludedMergeUnital; intros.
      iIntros "#H✓"; iSplit.
      - iIntros "[#Ha|#Ha]".
        * rewrite -H0. by iApply H.
        * by iApply H1.
      - by iApply H2.
    Qed.

    Global Instance merge_unital_last_resort (a1 a2 : A) {Σ} (P : iProp Σ):
      IsIncludedMerge a1 a2 P →
      IsIncludedMergeUnital a1 a2 (P ∨ a1 ≡ a2)%I | 999.
    Proof.
      intros.
      eapply unital_from_base => //.
      - eauto.
      - eauto.
      - rewrite /IsIncludedMerge in H.
        iIntros "#H✓ [#HP|#Ha]"; [|eauto].
        rewrite H.
        iLeft.
        by iApply "H✓".
    Qed. *)

  End base_instances_cmra.

  Section base_instances_ucmra.
    Context {A : ucmra}. (*
    Global Instance valid_op_unit_left (a : A) Σ :
      IsValidOp a ε a Σ True%I | 5.
    Proof. apply from_isop. rewrite /IsOp left_id //. Qed.

    Global Instance valid_op_unit_right (a : A) Σ:
      IsValidOp a a ε Σ True%I | 5.
    Proof. apply from_isop. rewrite /IsOp right_id //. Qed. *)

    Global Instance ucmra_subtract_all (a : A) : UcmraSubtract a a True ε | 100. 
      (* this one may glance over pcore like info we can actually keep *)
    Proof. by rewrite /UcmraSubtract /CmraSubtract /= right_id => _. Qed.

    Global Instance ucmra_subtract_unit (a : A) : UcmraSubtract a ε True a | 101.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= left_id //. Qed. (*

    Global Instance included_merge_unital_from_reg (a1 a2 : A) {Σ} (P : iProp Σ) :
      IsIncludedMerge a1 a2 P →
      IsIncludedMergeUnital a1 a2 P.
    Proof.
      rewrite /IsIncludedMerge /IsIncludedMergeUnital => HP.
      iIntros "#H✓"; iSplit.
      - iIntros "[#Ha|#Ha]".
        * by iApply HP.
        * iApply HP => //. iExists ε. rewrite right_id. by iRewrite "Ha".
      - rewrite HP.
        iIntros "#HP".
        iLeft. by iApply "H✓".
    Qed.
    Global Instance ucmra_has_right_id (a : A) : HasRightId a.
    Proof. exists ε. by rewrite right_id. Qed.  *)

    Global Instance find_local_update_from_subtract (x x' : A) r φ : 
      UcmraSubtract x' x φ r →
      (* This one has priority > than all custom instances, EXCEPT for find_local_update_from_Some.
         If that one triggers first, we would be required to have Some x, while ε (= None) would do
         It is to find things like 
          (Some p, None) ~l~> (Some $ Pos.pred $ Pos.succ p, None) 
         where there is no syntactic match (so ucmra_subtract_unit cannot be applied), 
          but instances of CmraSubtract for positive will use lia to prove p = Pos.pred $ Pos.succ p
      *)
      FindLocalUpdate x x' ε r (φ ∧ ✓ x') | 200.
    Proof.
      move => H.
      case => /H <- + /=. cbn.
      assert (r ≡ r ⋅ ε) as Hrε by by rewrite right_id.
      rewrite comm.
      rewrite {3}Hrε => Hv.
      apply op_local_update => n _.
      apply cmra_valid_validN.
      by apply Some_valid.
    Qed.
    Global Instance find_local_update_from_subtract_reverse (x x' : A) r φ :
      UcmraSubtract x x' φ r →
      Cancelable r →
      (* This one has priority > than all custom instances, EXCEPT for find_local_update_from_Some.
         It is to find things like (Some p, Some 1) ~l~> (Some (p - 1), None) *)
      FindLocalUpdate x x' r ε (φ) | 205.
    Proof. 
      (* it is okay to state this in this way, even though in general we might have more than r.
        This is because if we have instead (r ⋅ z), we will be able to give r, and keep z. Thus we end up in the same spot *)
      move => H Hr /H <- {H} /=.
      rewrite comm.
      by apply cancel_local_update_unit.
    Qed.
  End base_instances_ucmra.

  Section numbers. (*
    Global Instance nat_valid_op (a a1 a2 : nat) Σ: 
      IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
    Proof. apply from_isop. Qed. *)
    Global Instance ucmra_subtract_nat (a1 a2 : nat) : 
      TCIf (SolveSepSideCondition (a1 < a2)%nat) False TCTrue → (* guards against obviously impossible things *)
      UcmraSubtract a1 a2 (a2 ≤ a1) (a1 - a2)%nat.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= nat_op => _ Ha. fold_leibniz. lia. Qed. (*
    Global Instance nat_included_merge (a1 a2 : nat) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜a1 ≤ a2⌝%I.
    Proof.
      rewrite /IsIncludedMerge.
      iIntros "_"; iSplit. 
      - by iDestruct 1 as %?%nat_included.
      - iIntros "%". iExists (a2 - a1). iPureIntro. fold_leibniz. rewrite nat_op. lia.
    Qed.

    Global Instance nat_max_valid_op (a a1 a2 : max_nat) Σ :
      IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
    Proof. apply from_isop. Qed. *)
    Global Instance ucmra_subtract_max_nat (a1 a2 : nat) : 
      UcmraSubtract (MaxNat a1) (MaxNat a2) (a2 ≤ a1) (MaxNat a1).
    Proof.
      rewrite /UcmraSubtract /CmraSubtract /= max_nat_op => Ha. 
      fold_leibniz. f_equal. lia. 
    Qed. (*
    Global Instance nat_max_included_merge (a1 a2 : nat) {Σ} : IsIncludedMerge (MaxNat a1) (MaxNat a2) (Σ := Σ) ⌜a1 ≤ a2⌝%I.
    Proof.
      rewrite /IsIncludedMerge. iIntros "_"; iSplit.
      - by iDestruct 1 as %?%max_nat_included.
      - iIntros "%". iExists (MaxNat a2). rewrite max_nat_op. iPureIntro. fold_leibniz. f_equal. lia.
    Qed.

    Global Instance nat_min_valid_op (a a1 a2 : min_nat) Σ :
      IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I.
    Proof. apply from_isop. Qed. *)
    Global Instance cmra_subtract_min_nat (a1 a2 : nat) :
      CmraSubtract (MinNat a1) (MinNat a2) (a1 ≤ a2) (Some $ MinNat a1).
    Proof.
      rewrite /UcmraSubtract /CmraSubtract /= min_nat_op_min => Ha. 
      fold_leibniz. f_equal. lia.
    Qed. (*
    Global Instance nat_min_included_merge (a1 a2 : nat) {Σ} : IsIncludedMerge (MinNat a1) (MinNat a2) (Σ := Σ) ⌜a2 ≤ a1⌝%I.
    Proof.
      rewrite /IsIncludedMerge. iIntros "_"; iSplit.
      - by iDestruct 1 as %?%min_nat_included.
      - iIntros "%". iExists (MinNat a2). rewrite min_nat_op_min. iPureIntro. fold_leibniz. f_equal. lia.
    Qed. *)
    Lemma nat_min_coalloc n : CoAllocate (MinNat n) (Some $ MinNat 0). 
    (* 0 is the maximal element for minnat. This is not an instance - we don't usually want to coallocate 0 with n *)
    Proof.
      split => //.
      move => [c] /min_nat_included /= /Nat.le_0_r <- _ m.
      apply min_nat_included => /=. lia.
    Qed.

    Global Instance Z_valid_op (a a1 a2 : Z) Σ:
      IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I.
    Proof. apply from_isop. Qed.

    Global Instance ucmra_subtract_Z (z1 z2 : Z) :
      UcmraSubtract z1 z2 True (z1 - z2)%Z.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= Z_op => _. fold_leibniz. lia. Qed.

    Global Instance Z_included_merge (z1 z2 : Z) {Σ} : IsIncludedMerge z1 z2 (Σ := Σ) True%I.
    Proof.
      rewrite /IsIncludedMerge.
      iIntros "_"; iSplit.
      - eauto.
      - iIntros "%". iPureIntro => /=. exists (z2 - z1)%Z. fold_leibniz. rewrite Z_op. lia.
    Qed. (*
    Global Instance min_nat_has_right_id n : HasRightId (MinNat n).
    Proof. exists (MinNat n).  rewrite min_nat_op_min. fold_leibniz. f_equal. lia. Qed.

    Global Instance positive_valid_op (a a1 a2 : positive) Σ :
      IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I.
    Proof. apply from_isop. Qed. *)
    Global Instance cmra_subtract_positive_lt (a1 a2 : positive) : 
      SolveSepSideCondition (a2 < a1)%positive →
      CmraSubtract a1 a2 True (Some (a1 - a2)%positive).
    Proof. 
      rewrite /CmraSubtract /SolveSepSideCondition /= pos_op_add => Ha _.
      fold_leibniz. lia.
    Qed.
    Global Instance cmra_subtract_positive_eq (a1 a2 : positive) : 
      SolveSepSideCondition (a2 = a1) →
      CmraSubtract a1 a2 True None.
    Proof. by rewrite /CmraSubtract /= => ->. Qed. (*
    Global Instance positive_included_merge (a1 a2 : positive) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜(a1 < a2)%positive⌝%I.
    Proof. 
      rewrite /IsIncludedMerge. iIntros "_"; iSplit.
      - by iDestruct 1 as %?%pos_included.
      - iIntros "%". iExists (a2 - a1)%positive. iPureIntro. fold_leibniz. rewrite pos_op_add. lia.
    Qed.
    Global Instance positive_included_merge_unital (a1 a2 : positive) {Σ} : 
      IsIncludedMergeUnital a1 a2 (Σ := Σ) ⌜(a1 ≤ a2)%positive⌝%I.
    Proof. 
      eapply unital_from_base.
      - apply _.
      - eauto with lia.
      - iIntros "-> _ !>". eauto.
      - iIntros "_ %". 
        apply Positive_as_DT.le_lteq in H as [Hl | ->]; last by iRight.
        iLeft. iExists (a2 - a1)%positive. iPureIntro. fold_leibniz. rewrite pos_op_add. lia.
    Qed.
    Global Instance positive_non_unital (p : positive) Σ : NonUnital p Σ.
    Proof. rewrite /NonUnital => q. iIntros "%". revert H. fold_leibniz. rewrite pos_op_add. lia. Qed.

    Global Instance frac_valid_op (q q1 q2 : Qp) Σ :
      IsOp q q1 q2 → IsValidOp q q1 q2 Σ ⌜q1 + q2 ≤ 1⌝%Qp%I.
    Proof.
      rewrite /IsOp; split; last first.
      { rewrite H; eauto. }
      by iDestruct 1 as %?%frac_valid.
    Qed. *)
    Global Instance frac_subtract_half (q : Qp) :
      CmraSubtract q (q/2)%Qp True (Some (q/2)%Qp) | 10.
    Proof. rewrite /CmraSubtract /= => _. by rewrite frac_op Qp.div_2. Qed.
    Global Instance frac_subtract_left (q1 q2 q3 : Qp) φ mq4 :
      CmraSubtract q2 q3 φ mq4 →
      CmraSubtract (q1 + q2)%Qp q3 φ (Some $ default q1 (q4 ← mq4; Some (q1 + q4)%Qp)) | 10.
    Proof.
      rewrite /CmraSubtract /= => Hφ /Hφ {Hφ}. 
      destruct mq4 as [q4|] => /=.
      - rewrite !frac_op => <-.
        by rewrite !assoc (comm _ q1 q3).
      - by rewrite !frac_op (comm _ q1 q2) => ->. 
    Qed.
    Global Instance frac_subtract_right (q1 q2 q3 : Qp) φ mq4 :
      CmraSubtract q1 q3 φ mq4 →
      CmraSubtract (q1 + q2)%Qp q3 φ (Some $ default q2 (q4 ← mq4; Some (q2 + q4)%Qp)) | 20.
    Proof.
      rewrite /CmraSubtract /= => Hφ /Hφ {Hφ}. 
      destruct mq4 as [q4|] => /=.
      - rewrite !frac_op => <-.
        by rewrite (comm _ q2 q4) assoc.
      - by rewrite !frac_op => ->. 
    Qed.
    Global Instance frac_subtract_None (q1 q2 q3 : Qp) φ1 φ2 q4 :
      CmraSubtract q3 q1 φ1 (Some q4) →
      CmraSubtract q4 q2 φ2 None →
      CmraSubtract (q1 + q2)%Qp q3 (φ1 ∧ φ2) None.
    Proof.
      rewrite /CmraSubtract /= => Hφ1 Hφ2.
      case => /Hφ1 <- /Hφ2 <- //.
    Qed.
    Lemma frac_subtract_any (q1 q2 q3 : Qp) φ1 φ2 q4 mq :
      CmraSubtract q3 q1 φ1 (Some q4) →
      CmraSubtract q2 q4 φ2 mq →
      CmraSubtract (q1 + q2)%Qp q3 (φ1 ∧ φ2) mq.
    Proof.
      rewrite /CmraSubtract /= => Hφ1 Hφ2.
      case => /Hφ1 /=. fold_leibniz => <-.
      move => /Hφ2 <-. simpl.
      destruct mq => //=.
      by rewrite !frac_op assoc.
    Qed. (*
    Global Instance frac_included_merge (q1 q2 : Qp) {Σ} : IsIncludedMerge q1 q2 (Σ := Σ) ⌜(q1 < q2)%Qp⌝%I.
    Proof. 
      rewrite /IsIncludedMerge. iIntros "_" ; iSplit.
      - by iDestruct 1 as %?%frac_included.
      - iIntros "%".
        apply Qp_lt_sum in H as [q' ->].
        by iExists q'.
    Qed.
    Global Instance frac_included_merge_unital (q1 q2 : Qp) {Σ} : IsIncludedMergeUnital q1 q2 (Σ := Σ) ⌜(q1 ≤ q2)%Qp⌝%I.
    Proof.
      eapply unital_from_base.
      - apply _.
      - iIntros "%"; iPureIntro. eapply Qp_le_lteq. by left.
      - iIntros "-> _ !>". eauto.
      - iIntros "_ %".
        apply Qp_le_lteq in H as [Hq| ->]; last by iRight.
        iLeft.
        apply Qp_lt_sum in Hq as [q' ->].
        by iExists q'.
    Qed.
    Global Instance frac_non_unital (p : Qp) Σ : NonUnital p Σ.
    Proof. 
      rewrite /NonUnital => q. iIntros "%". fold_leibniz. 
      rewrite frac_op in H. apply eq_sym in H. by apply Qp_add_id_free in H. 
    Qed. *)

    Global Instance frac_coallocate_half :
      CoAllocate (1/2)%Qp (Some $ 1/2)%Qp.
    Proof.
      split.
      - rewrite /= frac_op !frac_valid Qp.div_2. naive_solver.
      - move => c.
        rewrite frac_op frac_valid frac_included => Hc.
        rewrite -{2}Qp.half_half.
        rewrite -Qp.add_le_mono_l => Hc'.
        absurd (1/2 < 1/2)%Qp.
        * by apply Qp.le_ngt.
        * by eapply Qp.lt_le_trans.
    Qed.

    Lemma frac_subtract_rec_half' (q1 q2 q3 : Qp) φ :
      CmraSubtract q1 q2 φ (Some q3) →
      CmraSubtract q1 (q2/2)%Qp φ (Some (q3/2 + q1/2)%Qp).
      (* This might work, if (1/4) syntactically matches (1/2/2). but yeah, do we want it? *)
    Proof.
      rewrite /CmraSubtract /= => Hφ /Hφ {Hφ}.
      rewrite !frac_op Qp.add_assoc -Qp.div_add_distr => ->.
      by rewrite Qp.div_2.
    Qed.

    Section fracsub.  (* expands FracSub, and thus mapsto biabd, instances with extra ones *)
      Global Instance frac_sub_from_cmra_subtract q1 q2 mq φ :
        CmraSubtract q1 q2 φ mq →
        SolveSepSideCondition φ →
        FracSub q1 q2 mq | 40.
      Proof.
        rewrite /CmraSubtract /= /SolveSepSideCondition /FracSub => H /H {H} <-.
        destruct mq => //=.
        rewrite Qp.add_comm //.
      Qed.
    End fracsub.
  End numbers.

  Section sets.
    Context `{Countable K}. (*
    Global Instance set_is_op_emp_l (X : gset K) :
      IsOp X ∅ X | 10.
    Proof. rewrite /IsOp. set_solver. Qed.
    Global Instance set_is_op_emp_r (X : gset K) :
      IsOp X X ∅ | 10.
    Proof. rewrite /IsOp. set_solver. Qed.
    Global Instance set_is_op (X Y : gset K) :
      IsOp (X ∪ Y) X Y | 20.
    Proof. done. Qed.

    Global Instance set_is_valid_op (a a1 a2 : gset K) Σ :
      IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
    Proof. apply from_isop. Qed. *)
    Global Instance ucmra_set_subtract_refl (a : gset K): UcmraSubtract a a True a | 10.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= gset_op. set_solver. Qed.
    Global Instance ucmra_set_subtract (a1 a2 : gset K) : UcmraSubtract a1 a2 (a2 ⊆ a1) a1 | 20.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= gset_op. set_solver. Qed. (*
    Global Instance set_included_merge (a1 a2 : gset K) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜a1 ⊆ a2⌝%I.
    Proof. 
      rewrite /IsIncludedMerge. iIntros "_"; iSplit.
      - by iDestruct 1 as %?%gset_included. 
      - iIntros "%". iExists a2. iPureIntro. set_solver.
    Qed.

    Global Instance set_disj_is_valid_op (X Y : gset K) Σ :
      IsValidOp (GSet (X ∪ Y)) (GSet X) (GSet Y) Σ ⌜X ## Y⌝%I | 20.
    Proof.
      split.
      - by iDestruct 1 as %?%gset_disj_valid_op.
      - iDestruct 1 as %?%gset_disj_valid_op.
        by rewrite gset_disj_union.
    Qed.
    Global Instance set_disj_valid_op_emp_l (X Y : gset K) Σ :
      IsValidOp (GSet X) (GSet X) (GSet ∅) Σ True%I | 10.
    Proof. apply from_isop. rewrite /IsOp. rewrite gset_disj_union; [f_equal | ]; set_solver. Qed.
    Global Instance set_disj_valid_op_emp_r (X Y : gset K) Σ :
      IsValidOp (GSet X) (GSet ∅) (GSet X) Σ True%I | 10.
    Proof. apply from_isop. rewrite /IsOp. rewrite gset_disj_union; [f_equal | ]; set_solver. Qed. *)
    Global Instance ucmra_disjoint_set_subtract_refl (a : gset K) :
      UcmraSubtract (GSet a) (GSet a) True ε | 10.
    Proof. eapply ucmra_subtract_all. Qed.
    Global Instance ucmra_disjoint_set_subtract_empty (a : gset K) :
      UcmraSubtract (GSet a) (GSet ∅) True (GSet a) | 11.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= left_id //.  Qed.
    Global Instance ucmra_disjoint_set_subtract_empty' (a : gset K) :
      UcmraSubtract (GSet a) ε True (GSet a) | 11.
    Proof. eapply ucmra_subtract_unit. Qed.
    Global Instance ucmra_disjoint_set_subtract (a1 a2 : gset K) : 
      UcmraSubtract (GSet a1) (GSet a2) (a2 ⊆ a1) (GSet (a1 ∖ a2)) | 20.
    Proof. 
      rewrite /UcmraSubtract /CmraSubtract /= /op /cmra_op /= /ucmra_op /=.
      assert (a2 ## a1 ∖ a2) by set_solver.
      rewrite decide_True //.
      intros. f_equal.
      by rewrite -union_difference_L.
    Qed. (*
    Global Instance disj_set_included_merge (a1 a2 : gset K) {Σ} : IsIncludedMerge (GSet a1) (GSet a2) (Σ := Σ) ⌜a1 ⊆ a2⌝%I.
    Proof. 
      rewrite /IsIncludedMerge. iIntros "_"; iSplit.
      - by iDestruct 1 as %?%gset_disj_included. 
      - iIntros "%".
        iExists (GSet (a2 ∖ a1)).
        iPureIntro. rewrite gset_disj_union; [|set_solver].
        fold_leibniz. f_equal. by apply union_difference_L.
    Qed. *)

    (* no coallocate for both: gset is all _finite_ sets, these have no maximum element *)
  End sets.

  Section multisets.
    Context `{Countable K}. (*
    Global Instance multiset_is_op_emp_l (X : gmultiset K) :
      IsOp X ∅ X | 10.
    Proof. rewrite /IsOp. multiset_solver. Qed.
    Global Instance multiset_is_op_emp_r (X : gset K) :
      IsOp X X ∅ | 10.
    Proof. rewrite /IsOp. multiset_solver. Qed.
    Global Instance multiset_is_op (X Y : gmultiset K) :
      IsOp (X ⊎ Y) X Y.
    Proof. done. Qed.

    Global Instance multiset_is_valid_op (a a1 a2 : gmultiset K) Σ :
      IsOp a a1 a2 → IsValidOp a a1 a2 Σ True%I | 10.
    Proof. apply from_isop. Qed. *)
    Global Instance ucmra_multiset_subtract_refl (a : gmultiset K) :
      UcmraSubtract a a True ε | 10.
    Proof. apply ucmra_subtract_all. Qed.
    Global Instance ucmra_multiset_subtract_empty (a : gset K) :
      UcmraSubtract a ∅ True a | 11.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= left_id //.  Qed.
    Global Instance ucmra_multiset_subtract_empty' (a : gset K) :
      UcmraSubtract a ε True a | 11.
    Proof. eapply ucmra_subtract_unit. Qed.
    Global Instance ucmra_multiset_subtract (a1 a2 : gmultiset K) :
      UcmraSubtract a1 a2 (a2 ⊆ a1) (a1 ∖ a2) | 20.
    Proof. rewrite /UcmraSubtract /CmraSubtract /= gmultiset_op. multiset_solver. Qed. (*
    Global Instance multiset_included_merge (a1 a2 : gmultiset K) {Σ} : IsIncludedMerge a1 a2 (Σ := Σ) ⌜a1 ⊆ a2⌝%I.
    Proof. 
      rewrite /IsIncludedMerge. iIntros "_"; iSplit.
      - by iDestruct 1 as %?%gmultiset_included. 
      - iIntros "%". iExists (a2 ∖ a1). iPureIntro. fold_leibniz. rewrite gmultiset_op. multiset_solver.
    Qed. *)
  End multisets.

  Section recursive.
    Implicit Types A : cmra. (*

    Global Instance option_some_valid_op {A} (a a1 a2 : A) Σ P :
      IsValidOp a a1 a2 Σ P → IsValidOp (Some a) (Some a1) (Some a2) Σ P.
    Proof.
      case => HP Ha.
      split; rewrite -Some_op option_validI //.
      by rewrite Ha option_equivI.
    Qed. *)
    Global Instance option_subtract {A} (a b : A) φ c :
      CmraSubtract a b φ c →
      UcmraSubtract (Some a) (Some b) φ c.
    Proof.
      rewrite /UcmraSubtract /CmraSubtract /= => Hφ /Hφ <-.
      by destruct c.
    Qed. (*
    Global Instance option_included_merge {A} (a1 a2 : A) {Σ} (P : iProp Σ):
      IsIncludedMergeUnital a1 a2 P →
      IsIncludedMerge (Some a1) (Some a2) P | 100.
    Proof.
      rewrite /IsIncludedMerge /IsIncludedMergeUnital => HaP.
      rewrite option_validI.
      iIntros "#H✓"; iSplit.
      - iDestruct 1 as (c) "#Hc".
        destruct c.
        * rewrite -Some_op.
          iApply (HaP with "[Hc]") => //.
          iLeft.
          iExists c.
          by rewrite option_equivI.
        * rewrite Some_op_opM.
          unfold opM.
          iApply (HaP with "[Hc]") => //.
          iRight.
          by rewrite option_equivI internal_eq_sym.
      - rewrite HaP.
        iIntros "#HP".
        iSpecialize ("H✓" with "HP").
        iDestruct "H✓" as "[Hleq|He]".
        * iDestruct "Hleq" as (b) "Hb".
          iExists (Some b).
          by rewrite option_equivI -Some_op.
        * iExists None. rewrite option_equivI. rewrite Some_op_opM /=.
          by iRewrite "He".
    Qed. (* we need below, even though it is trivial, to handle recursive cases *) 
    Global Instance option_none_excl_included_merge {A} (a : optionUR A) {Σ} :
      IsIncludedMerge None a (Σ := Σ) (True)%I.
    Proof.
      rewrite /IsIncludedMerge. iIntros "_". iSplit; eauto.
      iIntros "_". iExists a. by rewrite left_id.
    Qed.
    Global Instance option_some_none_excl_included_merge {A} (a : A) {Σ} :
      IsIncludedMerge (Some a) None (Σ := Σ) (False)%I.
    Proof.
      rewrite /IsIncludedMerge. iIntros "_"; iSplit; last done.
      iDestruct 1 as (c) "Hc".
      iDestruct "Hc" as %?.
      iPureIntro.
      destruct c as [c|].
      - rewrite Some_op_opM /= in H.
        apply (Some_equiv_eq _ (a ⋅ c)) in H as [x [xH _]] => //.
      - rewrite Some_op_opM /= in H.
        apply (Some_equiv_eq _ a) in H as [x [xH _]] => //.
    Qed. *)
    Global Instance option_some_coalloc {A} (a : A) (mb : option A) :
      CoAllocate a mb →
      CoAllocate (Some a) (Some mb).
    Proof.
      case: mb => [b|];
      case => /= Hab Hmb.
      - split => /=.
        * rewrite -Some_op.
          by rewrite !Some_valid.
        * move => c /option_included.
          case => //.
          move => [b' [c' [[= Hb] [Hc' [Hab'|+]]]]]; subst.
          + intros.
            exists None => /=.
            rewrite right_id.
            by rewrite Hab'.
          + rewrite -Some_op Some_valid.
            move => /Hmb Hmb' /Hmb' Hbc'.
            intros.
            apply Some_includedN. by right.
      - split => /=.
        { by rewrite right_id. }
        move => [c|] _; last first.
        { intros. exists None. by rewrite right_id. }
        rewrite -Some_op Some_valid.
        move => /Hmb //.
    Qed.

    Global Instance option_none_coalloc_excl_unit :
      CoAllocate None (Some $ Excl' ()).
    Proof.
      split => //=.
      case; case; case => //=; repeat intros; by exists (Excl' ()).
    Qed.
(*

    Global Instance sum_inl_valid_op {A1 A2} (a a1 a2 : A1) Σ P :
      IsValidOp a a1 a2 Σ P → IsValidOp (Cinl a) (Cinl (B := A2) a1) (Cinl (B := A2) a2) Σ P.
    Proof.
      case => HP Ha. 
      split; rewrite -Cinl_op csum_validI //.
      rewrite Ha.
      iIntros "Ha".
      by iRewrite "Ha".
    Qed.
    Global Instance sum_inr_valid_op {A1 A2} (a a1 a2 : A1) Σ P :
      IsValidOp a a1 a2 Σ P → IsValidOp (Cinr a) (Cinr (A := A2) a1) (Cinr (A := A2) a2) Σ P.
    Proof.
      case => HP Ha. 
      split; rewrite -Cinr_op csum_validI //.
      rewrite Ha.
      iIntros "Ha".
      by iRewrite "Ha".
    Qed.
    Global Instance sum_inl_inr_invalid_op {A B : cmra} (a : A) (b : B) Σ :
      IsValidOp (CsumBot) (Cinl (B := B) a) (Cinr (A := A) b) Σ False.
    Proof. 
      split; rewrite /op /= /cmra_op //=; eauto.
      rewrite uPred_cmra_valid_eq /= /uPred_cmra_valid_def /=.
      rewrite /validN /= /cmra_validN //=.
    Qed.
    Global Instance sum_inr_inl_invalid_op {A B : cmra} (a : A) (b : B) Σ :
      IsValidOp (CsumBot) (Cinr (A := B) a) (Cinl (B := A) b) Σ False.
    Proof. 
      split; rewrite /op /= /cmra_op //=; eauto.
      rewrite uPred_cmra_valid_eq /= /uPred_cmra_valid_def /=.
      rewrite /validN /= /cmra_validN //=.
    Qed. *)
    Lemma sum_inl_subtract {A1 A2} (a b : A1) φ c :
      CmraSubtract a b φ c →
      CmraSubtract (Cinl (B := A2) a) (Cinl (B := A2) b) φ (fmap Cinl c).
    Proof. (* these make bad instances since fmap will not syntactically match Some or None *)
      rewrite /CmraSubtract /= => Hφ /Hφ <-.
      by destruct c.
    Qed.
    Lemma sum_inr_subtract {A1 A2} (a b : A1) φ c :
      CmraSubtract a b φ c →
      CmraSubtract (Cinr (A := A2) a) (Cinr (A := A2) b) φ (fmap Cinr c).
    Proof.
      rewrite /CmraSubtract /= => Hφ /Hφ <-.
      by destruct c.
    Qed.
    Global Instance sum_inl_subtract_s {A1 A2 : cmra} (a b : A1) φ c :
      CmraSubtract a b φ (Some c) →
      CmraSubtract (Cinl (B := A2) a) (Cinl (B := A2) b) φ (Some $ Cinl c).
    Proof.
      move => /sum_inl_subtract //=.
    Qed.
    Global Instance sum_inl_subtract_n {A1 A2 : cmra} (a b : A1) φ :
      CmraSubtract a b φ (None) →
      CmraSubtract (Cinl (B := A2) a) (Cinl (B := A2) b) φ (None).
    Proof.
      move => /sum_inl_subtract /= Hc.
      apply Hc.
    Qed.
    Global Instance sum_inr_subtract_s {A1 A2 : cmra} (a b : A1) φ c :
      CmraSubtract a b φ (Some c) →
      CmraSubtract (Cinr (A := A2) a) (Cinr (A := A2) b) φ (Some $ Cinr c).
    Proof.
      move => /sum_inr_subtract //=.
    Qed.
    Global Instance sum_inr_subtract_n {A1 A2 : cmra} (a b : A1) φ :
      CmraSubtract a b φ (None) →
      CmraSubtract (Cinr (A := A2) a) (Cinr (A := A2) b) φ (None).
    Proof.
      move => /sum_inr_subtract /= Hc.
      apply Hc.
    Qed. (*
    Global Instance sum_inl_included_merge {A1 A2} (a1 a2 : A1) {Σ} (P : iProp Σ):
      IsIncludedMerge a1 a2 P →
      IsIncludedMerge (Cinl (B := A2) a1) (Cinl (B := A2) a2) (P)%I | 100.
    Proof.
      rewrite /IsIncludedMerge => HaP.
      iIntros "#H✓"; iSplit.
      - iDestruct 1 as (c) "#Hc".
        rewrite csum_equivI csum_validI.
        destruct c=> //=.
        iApply HaP => //.
        by iExists _.
      - rewrite csum_validI HaP.
        iIntros "#HP".
        iSpecialize ("H✓" with "HP").
        iDestruct "H✓" as (c) "Hc".
        iExists (Cinl c).
        by rewrite -Cinl_op csum_equivI.
    Qed.
    Global Instance sum_inl_included_merge_unital {A1 A2} (a1 a2 : A1) {Σ} (P : iProp Σ):
      IsIncludedMergeUnital a1 a2 P →
      IsIncludedMergeUnital (Cinl (B := A2) a1) (Cinl (B := A2) a2) (P)%I | 100.
    Proof.
      rewrite /IsIncludedMergeUnital csum_validI => HaP.
      iIntros "#H✓"; iSplit; iIntros "H".
      - iDestruct "H" as "[Hc|Hc]".
        * iDestruct "Hc" as (c) "#Hc".
          rewrite csum_equivI.
          destruct c=> //=.
          iApply HaP => //. iLeft.
          by iExists _.
        * rewrite csum_equivI.
          iApply HaP => //. by iRight.
      - rewrite HaP.
        iSpecialize ("H✓" with "H").
        iDestruct "H✓" as "[Hc|Hc]".
        * iDestruct "Hc" as (c) "#Hc".
          rewrite csum_equivI.
          iLeft. iExists (Cinl c).
          by rewrite -Cinl_op csum_equivI.
        * iRight. by rewrite csum_equivI.
    Qed.
    Global Instance sum_inr_included_merge {A1 A2} (a1 a2 : A1) {Σ} (P : iProp Σ):
      IsIncludedMerge a1 a2 P →
      IsIncludedMerge (Cinr (A := A2) a1) (Cinr (A := A2) a2) (P)%I | 100.
    Proof.
      rewrite /IsIncludedMerge => HaP.
      iIntros "#H✓"; iSplit.
      - iDestruct 1 as (c) "#Hc".
        rewrite csum_equivI csum_validI.
        destruct c=> //=.
        iApply HaP => //.
        by iExists _.
      - rewrite csum_validI HaP.
        iIntros "#HP".
        iSpecialize ("H✓" with "HP").
        iDestruct "H✓" as (c) "Hc".
        iExists (Cinr c).
        by rewrite -Cinr_op csum_equivI.
    Qed.
    Global Instance sum_inr_included_merge_unital {A1 A2} (a1 a2 : A1) {Σ} (P : iProp Σ):
      IsIncludedMergeUnital a1 a2 P →
      IsIncludedMergeUnital (Cinr (A := A2) a1) (Cinr (A := A2) a2) (P)%I | 100.
    Proof.
      rewrite /IsIncludedMergeUnital csum_validI => HaP.
      iIntros "#H✓"; iSplit; iIntros "H".
      - iDestruct "H" as "[Hc|Hc]".
        * iDestruct "Hc" as (c) "#Hc".
          rewrite csum_equivI.
          destruct c=> //=.
          iApply HaP => //. iLeft.
          by iExists _.
        * rewrite csum_equivI.
          iApply HaP => //. by iRight.
      - rewrite HaP.
        iSpecialize ("H✓" with "H").
        iDestruct "H✓" as "[Hc|Hc]".
        * iDestruct "Hc" as (c) "#Hc".
          rewrite csum_equivI.
          iLeft. iExists (Cinr c).
          by rewrite -Cinr_op csum_equivI.
        * iRight. by rewrite csum_equivI.
    Qed.
    Global Instance sum_inl_inr_included_merge {A1 A2} (a1 : A1) (a2 : A2) {Σ} :
      IsIncludedMerge (Cinl a1) (Cinr a2) (False%I : iProp Σ) | 100.
    Proof.
      rewrite /IsIncludedMerge; iIntros "_"; iSplit; last done.
      iDestruct 1 as (c) "#Hc".
      rewrite csum_equivI.
      destruct c=> //=.
    Qed.
    Global Instance sum_inr_inl_included_merge {A1 A2} (a1 : A1) (a2 : A2) {Σ} :
      IsIncludedMerge (Cinr a1) (Cinl a2) (False%I : iProp Σ) | 100.
    Proof.
      rewrite /IsIncludedMerge; iIntros "_"; iSplit; last done.
      iDestruct 1 as (c) "#Hc".
      rewrite csum_equivI.
      destruct c=> //=.
    Qed.
    Global Instance csum_right_id_l {A B : cmra} (a : A) :
      HasRightId a → HasRightId (Cinl (B := B) a).
    Proof. 
      move => [r rH].
      exists (Cinl r).
      rewrite -Cinl_op -rH //.
    Qed.
    Global Instance csum_right_id_r {A B : cmra} (a : A) :
      HasRightId a → HasRightId (Cinr (A := B) a).
    Proof. 
      move => [r rH].
      exists (Cinr r).
      rewrite -Cinr_op -rH //.
    Qed.

    Global Instance prod_valid_op {A1 A2} (x x1 x2 : A1) (y y1 y2 : A2) Σ P1 P2 P :
      IsValidOp x x1 x2 Σ P1 → 
      IsValidOp y y1 y2 Σ P2 → 
      MakeAnd P1 P2 P →
      IsValidOp (x, y) (x1, y1) (x2, y2) Σ P.
    Proof.
      case => HP1 Hxs.
      case => HP2 Hys.
      rewrite /MakeAnd. split; rewrite -pair_op prod_validI /=.
      - rewrite -H bi.intuitionistically_and -HP1 -HP2 //.
      - rewrite prod_equivI /= -Hxs -Hys //.
    Qed. *)
    Global Instance prod_subtract_Some {A1 A2} (a1 b1 c1 : A1) (a2 b2 c2 : A2) φ1 φ2 :
      CmraSubtract a1 b1 φ1 (Some c1) → 
      CmraSubtract a2 b2 φ2 (Some c2) → 
      CmraSubtract (a1, a2) (b1, b2) (φ1 ∧ φ2) (Some (c1, c2)). 
    Proof. rewrite /CmraSubtract => Hφ1 Hφ2 [/Hφ1 <- /Hφ2 <-] //=. Qed.
    Global Instance prod_subtract_None {A1 A2} (a1 b1 : A1) (a2 b2 : A2) φ1 φ2 :
      CmraSubtract a1 b1 φ1 None → 
      CmraSubtract a2 b2 φ2 None → 
      CmraSubtract (a1, a2) (b1, b2) (φ1 ∧ φ2) None. 
    Proof. rewrite /CmraSubtract => Hφ1 Hφ2 [/Hφ1 <- /Hφ2 <-] //=. Qed.
    Global Instance prod_ucmra_subtract {B1 B2 : ucmra} p (a1 b1 c1 : B1) (a2 b2 c2 : B2) φ1 φ2 :
      UcmraSubtract a1 b1 φ1 c1 → 
      UcmraSubtract a2 b2 φ2 c2 → 
      TCEq p (c1, c2) → (* avoids some unification troubles *)
      UcmraSubtract (a1, a2) (b1, b2) (φ1 ∧ φ2) p.
    Proof. rewrite /UcmraSubtract /CmraSubtract => Hφ1 Hφ2 -> [/Hφ1 <- /Hφ2 <-] //=. Qed. (*
    Global Instance prod_included_merge {A1 A2} (x1 x2 : A1) (y1 y2 : A2) {Σ} P1 P2 P :
      IsIncludedMerge x1 x2 (Σ := Σ) P1 →
      IsIncludedMerge y1 y2 (Σ := Σ) P2 →
      MakeAnd P1 P2 P →
      IsIncludedMerge (x1, y1) (x2, y2) (Σ := Σ) P.
    Proof.
      rewrite /IsIncludedMerge /MakeAnd => HP1 HP2 <-.
      rewrite bi.intuitionistically_and prod_validI /=.
      iIntros "[#Hx✓ #Hy✓]"; iSplit.
      - iDestruct 1 as (z) "#Hz".
        destruct z as [z1 z2].
        rewrite -pair_op.
        rewrite prod_equivI /=.
        iDestruct "Hz" as "[Hz1 Hz2]".
        iSplit.
        * iApply (HP1 with "[Hz1]") => //. 
          by iExists _.
        * iApply (HP2 with "[Hz2]") => //. 
          by iExists _.
      - iIntros "[#HP1 #HP2]".
        rewrite HP1 HP2.
        iSpecialize ("Hx✓" with "HP1").
        iSpecialize ("Hy✓" with "HP2").
        iDestruct "Hx✓" as (x3) "Hx3".
        iDestruct "Hy✓" as (y3) "Hy3".
        iExists (x3, y3).
        rewrite -pair_op prod_equivI /=.
        iSplit => //.
    Qed. (* extra instance because TC resolution gets confused for ucmras :( *)
    Global Instance prod_included_merge_ucmra {A1 A2 : ucmra} (x1 x2 : A1) (y1 y2 : A2) {Σ} P1 P2 P :
      IsIncludedMerge x1 x2 (Σ := Σ) P1 →
      IsIncludedMerge y1 y2 (Σ := Σ) P2 →
      MakeAnd P1 P2 P →
      IsIncludedMerge (x1, y1) (x2, y2) (Σ := Σ) P.
    Proof. simple eapply prod_included_merge. Qed.
     (** TODO: this proof is horrible *)
    Lemma prod_included_merge_unital {A1 A2} (x1 x2 : A1) (y1 y2 : A2) {Σ} P1 P2 P P' P'' :
      IsIncludedMergeUnital x1 x2 (Σ := Σ) P1 → 
      IsIncludedMergeUnital y1 y2 (Σ := Σ) P2 →
      MakeAnd P1 P2 P' →
      TCIf (NonUnital x2 Σ) (MakeAnd P' (x1 ≡ x2 → y1 ≡ y2) P'') (TCAnd (HasRightId x2) (TCEq P' P'')) →
      TCIf (NonUnital y2 Σ) (MakeAnd P'' (y1 ≡ y2 → x1 ≡ x2) P) (TCAnd (HasRightId y2) (TCEq P'' P)) →
      IsIncludedMergeUnital (x1, y1) (x2, y2) (Σ := Σ) P.
    Proof.
      rewrite /IsIncludedMergeUnital /IsIncludedMerge /MakeAnd /NonUnital /HasRightId => HP1 HP2 HP1P2 HTC1 HTC2.
      rewrite prod_validI /=.
      iIntros "[#H✓x #H✓y]"; iSplit.
      + iIntros "[Hc|#Hc]".
        - iDestruct "Hc" as (z) "#Hz".
          destruct z as [z1 z2].
          rewrite -pair_op.
          rewrite prod_equivI /=.
          iDestruct "Hz" as "[#Hz1 #Hz2]".
          destruct HTC1 as [Hx2 HP|[Hx2 HP]];
          destruct HTC2 as [Hy2 HP'|[Hy2 HP']];
          rewrite -HP' -HP -HP1P2 => {HP' HP HP1P2}.
          * rewrite !bi.intuitionistically_and /=.
            iSplit; [iSplit; [iSplit|]|].
            -- iApply (HP1) => //.
               by iLeft; iExists _.
            -- iApply (HP2) => //.
               by iLeft; iExists _.
            -- iIntros "!> #Hx".
               iRewrite "Hx" in "Hz1".
               iExFalso. by iApply Hx2.
            -- iIntros "!> #Hy".
               iRewrite "Hy" in "Hz2".
               iExFalso. by iApply Hy2.
          * rewrite !bi.intuitionistically_and /=.
            iSplit; [iSplit|].
            -- iApply (HP1) => //.
               by iLeft; iExists _.
            -- iApply (HP2) => //.
               by iLeft; iExists _.
            -- iIntros "!> #Hx".
               iRewrite "Hx" in "Hz1".
               iExFalso. by iApply Hx2.
          * rewrite !bi.intuitionistically_and /=.
            iSplit; [iSplit|].
            -- iApply (HP1) => //.
               by iLeft; iExists _.
            -- iApply (HP2) => //.
               by iLeft; iExists _.
            -- iIntros "!> #Hy".
               iRewrite "Hy" in "Hz2".
               iExFalso. by iApply Hy2.
          * rewrite !bi.intuitionistically_and /=.
            iSplit.
            -- iApply (HP1) => //.
               by iLeft; iExists _.
            -- iApply (HP2) => //.
               by iLeft; iExists _.
        - rewrite prod_equivI /=.
          iDestruct "Hc" as "[#Hc1 #Hc2]".
          destruct HTC1 as [Hx2 HP|[Hx2 HP]];
          destruct HTC2 as [Hy2 HP'|[Hy2 HP']];
          rewrite -HP' -HP -HP1P2 => {HP' HP HP1P2}.
          * rewrite !bi.intuitionistically_and /=.
            iSplit; [iSplit; [iSplit|]|].
            -- iApply HP1 => //. by iRight.
            -- iApply HP2 => //. by iRight.
            -- eauto. 
            -- eauto.
          * rewrite !bi.intuitionistically_and /=.
            iSplit; [iSplit|].
            -- iApply HP1 => //. by iRight.
            -- iApply HP2 => //. by iRight.
            -- eauto.
          * rewrite !bi.intuitionistically_and /=.
            iSplit; [iSplit|].
            -- iApply HP1 => //. by iRight.
            -- iApply HP2 => //. by iRight.
            -- eauto.
          * rewrite !bi.intuitionistically_and /=.
            iSplit.
            -- iApply HP1 => //. by iRight.
            -- iApply HP2 => //. by iRight.
      + destruct HTC1 as [Hx2 HP|[Hx2 HP]];
        destruct HTC2 as [Hy2 HP'|[Hy2 HP']];
        rewrite -HP' -HP -HP1P2 => {HP' HP HP1P2}.
        - rewrite !bi.intuitionistically_and /=.
          iIntros "[[[#HP1 #HP2] #Hxy] #Hyx]".
          iAssert (✓ x2)%I as "H✓x'" => //.
          iAssert (✓ y2)%I as "H✓y'" => //.
          rewrite {1}HP1 {1}HP2.
          iSpecialize ("H✓x" with "HP1").
          iSpecialize ("H✓y" with "HP2").
          iDestruct "H✓x" as "[Hx< | Hx=]";
          iDestruct "H✓y" as "[Hy< | Hy=]".
          * iLeft. iDestruct "Hx<" as (c1) "Hc1".
            iDestruct "Hy<" as (c2) "Hc2".
            iExists (c1, c2).
            rewrite -pair_op prod_equivI /=.
            by iSplit.
          * iSpecialize ("Hyx" with "Hy=").
            iDestruct "Hx<" as (c) "Hc".
            iRewrite "Hyx" in "Hc".
            iExFalso. by iApply Hx2.
          * iSpecialize ("Hxy" with "Hx=").
            iDestruct "Hy<" as (c) "Hc".
            iRewrite "Hxy" in "Hc".
            iExFalso. by iApply Hy2.
          * iRight.
            rewrite prod_equivI /=.
            by iSplit.
        - rewrite !bi.intuitionistically_and /=.
          iIntros "[[#HP1 #HP2] #Hxy]".
          iAssert (✓ x2)%I as "H✓x'" => //.
          iAssert (✓ y2)%I as "H✓y'" => //.
          rewrite {1}HP1 {1}HP2.
          iSpecialize ("H✓x" with "HP1").
          iSpecialize ("H✓y" with "HP2").
          iDestruct "H✓x" as "[Hx< | Hx=]";
          iDestruct "H✓y" as "[Hy< | Hy=]".
          * iLeft. iDestruct "Hx<" as (c1) "Hc1".
            iDestruct "Hy<" as (c2) "Hc2".
            iExists (c1, c2).
            rewrite -pair_op prod_equivI /=.
            by iSplit.
          * iDestruct "Hx<" as (c) "Hc".
            iLeft.
            destruct Hy2 as [c' Hyc].
            iExists (c, c').
            rewrite -pair_op prod_equivI /=.
            iSplit =>//.
            iRewrite "Hy=" => //.
          * iSpecialize ("Hxy" with "Hx=").
            iRight. rewrite prod_equivI /=.
            iSplit => //.
          * iRight.
            rewrite prod_equivI /=.
            by iSplit.
        - rewrite !bi.intuitionistically_and /=.
          iIntros "[[#HP1 #HP2] #Hxy]".
          iAssert (✓ x2)%I as "H✓x'" => //.
          iAssert (✓ y2)%I as "H✓y'" => //.
          rewrite {1}HP1 {1}HP2.
          iSpecialize ("H✓x" with "HP1").
          iSpecialize ("H✓y" with "HP2").
          iDestruct "H✓x" as "[Hx< | Hx=]";
          iDestruct "H✓y" as "[Hy< | Hy=]".
          * iLeft. iDestruct "Hx<" as (c1) "Hc1".
            iDestruct "Hy<" as (c2) "Hc2".
            iExists (c1, c2).
            rewrite -pair_op prod_equivI /=.
            by iSplit.
          * iSpecialize ("Hxy" with "Hy=").
            iRight. rewrite prod_equivI /=.
            iSplit => //.
          * iDestruct "Hy<" as (c) "Hc".
            iLeft.
            destruct Hx2 as [c' Hyc].
            iExists (c', c).
            rewrite -pair_op prod_equivI /=.
            iSplit =>//.
            iRewrite "Hx=" => //.
          * iRight.
            rewrite prod_equivI /=.
            by iSplit.
        - rewrite !bi.intuitionistically_and /=.
          iIntros "[#HP1 #HP2]".
          rewrite {1}HP1 {1}HP2.
          iSpecialize ("H✓x" with "HP1").
          iSpecialize ("H✓y" with "HP2").
          iDestruct "H✓x" as "[Hx< | Hx=]";
          iDestruct "H✓y" as "[Hy< | Hy=]".
          * iLeft. iDestruct "Hx<" as (c1) "Hc1".
            iDestruct "Hy<" as (c2) "Hc2".
            iExists (c1, c2).
            rewrite -pair_op prod_equivI /=.
            by iSplit.
          * iDestruct "Hx<" as (c) "Hc".
            iLeft.
            destruct Hy2 as [c' Hyc].
            iExists (c, c').
            rewrite -pair_op prod_equivI /=.
            iSplit =>//.
            iRewrite "Hy=" => //.
          * iDestruct "Hy<" as (c) "Hc".
            iLeft.
            destruct Hx2 as [c' Hyc].
            iExists (c', c).
            rewrite -pair_op prod_equivI /=.
            iSplit =>//.
            iRewrite "Hx=" => //.
          * iRight.
            rewrite prod_equivI /=.
            by iSplit.
    Qed.
    Global Instance prod_included_merge_unital_both_right_id {A1 A2} (x1 x2 : A1) (y1 y2 : A2) {Σ} P1 P2 P :
      IsIncludedMergeUnital x1 x2 (Σ := Σ) P1 → 
      IsIncludedMergeUnital y1 y2 (Σ := Σ) P2 →
      HasRightId x2 →
      HasRightId y2 →
      MakeAnd P1 P2 P →
      IsIncludedMergeUnital (x1, y1) (x2, y2) (Σ := Σ) P.
    Proof.
      intros.
      eapply prod_included_merge_unital => //.
      - right. split => //.
      - right. split => //.
    Qed. *)
    Global Instance prod_coalloc {A1 A2} x1 mx2 y1 my2 :
      CoAllocate (A := A1) x1 mx2 →
      CoAllocate (A := A2) y1 my2 →
      CoAllocate (x1, y1) (x2 ← mx2; y2 ← my2; Some (x2, y2)).
    Proof.
      destruct mx2 as [x2|]; first destruct my2 as [y2|];
      case => /= Hx Hxmax;
      case => /= Hy Hymax.
      - split.
        * rewrite !pair_valid /=; naive_solver.
        * case => c1 c2.
          rewrite prod_included /=.
          case => /Hxmax Hx1 /Hymax Hy1 {Hxmax Hymax}.
          rewrite -pair_op pair_valid.
          case => /Hx1 Hx1' /Hy1 Hy1' n {Hx1 Hy1}.
          by rewrite pair_includedN.
      - split => //.
        case => a1 a2.
        rewrite -pair_op => /pair_valid.
        case => _ /Hymax //.
      - split => //.
        case => a1 a2.
        rewrite -pair_op => /pair_valid.
        case => /Hxmax //.
    Qed. (*
    Global Instance prod_left_non_unital {A1 A2} (x1 : A1) (x2 : A2) Σ :
      NonUnital x1 Σ → NonUnital (x1, x2) Σ.
    Proof.
      rewrite /NonUnital => H [b1 b2]. rewrite prod_equivI /=.
      iIntros "[Hx1 _]". by erewrite <-H.
    Qed.
    Global Instance prod_right_non_unital {A1 A2} (x1 : A1) (x2 : A2) Σ :
      NonUnital x2 Σ → NonUnital (x1, x2) Σ.
    Proof.
      rewrite /NonUnital => H [b1 b2]. rewrite prod_equivI /=.
      iIntros "[_ Hx2]". by erewrite <-H.
    Qed.
    Global Instance prod_right_id_both {A1 A2} (x1 : A1) (x2 : A2) :
      HasRightId x1 → HasRightId x2 → HasRightId (x1, x2).
    Proof.
      rewrite /HasRightId.
      case => c Hx1.
      case => c' Hx2.
      exists (c, c').
      by rewrite -pair_op -Hx1 -Hx2.
    Qed.

    Global Instance excl_valid_op {O : ofe} (a1 a2 : excl O) Σ:
      IsValidOp ExclBot a1 a2 Σ False%I.
    Proof. split; rewrite excl_validI /=; eauto. Qed. *)
    Lemma excl_subtract {O : ofe} (o1 o2 : O) : (* this one is bad for recursive instances *)
      TCIf (SolveSepSideCondition (¬ (o1 ≡ o2))) False TCTrue →
      CmraSubtract (Excl o1) (Excl o2) (o1 ≡ o2) None.
    Proof. rewrite /CmraSubtract => _ -> //=. Qed.

    Global Instance excl_subtract_2 {O : ofe} (o1 o2 : O) :
      SolveSepSideCondition (o1 ≡ o2) →
      CmraSubtract (Excl o1) (Excl o2) True None | 80. (* 80 so that excl_subtract can be added with higher precedence *)
    Proof.
      rewrite /CmraSubtract /SolveSepSideCondition => -> _ //=.
    Qed.

 (*
    Global Instance excl_non_unital {O : ofe} (a : O) Σ :
      NonUnital (Excl a) Σ.
    Proof. rewrite /NonUnital => [[b| ]] /=; rewrite excl_equivI /op /cmra_op /= /excl_op_instance //. Qed.
    Global Instance excl_included_merge {O : ofe} (o1 o2 : excl O) {Σ} :
      IsIncludedMerge o1 o2 (Σ := Σ) False%I.
    Proof.
      rewrite /IsIncludedMerge. rewrite excl_validI. destruct o2 as [o2|]; last eauto.
      iIntros "_". iSplit; last eauto. iDestruct 1 as (c) "Hc". 
      unfold op, cmra_op => /=; unfold excl_op_instance.
      by rewrite excl_equivI.
    Qed.
    Global Instance excl_included_merge_unital {O : ofe} (o1 o2 : O) {Σ} :
      IsIncludedMergeUnital (Excl o1) (Excl o2) (Σ := Σ) (o1 ≡ o2)%I.
    Proof. 
      eapply unital_from_base.
      - apply _.
      - eauto.
      - rewrite excl_equivI. eauto.
      - iIntros "#H✓ #Heq". iRight. by iRewrite "Heq".
    Qed.

    Global Instance agree_valid_op {O : ofe} (a1 a2 : O) Σ :
      IsValidOp (to_agree a1) (to_agree a1) (to_agree a2) Σ (a1 ≡ a2)%I.
    Proof.
      split; rewrite agree_validI agree_equivI; first eauto.
      iIntros "H".
      iRewrite "H".
      by rewrite agree_idemp.
    Qed. *)
    Global Instance agree_subtract {O : ofe} (a : O) :
      CmraSubtract (to_agree a) (to_agree a) True (Some $ to_agree a).
    Proof. rewrite /CmraSubtract /= agree_idemp //. Qed.
    Global Instance agree_coalloc {O : ofe} (a : O) :
      CoAllocate (to_agree a) (Some $ to_agree a).
    Proof.
      split; first by rewrite /= agree_idemp.
      move => c _ /agree_op_inv Hac n.
      exists c.
      by rewrite Hac agree_idemp.
    Qed. (*
    Global Instance agree_has_right_id {O : ofe} (a : O) : HasRightId (to_agree a).
    Proof. exists (to_agree a). by rewrite agree_idemp. Qed.
    Global Instance agree_included_merge {O : ofe} Σ (o1 o2 : O) :
      IsIncludedMerge (Σ := Σ) (to_agree o1) (to_agree o2) (o1 ≡ o2).
    Proof.
      rewrite /IsIncludedMerge.
      iIntros "H✓".
      iSplit. 
      - iIntros "H". iDestruct "H" as (c) "#H2".
        iRevert "H✓".
        iRewrite "H2". iIntros "#H✓".
        iAssert (to_agree o1 ≡ c)%I as "Hx".
        { by iApply agree_validI. }
        iRevert "H2 H✓". iRewrite -"Hx". iClear "Hx" => {c}.
        iIntros "#H1 #H2".
        rewrite agree_idemp.
        rewrite agree_equivI. by iRewrite "H1".
      - iIntros "#H". 
        iExists (to_agree o1). iRewrite -"H".
        by rewrite agree_idemp.
    Qed.

    Global Instance agree_included_merge_unital {O : ofe} Σ (o1 o2 : O) :
      IsIncludedMergeUnital (Σ := Σ) (to_agree o1) (to_agree o2) (o1 ≡ o2).
    Proof.
      rewrite /IsIncludedMergeUnital.
      assert (H := agree_included_merge Σ o1 o2).
      rewrite /IsIncludedMerge in H.
      rewrite H.
      iIntros "H1".
      iSplit.
      - iIntros "[H2|H2]". 
        * by iApply "H1".
        * rewrite agree_equivI. by iRewrite "H2".
      - iIntros "#H2". iLeft.
        by iApply "H1".
    Qed.
    Lemma agree_equiv {O : ofe} (o1 o2 : O) : to_agree o1 ≡ to_agree o2 → o1 ≡ o2.
    Proof.
      intros.
      apply to_agree_included.
      exists (to_agree o1).
      rewrite agree_idemp. by rewrite H.
    Qed. *)

    Lemma auth_inv {A : ucmra} (a : auth A) :
      ✓ a → ∃ b1, a ≡ ◯ b1 ∨ ∀ n : nat, ∃ q b2, a ≡{n}≡ ●{q} b2 ⋅ ◯ b1.
    Proof.
      destruct a as [[[aq al]|] ar].
      - rewrite view.view_valid_eq /=.
        case => Haq Han. 
        exists ar. right => n.
        specialize (Han n) as [a [Hal Hal']].
        exists aq, a.
        split => /=.
        * rewrite Some_op_opM /= Hal //.
        * by rewrite left_id.
      - move => _ /=.
        exists ar; left.
        split => //=.
    Qed.

    Lemma view_frag_monoN {A : ofe} `{rel : view_rel A B} (b1 b2 : B) n : 
      b1 ≼{n} b2 ↔ (view_frag (rel := rel) b1) ≼{n} (view_frag (rel := rel) b2).
    Proof. 
      split. 
      - intros [c ->]. rewrite view_frag_op. by eexists.
      - move => [c +]. destruct c as [[[q a]|] b].
        * case => /=. 
          rewrite comm Some_op_opM /=.
          move => H. inversion H.
        * case => /= => _ Hb.
          by eexists.
    Qed.

    Lemma auth_frag_monoN {A : ucmra} (a1 a2 : A) n :
      a1 ≼{n} a2 ↔ (◯ a1 ≼{n} ◯ a2).
    Proof. apply view_frag_monoN. Qed. (*

    Global Instance auth_frag_valid_op {A : ucmra} (a a1 a2 : A) Σ P :
      IsValidOp a a1 a2 Σ P →
      IsValidOp (◯ a) (◯ a1) (◯ a2) Σ P.
    Proof.
      case => HP Ha.
      split; rewrite -auth_frag_op auth_frag_validI //.
      rewrite Ha.
      iIntros "H".
      by iRewrite "H".
    Qed. *)
    Global Instance auth_frag_subtract {A : ucmra} (a1 a2 a3 : A) φ :
      UcmraSubtract a1 a2 φ a3 →
      UcmraSubtract (◯ a1) (◯ a2) φ (◯ a3).
    Proof.
      rewrite /UcmraSubtract /CmraSubtract => Hφ /Hφ {Hφ} /= <-.
      by rewrite auth_frag_op.
    Qed. 
    Lemma dfracown1_invalid_op q : ✓ (DfracOwn 1 ⋅ q) → False.
    Proof.
      move => /dfrac_valid_own_l.
      move => /Qp.lt_nge HQp.
      by apply HQp.
    Qed. (*
    Lemma auth_auth_dfrac_op_validI Σ {A : ucmra} dq1 dq2 (a1 a2 : A) : ✓ (●{dq1} a1 ⋅ ●{dq2} a2) ⊣⊢@{iPropI Σ} ⌜✓ (dq1 ⋅ dq2)%Qp⌝ ∧ ✓ a1 ∧ (a1 ≡ a2).
    Proof.
      eapply (anti_symm _); last first.
      - iIntros "(% & Ha1 & Ha)".
        iRewrite -"Ha"; iStopProof.
        split => n.
        uPred.unseal => x Hx.
        rewrite /uPred_holds /= => Ha.
        apply auth_auth_dfrac_op_validN => //.
      - split => n.
        uPred.unseal => x Hx.
        rewrite /uPred_holds /= => /auth_auth_dfrac_op_validN [Hdqs [Ha Ha1]].
        rewrite /uPred_holds /=.
        rewrite /uPred_holds //=.
    Qed.
    Global Instance auth_auth_dfrac_own_valid_op {A : ucmra} (a1 a2 : A) (q q1 q2 : Qp) Σ Pq :
      IsValidOp q q1 q2 Σ Pq →
      IsValidOp (●{#q} a1) (●{#q1} a1) (●{#q2} a2) Σ (Pq ∧ a1 ≡ a2)%I.
    Proof.
      intros.
      split.
      - rewrite auth_auth_dfrac_op_validI.
        iIntros "(% & _ & #Ha)".
        rewrite bi.intuitionistically_and; iSplit; last done.
        destruct H as [<- _]. iPureIntro.
        rewrite dfrac_op_own in H0; revert H0 => /dfrac_valid_own Hq.
        by apply frac_valid.
      - rewrite auth_auth_dfrac_op_validI.
        iIntros "(% & _ & #Ha)".
        iRewrite -"Ha".
        rewrite -auth_auth_frac_op.
        iAssert (q ≡ q1 + q2)%Qp%I as "Hq".
        { destruct H as [_ <-]. iPureIntro.
          rewrite dfrac_op_own in H0; revert H0 => /dfrac_valid_own Hq.
          by apply frac_valid. }
        by iRewrite "Hq".
    Qed.
    Global Instance auth_auth_discard_valid_op {A : ucmra} (a1 a2 : A) Σ :
      IsValidOp (●□ a1) (●□ a1) (●□ a2) Σ (a1 ≡ a2)%I.
    Proof.
      intros.
      split.
      - rewrite auth_auth_dfrac_op_validI.
        iIntros "(% & _ & #$)".
      - rewrite auth_auth_dfrac_op_validI.
        iIntros "(% & _ & #Ha)".
        iRewrite -"Ha".
        by rewrite -auth_auth_dfrac_op dfrac_op_discarded.
    Qed. *)
    Global Instance auth_coalloc {A : ucmra} (a : A) :
      CoAllocate (● a) (Some $ ◯ a).
    Proof.
      split.
      { rewrite auth_auth_valid auth_both_valid.
        naive_solver. }
      move => c _ Hac.
      assert (✓ c) by by eapply cmra_valid_op_r.
      revert H Hac => /auth_inv [b [+ | +]] + n.
      - move => -> /auth_both_valid [Hb _].
        by rewrite -auth_frag_monoN.
      - intros Hn.
        move => /cmra_valid_validN Hn'.
        specialize (Hn' 0).
        contradict Hn'.
        specialize (Hn 0) as [q [c' ->]].
        rewrite assoc => Hac.
        apply cmra_validN_op_l in Hac.
        apply auth_auth_dfrac_op_validN in Hac as [Hac _].
        by apply dfracown1_invalid_op in Hac.
    Qed.
    Global Instance frag_coalloc {A : ucmra} (a a' a'' : A) ma :
      CoAllocate a ma → (* not really a restriction, since A is a ucmra*)
      TCEq ma (Some a') →
      IsOp a'' a a' →
      CoAllocate (◯ a) (Some $ ● a'' ⋅ ◯ a').
    Proof.
      move => + Hma. rewrite Hma => {ma Hma}.
      rewrite /IsOp => HCoAlloc Ha''.
      split.
      { destruct HCoAlloc as [Hab Habc].
        rewrite /= Ha'' auth_frag_valid (comm (op)) -(assoc op) -auth_frag_op auth_both_valid.
        rewrite Hab. 
        split; last naive_solver.
        intros. split => //.
        intros. exists ε. rewrite right_id (comm op) //. }
      move => c H Hac.
      assert (✓ c) by by eapply cmra_valid_op_r.
      revert H0 Hac => /auth_inv [b [+ | +]] + n.
      - move => Hc. revert H. rewrite Hc.
        case => x Hab. contradict Hab. 
        rewrite Ha''. destruct x as [[[? ?]|] ?] => /=;
        case => /= H; inversion H.
      - destruct H as [b' Hb] => Hn.
        specialize (Hn n) as [q [c' Hqc]]. revert Hqc.
        rewrite Hb => {Hb} _ Haab.
        rewrite Ha'' {Ha''}.
        destruct b' as [[[q' c'']|] b''].
        * revert Haab => /cmra_valid_op_r.
          rewrite (comm op) (assoc op) => /cmra_valid_op_l.
          rewrite /op /cmra_op /= /view_op_instance /= Some_op_opM /= -pair_op.
          rewrite /valid /cmra_valid /= /view_valid_instance /=.
          case => -.
          rewrite comm => /dfracown1_invalid_op //.
        * rewrite -(assoc op) {3}/op /cmra_op /= /view_op_instance.
          rewrite {4}/op /cmra_op /= left_id.
          apply view_both_dfrac_includedN.
          split; [ by right | ].
          split => //.
          eapply HCoAlloc; first by exists b''.
          revert Haab.
          rewrite (comm op) -(assoc op).
          change (View None b'') with (◯ b'').
          rewrite -auth_frag_op -(assoc op) -auth_frag_op => /cmra_valid_op_r /auth_frag_valid.
          rewrite (comm op b'') !(assoc op) (comm op a') //.
    Qed.
    Global Instance auth_partial_coalloc {A : ucmra} (a : A) :
      CoAllocate (●{#1/2} a) (Some $ ●{#1/2} a ⋅ ◯ a).
    Proof.
      split.
      { rewrite /= auth_auth_dfrac_valid assoc -auth_auth_dfrac_op dfrac_op_own.
        rewrite auth_both_dfrac_valid !dfrac_valid_own.
        naive_solver. }
      move => c Hac1 Hac.
      assert (✓ c) by by eapply cmra_valid_op_r.
      revert H Hac => /auth_inv [b [+ | +]] + n.
      - destruct Hac1 as [c' ->] => H.
        inversion H.
        simpl in H0.
        revert H0.
        rewrite Some_op_opM /=.
        destruct (view_auth_proj c') eqn:Hc'; rewrite Hc' => /= H';
        inversion H'.
      - intros Hn.
        move => /cmra_valid_validN Hn'.
        specialize (Hn' n); revert Hn'.
        destruct (Hn n) as [q [c' Hceq]] => {Hn}.
        rewrite Hceq assoc => Hac.
        assert (a ≡{n}≡ c') as Hac'.
        { eapply auth_auth_dfrac_op_invN.
          by eapply cmra_validN_op_l. }
        revert Hac.
        rewrite Hac' -auth_auth_dfrac_op => {Hac'}.
        move => /auth_both_dfrac_validN.
        destruct q.
        * rewrite dfrac_op_own dfrac_valid_own.
          rewrite -{2}Qp.half_half.
          rewrite -Qp.add_le_mono_l.
          case => Hq [Hbc Hc'].
          apply auth_both_dfrac_includedN.
          split => //.
          apply Qp.le_lteq in Hq as [Hq | ->]; last by right.
          left. by apply dfrac_own_included.
        * assert (●{#1 / 2} a ⋅ ◯ a ≼{n} c).
          { destruct Hac1 as [c'' Hc''].
            exists c''. by rewrite Hc''. }
          move: H => {Hac1}.
          rewrite Hceq.
          move => /auth_both_dfrac_includedN.
          case; case => // Hnope.
          contradict Hnope; case; case => //.
        * assert (●{#1 / 2} a ⋅ ◯ a ≼{n} c).
          { destruct Hac1 as [c'' Hc''].
            exists c''. by rewrite Hc''. }
          move: H => {Hac1}.
          rewrite Hceq.
          move => /auth_both_dfrac_includedN.
          case; case => //.
          case; case => //.
          + move => Hq.
            rewrite /op /cmra_op /= in Hq.
            revert Hq => [= ->] _.
            case => Hnope.
            rewrite /op /cmra_op /= in Hnope.
            rewrite Qp.half_half in Hnope.
            rewrite /valid /cmra_valid /= in Hnope.
            contradict Hnope.
            by apply Qcanon.Qcle_not_lt.
          + move => q' -> _.
            case.
            rewrite assoc dfrac_op_own Qp.half_half.
            move => /dfracown1_invalid_op //.
    Qed.
    Global Instance auth_frac_coalloc {A : cmra} (a : A) :
      CoAllocate (●F a) (Some $ ◯F a).
    Proof. apply auth_coalloc. Qed. (*

    Global Instance auth_frac_frag_valid_op {A : cmra} (a a1 a2 : A) (q q1 q2 : Qp) Σ P1 P2 :
      IsValidOp q q1 q2 Σ P1 → 
      IsValidOp a a1 a2 Σ P2 →
      IsValidOp (◯F{q} a) (◯F{q1} a1) (◯F{q2} a2) Σ (P1 ∧ P2)%I.
    Proof.
      intros.
      apply auth_frag_valid_op, option_some_valid_op.
      by eapply prod_valid_op.
    Qed. *)
    Global Instance auth_frac_frag_subtract_part {A : cmra} (a a1 a2 : A) (q q1 q2 : Qp) φq φa :
      CmraSubtract q1 q2 φq (Some q) →
      CmraSubtract a1 a2 φa (Some a) →
      UcmraSubtract (◯F{q1} a1) (◯F{q2} a2) (φq ∧ φa) (◯F{q} a). (* this instance fails if a2 is an evar *)
    Proof.
      rewrite /UcmraSubtract /CmraSubtract => Hφq Hφa.
      case => /Hφq {Hφq} /= <- /Hφa {Hφa} /= <-.
      by rewrite frac_auth_frag_op.
    Qed.
    Global Instance auth_frac_frag_subtract_part_coreid {A : cmra} (a : A) (q q1 q2 : Qp) φq :
      CmraSubtract q1 q2 φq (Some q) →
      CoreId a →
      UcmraSubtract (◯F{q1} a) (◯F{q2} a) (φq) (◯F{q} a).
    Proof.
      rewrite /UcmraSubtract /CmraSubtract => Hφq Ha.
      move => /Hφq {Hφq} /= <-.
      by rewrite -frac_auth_frag_op -core_id_dup.
    Qed.
    Lemma auth_frac_frag_subtract_all {A : cmra} (a1 a2 : A) (q1 q2 : Qp) φq :
      CmraSubtract q1 q2 φq None →             (* baaad instance *)
      CmraSubtract (◯F{q1} a1) (◯F{q2} a2) (φq ∧ a1 ≡ a2) None.
      (* this could be a UcmraSubtract instance, but since ◯ is usually top-level and
          the ε element is unusual for frac_auth, we forego that *)
    Proof.
      rewrite /CmraSubtract => Hφq /=.
      case => /Hφq {Hφq} /= <- -> //.
    Qed.
    Global Instance auth_frac_frag_subtract_all_better_1 {A : cmra} (a1 a2 : A) (q1 q2 : Qp) φq :
      SolveSepSideCondition (a1 ≡ a2) →
      CmraSubtract q1 q2 φq None →             
      CmraSubtract (◯F{q1} a1) (◯F{q2} a2) φq None | 30.
      (* this could be a UcmraSubtract instance, but since ◯ is usually top-level and
          the ε element is unusual for frac_auth, we forego that *)
    Proof.
      rewrite /SolveSepSideCondition /CmraSubtract => -> Hφq /=.
      move => /Hφq {Hφq} /= <- //.
    Qed.
    Global Instance auth_frac_frag_subtract_all_better_2_unif {A : cmra} (a : A) (q1 q2 : Qp) φq :
      CmraSubtract q1 q2 φq None →             
      CmraSubtract (◯F{q1} a) (◯F{q2} a) φq None | 25.
      (* this could be a UcmraSubtract instance, but since ◯ is usually top-level and
          the ε element is unusual for frac_auth, we forego that *)
    Proof. apply auth_frac_frag_subtract_all_better_1. by rewrite /SolveSepSideCondition. Qed.
(*
    Global Instance auth_auth_subtract_all {A : ucmra} (a1 a2 : A) φ c :
      UcmraSubtract a2 a1 φ c →
      UcmraSubtract (● a1) (● a2) (φ) (◯ c).
      (* this is not true; they are not equivalent, since the equivalence drops down to projections on ● and ◯ *)
    Proof.
      rewrite /UcmraSubtract /CmraSubtract /= => Hφ /= /Hφ <-.
      rewrite /equiv /= /ofe_equiv /=.
  rewrite /viewUR /=.
      case => /Hφq {Hφq} /= <- -> //.
    Qed. *)

  End recursive.

  (* add stuff for saved_anything?  *)

End validity.

From diaframe Require Import proofmode_base.

Section generic_instances.
  Global Instance mergable_frag_auth {A : ucmra} `{!inG Σ $ authR A} (a b : A) γ dq :
    MergablePersist (own γ $ ◯ b) (PROP := iPropI Σ) (λ p Pin P,
      TCAnd (TCEq Pin (own γ $ ●{dq} a)) $
            (IsIncludedMerge b a P)) | 10.
  Proof.
    rewrite /MergablePersist => p P2 P3 [-> HP] /=.
    apply is_included_merge' in HP.
    rewrite bi.intuitionistically_if_elim.
    iIntros "[H2 H1]".
    iDestruct (own_valid_2 with "H1 H2") as "H".
    rewrite auth_both_dfrac_validI.
    iDestruct "H" as "(H1 &H2 &H3)".
    iApply bi.intuitionistically_into_persistently_1.
    by iApply (HP with "[H2]").
  Qed.

  Global Instance mergable_auth_frag_dq {A : ucmra} `{!inG Σ $ authR A} (a b : A) γ dq :
    MergablePersist (own γ $ ●{dq} a) (PROP := iPropI Σ) (λ p Pin P,
      TCAnd (TCEq Pin (own γ $ ◯ b)) $
            (IsIncludedMerge b a P)) | 11.
  Proof.
    rewrite /MergablePersist => p P2 P3 [-> HP] /=.
    apply is_included_merge' in HP.
    rewrite bi.intuitionistically_if_elim.
    iIntros "[H1 H2]".
    iDestruct (own_valid_2 with "H1 H2") as "H".
    rewrite auth_both_dfrac_validI.
    iDestruct "H" as "(H1 & H2 & H3)".
    iApply bi.intuitionistically_into_persistently_1.
    by iApply (HP with "[H2]").
  Qed.

  Global Instance mergable_frac_auth_frac {A : cmra} `{!inG Σ $ frac_authR A} (a b : A) q γ :
    MergablePersist (own γ $ ●F a) (PROP := iPropI Σ) (λ p Pin P,
      TCAnd (TCEq Pin (own γ $ ◯F{q} b)) $
      TCIf (TCEq q 1%Qp) (TCEq P (a ≡ b)%I)
            (IsIncludedMerge (Some b) (Some a) P)) | 50.
  Proof.
    rewrite /MergablePersist => p P2 P3 [-> HP] /=.
    rewrite bi.intuitionistically_if_elim.
    iIntros "[H1 H2]".
    iDestruct (own_valid_2 with "H1 H2") as "H".
    rewrite /frac_auth_auth.
    rewrite /frac_auth_frag.
    rewrite auth_both_validI.
    iDestruct "H" as "[#H1 #H2]".
    iDestruct "H1" as (c) "#H1".
    case: HP => [-> ->| HP].
    - destruct c as [[qc c]|] .
      * rewrite -Some_op option_equivI -pair_op prod_equivI /=.
        rewrite frac_op.
        iDestruct "H1" as "[% _]".
        fold_leibniz.
        apply eq_sym in H0.
        contradict H0.
        apply Qp.add_id_free.
      * rewrite Some_op_opM /= option_equivI prod_equivI /=.
        by iDestruct "H1" as "[_ H1]".
    - apply is_included_merge' in HP.
      destruct c as [[qc c]|].
      * rewrite -Some_op option_equivI.
        rewrite -pair_op prod_equivI /=.
        rewrite option_validI prod_validI /=.
        iDestruct "H1" as "[_ H1]".
        iDestruct "H2" as "[_ H2]".
        iApply bi.intuitionistically_into_persistently_1.
        iApply HP; last by rewrite option_validI.
        iExists (Some c).
        by iRewrite "H1".
      * rewrite Some_op_opM /=.
        rewrite option_validI option_equivI.
        rewrite prod_validI prod_equivI /=.
        iDestruct "H1" as "[_ H1]".
        iDestruct "H2" as "[_ H2]".
        iApply bi.intuitionistically_into_persistently_1.
        iApply HP; last by rewrite option_validI.
        iExists None.
        rewrite Some_op_opM /=.
        by iRewrite "H1".
  Qed.

  Global Instance mergable_frac_auth_frac_ucmra {A : ucmra} `{!inG Σ $ frac_authUR A} (a b : A) q γ :
    MergablePersist (own γ $ ●F a) (PROP := iPropI Σ) (λ p Pin P,
      TCAnd (TCEq Pin (own γ $ ◯F{q} b)) $
      TCIf (TCEq q 1%Qp) (TCEq P (a ≡ b)%I)
            (IsIncludedMerge b a P)) | 49.
  Proof.
    rewrite /MergablePersist => p P2 P3 [-> HP] /=.
    rewrite bi.intuitionistically_if_elim.
    iIntros "[H1 H2]".
    iDestruct (own_valid_2 with "H1 H2") as "H".
    rewrite /IsIncludedMerge in HP.
    rewrite /frac_auth_auth.
    rewrite /frac_auth_frag.
    rewrite auth_both_validI.
    iDestruct "H" as "[#H1 #H2]".
    iDestruct "H1" as (c) "#H1".
    case: HP => [-> ->| HP].
    - destruct c as [[qc c]|] .
      * rewrite -Some_op option_equivI -pair_op prod_equivI /=.
        rewrite frac_op.
        iDestruct "H1" as "[% _]".
        fold_leibniz.
        apply eq_sym in H0.
        contradict H0.
        apply Qp.add_id_free.
      * rewrite Some_op_opM /= option_equivI prod_equivI /=.
        by iDestruct "H1" as "[_ H1]".
    - destruct c as [[qc c]|].
      * rewrite -Some_op option_equivI.
        rewrite -pair_op prod_equivI /=.
        rewrite option_validI prod_validI /=.
        iDestruct "H1" as "[_ H1]".
        iDestruct "H2" as "[_ H2]".
        iApply bi.intuitionistically_into_persistently_1.
        iApply HP => //.
        iExists (c).
        by iRewrite "H1".
      * rewrite Some_op_opM /=.
        rewrite option_validI option_equivI.
        rewrite prod_validI prod_equivI /=.
        iDestruct "H1" as "[_ H1]".
        iDestruct "H2" as "[_ H2]".
        iApply bi.intuitionistically_into_persistently_1.
        iApply HP => //.
        iExists ε.
        by rewrite right_id.
  Qed.

  Global Instance mergable_from_valid_op `{!inG Σ A} (a b c : A) γ P :
    MergableConsume (own γ a) true (PROP := iPropI Σ) (λ p Pin Pout,
      TCAnd (TCEq Pin (own γ b)) $
      TCAnd (IsValidOp c a b Σ P) $
            (TCEq Pout (□ P ∗ own γ c)%I)) | 10.
  Proof.
    rewrite /MergableConsume => p Pin Pout.
    case => [-> [[HabP Hcab] ->]] /=.
    rewrite bi.intuitionistically_if_elim /=.
    iIntros "[Ha Hb]".
    iDestruct (own_valid_2 with "Ha Hb") as "#HP"; rewrite HabP.
    iFrame "HP".
    iDestruct (own_valid_2 with "Ha Hb") as "#Hc"; rewrite Hcab.
    iRewrite "Hc".
    by iCombine "Ha Hb" as "Hab".
  Qed.

  Global Instance mergable_drop_unit {A : ucmra} `{!inG Σ A} (a : A) γ :
    AsCmraUnit a →
    MergableConsume (own γ a) true (PROP := iPropI Σ) (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₀)%I)
            (TCEq Pout emp%I)) | 9. 
    (* this should trigger soon, so that we don't look for other stuff in our environment. if its ε, we won't learn anything *)
    (* TODO: are there units for which ✓ ε is non-trivial? if so, we need to change emp to ✓ ε *)
  Proof.
    rewrite /MergableConsume /= => -> p Pin Pout [-> ->].
    eauto.
  Qed.

  Global Instance unit_abduct2 {A : ucmra} `{inG Σ A} (x : A) γ :
    AsCmraUnit x →
    HINT ε₁ ✱ [- ; emp] ⊫ [bupd]; own γ x ✱ [emp] | 150.
  Proof.
    move => ->. iStep. rewrite right_id. iApply own_unit.
  Qed.

  Global Instance biabd_auth_update `{inG Σ (authUR A)} γ x x' y y' φ q mq φq :
    FindLocalUpdate x x' y y' φ →
    CmraSubtract 1%Qp q φq mq →
    HINT own γ (● x) ✱ [- ; own γ (◯ y) ∗ ⌜φ ∧ φq⌝] ⊫ [bupd];
              own γ (●{#q} x') ✱ [own γ (◯ y') ∗ (default emp (q' ← mq; Some $ own γ $ ●{#q'} x'))] | 40.
  Proof.
    rewrite /FindLocalUpdate /CmraSubtract => Hφ Hq.
    iStep as (Hφwit Hφqwit) "H● H◯".
    specialize (Hφ Hφwit). specialize (Hq Hφqwit).
    revert Hq; destruct mq => /= [Hq'|->].
    - iCombine "H● H◯" as "H". rewrite -!own_op.
      rewrite -own_update //.
      assert (●{#q} x' ⋅ (◯ y' ⋅ ●{#c} x') ≡ ●{#c} x' ⋅ (●{#q} x' ⋅ ◯ y')) as -> by by rewrite (comm _ (◯ _)) !assoc (comm _ (●{_} _)).
      rewrite assoc.
      rewrite -auth_auth_dfrac_op dfrac_op_own. rewrite frac_op in Hq'.
      rewrite Qp.add_comm Hq'.
      by apply auth_update.
    - iCombine "H● H◯" as "H".
      rewrite right_id -!own_op.
      rewrite -own_update //.
      by apply auth_update.
  Qed.

  Global Instance auth_update_discard `{inG Σ (authUR A)} γ x x' y y' φ :
    FindLocalUpdate x x' y y' φ →
    HINT own γ (● x) ✱ [- ; own γ (◯ y) ∗ ⌜φ⌝] ⊫ [bupd]; own γ (●□ x') ✱ [own γ (◯ y') ∗ own γ (●□ x')] | 40.
  Proof.
    rewrite /FindLocalUpdate /= /CmraSubtract => Hφ.
    iStep as (Hwit) "H● H◯".
    specialize (Hφ Hwit).
    iCombine "H● H◯" as "H".
    rewrite own_update; last by eapply auth_update.
    rewrite !own_op.
    iMod "H" as "[H● H◯]".
    rewrite own_update; last by eapply auth_update_auth_persist.
    iSteps.
  Qed.

  Global Instance frac_auth_update `{inG Σ (frac_authUR A)} γ x x' q y y' φ :
    FindLocalUpdate x x' y y' φ →
    HINT own γ (●F x) ✱ [- ; own γ (◯F{q} y) ∗ ⌜φ⌝] ⊫ [bupd]; own γ (●F x') ✱ [own γ $ ◯F{q} y'] | 42.
  Proof.
    rewrite /FindLocalUpdate => Hφ.
    iStep as (Hwit) "H● H◯".
    iCombine "H● H◯" as "H".
    rewrite -!own_op.
    rewrite -own_update //.
    apply frac_auth_update, Hφ, Hwit.
  Qed.

  Global Instance frac_auth_update_1 `{!inG Σ (frac_authUR A)} γ x x' :
    HINT own γ (●F x) ✱ [- ; own γ (◯F x) ∗ ⌜✓ x'⌝] ⊫ [bupd]; own γ (●F x') ✱ [own γ $ ◯F x'] | 999.
  Proof.
    iStep as (Hvalid) "H● H◯".
    iCombine "H● H◯" as "H".
    rewrite -own_op -own_update //.
    apply frac_auth_update_1, Hvalid.
  Qed.

  Global Instance biabd_cmra_subtract {A : cmra} `{!inG Σ A} (a b : A) γ φ mc :
    CmraSubtract a b φ mc →
    HINT own γ a ✱ [- ; ⌜φ⌝] ⊫ [id]; own γ b ✱ [match mc with | Some c => own γ c | None => emp%I end] | 41.
  Proof.
    rewrite /CmraSubtract => Hφ.
    iStep as (Hwit) "Hown".
    rewrite -Hφ //.
    destruct mc as [c|] => /=.
    - by rewrite own_op.
    - by rewrite right_id.
  Qed.

  Global Instance biabd_alloc_coalloc `{inG Σ A} a b :
    CoAllocate (A := A) a b →
    HINT ε₁ ✱ [- ; ⌜✓ a⌝] ⊫ [bupd] γ; own γ a ✱ [match b with | Some b => own γ b | _ => emp end].
  Proof.
    case => Ha _. iStep as (Hvalid).
    iMod (own_alloc (a ⋅? b)) as (γ) "Hab". { by apply Ha. }
    destruct b as [b|] => /=; eauto with iFrame.
    iDestruct "Hab" as "[Ha Hb]"; eauto with iFrame.
  Qed.

  Global Instance own_is_easy `{!inG Σ A} γ (a : A) : IsEasyGoal (own γ a) := I.
End generic_instances.


Section pure_simplification.
  Global Instance simplify_some_equiv {O : ofe} (o1 o2 : O) : 
      SimplifyPureHypSafe (Some o1 ≡ Some o2) (o1 ≡ o2).
  Proof. split; last by move => ->. intros H. by inversion_clear H. Qed.

  Global Instance simplify_to_agree {O : ofe} (o1 o2 : O) : 
      SimplifyPureHypSafe (agree.to_agree o1 ≡ agree.to_agree o2) (o1 ≡ o2).
  Proof. split; last by move => ->. move => /to_agree_equiv //. Qed.

  Global Instance simplify_cinl_eq {A B : ofe} (a1 a2 : A) :
    SimplifyPureHypSafe (Cinl (B := B) a1 ≡ Cinl (B := B) a2) (a1 ≡ a2).
  Proof. split; last by move => ->. intro Ha. by inversion Ha. Qed.

  Global Instance simplify_cinr_eq {A B : ofe} (a1 a2 : A) :
    SimplifyPureHypSafe (Cinr (A := B) a1 ≡ Cinr (A := B) a2) (a1 ≡ a2).
  Proof. split; last by move => ->. intro Ha. by inversion Ha. Qed.

  Global Instance simplify_pair_eq {A B : ofe} (pr1 pr2 : A * B) :
    SimplifyPureHypSafe (pr1 ≡ pr2) (pr1.1 ≡ pr2.1 ∧ pr1.2 ≡ pr2.2).
  Proof. split; first by move => ->. destruct pr1, pr2 => //=. Qed.
End pure_simplification.

























