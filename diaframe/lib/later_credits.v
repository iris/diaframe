From diaframe Require Import proofmode_base.
From diaframe.hint_search Require Import search_biabd search_biabd_disj biabd_disj_query biabd_disj_from_path.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Import later_credits iprop fancy_updates.


Section later_credits_merging.
  Context `{!lcGS Σ}.

  Global Instance merge_later_credits (n m : nat) : 
    MergableConsume (PROP := iPropI Σ) (£ n) true (λ p Pin Pout,
    TCAnd (TCEq Pin (£ m)) (TCEq Pout (£ (n + m)))).
  Proof.
    move => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    by rewrite lc_split.
  Qed.

  Global Instance prove_later_credits (n m m' : nat) :
    SolveSepSideCondition (m ≤ n)%nat →
    nat_cancel.NatCancel m n O m' →
    HINT £ n ✱ [- ; emp] ⊫ [id]; £ m ✱ [£ m'] | 60.
  Proof.
    rewrite /nat_cancel.NatCancel => _ Hnm; iStep as "H£".
    by rewrite -lc_split -Hnm lc_split.
  Qed.
End later_credits_merging.


Section gathered_laters.
  Context {PROP : bi}.
  Implicit Types P : PROP.

  Definition gathered_later_def n P : PROP := ▷^n P.

  Definition gathered_later_aux : seal (@gathered_later_def). Proof. by eexists. Qed.
  Definition gathered_later : nat → PROP → PROP := gathered_later_aux.(unseal).
  Lemma gathered_later_eq : @gathered_later = @gathered_later_def.
  Proof. by rewrite -gathered_later_aux.(seal_eq). Qed.

  Definition gathered_later_go_aux : seal (@gathered_later_def). Proof. by eexists. Qed.
  Definition gathered_later_go : nat → PROP → PROP := gathered_later_go_aux.(unseal).
  Lemma gathered_later_go_eq : @gathered_later_go = @gathered_later_def.
  Proof. by rewrite -gathered_later_go_aux.(seal_eq). Qed.

  Definition start_gathering_def (n : nat) : PROP := emp.
  Definition start_gathering_aux : seal (@start_gathering_def). Proof. by eexists. Qed.
  Definition start_gathering : nat → PROP := start_gathering_aux.(unseal).
  Lemma start_gathering_eq : @start_gathering = @start_gathering_def.
  Proof. by rewrite -start_gathering_aux.(seal_eq). Qed.

  Global Instance gathered_later_hint n :
    HINT ε₀ ✱ [P ; start_gathering n ∗ gathered_later_go n P] ⊫ [id] Q; gathered_later n Q ✱ [⌜P = Q⌝].
  Proof.
    iStep 2 as (P) "Hn HP". rewrite gathered_later_go_eq gathered_later_eq /gathered_later_def.
    iExists P. iSteps.
  Qed.

  Global Instance start_gathering_hint n :
    HINT ε₀ ✱ [- ; emp] ⊫ [id]; start_gathering n ✱ [gathered_later_go n True].
  Proof.
    iSteps.
    rewrite start_gathering_eq gathered_later_go_eq /gathered_later_def.
    iSteps.
  Qed.

  Global Instance gathered_later_go_merge_spatial P Q Q' n m m' : MergableConsume (gathered_later_go n P) true (λ p Pin Pout,
    TCAnd (IntoLaterNMax Pin m Q) $
    TCAnd (TCOr (TCAnd (SolveSepSideCondition (m ≤ n)) $ TCAnd (TCEq Q Q') (TCEq m' O)) (TCAnd (nat_cancel.NatCancel m n m' O) (TCEq Q' (▷^m' Q)%I))) $
           TCEq Pout (gathered_later_go n (P ∗ □?p Q'))).
  Proof.
    rewrite /SolveSepSideCondition /nat_cancel.NatCancel => p Pin Pout [-> [[[Hmn [-> Hm']] | [Hm' ->]] ->]];
    rewrite gathered_later_go_eq /gathered_later_def.
    - assert (Q' ⊢ ▷^(n - m) Q') as HQ' by iSteps. rewrite {1}HQ'.
      rewrite -bi.laterN_add.
      replace (m + (n - m)) with n by lia.
      rewrite {1}bi.laterN_intuitionistically_if_2.
      iSteps.
    - replace m with (n + m') by lia. clear Hm'.
      rewrite bi.laterN_add.
      rewrite {1}bi.laterN_intuitionistically_if_2.
      iSteps.
  Qed.
End gathered_laters.


Section later_recursive_hints.
  Context `{!invGS Σ}.

  (* lemmas for n-ary credit elimination *)
  Lemma lc_fupd_elim_laterN E P n :
     £ n -∗ (▷^n P) -∗ |={E}=> P.
  Proof.
    revert P.
    elim: n; first eauto.
    move => n IH P /=.
    rewrite lc_succ.
    iIntros "[H£ H£n] HP".
    iMod (lc_fupd_elim_later with "H£ HP") as "HP".
    iPoseProof (IH with "H£n HP") as "$".
  Qed.

  Lemma lc_fupd_add_later n E1 E2 P :
    (* below could probably be generalized to the step taking fupd.. *)
    £ n -∗ (▷^n |={E1, E2}=> P) -∗ |={E1, E2}=> P.
  Proof.
    iIntros "Hf Hupd". iApply (fupd_trans E1 E1).
    iApply (lc_fupd_elim_laterN with "Hf Hupd").
  Qed.



  Lemma biabd_disj_later_credit_elim_adv p P n P' {TTl TTr : tele} Q R S M M' E D :
    IntoLaterNMax P n P' → 
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P' Q M R S D →
    CombinedModalitySafe (fupd E E) M M' →
    ModalityStrongMono M →
    BiAbdDisj (TTl := TeleS (λ _ : iProp Σ, TTl)) (TTr := TTr) p P Q M' (λ lP, tele_map (flip bi_sep (£ n ∗ gathered_later n lP)%I) $ tele_map (bi_laterN n) R) 
      (λ lP, tele_map (tele_map (flip bi_sep lP)) S) 
      (λ lP, tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 (D' ∗ lP) end)%I).
  Proof.
    rewrite /IntoLaterNMax /BiAbd => HPP /tforall_forall HPR HM1 HM2 V.
    apply tforall_forall => ttl /=.
    rewrite !tele_map_app /= HPP bi.laterN_intuitionistically_if_2.
    iIntros "(HP & HR & H£ & HV)".
    iCombine "HP HR" as "HPR". rewrite HPR gathered_later_eq /gathered_later_def.
    rewrite HM1.
    (* I think to strip laters from additional things in our context, we need to do it as in this lemma.
       we cannot change the S with something like a forall, since we need to know what resources to strip laters from NOW *)
    iMod (lc_fupd_elim_laterN with "H£ [HV HPR]") as "HPV". by iCombine "HV HPR" as "HPV".
    iIntros "!>". rewrite modality_strong_frame_r. iStopProof. apply HM2.
    setoid_rewrite tele_map_app => /=.
    setoid_rewrite tele_app_bind => /=.
    rewrite bi.sep_or_r. apply bi.or_mono.
    - apply bi.wand_elim_l', bi_texist_elim => ttr.
      rewrite !bi_texist_exist -(bi.exist_intro ttr).
      apply bi.wand_intro_r. by rewrite assoc.
    - destruct (tele_app D ttl); eauto. by apply bi.wand_elim_l', bi.pure_elim'.
  Qed.

  Set Universe Polymorphism.

  Lemma biabddisjhyp_later_credit_elim_adv p P n P' {TTl TTr : tele} Q R S M M1 M2 M2' E D M1' Mb :
    IntoLaterNMax P n P' → (* we can't use IntoLaterN since the n is an input there, not an output. *)
    SplitModality4 M M1 (fupd E E) Mb →
    BiAbdDisjUnfoldHypMod (TTl := TTl) (TTr := TTr) p P' Q Mb M1' M2 R S D →
    CombinedModalitySafe (fupd E E) M2 M2' → (* strange modality shuffling like how the hyp_modal instance does it *)
    ModalityStrongMono M2 →
    IntroducableModality M1' →
    BiAbdDisjUnfoldHypMod (TTl := TeleS (λ _ : iProp Σ, TTl)) (TTr := TTr) p P Q M M1 M2' (λ lP, tele_map (flip bi_sep (£ n ∗ gathered_later n lP)%I) $ tele_map (bi_laterN n) R)
      (λ lP, tele_map (tele_map (flip bi_sep lP)) S)
      (λ lP, tele_bind (λ ttl, match tele_app D ttl with None2 => None2 | Some2 D' => Some2 (D' ∗ lP) end)%I).
  Proof.
    move => ? HM [[? HMb']] HM2' HM2 HM1'. eapply unfolddisjhyp_from_base => //. 
    eapply biabd_disj_later_credit_elim_adv => //.
    case: HMb' => HMb _ _ .
    case: HM => HM HM1 _ HMb'.
    split; [ | done | ]. 
    - move => ?.
      rewrite -HM. apply HM1.
      rewrite HM2'. apply fupd_mono.
      rewrite -HMb. rewrite -HM1' /=. reflexivity.
    - split.
      * move => ??. rewrite !HM2'. intros. apply fupd_mono. by apply HM2.
      * move => ??. rewrite !HM2'. rewrite modality_strong_frame_l. apply fupd_mono. apply HM2.
  Qed.

  Unset Universe Polymorphism.
End later_recursive_hints.


(* tactic which applies the recursive rule *)

Ltac biabd_disj_step_later_credit_elim cont :=
  autoapply @biabddisjhyp_later_credit_elim_adv with typeclass_instances; [tc_solve | tc_solve | cont | | | ].

(* and override search to actually use it in disjunctive bi-abduction hint search *)
Ltac biabd_disj_step_hyp_naive p P cont ::=
  (biabd_disj_step_atom_shortcut; cont) + (
  tryif biabd_disj_step_later_mono then cont else (* ensures ⊳ (P ∗ Q) is not also tried as ⊳P ∗ ⊳ Q *)
  tryif biabd_disj_step_sep P then cont else
  (biabd_disj_step_later_credit_elim cont) ||
  (biabd_disj_step_or P cont) ||
  (biabd_disj_step_hyp_exist cont) || (*TODO rewrite this so that the single tactics like <- make progress/don't skip if a hint could not be found *)
  (biabd_disj_step_hyp_mod cont) ||
  (biabd_disj_step_wand; cont) ||
  (biabd_disj_step_forall cont) ||
  (biabd_disj_step_from_goal p P; cont)
  ).

Ltac biabd_disj_step_hyp p P cont ::=
  (connective_as_atom_shortcut; cont) +
  lazymatch P with
       (* we have tryifs here to support some tricky Laterable stuff. These ensure we don't quit too early when 
          looking for hints of the shape  BiAbd _ (▷ P) Q _ _ _  -> so no matching later in goal *)
  | bi_later _ => tryif biabd_disj_step_later_mono then cont else 
        ((biabd_disj_step_later_credit_elim cont) || (biabd_disj_step_from_goal p P; cont))
  | bi_laterN _ _ => tryif biabd_disj_step_later_mono then cont else 
        ((biabd_disj_step_later_credit_elim cont) || (biabd_disj_step_from_goal p P; cont))
  | bi_sep _ _ => biabd_disj_step_sep P; cont
  | bi_or _ _ => biabd_disj_step_or P cont
  | bi_exist _ => biabd_disj_step_hyp_exist cont
  | fupd _ _ _ => biabd_disj_step_hyp_mod cont
  | bupd _ => biabd_disj_step_hyp_mod cont
  | bi_wand _ _ => biabd_disj_step_wand; cont
  | bi_forall _ => biabd_disj_step_forall cont
  | _ => biabd_disj_step_hyp_naive p P cont
  end.


(* We no longer care if modalities appear behind laters, that is fine *)
Ltac2 Set biabd_path_checker := fun _ => true.


(* Override the recursive rule when hypothesis is a later to be not just later_mono *)
Ltac2 Set biabd_disj_step_hyp_later := fun cont =>
  let mycont := plus_once (fun _ =>
    ltac1:(biabd_disj_step_later_mono);
    (* if mono is applicable, does not mean we want to use it! :( *)
    cont (); fun _ => ()
  ) (fun _ _ =>
    let f := ltac1:(c |-
      biabd_disj_step_later_credit_elim ltac:(idtac; c tt)
    ) in
    f (as_ltac1_thunk cont)
  ) in
  mycont ().


Section test.
  Context `{!invGS Σ}.

  Lemma test1 P Q : ▷ (P ∗ Q) ∗ £ 1 ⊢ |={⊤}=> P ∗ Q.
  Proof. iSteps. Qed.

  Lemma test2 : £ 2 ⊢ |={⊤}=> £ 8 ∗ True.
  Proof.
    iStep as "H£".
    (* TODO: report that nat_cancel.NatCancel is crazy slow for unsolvable goals *)
  Abort.

  Context (P Q : iProp Σ).
  Context (bupd_hint : HINT P ✱ [-; emp] ⊫ [bupd]; Q ✱ [emp]).

  Lemma test3 : ▷ P ∗ £2 ⊢ |={⊤}=> ▷ Q ∗ £ 1.
  Proof. iSteps. Qed.
End test.




