From diaframe Require Import util_classes.
From diaframe.lib Require Import own_hints intuitionistically.
From diaframe.lib.own Require Import proofmode_classes.
From iris.algebra Require Export max_prefix_list local_updates.

From iris.bi Require Import bi.
From iris.base_logic Require Import own.
From iris.proofmode Require Import proofmode.
From diaframe Require Import proofmode_base.


(* This file adds automation for the max_prefix_list camera.
    It comes in a separate file since it has some side effects, like making
    `prefix_of` Typeclasses Opaque, and adding some pure automation. *)

Global Typeclasses Opaque prefix.
Global Arguments to_max_prefix_list : simpl never.


Section instances.
  Context {A : ofe}.
  Implicit Types ls : list A.

  Global Instance max_prefix_list_sub ls1 ls2 :
    SolveSepSideCondition (ls2 `prefix_of` ls1) →
    UcmraSubtract (A := max_prefix_listUR A) (to_max_prefix_list ls1) (to_max_prefix_list ls2) True (to_max_prefix_list ls1).
  Proof.
    move => Hpref _ /=.
    rewrite to_max_prefix_list_op_l //.
  Qed.

  Global Instance max_prefix_is_included `{!OfeDiscrete A} `{!LeibnizEquiv A} ls1 ls2 Σ' :
    IsIncludedMerge (Σ := Σ') (to_max_prefix_list ls1) (to_max_prefix_list ls2) ⌜ls1 `prefix_of` ls2⌝%I.
  Proof.
    rewrite /IsIncludedMerge.
    iStep. iSplit.
    - iSteps. iPureIntro. eapply to_max_prefix_list_included_L. by eexists.
    - iStep. iPureIntro. by eapply to_max_prefix_list_included_L.
  Qed.

  Global Instance combine_prefix_lists_left ls1 ls2 Σ' `{!OfeDiscrete A} :
    SolveSepSideCondition (ls1 `prefix_of` ls2) →
    IsValidOp (to_max_prefix_list ls2) (to_max_prefix_list ls1) (to_max_prefix_list ls2) Σ' True | 20.
  Proof.
    rewrite /SolveSepSideCondition => Hl. split; iSteps.
    rewrite to_max_prefix_list_op_l //.
  Qed.

  Global Instance combine_prefix_lists_right ls1 ls2 Σ' `{!OfeDiscrete A} :
    SolveSepSideCondition (ls2 `prefix_of` ls1) →
    IsValidOp (to_max_prefix_list ls1) (to_max_prefix_list ls1) (to_max_prefix_list ls2) Σ' True | 20.
  Proof.
    rewrite /SolveSepSideCondition => Hl. split; iSteps.
    rewrite to_max_prefix_list_op_r //.
  Qed.


  Fixpoint longest {B : Type} (l r : list B) : list B :=
    match l, r with
    | [], r => r
    | lh :: l', [] => l
    | lh :: l', rh :: r' => lh :: longest l' r'
    end.

  Lemma left_prefix_of_longest {B : Type} (l r : list B) : l `prefix_of` longest l r.
  Proof.
    revert r.
    elim: l; first (move => r; exists r => //=).
    move => lh l' IH r. destruct r as [|rh r'] => /=.
    - reflexivity.
    - apply prefix_cons, IH.
  Qed.

  Lemma longest_length {B : Type} (l r : list B) : length (longest l r) = length l `max` length r.
  Proof.
    revert r. elim: l => //=.
    move => lh l' IH r. destruct r => //=.
    apply f_equal, IH.
  Qed.

  Lemma right_prefix_of_longest_iff {B : Type} (l r : list B) :
    r `prefix_of` longest l r ↔
    r `prefix_of` l ∨ l `prefix_of` r.
  Proof.
    revert r; elim: l => /=.
    - move => r; split => //= _. right. exists r => //=.
    - move => lh l' IH r. destruct r; last split.
      * split => //=; try by (try left; eexists).
      * move => Hp. assert (b = lh) by by eapply prefix_cons_inv_1. subst.
        apply prefix_cons_inv_2 in Hp. destruct (IH r) as [Hlr _]. specialize (Hlr Hp).
        destruct Hlr.
        + left. by apply prefix_cons.
        + right. by apply prefix_cons.
      * case => Hp.
        + assert (b = lh) by by eapply prefix_cons_inv_1. subst.
          apply prefix_cons. apply IH. left. by eapply prefix_cons_inv_2.
        + assert (lh = b) by by eapply prefix_cons_inv_1. subst.
          apply prefix_cons. apply IH. right. by eapply prefix_cons_inv_2.
  Qed.

  Lemma longest_app_r {B : Type} (l r : list B) : longest l (l ++ r) = l ++ r.
  Proof.
    elim: l => //=.
    move => lh l' -> //.
  Qed.

  Lemma longest_app_l {B : Type} (l r : list B) : longest (l ++ r) l = l ++ r.
  Proof.
    elim: l => //=.
    { elim: r => //=. }
    move => lh l' -> //.
  Qed.

  Lemma longest_app_both {B : Type} (h l r : list B) : longest (h ++ l) (h ++ r) = h ++ longest l r.
  Proof.
    elim: h => //=.
    move => hh h' -> //.
  Qed.

  Lemma longest_drop_skip_eq {B : Type} (l r : list B) : longest l r = l ++ (drop (length l) r).
  Proof.
    revert r; elim: l => //=.
    move => lh l' IH r. destruct r; first rewrite right_id //.
    f_equal. simpl. apply IH.
  Qed.

  Lemma right_prefix_of_longest_then_one_eq {B : Type} (l r : list B) :
    r `prefix_of` longest l r →
    longest l r = l ∨ longest l r = r. (* other way around does not hold, maybe if arguments are swapped of longest *)
  Proof.
    rewrite right_prefix_of_longest_iff. case.
    - case => [p Hp]; subst. rewrite longest_app_l. eauto.
    - case => [p Hp]; subst. rewrite longest_app_r. eauto.
  Qed.

  Global Instance combine_prefix_lists_fallback ls1 ls2 Σ' `{!OfeDiscrete A} `{!LeibnizEquiv A} :
    IsValidOp (to_max_prefix_list (longest ls1 ls2)) (to_max_prefix_list ls1) (to_max_prefix_list ls2) Σ'
      ⌜ls1 `prefix_of` longest ls1 ls2 ∧ ls2 `prefix_of` longest ls1 ls2⌝%I | 30.
  Proof.
    split; iSteps; iPureIntro.
    - apply left_prefix_of_longest.
    - apply right_prefix_of_longest_iff.
      apply to_max_prefix_list_op_valid_L in H as [H|H]; eauto.
    - apply to_max_prefix_list_op_valid_L in H as [H|H]; eauto.
      * rewrite to_max_prefix_list_op_l //.
        repeat f_equiv. fold_leibniz.
        destruct H as [p ->]. apply longest_app_r.
      * rewrite to_max_prefix_list_op_r //.
        repeat f_equiv. fold_leibniz.
        destruct H as [p ->]. apply longest_app_l.
  Qed.


  (* this is often a useful lemma *)
  Lemma list_eq_from_prefix_length_ineq {B : Type} (l r : list B) :
    l `prefix_of` r →
    length r ≤ length l →
    l = r.
  Proof.
    case => [p ->] {r}.
    destruct p; first by rewrite right_id //.
    rewrite length_app /=. lia.
  Qed.

  Global Instance simplify_prefix_cons (hl : A) ls hr rs : 
    SimplifyPureHypSafe (hl :: ls `prefix_of` hr :: rs) (hl = hr ∧ ls `prefix_of` rs).
  Proof.
    split.
    - move => Hp. split; last by eapply prefix_cons_inv_2. by apply prefix_cons_inv_1 in Hp.
    - case => -> Hp. by apply prefix_cons.
  Qed.

  Global Instance simplify_trivial_prefix `(ls : list A) : SimplifyPureHypSafe (ls `prefix_of` ls) True.
  Proof. split; eauto. Qed.

  (* TODO: the Iris lemma is awkward to use, one wants this one. Upstream? *)
  Lemma max_prefix_list_local_update' ls1 ls2 `{!OfeDiscrete A} :
    ls1 `prefix_of` ls2 →
    (to_max_prefix_list ls1, ε) ~l~> (to_max_prefix_list ls2, to_max_prefix_list ls2).
  Proof.
    intros. etrans.
    - apply core_id_local_update; [ | eauto]. tc_solve.
    - rewrite left_id. by apply max_prefix_list_local_update.
  Qed.

  Global Instance update_prefix_list ls1 ls2 `{!OfeDiscrete A} : 
    SolveSepSideCondition (ls1 `prefix_of` ls2) →
    FindLocalUpdate (to_max_prefix_list ls1) (to_max_prefix_list ls2) (ε) (to_max_prefix_list ls2) True.
  Proof.
    move => Hpref _.
    eapply max_prefix_list_local_update' => //.
  Qed.

  Global Instance simplify_max_prefix_list_eq `{!OfeDiscrete A} `{!LeibnizEquiv A} ls1 ls2 :
    SimplifyPureHypSafe (to_max_prefix_list ls1 ≡ to_max_prefix_list ls2) (ls1 = ls2).
  Proof.
    split; last by move => ->.
    unfold to_max_prefix_list.
    move => Hm. rename ls1 into l1. rename ls2 into l2.
    enough (∀ i, l1 !! i = l2 !! i).
    - clear -H. revert l2 H.
      induction l1 as [|l' l1].
      * case; first done. move => a l /(dep_eval O) /= Heq. simplify_eq.
      * case.
        { move => /(dep_eval O) /= Heq. simplify_eq. }
        move => a l IH.
        f_equal. 
        + specialize (IH O). simpl in IH. by simplify_eq.
        + apply IHl1 => i. specialize (IH (S i)).
          by simpl in IH.
    - move => i.
      let HMt := type of Hm in
      match HMt with
      | ?lhs ≡ ?rhs =>
        assert (lhs !! i ≡ rhs !! i)
      end. by rewrite Hm.
      revert H.
      rewrite !lookup_fmap /=.
      rewrite !lookup_map_seq_0 /=.
      destruct (l2 !! i) => /=.
      * move => Hl.
        apply fmap_Some_equiv_1 in Hl.
        destruct Hl as [? [? ?]]. rewrite H.
        f_equal. apply to_agree_equiv in H0. by simplify_eq.
      * destruct (l1 !! i); last done.
        move => Hl. inversion Hl.
  Qed.
End instances.



Global Hint Extern 4 (✓ (to_max_prefix_list _)) => apply to_max_prefix_list_valid : solve_pure_add.


(* To get simplification working, we need SolveSepSideCondition to be able to prove `prefix_of`.
   Below is a tactic capable of proving some of those goals *)

Ltac prove_prefix :=
  lazymatch goal with
  | |- ?lhs `prefix_of` ?rhs =>
    try assumption;
    try reflexivity;
    match lhs with
    | ?l:: ?lhs' => (* we could do the same for app, but only on syntactic equality - otherwise it is not safe *)
      lazymatch rhs with
      | ?r :: ?rhs' =>
        apply prefix_cons_alt; 
        [ pure_solver.trySolvePure |  prove_prefix ]
      end
    | _ =>
      (* see if right is left with an append operation *)
      lazymatch rhs with
      | lhs ++ _ => apply prefix_app_r; reflexivity
      | _ =>
        (* try finding it through transitivity *)
        match goal with
        | H : ?m `prefix_of` ?rhs' |- ?lhs' `prefix_of` ?rhs' => (* rematch goal, otherwise it doesnt work? *)
          etrans; last exact H; (* this is to break loops: *)clear H; (* TODO: this can break [iSteps as] renaming! *)
          prove_prefix
        | H : ?lhs' `prefix_of` ?m |- ?lhs' `prefix_of` ?rhs' => (* also try from other side *)
          etrans; first exact H; (* this is to break loops: *)clear H;
          prove_prefix
        end
      end
    end
  end.
(* TODO: can we make a generic preorder solver in Ltac2..? with 'weak reflection'..? *)

Global Hint Extern 4 (_ `prefix_of` _) => prove_prefix : solve_pure_add.




