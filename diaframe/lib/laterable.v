From iris.bi Require Export bi telescopes lib.laterable.
From iris.proofmode Require Import proofmode environments.
From diaframe Require Import proofmode_base lib.intuitionistically.


(* Adds support for goals of the form 'make_laterable G'. *)

Section laterable_goal.
  Context {PROP : bi}.
  Implicit Type P : PROP.

  Global Instance make_laterable_as_solve_goal P :
    AsSolveGoal id (make_laterable P)%I (Transform $ make_laterable P).
  Proof. unseal_diaframe; rewrite /AsSolveGoal //. Qed.


  Global Instance make_laterable_re_intro P P' R :
    TCIf (Laterable P) False (IntoLaterable P P') →
    TRANSFORM □⟨false⟩ P, make_laterable R =[hyp]=> (make_laterable $ IntroduceHyp P' R).
  Proof.
    rewrite /TransformHyp. unseal_diaframe => /=.
    move => [Hl [] | HPQ].
    destruct HPQ as [-> HPQ].
    iIntros "HQR HQ".
    iApply make_laterable_except_0.
    rewrite {1}/make_laterable.
    iDestruct "HQR" as "(%Z & HZ & #HZR)".
    iApply make_laterable_intro; last by iAccu.
    { tc_solve. }
    iIntros "!> [HZ HQ]".
    iMod ("HZR" with "HZ") as "HQR".
    iIntros "!>".
    by iApply "HQR".
  Qed.

  Global Instance make_laterable_introducable Δ P :
    Laterable (PROP := PROP) emp%I →
    TCForall Laterable (env_to_list $ env_spatial Δ) → 
    TRANSFORM Δ, make_laterable P =[ctx]=> P | 30.
  Proof.
    rewrite /TransformCtx => He HM.
    rewrite envs_entails_unseal => HΔ.
    assert (Laterable ([∗] env_spatial Δ)) by by apply big_sepL_laterable.
    rewrite of_envs_alt.
    apply bi.pure_elim_l => Hwf.
    iIntros "[#H1 H2]".
    iApply (make_laterable_intro with "[H1] H2").
    iIntros "!> HR".
    iApply HΔ. rewrite of_envs_alt.
    iSplit; first by iPureIntro.
    by iSplitR.
  Qed.
End laterable_goal.


Section laterable_hyp.
  Context {PROP : bi}.

  Definition later_part_of_def (L P : PROP) : PROP := P.

  Definition pers_part_of_def (L P : PROP) : PROP := □ (▷P -∗ ◇ L).

  Definition later_part_of_aux : seal (@later_part_of_def). Proof. by eexists. Qed.
  Definition later_part_of := later_part_of_aux.(unseal).
  Global Opaque later_part_of.
  Global Arguments later_part_of _ _ : simpl never.

  Definition pers_part_of_aux : seal (@pers_part_of_def). Proof. by eexists. Qed.
  Definition pers_part_of := pers_part_of_aux.(unseal).
  Global Opaque pers_part_of.
  Global Arguments pers_part_of _ _ : simpl never.

  Definition later_later_part_of_def L P : PROP := ▷ later_part_of L P.
  Definition later_later_part_of_aux : seal (@later_later_part_of_def). Proof. by eexists. Qed.
  Definition later_later_part_of := later_later_part_of_aux.(unseal).
  Global Opaque later_later_part_of.
  Global Arguments later_later_part_of _ _ : simpl never.

  Lemma later_later_part_of_eq : @later_later_part_of = @later_later_part_of_def.
  Proof. by rewrite -later_later_part_of_aux.(seal_eq). Qed.

  Lemma later_part_of_eq : @later_part_of = @later_part_of_def.
  Proof. by rewrite -later_part_of_aux.(seal_eq). Qed.

  Lemma pers_part_of_eq : @pers_part_of = @pers_part_of_def.
  Proof. by rewrite -pers_part_of_aux.(seal_eq). Qed.

  Global Instance laterable_into_parts' (L : PROP) :
    Laterable L →
    HINT L ✱ [- ; emp] ⊫ [id] P; later_later_part_of L P ✱ [pers_part_of L P].
  Proof.
    rewrite /Laterable => HL. iStep as "HL". rewrite {1}HL.
    rewrite later_later_part_of_eq /later_later_part_of_def later_part_of_eq pers_part_of_eq /pers_part_of_def /=.
    iDecompose "HL" as (P) "HQ HL".
    iExists P. rewrite /later_part_of_def. iFrame. (* iStep will get us a ▷ emp as left-over, which we cannot discard! *)
    iSteps.
  Qed.

  Lemma biabd_through_later_later_part (TT : tele) (P P' L : TT -t> PROP) teq :
    (TC∀.. (tt : TT), TCEq (tele_app P tt) (▷ later_part_of (tele_app L tt) (tele_app P' tt))%I) →
    SimplTeleEq TT teq →
    BiAbd (TTl := TT) (TTr := TT) false (ε₀)%I P id (tele_bind (λ tt, later_later_part_of (tele_app L tt) (tele_app P' tt))) (tele_map (tele_map (bi_pure)) (teq))%I.
  Proof.
    rewrite /BiAbd /SimplTeleEq /= => /tforall_forall HP /tforall_forall Hteq.
    apply tforall_forall => ttl.
    iStep as "HLP'". rewrite !tele_app_bind.
    iExists (ttl).
    rewrite HP !tele_map_app later_later_part_of_eq.
    iFrame; iPureIntro.
    specialize (Hteq ttl).
    apply (dep_eval_tele ttl) in Hteq.
    apply Hteq.
    apply tele_eq_app_both.
  Qed.

  Global Existing Instance biabd_through_later_later_part | 40.

  Global Instance pers_part_wand (L P : PROP) `{BiFUpd PROP} : 
      AtomIntoWand true (pers_part_of L P) (▷(later_part_of L P))%I (∀ E, |={E}=> L)%I.
  Proof.
    rewrite /AtomIntoWand /IntoWand2 /= later_part_of_eq pers_part_of_eq.
    iStep 3 as (E) "HPL1 HPL2".
    by iMod ("HPL1" with "HPL2") as "$".
  Qed.

  Global Instance pers_part_pers (L P : PROP) : Persistent (pers_part_of L P).
  Proof. rewrite pers_part_of_eq. tc_solve. Qed.

  Global Instance pers_part_into_wand L (P : PROP): IntoWand true false (pers_part_of L P) (▷(later_part_of L P))%I (◇ L)%I.
  Proof. rewrite /IntoWand /= later_part_of_eq pers_part_of_eq; iSteps. Qed.

  Global Instance later_later_from_later_later (L P : PROP) :
    HINT ▷ later_part_of L P ✱ [- ; emp] ⊫ [id]; later_later_part_of L P ✱ [emp].
  Proof. rewrite /BiAbd /= later_later_part_of_eq //. Qed.

  Global Instance later_later_from_now_later (L P : PROP) :
    HINT later_part_of L P ✱ [- ; emp] ⊫ [id]; later_later_part_of L P ✱ [emp].
  Proof. rewrite /BiAbd /= later_later_part_of_eq /later_later_part_of_def. iIntros "[HP _]". by iFrame. Qed.

End laterable_hyp.