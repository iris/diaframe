From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode environments.
From diaframe Require Import proofmode_base.


(* Makes Diaframe support the [□] modality in goals. *)

Section intuitionistically.
  Context {PROP : bi}.
  Implicit Type P : PROP.

  (** This registers [□ P] as a goal for which transformer hints should be sought. 
    The business with the modality [M] is to cover the case where the goal is something like
    [|={⊤}=> □ P] (i.e. with [M = fupd ⊤ ⊤]). *)
  Global Instance intuitionistically_as_solve_goal P M :
    IntroducableModality M →
    AsSolveGoal M (□ P)%I (Transform $ □ P)%I.
  Proof. unseal_diaframe; rewrite /AsSolveGoal //. eauto. Qed.

  (** For [□ P], there are no hypothesis transformer hints, since the introduction of the
      [□] modality keeps hypotheses untouched. There is exactly one context transformer hint: *)
  Global Instance intuitionistically_transform_ctx Δ P :
    TCEq (env_spatial Δ) Enil → 
    (** above states that if there are no spatial hypotheses.. *)
    TRANSFORM Δ , □ P =[ctx]=> P | 30.
    (** then [□ P] can be transformed to [P] in context [Δ]. *)
  Proof.
    rewrite /TransformCtx => HM.
    rewrite envs_entails_unseal.
    rewrite {2}env_spatial_is_nil_intuitionistically; last rewrite /env_spatial_is_nil HM //.
    move => -> //.
  Qed.
  (** Note that it is possible to add a hypothesis transformer hint to remove all spatial hypothesis.
    See [lib/persistently.v] for such a hint. *)


  (** This is an additional, unrelated bi-abduction hint that allows one to prove [□ P] from [P]
      if [P] is intuitionistic. *)
  Global Instance intuitionistically_intro_if (P : PROP) :
    Affine P → Persistent P →
    HINT ε₀ ✱ [- ; P] ⊫ [id] ; □ P ✱ [True].
  Proof.
    move => HP1 HP2. iSteps.
  Qed.
End intuitionistically.