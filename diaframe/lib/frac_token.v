From iris.bi Require Import bi.
From iris.bi Require Export fractional.
From iris.algebra Require Import auth numbers.
From iris.proofmode Require Import tactics.
From iris.base_logic Require Import base_logic own.

From diaframe Require Import proofmode_base.
From diaframe.lib Require Import tokenizable own_hints.


(* This file defines the token ghost state used by, among others, ARC.
   The concrete definitions are abstracted away in this library too provide
   a cleaner presentation of the required ghost-state reasoning.
   Additionally, the proofs of the various updates sometimes require foresight, 
   and Diaframe cannot handle such proofs automatically. 
   Since this Diaframe comes with biabduction hints, Diaframe can reason about token automatically. *)

Section fractional_tokenizable.
  Definition fracTokenR := authR $ optionUR $ prodR fracR positiveR.
  Class fracTokenG Σ := { #[local] fracPayloadTokenCounter_inG :: inG Σ fracTokenR }.
  Definition fracTokenΣ : gFunctors := #[GFunctor fracTokenR].
  Global Instance subG_fracTokenΣ {Σ} : subG fracTokenΣ Σ → fracTokenG Σ.
  Proof. solve_inG. Qed.

  Context `{!fracTokenG Σ}.
  Context (P : Qp → iProp Σ).

  Instance P_AsFractional q : Fractional P → AsFractional (P q) P q.
  Proof. done. Qed.

  (* TODO: Including [⌜Fractional P⌝] inside the definitions makes them
    not NonExpansive, which might be useful in some cases. Probably, we
    could make it work with a weaker version like:

  [Definition FractionalI (Q : Qp → iProp Σ) : iProp Σ :=
    □ (∀ q1 q2, Q (q1 + q2)%Qp ∗-∗ Q q1 ∗ Q q2).]

  for which we have [⌜Fractional Q⌝ ⊢ FractionalI Q]. *)

  Program Definition fractional_payload_token_counter : tokenizable.Tokenizable $ iPropI Σ :=
    tokenizable.Build_Tokenizable
      (⌜Fractional P⌝ ∗ P 1)%I
      (λ γ, ∃ q, own γ (◯ (Some (q, 1%positive))) ∗ P q)%I
      (λ γ p, ⌜Fractional P⌝ ∗ ∃ q q', own γ (● (Some (q, p))) ∗ ⌜(q + q' = 1)%Qp⌝ ∗ P q')%I
      (λ γ q, own γ (●{#q} None))
      _.
  Next Obligation.
    split.
    - iSteps.
    - iStep 3 as (γ p HP q1 q2 Hq) "H● HP".
      iStepDebug.
      solveStep.
      solveStep.
      solveStep.
      solveStep.
      solveStep.
      iExists (q1 + (q2 / 2))%Qp.
      unshelve solveSteps; shelve_unifiable.
      { iPureIntro. split; last done. simpl. apply frac_valid. rewrite -Hq. apply Qp.add_le_mono_l. by apply Qp.lt_le_incl, Qp.div_lt. }
      iExists (q2 / 2)%Qp. iSteps.
      { iPureIntro. rewrite -Qp.add_assoc.
        by erewrite Qp.div_2. }
      iDestruct "HP" as "[HP HP']".
      iSteps.
      replace (Pos.succ p - p)%positive with 1%positive by lia.
      iSteps.
    - iStep 2 as (γ HP) "H● HP".
      iStepDebug.
      solveStep.
      solveStep.
      solveStep.
      solveStep.
      solveStep.
      iExists (1/2)%Qp.
      solveSteps.
      iExists (1/2)%Qp.
      unshelve solveSteps.
      { iPureIntro; apply Qp.div_2. }
      iDestruct "HP" as "[HP HP']".
      iSteps.
    - iStep 4 as (γ p Hp HP q1 q2 Hq q3 _ _ Hq3 _) "H● HP H◯ HP'".
      apply Qp.lt_sum in Hq3 as [q ->].
      iStep.
      iExists q.
      iStep.
      iExists (q3 + q2)%Qp.
      iSteps.
      * by rewrite assoc (Qp.add_comm q).
      * by iCombine "HP' HP" as "$".
    - iSteps as (γ HP q1 q2 Hq _ _ _ _) "HPq2 HPq1".
      rewrite -Hq.
      by iCombine "HPq1 HPq2" as "$".
    - iSteps.
    - iSteps.
    - tc_solve.
    - iStep 3 as (γ q) "Hq". 
      iDestruct (own_valid with "Hq") as %[Hx0 _]%auth_auth_dfrac_valid.
      revert Hx0 => /dfrac_valid_own Hx0; iSteps.
    - intros γ p q. eapply (anti_symm _).
      * by iDestruct 1 as "[$ $]".
      * iStep as (Hq) "Hq". rewrite Qp.add_comm. iSteps.
  Qed.

  Let tc := fractional_payload_token_counter.

  Definition token := tokenizable.token tc.
  Definition token_counter := tokenizable.token_counter tc.
  Definition no_tokens := tokenizable.no_tokens tc.
End fractional_tokenizable.

Section automation.
  Context `{!fracTokenG Σ}.
  Context (P : Qp → iProp Σ).

  Global Instance biabd_alloc_none q φ mq :
    CmraSubtract 1%Qp q φ mq →
    HINT ε₁ ✱ [- ; ⌜φ⌝] ⊫ [bupd] γ; no_tokens P γ q ✱ [default emp (q' ← mq; Some $ no_tokens P γ q')].
  Proof. apply: tokenizable.biabd_alloc_none. Qed.

  Global Instance biabd_alloc_some :
    HINT ε₁ ✱ [ - ; ⌜Fractional P⌝ ∗ P 1] ⊫ [bupd] γ; token_counter P γ 1 ✱ [token P γ].
  Proof. apply: tokenizable.biabd_alloc_some. Qed.

  Global Instance biabd_create_token γ p1 p2 :
    SolveSepSideCondition (p2 = Pos.succ p1) →
    HINT token_counter P γ p1 ✱ [- ; emp] ⊫ [bupd]; token_counter P γ p2 ✱ [token P γ].
  Proof. apply: tokenizable.biabd_create_token. Qed.

  Global Instance biabd_create_first_token γ p :
    SolveSepSideCondition (p = 1%positive) →
    HINT no_tokens P γ 1 ✱ [- ; ⌜Fractional P⌝ ∗ P 1] ⊫ [bupd]; token_counter P γ p ✱ [token P γ].
  Proof. apply: tokenizable.biabd_create_first_token. Qed.

  Global Instance biabd_delete_token γ p1 p2 :
    SolveSepSideCondition (1 < p1)%positive →
    SolveSepSideCondition (p2 = Pos.pred p1) →
    HINT token_counter P γ p1 ✱ [- ; token P γ] ⊫ [bupd]; token_counter P γ p2 ✱ [emp].
  Proof. apply: tokenizable.biabd_delete_token. Qed.

  Global Instance biabd_delete_last_token γ q mq φ p :
    SolveSepSideCondition (p = 1)%positive →
    CmraSubtract 1%Qp q φ mq →
    HINT token_counter P γ p ✱ [- ; token P γ ∗ ⌜φ⌝] ⊫ [bupd]; no_tokens P γ q ✱ [(⌜Fractional P⌝ ∗ P 1) ∗ default emp (q' ← mq; Some $ no_tokens P γ q')].
  Proof. apply: tokenizable.biabd_delete_last_token. Qed.

  Global Instance biabd_delete_last_token_to_req γ p :
    SolveSepSideCondition (p = 1%positive) →
    HINT token_counter P γ p ✱ [- ; token P γ] ⊫ [bupd]; P 1 ✱ [no_tokens P γ 1 ∗ ⌜Fractional P⌝].
  Proof. 
    rewrite /BiAbd => ->. 
    cbn -[bi_intuitionistically_if tokenizable.token tokenizable.token_counter]. 
    rewrite tokenizable.biabd_delete_last_token_to_req.
    iSteps.
  Qed.

  Global Instance no_tokens_split γ q1 q2 mq :
    FracSub q1 q2 mq →
    HINT no_tokens P γ q1 ✱ [- ; True] ⊫ [id]; no_tokens P γ q2 ✱ [match mq with | Some q => no_tokens P γ q | None => True end].
  Proof. apply: tokenizable.no_tokens_split. Qed.

  Fixpoint token_iter n γ : iProp Σ :=
    match n with
    | O => emp
    | S n => token P γ ∗ token_iter n γ
    end.

  Lemma token_iter_eq γ n : token_iter n γ = tokenizable.tokenizable.token_iter (fractional_payload_token_counter P) n γ.
  Proof.
    induction n as [| n IH] => //=.
    by rewrite IH.
  Qed.

  Global Instance merge_token_no_tokens γ q :
    MergableConsume (▷ token P γ) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens P γ q))
            (TCEq Pout (◇ False)))%I | 11.
  Proof. apply: tokenizable.merge_token_no_tokens. Qed.

  Global Instance merge_token_no_tokens_now γ q :
    MergableConsume (token P γ) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens P γ q))
            (TCEq Pout (◇ False)))%I | 11.
  Proof. apply: tokenizable.merge_token_no_tokens_now. Qed.

  Global Instance merge_no_tokens_token_now γ q :
    MergableConsume (no_tokens P γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (token P γ))
            (TCEq Pout (◇ False)))%I | 10.
  Proof. apply: tokenizable.merge_no_tokens_token_now. Qed.

  Global Instance merge_no_tokens_token γ q :
    MergableConsume (no_tokens P γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (▷ token P γ))
            (TCEq Pout (◇ False)))%I | 10.
  Proof. apply: tokenizable.merge_no_tokens_token. Qed.

  Global Instance merge_no_tokens_token_later_counter γ q pos:
    MergableConsume (no_tokens P γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (▷ token_counter P γ pos))
            (TCEq Pout (◇ False)))%I | 11.
  Proof. apply: tokenizable.merge_no_tokens_token_later_counter. Qed.

  Global Instance merge_no_tokens_token_counter γ q pos:
    MergableConsume (no_tokens P γ q) true (λ p Pin Pout,
      TCAnd (TCEq Pin (token_counter P γ pos))
            (TCEq Pout (◇ False)))%I | 12.
  Proof. apply: tokenizable.merge_no_tokens_token_counter. Qed.

  Global Instance merge_no_tokens γ q q1 q2 :
    MergableConsume (no_tokens P γ q1) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens P γ q2)) $
      TCAnd (IsOp q q1 q2) 
            (TCEq Pout (no_tokens P γ q ∗ ⌜q ≤ 1⌝%Qp)))%I | 20.
  Proof. apply: tokenizable.merge_no_tokens. Qed.

  Global Instance merge_token_counter_no_tokens γ q pos:
    MergableConsume (token_counter P γ pos) true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens P γ q))
            (TCEq Pout (◇ False)))%I | 11.
  Proof. apply: tokenizable.merge_token_counter_no_tokens. Qed.

  Global Instance merge_token_counter_no_tokens_later γ q pos:
    MergableConsume (▷ token_counter P γ pos)%I true (λ p Pin Pout,
      TCAnd (TCEq Pin (no_tokens P γ q))
            (TCEq Pout (◇ False)))%I | 11.
  Proof. apply: tokenizable.merge_token_counter_no_tokens_later. Qed.

  Global Instance no_tokens_fractional γ : Fractional (no_tokens P γ).
  Proof. tc_solve. Qed.

  Global Instance no_tokens_timeless γ q : Timeless (no_tokens P γ q).
  Proof. tc_solve. Qed.

  Global Instance token_into_exist γ : 
    AtomIntoExist (token P γ) (λ q : Qp, P q ∗ ◇ (P q -∗ token P γ))%I.
  Proof. 
    rewrite /AtomIntoExist /IntoExistCareful2 /IntoExistCareful. iSteps as (q) "Hq".
    iApply bi.except_0_intro. iSteps.
  Qed.

  Global Instance token_is_easy γ : IsEasyGoal (token P γ) := I.
End automation.

Global Opaque token_counter.
Global Opaque no_tokens.
Global Opaque token.

(* this is missing and sometimes causes very slow TC resolution.
    TODO: merge into Iris? *)
Global Hint Mode Fractional + + : typeclass_instances.

Section token_counter_extra.
  Context `{!fracTokenG Σ}.
  Context (P : Qp → iProp Σ).

  Global Instance biabd_alloc_multiple (p : positive) :
    HINT ε₁ ✱ [- ; ⌜Fractional P⌝ ∗ P 1] ⊫ [bupd] γ; token_counter P γ p ✱ [token_iter P (Pos.to_nat p) γ] | 500.
  Proof.
    rewrite /BiAbd /= empty_hyp_last_eq left_id.
    rewrite -{1}(Pos2Nat.id p).
    assert (0 < Pos.to_nat p) by lia. revert H.
    generalize (Pos.to_nat p) => n {p}.
    induction n as [|[|n] IHn]; first lia.
    - clear IHn => _. cbn. iSteps.
    - intros Hn.
      rewrite IHn; last by lia.
      iSteps.
  Qed.

  Global Instance token_iter_token (n : nat) γ :
    SolveSepSideCondition (0 < n) →
    HINT token_iter P n γ ✱ [ - ; emp] ⊫ [id]; token P γ ✱ [token_iter P (Nat.pred n) γ] | 55.
  Proof.
    rewrite /SolveSepSideCondition. destruct n => [| _]; first lia.
    iSteps.
  Qed.

  Global Instance token_iter_token_iter (n1 n2 : nat) γ :
    SolveSepSideCondition (n1 = n2) →
    HINT token_iter P n1 γ ✱ [ - ; emp] ⊫ [id]; token_iter P n2 γ ✱ [emp] | 55.
  Proof. move => ->; tc_solve. Qed.

  Global Instance token_counter_eq_hint γ p1 p2 :
    SolveSepSideCondition (p1 = p2) →
    HINT token_counter P γ p1 ✱ [- ; emp] ⊫ [id]; token_counter P γ p2 ✱ [emp] | 55.
  Proof. rewrite /SolveSepSideCondition => ->. tc_solve. Qed.

  (* disj instances *)

  Global Instance no_tokens_change q mq γ :
    FracSub 1%Qp q mq →
    HINT no_tokens P γ q ✱ [-; ⌜Fractional P⌝ ∗ P 1 ∗ match mq with | None => emp | Some q' => no_tokens P γ q' end] 
        ⊫ [bupd]; token P γ ✱ [token_counter P γ 1].
  Proof.
    rewrite /FracSub /BiAbd /=.
    case: mq => [q' Hqq' | ->].
    - iStep as (HP _) "HP Hnot". rewrite Hqq'.
      rewrite (comm _ (token _ _)).
      iSteps.
    - iStep as (HP) "Hnot HP".
      rewrite (comm _ (token _ _)).
      iSteps.
  Qed.

  Global Instance token_dealloc_hint γ p q φ mq :
    TCIf (SolveSepSideCondition (p ≠ 1)%positive) False TCTrue →
    CmraSubtract 1%Qp q φ mq →
    BiAbdDisj (TTl := [tele]) (TTr := [tele]) false 
      (token_counter P γ p)
      (no_tokens P γ q)
      bupd
      (token P γ ∗ ⌜φ⌝)%I
      (⌜p = 1%positive⌝ ∗ ⌜Fractional P⌝ ∗ P 1 ∗ default emp (q' ← mq; Some $ no_tokens P γ q'))%I
      (Some2 ( ⌜p ≠ 1%positive⌝ ∗ token_counter P γ p ∗ token P γ))%I.
  Proof.
    rewrite /BiAbdDisj =>  /= _ Hq.
    destruct (decide (p = xH)) as [-> |Hneq]; iSteps.
  Qed.
End token_counter_extra.



