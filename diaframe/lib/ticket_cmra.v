From iris.algebra Require Export coPset.
From diaframe Require Import util_classes solve_defs.
From diaframe.lib Require Import own_hints.


(* Wrapper around the coPset cmra, making it easier for Diaframe to be guided by the syntax *)

Section ticket_lemmas.
  Definition ticket (n : nat) : coPset := {[ Pos.of_succ_nat n ]}.

  Fixpoint tickets_lt_prim (n : nat) : coPset :=
    match n with
    | O => ∅
    | S n => ticket n ∪ tickets_lt_prim n
    end.

  Definition tickets_geq (n : nat) : coPset := ⊤ ∖ tickets_lt_prim n.

  Lemma tickets_distinct n m : ticket n ## ticket m ↔ n ≠ m.
  Proof.
    split => Hnm1.
    - move => Hnm2. subst.
      revert Hnm1 => /disjoint_intersection_L.
      rewrite intersection_singletons_L. set_solver.
    - unfold ticket.
      apply disjoint_singleton_l.
      apply not_elem_of_singleton_2.
      contradict Hnm1. simplify_eq. lia.
  Qed.

  Lemma tickets_distinct_from_lt m n : (n ≤ m) ↔ ticket m ## tickets_lt_prim n.
  Proof.
    move: m.
    induction n as [|n].
    { split; [ set_solver | lia]. }
    move => m /=; split.
    - move => Hm.
      apply disjoint_union_r; split.
      * apply tickets_distinct. lia.
      * apply IHn. lia.
    - move => /disjoint_union_r [/tickets_distinct + /IHn +]. lia.
  Qed.

  Lemma ticket_in_tickets_geq n m : (n ≤ m) ↔ ticket m ⊆ tickets_geq n.
  Proof.
    unfold tickets_geq; split => Hnm.
    - apply subseteq_difference_r; last done.
      by apply tickets_distinct_from_lt.
    - apply tickets_distinct_from_lt. set_solver.
  Qed.

  Lemma split_tickets_geq (n : nat) :
    tickets_geq n = tickets_geq (S n) ∪ ticket n.
  Proof.
    rewrite /tickets_geq /=.
    rewrite difference_union_distr_r_L.
    rewrite union_intersection_r_L.
    rewrite difference_union_L.
    replace (⊤ ∪ ticket n) with (⊤ : coPset) by set_solver.
    replace (⊤ ∩ (⊤ ∖ tickets_lt_prim n ∪ ticket n)) with ((⊤ ∖ tickets_lt_prim n ∪ ticket n)) by set_solver.
    enough (ticket n ⊆ ⊤ ∖ tickets_lt_prim n). set_solver.
    by apply ticket_in_tickets_geq.
  Qed.

  Global Instance tickets_geq_subtract n m:
    SolveSepSideCondition (m = S n) →
    UcmraSubtract (CoPset $ tickets_geq n) (CoPset $ tickets_geq m) True (CoPset $ ticket n).
  Proof.
    rewrite /UcmraSubtract /CmraSubtract /= => -> _.
    rewrite coPset_disj_union; last first.
    { unfold tickets_geq. apply disjoint_difference_l1. simpl. set_solver. }
    rewrite -split_tickets_geq //.
  Qed.

  Global Instance tickets_geq_equal n m :
    SolveSepSideCondition (n = m) →
    UcmraSubtract (CoPset $ tickets_geq n) (CoPset $ tickets_geq m) True ε.
  Proof.
    rewrite /SolveSepSideCondition => ->. apply _.
  Qed.

  Global Instance tickets_equal n m :
    SolveSepSideCondition (n = m) →
    UcmraSubtract (CoPset $ ticket n) (CoPset $ ticket m) True ε.
  Proof.
    rewrite /SolveSepSideCondition => ->. apply _.
  Qed.

  Fixpoint tickets_lt (n : nat) : coPset :=
    match n with
    | O => ∅
    | 1 => ticket 0
    | S n => ticket n ∪ tickets_lt n
    end.

  Lemma tickets_lt_lt_prim_eq (n : nat) : tickets_lt n = tickets_lt_prim n.
  Proof.
    induction n => //=.
    destruct n; first simpl.
    - apply leibniz_equiv. by rewrite right_id.
    - by rewrite IHn.
  Qed.

  Global Instance tickets_lt_subtract n m :
    SolveSepSideCondition (n = S m) →
    UcmraSubtract (CoPset $ tickets_lt n) (CoPset $ tickets_lt m) True (CoPset $ ticket m).
  Proof.
    rewrite !tickets_lt_lt_prim_eq.
    rewrite /SolveSepSideCondition => ->.
    rewrite /UcmraSubtract /CmraSubtract /= => _.
    rewrite coPset_disj_union.
    f_equiv. set_solver.
    symmetry.
    by apply tickets_distinct_from_lt.
  Qed.

  Global Instance tickets_geq_coalloc n :
    CoAllocate (CoPset $ tickets_geq n) (Some $ CoPset $ tickets_lt n).
  Proof.
    rewrite tickets_lt_lt_prim_eq.
    split.
    - split => _ //=.
      apply coPset_disj_valid_op. rewrite /tickets_geq. set_solver.
    - move => [c|].
      * move => /coPset_disj_included Hc1.
        move => /coPset_disj_valid_op Hc2.
        assert (∃ d, c = d ∪ tickets_lt_prim n ∧ tickets_lt_prim n ## d) as [d [Hd1 Hd2]].
        { exists (c ∖ tickets_lt_prim n); split; last set_solver.
          rewrite difference_union_L. set_solver. }
        revert Hc1 Hc2. subst => _ /disjoint_union_r [Hd3 _].
        assert (tickets_lt_prim n ∪ tickets_geq n ## d) as Hd by by apply disjoint_union_l.
        move: Hd {Hd2 Hd3}.
        rewrite /tickets_geq -union_difference_L; last set_solver.
        move => Hd. assert (d = ∅) as ->. { set_solver. }
        rewrite left_id_L //.
      * done.
  Qed.

End ticket_lemmas.

Definition ticketR := coPset_disjR.
Definition ticketUR := coPset_disjUR.

From iris.base_logic.lib Require Import own.
From iris.proofmode Require Import tactics.
From diaframe Require Import proofmode_base lib.persistently.

Section ticket_merges.
  Context `{!inG Σ ticketR}.

  Global Instance merge_ticket_with_ticket γ n m :
    MergablePersist (own γ $ CoPset $ ticket m) (λ p Pin Pout,
      TCAnd (TCEq Pin (own γ $ CoPset $ ticket n)) $
          TCEq Pout ⌜n ≠ m⌝)%I.
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    iStep as "Hm Hn".
    iDestruct (own_valid_2 with "Hm Hn") as %?%coPset_disj_valid_op%tickets_distinct.
    iSteps.
  Qed.

  Global Instance merge_ticket_with_geq γ n m :
    MergablePersist (own γ $ CoPset $ tickets_geq m) (λ p Pin Pout,
      TCAnd (TCEq Pin (own γ $ CoPset $ ticket n)) $
          TCEq Pout ⌜n < m⌝)%I.
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    iStep as "Hm Hn".
    iDestruct (own_valid_2 with "Hm Hn") as %?%coPset_disj_valid_op. iStep.
    destruct (decide (n < m)) => //.
    assert (m ≤ n) as Hnm by lia.
    apply ticket_in_tickets_geq in Hnm.
    absurd (ticket n ## ticket n); last set_solver.
    rewrite disjoint_intersection_L intersection_singletons_L. set_solver.
  Qed.

  Global Instance tickets_geq_need_extra γ n m : 
    SolveSepSideCondition (m = S n) →
    HINT own γ (CoPset (tickets_geq m)) ✱ [- ; own γ (CoPset $ ticket n)] ⊫ [id]; 
         own γ (CoPset (tickets_geq n)) ✱ [emp].
  Proof.
    move => ->. iStep as "Hm Hn".
    rewrite (split_tickets_geq n) -coPset_disj_union; last first.
    { unfold tickets_geq. apply disjoint_difference_l1. simpl. set_solver. }
    rewrite own_op.
    iSteps.
  Qed.

  Global Instance my_ticket_disj_hint γ x1 x2 :
    TCIf (SolveSepSideCondition (x1 ≠ x2)) False TCTrue →
    BiAbdDisj (TTl := [tele]) (TTr := [tele]) false (own γ (CoPset $ ticket x1)) (own γ (CoPset $ ticket x2)) id
      emp%I ⌜x1 = x2⌝%I (Some2 (own γ (CoPset $ ticket x1) ∗ ⌜x1 ≠ x2⌝))%I | 55. (* priority after asmp hint *)
  Proof.
    rewrite /BiAbdDisj /= => _ .
    destruct (decide (x1 = x2)) as [-> | Hneq]; iSteps.
  Qed.
End ticket_merges.


