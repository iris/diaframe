From iris.program_logic Require Import weakestpre.
From iris.proofmode Require Import base environments ltac_tactics.
From diaframe.symb_exec Require Import defs.
From diaframe Require Import solve_defs util_classes.
From diaframe.steps Require Import tactics.

(* This file contains an extension to Diaframe: the 'iExpr [e] has post [U] with [hyps]' tactic.
   If you use this tactic on a WP (K e) {{ Φ }}, you will obtain two new goals:
   - the first goal requires you to show WP e {{ U }}, using hypothesis specified in hyps
   - in the second goal you obtain U and all remaining hypothesis, to prove WP (K v) {{ Φ }}.
   This can be useful if using Diaframe's automation directly causes too many case splits. *)

Section lemmas.
  Context `{!irisGS Λ Σ}.

  Lemma wp_bind_wand K `{!LanguageCtx K} s E e Φ Ψ :
    WP e @ s; E {{ Φ }} ∗ (∀ v, Φ v -∗ WP K (of_val v) @ s; E {{ Ψ }}) ⊢ WP K e @ s; E {{ Ψ }}.
  Proof. rewrite -wp_bind. exact: wp_wand_r. Qed.

  Lemma tac_wp_cut_step_on Φ (js : list ident) e' K e E s Ψ Δ P M :
    CollectModal P M (WP e @ s; E {{ Ψ }}) →
    IntroducableModality M →
    ReshapeExprAnd (expr Λ) e K e' (LanguageCtx K) →
    match envs_split Left js Δ with
    | Some (Δ1,Δ2) =>
       (* The constructor [conj] of [∧] still stores the contexts [Δ1] and [Δ2'] *)
       envs_entails Δ1 (WP e' @ s ; E {{ Φ }}) ∧ envs_entails Δ2 (∀ v, Φ v -∗ WP K (of_val v) @ s ; E {{ Ψ }})
    | None => False
    end →
    envs_entails Δ P.
  Proof.
    destruct (envs_split _ _ _) as [[Δ1 Δ2]|] eqn:? => //.
    rewrite /CollectModal /ModWeaker => <- <- /=.
    case => -> HK.
    case.
    rewrite envs_entails_unseal.
    rewrite (envs_split_sound Δ) // => -> ->.
    exact: wp_bind_wand.
  Qed.
End lemmas.

(* Local post notation *)
  Notation "{{ x .. y , 'RET' pat ; Q } }" :=
    (λ v, (∃ x, .. (∃ y, ⌜v = pat⌝ ∗ Q) .. ))%I
    (at level 20, x closed binder, y closed binder,
       format " {{  x  ..  y ,  RET  pat ;  Q  } } ") : bi_scope.

  Notation "{{ 'RET' pat ; Q } }" :=
    (λ v, ⌜v = pat⌝ ∗ Q)%I
    (at level 20,
       format "{{  RET  pat ;  Q  } } ") : bi_scope.

  Tactic Notation "iExpr" open_constr(efoc) "has" "post" constr(post) "with" constr(idents) :=
      notypeclasses refine (tac_wp_cut_step_on post (map INamed idents) efoc _ _ _ _ _ _ _ _ _ _ _ _);
      [tc_solve.. | simpl; split; [| do 2 iStep ]].