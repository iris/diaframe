From iris.bi Require Export bi telescopes lib.laterable lib.fixpoint.
From iris.proofmode Require Import proofmode environments.
From diaframe Require Import proofmode_base utils_ltac2.

Set Default Proof Using "Type*".


(* Adds a do_löb constructor, and appropriate hints so that [do_löb TT args pre goal] 
   will cause Löb induction to be done over (pre args -∗ goal args), while generalizing 
   at least over (args : TT). It will generalize over more variables if they are detected
   to be relevant *)

Class AsFunOfOnly {A : Type} (a : A) {TT : tele@{bi.Quant}} (tt : TT) (a' : TT -t> A) :=
  as_fun_of_only : tele_app a' tt = a.

(* Universes have been very carefully tweaked to not force bi.Logic <= bi.Quant here,
  or in clients. *)
Polymorphic Class AsFunOfAmongOthers {A : Type} (a : A) {TT : tele@{bi.Quant}} (tt : TT) {TT2 : tele@{bi.Quant}} (tt2 : TT2) 
  (a' : tele_fun@{bi.Quant diaframe_telefun diaframe_telefun} TT2 (tele_fun@{bi.Quant _ diaframe_telefun} TT A)) :=
  as_fun_of_among_others : tele_app (tele_app a' tt2) tt = a.

Class GatherPureOn {TT : tele@{bi.Quant}} (tt : TT) (the_pure : TT -t> Prop) := 
  gather_pure_holds : tele_app the_pure tt.

Class GetRelevantPure {TT : tele@{bi.Quant}} (tt : TT) (φ : Prop) :=
  relevant_pure_holds : φ.

Global Instance gather_pure_on_inst {TT : tele@{bi.Quant}} (tt : TT) φ φ' :
  GetRelevantPure tt φ →
  AsFunOfOnly φ tt φ' →
  GatherPureOn tt φ'.
Proof.
  rewrite /GetRelevantPure /do_lob.AsFunOfOnly /GatherPureOn.
  move => Hφ -> //.
Qed.


(* This class is used to filter out variables that can be detected as non-generalizable.
    These are usually section variables that are then used in definitions, and that cannot yet be seen as a parameter of this definition. *)
Class ReFilterNonGeneralizableVariables `(a : A) {TT_in : tele@{bi.Quant}} 
      (args_in : TT_in) {TT_out : tele@{bi.Quant}} (args_out : TT_out) (f : TT_out -t> TT_in) :=
  refiltered_variables : tele_app f args_out = args_in.


Ltac get_as_fun_of_raw term arg := (* does not check that arg is a variable *)
  let pat := eval pattern arg in term in
  lazymatch pat with
  | ?the_fun arg =>
    constr:(the_fun)
  end.

Ltac get_as_fun_of term arg := (* shortcircuits to trivial function if arg is not a variable *)
  constr:(
    ltac:(
      (is_var arg; 
       let result := get_as_fun_of_raw term arg in
       exact result) ||
      (assert_fails (is_var arg);
      let atype := type of arg in
      let result := constr:((λ _ : atype, term)) in
      exact result)
  )).

Ltac find_as_fun_only := 
  match goal with 
    | |- ?f = ?rhs =>
      is_evar f; done
    | |- ?f ?r = ?rhs =>
      let rhs_f := get_as_fun_of rhs r in
      let H := fresh "H" in
      enough (f = rhs_f) as H; [rewrite H; done | ]; find_as_fun_only
    end.

Global Hint Extern 4 (AsFunOfOnly _ _ _) =>
  unfold AsFunOfOnly; cbn; find_as_fun_only : typeclass_instances.



Ltac find_as_fun_only_raw := 
  match goal with 
    | |- ?f = ?rhs =>
      is_evar f; done
    | |- ?f ?r = ?rhs =>
      let rhs_f := get_as_fun_of_raw rhs r in
      let H := fresh "H" in
      enough (f = rhs_f) as H; [rewrite H; done | ]; find_as_fun_only
    end.


Section do_lob_props.
  Context {PROP : bi} `{!BiLöb PROP}.

  Definition do_löb_def (TT : tele) (args : TT) (pre : TT -t> PROP) (goal : TT -t> PROP) : PROP :=
    (tele_app pre args) -∗ (tele_app goal args).
  Definition do_löb_aux : seal (@do_löb_def). Proof. by eexists. Qed.
  Definition do_löb := do_löb_aux.(unseal).
  Lemma do_löb_eq : @do_löb = @do_löb_def. Proof. by rewrite -do_löb_aux.(seal_eq). Qed.


  Lemma absorb_fun_of_only (TT : tele) args pre goal P P' :
    AsFunOfOnly P args P' →
    do_löb TT args (tele_bind (λ tt, tele_app pre tt ∗ tele_app P' tt)) goal ⊢ P -∗ do_löb TT args pre goal.
  Proof.
    rewrite /AsFunOfOnly => <- {P}.
    rewrite do_löb_eq /do_löb_def /= tele_app_bind.
    iSteps.
  Qed.

  Lemma absorb_fun_of_more_no_bind_check (TT : tele) args pre goal P (TT2 : tele) (tt2 : TT2) P' :
    AsFunOfAmongOthers P args tt2 P' →
    do_löb (TelePairType TT TT2) (tele_pair_arg args tt2)
      (tele_curry $ tele_dep_bind (λ tt1, tele_map (bi_sep (tele_app pre tt1)) $ as_dependent_fun _ _ (
                                  tele_map (λ f, tele_app f tt1) P') tt1))
      (tele_curry $ tele_dep_bind (λ tt1, tele_bind (λ _, tele_app goal tt1)))
      ⊢ P -∗ do_löb TT args pre goal.
  Proof.
    rewrite /AsFunOfAmongOthers => <-.
    rewrite do_löb_eq /do_löb_def /=.
    iStep 3 as "HΦ HP Hpre".
    rewrite /tele_pair_arg.
    rewrite !tele_curry_uncurry_relation.
    rewrite -!tele_uncurry_curry_compose.
    rewrite !tele_dep_appd_bind.
    rewrite !tele_map_app.
    rewrite -!dependent_fun_eq.
    rewrite !tele_map_app.
    rewrite tele_app_bind.
    iSteps.
  Qed.

  Lemma absorb_fun_of_more_re_bind (TT TT2 : tele@{bi.Quant}) 
    args pre goal (goal' : TT -t> TT2 -t> PROP) P' (tt2 : TT2) P :
    AsFunOfAmongOthers P args tt2 P' →
    (∀.. (tt1 : TT), AsFunOfOnly (tele_app goal tt1) tt2 (tele_app goal' tt1)) →
    do_löb (TelePairType@{bi.Quant _ _ bi.Quant bi.Quant} TT TT2) (tele_pair_arg args tt2)
      (tele_curry $ tele_dep_bind (λ tt1, tele_map (bi_sep (tele_app pre tt1)) $ as_dependent_fun _ _ 
          (tele_map@{bi.Quant _ _ diaframe_telefun diaframe_telefun} (λ f, tele_app f tt1) P') tt1))
      (tele_curry $ tele_dep_bind (λ tt1, as_dependent_fun _ _  (tele_app goal' tt1) tt1))
      ⊢ P -∗ do_löb TT args pre goal.
  Proof.
    rewrite /AsFunOfAmongOthers => <- /tforall_forall Hgoal.
    rewrite do_löb_eq /do_löb_def /=.
    iStep 3 as "HΦ HP' Hpre".
    rewrite /tele_pair_arg.
    rewrite !tele_curry_uncurry_relation.
    rewrite -!tele_uncurry_curry_compose.
    rewrite !tele_dep_appd_bind.
    rewrite !tele_map_app.
    rewrite -!dependent_fun_eq.
    rewrite !tele_map_app.
    rewrite -Hgoal.
    iSteps.
  Qed.

  Lemma envs_entails_spat_pers_ind (Δ : envs PROP) G1 G2 f :
    (AsFunOfOnly G1 ((@TeleArgCons PROP (λ _ : PROP, TeleO) ([∗] env_spatial Δ)%I tt) : tele_arg $ TeleS (λ _ : PROP, TeleO)) f) →
    (∀ P, □ (P -∗ f P) ∗ P ⊢ G2) →
    envs_entails Δ G1 →
    envs_entails Δ G2.
  Proof.
    rewrite envs_entails_unseal of_envs_alt.
    assert (⌜envs_wf Δ⌝ ∧ □ [∧] env_intuitionistic Δ ⊢ □ (⌜envs_wf Δ⌝ ∧ □ [∧] env_intuitionistic Δ)) as H.
    { iIntros "[% #HΔ] !>". eauto. }
    revert H.
    rewrite bi.persistent_and_sep_assoc.
    generalize (⌜envs_wf Δ⌝ ∧ □ [∧] env_intuitionistic Δ)%I => R {2}->. rewrite /AsFunOfOnly /= => <-.
    generalize ([∗] env_spatial Δ)%I => P HG2 /bi.wand_intro_r ->.
    apply HG2.
  Qed.

  Ltac do_env_ind ident :=
    eapply envs_entails_spat_pers_ind;
    [ unfold AsFunOfOnly; cbn; find_as_fun_only_raw
    | move => ident; cbn ].

  Lemma run_do_löb (TT : tele) args pre goal Δ φ :
    GatherPureOn args φ → (* this could also be stated as above.. not sure what's best, for above would require extended context *)
    envs_entails Δ ((□ ▷ (∀.. (tt : TT), tele_app pre tt ∗ [∗] (env_spatial Δ) ∗ <affine> ⌜tele_app φ tt⌝ -∗ tele_app goal tt)) -∗ 
                         (∀.. (tt : TT), tele_app pre tt ∗ <affine> ⌜tele_app φ tt⌝ -∗ tele_app goal tt)) →
    envs_entails Δ $ do_löb TT args pre goal.
  Proof.
    rewrite /GatherPureOn => Hφ.
    do_env_ind P.
    rewrite do_löb_eq /do_löb_def. 
    iIntros "[#IH2 HP] Har".
    iRevert (Hφ). iIntros "Hφ".
    iLöb as "IH" forall (args).
    iApply ("IH2" with "HP [] [$Har $Hφ]"); last first.
    iIntros "!> !>" (tt) "[Hpre [HP Hφ]]". iApply ("IH" with "HP Hpre"). done. 
  Qed. (* careful not to use iSteps in this proof for universe reasons. *)

  Lemma run_do_löb_filter_gen (TT : tele) args pre goal Δ φ (TT' : tele) (args' : TT') f : 
    GatherPureOn args φ →
    ReFilterNonGeneralizableVariables (tele_app pre args, tele_app goal args) args args' f →
    envs_entails Δ ((□ ▷ (∀.. (tt' : TT'), let tt := tele_app f tt' in tele_app pre tt ∗ [∗] (env_spatial Δ) ∗ <affine> ⌜tele_app φ tt⌝ -∗ tele_app goal tt)) -∗ 
                         (∀.. (tt' : TT'), let tt := tele_app f tt' in tele_app pre tt ∗ <affine> ⌜tele_app φ tt⌝ -∗ tele_app goal tt)) →
    envs_entails Δ $ do_löb TT args pre goal.
  Proof.
    rewrite /GatherPureOn /ReFilterNonGeneralizableVariables => Hφ Hf. subst args.
    do_env_ind P.
    rewrite do_löb_eq /do_löb_def. 
    iIntros "[#IH2 HP] Har".
    iRevert (Hφ). iIntros "Hφ".
    iLöb as "IH" forall (args').
    iApply ("IH2" with "HP [] [$Har $Hφ]"); last first.
    iIntros "!> !>" (tt) "[Hpre [HP Hφ]]". iApply ("IH" with "HP Hpre"). done.
  Qed.
End do_lob_props.


Class VariablesOccurIn {A : Type} (a : A) {TT : tele} (tt : TT) := they_indeed_occur : True.


Section do_lob_proofmode.
  Context {PROP : bi} `{!BiLöb PROP}.

  Global Instance do_lob_as_solve_goal (M : PROP → PROP) (TT : tele) args pre goal:
    IntroducableModality M →
    AsSolveGoal M (do_löb TT args pre goal) (Transform $ do_löb TT args pre goal).
  Proof. unseal_diaframe; rewrite /AsSolveGoal. eauto. Qed.

  (* TODO: this instance could maybe be dropped? *)
  Global Instance do_lob_re_intro_only p P TT (P' : TT -t> PROP) args pre goal : 
    VariablesOccurIn P args →
    AsFunOfOnly P args P' →
    TRANSFORM □⟨p⟩ P, do_löb TT args pre goal =[hyp]=> 
        do_löb TT args 
            (tele_bind (λ tt, tele_app pre tt ∗ tele_app (tele_map (bi_intuitionistically_if p) P') tt))%I 
         goal | 20.
  Proof.
    move => _ HP. rewrite /TransformHyp /=.
    assert (AsFunOfOnly (□?p P)%I args (tele_map (bi_intuitionistically_if p) P')).
    { revert HP. rewrite /AsFunOfOnly => <-. rewrite tele_map_app //. }
    rewrite -absorb_fun_of_only //.
  Qed.

  Global Instance do_lob_re_intro_more_better (p : bool) P TT1 TT2 (P' : TT2 -t> TT1 -t> PROP) 
      pre (goal : TT1 -t> PROP) goal' (tt2 : TT2) (tt1 : TT1) : 
    VariablesOccurIn P tt1 →
    AsFunOfAmongOthers P tt1 tt2 P' →
    (TC∀.. (tt1 : TT1), AsFunOfOnly (tele_app goal tt1) tt2 (tele_app goal' tt1)) →
    TRANSFORM □⟨p⟩ P, do_löb TT1 tt1 pre goal =[hyp]=>
      do_löb (TelePairType TT1 TT2) (tele_pair_arg tt1 tt2) 
            (tele_curry $ tele_dep_bind (λ tt1, tele_map (bi_sep (tele_app pre tt1)) $ 
                                                as_dependent_fun _ _ (tele_map (λ f, tele_app f tt1) (tele_map (tele_map $ bi_intuitionistically_if p) P')) tt1))
            (tele_curry $ tele_dep_bind (λ tt1, as_dependent_fun _ _  (tele_app goal' tt1) tt1)) | 15.
  Proof.
    move => _.
    rewrite /TransformHyp /TCTForall => HP Hgoal.
    assert (AsFunOfAmongOthers (□?p P)%I tt1 tt2 
      (tele_map (tele_map@{bi.Quant _ _ diaframe_telefun diaframe_telefun} $ bi_intuitionistically_if p) P')).
    { revert HP. rewrite /AsFunOfAmongOthers => <-. rewrite !tele_map_app //. }
    rewrite -absorb_fun_of_more_re_bind //.
  Qed.

  Definition post_löb_def (arg : PROP) : PROP := arg.
  Definition post_löb_aux : seal (@post_löb_def). Proof. by eexists. Qed.
  Definition post_löb := post_löb_aux.(unseal).
  Global Opaque post_löb.
  Global Arguments post_löb _ : simpl never.
  Lemma post_löb_eq : @post_löb = @post_löb_def.
  Proof. rewrite -post_löb_aux.(seal_eq) //. Qed.

  Lemma run_do_lob_mod Δ (TT : tele) (args : TT) φ pre goal : 
    GatherPureOn args φ →
    TRANSFORM Δ, do_löb TT args pre goal =[ctx]=>
      ((□ ▷ (∀.. (tt : TT), tele_app pre tt ∗ [∗] (env_spatial Δ) ∗ <affine> ⌜tele_app φ tt⌝ -∗ tele_app goal tt)) -∗ 
                         (∀.. (tt : TT), tele_app pre tt ∗ <affine> ⌜tele_app φ tt⌝ -∗ post_löb $ tele_app goal tt)).
  Proof.
    rewrite /TransformCtx. rewrite post_löb_eq.
    apply run_do_löb.
  Qed.

  Global Instance run_do_lob_mod_filter_no_gen `{!BiLöb PROP} (Δ : envs PROP) (TT : tele) (args : TT) (TT' : tele) (args' : TT') f φ pre goal : 
    GatherPureOn args φ →
    ReFilterNonGeneralizableVariables (tele_app pre args, tele_app goal args) args args' f →
    TRANSFORM Δ, do_löb TT args pre goal =[ctx]=>
      ((□ ▷ (∀.. (tt' : TT'), let tt := tele_app f tt' in tele_app pre tt ∗ [∗] (env_spatial Δ) ∗ <affine> ⌜tele_app φ tt⌝ -∗ tele_app goal tt)) -∗ 
                         (∀.. (tt' : TT'), let tt := tele_app f tt' in tele_app pre tt ∗ <affine> ⌜tele_app φ tt⌝ -∗ post_löb $ tele_app goal tt)) | 0.
  Proof.
    rewrite /TransformCtx. rewrite post_löb_eq.
    apply run_do_löb_filter_gen.
  Qed.
End do_lob_proofmode. 


Ltac strip_known_fun_args :=
  lazymatch goal with 
  | |- tele_app ?f ?r = ?rhs =>
    is_evar f
  | |- ?f ?r = ?rhs =>
    let rhs_f := get_as_fun_of rhs r in
    let H := fresh "H" in
    enough (f = rhs_f) as H; [rewrite H; done | ]; strip_known_fun_args
  end.

(* the NoLobGen class explicitly forbids do_löb from generalizing over certain variables.
   For example, we dont want to generalize over the Σ in iProp Σ *)

Class NoLobGen {A : Type} (a : A) := { no_lob_gen : True }.

Global Instance dont_prop_gen (PROP : bi) : NoLobGen PROP := ltac:(by constructor).
Global Hint Extern 4 (@NoLobGen (?X ?Σ) _) =>
  lazymatch (type of Σ) with
  | iprop.gFunctors => by constructor
  end : typeclass_instances.
Global Instance no_gfunctors_gen (Σ : iprop.gFunctors) : NoLobGen Σ := ltac:(by constructor).


Class IsSectionVar {A : Type} (a : A) := { is_section_var : True }.
Global Instance no_lob_on_section_vars `(a : A) : IsSectionVar a → NoLobGen a.
Proof. by constructor. Qed.

Ltac check_implicitly_used_section_var a := 
  is_var a; (* old Ltac hack:
  assert_succeeds (
    assert (∃ b, a = b) as [? ?] by admit;
    assert_fails (subst a)
  ) *)
  let f := ltac2:(arg |-
    let term := Option.get (Ltac1.to_constr arg) in
    match Constr.Unsafe.kind term with
    | Constr.Unsafe.Var i =>
      (* now check if this ident is a section variable with Env.get.
          Env.get returns an optional reference. If it is present,
          the ident is a section variable *)
      match Env.get [i] with
      | None => Control.zero Assertion_failure
      | Some _ => ()
      end
    | _ => Control.zero Assertion_failure
    end
  ) in
  f a.

Global Hint Extern 4 (@IsSectionVar ?A ?a) =>
  check_implicitly_used_section_var a;
  by constructor : typeclass_instances.

Section test_is_section_var.
  Context (n : nat).
  Definition m : nat := n.

  (* this worked with the old Ltac hack: 
      n is implicitly used in the definition of m, which is relevant in our context *)
  Goal m = m → IsSectionVar n.
  Proof. intros Heq. tc_solve. Qed.

  (* this used to fail with the old Ltac hack: 
    n can be substituted, since we do not use m here *)
  Goal IsSectionVar n.
  Proof. tc_solve. Abort.

  Goal True.
  Proof.
    (* this used to fail if one uses Control.hyp directly with shelve:
      apparently, the old solution only worked on goals that are presented to the user,
      not ones visible in the intermediate proof state monad. *)
    assert_succeeds (assert (IsSectionVar n) by tc_solve). exact I.
  Qed.
End test_is_section_var.


Ltac discover_other_args := 
  match goal with 
  | |- tele_app ?f ?free_arg = ?leftpart =>
    (* maybe we could instead match on hypotheses, and filter all terms there.
       below will produce all terms that appear in the goal, for EVERY occurence.
       I think this might be quite inefficient *)
    match leftpart with
    | context [?term] => 
      is_var term; assert_fails (unfold term);
      lazymatch f with
      | context [term] => fail
      | _ => idtac
      end;
      let X := type of term in
      assert_fails (assert (@NoLobGen X term) by tc_solve);
      let TT := match type of free_arg with | tele_arg ?TT => TT end in
      let TT1 := open_constr:((_ : tele)) in 
      unify TT (TeleS (λ _: X, TT1)); 
      let tail_arg := open_constr:((_ : TT1)) in
      unify free_arg (TargS' (λ _ : X, TT1) term tail_arg);
      rewrite unfold_tele_app_S /=; 
      discover_other_args
    | _ => 
      let TT := match type of free_arg with | tele_arg ?TT => TT end in
      unify TT TeleO; unify free_arg TargO;
      rewrite (unfold_tele_app_O _ free_arg)
    end
  end.

Global Hint Extern 4 (AsFunOfAmongOthers _ _ _ _) =>
  unfold AsFunOfAmongOthers; cbn;
  strip_known_fun_args;
  discover_other_args;
  find_as_fun_only : typeclass_instances.

Ltac check_vars_occur trm targ :=
  lazymatch targ with
  | tt => fail
  | TeleArgCons ?arg ?rest => 
    lazymatch trm with
    | context [arg] =>
      idtac
    | _ =>
      check_vars_occur trm rest
    end
  end.

Global Hint Extern 4 (VariablesOccurIn ?P ?tt) =>
  let tnorm := eval cbn in tt in
  check_vars_occur P tnorm; 
  exact I : typeclass_instances.


Lemma relevant_pure_from_sig (TT : tele) (tt : TT) (φ : Prop) :
  φ →
  GetRelevantPure tt φ.
Proof. done. Qed.

Fixpoint condense_witness_list (l : list (sig id)) : sig id :=
  match l with
  | [] => exist id True I
  | exist _ P wP :: l' => 
    match condense_witness_list l' with
    | exist _ Q wQ =>
      exist id (P ∧ Q) (conj wP wQ)
    end
  end.

Ltac prove_relevant_pure :=
  lazymatch goal with
  | |- @GetRelevantPure ?TT ?arg _ =>
    let the_arg := eval cbn in arg in
    let rec gather plist :=
      match goal with
      | H : ?Htype |- _ =>
        lazymatch (type of Htype) with
        | Prop => 
          constr:(ltac:(
            do_lob.check_vars_occur Htype the_arg;
            let witness := constr:(exist id Htype H) in
            lazymatch plist with
            | context [witness] => fail
            | _ => 
              let plist' := constr:(cons witness plist) in
              let result := gather plist' in
              exact result
            end
          ))
        end
      | |- _ => plist
      end
    in 
    let result := gather constr:(@nil (sig id)) in
    let result2 := eval cbn in (condense_witness_list result) in
    lazymatch result2 with
    | exist _ ?φ ?wφ =>
      refine (relevant_pure_from_sig TT arg φ wφ)
    end
  end.

Global Hint Extern 4 (GetRelevantPure ?arg _) =>
  prove_relevant_pure : typeclass_instances.


Definition IsMainGoal `(a : A) := True. 
  (* used to introduce a trivial syntactic dependency on the goal, so that IsSectionVar functions as intended *)

Ltac do_filter_non_generalizable :=
  lazymatch goal with
  | |- ReFilterNonGeneralizableVariables ?G ?tt_in ?tt_out ?f =>
    let G' := eval simpl in G in
    change G with G';
    let HG := fresh "HG" in
    assert (IsMainGoal G') as HG by exact I;
    revert HG;
    (* first, detect and generalize on non-variable arguments: they should probably be explicitly kept *)
    let rec do_some_gen tin :=
      lazymatch tin with
      | tt => idtac
      | TeleArgCons ?head ?tail =>
        first 
        [ assert_fails (is_var head) (* if not a variable, generalize over it, then add it as an argument *);
          let v := fresh "v" in 
          set (v := head)
        | idtac ];
        do_some_gen tail
      end
    in
    do_some_gen tt_in; intro HG;
    let rec do_unif tin tout :=
      lazymatch tin with
      | tt => unify tout (tt : tele_arg TeleO)
      | TeleArgCons ?head ?tail =>
        first
        [ assert_fails (assert (NoLobGen head) by tc_solve);
          let TT1 := open_constr:((_ : tele)) in 
          let X := type of head in
          let tail_arg := open_constr:((_ : TT1)) in 
          let trm := constr:((@TeleArgCons X (λ _: X, TT1) head tail_arg) : tele_arg (TeleS (λ _ : X, TT1))) in
          unify tout trm;
          match tt_out with (* tt_out is the actual goal term *)
          | context [TeleArgCons ?actual_head tail_arg] =>  (* tail_arg is actually not restricting, for some reason, so we do a regular match *)
            unify actual_head head; (* this then chooses the correct match *)
            lazymatch actual_head with
            | head => idtac
            | _ => 
                change actual_head with head (* occurs back in the goal, where tt_out occurs *)
            end
          end;
          do_unif tail tail_arg 
        | do_unif tail tout]
      end
    in 
    lazymatch goal with
    | |- ReFilterNonGeneralizableVariables _ ?tt_in' _ _ =>
      do_unif tt_in' tt_out;
      unfold ReFilterNonGeneralizableVariables; simpl;
      find_as_fun_only
    end
  end.


Global Hint Extern 4 (ReFilterNonGeneralizableVariables ?G ?tt_in ?tt_out ?f) =>
  do_filter_non_generalizable : typeclass_instances.



























