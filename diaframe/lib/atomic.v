From iris.bi Require Export bi telescopes lib.atomic.
From iris.proofmode Require Import tactics notation reduction.
From iris.program_logic Require Import weakestpre lifting atomic.
From diaframe Require Import proofmode_base.
From diaframe.lib Require Import greatest_fixpoint.

Import bi.

Set Universe Polymorphism.

Section aupd_autom.
  Context `{BiFUpd PROP}.

  Lemma atomic_update_to_gfp {TA TB : tele} Eo Ei (α : TA → PROP) (β Φ : TA → TB → PROP) :
    run_greatest_fixpoint (λ Ψ, atomic_acc Eo Ei α Ψ β Φ) ⊢ atomic_update Eo Ei α β Φ.
  Proof.
    rewrite run_greatest_fixpoint_eq /run_greatest_fixpoint_def atomic.atomic_update_unseal /atomic.atomic_update_def //=.
  Qed.

  (* this instance is necessary to prevent looping TC search *)
  Global Instance au_later_except_0 {TA TB : tele} Eo Ei (α : TA → PROP) (β Φ : TA → TB → PROP) :
    LaterToExcept0 (atomic_update Eo Ei α β Φ) (▷ atomic_update Eo Ei α β Φ)%I | 10.
  Proof. rewrite /LaterToExcept0 //. eauto. Qed.

  (* an atomic accessor in a shape more accessible to the automation *)
  (* We need to use option2 to get around the universe constraints: option < tele *)
  Definition atomic_acc' {TA TB : tele} Eo Ei (α : TA → PROP) P (β Φ : TA → TB → PROP) : PROP :=
    |={Eo, Ei}=> ∃ x, α x ∗
      (∀ my : option2 (tele_arg TB), 
          ((α x ∧ ⌜my = None2⌝) ∨ (∃.. y, β x y ∧ ⌜my = Some2 y⌝)) 
              ={Ei, Eo}=∗ 
            match my with
            | None2 => P
            | Some2 y => Φ x y
            end).

  Lemma atomic_accs_equiv {TA TB : tele} Eo Ei (α : TA → PROP) P (β Φ : TA → TB → PROP) :
    atomic_acc Eo Ei α P β Φ ⊣⊢ atomic_acc' Eo Ei α P β Φ.
  Proof.
    (* cannot use automation here since operating on abstract telescopes *)
    apply (anti_symm _); rewrite /atomic_acc /atomic_acc'. 
    - iMod 1 as "[%x [Hα Hx]]".
      iExists _. iFrame.
      iIntros "!>" ([y|]) "[[Hα %]|[%y' [Hβ %]]]"; try simplify_eq.
      * iDestruct "Hx" as "[_ Hx]".
        by iApply "Hx".
      * iDestruct "Hx" as "[Hx _]".
        by iApply "Hx".
    - iMod 1 as "[%x [Hα Hx]]".
      iExists _. iFrame.
      iIntros "!>"; iSplit.
      * iIntros "Hα".
        iSpecialize ("Hx" $! None2).
        iApply "Hx". iLeft. by iFrame.
      * iIntros (y) "Hβ".
        iSpecialize ("Hx" $! (Some2 y)).
        iApply "Hx". iRight. iExists _. by iFrame.
  Qed.

  (* when we need to prove an atomic update, we first run the greatest fixpoint *)
  Global Instance abduct_aupd_as_gfp {TA TB : tele} Eo Ei (α : TA → PROP) (β Φ : TA → TB → PROP) :
    HINT1 ε₁ ✱ [run_greatest_fixpoint (λ Ψ, atomic_acc' Eo Ei α Ψ β Φ)] ⊫ [id]; atomic_update Eo Ei α β Φ.
  Proof.
    rewrite /Abduct /= empty_hyp_last_eq left_id. rewrite <-atomic_update_to_gfp.
    rewrite run_greatest_fixpoint_eq /run_greatest_fixpoint_def.
    rewrite {1}greatest_fixpoint_proper //.
    move => R [] /=.
    generalize (R ()) => R' {R}.
    rewrite atomic_accs_equiv //.
  Qed.

  (* after running the fixpoint and introducing it, we proceed as follows: *)
  Global Instance atomic_acc_abd {TA TB : tele} Eo Ei' Ei (α : TA → PROP) P (β Φ : TA → TB → PROP) :
    HINT1 ε₀ ✱ [
        |={Eo, Ei'}=> ∃.. x, α x ∗ (* A neat trick is that we need Ei ⊆ Ei', but we can actually defer that to below! *)
        ((α x ={Ei',Eo}=∗ ⌜Ei ⊆ Ei'⌝ ∧ (emp ={Eo}=∗ P)) ∧ (* the emp -∗ ... forces Diaframe to close the mask first. *)
         (∀.. y : TB, β x y ={Ei',Eo}=∗ ⌜Ei ⊆ Ei'⌝ ∧ (emp ={Eo}=∗ Φ x y)))
      ] ⊫ [id]; atomic_acc' Eo Ei α P β Φ.
  Proof.
    rewrite /Abduct /atomic_acc' /= empty_hyp_first_eq left_id.
    iIntros ">[%x (Hα & Hy)]".
    destruct (decide (Ei ⊆ Ei')).
    - iExists _. iFrame.
      iMod (fupd_mask_subseteq) as "Hcl"; last iIntros "!>". exact s.
      iIntros (my) "[[Hα ->] | [%y [Hβ ->]]]"; iMod "Hcl" as "_".
      * iDestruct "Hy" as "[Hy _]". iMod ("Hy" with "Hα") as "[% HP]". by iApply "HP".
      * iDestruct "Hy" as "[_ Hy]". iMod ("Hy" with "Hβ") as "[% HΦ]". by iApply "HΦ".
    - iDestruct "Hy" as "[Hy _]".
      iMod ("Hy" with "Hα") as "[% HP]". contradiction.
  Qed.

  (* we can access the contents of an atomic update as follows: *)
  Global Instance atomic_update_access p {TA TB : tele} Eo Ei E (α : TA → PROP) (β Φ : TA → TB → PROP) beq :
    (* TODO: at some point we may wish toget rid of this inhabited constraint, but I do not see a nice way of doing that.
      It can probably be done by introducing an extra copy of tele that we force to be lower in the universe hierarchy,
      then having a way to lift it back to the regular telescopes. *)
    ∀ (inh : Inhabited TB),
    SimplTeleEq TB beq → (* like invariants, AtomIntoWand causes problems because the E cannot be found *)
    IntoWand2 p (atomic_update Eo Ei α β Φ) ⌜Eo ⊆ E⌝ (|={E, Ei}=> ∃.. x, α x ∗ ◇ (∀ (b : bool), ∀.. (y : TB), 
        ((β x y ∧ ⌜b = false⌝) ∨ (α x ∧ ⌜b = true⌝) ∧ ⌜tele_app (tele_app beq y) $ inhabitant inh⌝) ={Ei, E}=∗ empty_goal ∗ True ∗ if b then (atomic_update Eo Ei α β Φ) else Φ x y )).
  Proof.
    case => tb /= Hbeq.
    rewrite /IntoWand2 /= bi.intuitionistically_if_elim.
    apply bi.wand_intro_l, bi.wand_elim_l', pure_elim' => Hmask.
    iIntros "Ht >[%x [Hα Hcl]]".
    iExists _. iFrame "Hα".
    iIntros "!> !>" (b y) "[[Hr ->] | [[Hx ->] %]]";
    rewrite empty_goal_eq left_id.
    - iDestruct "Hcl" as "[_ Hcl]".
      iFrame.
      by iMod ("Hcl" with "Hr") as "$".
    - iDestruct "Hcl" as "[Hcl _]".
      iFrame.
      by iMod ("Hcl" with "Hx") as "$".
  Qed.

  (* allow automation to prove AU with AU directly (necessary when proving aborting view shift with an AU in the context *)
  Global Instance aupd_atom p {TA TB : tele} Eo Ei (α : TA → PROP) (β Φ : TA → TB → PROP) :
    AtomAndConnective p (atomic_update Eo Ei α β Φ).
  Proof. done. Qed.

  (* we can access the contents of an atomic update as follows: *)
  Global Instance atomic_update_into_connective {TA TB : tele} Eo Ei (α : TA → PROP) (β Φ : TA → TB → PROP) beq :
    (* TODO: at some point we may wish toget rid of this inhabited constraint, but I do not see a nice way of doing that.
      It can probably be done by introducing an extra copy of tele that we force to be lower in the universe hierarchy,
      then having a way to lift it back to the regular telescopes. *)
    ∀ (inh : Inhabited TB),
    SimplTeleEq TB beq → (* like invariants, AtomIntoWand causes problems because the E cannot be found *)
    AtomIntoConnective (atomic_update Eo Ei α β Φ) (∀ E, ⌜Eo ⊆ E⌝ -∗ |={E, Ei}=> ∃.. x, α x ∗ ◇ (∀ (b : bool), ∀.. (y : TB), 
        ((β x y ∧ ⌜b = false⌝) ∨ (α x ∧ ⌜b = true⌝) ∧ ⌜tele_app (tele_app beq y) $ inhabitant inh⌝) ={Ei, E}=∗ empty_goal ∗ True ∗ if b then (atomic_update Eo Ei α β Φ) else Φ x y ))%I.
  Proof.
    case => tb /= Hbeq.
    rewrite /AtomIntoConnective /=.
    apply bi.forall_intro => E.
    apply bi.wand_intro_l, bi.wand_elim_l', pure_elim' => Hmask.
    iIntros "Ht >[%x [Hα Hcl]]".
    iExists _. iFrame "Hα".
    iIntros "!> !>" (b y) "[[Hr ->] | [[Hx ->] %]]";
    rewrite empty_goal_eq left_id.
    - iDestruct "Hcl" as "[_ Hcl]".
      iFrame.
      by iMod ("Hcl" with "Hr") as "$".
    - iDestruct "Hcl" as "[Hcl _]".
      iFrame.
      by iMod ("Hcl" with "Hx") as "$".
  Qed.


  (* this is useful in various places *)
  Lemma atomic_update_mono {TA TB TB' : tele} Eo Ei (α α' : TA → PROP) (β Φ : TA → TB → PROP) (β' Ψ : TA → TB' → PROP) :
    atomic_update Eo Ei α β Φ -∗ (□ ((∀.. a, (|={Ei}=> α a) ∗-∗ |={Ei}=> α' a) ∧ (∀.. a, 
        ∃ (f: TB' → TB), (∀.. b', β' a b' ={Ei}=∗ β a (f b')) ∧ (∀.. b', Φ a (f b') ={Eo}=∗ Ψ a b')))) -∗ 
    atomic_update Eo Ei α' β' Ψ.
  Proof.
    rewrite atomic.atomic_update_unseal /atomic.atomic_update_def.
    iIntros "HΦ #(Ha & Hf)".
    iApply (greatest_fixpoint_strong_mono with "[] HΦ"); try apply atomic_update_pre_mono.
    iIntros "!>" (F' []).
    rewrite /atomic_update_pre.
    rewrite /atomic_acc.
    iMod 1 as "[%x [Hα Hx]]".
    iExists _.
    iAssert (|={Ei}=> α x)%I with "[Hα]" as "Hα"; first eauto.
    iDestruct ("Ha" with "Hα") as ">$".
    iIntros "!>". iSplit.
    - iIntros "Hα". 
      iDestruct "Hx" as "[Hx _]".
      iApply fupd_trans.
      iApply "Hx".
      iApply "Ha". by iFrame.
    - iIntros (y) "Hβ".
      iDestruct "Hx" as "[_ Hx]".
      iDestruct ("Hf" $! x) as "[%f [HB H]]".
      iApply fupd_trans.
      iApply "H".
      iApply fupd_trans.
      iApply "Hx".
      iApply "HB".
      iApply "Hβ".
  Qed.


  (* These lemmas can be useful to restore Diaframe automation if manually opening AU's. *)
  Lemma diaframe_close_left (α : PROP) au RC Ei Eo :
    (α ={Ei,Eo}=∗ au) ∧ RC ⊢ (α ={Ei,Eo}=∗ χ ∗ au).
  Proof. by rewrite bi.and_elim_l empty_goal_eq left_id. Qed.

  Lemma diaframe_close_right LC Ei Eo (β : PROP) Φ :
    LC ∧ (β ={Ei,Eo}=∗ Φ) ⊢ (β ={Ei,Eo}=∗ χ ∗ Φ).
  Proof. by rewrite bi.and_elim_r empty_goal_eq left_id. Qed.

  Lemma diaframe_close_right_quant LC Ei Eo `(β : A → PROP) Φ :
    LC ∧ (∀ (x : A), β x ={Ei,Eo}=∗ Φ x) ⊢ (∀ (x : A), β x ={Ei,Eo}=∗ χ ∗ Φ x).
  Proof.
    rewrite bi.and_elim_r empty_goal_eq. 
    apply bi.forall_mono => x. by rewrite left_id.
  Qed.

End aupd_autom.

Global Opaque atomic_acc'.

Unset Universe Polymorphism.

