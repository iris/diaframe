From Ltac2 Require Export Ltac2 Printf FSet.
From Coq.micromega Require Import Psatz Zify.
From stdpp Require Import base.

#[export] Set Default Proof Mode "Classic".


(** This file provides a way to detect the newly introduced variables
   by any tactic. 

   This approach does nót work if the tactic itself does something like
     [revert H; move => H]: our tactic will notice that H got removed somewhere
   in the meantime, so will require you to rename it again.

   The [zify] tactic used by [lia] does this. This file overrides
   [Zify.zify_pre_hook] to fix that.
*)

Inductive RecordCurrentVariables : Prop := recorded.

Ltac2 get_idents (u : unit) : ident list :=
  List.map (fun (ident, _, _) => ident) (Control.hyps ()).

Local Ltac2 Notation "red_flags" s(strategy) := s.

Ltac2 cbv_flags : Std.red_flags := (red_flags beta match fix cofix zeta delta).

Ltac2 is_solved_goal (_ : unit) :=
  let cell : bool ref := { contents := false } in
  Control.enter (fun _ => cell.(contents) := true);
  Bool.neg (cell.(contents)).

Ltac2 count_goals (_ : unit) :=
  let cnt : int ref := { contents := 0} in
  Control.enter (fun _ =>
    (* TODO: is this run concurrently? *)
    cnt.(contents) := Int.add (cnt.(contents)) 1
  );
  cnt.(contents).

Ltac2 focus_last (continuation : unit -> 'a) : 'a :=
  let n := count_goals () in
  Control.focus n n continuation.

Ltac2 find_fresh_idents_between (u : unit) : (unit -> ident list) * (unit -> unit) :=
  (** If this function is called, it returns two closures. If the first
     closure is called on a single goal, it will return all the idents that have
     been newly introduced into the Coq context. Once you are done,
     you are required to call the second closure to clean up *)

  (** Unfortunately, doing that is a bit more complicated than looking 
    at the difference between two calls to [Control.hyps ()]. That approach
    will not notice if an ident used in the old context got cleared, and
    a new ident (of possibly different type!) got introduced with the same name.
    To do this, we piggyback on the way evars keep an account of which variables
    were in scope when they got created. *)
  let fresh_evar := (* creates a fresh evar of type [RecordCurrentVariables] *)
    let x := '(_ : RecordCurrentVariables) in
    match Constr.Unsafe.kind x with
    | Constr.Unsafe.Cast c _ _ => 
      c
    | _ => Control.throw Not_found
    end in 

  let checkme := Fresh.in_goal (Option.get (Ident.of_string "checkme")) in
  (* create a _fresh_ name which we will use to reference this evar in our goal *)
  Std.pose (Some checkme) fresh_evar;
  let checkme_constr := (Constr.Unsafe.make (Constr.Unsafe.Var checkme)) in
  (* and 'entangles' it with the current proof goal. This is crucial for 
    getting Coq to notice that it needs to start enforcing constraints.

    We precompute the idents present in the constraints of [checkme] *)
  let (old_idents, raw_evar) :=
    let raw_evar := Std.eval_cbv cbv_flags checkme_constr in
    match Constr.Unsafe.kind raw_evar with
    | Constr.Unsafe.Evar e cs =>
      (Array.to_list (Array.map (fun c =>
        match Constr.Unsafe.kind c with
        | Constr.Unsafe.Var i => i
        | _ => Control.throw Not_found
        end
      ) cs), e)
    | _ => Control.throw Not_found
    end in

  (* In case all goals get solved, we want to prevent giving the user a 'shelved goal'.
     The client should manually check if all goals get solved and call the below cleanup
     closure to get rid of the uninstantiated evar. *)
  let cleanup := (fun _ => 
    Control.new_goal raw_evar; 
    focus_last (fun _ => exact recorded)
  ) in

  (* and return the closure: *)
  ((fun () => 
    (* We need to call [cbv] to get rid of the let-binding, _and_ to 
      refresh the constraints. I am not sure why the constraint refreshing
      only happens after [cbv] *)
    let refreshed_evar := Std.eval_cbv cbv_flags checkme_constr in

    (* We jump through some hoops to get out precisely the constraints
      that are idents ([Constr.Unsafe.Var]). *)
    let new_constrs :=
      match Constr.Unsafe.kind refreshed_evar with
      | Constr.Unsafe.Evar e cs =>
        Array.to_list cs
      | _ => Control.throw Not_found
      end in
    let new_var_constrs_only := List.filter Constr.is_var new_constrs in
    let new_idents_only := List.map (fun c => 
      match Constr.Unsafe.kind c with
      | Constr.Unsafe.Var i => i
      | _ => Control.throw Not_found
      end) new_var_constrs_only in

    (* Then, we check which of those idents were already present in [old_idents].
      These are the idents which have been 'kept' since this closure was generated. *)
    let kept_idents := List.filter (fun i => List.mem Ident.equal i old_idents) new_idents_only in

    (* We now have all the information we need. We clear (but do not unify!) the evar from this goal. *)
    Std.clear [checkme];
    (* We do not unify so that other goals can still read the required information from the evar. *)

    (* We return precisely the idents which are not in [kept_idents] *)
    List.filter (fun i => Bool.neg (List.mem Ident.equal i kept_idents)) (get_idents ())
  ), cleanup).

Ltac2 rec zip (ls1 : 'a list) (ls2 : 'b list) :=
  match ls1 with
  | [] => []
  | a :: ls1 =>
    match ls2 with
    | [] => []
    | b :: ls2 =>
      (a, b) :: zip ls1 ls2
    end
  end.

Ltac2 rec firstn_safe (n : int) (ls : 'a list) :=
  Control.assert_valid_argument "firstn_safe" (Int.ge n 0);
  match Int.equal n 0 with
  | true => []
  | false
    => match ls with
       | [] => []
       | x :: xs
         => x :: firstn_safe (Int.sub n 1) xs
       end
  end.

Ltac2 rec skipn_safe (n : int) (ls : 'a list) :=
  Control.assert_valid_argument "skipn_safe" (Int.ge n 0);
  match Int.equal n 0 with
  | true => ls
  | false
    => match ls with
       | [] => []
       | x :: xs
         => skipn_safe (Int.sub n 1) xs
       end
  end.

Ltac2 find_dependent_on_ident (i : ident) : ident :=
  multi_match! goal with
  | [ hyp : context [?i_constr] |- _] =>
    Std.unify i_constr (Control.hyp i);
    hyp
  end.

Ltac2 Type exn ::= [MoreValues].

Ltac2 Type 'a set := { mutable set_as_fset : 'a FSet.t }.

Ltac2 empty_set (set_tag : 'a Tags.tag) := { set_as_fset := FSet.empty set_tag}.

Ltac2 set_add (a_set : 'a set) (el : 'a) : unit :=
  if FSet.mem el (a_set.(set_as_fset)) then
    ()
  else
    a_set.(set_as_fset) := FSet.add el (a_set.(set_as_fset)).

Ltac2 set_remove (a_set : 'a set) (el : 'a) : unit :=
  a_set.(set_as_fset) := FSet.remove el (a_set.(set_as_fset)).

Ltac2 set_from_list (els : 'a list) (set_tag : 'a Tags.tag) :=
  let result : 'a set := empty_set set_tag in
  List.iter (set_add result) els;
  result.

Ltac2 rec get_all (closure : unit -> 'a) : 'a list :=
  match Control.case closure with
  | Val pr =>
    let (hd, tailgen) := pr in
    hd :: get_all (fun _ => tailgen MoreValues)
  | Err exn =>
    []
  end.

Ltac2 add_all_to_set (closure : unit -> 'a) (result : 'a set) : unit :=
  let rec get_more (c : unit -> 'a) : unit :=
    match Control.case c with
    | Val pr =>
      let (hd, tailgen) := pr in
      set_add result hd;
      get_more (fun _ => tailgen MoreValues)
    | Err exn =>
      ()
    end in
  get_more closure.

Ltac2 find_all_dependent_on_ident (i : ident) : ident set :=
  let result := empty_set Tags.ident_tag in
  add_all_to_set (fun _ => find_dependent_on_ident i) result;
  result.

Ltac2 find_all_dependent_on_idents (is : ident list) : ident list :=
  let result := empty_set Tags.ident_tag in
  List.iter (fun i => add_all_to_set (fun _ => find_dependent_on_ident i) result) is;
  List.iter (fun i => set_remove result i) is;
  FSet.elements (result.(set_as_fset)).


Ltac2 to_list_of_size 
  (els : 'a list) (size : int) (default : 'a)
  (too_short_trig : int -> unit) (too_long_trig : int -> unit) : 'a list :=
  let els_len := List.length els in
  if Int.lt els_len size then
    too_short_trig (Int.sub size els_len);
    List.append els (List.repeat default (Int.sub size els_len))
  else
    if Int.gt els_len size then
      (* silently discard stuff if no goals remain. *)
      if Int.equal size 0 then () else too_long_trig (Int.sub els_len size);
      List.firstn size els
    else
      els.

Ltac2 Type rename_error_verbosity := [ SilenceRenameErrors | VerboseRenameErrors ].

Ltac2 Type NamingProblems := {
  mutable global_problems : (unit -> unit) option;
  mutable subgoal_problems : (unit -> unit) option list;
}.

Ltac2 maybe_closure_append (omsg1 : (unit -> unit) option) (msg2 : unit -> unit) : unit -> unit :=
  match omsg1 with
  | None => msg2
  | Some m => (fun _ => m (); msg2 ())
  end.

Ltac2 add_global_problem (prob : NamingProblems) (msg : unit -> unit) : unit :=
  prob.(global_problems) := Some (
    maybe_closure_append (prob.(global_problems)) msg
  ).

Ltac2 add_subgoal_problem (prob : NamingProblems) (i : int) (msg : unit -> unit) : unit :=
  prob.(subgoal_problems) := List.mapi (fun j om => 
    if Int.equal j i then
      Some (maybe_closure_append om msg)
    else
      om
  ) (prob.(subgoal_problems)).


Ltac2 reintroduce_with_pats (register_closure : (unit -> unit) -> unit) 
    (idents : ident list) (pats : Ltac1.t list) : unit :=
  let pat_len := List.length pats in
  let ident_len := List.length idents in
  let too_many_pats := Int.gt pat_len ident_len in
  let too_little_pats := Int.lt pat_len ident_len in
  let trunc_pat_list := (* cant use [to_list_of_size] since the introduction is staggered *)
    match too_many_pats with
    | true =>
      register_closure (fun _ => printf "Too many intro patterns were supplied!");
      register_closure (fun _ =>
        ltac1:(pats |- idtac "Dropping the following intro patterns:  " pats)
          (Ltac1.of_list (List.skipn ident_len pats))
      );
      firstn_safe (List.length idents) pats
    | false => pats
    end in

  let extra_reverts : ident list option ref := { contents := None } in
  Control.once (fun _ => 
    Control.plus (fun _ => Std.revert idents) (fun _ =>
      (* revert has failed, because of dependent hypothesis. 
        We compute all the dependent hyps, revert them for now, then reintroduce them later *)
      let i_deps := find_all_dependent_on_idents idents in
      extra_reverts.(contents) := Some i_deps ;
      Std.revert i_deps;
      Std.revert idents
    )
  );
  ltac1:(xs |- intros xs) (Ltac1.of_list trunc_pat_list);
  (* one might want to avoid [revert; intros] for a prettier proof term, 
     but [rename || destruct] does not work with i.e. intropattern [_] *)

  match too_little_pats with
  | false => ()
  | true =>
    let run_intros (_ : unit) := 
      (* We need to use [IntroFresh] here, since the provided patterns could conflict with the old names. *)
      Std.intros false (List.map (fun i => Std.IntroNaming (Std.IntroFresh i))
                       (List.skipn (List.length trunc_pat_list) idents))
    in
    register_closure (fun _ => printf "Too little intro patterns were supplied! Please supply a name for:");
    (* To obtain and print the actual names, we reuse [find_fresh_idents_between] *)
    let (f, cleanup) := find_fresh_idents_between () in
    run_intros ();
    let new_idents := f () in
    cleanup ();
    List.iter (fun i =>
      (* fprintf to print now, while focused, so that the closure still works when unfocused *)
      let msg := fprintf "  %I : %t" i (Constr.type (Control.hyp i)) in
      register_closure (fun _ => Message.print msg)
    ) new_idents
  end;

  (* Introduce the extra reverts, if there were any *)
  match extra_reverts.(contents) with
  | None => ()
  | Some is =>
    Std.intros false (List.map (fun i => Std.IntroNaming (Std.IntroIdentifier i)) is)
  end.

(* A weird interaction between the [unshelve] tactical and [clear] can
   result in pushing the [RecordCurrentVariables] evar to the current goal.
   This tactic will shelve all goals of that type *)
Ltac2 shelve_record_current_variables_goals (_ : unit) :=
  Control.enter (fun _ =>
    lazy_match! goal with
    | [ |- RecordCurrentVariables] => Control.shelve ()
    | [ |- _] => ()
    end
  ).

Ltac2 initialize_subgoal_problems (prob : NamingProblems) :=
  match prob.(subgoal_problems) with
  | [] =>
    prob.(subgoal_problems) := List.repeat None (count_goals ())
  | _ => ()
  end.

Ltac2 rename_multi_after_closure (prob : NamingProblems)
  (raw_multi_intro_pats : Ltac1.t)
  (closure : unit -> unit) :=
  let (f, cleanup) := find_fresh_idents_between () in
  closure ();
  shelve_record_current_variables_goals ();
  initialize_subgoal_problems prob;
  let raw_pat_list_list := Option.get (Ltac1.to_list raw_multi_intro_pats) in
  let pat_list_to_closure := (fun i pat_list =>
    (fun _ =>
      let new_idents := f () in
      let raw_pat_list := Option.get (Ltac1.to_list pat_list) in
      reintroduce_with_pats (add_subgoal_problem prob i) new_idents raw_pat_list
    )
  ) in
  let pat_list_list := to_list_of_size raw_pat_list_list (count_goals ()) (Ltac1.of_list [])
      (fun i => add_global_problem prob (fun _ => 
        printf "Too little branches of variable names supplied! Missing %i branches" i 
      ))
      (fun i => add_global_problem prob (fun _ =>
        printf "Too many branches of variable names supplied! Dropping %i branches" i
      ))
  in
  Control.dispatch (List.mapi pat_list_to_closure pat_list_list);
  cleanup ().

Ltac2 print_problems (prob : NamingProblems) : unit :=
  match prob.(global_problems) with
  | None => ()
  | Some c => c (); printf ""
  end;
  List.iteri (fun i mc =>
    match mc with
    | None => ()
    | Some c => 
      if Int.gt (List.length (prob.(subgoal_problems))) 1 then
        printf "Subgoal %i naming problems:" (Int.add i 1);
        c ();
        printf ""
      else c ()
    end
  ) (prob.(subgoal_problems)).

Ltac2 no_problems (_ : unit) : NamingProblems := {
  global_problems := None;
  subgoal_problems := [];
}.

Ltac zify_pre_hook ::=
  lazymatch goal with
  | f : utils_ltac2.RecordCurrentVariables |- _ =>
    clear f
  | |- _ => idtac
  end.


(** Control.plus will cause later failures to backtrack on this choice.
   If you do not want that, use plus_once .*)
Ltac2 plus_once f g :=
  Control.once (fun _ =>
    Control.plus f g
  ).

Ltac2 rec plus_once_list0 (ts : (unit -> 'a) list) : 'a :=
  match ts with
  | [] => Control.zero Assertion_failure
  | t :: ts' =>
    plus_once t (fun _ => plus_once_list0 ts')
  end.

(* Adapted from 'first' *)
Ltac2 Notation "plus_once_list" "[" tacs(list0(thunk(tactic(6)), "|")) "]" := plus_once_list0 tacs.


Ltac2 assert_fails (thunk : unit -> unit) :=
  let wrapped_thunk c :=
    plus_once (fun _ => thunk ();
      ltac1val:(idtac))
    (fun _ => ltac1val:(gfail))
  in
  ltac1:(arg |- assert_fails ltac:(idtac; arg ())) (Ltac1.lambda wrapped_thunk).

Ltac2 assert_succeeds (thunk : unit -> unit) :=
  let wrapped_thunk c :=
    plus_once (fun _ => thunk ();
      ltac1val:(idtac))
    (fun _ => ltac1val:(gfail))
  in
  ltac1:(arg |- assert_succeeds ltac:(idtac; arg ())) (Ltac1.lambda wrapped_thunk).


(** Without additional work, it is impossible to receive Ltac constructed values
  back in Ltac2. But it can be done, with below. [ltac1_get_set_pair ()] returns
  an [Ltac1.t] 'setter' tactic, which will save the argument it gets passed (in Ltac1).
  After setting, the 'getter' can be used to retrieve the set value *)
Ltac2 ltac1_get_set_pair (u : unit) : 
  (* setter * getter *)
    Ltac1.t * (unit -> constr) :=
  let r := { contents := None } in
  let setter ltac1result := 
    let () := match Ltac1.to_constr ltac1result with
    | None => ()
    | Some c => 
      r.(contents) := Some c
    end in
    (* below is the returned tactic after application. 
      It is not run, since we supply it as argument to [(fun _ => ())] *)
    ltac1val:(idtac "continuation; should not be run")
  in
  let getter (_ : unit) :=
     match r.(contents) with
    | None => 
      Message.print (Message.of_string "container was not set?");
      Control.throw Not_found
    | Some c => 
      c
    end in
  (Ltac1.lambda setter, getter).


(**  [read_ltac1_constr constr1gen arg] passes [arg : constr] to 
  the Ltac1 value constructing tactic [constr1gen], and returns the 
  result as an Ltac2 constr. Also backtracks on failure of the Ltac1 tactic *)
Ltac2 read_ltac1_constr (constr1gen : Ltac1.t) (arg : constr) : constr :=
  let (setter, getter) := ltac1_get_set_pair () in
  let tac := ltac1val:(fun inputarg inputtac settertac => 
      let output := inputtac inputarg in 
      settertac output
  ) in
  let () := Ltac1.apply tac [Ltac1.of_constr arg; constr1gen; setter] 
      (fun _ => ()) in
  getter ().

(** non-hard failing version of unfold, remove once https://github.com/coq/coq/issues/14286 is fixed *)
Ltac2 unfold_const (theconst : constr) : constr :=
  read_ltac1_constr ltac1val:(fun a => let u := eval unfold a in a in u) theconst.


(**  [read_ltac1_constr2 constr1gen arg] passes [arg1, arg2 : constr] to 
  the Ltac1 value constructing tactic [constr1gen], and returns the 
  result as an Ltac2 constr. Also backtracks on failure of the Ltac1 tactic *)
Ltac2 read_ltac1_constr2 (constr1gen : Ltac1.t) (arg1 : constr) (arg2 : constr) : constr :=
  let (setter, getter) := ltac1_get_set_pair () in
  let tac := ltac1val:(fun inputarg1 inputarg2 inputtac settertac => 
      let output := inputtac inputarg1 inputarg2 in 
      settertac output
  ) in
  let () := Ltac1.apply tac [Ltac1.of_constr arg1; Ltac1.of_constr arg2; constr1gen; setter] 
      (fun _ => ()) in
  getter ().

(** non-hard failing version of unfold, remove once https://github.com/coq/coq/issues/14286 is fixed *)
Ltac2 unfold_in (to_unfold : constr) (target : constr) : constr :=
  read_ltac1_constr2 ltac1val:(fun a b => let u := eval unfold a in b in u) to_unfold target.

Ltac2 check_tc_convertible (c1 : constr) (c2 : constr) : unit :=
  assert_succeeds (fun _ =>
    assert (TCEq $c1 $c2) as _  by ltac1:(autoapply @TCEq_refl with typeclass_instances)
  ).

Ltac2 tc_unfold_in (to_unfold : constr) (target : constr) : constr :=
  let result := unfold_in to_unfold target in
  check_tc_convertible to_unfold (unfold_const to_unfold);
  result.

Ltac2 tc_unfold_const (theconst : constr) : constr :=
  let result := unfold_const theconst in
  check_tc_convertible theconst result;
  result.

Ltac2 tc_solve (u : unit) := ltac1:(tc_solve).


Ltac2 as_ltac1_thunk (thunk : unit -> unit) : Ltac1.t :=
  let cont := fun _ => thunk (); ltac1val:(idtac) in
  Ltac1.lambda cont.


Ltac2 Type exn ::= [ Ltac1_result (Ltac1.t) ].

Ltac ltac2_return arg :=
  let f := ltac2:(arg' |-
    Control.zero (Ltac1_result arg')
  ) in
  f arg.

Ltac2 catch_ltac1 (return_tac1 : Ltac1.t) (om : message option) : exn :=
  let thunk : unit -> unit := (fun () => Ltac1.run return_tac1) in
  match (Control.case thunk) with
  | Val _ =>
    Control.throw (Invalid_argument om)
  | Err e => e
  end.

Ltac2 receive_ltac1 (return_tac1 : Ltac1.t) : Ltac1.t :=
  match (catch_ltac1 return_tac1 None) with
  | Ltac1_result result => result
  | _ =>
    Control.throw Assertion_failure
  end.

(* Above can be used as follows:
Goal True.
ltac2:(
  let c_raw := receive_ltac1 ltac1val:(
    let cont := ltac:(idtac "hallo daar") in
    ltac2_return cont
  ) in
  printf "received";
  Ltac1.run c_raw
). *)


(** Coq has been changing the argument order on these...
  https://github.com/coq/coq/pull/18197
  FIXME: >= 8.19, replace this with just List.fold_∗
*)
Ltac2 rec fold_right (f : 'a -> 'b -> 'b) (ls : 'a list) (a : 'b) : 'b :=
  match ls with
  | [] => a
  | l :: ls => f l (fold_right f ls a)
  end.

Ltac2 rec fold_left (f : 'a -> 'b -> 'a) (a : 'a) (xs : 'b list) : 'a :=
  match xs with
  | [] => a
  | x :: xs => fold_left f (f a x) xs
  end.

Ltac2 rec fold_right2 (f : 'a -> 'b -> 'c -> 'c) (ls1 : 'a list) (ls2 : 'b list) (a : 'c) :=
  match ls1 with
  | []
    => match ls2 with
       | [] => a
       | _ :: _ => Control.throw_invalid_argument "List.fold_right2"
       end
  | l1 :: ls1
    => match ls2 with
       | [] => Control.throw_invalid_argument "List.fold_right2"
       | l2 :: ls2
         => f l1 l2 (fold_right2 f ls1 ls2 a)
       end
  end.


Ltac2 rec of_coq_list (ls : constr) : constr list :=
  lazy_match! ls with
  | [] => []
  | ?l :: ?ls => l :: of_coq_list ls
  end.


(* [get_head_term_n_args] returns the head symbol and arguments of provided [p : constr],
even if [p] is a lambda-term. Useful for matching beneath binders.
Also returns the list of binders under which was recursed *)
Ltac2 rec get_head_term_n_args (p : constr) : constr * constr list * binder list :=
  match Constr.Unsafe.kind p with
  | Constr.Unsafe.Lambda b p' =>
    let (h, args_i, bs) := (get_head_term_n_args p') in
    (h, args_i, b :: bs)
  | Constr.Unsafe.App p' args =>
    let (h, args_i, bs) := (get_head_term_n_args p') in
    (h, List.append args_i (Array.to_list args), bs)
  | Constr.Unsafe.Cast p' _ _ => get_head_term_n_args p'
    (* we consider projections as a complete head_term, since [bi_..] are projections *)
  | _ => p, [], []
  end.

(* Rebind allows us to construct safe constrs again *)
Ltac2 rec rebind (c : constr) (bs : binder list) : constr :=
  match bs with
  | [] => c
  | b :: bs =>
    Constr.Unsafe.make
      (Constr.Unsafe.Lambda b (rebind c bs))
  end.

