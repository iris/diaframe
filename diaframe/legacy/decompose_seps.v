From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import env_utils util_classes.

Section decompose_lemmas.
  Context {PROP : bi}.
  Implicit Types P Q : PROP.

  Lemma into_sep_compat1 P Q1 Q2 : IntoSepCareful P Q1 Q2 → IntoSep P Q1 Q2.
  Proof. auto. Qed.

  Lemma into_sep_compat2 P Q1 Q2 : IntoSep P Q1 Q2 → IntoSepCareful P Q1 Q2.
  Proof. auto. Qed.

  Lemma tac_and_destruct' (Δ : envs PROP) i p j1 j2 P P1 P2 Q :
    envs_lookup i Δ = Some (p, P)
    → (if p then IntoAnd true P P1 P2 else IntoSepCareful P P1 P2)
      → match envs_simple_replace i p (Esnoc (Esnoc Enil j1 P1) j2 P2) Δ with
        | Some Δ' => envs_entails Δ' Q
        | None => False
        end → envs_entails Δ Q.
  Proof.
  destruct p => H1 H2.
  - by eapply coq_tactics.tac_and_destruct.
  - by eapply coq_tactics.tac_and_destruct, into_sep_compat1.
  Qed.

End decompose_lemmas.

Section ident_manipulation.

  Definition concat_rev (k : list Ascii.ascii) : string := foldl (λ a c, String c a) "" k.

  Fixpoint to_ident_list_rec (k : string) (res: list ident) (cur : list Ascii.ascii) :=
    match k return (list ident) with
    | "" => 
        match cur with 
        | [] => res 
        | cur => (INamed (concat_rev cur))::res
        end
    | String a k => 
        if is_space a then 
          match cur with 
          | [] => to_ident_list_rec k res cur 
          | cur' => to_ident_list_rec k ((INamed (concat_rev cur'))::res) [] 
          end
        else
          to_ident_list_rec k res (a::cur)
    end.

  Definition to_ident_list (k : string) := rev (to_ident_list_rec k [] []).

End ident_manipulation.

(** The iDecomposeSeps tactic tries to detect all hypotheses of the form 'H : P ∗ Q', and (recursively) 
    decompose them to 'H1 : P, H2 : Q'. Note that we have appended numbers to the original hypothesis name.
    We need something to generate such a fresh name, which is the following Ltac. *)

Ltac get_fresh_name Δ base' append :=
  let rec fresh_name base :=
    let name_in_env := eval cbv in (existsb (fun i => ident_beq i base) (envs_dom Δ)) in
    match constr:(name_in_env) with
    | true => 
      let new_name := eval cbv in (base +:+ append) in
      fresh_name new_name
    | false => base
    end 
  in fresh_name base'.

(* Using get_fresh_name, we can build an iDestructFresh tactic, which will decompose the hypothesis of
   the given name, without needing to be supplied names for its destructees. *)

Tactic Notation "iDestructFresh" constr(i) constr(H) :=
  lazymatch goal with
  | |- envs_entails ?Δ _ =>
    eapply tac_and_destruct' with i _ _ _ _ _ _; (* (i:=H) (j1:=H1) (j2:=H2) *)
      [pm_reflexivity ||
       let H := pretty_ident H in
       fail "iDestructFresh:" H "not found"
      |pm_reduce; tc_solve ||
       let P :=
         lazymatch goal with
         | |- IntoSepCareful ?P _ _ => P
         | |- IntoAnd _ ?P _ _ => P
         end in
       fail "iDestructFresh: cannot destruct" P
      | lazymatch goal with
        | |- (match envs_simple_replace i _ (Esnoc (Esnoc Enil ?j1 _) ?j2 _) Δ with
              | Some Δ' => envs_entails Δ' _
              | None => False
              end) => 
                let H1 := get_fresh_name Δ H "1" in
                let H2 := get_fresh_name Δ H "2" in
                unify j1 (INamed H1); unify j2 (INamed H2); pm_reduce;
              lazymatch goal with
              | |- False => fail "iDestructFresh: generating fresh names failed! get_fresh_name_tactic is faulty."
              | |- _ => idtac
              end
        | |- _ => fail "iDestructFresh: third goal of tac_and_destruct not recognizable"
       end]
  | |- _ => fail "iDestructFresh: goal is not a IPM entailment."
  end.

(* The way to build iDecomposeSeps is now clear: we simply call try iDestructFresh on every hypothesis 
   in the current environment. *)

Ltac decompose_sep_tac idents :=
    match idents with
    | [] => idtac
    | ?H :: ?idents => 
      let H' := eval cbv in
        (match H with
        | INamed H' => H'
        | IAnon _ => "_Hyp_"
        end) in
      try (iDestructFresh H H'); decompose_sep_tac idents
    end.

Tactic Notation "iDecomposeSeps" :=
  lazymatch goal with
  | |- envs_entails ?Δ _ =>
    let ident_list := eval cbv in (envs_dom Δ) in
    decompose_sep_tac ident_list
  end.













