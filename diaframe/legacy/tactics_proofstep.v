From iris.program_logic Require Import ectx_language.
From iris.proofmode Require Import base classes environments reduction tactics.
From diaframe Require Import proofstep. 

(** The iStep tac, offering typeclass based automation for IPM proofs. *)

Ltac substitute_tele_evar TT witness :=
  let rec mk_tele_evar ts ta :=
    lazymatch ts with
    | [tele] => unify ta [tele_arg]
    | @TeleS ?Y ?binder => 
      let y := open_constr:(_ : Y) in
      let fy := open_constr:(_ : binder y) in
      unify ta (TargS y fy);
      let new_ts := eval cbn in (binder y) in
      mk_tele_evar new_ts fy
    end in
  mk_tele_evar TT witness.

Ltac solve_lookup fail_tac :=
  let rec solve_lookup_internal _ :=
    lazymatch goal with
    | |- envs_lookup_list _ ?x _ = Some (_, []) => apply envs_lookup_list_nil
    | |- envs_lookup_list _ ?x _ = Some (_, ?P :: _) =>
       eapply envs_lookup_list_cons;
         [iAssumptionCore || fail_tac P
         |solve_lookup_internal ()]
    end in
  lazymatch goal with
  | |- _ = Some (_, @tele_app ?TT _ ?tfun ?witness) =>
    substitute_tele_evar TT witness; cbn;
    solve_lookup_internal ()
  | |- _ => solve_lookup_internal ()
  end.

Ltac iStep_tac xs ks :=
  iStartProof;
  notypeclasses refine (tac_proof_step_emp _ _ _ ks _ _ _ _ _ _ _ _ _ _ _ _);
    [tc_solve (* ProofStep *)
    |tc_solve (* MaybeIntoLaterNEnvs *)
    |solve_lookup ltac:(fun P => fail "iStep: cannot find hypothesis" P) (* env_lookup_list *)
    |try (by auto) (* pure requirement φ *)
    |cbn [ tforall tele_fold tele_bind tele_app tele_map fill foldl]; try (intros xs); intros;
     eexists _; split; 
      [reflexivity || fail "iStep: Some identifiers " ks " are present in proof context" 
      | pm_reduce; cbn [envs_delete_list bi_laterN snd]; iSimpl] ].

Ltac iStep_with_hyp_tac k xs ks1 ks2 :=
  iStartProof;
  iDecomposeSeps;
  notypeclasses refine (tac_proof_step_from_env _ _ _ k _ _ _ ks1 _ _ _ _ _ _ _ _ _ _ _ _ _);
    [reflexivity || fail "iStep: could not find " k " in the context" (* env_lookup *)
    |tc_solve
      || fail "iStep: Failed to convert " k " to a ProofStep instance." (* ProofStep' *)
    |tc_solve (* MaybeIntoLaterNEnvs *)
    |solve_lookup ltac:(fun P => fail "iStep: cannot find hypothesis" P) (* env_lookup_list *)
    |try (by auto) (* pure requirement φ *)
    |cbn [ tforall tele_fold tele_bind tele_app tele_map fill foldl]; try (intros xs); intros;
      eexists _; split; 
       [reflexivity || fail "iStep: Some identifiers " ks1 " are present in proof context" 
       | pm_reduce; cbn [envs_delete_list bi_laterN snd]; 
        iSimpl; try (rewrite !(right_id emp%I)); try (rewrite !(left_id emp%I));
        iFrame "∗ #"; repeat (iIntros ( ? )); iIntros ks2
        ] ].

Ltac iStep_with_result_tac result xs ks1 ks2 :=
  iStartProof;
  iDecomposeSeps;  
  notypeclasses refine (tac_proof_step_from_result _ _ _ _ ks1 _ _ _ _ _ _ _ _ _ _ _ _ _);
    [by (eapply result || eapply bi.entails_wand, result) 
      || fail "iStep: could not convert " result " to a separation logic result." (* ⊢ Q *)
    |tc_solve
      || fail "iStep: Failed to convert " result " to a ProofStep instance." (* ProofStep' *)
    |tc_solve (* MaybeIntoLaterNEnvs *)
    |solve_lookup ltac:(fun P => fail "iStep: cannot find hypothesis" P) (* env_lookup_list *)
    |try (by auto) (* pure requirement φ *)
    |cbn [ tforall tele_fold tele_bind tele_app tele_map fill foldl]; try (intros xs); intros;
      eexists _; split; 
       [reflexivity || fail "iStep: Some identifiers " ks1 " are present in proof context" 
       | pm_reduce; cbn [envs_delete_list bi_laterN snd]; 
        iSimpl; try (rewrite !(right_id emp%I)); try (rewrite !(left_id emp%I));
        iFrame "∗ # %"; repeat (iIntros ( ? )); iIntros ks2
        ] ].

Ltac iStep_with_tac par xs ks1 ks2 :=
  lazymatch type of par with
  | string => iStep_with_hyp_tac par xs ks1 ks2
  | _ => iStep_with_result_tac par xs ks1 ks2 || fail "Could not coerce " par " to a separation logic result."
  end.

Tactic Notation "iStep" "as" "(" simple_intropattern_list(xs) ")" constr(k) :=
  let ks := constr:(to_ident_list k) in let ks' := eval cbn in ks in iStep_tac xs ks'.
Tactic Notation "iStep" "as" "(" simple_intropattern_list(xs) ")" := iStep as ( xs ) "".
Tactic Notation "iStep" "as" constr(k) := iStep as ( ) k.
Tactic Notation "iStep" := iStep as "".
Tactic Notation "iSteps" := repeat iStep.

Tactic Notation "iStep" "as" "(" simple_intropattern_list(xs) ")" constr(k1) "," constr(k2) "with" constr(j) :=
  let ks1 := constr:(to_ident_list k1) in let ks1' := eval cbn in ks1 in 
  iStep_with_tac j xs ks1' k2.
Tactic Notation "iStep" "as" "(" simple_intropattern_list(xs) ")" constr(k2) "with" constr(j) := 
  iStep_with_tac j xs (@nil ident) k2.
Tactic Notation "iStep" "as" "(" simple_intropattern_list(xs) ")" "with" constr(j) := iStep as ( xs ) "" with j.
Tactic Notation "iStep" "as" constr(k) "with" constr (j) := iStep as ( ) k with j.
Tactic Notation "iStep" "with" constr(j) := iStep as "" with j.

Tactic Notation "iAssumptionStrongCore" :=
  let rec find Γ i p P :=
    lazymatch Γ with
    | Esnoc ?Γ ?j ?Q => first [pose proof (_ : FromAssumptionIStep p Q P) as Hass; unify i j|find Γ i p P]
    end in
  match goal with
  | |- envs_lookup ?i (Envs ?Γp ?Γs _) = Some (_, ?P) ∧ FromAssumptionIStep _ ?P ?P' =>
     first [is_evar i; fail 1 | split; [pm_reflexivity|tc_solve] ]
  | |- envs_lookup ?i (Envs ?Γp ?Γs _) = Some (_, ?P) ∧ FromAssumptionIStep _ ?P ?P' =>
     is_evar i; first [find Γp i true P' | find Γs i false P']; split; [pm_reflexivity|done]
  | |- envs_lookup_delete _ ?i (Envs ?Γp ?Γs _) = Some (_, ?P, _) ∧ FromAssumptionIStep _ ?P ?P' =>
     first [is_evar i; fail 1 | split; [pm_reflexivity|tc_solve]]
  | |- envs_lookup_delete _ ?i (Envs ?Γp ?Γs _) = Some (_, ?P, _) ∧ FromAssumptionIStep _ ?P ?P'=>
     is_evar i; first [find Γp i true P' | find Γs i false P']; split; [pm_reflexivity|done]
  end.

Ltac solve_lookup_asmp fail_tac :=
  let rec solve_lookup_internal _ :=
    lazymatch goal with
    | |- envs_lookup_list _ ?x _ = Some (?ps, ?Ps) ∧ FromAssumptions ?ps ?Ps [] => apply envs_lookup_list_fromasmp_nil
    | |- envs_lookup_list _ ?x _ = Some (?ps, ?Ps) ∧ FromAssumptions ?ps ?Ps (?P :: _) =>
       eapply envs_lookup_list_fromasmp_cons;
         [iAssumptionStrongCore || fail_tac P
         |solve_lookup_internal ()]
    end in
  lazymatch goal with
  | |- _ ∧ FromAssumptions _ _ (@tele_app ?TT _ ?tfun ?witness) =>
    substitute_tele_evar TT witness; cbn;
    solve_lookup_internal ()
  | |- _ => solve_lookup_internal ()
  end.

Ltac iStepA_tac xs ks :=
  iStartProof;
  notypeclasses refine (tac_proof_step_asmp_emp _ _ _ ks _ _ _ _ _ _ _ _ _ _ _ _ _);
    [tc_solve (* ProofStep *)
    |tc_solve (* MaybeIntoLaterNEnvs *)
    |solve_lookup_asmp ltac:(fun P => fail "iStep: cannot find hypothesis" P) (* env_lookup_list *)
    |try (by auto) (* pure requirement φ *)
    |cbn [ tforall tele_fold tele_bind tele_app tele_map fill foldl]; try (intros xs); intros;
     eexists _; split; 
      [reflexivity || fail "iStep: Some identifiers " ks " are present in proof context" 
      | pm_reduce; cbn [envs_delete_list bi_laterN snd]; iSimpl] ].

Tactic Notation "iStepA" "as" "(" simple_intropattern_list(xs) ")" constr(k) :=
  let ks := constr:(to_ident_list k) in let ks' := eval cbn in ks in iStepA_tac xs ks'.
Tactic Notation "iStepA" "as" "(" simple_intropattern_list(xs) ")" := iStepA as ( xs ) "".
Tactic Notation "iStepA" "as" constr(k) := iStepA as ( ) k.
Tactic Notation "iStepA" := iStepA as "".
Tactic Notation "iStepsA" := repeat iStepA.




