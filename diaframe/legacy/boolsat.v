From stdpp Require Import base pmap.


Section bool_expr_mixin.
  Context (expr_car : Type).
  Context (negate : expr_car → expr_car).
  Context (conjunct : expr_car → expr_car → expr_car).
  Context (as_bool : expr_car → Pmap bool → bool).
  Context (subs : expr_car → positive → expr_car → expr_car).
  Context (make_false_by : expr_car → positive → option expr_car).
  Context (is_sat : expr_car → bool).
  Context (occurs : expr_car → positive → bool).

  Record BoolExprMixin := {
    bexpr_insert_subs e1 i e2 M : as_bool e1 (insert i (as_bool e2 M) M) = as_bool (subs e1 i e2) M;
    bexpr_make_false_by_valid e1 i e2 M : 
      make_false_by e1 i = Some e2 →
      as_bool e1 (insert i (as_bool e2 M) M) = false;
    bexpr_negate_valid e1 M : as_bool (negate e1) M = negb $ as_bool e1 M;
    bexpr_conjunct_valid e1 e2 M : as_bool (conjunct e1 e2) M = andb (as_bool e1 M) (as_bool e2 M);
    bexpr_is_sat_valid e : is_sat e = true → ∃ M, as_bool e M = true;
    bexpr_occurs_valid e i m v : occurs e i = false → as_bool e m = as_bool e (<[i:=v]> m);
  }.
End bool_expr_mixin.


Structure bool_expr := BoolExpr {
  expr_car :> Type;
  negate : expr_car → expr_car;
  conjunct : expr_car → expr_car → expr_car;
  as_bool : expr_car → Pmap bool → bool;
  subs : expr_car → positive → expr_car → expr_car;
  make_false_by : expr_car → positive → option expr_car;
  is_sat : expr_car → bool;
  occurs : expr_car → positive → bool;
  bool_expr_mixin : BoolExprMixin expr_car negate conjunct as_bool subs make_false_by is_sat occurs;
}.


Arguments negate {β} _ : rename.
Arguments conjunct {β} _ _ : rename.
Arguments as_bool {β} _ _ : simpl never, rename.
Arguments subs {β} _ _%positive _ : rename.
Arguments make_false_by {β} _ _ : rename.
Arguments is_sat {β} _ : rename.
Arguments occurs {β} _ : rename.


Section bool_expr_laws.
  Context (β : bool_expr).
  Implicit Types (e : β).
  Implicit Types (i : positive).
  Implicit Types (M : Pmap bool).

  Lemma insert_subs e1 i e2 M : as_bool e1 (insert i (as_bool e2 M) M) = as_bool (subs e1 i e2) M.
  Proof. eapply bexpr_insert_subs, bool_expr_mixin. Qed.

  Lemma make_false_by_valid e1 i e2 M :
    make_false_by e1 i = Some e2 →
    as_bool e1 (insert i (as_bool e2 M) M) = false.
  Proof. eapply bexpr_make_false_by_valid, bool_expr_mixin. Qed.

  Lemma negate_valid e M : as_bool (negate e) M = negb $ as_bool e M.
  Proof. eapply bexpr_negate_valid, bool_expr_mixin. Qed.

  Lemma conjunct_valid e1 e2 M : as_bool (conjunct e1 e2) M = andb (as_bool e1 M) (as_bool e2 M).
  Proof. eapply bexpr_conjunct_valid, bool_expr_mixin. Qed.

  Lemma is_sat_valid e : is_sat e = true → ∃ M, as_bool e M = true.
  Proof. eapply bexpr_is_sat_valid, bool_expr_mixin. Qed.

  Lemma occurs_valid e i m v : occurs e i = false → as_bool e m = as_bool e (<[i:=v]> m).
  Proof. eapply bexpr_occurs_valid, bool_expr_mixin. Qed.

End bool_expr_laws.

(*
From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import symbolic_executor class_instances tactics_proofstep proofstep.
From diaframe.experiments Require Import solve_utils tele_utils solve_defs. 
TODO: if we want to do this again, reinstate proper imports*)
From iris.bi Require Import bi telescopes.

Import bi.


Section bi_test.
  Context {PROP : bi}.
  Context (β : bool_expr).
  Implicit Types (e : β).
  Open Scope positive_scope.


  Lemma test_case (P Q : PROP) a b d e1 e2 e3 :
    make_false_by e2 3 = Some e3 →
(*    c = as_bool e3 $ <[1:=a]> $ <[2:=b]> $ <[3:= d]> ∅ →*)
    (P??(as_bool (subs e1 3 e3) $ <[1:=a]> $ <[2:=b]> $ <[3:= d]> ∅) ⊢ emp) →
    ∃ c, 
    P??(as_bool e1 $ <[1:=a]> $ <[2:=b]> $ <[3:= c]> ∅) ⊢ Q??(as_bool e2 $ <[1:=a]> $ <[2:=b]> $ <[3:= c]> ∅).
  Proof.
    intros Hmf HPn.
    exists (as_bool e3 $ <[1:=a]> $ <[2:=b]> $ <[3:= d]> ∅).
    rewrite -(insert_commute _ 3%positive) //.
    rewrite -(insert_commute _ 3%positive) //.
    rewrite -(insert_insert _ 3%positive _ d).
    rewrite (insert_commute _ 3%positive 1%positive) //.
    rewrite (insert_commute _ 3%positive 2%positive) //.
    erewrite (make_false_by_valid _ _ _ _ _ Hmf) => //.
    change (Q??false) with (emp : PROP)%I.
    rewrite insert_subs.
    done.
  Qed.

End bi_test.

Section map_facts.
  Context `{FinMap K M}.

  Lemma intersection_with_union_with {A} (a b c : M A) (f g : A → A → option A) :
    (∀ i : K, 
      (x' ← a !! i; y' ← b !! i; z' ← c !! i;
      (r1 ← g x' y'; f r1 z')) = 
      (x' ← a !! i; y' ← b !! i; z' ← c !! i;
      match f x' z', f y' z' with
      | Some r1, Some r2 => g r1 r2
      | None, r2 => r2
      | r1, None => r1
      end)) →
    intersection_with f (union_with g a b) c = 
    union_with g (intersection_with f a c) (intersection_with f b c).
  Proof.
    intros.
    apply map_eq => i.
    rewrite lookup_intersection_with. 
    rewrite !lookup_union_with !lookup_intersection_with.
    specialize (H7 i).
    destruct (a !! i) eqn:?;
    destruct (b !! i) eqn:?;
    destruct (c !! i) eqn:? => //=.
    - revert H7.
      compute => ->.
      by destruct (f a1 a2).
    - compute.
      by destruct (g a0 a1).
    - compute.
      by destruct (f a0 a1).
    - compute.
      by destruct (f a0 a1).
  Qed.

End map_facts.

Module bool_expr_instance.
  Open Scope positive_scope.

  Definition bool_expr_and := Pmap bool.
  Definition bool_exprs_or := list (Pmap bool).

  Definition negb_if (cond : bool) (val : option bool) : bool :=
    let val' := match val with | Some val' => val' | None => false end in
    if cond then Datatypes.negb val' else val'.

  Global Arguments negb_if (!_) (!_) /.

  Definition as_bool_and (bexpr : bool_expr_and) (m : Pmap bool) : bool := 
    map_fold (λ i b, andb (negb_if b $ m !! i)) true bexpr.

  Definition as_bool_or (bexpr : bool_exprs_or) (m : Pmap bool) : bool := 
    foldl orb false $ flip as_bool_and m <$> bexpr.

  Definition bexpr_and_conj (e1 e2 : bool_expr_and) : bool_exprs_or :=
    match decide (intersection_with (λ bl br, if beq bl br then None else Some true) e1 e2 = ∅) with
    | left _ => [union_with (λ bl br, Some bl) e1 e2]
    | right _ => [] (* e1 and e2 have overlap which differ, meaning this conjunction is always false *)
    end.

  Definition bexpr_or_conj (e1 e2 : bool_exprs_or) : bool_exprs_or :=
    mjoin ((mjoin ((λ b, bexpr_and_conj b <$> e2) <$> e1))).

  Definition bexpr_and_neg (e : bool_expr_and) : bool_exprs_or :=
    reverse $ snd $ map_fold (λ i b pr, (<[i:=b]> pr.1, <[i:=Datatypes.negb b]> pr.1 :: pr.2)) (∅, []) e.

  Definition bexpr_or_neg (e : bool_exprs_or) : bool_exprs_or :=
    foldl bexpr_or_conj [∅] $ bexpr_and_neg <$> e.

  Definition bexpr_negb_if (b : bool) (e : bool_exprs_or) : bool_exprs_or :=
    if b then bexpr_or_neg e else e.

  Definition bexpr_and_occurs (b : bool_expr_and) i : bool := 
    match b !! i with
    | None => false
    | Some _ => true
    end.

  Definition bexpr_or_occurs e i : bool := 
    foldl orb false $ flip bexpr_and_occurs i <$> e.

  Definition bexpr_and_sub b1 i e2 : bool_exprs_or :=
    match b1 !! i with
    | None => [b1]
    | Some b => bexpr_or_conj [delete i b1] (bexpr_negb_if b e2)
    end.

  Definition bexpr_or_sub e1 i e2 : bool_exprs_or :=
    mjoin $ (λ e, bexpr_and_sub e i e2) <$> e1.

  Definition bexpr_or_is_sat (e : bool_exprs_or) : bool :=
    match e with
    | [] => false
    | _ => true
    end.

  Definition make_bexpr_and_false (e : bool_expr_and) (i : positive) : option bool_exprs_or :=
    b ← (e !! i); Some (bexpr_negb_if b $ <[i := b]> <$> bexpr_and_neg (delete i e)).

  Definition make_bexpr_or_false (e : bool_exprs_or) (i : positive) : option bool_exprs_or :=
    match e with
    | [] => Some [ {[i:=false]} ]
    | [b] => make_bexpr_and_false b i
    | _ => None
    end.
(* A nicer definition would be as below, but when does this terminate?
  Fixpoint make_bexpr_or_false (e : bool_exprs_or) (i : positive) : option bool_exprs_or :=
    match e with
    | [] => Some [ {[i := false]} ]
    | cons b e => b' ← make_bexpr_and_false b i; 
                  b2 ← make_bexpr_or_false (bexpr_or_sub e i b') i;
                  Some $ bexpr_or_sub b' i b2
    end. *)

  Global Instance false_orb_leftid : LeftId (=) false orb.
  Proof. done. Qed.

  Global Instance false_orb_rightid : RightId (=) false orb.
  Proof. move => [] //. Qed.

  Global Instance orb_assoc : Assoc (=) orb.
  Proof. move => [] [] [] //. Qed.

  Lemma foldl_cons {A} (h : A) t op start : 
    LeftId (=) start op →
    RightId (=) start op →
    Assoc (=) op →
    foldl op start (h :: t) = op h $ (foldl op start t).
  Proof.
    intros.
    revert h.
    induction t.
    - simpl => h.
      by rewrite H H0.
    - simpl => h.
      rewrite -H1.
      change (foldl op (op start (op h a)) t) with (foldl op start (op h a :: t)).
      rewrite IHt.
      rewrite -H1.
      apply f_equal.
      by rewrite -IHt /=.
  Qed.

  Lemma foldl_app_outside {A} (t1 t2 : list A) op start : 
    LeftId (=) start op →
    RightId (=) start op →
    Assoc (=) op →
    foldl op start (app t1 t2) = op (foldl op start t1) $ (foldl op start t2).
  Proof.
    intros.
    induction t1.
    - simpl. by rewrite H.
    - cbn [app].
      rewrite foldl_cons IHt1.
      rewrite H1.
      by rewrite -(foldl_cons a t1).
  Qed.

  Lemma as_bool_or_cons e t m : as_bool_or (e :: t) m = as_bool_and e m || as_bool_or t m.
  Proof. rewrite /as_bool_or -foldl_cons //. Qed.

  Lemma as_bool_or_app e1 e2 m : as_bool_or (e1 ++ e2)%list m = as_bool_or e1 m || as_bool_or e2 m.
  Proof.
    induction e1 as [|b1 e1] => //=.
    by rewrite !as_bool_or_cons IHe1 orb_assoc.
  Qed.

  Lemma join_app {A} (a b : list (list A)) : mjoin (app a b) = app (mjoin a) $ mjoin b.
  Proof.
    revert b.
    induction a => //= b.
    by rewrite -assoc IHa.
  Qed.

  Lemma as_bool_or_conj_cons e1 a e2 m :
    as_bool_or (bexpr_or_conj (a :: e1) e2) m = as_bool_or (bexpr_or_conj [a] e2) m || as_bool_or (bexpr_or_conj e1 e2) m.
  Proof.
    rewrite /bexpr_or_conj /as_bool_or /=.
    rewrite join_app fmap_app.
    by rewrite foldl_app_outside app_nil_r.
  Qed.

  Lemma as_bool_and_insert i b e m : 
    e !! i = None →
    as_bool_and (<[i:=b]> e) m = negb_if b (m !! i) && as_bool_and e m.
  Proof.
    move => Hei.
    rewrite /as_bool_and map_fold_insert_L //.
    move => j1 j2 z1 z2 y _ _ _.
    rewrite andb_comm -andb_assoc.
    apply f_equal.
    by rewrite andb_comm.
  Qed.

  Lemma as_bool_and_true_element_true e m :
    as_bool_and e m = true →
    ∀ i b, e !! i = Some b → negb_if b (m !! i) = true.
  Proof.
    induction e as [|i x e] using map_ind => //=.
    rewrite as_bool_and_insert //.
    move => /andb_prop [Hxmi /IHe Hm0] i0 b Hi0b.
    destruct (decide (i0 = i)).
    - subst. rewrite lookup_insert in Hi0b; by simplify_eq.
    - apply Hm0.
      rewrite -Hi0b lookup_insert_ne //.
  Qed.

  Lemma as_bool_and_insert2 i b e m : 
    e !! i = None ∨ e !! i = Some b →
    as_bool_and (<[i:=b]> e) m = negb_if b (m !! i) && as_bool_and e m.
  Proof.
    move => [Hei|Hei]; first by apply as_bool_and_insert.
    rewrite insert_id //.
    destruct (as_bool_and e m) eqn:Hem; last by rewrite andb_false_r.
    rewrite andb_true_r eq_comm.
    eapply as_bool_and_true_element_true => //.
  Qed.

  Lemma expr_intersection_differ_empty (e1 e2 : bool_expr_and) :
    intersection_with (λ bl br : bool, if beq bl br then None else Some true) e1 e2 = ∅ →
    ∀ i b1 b2, e1 !! i = Some b1 → e2 !! i = Some b2 → b1 = b2.
  Proof.
    intros.
    apply beq_true.
    destruct (beq b1 b2) eqn:Hb => //.
    contradict H.
    enough ((intersection_with (λ bl br : bool, if beq bl br then None else Some true) e1 e2) !! i = Some true).
    { contradict H.
      rewrite H.
      apply lookup_empty_Some. }
    apply lookup_intersection_with_Some.
    exists b1, b2.
    split => //.
    split => //.
    by rewrite Hb.
  Qed.

  Lemma bexpr_and_conj_spec e1 e2 m :
    foldl orb false (flip as_bool_and m <$> bexpr_and_conj e1 e2) = as_bool_and e1 m && as_bool_and e2 m.
  Proof.
    rewrite /bexpr_and_conj.
    case: (decide (intersection_with _ e1 e2 = ∅)).
    - move => /expr_intersection_differ_empty H.
      induction e1 using map_ind.
      * simpl.
        by rewrite LeftId_instance_1.
      * simpl.
        enough (negb_if x (m !! i) && as_bool_and (union_with (λ bl _ : bool, Some bl) m0 e2) m =
                as_bool_and (<[i:=x]> m0) m && as_bool_and e2 m).
        { destruct (e2 !! i) eqn:?.
          + rewrite -{1}(insert_id e2 i b) //.
            rewrite -(insert_union_with _ m0 e2 _ _ _ x) //.
            rewrite as_bool_and_insert2 //.
            right.
            rewrite lookup_union_with H0 left_id Heqo.
            apply f_equal, eq_sym.
            eapply H => //.
            by rewrite lookup_insert.
          + rewrite -insert_union_with_l //.
            rewrite as_bool_and_insert //.
            by rewrite lookup_union_with H0 Heqo. }
        simpl in IHe1.
        rewrite as_bool_and_insert // -andb_assoc IHe1 //.
        intros.
        revert H0.
        destruct (decide (i = i0)); [subst; by rewrite H1|].
        intros.
        eapply H => //.
        rewrite lookup_insert_ne //.
    - move => Hne /=.
      apply map_choose in Hne.
      destruct Hne as [i [x Hix]].
      apply lookup_intersection_with_Some in Hix.
      destruct Hix as [b1 [b2 [He1 [He2 Hb1b2]]]].
      destruct (beq b1 b2) eqn:Hb1b2' => //; simplify_eq.
      rewrite -(insert_id e1 i b1) //.
      rewrite -(insert_id e2 i b2) //.
      rewrite !as_bool_and_insert2; try by right.
      destruct b1; destruct b2; destruct (m !! i) as [[]|] => //=;
      by rewrite andb_false_r.
  Qed.

  Lemma bexpr_and_valid (e1 e2 : bool_exprs_or) (m : Pmap bool) : 
    as_bool_or (bexpr_or_conj e1 e2) m = andb (as_bool_or e1 m) (as_bool_or e2 m).
  Proof.
    revert e2.
    induction e1 as [|b1 e1].
    - intros e2.
      by rewrite /bexpr_or_conj /= /as_bool_or fmap_nil /=.
    - intros.
      rewrite as_bool_or_cons.
      rewrite andb_orb_distrib_l.
      rewrite as_bool_or_conj_cons.
      rewrite IHe1.
      apply (f_equal (λ b, b || as_bool_or e1 m && as_bool_or e2 m)).
      rewrite {1}/as_bool_or /bexpr_or_conj /= app_nil_r /=.
      induction e2 as [|b2 e2] => //=.
      { by rewrite andb_false_r. }
      rewrite fmap_app foldl_app_outside.
      rewrite bexpr_and_conj_spec IHe2.
      rewrite as_bool_or_cons.
      by rewrite andb_orb_distrib_r andb_comm.
  Qed.

  Definition expr_equiv : relation bool_exprs_or := λ e1 e2, ∀ m, as_bool_or e1 m = as_bool_or e2 m.

  Global Instance expr_equiv_preorder : PreOrder expr_equiv.
  Proof.
    constructor => //.
    move => e1 e2 e3 He1e2 He2e3 m.
    by rewrite He1e2 He2e3.
  Qed.

  Global Instance permutation_equiv_subrelation :
    subrelation (≡ₚ) expr_equiv.
  Proof.
    move => e1 e2 Hp m.
    revert e1 e2 Hp.
    apply Permutation_ind => //=.
    - move => b e1 e2 Hp Hem.
      by rewrite !as_bool_or_cons Hem.
    - move => b1 b2 e1.
      rewrite !as_bool_or_cons assoc.
      by rewrite (orb_comm (as_bool_and b2 _)) -assoc.
    - move => e1 e2 e3 Hp1 Hm1 Hp2 Hm2.
      by rewrite Hm1.
  Qed.

  Global Instance as_bool_or_proper : Proper (expr_equiv ==> (=) ==> (=)) as_bool_or.
  Proof. move => e1 e2 He1 m m' ->. apply He1. Qed.

  Lemma orb_to_disj a b : a || b = a || (Datatypes.negb a && b).
  Proof. destruct a; destruct b => //. Qed.

  Lemma negb_negb_if a b : Datatypes.negb (negb_if a b) = negb_if (Datatypes.negb a) b.
  Proof. destruct a; destruct b => //=. by rewrite negb_involutive. Qed.

  Lemma bexpr_and_neg_spec e m :
    as_bool_or (bexpr_and_neg e) m = Datatypes.negb (as_bool_and e m).
  Proof.
    rewrite /bexpr_and_neg.
    rewrite reverse_Permutation.
    set (fold_fun := (λ (i0 : positive) (b : bool) (pr : Pmap bool * list (Pmap bool)),
        (<[i0:=b]> pr.1, <[i0:=Datatypes.negb b]> pr.1 :: pr.2))).
    enough (as_bool_or (map_fold fold_fun (∅, []) e).2 m = Datatypes.negb (as_bool_and e m) ∧ 
            (map_fold fold_fun (∅, []) e).1 = e).
    { by apply H. }
    revert e.
    apply (map_fold_ind (λ r e, as_bool_or r.2 m = Datatypes.negb (as_bool_and e m) ∧ r.1 = e)) => //=.
    move => i b e [e' r'] Hei /= [Herm ->].
    split => //.
    rewrite as_bool_or_cons.
    rewrite !as_bool_and_insert // Herm.
    rewrite negb_andb orb_comm eq_comm orb_comm.
    rewrite orb_to_disj negb_involutive.
    apply f_equal.
    by rewrite andb_comm negb_negb_if.
  Qed.

  Lemma and_conj_true_r e : bexpr_and_conj e ∅ = [e].
  Proof.
    rewrite /bexpr_and_conj.
    rewrite right_absorb.
    destruct (decide _) => //.
    by rewrite right_id.
  Qed.

  Lemma and_conj_true_l e : bexpr_and_conj ∅ e = [e].
  Proof.
    rewrite /bexpr_and_conj.
    rewrite left_absorb.
    destruct (decide _) => //.
    by rewrite left_id.
  Qed.

  Global Instance or_conj_true_leftid : LeftId (=) [∅] bexpr_or_conj.
  Proof.
    move => e.
    rewrite /bexpr_or_conj /= right_id.
    induction e => //=.
    by rewrite and_conj_true_l IHe.
  Qed.

  Global Instance or_conj_true_rightid : RightId (=) [∅] bexpr_or_conj.
  Proof.
    move => e.
    rewrite /bexpr_or_conj /=.
    induction e => //=.
    by rewrite and_conj_true_r IHe.
  Qed.

  Lemma and_conj_almost_assoc e1 e2 e3 : 
    mjoin $ (flip bexpr_and_conj e1) <$> bexpr_and_conj e3 e2 =
    mjoin $ bexpr_and_conj e3 <$> bexpr_and_conj e2 e1.
  Proof.
    rewrite /bexpr_and_conj /=.
    set (f := (λ bl br, if beq bl br then None else Some true)).
    set (g := (λ bl _, Some bl)).
    assert (Comm eq f) as Hcommf by move => [] [] //=.
    assert (∀ (d1 d2 d3 : bool_expr_and), intersection_with f d2 d3 = ∅ →
        intersection_with f (union_with g d2 d3) d1 = 
        union_with g (intersection_with f d2 d1) (intersection_with f d3 d1)) as Hiu.
    { move => d1 d2 d3 /expr_intersection_differ_empty He2e3.
      rewrite intersection_with_union_with // => i.
      specialize (He2e3 i); revert He2e3.
      case: (d2 !! i) => //= y.
      case: (d3 !! i) => //= z.
      case: (d1 !! i) => //= x Hzy.
      specialize (Hzy y z eq_refl eq_refl).
      rewrite Hzy.
      destruct (f z x) => //. }
    rewrite /flip /=.
    case: (decide (intersection_with _ e3 e2 = ∅));
    case: (decide (intersection_with _ e2 e1 = ∅)).
    - move => He1e2.
      move => He2e3 /=.
      rewrite !right_id.
      rewrite Hiu //.
      set (Comm_instance_1 _ _ e3 (union_with g e2 e1)) as H'.
      rewrite H' {H'}.
      rewrite Hiu //.
      rewrite (Comm_instance_1 _ _ e1 e3).
      rewrite (Comm_instance_1 _ _ e2 e3).
      rewrite He1e2 He2e3 left_id right_id.
      case: (decide (intersection_with f e3 e1 = ∅)) => //= He3e1.
      apply (f_equal (λ r, cons r nil)).
      apply map_eq => i.
      rewrite !lookup_union_with.
      specialize (expr_intersection_differ_empty _ _ He1e2 i) => Hb1b2.
      specialize (expr_intersection_differ_empty _ _ He2e3 i) => Hb2b3.
      destruct (e1 !! i) eqn:He1; rewrite He1;
      destruct (e2 !! i) eqn:He2; rewrite He2;
      destruct (e3 !! i) eqn:He3; rewrite He3 => //=.
    - move => He1e2 He2e3 /=.
      rewrite right_id.
      rewrite Hiu //.
      enough (union_with g (intersection_with f e3 e1) (intersection_with f e2 e1) ≠ ∅).
      { destruct (decide _) => //. }
      specialize (map_choose _ He1e2) => {He1e2} He1e2i.
      destruct He1e2i as [i [b He1e2]].
      enough (union_with g (intersection_with f e3 e1) (intersection_with f e2 e1) !! i = Some b).
      { contradict H; rewrite H lookup_empty //. }
      rewrite lookup_union_with He1e2.
      assert (b = true) as ->.
      { revert He1e2 => /lookup_intersection_with_Some He1e2.
        destruct He1e2 as [[] [[] [H1 [H2 H3]]]] => //=;
        rewrite /f /= in H3; simplify_eq => //. }
      destruct (intersection_with f e3 e1 !! i) eqn: He1e2i; rewrite He1e2i => //=.
      assert (b = true) as ->.
      { revert He1e2i => /lookup_intersection_with_Some He1e2i.
        destruct He1e2i as [[] [[] [H1 [H2 H3]]]] => //=;
        rewrite /f /= in H3; simplify_eq => //. }
      done.
    - move => He1e2 He2e3 /=.
      rewrite right_id.
      rewrite (Comm_instance_1 _ _ e3 (union_with g e2 e1)).
      rewrite Hiu //.
      apply eq_sym.
      enough (union_with g (intersection_with f e2 e3) (intersection_with f e1 e3) ≠ ∅).
      { destruct (decide _) => //. }
      rewrite (Comm_instance_1 _ _ e2 e3).
      specialize (map_choose _ He2e3) => {He2e3} He2e3i.
      destruct He2e3i as [i [b He2e3]].
      enough (union_with g (intersection_with f e3 e2) (intersection_with f e1 e3) !! i = Some b).
      { contradict H; rewrite H lookup_empty //. }
      rewrite lookup_union_with He2e3.
      case: (intersection_with f e1 e3 !! i) => //=.
    - by move => He1e2 He2e3 /=.
  Qed.

  Lemma bexpr_and_conj_case e1 e2 : bexpr_and_conj e1 e2 = [] ∨ ∃ b, bexpr_and_conj e1 e2 = [b].
  Proof.
    rewrite /bexpr_and_conj.
    case: (decide (intersection_with _ _ _ = ∅)) => //= Hint.
    - right. by eexists.
    - by left.
  Qed.

  Global Instance or_conj_assoc : Assoc (=) bexpr_or_conj.
  Proof.
    move => e3 e2 e1.
    rewrite /bexpr_or_conj.
    induction e3 as [|b1 e3] => //.
    change (b1 :: e3) with ([b1] ++ e3)%list.
    rewrite !fmap_app !join_app !right_id !fmap_app !join_app -IHe3 {IHe3} /=.
    match goal with
    | |- (?l1 ++ ?l2 = ?l3 ++ ?l2)%list => enough (l1 = l3) as H; first by rewrite H
    end.
    induction e2 as [|b2 e2] => //.
    change (b2 :: e2) with ([b2] ++ e2)%list.
    rewrite !fmap_app !join_app !right_id !fmap_app !join_app -IHe2 {IHe2} /=.
    match goal with
    | |- (?l1 ++ ?l2 = ?l3 ++ ?l2)%list => enough (l1 = l3) as H; first by rewrite H
    end.
    induction e1 as [|b3 e1].
    - simpl. apply eq_sym. match goal with | |- mjoin ?l = [] => enough (l = []) as -> => // end.
      apply join_nil.
      apply Forall_fmap.
      apply Forall_true => x //=.
    - change (b3 :: e1) with ([b3] ++ e1)%list.
      destruct (bexpr_and_conj_case b1 b2).
      * rewrite H /=.
        rewrite !fmap_app !join_app /=.
        rewrite IHe1 {IHe1}.
        rewrite H /= right_id.
        rewrite -and_conj_almost_assoc /= H //=.
      * destruct H as [b Hb].
        rewrite !fmap_app !join_app.
        rewrite IHe1 Hb /= !right_id.
        match goal with
        | |- (?l1 ++ ?l2 = ?l3 ++ ?l2)%list => 
          enough (l1 = l3) as H; first by rewrite H
        end.
        by rewrite -and_conj_almost_assoc /flip /= Hb /= right_id.
  Qed.

  Lemma bexpr_or_neg_valid e m :
    as_bool_or (bexpr_or_neg e) m = Datatypes.negb (as_bool_or e m).
  Proof.
    rewrite /bexpr_or_neg.
    induction e => //.
    cbn [fmap list_fmap flip].
    rewrite foldl_cons.
    rewrite bexpr_and_valid IHe {IHe}.
    by rewrite (as_bool_or_cons a e) negb_orb bexpr_and_neg_spec.
  Qed.

  Lemma bexpr_and_occurs_valid e i v m :
    bexpr_and_occurs e i = false →
    as_bool_and e m = as_bool_and e (<[i:=v]> m).
  Proof.
    rewrite /bexpr_and_occurs.
    destruct (e !! i) as [b|] eqn:He => //= _.
    revert He.
    induction e as [|b j e] using map_ind => //=.
    move => /lookup_insert_None [/IHe Hei Hij].
    rewrite !as_bool_and_insert // -Hei.
    rewrite lookup_insert_ne //.
  Qed.

  Lemma bexpr_or_occurs_valid e i v m :
    bexpr_or_occurs e i = false →
    as_bool_or e m = as_bool_or e (<[i:=v]> m).
  Proof.
    rewrite /bexpr_or_occurs.
    induction e as [|b e] => //.
    cbn [fmap list_fmap flip].
    rewrite foldl_cons => /orb_false_elim [Hbi /IHe Hei].
    rewrite !as_bool_or_cons -Hei -bexpr_and_occurs_valid //.
  Qed.

  Lemma negb_if_negb_true b :
    negb_if b (Some $ Datatypes.negb b) = true.
  Proof. case: b => //=. Qed.

  Lemma as_bool_and_negb_self_gen e1 e2 :
    e1 ⊆ e2 →
    as_bool_and e1 (Datatypes.negb <$> e2) = true.
  Proof.
    rewrite /as_bool_and.
    induction e1 as [|j b e1] using map_ind => //= /map_subseteq_spec He1e2.
    assert (e2 !! j = Some b) as He2b.
    { revert He1e2.
      move => /(dep_eval j) /(dep_eval b).
      rewrite lookup_insert => /(dep_eval eq_refl) //. }
    rewrite map_fold_insert_L //.
    - rewrite lookup_fmap He2b.
      rewrite negb_if_negb_true /=.
      apply IHe1.
      apply map_subseteq_spec => i x He1x.
      apply He1e2.
      destruct (decide (j = i)) as [->|Hnij].
      contradict He1x. rewrite H //.
      rewrite lookup_insert_ne //.
    - intros.
      rewrite !andb_assoc (andb_comm (negb_if _ _)) //.
  Qed.

  Lemma as_bool_and_negb_self e :
    as_bool_and e (Datatypes.negb <$> e) = true.
  Proof. by apply as_bool_and_negb_self_gen. Qed.

  Lemma bexpr_is_sat_valid e :
    bexpr_or_is_sat e = true →
    ∃ m, as_bool_or e m = true.
  Proof.
    case e => //= b e' {e} _.
    exists (Datatypes.negb <$> b).
    rewrite as_bool_or_cons.
    by rewrite as_bool_and_negb_self /=.
  Qed.

  Lemma bexpr_is_sat_complete e :
    (∃ m, as_bool_or e m = true) →
    bexpr_or_is_sat e = true.
  Proof.
    move => [m Hem].
    destruct e as [|b e'] => //=.
  Qed.

  Lemma bexpr_negb_if_valid b e m : as_bool_or (bexpr_negb_if b e) m = negb_if b $ Some $ as_bool_or e m.
  Proof.
    destruct b => //=.
    by rewrite bexpr_or_neg_valid.
  Qed.

  Lemma bexpr_and_sub_spec b1 i e2 m :
    as_bool_and b1 (<[i:= as_bool_or e2 m]> m) = as_bool_or (bexpr_and_sub b1 i e2) m.
  Proof.
    rewrite /bexpr_and_sub.
    destruct (b1 !! i) eqn:Hbi; rewrite Hbi; last first.
    { apply eq_sym, bexpr_and_occurs_valid.
      rewrite /bexpr_and_occurs Hbi //. }
    rewrite bexpr_and_valid.
    rename b into v.
    induction b1 as [|j b b1] using map_ind => //=.
    destruct (decide (i = j)) as [Heq|Hne].
    - revert IHb1 Hbi.
      rewrite Heq => {Heq} IHb1.
      rewrite lookup_insert => [= ->].
      rewrite as_bool_and_insert // lookup_insert.
      rewrite delete_insert_delete.
      rewrite delete_notin //.
      rewrite bexpr_negb_if_valid.
      apply eq_sym.
      rewrite andb_comm.
      apply f_equal.
      rewrite /as_bool_or /=.
      apply bexpr_and_occurs_valid.
      rewrite /bexpr_and_occurs H //.
    - rewrite delete_insert_ne //.
      rewrite {2}/as_bool_or /=.
      set (H':= as_bool_and_insert j b (delete i b1)).
      rewrite H' {H'}; last first.
      { rewrite lookup_delete_ne //. }
      rewrite as_bool_and_insert //.
      rewrite lookup_insert_ne // -andb_assoc.
      apply f_equal.
      rewrite IHb1 // -Hbi.
      rewrite lookup_insert_ne //.
  Qed.

  Lemma bexpr_or_sub_spec e1 i e2 m :
    as_bool_or e1 (<[i:=as_bool_or e2 m]> m) = as_bool_or (bexpr_or_sub e1 i e2) m.
  Proof.
    rewrite /bexpr_or_sub.
    induction e1 as [|b1 e1] => //=.
    rewrite as_bool_or_cons.
    rewrite as_bool_or_app.
    rewrite IHe1 bexpr_and_sub_spec //.
  Qed.

  Lemma make_bexpr_and_false_spec b i e m :
    make_bexpr_and_false b i = Some e →
    as_bool_and b (<[i:=as_bool_or e m]> m) = false.
  Proof.
    rewrite /make_bexpr_and_false.
    destruct (b !! i) as [v|] eqn:Hbiv => //= [= Hbe].
    rewrite -(insert_id b i v) //.
    rewrite -insert_delete.
    rewrite as_bool_and_insert; last first.
    { rewrite lookup_delete //. }
    rewrite lookup_insert.
    rewrite -bexpr_and_occurs_valid; last first.
    { rewrite /bexpr_and_occurs lookup_delete //. }
    rewrite -Hbe {Hbe}.
    replace (<[i:=v]> <$> bexpr_and_neg (delete i b)) with (bexpr_or_conj [{[i:=v]}] (bexpr_and_neg (delete i b))).
    - rewrite bexpr_negb_if_valid bexpr_and_valid {Hbiv}.
      generalize v at 3.
      wlog: v /(v = false).
      * move => Hv v2; destruct v => //=.
        rewrite negb_involutive.
        all: apply (Hv _ eq_refl).
      * move => -> {v} /= v.
        rewrite -andb_assoc.
        rewrite bexpr_and_neg_spec.
        rewrite andb_negb_l andb_false_r //.
    - rewrite /bexpr_or_conj /= right_id.
(* put stuff below in lemmas..?*)
      assert (delete i b !! i = None) by by rewrite lookup_delete.
      revert H.
      generalize (delete i b) => b' Hb' {Hbiv b m e}.
      assert (Forall (λ b, b !! i = None) $ bexpr_and_neg b').
      { rewrite /bexpr_and_neg.
        apply Forall_reverse.
        revert Hb'.
        set (r := map_fold _ _ _).
        apply (map_fold_ind (λ r e, (e !! i = None → Forall (λ b, b!!i = None) r.2) ∧ r.1 = e)) => //=.
        move => j w b'' [e b] Hbj1 /= [Hbb ->].
        split => // /lookup_insert_None [Hbi Hij].
        apply Forall_cons.
        split; last by apply Hbb.
        rewrite lookup_insert_ne //. }
      revert H.
      generalize (bexpr_and_neg b') => {b' Hb'} e.
      induction e as [|b e] => //= /Forall_cons [Hbi Hei].
      rewrite {1}/bexpr_and_conj IHe //.
      rewrite decide_left.
      { apply map_eq => j. 
        rewrite lookup_empty.
        rewrite lookup_intersection_with.
        destruct (decide (i = j)) as [Heq|Hne].
        + rewrite -Heq Hbi.
          rewrite lookup_insert //=.
        + rewrite lookup_insert_ne //. }
      intros.
      rewrite -insert_union_with_l // left_id //=.
  Qed.

  Lemma make_bexpr_or_false_spec e1 i e2 m :
    make_bexpr_or_false e1 i = Some e2 →
    as_bool_or e1 (<[i:=as_bool_or e2 m]> m) = false.
  Proof.
    rewrite /make_bexpr_or_false.
    destruct e1 as [|b1 [|b2 e1]] => //=.
    rewrite /as_bool_or /=.
    apply make_bexpr_and_false_spec.
  Qed.

  Lemma bexprs_map_list_mixin : 
    BoolExprMixin bool_exprs_or bexpr_or_neg bexpr_or_conj as_bool_or 
                  bexpr_or_sub make_bexpr_or_false bexpr_or_is_sat bexpr_or_occurs.
  Proof.
    constructor; intros.
    - apply bexpr_or_sub_spec.
    - by apply make_bexpr_or_false_spec.
    - apply bexpr_or_neg_valid.
    - apply bexpr_and_valid.
    - by apply bexpr_is_sat_valid.
    - by apply bexpr_or_occurs_valid.
  Qed.

  Definition bool_expr_instance : bool_expr :=
    BoolExpr _ _ _ _ _ _ _ _ bexprs_map_list_mixin.

End bool_expr_instance.































