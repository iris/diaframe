From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes solve_defs tele_utils.
From diaframe.steps Require Import delay_choice solve_sep solve_one namer_tacs small_steps pure_solver.
From iris.bi Require Import bi telescopes.

Import bi.

Section additional_rules.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma solve_sep_disj {TT : tele} b1 b2 M P P1 P2 Q c R :
    (∀.. tt : TT, FromOr (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    (open_choice c true → R ⊢ SolveSep (TT := TT) b1 M P1 Q) →
    (open_choice c false → R ⊢ SolveSep (TT := TT) b1 M P2 Q) →
    ModalityMono M →
    R ⊢ SolveSep (TT := TT) b2 M P Q.
  Proof.
    move => /tforall_forall HPP.
    move => /choice_disj_open_choice HP /HP {HP} HR.
    eassert (R ⊢ SolveSep b1 M P1 Q ∨ SolveSep b1 M P2 Q) as ->.
    { case: HR => ->; eauto. }
    move => HM.
    rewrite /SolveSep.
    apply or_elim.
    - apply HM, bi_texist_mono => tt.
      by rewrite -HPP -or_intro_l.
    - apply HM, bi_texist_mono => tt.
      by rewrite -HPP -or_intro_r.
  Qed.

  Lemma solve_one_disj {TT : tele} b1 b2 M P P1 P2 c R :
    (∀.. tt : TT, FromOr (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    (open_choice c true → R ⊢ SolveOne (TT := TT) b1 M P1) →
    (open_choice c false → R ⊢ SolveOne (TT := TT) b1 M P2) →
    ModalityMono M →
    R ⊢ SolveOne (TT := TT) b2 M P.
  Proof.
    move => /tforall_forall HPP.
    move => /choice_disj_open_choice HP /HP {HP} HR.
    eassert (R ⊢ SolveOne b1 M P1 ∨ SolveOne b1 M P2) as ->.
    { case: HR => ->; eauto. }
    move => HM.
    rewrite /SolveOne.
    apply or_elim.
    - apply HM, bi_texist_mono => tt.
      by rewrite -HPP -or_intro_l.
    - apply HM, bi_texist_mono => tt.
      by rewrite -HPP -or_intro_r.
  Qed.

End additional_rules.

Section coq_tactics.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma tac_solve_sep_disj Δ {TT : tele} M P P1 P2 Q c :
    (∀.. tt : TT, FromOr (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ModalityMono M →
    (open_choice c true → envs_entails Δ $ SolveSep (TT := TT) false M P1 Q) →
    (open_choice c false → envs_entails Δ $ SolveSep (TT := TT) false M P2 Q) →
    envs_entails Δ $ SolveSep (TT := TT) true M P Q.
  Proof.
    rewrite envs_entails_eq.
    intros.
    eapply solve_sep_disj => //.
  Qed.

  Lemma tac_solve_one_disj Δ {TT : tele} M P P1 P2 c :
    (∀.. tt : TT, FromOr (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ModalityMono M →
    (open_choice c true → envs_entails Δ $ SolveOne (TT := TT) false M P1) →
    (open_choice c false → envs_entails Δ $ SolveOne (TT := TT) false M P2) →
    envs_entails Δ $ SolveOne (TT := TT) true M P.
  Proof.
    rewrite envs_entails_eq.
    intros.
    eapply solve_one_disj => //.
  Qed.

End coq_tactics.

Ltac solveSepStep namer_tac ::= 
  first
  [notypeclasses refine (tac_solve_sep_normalize _ _ _ _ _ _ _ _); [tc_solve.. | simpl]
(*  |simple notypeclasses refine (tac_solve_sep_emp _ _ _ _ _ _ _); [tc_solve | tc_solve | simpl]*)
  |simple notypeclasses refine (tac_solve_sep_fromexist_nested _ _ _ _ _ _ _ _); [ | | tc_solve | tc_solve | simpl]
  |notypeclasses refine (tac_solve_sep_fromsep _ _ _ _ _ _ _ _ _ _ _ _ _ _); 
    [tc_solve.. | simpl] (* by putting pure after fromsep, we ensure that the pure propositions are not ∧ :)*)
  |notypeclasses refine (tac_solve_sep_pure _ _ _ _ _ _ _ _ _); [tc_solve.. | simpl]
  |notypeclasses refine (tac_solve_sep_disj _ _ _ _ _ _ _ _ _ _ _); [tc_solve.. | simpl; intros ? | simpl; intros ?]
  |notypeclasses refine (tac_solve_sep_biabd _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _); 
    [tc_solve.. | register_delete namer_tac; simpl]
  |notypeclasses refine (tac_solve_sep_biabdemp _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _);
    [tc_solve.. | simpl]
  | lazymatch goal with
    | H : open_choice ?c ?r |- _ =>
      (* we have tried everything, but have failed to progress. 
         So maybe we are in an unprovable branch! try giving up *)
      give_up
    end
  ].

Ltac solveOneStep namer_tac ::=
  first
  [notypeclasses refine (tac_solve_one_normalize _ _ _ _ _ _ _); [tc_solve.. | simpl]
  |simple notypeclasses refine (tac_solve_one_fromexist_nested _ _ _ _ _ _ _); [ | | tc_solve | tc_solve | simpl]
  |notypeclasses refine (tac_solve_one_fromsep _ _ _ _ _ _ _ _ _ _ _ _ _); [tc_solve.. | simpl]
  |notypeclasses refine (tac_solve_one_pure _ _ _ _ _ _ _ _ _); [tc_solve.. | simpl; eauto | tc_solve]
  |notypeclasses refine (tac_solve_one_disj _ _ _ _ _ _ _ _ _ _); [tc_solve.. | simpl; intros ? | simpl; intros ?]
  |notypeclasses refine (tac_solve_one_abd _ _ _ _ _ _ _ _ _ _ _); [tc_solve | register_delete namer_tac; simpl]
  |notypeclasses refine (tac_solve_one_abdemp _ _ _ _ _ _ _ _); [tc_solve | simpl]
  | lazymatch goal with
    | H : open_choice ?c ?r |- _ =>
      (* we have tried everything, but have failed to progress. 
         So maybe we are in an unprovable branch! try giving up *)
      give_up
    end
  ].

Ltac trySolvePurePreSplit φ R ::=
  (* fancy pure stuff from RefinedC should go here *)
  first
  [ has_evar φ; 
    (split; first by trySolvePure ||
    (* if there is an evar, we need to solve it now *)
    fail 1 "Pure condition"φ"needs to be proved now!")
  (* else: check if we still have open_choices *)
  | lazymatch goal with
    | H: open_choice ?c ?r |- _ =>
      (* we have an open choice, so some leeway to prove our goal *)
      first
      [ split;[ by trySolvePure | ] (* do we need require? *)
      (* if φ is false, we give up.
         if φ is decidable and cant be proven immediately, we require it. 
         if φ is not decidable, we give up, hoping that the other branch will work *)
      | assert (¬ φ) as _ by trySolvePure; give_up
      | eassert (Decision φ) as _ by tc_solve;
        (* in the case that require causes a contradiction, the goal maybe empty, in which case '[assumption | ]' fails 
            But in this case fail succeeds, so we check for that situation with fail*)
        ((require φ; (split; [ assumption | ] || fail "Fail succeeds on empty goals")) 
          || fail 3 "Requiring"φ" failed but that should not happen")
      | give_up ]
    end
  (* final option: shelve it *)
   | lazymatch φ with
     | False => fail 3 "Current pure goal is False! Something went terribly wrong!"
     | _ => split; [first [by trySolvePure | shelve] | ]
     end ].













