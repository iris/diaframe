From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes tele_utils solve_defs.
From iris.bi Require Import bi telescopes.

Import bi.


Section toy.
  Context {PROP : bi}.
  Implicit Types P : PROP.
  Implicit Types M : PROP → PROP.

  Lemma two_mods M M' M1 M2 P Q R S T :
    (M1 (P ∗ R) ⊢ M2 (Q ∗ S)) →
    ModalityEC M1 →
    SplitModality3 M M' M2 →
    P ∗ (M' ((M1 R) ∗ (S -∗ T))) ⊢ M (Q ∗ T).
  Proof. (* this is only okay if M1 is later *)
    move => HPQRS HM1 [HMs HM' HM2].
    rewrite modality_ec_frame_r.
    rewrite -HMs.
    apply HM' => {HM'}.
    rewrite sep_comm sep_assoc modality_ec_frame_r.
    assert (M1 (R ∗ P) ⊢ M1 (P ∗ R)) as ->.
    { apply HM1. by rewrite sep_comm. }
    rewrite HPQRS modality_ec_frame_l.
    apply HM2.
    by rewrite -assoc wand_elim_r.
  Qed.

  Lemma two_mods_solveone M M' M1 M2 P Q R :
    (M1 (P ∗ R) ⊢ M2 Q) →
    ModalityEC M1 →
    SplitModality3 M M' M2 →
    P ∗ (M' $ M1 R) ⊢ M Q.
  Proof. (* this is okay more often *)
    move => HPQRS HM1 [HMs HM' HM2].
    rewrite modality_ec_frame_r.
    rewrite -HMs.
    apply HM' => {HM'}.
    rewrite modality_ec_frame_l.
    enough (M1 (R ∗ P) ⊢ M1 (P ∗ R)) as -> => //.
    apply HM1. by rewrite sep_comm.
  Qed.

End toy.