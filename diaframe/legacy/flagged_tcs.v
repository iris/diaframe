From stdpp Require Import coPset.
From iris.proofmode Require Import environments reduction.
From iris.program_logic Require Import language weakestpre lifting.
(*From diaframe Require Import symbolic_executor proofstep class_instances class_instances_reloc tactics_proofstep.
 TODO: reinstate proper imports if we want to use this again*)

Class TCFlag (Hole : Prop) (flags : coPset) := 
  tc_flag_elem : Hole.

Proposition drop_flag (Hole : Prop) flag : Hole → TCFlag Hole flag.
Proof. by rewrite /TCFlag. Qed.

Section ProofstepFlags.

  Implicit Types PROP : bi.
  Implicit Types Λ : language.
  Implicit Types V W : Type.
  Implicit Types TT : tele.

  Global Instance proofstep_from_symb_exec_no_pre_flag {PROP} (P : PROP) 
    `(execute : ExecuteOp PROP Λ V) e R 
    `(env_args : V → W) (red_cond : ReductionCondition PROP Λ W)  (context_cond : ContextCondition Λ) 
    K e_in' Qs n φ TT e_out' mQsout (flags_exec flags_step flags_allowed : coPset):
    TCFlag (AsExecutionOf P execute e R) flags_exec →
    flags_exec ⊆ flags_allowed →
    ExecuteReductionCompatibility execute env_args red_cond context_cond →
    ReshapeExprAnd Λ e K e_in' (
      TCAnd (TCFlag (ReductionStep' red_cond emp%I e_in' Qs n φ TT e_out' mQsout (env_args R)) flags_step)
            (flags_step ⊆ flags_allowed)
    ) →
    SatisfiesContextCondition context_cond K →
    TCFlag (ProofStep' P emp%I φ Qs n (tele_map (λ e_out, execute (K e_out) R) e_out') mQsout) flags_allowed.
  Proof.
    rewrite /TCFlag. 
    move => Hexec _ Hcompat [HK [Hred _]].
    eapply proofstep_from_symb_exec_no_pre => //.
  Qed.

End ProofstepFlags.

Section FlagDefs1.

  Definition wp_executionF : positive := 1.
  Definition wp_valueF : positive := 2.

End FlagDefs1.

Section WPFlags.

  Context `{!irisGS Λ Σ}.

  Existing Instance drop_flag.

  Global Instance as_wp_execution_flag e E s Φ : 
    TCFlag (
      AsExecutionOf (WP e @ s ; E {{ Φ }})%I wp_execute_op e [tele_arg E; s; Φ]
    ) {[ wp_executionF ]}.
  Proof. apply _. Qed.

  Global Instance pure_wp_step_exec (e : expr Λ) φ n e' w flag :
    Inhabited (state Λ) →
    TCFlag (PureExec φ n e e') flag →
    TCFlag (ReductionStep (wp_red_cond, w) e ⊣ [] ; φ =[▷^n]=> e' ⊣ []) flag.
  Proof. rewrite /TCFlag. apply _. Qed.

  Global Instance value_step_wp e v E s Φ:
    IntoVal e v →
    TCFlag (ProofStep WP e @ s ; E {{ Φ }} ⊣ [] => Φ v ⊣ []) {[ wp_valueF ]}.
  Proof. apply _. Qed.

End WPFlags.

Section FlagDefsHeapLang.

  Definition heap_loadF : positive := 3.
  Definition heap_storeF : positive := 4.
  Definition heap_allocF : positive := 5.

  Definition heap_pure_recC_F : positive := 6.
  Definition heap_pure_pairC_F : positive := 7.
  Definition heap_pure_injLC_F : positive := 8.
  Definition heap_pure_injRC_F : positive := 9.

  Definition heap_pure_betaF : positive := 10.
  Definition heap_pure_unopF : positive := 11.
  Definition heap_pure_binopF : positive := 12.
  Definition heap_pure_eqopF : positive := 13.
  Definition heap_pure_ifT_F : positive := 14.
  Definition heap_pure_ifF_F : positive := 15.
  Definition heap_pure_proj1_F : positive := 16.
  Definition heap_pure_proj2_F : positive := 17.
  Definition heap_pure_caseL_F : positive := 18.
  Definition heap_pure_caseR_F : positive := 19.

End FlagDefsHeapLang.

From iris.heap_lang Require Import notation proofmode.

Section HeapLangFlags.
  Context `{!heapGS Σ}.

  Existing Instance drop_flag.

  Global Instance load_step_wp (l : loc) v q R : 
    TCFlag (ReductionStep (wp_red_cond, R) ! #l ⊣ [l ↦{q} v] =[▷]=> v ⊣ [Some (l ↦{q} v)]) 
      {[ heap_loadF ]}.
  Proof. apply _. Qed.

  Global Instance alloc_step_wp e v (R : [tele_pair coPset; stuckness]) :
    IntoVal e v →
    TCFlag (ReductionStep (wp_red_cond, R) ref e ⊣ [] =[▷]=> ∃ l : loc, # l ⊣ [Some (l ↦ v)]) 
           {[ heap_allocF ]}.
  Proof. apply _. Qed.

  Global Instance store_step_wp (l : loc) (v' : val) (R : [tele_pair coPset; stuckness]) :
    TCFlag (ReductionStep (wp_red_cond, R) #l <- v' ⊣ [l ↦ -] =[▷]=> #() ⊣ [Some (l ↦ v')]) 
           {[ heap_storeF ]}.
  Proof. apply _. Qed.

  Global Instance pure_recc f x (erec : expr) :
    TCFlag (PureExec True 1 (Rec f x erec) (Val $ RecV f x erec)) {[ heap_pure_recC_F ]}.
  Proof. apply _. Qed.
  Global Instance pure_pairc (v1 v2 : val) :
    TCFlag (PureExec True 1 (Pair (Val v1) (Val v2)) (Val $ PairV v1 v2)) {[ heap_pure_pairC_F ]}.
  Proof. apply _. Qed.
  Global Instance pure_injlc (v : val) :
    TCFlag (PureExec True 1 (InjL $ Val v) (Val $ InjLV v)) {[ heap_pure_injLC_F ]}.
  Proof. apply _. Qed.
  Global Instance pure_injrc (v : val) :
    TCFlag (PureExec True 1 (InjR $ Val v) (Val $ InjRV v)) {[ heap_pure_injRC_F ]}.
  Proof. apply _. Qed.

  Global Instance pure_beta f x (erec : expr) (v1 v2 : val) `{!AsRecV v1 f x erec} :
    TCFlag (PureExec True 1 (App (Val v1) (Val v2)) (subst' x v2 (subst' f v1 erec))) {[ heap_pure_betaF ]}.
  Proof. apply _. Qed.

  Global Instance pure_unop op v v' :
    TCFlag (PureExec (un_op_eval op v = Some v') 1 (UnOp op (Val v)) (Val v')) {[ heap_pure_unopF ]}.
  Proof. apply _. Qed.

  Global Instance pure_binop op v1 v2 v' :
    TCFlag (PureExec (bin_op_eval op v1 v2 = Some v') 1 (BinOp op (Val v1) (Val v2)) (Val v')) 
           {[ heap_pure_binopF ]} | 10.
  Proof. apply _. Qed.
  (* Higher-priority instance for [EqOp]. *)
  Global Instance pure_eqop v1 v2 :
    TCFlag (PureExec (vals_compare_safe v1 v2) 1
      (BinOp EqOp (Val v1) (Val v2))
      (Val $ LitV $ LitBool $ bool_decide (v1 = v2))) {[ heap_pure_eqopF ]} | 1.
  Proof. apply _. Qed.

  Global Instance pure_if_true e1 e2 : 
    TCFlag (PureExec True 1 (If (Val $ LitV $ LitBool true) e1 e2) e1) {[ heap_pure_ifT_F ]}.
  Proof. apply _. Qed.

  Global Instance pure_if_false e1 e2 : 
    TCFlag (PureExec True 1 (If (Val $ LitV  $ LitBool false) e1 e2) e2) {[ heap_pure_ifF_F ]}.
  Proof. apply _. Qed.

  Global Instance pure_fst v1 v2 :
    TCFlag (PureExec True 1 (Fst (Val $ PairV v1 v2)) (Val v1)) {[ heap_pure_proj1_F ]}.
  Proof. apply _. Qed.

  Global Instance pure_snd v1 v2 :
    TCFlag (PureExec True 1 (Snd (Val $ PairV v1 v2)) (Val v2)) {[ heap_pure_proj2_F ]}.
  Proof. apply _. Qed.

  Global Instance pure_case_inl v e1 e2 :
    TCFlag (PureExec True 1 (Case (Val $ InjLV v) e1 e2) (App e1 (Val v))) {[ heap_pure_caseL_F ]}.
  Proof. apply _. Qed.

  Global Instance pure_case_inr v e1 e2 :
    TCFlag (PureExec True 1 (Case (Val $ InjRV v) e1 e2) (App e2 (Val v))) {[ heap_pure_caseR_F ]}.
  Proof. apply _. Qed.

End HeapLangFlags.

Section HeapLangFlagSets.

  Notation "{[ value ]}" := ({[ value ]} : coPset).
  Notation "{[ x ; y ; .. ; z ]}" := (
  (union .. (union (singleton x) (singleton y)) .. (singleton z)) : coPset).
  Notation "=> value" := ltac:(let raw_set := eval vm_compute in (value : coPset) in exact raw_set) 
                          (at level 0, only parsing).

  Definition all_execFS : coPset := => {[ wp_executionF ]}.

  Definition pure_projFS : coPset := => {[ heap_pure_proj1_F; heap_pure_proj2_F ]} ∪ all_execFS.
  Definition pure_caseFS : coPset := => {[ heap_pure_caseL_F; heap_pure_caseR_F ]} ∪ all_execFS.
  Definition pure_ifFS : coPset := => {[ heap_pure_ifT_F; heap_pure_ifF_F ]} ∪ all_execFS.
  Definition pure_injCFS : coPset := => {[ heap_pure_injLC_F; heap_pure_injRC_F ]} ∪ all_execFS.
  Definition pure_opFS : coPset := => {[ heap_pure_unopF; heap_pure_binopF; heap_pure_eqopF ]} 
                                          ∪ all_execFS.

  Definition pure_FS : coPset := =>
    pure_projFS ∪ pure_caseFS ∪ pure_ifFS ∪ pure_injCFS ∪ pure_opFS
                ∪ {[ heap_pure_betaF; heap_pure_pairC_F; heap_pure_recC_F; wp_valueF ]}.

  Definition noside_FS : coPset := => pure_FS ∪ {[ heap_loadF ]}.

End HeapLangFlagSets.

Section coq_tactics.

  Context {PROP : bi}.

  Lemma tac_proof_step_emp_flag {TT} (flags_allowed : coPset) Δ Δ' js ks ps P (φ : Prop) Qs n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    TCFlag (ProofStep' P emp%I φ Qs n Pout Qsout) flags_allowed →
    coq_tactics.MaybeIntoLaterNEnvs n Δ Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs) →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof. rewrite /TCFlag. apply tac_proof_step_emp. Qed.

End coq_tactics.

Ltac copset_solver := 
  match goal with
  | |- ?Y ⊆@{coPset} ⊤ => apply coPset.coPset_top_subseteq
  | |- ?X ∪ ?Y ⊆@{coPset} ?Z => apply union_subseteq; split; copset_solver
  | |- {[?x]} ⊆@{coPset} ?Y => by apply elem_of_subseteq_singleton
  end.

Hint Extern 4 (?X ⊆@{coPset} ?Y) => copset_solver : typeclass_instances.

Ltac iStep_flag_tac flags_ok xs ks :=
iStartProof;
notypeclasses refine (tac_proof_step_emp_flag flags_ok _ _ _ ks _ _ _ _ _ _ _ _ _ _ _ _);
  [tc_solve (* ProofStep *)
  |tc_solve (* MaybeIntoLaterNEnvs *)
  |solve_lookup ltac:(fun P => fail "iStep: cannot find hypothesis" P) (* env_lookup_list *)
  |try (by auto) (* pure requirement φ *)
  |cbn [ tforall tele_fold tele_bind tele_app tele_map fill foldl]; try (intros xs); intros;
   eexists _; split; 
    [reflexivity || fail "iStep: Some identifiers " ks " are present in proof context" 
    | pm_reduce; cbn [envs_delete_list bi_laterN snd]; iSimpl] ].

Ltac iStep_flag_tac_simpl_arg flags_ok k xs :=
  let flags_ok' := constr:(flags_ok : coPset) in 
  let ks := constr:(to_ident_list k) in let ks' := eval cbn in ks in iStep_flag_tac flags_ok' xs ks'.

Ltac show := 
  match goal with
  | |- ?P => idtac P
  end.

Tactic Notation "iStepF" "as" "(" simple_intropattern_list(xs) ")" constr(k) "⊆" uconstr(flags_ok) :=
      iStep_flag_tac_simpl_arg flags_ok k xs.
Tactic Notation "iStepF" "as" "(" simple_intropattern_list(xs) ")" "⊆" uconstr(flags_ok) := 
      iStep_flag_tac_simpl_arg flags_ok "" xs.
Tactic Notation "iStepF" "as" constr(k) "⊆" uconstr(flags_ok) := iStepF as ( ) k ⊆ flags_ok.
Tactic Notation "iStepF" "⊆" uconstr(flags_ok)  := iStepF as "" ⊆ flags_ok.
Tactic Notation "iStepsF" "⊆" uconstr(flags_ok) := 
  let flags_ok_raw := eval vm_compute in (flags_ok : coPset) in
  set (flags_ok_ldef := flags_ok_raw);
  repeat iStepF ⊆ flags_ok_ldef;
  clear flags_ok_ldef.

Section test_wp.
  Context `{!heapGS Σ}.

  Fixpoint is_list (l: val) (ls : list Z) : iProp Σ :=
    match ls with
    | nil => ⌜l = InjLV #()⌝
    | x :: xs => ∃ (hd:loc) l', ⌜l = InjRV #hd⌝ ∗ hd ↦ (#x, l') ∗ is_list l' xs
    end%I.

  Definition list_inc : val := rec: "inc" "x" :=
    match: "x" with
      NONE => #()
    | SOME "x2" => let: "h" := Fst (! "x2") in let: "t" := Snd (! "x2") in 
                 "x2" <- (("h" + #1), "t") ;; "inc" "t"
    end.

  Lemma list_inc_correct (xs : list Z) (l : val) : 
    {{{ is_list l xs }}} list_inc l {{{ RET #(); is_list l (map Z.succ xs) }}}.
  Proof.
    iIntros (Φ) "H Post".
    iLöb as "IH" forall (l xs Φ).
    destruct xs as [|z xs'].
    - iDestruct "H" as %->.
      iStepsOpen.
      (* iTauto *)
      by iApply "Post".
    - iSimpl in "H".
      iDestruct "H" as (hd l') "[-> [H2 H3]]".
      iStepOpen.
      iStepsF ⊆ ⊤.
      iStepsA.
      iStep as "!> H3" with "IH".
      (* iTauto should be able to solve it from here? *)
      iApply "Post".
      iSimpl.
      iExists _, _.
      by iFrame.
  Qed.

End test_wp.

From reloc.logic.proofmode Require Import spec_tactics tactics.
From reloc.logic Require Import model rules derived.

Section ReLoCFlags.

  Definition left_executionF : positive := 19.
  Definition right_executionF : positive := 20.
  Definition tp_executionF : positive := 21.

  Definition rel_valueF : positive := 22.
  Definition tp_forkF : positive := 23.

End ReLoCFlags.

Section ReLocFlagInstances.

  Context `{!relocG Σ}.

  Existing Instance drop_flag.

  Global Instance as_right_execute tl tr E A : 
    TCFlag (AsExecutionOf (REL tl << tr @ E : A) right_execute tr [tele_arg tl; E; A]) {[ right_executionF ]}.
  Proof. apply _. Qed.

  Global Instance as_tp_execute e j E1 E2 Φ : 
    TCFlag (AsExecutionOf (spec_ctx -∗ j ⤇ e ={E1,E2}=∗ Φ)%I tp_execute e [tele_arg j; E1; E2; Φ]) {[ tp_executionF ]}.
  Proof. apply _. Qed.

  Global Instance as_left_execute tl tr A : 
    TCFlag (AsExecutionOf (REL tl << tr : A) left_execute tl [tele_arg tr; A]) {[ left_executionF ]}.
  Proof. apply _. Qed.

  Global Instance as_tp_execute' e j E1 E2 Φ : 
    TCFlag (AsExecutionOf (spec_ctx ∗ j ⤇ e ={E1,E2}=∗ Φ)%I tp_execute e [tele_arg j; E1; E2; Φ]) {[ tp_executionF ]}.
  Proof. apply _. Qed.

  Global Instance as_tp_execution_elim_modal e j E1 Ψ :
    (∀ P, ElimModal True false false (|={E1}=> P) P Ψ Ψ) →
    TCFlag (AsExecutionOf (spec_ctx -∗ j ⤇ e -∗ Ψ)%I tp_execute e [tele_arg j; E1; E1; Ψ]) {[ tp_executionF ]}.
  Proof. apply _. Qed.

  Global Instance as_tp_execution_elim_modal' e j E1 Ψ :
    (∀ P, ElimModal True false false (|={E1}=> P) P Ψ Ψ) →
    TCFlag (AsExecutionOf (spec_ctx ∗ j ⤇ e -∗ Ψ)%I tp_execute e [tele_arg j; E1; E1; Ψ]) {[ tp_executionF ]}.
  Proof. apply _. Qed.

  Global Instance as_tp_execute_pre_rel j e tl tr E A :
    TCFlag (AsExecutionOf (j ⤇ e -∗ REL tl << tr @ E : A)%I tp_execute_pre_rel e [tele_arg j; E; tl; tr; A]) {[ tp_executionF ]}.
  Proof. apply _. Qed.

  Global Instance load_step_tp (l : loc) v q E :
    TCFlag (ReductionStep (tp_reduction_condition, E) ! #l ⊣ [l ↦ₛ{q} v] ; nclose specN ⊆ E => v ⊣ [Some (l ↦ₛ{q} v)])
          {[ heap_loadF ]}.
  Proof. apply _. Qed.

  Global Instance store_step_tp (l : loc) v e v' E :
    IntoVal e v' →
    TCFlag (ReductionStep (tp_reduction_condition, E) #l <- e ⊣ [l ↦ₛ v] ; nclose specN ⊆ E => #() ⊣ [Some (l ↦ₛ v')%I])
           {[ heap_storeF ]}.
  Proof. apply _. Qed.

  Global Instance alloc_step_tp e v E :
    IntoVal e v →
    TCFlag (ReductionStep (tp_reduction_condition, E) ref e ⊣ [] ; nclose specN ⊆ E => ∃ l : loc, #l ⊣ [Some (l ↦ₛ v)])
           {[ heap_allocF ]}.
  Proof. apply _. Qed.

  Global Instance fork_step_tp e E :
    TCFlag (ReductionStep (tp_reduction_condition, E) Fork e ⊣ [] ; nclose specN ⊆ E => ∃ i : nat, #() ⊣ [Some (i ⤇ e)])
           {[ tp_forkF ]}.
  Proof. apply _. Qed.

  Global Instance pure_step_tp φ n (e : expr) e' E flag:
    TCFlag (PureExec φ n e e') flag →
    TCFlag (ReductionStep (tp_reduction_condition, E) e ⊣ [] ; φ ∧ nclose specN ⊆ E => e' ⊣ []) flag.
  Proof. rewrite /TCFlag. apply _. Qed.

  Global Instance value_step_tp E e1 e2 v1 v2 (A : lrel Σ) :
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    TCFlag (ProofStep REL e1 << e2 @ E : A ⊣ [] => |={E,⊤}=> A v1 v2 ⊣ []) {[ rel_valueF ]}.
  Proof. apply _. Qed.

End ReLocFlagInstances.

Section ReLocFromInstances.

  Context `{!relocG Σ}.

  Global Instance proofstep_rel_bind_l_general (e1 e2 tr : expr) (K : expr → expr) (Φ : val → iProp Σ) A :
    ReshapeExprAnd heap_lang e1 K e2 (SatisfiesContextCondition context_as_item_condition K) →
    ProofStep REL e1 << tr : A ⊣ []; True, WP e2 {{ Φ }} => ∀ v : val, Φ v -∗ REL K v << tr : A ⊣ [] | 10.
  Proof.
    intros [-> [HK]].
    rewrite /ProofStep' /= H !left_id => _.
    rewrite -refines_wp_l.
    apply wp_wand_r.
  Qed.

  Global Instance proofstep_rel_bind_l_no_post (e1 e2 tr : expr) (K : expr → expr) A :
    ReshapeExprAnd heap_lang e1 K e2 (SatisfiesContextCondition context_as_item_condition K) →
    ProofStep REL e1 << tr : A ⊣ []; True, WP e2 {{ v, REL K v << tr : A }} => emp ⊣ [] | 1.
  Proof.
    intros [-> [HK]].
    rewrite /ProofStep' /= H !left_id => _.
    by rewrite -refines_wp_l right_id.
  Qed.

  Global Instance proofstep_rel_bind (tl tr el er : expr) (Kl Kr : expr → expr) E A A' :
    ReshapeExprAnd heap_lang tl Kl el (SatisfiesContextCondition context_as_item_condition Kl) →
    ReshapeExprAnd heap_lang tr Kr er (SatisfiesContextCondition context_as_item_condition Kr) →
    ProofStep REL tl << tr @ E : A' ⊣ []; True, REL el << er @ E : A => ∀ vl vr : val, A vl vr -∗ REL Kl vl << Kr vr : A' ⊣ [].
  Proof.
    intros [-> [HKl]] [-> [HKr]].
    rewrite /ProofStep' /= H H0 !left_id => _.
    apply bi.wand_elim_l', refines_bind.
  Qed.

End ReLocFromInstances.

Section ReLocFlagSets.

  Notation "{[ value ]}" := ({[ value ]} : coPset).
  Notation "{[ x ; y ; .. ; z ]}" := (
  (union .. (union (singleton x) (singleton y)) .. (singleton z)) : coPset).
  Notation "=> value" := ltac:(let raw_set := eval vm_compute in (value : coPset) in exact raw_set) 
                          (at level 0, only parsing).

  Definition pureR_projFS : coPset := => {[ heap_pure_proj1_F; heap_pure_proj2_F; right_executionF ]}.
  Definition pureR_caseFS : coPset := => {[ heap_pure_caseL_F; heap_pure_caseR_F; right_executionF ]}.
  Definition pureR_ifFS : coPset := => {[ heap_pure_ifT_F; heap_pure_ifF_F; right_executionF ]}.
  Definition pureR_injCFS : coPset := => {[ heap_pure_injLC_F; heap_pure_injRC_F; right_executionF ]}.
  Definition pureR_opFS : coPset := => {[ heap_pure_unopF; heap_pure_binopF; heap_pure_eqopF; right_executionF ]}.

  Definition pureR_FS : coPset := =>
     (⊤ ∖ {[ left_executionF; tp_executionF; wp_executionF; heap_loadF; heap_storeF; heap_allocF; tp_forkF ]}) : coPset.

  Definition nosideR_FS : coPset := => 
     (⊤ ∖ {[ left_executionF; tp_executionF; wp_executionF; heap_storeF; heap_allocF; tp_forkF ]}) : coPset.

  Definition right_FS : coPset := => (⊤ ∖ {[ left_executionF; tp_executionF; wp_executionF ]}) : coPset.

  Definition pureL_projFS : coPset := => {[ heap_pure_proj1_F; heap_pure_proj2_F; left_executionF ]}.
  Definition pureL_caseFS : coPset := => {[ heap_pure_caseL_F; heap_pure_caseR_F; left_executionF ]}.
  Definition pureL_ifFS : coPset := => {[ heap_pure_ifT_F; heap_pure_ifF_F; left_executionF ]}.
  Definition pureL_injCFS : coPset := => {[ heap_pure_injLC_F; heap_pure_injRC_F; left_executionF ]}.
  Definition pureL_opFS : coPset := => {[ heap_pure_unopF; heap_pure_binopF; heap_pure_eqopF; left_executionF ]}.

  Definition pureL_FS : coPset := =>
     (⊤ ∖ {[ right_executionF; tp_executionF; wp_executionF; heap_loadF; heap_storeF; heap_allocF; tp_forkF ]}) : coPset.

  Definition nosideL_FS : coPset := => 
     (⊤ ∖ {[ right_executionF; tp_executionF; wp_executionF; heap_storeF; heap_allocF; tp_forkF ]}) : coPset.

  Definition left_FS : coPset := => (⊤ ∖ {[ right_executionF; tp_executionF; wp_executionF ]}) : coPset.

End ReLocFlagSets.

Tactic Notation "iStepFOpen" "as" "(" simple_intropattern_list(xs) ")" constr(k) "⊆" uconstr(flags_ok) := 
    wrap_assert_recv ltac:(iStep_flag_tac_simpl_arg flags_ok k xs).
Tactic Notation "iStepFOpen" "as" "(" simple_intropattern_list(xs) ")" "⊆" uconstr(flags_ok) := 
    wrap_assert_recv ltac:(iStep_flag_tac_simpl_arg flags_ok "" xs).
Tactic Notation "iStepFOpen" "as" constr(k) "⊆" uconstr(flags_ok) := wrap_assert_recv ltac:(iStepF as k ⊆ flags_ok).
Tactic Notation "iStepFOpen" "⊆" uconstr(flags_ok) := wrap_assert_recv ltac:(iStepF ⊆ flags_ok).
Tactic Notation "iStepsFOpen" "⊆" uconstr(flags_ok) := wrap_assert_recv ltac:(iStepsF ⊆ flags_ok).

Tactic Notation "iStepL" "as" "(" simple_intropattern_list(xs) ")" constr(k) := iStep_flag_tac_simpl_arg left_FS k xs.
Tactic Notation "iStepL" "as" "(" simple_intropattern_list(xs) ")" := iStep_flag_tac_simpl_arg left_FS "" xs.
Tactic Notation "iStepL" "as" constr(k) := iStepL as ( ) k.
Tactic Notation "iStepL" := iStepL as "".
Tactic Notation "iStepsL" := repeat iStepL.

Tactic Notation "iStepLOpen" "as" "(" simple_intropattern_list(xs) ")" constr(k) := 
    wrap_assert_recv ltac:(iStep_flag_tac_simpl_arg left_FS k xs).
Tactic Notation "iStepLOpen" "as" "(" simple_intropattern_list(xs) ")" := 
    wrap_assert_recv ltac:(iStep_flag_tac_simpl_arg left_FS "" xs).
Tactic Notation "iStepLOpen" "as" constr(k) := wrap_assert_recv ltac:(iStepL as k).
Tactic Notation "iStepLOpen" := wrap_assert_recv ltac:(iStepL).
Tactic Notation "iStepsLOpen" := wrap_assert_recv ltac:(iStepsL).

Tactic Notation "iStepR" "as" "(" simple_intropattern_list(xs) ")" constr(k) := iStep_flag_tac_simpl_arg right_FS k xs.
Tactic Notation "iStepR" "as" "(" simple_intropattern_list(xs) ")" := iStep_flag_tac_simpl_arg right_FS "" xs.
Tactic Notation "iStepR" "as" constr(k) := iStepR as ( ) k.
Tactic Notation "iStepR" := iStepR as "".
Tactic Notation "iStepsR" := repeat iStepR.

Tactic Notation "iStepROpen" "as" "(" simple_intropattern_list(xs) ")" constr(k) := 
    wrap_assert_recv ltac:(iStep_flag_tac_simpl_arg right_FS k xs).
Tactic Notation "iStepROpen" "as" "(" simple_intropattern_list(xs) ")" := 
    wrap_assert_recv ltac:(iStep_flag_tac_simpl_arg right_FS "" xs).
Tactic Notation "iStepROpen" "as" constr(k) := wrap_assert_recv ltac:(iStepR as k).
Tactic Notation "iStepROpen" := wrap_assert_recv ltac:(iStepR).
Tactic Notation "iStepsROpen" := wrap_assert_recv ltac:(iStepsR).

From iris.heap_lang.lib Require Export spawn par.

Section test_reloc.

  Context `{!spawnG Σ, relocG Σ}.

  Lemma par_comm e1 e2 (A B : lrel Σ) :
    (REL e1 << e1 : A) -∗
    (REL e2 << e2 : B) -∗
    REL (e2 ||| e1)%V << (e1 ||| e2)%V : lrel_true.
  Proof.
    iIntros "He1 He2".
    iStepsFOpen ⊆ (⊤ ∖ {[ left_executionF; tp_forkF ]}).
    iStepR as (i) "Hi".
    iStepsR.
    iRevert "Hi".
    iStep.
    iIntros "Hi".
    iStepOpen.
    iSteps.
    rewrite {3}refines_eq /refines_def.
    iIntros (j K) "#Hs Hj !>".
    tp_bind j e2.
    set (C:=(AppRCtx (λ: "v2", let: "v1" := spawn.join #x in ("v1", "v2")) :: K)).
    iStep with (spawn_spec (nroot.@"par")).
    iSplitL "He2 Hj".
    { rewrite refines_eq /refines_def.
      iStep.
      iApply fupd_wp.
      by iApply "He2". }
    iIntros "!>" (l) "l_hndl".
    iSteps.
    rewrite refines_eq /refines_def.
    tp_bind i e1.
    iMod ("He1" with "Hs Hi") as "He1".
    iStep with "He1".
    iDestruct 1 as (v2) "[Hi Hv]".
    iSteps.
    iStep with join_spec.
    iIntros "!>". iDestruct 1 as (w2) "[Hj Hw]".
    unfold C. iSimpl in "Hj".
    iRevert "Hs Hi".
    rewrite bi.intuitionistically_elim.
    iSteps.
    iIntros "Hs _".
    iRevert "Hs Hj".
    iStepsOpen.
    iIntros "_ Hj !>".
    iSteps.
    iExists _; eauto.
  Qed.

  Lemma par_unit_1 e A :
    (REL e << e : A) -∗
    REL (#() ||| e)%V << e : lrel_true.
  Proof.
    iIntros "He".
    iStepLOpen.
    iStepsF ⊆ pureL_FS.
    iStep with (spawn_spec (nroot.@"par") (λ v, True)%I).
    iSplitR; first by iSteps.
    iIntros "!>" (l) "hndl".
    iStepsF ⊆ pureL_FS.
    iStep as "Hv" with "He".
    iStepsL.
    iStep as "!>" with join_spec.
    by iSteps.
  Qed.

  Lemma par_unit_2 e A :
    (REL e << e : A) -∗
    REL e << (#() ||| e)%V : lrel_true.
  Proof.
    iIntros "H".
    iStepsFOpen ⊆ (⊤ ∖ {[ tp_forkF ]}).
    iStep as (i) "Hi".
    iSteps.
    iRevert "Hi".
    iSteps.
    iIntros "_".
    iStep as "Hv" with "H".
    by iStepsOpen.
  Qed.

End test_reloc.


























