From iris.bi Require Export bi telescopes.
From iris.proofmode Require Import base environments coq_tactics.

Import bi.
Import env_notations.

Class ProofStep' {PROP : bi} {TT}
    (P Q : PROP) (φ : Prop) (Qs : list PROP) (n : nat)
    (Pout : TT -t> PROP) (mQsout : TT -t> list (option PROP)) :=
  proof_step : φ → Q ∗ ▷^n (
      [∗] Qs ∗ (∀.. (xs : TT), 
        ([∗ list] mQ ∈ (tele_app mQsout xs), default emp mQ) -∗ (tele_app Pout xs))
    ) ⊢ P.
Global Hint Mode ProofStep' + - ! - - - - - - : typeclass_instances.

Fixpoint envs_lookup_list {PROP} (rp : bool) (js : list ident) (Δ : envs PROP) :=
  match js return option (list bool * list PROP) with
  | [] => Some ([], [])
  | j :: js =>
     '(p,P,Δ') ← envs_lookup_delete rp j Δ;
     '(ps,Ps) ← envs_lookup_list rp js Δ';
     Some (p :: ps, P :: Ps)
  end.

Fixpoint envs_delete_list {PROP} (rp : bool) (js : list ident) (ps : list bool) Δ :=
  match js, ps return envs PROP with
  | j :: js, p :: ps =>
     envs_delete_list rp js ps
       (envs_delete rp j p Δ)
  | _, _ => Δ
  end.

Fixpoint envs_proof_step_replace {PROP : bi}
    (ps : list bool) (js ks : list ident) (mQs : list (option PROP))
    (Δ : envs PROP) : option (envs PROP) :=
  match mQs, ps, js with
  | [], _, _ =>
     (* Remove non-persistent consumed hypotheses [js] *)
     Some (envs_delete_list false js ps Δ)
  | None :: mQs, p :: ps, j :: js =>
     (* Remove consumed hypothesis [j] if it is not persistent *)
     envs_proof_step_replace ps js ks mQs (envs_delete false j p Δ)
  | Some Q :: mQs, p :: ps, j :: js =>
     (* Replace consumed hypothesis [j] with produced [Q] *)
      if p then ( 
     (* [j] is persistent, so we do not consume it, and append Q to the environment. *)
     (* We append after replacing, since js might contain the new label. *)
     (* If that happens, the added hypothesis would be deleted, which is unwanted. *)
     (* Before we continue, we consume a hypthosesis name k, if those are still present. *)
        Δ' ← envs_proof_step_replace ps js (tail ks) mQs Δ;
        envs_add_fresh false (hd_error ks) Q Δ'
      ) else (
        Δ' ← envs_replace j false false (Esnoc Enil j Q) Δ;
        envs_proof_step_replace ps js ks mQs Δ'
      )
  | None :: mQs, [], [] =>
     (* Ignore spurious [None] in the list of produced hypotheses *)
     envs_proof_step_replace ps js ks mQs Δ
  | Some Q :: mQs, [], [] =>
     (* Handle produced hypothesis that did not consume one *)
     Δ' ← envs_add_fresh false (hd_error ks) Q Δ;
     envs_proof_step_replace [] [] (tail ks) mQs Δ'
  | _, _, _ => None
  end.

Class FromAssumptionIStep {PROP : bi} (p : bool) (P Q : PROP) :=
from_assumption_istep : FromAssumption p P Q.
Arguments FromAssumptionIStep {_} _ _%I _%I : simpl never.
Arguments from_assumption_istep {_} _ _%I _%I {_}.
Global Hint Mode FromAssumptionIStep + + - ! : typeclass_instances. (* The Q can be an evar, but should at least have some shape *)

Global Instance fromassumption_to_istep {PROP : bi}  p (P Q : PROP) : FromAssumption p P Q → FromAssumptionIStep p P Q.
Proof. by rewrite /FromAssumptionIStep. Qed.

Section environment_facts.
  Context {PROP : bi}.
  Implicit Types Δ : envs PROP.
  Implicit Types Γ : env PROP.

  Ltac destruct_option_tac A E_name :=
    let rec rewrite_until_goal old_goal :=
      match goal with
      | |- ?a = ?b -> old_goal => (move => <- || move => -> || idtac)
      | |- ?a = ?b -> ?G => (move => <- || move => ->); rewrite_until_goal old_goal
      | |- _ => idtac
      end
    in
    let rec destruct_opt mA old_goal :=
      lazymatch mA with
      | pm_option_bind ?p ?mA' =>
        destruct_opt mA' old_goal
      | (match ?mA' with _ => _ end) =>
        destruct_opt mA' old_goal
      | _ => 
        let E := fresh "E" in
        case E : mA => [A|] /= ;
        revert E; move => E_name //;
        (* // drops the nonsense branch *)
        lazymatch goal with
          | |- Some ?A = Some ?B -> ?P => case
          | |- _ => idtac 
        end; (* simplifies equalities of Some a = Some b to a = b, ignoring goals where this won't work *)
        rewrite_until_goal old_goal
      end
    in
    lazymatch goal with
    | H : pm_option_bind ?p ?mA = ?mB |- ?P =>
      revert H; destruct_option_tac A E_name
    | |- pm_option_bind ?p ?mA = ?mB -> ?P =>
      destruct_opt mA P
    end.

  Tactic Notation "destruct_option" "as" simple_intropattern(A) simple_intropattern (H) :=
      destruct_option_tac A H.
  Tactic Notation "destruct_option" "as" simple_intropattern(A) := 
      let H := fresh "H" in destruct_option as A H.
  Tactic Notation "destruct_option" := let x := fresh "x" in destruct_option as x.

  Proposition ident_beq_ind (P : bool → Prop) i j:
    (i = j → P true) → (i ≠ j → ident_beq i j = false → P false) → P (ident_beq i j).
  Proof.
    move => Htrue Hfalse.
    case E : (ident_beq _ _).
    - by apply Htrue, ident_beq_true.
    - apply Hfalse, E. move /ident_beq_true => Hij.
      move: Hij; rewrite E //.
  Qed.

  Proposition ident_beq_refl i : ident_beq i i = true.
  Proof. by apply ident_beq_true. Qed.

  Lemma envs_lookup_list_nil rp Δ : envs_lookup_list rp [] Δ = Some ([], []).
  Proof. done. Qed.
  Lemma envs_lookup_list_cons rp j js p ps P Ps Δ Δ' :
    envs_lookup_delete rp j Δ = Some (p, P, Δ') →
    envs_lookup_list rp js Δ' = Some (ps, Ps) →
    envs_lookup_list rp (j :: js) Δ = Some (p :: ps, P :: Ps).
  Proof. by move=> /= -> /= ->. Qed.

  Lemma envs_lookup_list_correct Δ rp js ps Ps :
    envs_lookup_list rp js Δ = Some (ps, Ps) →
    of_envs Δ ⊢ ([∗] Ps ∗ of_envs (envs_delete_list rp js ps Δ)) ∧ ⌜envs_wf Δ⌝.
  Proof.
    move => H; apply: and_intro.
    * revert Δ ps Ps H. induction js as [|j js IH]=> Δ ps Ps /=;
      first (case => _ <- /=; by rewrite left_id).
      destruct_option as [[p P] Δ''] HΔ.
      destruct_option as [ps' Ps'] H.
      move /envs_lookup_delete_Some : HΔ H => [? ->] /= H.
      by rewrite envs_lookup_sound' // intuitionistically_if_elim IH // assoc.
    * rewrite of_envs_eq.
      apply and_elim_l.
  Qed.

  Lemma envs_lookup_list_cons_decompose' Δ rp j js ps Ps :
    envs_lookup_list rp (j :: js) Δ = Some (ps, Ps) →
        (p ← hd_error ps;
          envs_lookup_list rp js (envs_delete rp j p Δ)
        ) = Some (tail ps, tail Ps)
      ∧ (default False (
          p ← hd_error ps; P ← hd_error Ps; 
          Some (envs_lookup j Δ = Some (p, P)))
        ).
  Proof.
    move => /= H.
    destruct_option as [[p' P] Δ'] HΔ'.
    destruct_option as [ps' Ps'] => /=.
    move /envs_lookup_delete_Some: HΔ' => [-> <-] //.
  Qed.

  Lemma envs_lookup_list_cons_decompose Δ rp j js ps Ps :
    envs_lookup_list rp (j :: js) Δ = Some (ps, Ps) →
      (p ← hd_error ps;
        envs_lookup_list rp js (envs_delete rp j p Δ)
      ) = Some (tail ps, tail Ps).
  Proof. apply envs_lookup_list_cons_decompose'. Qed.

  Lemma envs_delete_wf Δ p j q : envs_wf Δ → envs_wf (envs_delete p j q Δ).
  Proof.
    move => Hwf; move: p q.
    wlog suff: /(∀ p, envs_wf (envs_delete p j p Δ)).
    - move => H [|] [|]; auto.
      * by rewrite -envs_delete_spatial.
      * by rewrite envs_delete_intuitionistic.
    - case: Δ Hwf => [Γp Γs n] [/=HΓp HΓs HΓps]; case.
      all: split => /=; auto using env_delete_wf => i.
      all: case: (HΓps i); auto using env_delete_fresh.
  Qed.

  Instance env_delete_comm Γ : Comm eq (λ i j, env_delete i (env_delete j Γ)).
  Proof.
    move => /=; induction Γ as [|Γ IH j P] => //= k l.
    elim/ident_beq_ind : _ => [-> | _ Hi_lj] /=;
    elim/ident_beq_ind : _ => [ // -> | _ Hi_kj] //=.
    - by rewrite ident_beq_refl //.
    - by rewrite Hi_lj IH.
  Qed.

  Lemma envs_delete_comm Δ r1 r2 i j p q : 
    envs_delete r1 i p (envs_delete r2 j q Δ) = envs_delete r2 j q (envs_delete r1 i p Δ).
  Proof.
    case: Δ p q r1 r2 => [Γp Γs n] [|] [|] [|] [|] //=; rewrite env_delete_comm //.
  Qed.

  Instance ident_beq_comm : Comm eq ident_beq.
  Proof.
    move => i j.
    elim /ident_beq_ind : _ => [-> | Hij Hi_ij].
    - by rewrite ident_beq_refl.
    - elim /ident_beq_ind : _ => //.
  Qed.

  Lemma env_replace_wf_singleton (Γ Γ' : env PROP) j Q :
    env_wf Γ → env_replace j (Esnoc Enil j Q) Γ = Some Γ' → env_wf Γ'.
  Proof.
    move => H1 H2;
    apply: env_replace_wf => //.
    by apply env_delete_wf.
  Qed.

  Lemma envs_replace_wf_singleton Δ Δ' p j Q : 
     envs_wf Δ → envs_replace j p p (Esnoc Enil j Q) Δ = Some Δ' → envs_wf Δ'.
  Proof.
    case: p Δ => [|] [Γp Γs n] [/=HΓp HΓs HΓps];
    rewrite /envs_replace /=;
    destruct_option;
    destruct_option as Γ' HΓ'; split => //=;
    try (by apply: env_replace_wf_singleton HΓ'); move => i.
    all: elim /ident_beq_ind : (ident_beq i j) => [-> | Hij Hi_ij]; auto.
    all: case: (HΓps i) => Hi; auto.
    1: left. 2: right.
    all: apply: env_replace_fresh => //=; try by rewrite Hi_ij.
    all: apply env_delete_fresh => //.
  Qed.

  Lemma envs_lookup_sound_2' Δ rp i p P :
    envs_wf Δ → 
    envs_lookup i Δ = Some (p, P) → 
    □?p P ∗ of_envs (envs_delete rp i p Δ) -∗ of_envs Δ.
  Proof.
    case: rp; [ by apply envs_lookup_sound_2 | ].
    case: p => [|].
    - rewrite envs_delete_intuitionistic => _ _ /=.
      by rewrite intuitionistically_elim_emp left_id.
    - rewrite envs_delete_spatial.
      apply envs_lookup_sound_2.
  Qed.

  Proposition envs_lookup_envs_delete_ne Δ i j rp q: 
    i ≠ j →
    envs_lookup j (envs_delete rp i q Δ) = envs_lookup j Δ.
  Proof.
    move => Hij.
    wlog: rp /(rp = true).
    - case: rp; auto.
      case: q => Hrp.
      * by rewrite envs_delete_intuitionistic.
      * by rewrite envs_delete_spatial; apply Hrp.
    - move => ->.
      case: q Δ => [|] [Γp Γs n] /=.
      * rewrite env_lookup_env_delete_ne //.
      * rewrite env_lookup_env_delete_ne //.
  Qed.

  Proposition envs_lookup_envs_delete Δ j q:
    envs_wf Δ →
    (∃ Q, envs_lookup j Δ = Some (q, Q)) → 
    envs_lookup j (envs_delete true j q Δ) = None.
  Proof.
    case: Δ q => [Γp Γs n] [|] [/=HΓp HΓs HΓps];
    destruct (Γp !! j) as [P|] eqn:HP; case => [Q HQ]; revert HQ; try by destruct_option.
    - rewrite env_lookup_env_delete //.
      case: (HΓps j) => //.
      * rewrite HP //.
      * move => -> //.
    - case => //.
    - destruct_option as P => HQ.
      rewrite env_lookup_env_delete //.
  Qed.

  Proposition envs_lookup_delete_correct Δ j rp :
    envs_lookup_delete rp j Δ = '(p, P) ← envs_lookup j Δ; Some (p, P, envs_delete rp j p Δ).
  Proof.
    move: Δ => [Γp Γs n] /=.
    rewrite !env_lookup_delete_correct.
    case: (Γp !! j) => [P|] //=.
    case: (Γs !! j) => [P|] //=.
  Qed.

  Lemma envs_lookup_list_envs_delete rp1 rp2 j p js Δ :
    Forall (λ i, i ≠ j) js →
    envs_lookup_list rp1 js (envs_delete rp2 j p Δ) = envs_lookup_list rp1 js Δ.
  Proof.
    move: rp1.
    wlog: rp2 /(rp2 = true).
    { case: p rp2 => [|] [|] Hprp rp; try (by apply: Hprp).
      - by rewrite envs_delete_intuitionistic.
      - by rewrite envs_delete_spatial; apply: Hprp. }
    move => -> {rp2}.
    revert Δ.
    induction js as [|i js IH] => //= Δ rp Hjs.
    inversion Hjs; subst => {Hjs}.
    rewrite !envs_lookup_delete_correct.
    rewrite envs_lookup_envs_delete_ne //.
    case HΔi : (envs_lookup i Δ) => [[q Q]|] //=.
    rewrite envs_delete_comm IH //.
  Qed.

  Lemma envs_delete_list_envs_delete rp1 rp2 j js ps Δ p P:
    Forall (λ i, i ≠ j) js →
    envs_wf Δ →
    envs_lookup j Δ = Some (p, P) → 
    of_envs (envs_delete_list rp1 js ps (envs_delete rp2 j p Δ)) -∗ □?p P -∗ of_envs (envs_delete_list rp1 js ps Δ).
  Proof.
    wlog: rp2 /(rp2 = true).
    { move => Hprp Hjs Hwf Hj.
      destruct p; destruct rp2; auto.
      - rewrite envs_delete_intuitionistic => /=.
        by rewrite intuitionistically_elim_emp left_id. 
      - rewrite envs_delete_spatial; auto. }
    move => -> {rp2}.
    move: Δ ps.
    induction js as [|i js IHjs] => Δ [| q ps] Hjs Hwf HΔj //=; 
      try by apply wand_intro_l, envs_lookup_sound_2'.
    inversion Hjs; subst => {Hjs}.
    rewrite envs_delete_comm.
    apply: IHjs => //.
    - by apply envs_delete_wf.
    - rewrite envs_lookup_envs_delete_ne //.
  Qed.

  Lemma envs_lookup_list_envs_delete_js_ne rp1 rp2 j js Δ p ps Ps:
    envs_wf Δ →
    rp2 = true ∨ p = false →
    (∃ P, envs_lookup j Δ = Some (p, P)) → 
    envs_lookup_list rp1 js (envs_delete rp2 j p Δ) = Some (ps, Ps) →
    Forall (λ i, i ≠ j) js.
  Proof.
    wlog: rp2 /(rp2 = true).
    { case: p rp2 => [|] [|] => Hprp Hwf [Hrp|Hp] //; try (by apply: Hprp; auto).
      - by rewrite envs_delete_spatial; apply: Hprp; auto. }
    move => -> {rp2} + _ + +.
    move: Δ ps Ps.
    induction js as [|i js IH] => // Δ ps Ps Hwf Hj /envs_lookup_list_cons_decompose' [Hjs1 Hij].
    revert Hij.
    destruct_option as q => Hjs.
    rewrite envs_delete_comm in Hjs => Hij.
    enough (i ≠ j).
    - constructor => //. 
      apply: IH; [ by apply envs_delete_wf | | done].
      rewrite envs_lookup_envs_delete_ne //.
    - move: Hij. destruct (hd_error Ps) as [Q|] eqn:HQ => //= Hij.
      contradict Hij.
      rewrite Hij.
      erewrite envs_lookup_envs_delete => //.
  Qed.

  Lemma env_delete_env_replace Γ Γ' j P:
    env_replace j (Esnoc Enil j P) Γ = Some Γ' →
    env_delete j Γ' = env_delete j Γ.
  Proof.
    move: Γ'.
    induction Γ as [|Γ IHΓ i Q] => Γ' //=.
    rewrite [ident_beq _ _]comm.
    elim /ident_beq_ind : _ => [_| Hij Hi_ij].
    - case (Γ !! j) => //; case => <- /=.
      by rewrite ident_beq_refl.
    - destruct_option as Γ'' => /=.
      rewrite [ident_beq _ _]comm Hi_ij.
      erewrite IHΓ => //.
  Qed.

  Lemma envs_delete_envs_replace Δ Δ' j p P:
    envs_replace j p p (Esnoc Enil j P) Δ = Some Δ' →
    envs_delete true j p Δ' = envs_delete true j p Δ.
  Proof.
    case: Δ p Δ' => [Γp Γs n] [|] Δ'.
    all: rewrite /envs_replace /envs_delete /=.
    all: destruct_option; destruct_option as Γ' HΓ'.
    all: erewrite env_delete_env_replace => //.
  Qed.

  Lemma envs_delete_envs_replace_spatial Δ Δ' j P:
    envs_replace j false false (Esnoc Enil j P) Δ = Some Δ' →
    envs_delete false j false Δ' = envs_delete false j false Δ.
  Proof.
    rewrite !envs_delete_spatial.
    apply envs_delete_envs_replace.
  Qed.

  Lemma env_lookup_env_replace_singleton Γ Γ' i P :
    env_replace i (Esnoc Enil i P) Γ = Some Γ' →
    Γ' !! i = Some P.
  Proof.
    move: Γ'.
    induction Γ as [|Γ' IHΓ' j Q] => Γ //=.
    rewrite [ident_beq j i]comm.
    elim/ident_beq_ind : _ => [->|Hi Hi_ij].
    - destruct (Γ' !! j) => //; case => <- /=.
      by rewrite ident_beq_refl.
    - destruct_option as Γ'' => /=.
      rewrite Hi_ij IHΓ' //.
  Qed.

  Lemma envs_lookup_envs_replace_singleton Δ Δ' j p P :
    envs_replace j p p (Esnoc Enil j P) Δ = Some Δ' →
    envs_lookup j Δ' = Some (p, P).
  Proof.
    case: Δ p => [Γp Γs n] [|];
    rewrite /envs_replace /envs_simple_replace /envs_lookup /=;
    destruct_option as ? H;
    destruct_option as Γ'; [ | rewrite H];
    erewrite env_lookup_env_replace_singleton => //.
  Qed.

  Lemma envs_replace_lookup_Some Δ Δi Δ' i p:
    envs_wf Δ →
    envs_replace i p p Δi Δ = Some Δ' →
    ∃ Q, envs_lookup i Δ = Some (p, Q).
  Proof.
    case: Δ p => [Γp Γs n] [|] Hwf;
    rewrite /envs_replace /envs_simple_replace /envs_lookup /=;
    destruct_option as Γ1' HΓ1';
    destruct_option as Γ2' HΓ2' => _; move: HΓ2';
    move /env_replace_lookup => [Q HQ];
    exists Q; revert HQ.
    - move => -> //.
    - destruct Hwf as [_ _ HΓps].
      case : (HΓps i) => /= [-> -> //|-> //].
  Qed.

  Lemma envs_proof_step_replace_sound_base Δ js ks ps Ps :
    envs_lookup_list false js Δ = Some (ps, Ps) →
    envs_wf Δ →
    of_envs (envs_delete_list false js ps Δ) ⊢ ∀ mQs Δ',
      ⌜ envs_proof_step_replace ps js ks mQs Δ = Some Δ' ⌝ →
      ([∗ list] mQ ∈ mQs, default emp mQ) -∗ of_envs Δ'.
  Proof.
    intros Hjs Hwf.
    apply forall_intro=> mQs; apply forall_intro=> Δ'.
    apply impl_intro_l, pure_elim_l=> HΔ'. apply wand_intro_r.
    revert mQs ps ks Δ Δ' HΔ' Ps Hjs Hwf.
    induction js as [|j js IH]; simplify_eq/=.
    - intros mQs. induction mQs as [|[Q|] Qs IH]; simplify_eq/=.
      + intros ? ? Δ Δ' [= <-] _ _ _. by rewrite right_id.
      + intros [|p ps] ks Δ Δ' ? _ _ ? => //.
        destruct_option as Δ'' HΔ'' => HΔ'.
        rewrite envs_add_fresh_sound // => /=.
        rewrite assoc wand_elim_l IH //.
        apply: envs_add_fresh_wf => //.
      + intros [|p ps] ks Δ Δ' ? _ _ ? => //. rewrite left_id IH //.
    - case => [|[Q|] mQs] [|p ps] ks Δ Δ' + Ps /envs_lookup_list_cons_decompose /=Hjs Hwf //= => /=.
      + case => ->. by rewrite right_id.
      + destruct p.
        * destruct_option as Δ'' HΔ'' => HΔ'.
          rewrite [(Q ∗ _)%I]comm assoc envs_delete_intuitionistic.
          rewrite envs_delete_intuitionistic in Hjs.
          rewrite IH //.
          apply wand_elim_l'.
          by rewrite envs_add_fresh_sound //.
        * destruct_option as Δ'' HΔ'' => /IH IH' {IH}.
          assert (Forall (λ i, i ≠ j) js).
          { apply: envs_lookup_list_envs_delete_js_ne; last done; auto.
            apply: envs_replace_lookup_Some => //. }
          rewrite assoc; revert IH' Hjs.
          erewrite <-envs_delete_envs_replace_spatial => //.
          rewrite envs_lookup_list_envs_delete // => IH' /IH' <- {IH'};
            [ | by apply: envs_replace_wf_singleton ].
          apply sep_mono_l.
          rewrite envs_delete_list_envs_delete;
           eauto using envs_replace_wf_singleton, envs_lookup_envs_replace_singleton, wand_elim_l.
      + rewrite left_id. move /IH => -> //.
        by apply envs_delete_wf.
  Qed.

  Lemma envs_proof_step_replace_sound Δ js ks ps Ps :
    envs_lookup_list false js Δ = Some (ps, Ps) →
    of_envs Δ ⊢ [∗] Ps ∗ ∀ mQs Δ',
      ⌜ envs_proof_step_replace ps js ks mQs Δ = Some Δ' ⌝ →
      ([∗ list] mQ ∈ mQs, default emp mQ) -∗ of_envs Δ'.
  Proof.
    intros Hjs. rewrite envs_lookup_list_correct //.
    apply pure_elim_r => Hwf. apply sep_mono_r.
    apply: envs_proof_step_replace_sound_base => //.
  Qed.

  Lemma tac_proof_step_general {TT} Δ Δ'' Δ' Q js ks ps P (φ : Prop) Qs n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    ((∃ k p, envs_lookup_delete false k Δ = Some (p, Q, Δ'')) ∨ (Q = emp%I ∧ Δ'' = Δ)) →
    ProofStep' P Q φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ'' Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs) →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof.
    rewrite /ProofStep' envs_entails_eq => HQ Hps Hlater Hlk Hφ.
    rewrite -Hps; [ | apply Hφ].
    move => /tforall_forall HΔ.
    enough (of_envs Δ'' ⊢ ▷^n ([∗] Qs
          ∗ (∀.. xs : TT, ([∗ list] mQ ∈ tele_app Qsout xs, default emp mQ) -∗ tele_app Pout xs))) => [|{HQ}].
    { case: HQ => [[k [p HQ]]|[-> <-]].
      - rewrite envs_lookup_delete_sound //.
        rewrite intuitionistically_if_elim.
        apply sep_mono_r => //.
      - by rewrite left_id. }
    rewrite coq_tactics.into_laterN_env_sound.
    apply laterN_mono.
    rewrite envs_proof_step_replace_sound //. apply bi.sep_mono_r.
    rewrite bi_tforall_forall. apply bi.forall_intro=> xs.
    destruct (HΔ xs) as (Δ''' & ? & <-).
    rewrite (bi.forall_elim (tele_app Qsout xs)) (bi.forall_elim Δ''').
    by rewrite bi.pure_True // bi.True_impl.
  Qed.

  Lemma tac_proof_step_from_env {TT} Δ Δ'' Δ' k p Q js ks ps P (φ : Prop) Qs n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    envs_lookup_delete false k Δ = Some (p, Q, Δ'') →
    ProofStep' P Q φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ'' Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs) →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof. move => HΔ'; apply: tac_proof_step_general; left. by exists k; exists p. Qed.

  Lemma tac_proof_step_emp {TT} Δ Δ' js ks ps P (φ : Prop) Qs n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    ProofStep' P emp%I φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs) →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof. apply: tac_proof_step_general; by right. Qed.

  Lemma tac_proof_step_from_result {TT} Δ Δ' Q js ks ps P (φ : Prop) Qs n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    (⊢ Q) →
    ProofStep' P Q φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs) →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof.
    move => HQ HPs. apply: tac_proof_step_emp.
    move: HPs; rewrite /ProofStep' -HQ //.
  Qed.

  Definition FromAssumptions ps (Ps Ps' : list PROP) := Forall2 apply (zip_with FromAssumptionIStep ps Ps) Ps'.

  Lemma envs_lookup_list_fromasmp_nil rp Δ : envs_lookup_list rp [] Δ = Some ([], []) ∧
                FromAssumptions [] [] [].
  Proof. split => //. by apply Forall2_nil. Qed.
  Lemma envs_lookup_list_fromasmp_cons rp j js p ps P Ps P' Ps' Δ Δ' :
    envs_lookup_delete rp j Δ = Some (p, P, Δ') ∧ FromAssumptionIStep p P P' →
    envs_lookup_list rp js Δ' = Some (ps, Ps) ∧ FromAssumptions ps Ps Ps' →
    envs_lookup_list rp (j :: js) Δ = Some (p :: ps, P :: Ps) ∧ FromAssumptions (p :: ps) (P :: Ps) (P' :: Ps').
  Proof. move=> /= [-> HP] /= [-> HPs] /=; split => //. apply Forall2_cons => //=. Qed.

  Lemma envs_lookup_list_from_asmp_correct rp Δ js ps Ps Ps' :
    envs_lookup_list rp js Δ = Some (ps, Ps) ∧ FromAssumptions ps Ps Ps' →
    of_envs Δ -∗ ([∗] Ps' ∗ of_envs (envs_delete_list rp js ps Δ)) ∧ ⌜envs_wf Δ⌝.
  Proof.
    move => H. apply: and_intro. 
    - revert Δ ps Ps Ps' H.
      induction js as [|j js IH] => Δ ps Ps Ps' /=.
      * case => [[_ <-] HPs] /=; inversion HPs; subst;
        first (by rewrite /= left_id).
        by rewrite zip_with_nil_r in H.
      * move => [+ +].
        destruct_option as [[p P] Δ''] HΔ.
        destruct_option as [ps' Ps''] H.
        move /envs_lookup_delete_Some : HΔ H => [Hj ->] /= H HPs.
        inversion HPs as [|P'' P' Ps'''' Ps''']; subst.
        rewrite /FromAssumptions in HPs.
        rewrite envs_lookup_sound' //.
        rewrite H0 /= -assoc.
        apply sep_mono_r.
        eapply IH; split => //.
    - rewrite of_envs_eq.
      apply and_elim_l.
  Qed.

  Lemma envs_proof_step_replace_sound_from_asmp Δ js ks ps Ps Ps' :
    envs_lookup_list false js Δ = Some (ps, Ps) ∧ FromAssumptions ps Ps Ps' →
    of_envs Δ ⊢ [∗] Ps' ∗ ∀ mQs Δ',
      ⌜ envs_proof_step_replace ps js ks mQs Δ = Some Δ' ⌝ →
      ([∗ list] mQ ∈ mQs, default emp mQ) -∗ of_envs Δ'.
  Proof.
    intros [Hjs Hasm]. rewrite envs_lookup_list_from_asmp_correct //.
    apply pure_elim_r => Hwf. apply sep_mono_r.
    apply: envs_proof_step_replace_sound_base => //.
  Qed.

  Lemma tac_proof_step_asmp_general {TT} Δ Δ'' Δ' Q js ks ps P (φ : Prop) Qs Qs' n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    ((∃ k p, envs_lookup_delete false k Δ = Some (p, Q, Δ'')) ∨ (Q = emp%I ∧ Δ'' = Δ)) →
    ProofStep' P Q φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ'' Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs') ∧ FromAssumptions ps Qs' Qs →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof.
    rewrite /ProofStep' envs_entails_eq => HQ Hps Hlater Hlk Hφ.
    rewrite -Hps; [ | apply Hφ].
    move => /tforall_forall HΔ.
    enough (of_envs Δ'' ⊢ ▷^n ([∗] Qs
          ∗ (∀.. xs : TT, ([∗ list] mQ ∈ tele_app Qsout xs, default emp mQ) -∗ tele_app Pout xs))) => [|{HQ}].
    { case: HQ => [[k [p HQ]]|[-> <-]].
      - rewrite envs_lookup_delete_sound //.
        rewrite intuitionistically_if_elim.
        apply sep_mono_r => //.
      - by rewrite left_id. }
    rewrite coq_tactics.into_laterN_env_sound.
    apply laterN_mono.
    rewrite envs_proof_step_replace_sound_from_asmp //. apply bi.sep_mono_r.
    rewrite bi_tforall_forall. apply bi.forall_intro=> xs.
    destruct (HΔ xs) as (Δ''' & ? & <-).
    rewrite (bi.forall_elim (tele_app Qsout xs)) (bi.forall_elim Δ''').
    by rewrite bi.pure_True // bi.True_impl.
  Qed.

  Lemma tac_proof_step_asmp_from_env {TT} Δ Δ'' Δ' k p Q js ks ps P (φ : Prop) Qs Qs' n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    envs_lookup_delete false k Δ = Some (p, Q, Δ'') →
    ProofStep' P Q φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ'' Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs') ∧ FromAssumptions ps Qs' Qs →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof. move => HΔ'; apply: tac_proof_step_asmp_general; left. by exists k; exists p. Qed.

  Lemma tac_proof_step_asmp_emp {TT} Δ Δ' js ks ps P (φ : Prop) Qs Qs' n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    ProofStep' P emp%I φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs') ∧ FromAssumptions ps Qs' Qs →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof. apply: tac_proof_step_asmp_general; by right. Qed.

  Lemma tac_proof_step_asmp_from_result {TT} Δ Δ' Q js ks ps P (φ : Prop) Qs Qs' n
      (Pout : TT -t> PROP) (Qsout : TT -t> list (option PROP)) :
    (⊢ Q) →
    ProofStep' P Q φ Qs n Pout Qsout →
    coq_tactics.MaybeIntoLaterNEnvs n Δ Δ' →
    envs_lookup_list false js Δ' = Some (ps, Qs') ∧ FromAssumptions ps Qs' Qs →
    φ →
    (∀.. xs, ∃ Δ'',
      envs_proof_step_replace ps js ks (tele_app Qsout xs) Δ' = Some Δ'' ∧
      envs_entails Δ'' (tele_app Pout xs)) →
    envs_entails Δ P.
  Proof.
    move => HQ HPs. apply: tac_proof_step_asmp_emp.
    move: HPs; rewrite /ProofStep' -HQ //.
  Qed.

End environment_facts.

Notation "'ProofStep' P ⊣ Qs ; φ , Q =[ ▷^ n ]=> ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     Q%I
     φ
     Qs%I
     n
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, n at level 9, x1 binder, xn binder, P, Qs, φ, Q, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ ,  Q  =[ ▷^ n ]=>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ , Q =[▷]=> ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     Q%I
     φ
     Qs%I
     1
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, x1 binder, xn binder, P, Qs, φ, Q, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ ,  Q  =[▷]=>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ , Q => ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     Q%I
     φ
     Qs%I
     0
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, x1 binder, xn binder, P, Qs, φ, Q, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ ,  Q  =>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ =[ ▷^ n ]=> ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     emp%I
     φ
     Qs%I
     n
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, n at level 9, x1 binder, xn binder, P, Qs, φ, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ  =[ ▷^ n ]=>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ =[▷]=> ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     emp%I
     φ
     Qs%I
     1
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, x1 binder, xn binder, P, Qs, φ, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ  =[▷]=>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ => ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     emp%I
     φ
     Qs%I
     0
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, x1 binder, xn binder, P, Qs, φ, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ  =>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs =[ ▷^ n ]=> ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     emp%I
     True
     Qs%I
     n
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, n at level 9, x1 binder, xn binder, P, Qs, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  =[ ▷^ n ]=>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs =[▷]=> ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     emp%I
     True
     Qs%I
     1
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, x1 binder, xn binder, P, Qs, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  =[▷]=>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs => ∃ x1 .. xn , Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleS (λ x1, .. (λ xn, TeleO) .. ))
     P%I
     emp%I
     True
     Qs%I
     0
     (λ x1, .. (λ xn , Pout%I) ..)
     (λ x1, .. (λ xn , mQsout%I) ..))
  (at level 20, x1 binder, xn binder, P, Qs, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  =>  ∃  x1 .. xn  ,  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ , Q =[ ▷^ n ]=> Pout ⊣ mQsout" :=
  (ProofStep'
     (TT:=TeleO)
     P%I
     Q%I
     φ
     Qs%I
     n
     Pout%I
     mQsout%I)
  (at level 20, n at level 9, P, Qs, φ, Q, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ ,  Q  =[ ▷^ n ]=>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ , Q =[▷]=> Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; φ, Q =[▷^1]=> Pout ⊣ mQsout)
  (at level 20, P, Qs, φ, Q, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ ,  Q  =[▷]=>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ , Q => Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; φ, Q =[▷^0]=> Pout ⊣ mQsout)
  (at level 20, P, Qs, φ, Q, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ ,  Q  =>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ =[ ▷^ n ]=> Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; φ, emp =[▷^n]=> Pout ⊣ mQsout)
  (at level 20, n at level 9, P, Qs, φ, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ  =[ ▷^ n ]=>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ =[▷]=> Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; φ, emp =[▷]=> Pout ⊣ mQsout)
  (at level 20, P, Qs, φ, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ  =[▷]=>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs ; φ => Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; φ, emp => Pout ⊣ mQsout)
  (at level 20, P, Qs, φ, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  ;  φ  =>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs =[ ▷^ n ]=> Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; True =[▷^n]=> Pout ⊣ mQsout)
  (at level 20, n at level 9, P, Qs, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  =[ ▷^ n ]=>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs =[▷]=> Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; True =[▷]=> Pout ⊣ mQsout)
  (at level 20, P, Qs, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  =[▷]=>  Pout  ⊣  mQsout"
).

Notation "'ProofStep' P ⊣ Qs => Pout ⊣ mQsout" :=
  (ProofStep P ⊣ Qs ; True => Pout ⊣ mQsout)
  (at level 20, P, Qs, Pout, mQsout at level 200, format
  "'ProofStep'  P  ⊣  Qs  =>  Pout  ⊣  mQsout"
).