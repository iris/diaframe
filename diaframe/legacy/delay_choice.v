From Coq Require Import ssreflect ssrfun.
From stdpp Require Import base decidable.

Section disj_choice_lemmas.

  Definition choice_def (b1 b2 : bool) := b1 = b2.
  Definition choice_aux : seal (@choice_def). Proof. by eexists. Qed.
  Definition open_choice := choice_aux.(unseal).
  Definition made_choice := choice_aux.(unseal).
  Global Opaque open_choice.
  Global Opaque made_choice.
  Global Arguments open_choice (_ _) : simpl never.
  Global Arguments made_choice (_ _) : simpl never.
  Lemma open_choice_eq : open_choice = choice_def.
  Proof. by rewrite -choice_aux.(seal_eq). Qed.
  Lemma made_choice_eq : made_choice = choice_def.
  Proof. by rewrite -choice_aux.(seal_eq). Qed.

  Lemma andb_true_elim b1 b2 :
    b1 && b2 = true →
    b1 = true ∧ b2 = true.
  Proof.
    move => Hb1b2.
    apply eq_sym in Hb1b2.
    by apply andb_true_eq in Hb1b2 as [-> ->].
  Qed.

  Lemma open_choice_andb_true_split b1 b2 : 
    open_choice (b1 && b2) true →
    open_choice b1 true ∧ made_choice b2 true.
  Proof. rewrite open_choice_eq made_choice_eq /choice_def => /andb_true_elim //. Qed.

  Lemma open_choice_orb_false_split b1 b2 : 
    open_choice (b1 || b2) false →
    open_choice b1 false ∧ made_choice b2 false.
  Proof. rewrite open_choice_eq made_choice_eq /choice_def => /orb_false_elim //. Qed.

  Lemma made_choice_andb_true_split b1 b2 : 
    made_choice (b1 && b2) true →
    made_choice b1 true ∧ made_choice b2 true.
  Proof. rewrite made_choice_eq /choice_def => /andb_true_elim //. Qed.

  Lemma made_choice_orb_false_split b1 b2 : 
    made_choice (b1 || b2) false →
    made_choice b1 false ∧ made_choice b2 false.
  Proof. rewrite made_choice_eq /choice_def => /orb_false_elim //. Qed.

  Lemma made_choice_false_true P : made_choice false true → P.
  Proof. rewrite made_choice_eq /choice_def //. Qed.

  Lemma made_choice_true_false P : made_choice true false → P.
  Proof. rewrite made_choice_eq /choice_def //. Qed.

  Lemma made_choice_both b P : made_choice b false → made_choice b true → P.
  Proof. rewrite made_choice_eq /choice_def => -> //. Qed.

  Lemma andb_false_case b1 b2 :
    b1 && b2 = false →
    b2 = false ∨ (b1 = false ∧ b2 = true).
  Proof. destruct b1 => /=; eauto. destruct b2; eauto. Qed.

  Lemma orb_true_case b1 b2 :
    b1 || b2 = true →
    b2 = true ∨ (b1 = true ∧ b2 = false).
  Proof. destruct b1 => /=; eauto. destruct b2; eauto. Qed.

  Lemma open_choice_andb_false_case b1 b2 :
    open_choice (b1 && b2) false →
    (made_choice b2 false) ∨ (open_choice b1 false ∧ made_choice b2 true).
  Proof. rewrite open_choice_eq made_choice_eq /choice_def => /andb_false_case //. Qed.

  Lemma open_choice_orb_true_case b1 b2 :
    open_choice (b1 || b2) true →
    (made_choice b2 true) ∨ (open_choice b1 true ∧ made_choice b2 false).
  Proof. rewrite open_choice_eq made_choice_eq /choice_def => /orb_true_case //. Qed.

  Lemma made_choice_andb_false_case b1 b2 :
    made_choice (b1 && b2) false →
    (made_choice b2 false) ∨ (made_choice b1 false ∧ made_choice b2 true).
  Proof. rewrite made_choice_eq /choice_def => /andb_false_case //. Qed.

  Lemma made_choice_orb_true_case b1 b2 :
    made_choice (b1 || b2) true →
    (made_choice b2 true) ∨ (made_choice b1 true ∧ made_choice b2 false).
  Proof. rewrite made_choice_eq /choice_def => /orb_true_case //. Qed.

  Lemma made_choice_bool_decide_true (P : Prop) `{Decision P} :
    made_choice (bool_decide P) true → P.
  Proof. rewrite made_choice_eq /choice_def => /bool_decide_eq_true_1 //. Qed.

  Lemma made_choice_bool_decide_false (P : Prop) `{Decision P} :
    made_choice (bool_decide P) false → ¬P.
  Proof. rewrite made_choice_eq /choice_def => /bool_decide_eq_false_1 //. Qed.

  Lemma made_choice_negb b r :
    made_choice (negb b) r →
    made_choice b (negb r).
  Proof. rewrite made_choice_eq /choice_def; case: b; case: r => //=. Qed.

  Lemma made_choice_eqb b r1 r2 :
    made_choice (eqb b r1) r2 →
    made_choice b (if r1 then r2 else negb r2).
  Proof. rewrite made_choice_eq /choice_def; move: b r1 r2 => [][][] //=. Qed.

  Lemma choice_disj_open_choice (P Q : Prop) c :
    (open_choice c true → P) →
    (open_choice c false → Q) →
    P ∨ Q.
  Proof.
    destruct c; rewrite open_choice_eq /choice_def.
    - move => /(flip apply eq_refl) HP _.
      by left.
    - move => _ /(flip apply eq_refl) HQ.
      by right.
  Qed.

  Lemma case_made_choice b :
    ∃ r, made_choice b r.
  Proof. rewrite made_choice_eq /choice_def; by exists b. Qed.

End disj_choice_lemmas.

Ltac handle_made_choice H :=
  simpl in H;
  lazymatch type of H with
  | made_choice ?c ?r =>
    first (* first detect when c is just the opposite of r, ie c = true, r = false *)
    [ refine (made_choice_false_true _ H)
    | refine (made_choice_true_false _ H)
    | lazymatch c with
      | ?c1 && ?c2 =>
        let H1 := fresh "H1" in
        lazymatch r with
        | true =>
          apply made_choice_andb_true_split in H;
          destruct H as [H1 H]; 
          handle_made_choice H1; handle_made_choice H
        | false =>
          apply made_choice_andb_false_case in H as [H1|[H1 H]];
          [handle_made_choice H1 | handle_made_choice H1; handle_made_choice H]
        end
      | ?c1 || ?c2 =>
        lazymatch r with
        | true =>
          let H1 := fresh "H1" in
          apply made_choice_orb_true_case in H as [H1|[H1 H]];
          [handle_made_choice H1 |handle_made_choice H1; handle_made_choice H]
        | false =>
          apply made_choice_orb_false_split in H;
          let H1 := fresh "H1" in
          destruct H as [H1 H];
          handle_made_choice H1; handle_made_choice H
        end
      | r => (* no information to be gleaned here, drop hypothesis *)
        clear H
      | negb ?c' =>
        let result_pre_type := type of (made_choice_negb _ _ H) in
        let result_type := eval simpl in result_pre_type in
        lazymatch goal with
        | H' : result_type |- _ => 
          clear H (* we already know this, so drop H *)
        | |- _ => 
          apply made_choice_negb in H;
          handle_made_choice H
        end
      | eqb ?c' ?r' =>
        let result_pre_type := (type of (made_choice_eqb _ _ _ H)) in
        let result_type := eval simpl in result_pre_type in
        lazymatch goal with
        | H' : result_type |- _ => 
          clear H (* we already know this, so drop H *)
        | |- _ =>
          apply made_choice_eqb in H;
          handle_made_choice H
        end
      | _ =>
        first
        [ let nr := eval cbn in (negb r) in
          lazymatch goal with
          | H' : made_choice c nr |- _ =>
            (* if this is the case, we are in a contradictory case: *)
            refine (made_choice_both _ _ H H') || refine (made_choice_both _ _ H' H)
          end
        | lazymatch c with
          | bool_decide ?P =>
            let H' := fresh "H" in
            first
            [apply made_choice_bool_decide_true in H as H'
            |apply made_choice_bool_decide_false in H as H']
          | _ => idtac
          end ]
      end ]
  end.

(* should fail if no progress was made. This makes sure the
    non-lazy match covers all choices *)
Ltac handle_open_choices :=
  lazymatch goal with
  | H : open_choice ?c ?r |- _ =>
    simpl in H;
    first 
    [is_constructor r
    |fail 4 "RHS of hypothesis"H"should be a constructor!"];
    first 
    [ has_evar c;
      lazymatch c with
      | ?c1 && ?c2 =>
        let H1 := fresh "H1" in
        lazymatch r with
        | true =>
          apply open_choice_andb_true_split in H;
          destruct H as [H H1];
          handle_made_choice H1
        | false =>
          apply open_choice_andb_false_case in H as [H1|[H H1]];
          handle_made_choice H1 
          (* right hand side should repeat handle open choices, but this tactic is called with repeat *)
        end
      | ?c1 || ?c2 =>
        let H1 := fresh "H1" in
        lazymatch r with
        | true =>
          apply open_choice_orb_true_case in H as [H1|[H H1]];
          handle_made_choice H1
        | false =>
          apply open_choice_orb_false_split in H;
          destruct H as [H H1];
          handle_made_choice H1
        end
      | _ =>
        first
        [is_evar c; fail 2 (*c is an evar, which is okay: but nothing to do here, so fail *)
        |fail 3 "Hypothesis"H"is an open choice of unknown form!"]
          (*c is not an evar, but not an && or || either; this should not happen *)
      end
    | fail 1 "Hypothesis"H"contains an open choice but "c" contains no evars! BAD!"]
  end.

Ltac make_oppose_current_choices c r :=
  first 
  [ lazymatch goal with
    | H : made_choice ?c' ?r' |- _ =>
      let cn := open_constr:(_ : bool) in
      first
      [ lazymatch r with
        | true => 
          unify c (cn || (negb $ eqb c' r'))
        | false => 
          unify c (cn && eqb c' r')
        end
        (* unification may fail if c' depends on variables not yet in context for c *)
        (* in this case, just skip negating c' *)
      | unify c cn];
      revert H;
      make_oppose_current_choices cn r
    end
  | lazymatch r with
    | true => unify c false
    | false => unify c true
    end;
    repeat (
      lazymatch goal with
      | |- made_choice ?c ?r → _ =>
        intros ?
      end
    )
 ].

Ltac give_up :=
  lazymatch goal with
  | H : open_choice ?c ?r |- _ =>
    (* we must make H a contradiction, forcing c to be not equal to r *)
    (* first extract the extra freedom we might have: always making c the negation of r *)
    is_evar c;
    let cl := open_constr:(_ : bool) in
    let cr := open_constr:(_ : bool) in
    lazymatch r with
    | true => unify c (cr && cl)
    | false => unify c (cr || cl)
    end;
    make_oppose_current_choices cl r;
    repeat handle_open_choices
  end || fail "Could not find an open choice".

(*Ltac require_bool b :=
  lazymatch goal with
  | H : open_choice ?c ?r |- _ =>
    let cn := open_constr:(_ : bool) in
    first
    [ lazymatch r with
      | true => unify c (andb cn b)
      | false => unify c (orb cn (negb b))
      end;
      repeat handle_open_choices
    (* unification may fail if b depends on variables not yet in context when
        c was created. In this case, give_up *)
    | give_up]
  end. *)

Ltac require_bool b :=
  lazymatch goal with
  | H' : open_choice ?c ?r |- _ =>
    let H := fresh "H" in
    destruct (case_made_choice b) as [[] H];
    [handle_made_choice H | give_up]
  end.

Ltac require P :=
  first 
  [ first 
    (* try to detect whether we already have contradictory information about P *)
    [ lazymatch P with
      | True =>
        (* true is also decidable, but we do not wish to make other bools dependent on this,
           since bool_decide True is always true. Therefore, we introduce True in the context,
            so it can be solved with assumption, but only if that cannot already be done. *)
        first [assert True by assumption | assert (True) by exact I]
      | False => 
        (* false is 'decidable', but we wish to give up since bool_decide False won't reduce *)
        give_up
      | ¬?nP =>
        lazymatch goal with
        | H : made_choice (bool_decide nP) true |- _ =>
          (* we want ¬nP, but we have nP: this is contradictory, give up *)
          give_up
        end
      | _ =>
        lazymatch goal with
        | H : made_choice (bool_decide (¬P)) true |- _ =>
          (* we want P, but we have ¬P: this is contradictory, give up *)
          give_up
        end
      end
    | lazymatch goal with
      | H : made_choice (bool_decide P) false |- _ =>
        (* we want P, but we have ¬P: this is contradictory, give up *)
        give_up
      end
    | let bdp := 
        match P with
        | ¬?nP => constr:(negb (bool_decide nP))
        | _ => constr:(bool_decide P)
        end
      in
      require_bool bdp]
  | fail 1 "Proposition"P" cannot be required since it is not decidable"].

Ltac smart_disj_solve_step :=
  (progress repeat handle_open_choices) ||
  assumption ||
  lazymatch goal with
  | |- ?P ∧ ?Q =>
    split
  | |- ?P ∨ ?Q =>
    let H := fresh "H" in
    eapply choice_disj_open_choice; intros H
  | |- ?P =>
    first
    [eassert (Decision P) as _ by typeclasses eauto;
     require P
    | fail 3 "Atom"P"is not an assumption or decidable: quit."]
  end.

Ltac smart_disj_solve :=
  unshelve (progress (repeat (smart_disj_solve_step))); try exact false.

Module test.
Section test.
  Context {P : Prop}.
  Context `{!Decision Q, !Decision R}.

  Lemma choice_test : 
    P → ((P ∧ Q) ∨ (P ∧ ¬Q)).
  Proof.
    intros.
    smart_disj_solve.
  Qed.

  Lemma choice_test2 :
    P → (((P ∧ Q ∧ R) ∨ (P ∧ Q ∧ ¬R)) ∨ (P ∧ ¬Q)).
  Proof.
    intros.
    smart_disj_solve.
  Qed.

  Lemma choice_test3 :
    P → (((P ∧ Q ∧ R) ∨ (P ∧ Q ∧ ¬R) ∨ False) ∨ (P ∧ ¬Q)).
  Proof.
    intros.
    smart_disj_solve.
  Qed.

  Lemma choice_test4 :
    ((Q ∧ ¬R) ∨ (¬Q ∧ R)) ∨ ((Q ∧ R) ∨ (¬Q ∧ ¬R)).
  Proof.
    smart_disj_solve.
  Qed.

  Lemma choice_test5 `{Decision P} :
    ((P ∨ Q) ∧ (¬P ∨ R)) ∨ ((¬P ∧ ¬Q) ∨ (P ∧ ¬R)).
  Proof.
    unfold Decision in *.
    tauto.
  Qed.

  Lemma choice_test6 `{Decision P} :
    ((Q ∧ ¬R ∧ P) ∨ (¬Q ∧ P ∧ R)) ∨ ((P ∧ Q ∧ R) ∨ (P ∧ ¬Q ∧ ¬R)) ∨ 
    ((Q ∧ ¬R ∧ ¬P) ∨ (¬Q ∧ R ∧ ¬ P)) ∨ ((Q ∧ R ∧ ¬P) ∨ (¬Q ∧ ¬R ∧ ¬ P)).
  Proof.
    time smart_disj_solve.
  Qed.

End test.
End test.



