From iris.bi Require Export bi telescopes.
From iris.proofmode Require Import base environments coq_tactics.

Import bi.
Import env_notations.

(* This file defines some extra utilities for dealing with the environments of the Iris Proof Mode.
    Most concretely, it defines the following method: *)

Definition envs_add_fresh {PROP} p (k : option ident) Q Δ :=
  let '(k', Δ') := 
    match k return (ident * envs PROP) with
    | None => (IAnon (env_counter Δ), envs_incr_counter Δ)
    | Some k' => (k', Δ)
    end in
  envs_app p (Esnoc Enil k' Q) Δ'.

(* Here envs_add_fresh : bool → option ident → PROP → envs PROP → option (envs PROP)
   and if (envs_add_fresh b mi P Δ) = Some Δ', then Δ' is equal to Δ with P as extra hypothesis.
   if mi is None, the hypothesis P is anonymous. If mi is Some i, the hypothesis has name i.

   Below are lemmas which prove the required properties of envs_add_fresh
*)

Fixpoint envs_replace_list {PROP : bi} (reps : list (ident * bool * PROP * ident)) (Δ : envs PROP) : option (envs PROP) :=
  match reps with
  | [] => Some Δ
  | (i, p, P, j ):: reps' =>
    Δ' ← envs_simple_replace i p (Esnoc Enil j P) Δ; envs_replace_list reps' Δ'
  end.

Section environment_facts.
  Context {PROP : bi}.
  Implicit Types Δ : envs PROP.
  Implicit Types Γ : env PROP.

  Ltac destruct_option_tac A E_name :=
    let rec rewrite_until_goal old_goal :=
      match goal with
      | |- ?a = ?b -> old_goal => (move => <- || move => -> || idtac)
      | |- ?a = ?b -> ?G => (move => <- || move => ->); rewrite_until_goal old_goal
      | |- _ => idtac
      end
    in
    let rec destruct_opt mA old_goal :=
      lazymatch mA with
      | pm_option_bind ?p ?mA' =>
        destruct_opt mA' old_goal
      | (match ?mA' with _ => _ end) =>
        destruct_opt mA' old_goal
      | _ => 
        let E := fresh "E" in
        case E : mA => [A|] /= ;
        revert E; move => E_name //;
        (* // drops the nonsense branch *)
        lazymatch goal with
          | |- Some ?A = Some ?B -> ?P => case
          | |- _ => idtac 
        end; (* simplifies equalities of Some a = Some b to a = b, ignoring goals where this won't work *)
        rewrite_until_goal old_goal
      end
    in
    lazymatch goal with
    | H : pm_option_bind ?p ?mA = ?mB |- ?P =>
      revert H; destruct_option_tac A E_name
    | |- pm_option_bind ?p ?mA = ?mB -> ?P =>
      destruct_opt mA P
    end.

  Tactic Notation "destruct_option" "as" simple_intropattern(A) simple_intropattern (H) :=
      destruct_option_tac A H.
  Tactic Notation "destruct_option" "as" simple_intropattern(A) := 
      let H := fresh "H" in destruct_option as A H.
  Tactic Notation "destruct_option" := let x := fresh "x" in destruct_option as x.

  Proposition ident_beq_ind (P : bool → Prop) i j:
    (i = j → P true) → (i ≠ j → ident_beq i j = false → P false) → P (ident_beq i j).
  Proof.
    move => Htrue Hfalse.
    case E : (ident_beq _ _).
    - by apply Htrue, ident_beq_true.
    - apply Hfalse, E. move /ident_beq_true => Hij.
      move: Hij; rewrite E //.
  Qed.

  Proposition ident_beq_refl i : ident_beq i i = true.
  Proof. by apply ident_beq_true. Qed.

  Lemma envs_app_wf_singleton Δ Δ' p j Q : 
    envs_wf Δ → envs_app p (Esnoc Enil j Q) Δ = Some Δ' → envs_wf Δ'.
  Proof.
    case: Δ p => [Γp Γs n] [|] [/= HΓp HΓs HΓps] H;
    do 2 destruct_option.
    all: split => //=; try (constructor; done).
    all: move => i; case: (HΓps i) => -> {HΓps}; auto.
    all: elim/ident_beq_ind : _ => [-> | ]; auto.
  Qed.

  Proposition envs_add_fresh_sound Δ Δ' p k Q : 
    envs_add_fresh p k Q Δ = Some Δ' →
      of_envs Δ ⊢ □?p Q -∗ of_envs Δ'.
  Proof.
    case: k => [k|]; rewrite /envs_add_fresh => H.
    2: rewrite -envs_incr_counter_sound.
    all: rewrite envs_app_singleton_sound => // /= {H}.
  Qed.

  Proposition envs_add_fresh_wf Δ Δ' p k Q :
    envs_wf Δ → envs_add_fresh p k Q Δ = Some Δ' → envs_wf Δ'.
  Proof.
    case: k => [k|] Hwf /=; rewrite /envs_add_fresh.
    all: by apply: envs_app_wf_singleton.
  Qed.

  Proposition env_app_l_lookup Γ Γapp Γ' i :
    env_app Γ Γapp = Some Γ' →
    Γ !! i = None →
    Γ' !! i = Γapp !! i.
  Proof.
    revert Γ'.
    induction Γ as [|Γ IHΓ i' P] => /= Γ'; first by case => -> //.
    destruct_option.
    specialize (IHΓ _ H).
    destruct (x !! i') => //.
    case => <- /=.
    destruct (ident_beq i i') => //.
  Qed.

  Proposition envs_app_lookup Δ p Γ Δ' i :
    envs_app p Γ Δ = Some Δ' →
    Γ !! i = None →
    envs_lookup i Δ' = envs_lookup i Δ.
  Proof.
    destruct Δ as [Γp Γs n] => /=.
    case: p; do 2 destruct_option => /=.
    - move => /env_app_l_lookup Happ_lookup.
      by rewrite (Happ_lookup _ _ H0).
    - move => /env_app_l_lookup Happ_lookup.
      by rewrite (Happ_lookup _ _ H0).
  Qed.

  Proposition envs_app_disjoint Δ p Γ Δ' i :
    envs_app p Γ Δ = Some Δ' →
    Γ !! i = None ∨ envs_lookup i Δ = None.
  Proof.
    destruct Δ as [Γp Γs n] => /=.
    case: p; do 2 destruct_option => /=.
    - move: H H0 => + + _.
      move => /env_app_disjoint HiΓs.
      move => /env_app_disjoint HiΓp.
      specialize (HiΓp i) as [-> | ->]; first by left.
      specialize (HiΓs i) as [-> | ->]; [by left | by right].
    - move: H H0 => + + _.
      move => /env_app_disjoint HiΓp.
      move => /env_app_disjoint HiΓs.
      specialize (HiΓp i) as [-> | ->]; first by left.
      specialize (HiΓs i) as [-> | ->]; [by left | by right].
  Qed.

  Proposition envs_lookup_incr_counter Δ i :
    envs_lookup i Δ = envs_lookup i (envs_incr_counter Δ).
  Proof. by destruct Δ. Qed.

  Proposition envs_lookup_add_fresh Δ Δ' i p P j q Q :
    envs_lookup i Δ = Some (p, P) →
    envs_add_fresh q j Q Δ = Some Δ' →
    envs_lookup i Δ' = Some (p, P).
  Proof.
    rewrite /envs_add_fresh.
    case: j => [j|].
    - intros HiΔ HΔΔ'.
      rewrite -HiΔ.
      eapply envs_app_lookup => //.
      destruct (envs_app_disjoint _ _ _ _ i HΔΔ') => //.
      contradict H.
      by rewrite HiΔ.
    - intros HiΔ HΔΔ'.
      rewrite -HiΔ.
      rewrite (envs_lookup_incr_counter Δ i).
      eapply envs_app_lookup => //.
      destruct (envs_app_disjoint _ _ _ _ i HΔΔ') => //.
      contradict H.
      by rewrite -envs_lookup_incr_counter HiΔ.
  Qed.

  Proposition env_app_r_lookup Γ Γapp Γ' i :
    env_app Γ Γapp = Some Γ' →
    Γapp !! i = None →
    Γ' !! i = Γ !! i.
  Proof.
    revert Γ'.
    induction Γ as [|Γ IHΓ i' P] => /= Γ'; first by case => -> //.
    destruct_option.
    specialize (IHΓ _ H).
    destruct (x !! i') => //.
    case => <- /IHΓ /= -> //.
  Qed.

  Proposition envs_app_lookup_add Δ p Γ Δ' i :
    envs_app p Γ Δ = Some Δ' →
    envs_lookup i Δ = None →
    envs_lookup i Δ' = P ← Γ !! i; Some (p, P).
  Proof.
    destruct Δ as [Γp Γs n] => /=.
    case: p; do 2 destruct_option => /=.
    - destruct (Γp !! i) eqn:HΓpi => //.
      destruct_option => _.
      rewrite (env_app_r_lookup _ _ _ _ H0 HΓpi).
      destruct (Γ !! i) => //=.
    - destruct (Γp !! i) eqn:HΓpi => //.
      destruct_option => _.
      by rewrite (env_app_r_lookup _ _ _ _ H0 H1).
  Qed.

  Proposition envs_add_fresh_lookup Δ p j P Δ' :
    envs_add_fresh p j P Δ = Some Δ' →
    let k := 
      match j with 
      | Some k => k 
      | None => (IAnon (env_counter Δ)) 
      end in
    envs_lookup k Δ' = Some (p, P).
  Proof.
    destruct j as [k|] => /=.
    - rewrite /envs_add_fresh => HpΔ.
      erewrite envs_app_lookup_add => // /=.
      { rewrite ident_beq_refl //. }
      destruct (envs_app_disjoint _ _ _ _ k HpΔ) => //.
      contradict H => /=.
      by rewrite ident_beq_refl.
    - rewrite /envs_add_fresh => HpΔ.
      erewrite envs_app_lookup_add => //.
      { simpl. enough (positive_beq _ _ = true) as -> => //.
        by apply positive_beq_true. }
      destruct (envs_app_disjoint _ _ _ _ (IAnon (env_counter Δ)) HpΔ) => //.
      contradict H => /=.
      enough (positive_beq _ _ = true) as -> => //.
      by apply positive_beq_true.
  Qed.

  Proposition envs_add_fresh_disjoint Δ p j P Δ' i :
    envs_add_fresh p j P Δ = Some Δ' →
    let k := 
      match j with 
      | Some k => k 
      | None => (IAnon (env_counter Δ)) 
      end in
    i ≠ k ∨ envs_lookup i Δ = None.
  Proof.
    destruct j as [k|] => /=; rewrite /envs_add_fresh => HpΔ.
    - destruct (envs_app_disjoint _ _ _ _ i HpΔ) => /=; last by right.
      left. contradict H; subst => /=.
      by rewrite ident_beq_refl.
    - destruct (envs_app_disjoint _ _ _ _ i HpΔ) => /=.
      * left. contradict H; subst => /=.
        enough (positive_beq _ _ = true) as -> => //.
        by apply positive_beq_true.
      * right. by rewrite envs_lookup_incr_counter.
  Qed.

End environment_facts.



