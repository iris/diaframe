From iris.proofmode Require Import base reduction coq_tactics tactics notation.
From iris.bi Require Import bi telescopes.

From diaframe Require Import utils_ltac2.
From Ltac2 Require Import Fresh.


(* This file defines a lot of utilities for dealing with stdpp's telescope types.
   The most tricky parts here are extracting the dependency of a function on its telescoping arguments,
   as well as 'merging' two telescopes.
   These can be found below as ExtractDep and TeleConcatType. *)


Notation "'λᵗ' x .. y , e" :=
  (tele_app (TT := TeleS (λ x, .. (TeleS (λ y, TeleO)) .. )) (λ x, .. (λ y, e) .. ))
  (at level 200, x binder, y binder, right associativity,
   format "'[  ' 'λᵗ'  x  ..  y ']' ,  e") : stdpp_scope.

Notation "'[tele_pair' x ; .. ; z ']'" := 
  (TeleS (fun _ : x => .. (TeleS (fun _ : z => TeleO)) ..))
  (format "[tele_pair  '[hv' x ;  .. ;  z ']' ]").

(* this is less broken than stdpp's default tele_arg stuff
  TODO: Make stdpp merge request *)
Notation "'[tele_arg3'  x ; .. ; z ]" :=
  ((TeleArgCons 
      x
      (.. (TeleArgCons z TargO) .. ))).

(** Helper functions on telescopes *)

(* Getting witnesses out of a term with type telescope *)

Set Universe Polymorphism.
(* NOTE: This entire file is now universe polymorphic. Diaframe used to pose a
  bi.Logic = bi.Quant constraint. And telescopes were ofcourse the culprit,
  for some unclear reason. There are also some [Unset Universe Minimization ToSet]
  scattered around this file, since that also broke stuff.*)

Definition targ_proj1 {X} {f : X → tele} (a : TeleS f) : X := 
  let (result, _) := a in result.

Definition targ_proj2 {X} {f : X → tele} (a : TeleS f) : f $ targ_proj1 a :=
  match a as t return (f (targ_proj1 t)) with
  | {| tele_arg_head := h; tele_arg_tail := tail |} => tail
  end.

Notation "(.t1)" := (targ_proj1) (at level 100, no associativity).
Notation "(.t2)" := (targ_proj2) (at level 100, no associativity).

Notation "A .t1" := (targ_proj1 A) (at level 2, left associativity, format "A .t1").
Notation "A .t2" := (targ_proj2 A) (at level 2, left associativity, format "A .t2").

Global Arguments targ_proj1 {_ _} !_ /.
Global Arguments targ_proj2 {_ _} !_ /.

Polymorphic Fixpoint tele_arg_rect (P : ∀ TT, tele_arg TT → Type) 
  (base : P TeleO TargO)
  (rec : (∀ T (b : T → tele) x xs, P (b x) xs → P (TeleS b) (TargS x xs)))
  (TT : tele) : 
  ∀ (xs : tele_arg TT), P TT xs :=
  match TT return (∀ (xs : TT), P TT xs) with
  | TeleO => λ unit_el,
    match unit_el with
    | () => base
    end
  | @TeleS Y b => λ xs,
    match xs with
    | {| tele_arg_head := x; tele_arg_tail := tail |} =>
      rec Y b x tail $ tele_arg_rect P base rec (b x) tail
    end
  end.

(* Adding to the tail of a telescope, when the argument is not dependent on the telescope itself *)
(* this is used in the Ltac which extracts dependency on variables *)

Fixpoint TelePost (TT : tele) (X : Type) : tele :=
  match TT with
  | TeleO => TeleS (λ x : X, TeleO)
  | @TeleS Y binder => TeleS (λ y : Y, TelePost (binder y) X)
  end.

Notation TargS' f a b :=
  ((@TeleArgCons _ (λ x, tele_arg (f x)) a b) : (tele_arg (TeleS f))) (f in scope function_scope, only parsing).

Definition TargPost {TT : tele} (targ : TT) {X : Type} (x : X) : TelePost TT X :=
  (tele_arg_rect (λ TT targ, ∀ (X : Type) (x : X), TelePost TT X)
      (λ X x, TargS' (λ _, TeleO) x TargO)
      (λ Y b y targ rec X tt, TargS y (rec X tt))) TT targ X x.

(* Note that to apply these functions the dependency on targ must be explicit, otherwise the type
  the type of the function cannot even be stated. 
  Right, so now we just apply TargPost' multiple times to merge telescopes, right?
  Nope, have to do it all at once: *)

Polymorphic Fixpoint TeleConcatType (TT : tele) : (TT -t> tele) → tele :=
  match TT return (TT -t> tele) → tele with
  | TeleO => λ TT2, TT2
  | @TeleS Y binder => λ f, @TeleS Y (λ y, TeleConcatType (binder y) (f y))
  end.

(* Alternative definition:
Definition TeleConcatType' (TT : tele) (tfun : TT -t> tele) : tele := tele_fold (@TeleS) id tfun. *)

Definition TeleConcatArg : ∀ {TT : tele} (targ : TT)
   (X : TT -t> tele), tele_app X targ → TeleConcatType TT X :=
  tele_arg_rect (λ TT targ, ∀ (X : TT -t> tele), tele_app X targ → TeleConcatType TT X) 
    (λ X x, x)
    (λ Y b y targ rec X tt, TargS y (rec (X y) tt)).
(* is there a targ_fold..? *)

Definition tele_fun_compose' {TT1 TT2 : tele} {T : Type} :
  (TT2 -t> T) → (TT1 -t> TT2) → (TT1 -t> T) :=
  λ t1 t2, tele_bind (λ tt1, (tele_app t1) (tele_app t2 tt1)).

Global Arguments tele_fun_compose' {!TT1} {!TT2} {_} _ _ /.

Definition tele_pointwise (TT : tele) {T} : (relation T) → (relation (TT -t> T)) :=
  λ rel f g, ∀.. tt, rel (tele_app f tt) (tele_app g tt).

Global Arguments tele_pointwise TT {_} rel f g /.

Global Typeclasses Opaque tele_pointwise.

Fixpoint tele_fun_dep (TT : tele) : (TT -t> Type) → Type :=
  match TT return ((TT -t> Type) → Type) with
  | TeleO => (λ cod, cod)
  | TeleS b => (λ cod, ∀ x, tele_fun_dep (b x) (cod x))
  end.

Notation "TT -td_r> A" :=
  (tele_fun_dep TT A) (at level 99, A at level 200, right associativity).

Definition tele_appd {TT : tele} {A : TT -t> Type} (f : TT -td_r> A) : ∀ (tt : tele_arg TT), tele_app A tt :=
  λ tt, (tele_arg_rect (λ (TT : tele) (tt : tele_arg TT), ∀ A, (TT -td_r> A) → tele_app A tt)
     (λ A a, a)
     (λ Y b y targ rec X tt, rec (X y) (tt y))) TT tt A f.
Arguments tele_appd {!_ !_} _ !_ /.

Definition tele_dep_bind_raw {TT : tele} {A : TT -t> Type} : (∀ tt, tele_app A tt) → TT -td_r> A :=
  (fix rec TT : ∀ A, (∀ tt, tele_app A tt) → TT -td_r> A :=
    match TT return ∀ A, (∀ tt, tele_app A tt) → TT -td_r> A with
    | TeleO => λ A f, f [tele_arg]
    | TeleS b => λ A f x, rec (b x) (A x) (λ tt', f (TargS x tt'))
    end) TT A.

Lemma tele_appd_tele_dep_bind_raw_eq {TT1 : tele} {A : TT1 -t> Type} (f : (∀ tt1, tele_app A tt1)) (tt1 : TT1) :
  (tele_appd $ tele_dep_bind_raw f) tt1 = f tt1.
Proof.
  revert tt1.
  induction TT1.
  - by apply tforall_forall => /=.
  - apply tforall_forall => /= x.
    apply tforall_forall => tt1.
    apply H.
Qed.

Polymorphic Fixpoint tele_fun_dep_tele (TT : tele) : (TT -t> tele) → Type → Type :=
  match TT return ((TT -t> tele) → Type → Type) with
  | TeleO => (λ cod A, cod -t> A)
  | TeleS b => (λ cod A, ∀ x, tele_fun_dep_tele (b x) (cod x) A)
  end.

Notation "TT1 -td> TT2 -c> A" :=
  (tele_fun_dep_tele TT1 TT2 A) (at level 99, A at level 200, right associativity).

Polymorphic Definition tele_appd_dep {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TT1 -td> TT2 -c> A) : ∀ (tt : tele_arg TT1), tele_app TT2 tt -t> A :=
  λ tt, (tele_arg_rect (λ (TT : tele) (tt : tele_arg TT), ∀ TT2, (TT -td> TT2 -c> A) → tele_app TT2 tt -t> A)
    (λ TT2 f, f)
    (λ Y x y targ rec X tt, rec (X y) (tt y))) TT1 tt TT2 f.
Arguments tele_appd_dep {!_ _ _} _ !_ /.

Definition as_tele_fun_dep_raw {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TT1 -td> TT2 -c> A) :
   TT1 -td_r> (tele_map (flip tele_fun A) TT2).
Proof.
  apply tele_dep_bind_raw => tt1.
  rewrite tele_map_app /=.
  by apply tele_appd_dep.
Defined.

Definition as_tele_fun_dep_raw' {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TT1 -td> TT2 -c> A) :
   TT1 -td_r> (tele_map (flip tele_fun A) TT2) 
    := (fix rec TT1 : ∀ TT2, (TT1 -td> TT2 -c> A) → TT1 -td_r> (tele_map (flip tele_fun A) TT2) :=
        match TT1 with
        | TeleO => λ TT2 f, f
        | TeleS b => λ TT2 f x, rec (b x) (TT2 x) (f x)
        end) TT1 TT2 f.

Lemma tele_arg_inv_eq {TT : tele} (tt : TT) : 
  match TT as TT return TT → Prop with
  | TeleO => λ tt, tt = TargO
  | TeleS f => λ tt, tt = {| tele_arg_head := tt.t1; tele_arg_tail := tt.t2 |}
   end tt.
Proof. induction TT => //=; by destruct tt. Defined.
(* these are Defined, since sometimes we do eq_rect on equalities produced using these lemmas *)

Lemma tele_arg_S_inv_strong_hd_eq `{b : X → tele} (x : TeleS b) : 
  x = {| tele_arg_head := x.t1; tele_arg_tail := x.t2 |}.
 Proof. exact (tele_arg_inv_eq x). Defined.

Set Universe Polymorphism.
Lemma tele_arg_O_inv_eq (tt : TeleO) :
  tt = TargO.
Proof. apply (tele_arg_inv_eq tt). Defined.

Global Arguments tele_arg_O_inv_eq _ /.
Global Arguments tele_arg_S_inv_strong_hd_eq {_ _} _/.

Lemma unfold_tele_app_S {A C : Type} {TTf : A → tele} (c : TeleS TTf) (g : TeleS TTf -t> C) :
  tele_app (TT := TeleS TTf) g c = tele_app (g c.t1) c.t2.
Proof. destruct c => //=. Defined.

Lemma unfold_tele_app_O {C : Type} (g : TeleO -t> C) (c : TeleO):
  tele_app (TT := TeleO) g c = g.
Proof. rewrite (tele_arg_O_inv_eq c) => //=. Defined.

Definition elim_dep_teleo_fun {H : TeleO → Type} : 
  H TargO →
  (∀ (x : TeleO), H x). (* why not use tele_bind? well, dependent functions are a thing *)
Proof. intros. by erewrite tele_arg_O_inv_eq. Defined. (* this makes the construction compute *)

Definition elim_dep_teles_fun `{TTf : A → tele} {H : TeleS TTf → Type} :
  (∀ a, ∀ cons, H $ TargS a cons) →
  (∀ (x : TeleS TTf), H x).
Proof. intros. destruct x => //=. Defined.

Global Arguments elim_dep_teleo_fun {_} _ /.
Global Arguments elim_dep_teles_fun {_ _ _} _ /.

Lemma reducing_tforall_forall {TT : tele} (Ψ : TT → Prop) :
  tforall Ψ → (∀ x, Ψ x).
Proof.
  unfold tforall. induction TT as [|X ft IH].
  - simpl.
    intros ? p. rewrite (tele_arg_O_inv_eq p). done.
  - simpl.
    move => Hx a. destruct a as [x t'].
    specialize (IH x (λ t', Ψ $ TargS x t')).
    eapply IH; last done.
Defined.

Lemma reducing_forall_tforall {TT : tele} (Ψ : TT → Prop) :
  (∀ x, Ψ x) → tforall Ψ.
Proof.
  unfold tforall. induction TT as [|X ft IH] => //=.
  simpl.
  move => Hx a.
  eapply IH => s.
  eapply Hx.
Defined.

Definition tele_app_on_tele_map_tele_fun {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} {tt1 : TT1} :
  (tele_app (tele_map (flip tele_fun A) TT2) tt1) → ∀ (tt2 : tele_app TT2 tt1), A :=
    (tele_arg_rect (λ TT tt, ∀ TT2, (tele_app (tele_map (flip tele_fun A) TT2) tt) → ∀ (tt2 : tele_app TT2 tt), A)
      (λ X x, λ u, tele_app x u)
      (λ Y b y targ rec TT2 f tt2, rec (TT2 y) f tt2)) TT1 tt1 TT2.

Lemma as_tele_fun_dep_raw_eq {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TT1 -td> TT2 -c> A) :
  ∀ tt1, tele_appd_dep f tt1 = eq_rect _ (λ U, U) (tele_appd (as_tele_fun_dep_raw f) tt1) _ (tele_map_app _ _ _).
Proof.
  move => tt1 /=.
  rewrite tele_appd_tele_dep_bind_raw_eq.
  by rewrite rew_opp_r.
Qed.

Lemma as_tele_fun_dep_raw_eq' {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TT1 -td> TT2 -c> A) :
  ∀ tt1 tt2, tele_app (tele_appd_dep f tt1) tt2 = tele_app_on_tele_map_tele_fun (tele_appd (as_tele_fun_dep_raw' f) tt1) tt2.
Proof. 
  induction TT1.
  - case => //=.
  - case => y tl tt2. rewrite H //.
Qed.

Definition tele_uncurry {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TeleConcatType TT1 TT2 -t> A) 
      : (TT1 -td> TT2 -c> A)
  := (fix rec TT : (∀ TT2, (TeleConcatType TT TT2 -t> A) → TT -td> TT2 -c> A) :=
      match TT return (∀ TT2, (TeleConcatType TT TT2 -t> A) → TT -td> TT2 -c> A) with
      | TeleO => (λ TT2 f, f)
      | TeleS b => (λ TT2 f x, rec (b x) (TT2 x) (f x))
      end) TT1 TT2 f.

Polymorphic Definition tele_curry {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TT1 -td> TT2 -c> A)
      : TeleConcatType TT1 TT2 -t> A
  := (fix rec TT : (∀ TT2, (TT -td> TT2 -c> A) → TeleConcatType TT TT2 -t> A) :=
      match TT return (∀ TT2, (TT -td> TT2 -c> A) → TeleConcatType TT TT2 -t> A) with
      | TeleO => (λ TT2 f, f)
      | TeleS b => (λ TT2 f x, rec (b x) (TT2 x) (f x))
      end) TT1 TT2 f.

Lemma tele_curry_uncurry_compose {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TeleConcatType TT1 TT2 -t> A) :
  ∀ (tt : TeleConcatType TT1 TT2), tele_app f tt = tele_app (tele_curry $ tele_uncurry f) tt.
Proof.
  induction TT1 => //=.
  apply tforall_forall => /= x.
  apply tforall_forall => x0.
  apply H.
Qed.

Lemma tele_uncurry_curry_compose {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TT1 -td> TT2 -c> A) :
  ∀ (tt1 : TT1) (tt2 : tele_app TT2 tt1), 
    tele_app (tele_appd_dep f tt1) tt2 = tele_app (tele_appd_dep (tele_uncurry $ tele_curry f) tt1) tt2.
Proof.
  induction TT1 => //.
  move => tt1 x.
  destruct (tele_arg_S_inv tt1) as [x' [tt1' Htt1]].
  subst. (* <3 subst *)
  apply H.
Qed.

Lemma tele_curry_uncurry_relation {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : TeleConcatType TT1 TT2 -t> A) :
  ∀ (tt1 : TT1) (tt2 : tele_app TT2 tt1), 
    tele_app f (TeleConcatArg tt1 TT2 tt2) = tele_app (tele_appd_dep (tele_uncurry f) tt1) tt2.
Proof.
  induction TT1 => //.
  { rewrite -tforall_forall //=. }
  rewrite -tforall_forall /= => x.
  apply tforall_forall => tt1 tt2.
  apply H.
Qed.

Polymorphic Definition tele_dep_bind {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} : (∀ tt1, tele_app TT2 tt1 -t> A) → TT1 -td> TT2 -c> A
  := (fix rec TT : ∀ TT2, (∀ tt1, tele_app TT2 tt1 -t> A) → TT -td> TT2 -c> A :=
      match TT return ∀ TT2, (∀ tt1, tele_app TT2 tt1 -t> A) → TT -td> TT2 -c> A with
      | TeleO => (λ TT2 f, f TargO)
      | TeleS b => (λ TT2 f x, rec (b x) (TT2 x) (λ tt1, f (TargS x tt1)))
      end) TT1 TT2 .

Lemma tele_dep_appd_bind {TT1 : tele} {TT2 : TT1 -t> tele} {A : Type} (f : (∀ tt1, tele_app TT2 tt1 -t> A)) (tt1 : TT1) :
  (tele_appd_dep $ tele_dep_bind f) tt1 = f tt1.
Proof.
  revert tt1.
  induction TT1.
  - by apply tforall_forall => /=.
  - apply tforall_forall => /= x.
    apply tforall_forall => tt1.
    apply H.
Qed.

Record tele_arg_app (TT1 : tele) (TT2 : TT1 -t> tele) : Type := TeleArgApp 
  { tele_arg_start : TT1;
    tele_arg_end : tele_app TT2 tele_arg_start }.
Global Arguments TeleArgApp {_ _} _ _.

Fixpoint tele_arg_concat_inv_strong_hd {TT1 : tele} : ∀ {TT2 : TT1 -t> tele} (tt : TeleConcatType TT1 TT2),  tele_arg_app TT1 TT2 :=
  match TT1 return ∀ (TT2 : TT1 -t> tele) (tt : TeleConcatType TT1 TT2), tele_arg_app TT1 TT2 with
  | TeleO => λ TT2 tt, TeleArgApp TargO tt
  | TeleS b => 
      λ TT2 '(TeleArgCons x tt'),
        match @tele_arg_concat_inv_strong_hd (b x) (TT2 x) tt' with
        | TeleArgApp tt1 tt2 =>
          TeleArgApp ((TeleArgCons (f := b) x tt1) : tele_arg $ TeleS b) tt2 
        end
  end.

Lemma tele_arg_concat_inv_eq {TT1 : tele} {TT2 : TT1 -t> tele} (tt : TeleConcatType TT1 TT2) : 
  tt = match (tele_arg_concat_inv_strong_hd tt) with
       | TeleArgApp tt1 tt2 => TeleConcatArg tt1 _ tt2
       end.
Proof.
  induction TT1 => //.
  revert tt => /=.
  apply reducing_tforall_forall => /= x.
  apply reducing_forall_tforall => tt1.
  specialize (H _ _ tt1).
  revert H.
  generalize (tele_arg_concat_inv_strong_hd tt1).
  case => tt1_L tt1_R -> //=.
Defined.

Lemma tele_arg_concat_eq {TT1 : tele} {TT2 : TT1 -t> tele} (tt1 : TT1) (tt2 : tele_app TT2 tt1) :
  TeleArgApp tt1 tt2 = tele_arg_concat_inv_strong_hd (TeleConcatArg tt1 _ tt2).
Proof.
  induction TT1 => //=.
  { set H := tele_arg_O_inv_eq tt1; by subst. }
  revert tt1 tt2 => /=.
  rewrite -tforall_forall => /= x.
  apply reducing_forall_tforall => tt1 tt2.
  by rewrite -(H x (TT2 x) tt1 tt2).
Qed.

Definition TelePairType (TT1 TT2 : tele) := TeleConcatType TT1 (tele_bind (λ _, TT2)).

Definition as_dependent_fun (TT1 TT2 : tele) {A : Type} (f : TT2 -t> A) : 
    ∀ tt1 : TT1, tele_app (tele_bind (λ _, TT2)) tt1 -t> A
  := λ tt, (tele_arg_rect (λ TT tt, ∀ TT2, (TT2 -t> A) → tele_app (tele_bind (λ _, TT2)) tt -t> A)
      (λ TT f, f)
      (λ Y b y targ rec TT2 f, rec TT2 f)) TT1 tt TT2 f.

Global Arguments as_dependent_fun _ _ {_} _ _ /.

Definition as_dependent_arg (TT1 TT2 : tele) (tt2 : TT2) : ∀ tt1 : TT1, tele_arg $ tele_app (tele_bind (λ _, TT2)) tt1
:= λ tt1, (tele_arg_rect (λ TT tt1, tele_arg $ tele_app (tele_bind (λ _, TT2)) tt1)
      (tt2)
      (λ Y b y targ rec, rec)) TT1 tt1.

Definition as_non_dependent_arg (TT1 TT2 : tele) : ∀ tt1 : TT1, tele_app (tele_bind (λ _, TT2)) tt1 → TT2
 := λ tt1, (tele_arg_rect (λ TT tt1, tele_app (tele_bind (λ _, TT2)) tt1 → TT2)
      (λ tin, tin)
      (λ Y b y targ rec, rec)) TT1 tt1.

Lemma non_dependent_arg_eq (TT1 TT2 : tele) (tt2 : TT2) : 
    ∀ tt1 : TT1, as_non_dependent_arg _ _ tt1 $ as_dependent_arg _ _ tt2 tt1 = tt2.
Proof. induction TT1; case => //=. Qed.

Unset Universe Minimization ToSet.

Lemma dependent_arg_eq (TT1 TT2 : tele) : 
    ∀ (tt1 : TT1) (tt2 : tele_app (tele_bind (λ _, TT2)) tt1), 
      as_dependent_arg _ _ (as_non_dependent_arg _ _ tt1 tt2) tt1 = tt2.
Proof. induction TT1; case => //=. Qed.

Definition tele_pair_arg {TT1 TT2 : tele} (tt1 : TT1) (tt2 : TT2) : TelePairType TT1 TT2
  := TeleConcatArg tt1 (tele_bind (λ _, TT2)) (as_dependent_arg _ _ tt2 tt1).

Lemma dependent_fun_eq (TT1 TT2 : tele) {A : Type} (f : TT2 -t> A) :
  ∀ tt1 tt2, tele_app f tt2 = tele_app ((as_dependent_fun _ _ f) tt1) (as_dependent_arg TT1 TT2 tt2 tt1).
Proof. induction TT1; case => //=. Qed.

Set Universe Minimization ToSet.

Definition as_pair_fun {TT1 TT2 : tele} {T : Type} (f : TT1 -t> TT2 -t> T) : TelePairType TT1 TT2 -t> T 
  := tele_curry (tele_dep_bind (λ tt1 : TT1, as_dependent_fun TT1 TT2 (tele_app f tt1) tt1)).

Unset Universe Minimization ToSet.

Lemma as_pair_fun_on_pair_arg {TT1 TT2 : tele} {T : Type} (f : TT1 -t> TT2 -t> T) (tt1 : TT1) (tt2 : TT2) :
  tele_app (as_pair_fun f) (tele_pair_arg tt1 tt2) = tele_app (tele_app f tt1) tt2.
Proof.
  rewrite /as_pair_fun /tele_pair_arg.
  rewrite tele_curry_uncurry_relation.
  rewrite -tele_uncurry_curry_compose.
  rewrite tele_dep_appd_bind.
  by rewrite -dependent_fun_eq.
Qed.

Set Universe Minimization ToSet.

(* sometimes we need to use telescopic equalities to get a good type. this helps constructing those fixes *)

Polymorphic Definition tele_fix_app_bind_1_gen {A : tele} {C : Type} (g : C → Type) (a : A) :
  ∀ (TTf : A → C), 
    g (TTf a) →
    g (tele_app (tele_bind TTf) a) :=
  (tele_arg_rect (λ A a, ∀ (TTf : A → C), g (TTf a) → g (tele_app (tele_bind TTf) a))
    (λ TTf w, w)
    (λ Y b y targ rec TTf w, rec (λ b, TTf $ TargS y b) w)) A a.

Global Arguments tele_fix_app_bind_1_gen {_ _} _ !_ _ _ /.

Definition tele_fix_app_bind_2_gen {A : tele} {C : Type} (g : C → Type) (a : A) :
  ∀ (TTf : A → C), 
    g (tele_app (tele_bind TTf) a) →
    g (TTf a) :=
  (tele_arg_rect (λ A a, ∀ (TTf : A → C), g (tele_app (tele_bind TTf) a) → g (TTf a))
    (λ f w, w)
    (λ Y b y targ rec f w, rec (λ b, f $ TargS y b) w)) A a.

Global Arguments tele_fix_app_bind_2_gen {_ _} _ !_ _ _ /.

Lemma tele_fix_app_bind_cancel_1 {A : tele} {C : Type} (g : C → Type) (a : A) (TTf : A → C) w :
  tele_fix_app_bind_2_gen g a TTf $ tele_fix_app_bind_1_gen g a TTf w = w.
Proof.
  induction A; destruct a => //=.
  rewrite /tele_fix_app_bind_2_gen /tele_fix_app_bind_1_gen /= in H.
  rewrite H //. 
Qed.

Lemma tele_fix_app_bind_cancel_2 {A : tele} {C : Type} (g : C → Type) (a : A) (TTf : A → C) w :
  tele_fix_app_bind_1_gen g a TTf $ tele_fix_app_bind_2_gen g a TTf w = w.
Proof.
  induction A; destruct a => //=.
  rewrite /tele_fix_app_bind_2_gen /tele_fix_app_bind_1_gen /= in H.
  rewrite H //.
Qed.

Unset Universe Minimization ToSet.

Lemma tele_fix_app_bind_1_outward {A : tele} {C : Type} (G H : C → Type) (a : A) (TTf : A → C) w (h : ∀ c, G c → H c) :
  h _ $ tele_fix_app_bind_1_gen G a TTf w = tele_fix_app_bind_1_gen H a TTf (h _ w).
Proof.
  induction A; destruct a => //=.
  rewrite /tele_fix_app_bind_2_gen /tele_fix_app_bind_1_gen /= in H0.
  rewrite H0 //.
Qed.

Set Universe Minimization ToSet.

Lemma tele_fix_app_bind_2_outward {A : tele} {C : Type} (G H : C → Type) (a : A) (TTf : A → C) w (h : ∀ c, G c → H c) :
  h _ $ tele_fix_app_bind_2_gen G a TTf w = tele_fix_app_bind_2_gen H a TTf (h _ w).
Proof.
  induction A; destruct a => //=.
  rewrite /tele_fix_app_bind_2_gen /tele_fix_app_bind_1_gen /= in H0.
  rewrite H0 //.
Qed.

Lemma tele_fix_app_bind_adjoint {A : tele} {C D : Type} (G : C → Type) (a : A) (TTf : A → C) w (h : G (TTf a) → D) :
  h $ tele_fix_app_bind_2_gen G a TTf w = tele_fix_app_bind_1_gen (λ c, G c → D) a TTf h w.
Proof.
  induction A; destruct a => //=.
  rewrite /tele_fix_app_bind_2_gen /tele_fix_app_bind_1_gen /= in H.
  rewrite H //.
Qed.



Polymorphic Definition tele_app_bind_type_fix {A : tele} {B : Type} (a : A) :
  ∀ (TTf : A → tele), 
  (TTf a -t> B) →
  tele_app (tele_bind TTf) a -t> B := tele_fix_app_bind_1_gen (λ TT : tele, TT -t> B) a.

Global Arguments tele_app_bind_type_fix {_ _ !_ _} _ /.

Definition tele_app_bind_arg_fix {A : tele} (a : A) :
  ∀ (TTf : A → tele),
  tele_app (tele_bind TTf) a → TTf a := tele_fix_app_bind_2_gen tele_arg a.

Global Arguments tele_app_bind_arg_fix {_ !_ _} _ /.


Lemma tele_app_bind_fix_eq {A : tele} {B : Type} {TTf : A → tele} (a : A) arg (f : TTf a -t> B) :
  tele_app (tele_app_bind_type_fix f) arg = tele_app f (tele_app_bind_arg_fix arg).
Proof.
  rewrite tele_fix_app_bind_adjoint /tele_app_bind_type_fix.
  rewrite (tele_fix_app_bind_1_outward (λ TT, TT -t> B) _ _ _ _ (λ c f, tele_app f)).
  reflexivity.
  (* alternatively: induction a => //=. rewrite IHa //. *)
Qed.


(* Helper classes for handling telescopes *)
Class ExtractDep {T : Type} {TT : tele} (P : TT -t> T) (TT1 : tele) (P' : TT1 -t> T) (TT2 : TT1 -t> tele) (h : TT1 -td> TT2 -c> TT) :=
  extract_dep : (∀.. tt1 tt2, tele_app P' tt1 = tele_app P (tele_app (tele_appd_dep h tt1) tt2)).

Class TransportTeleProp {PROP : bi} `{TTf : A → tele} (a : A) (S : TTf a -t> PROP) (S' : TeleS TTf -t> PROP) := 
  transported_fun_as_seed : ∀ a' tt, (tele_app (S' a') tt ⊣⊢
    ∃ (H : a' = a), tele_app (eq_rect_r (λ a'', TTf a'' -t> PROP) S H) tt).

Section tele_lemmas. (* TODO: not sure how much these tele relations are still used. remove? *)

  Lemma tele_fun_compose_eq' {TT1 TT2 : tele} {T : Type} (f : TT2 -t> T) (g : TT1 -t> TT2) x :
    tele_app (tele_fun_compose' f g) x = tele_app f (tele_app g x).
  Proof. unfold tele_fun_compose'. rewrite tele_app_bind. done. Qed.

  Global Instance tele_pointwise_reflexive {TT T} (rel : relation T) :
    Reflexive rel →
    Reflexive (tele_pointwise TT rel).
  Proof.
    move => Hrel tfun /=.
    rewrite tforall_forall => tt.
    apply Hrel.
  Qed.

  Global Instance tele_pointwise_symmetric {TT T} (rel : relation T) :
    Symmetric rel →
    Symmetric (tele_pointwise TT rel).
  Proof.
    move => Hrel tfun1 tfun2 /= /tforall_forall Htfun.
    apply tforall_forall => tt.
    by apply Hrel, Htfun.
  Qed.

  Global Instance tele_pointwise_transitive {TT T} (rel : relation T) :
    Transitive rel →
    Transitive (tele_pointwise TT rel).
  Proof.
    move => Hrel tfun1 tfun2 tfun3 /= /tforall_forall Htfun12 /tforall_forall Htfun23.
    apply tforall_forall => tt.
    by eapply Hrel; [apply Htfun12 | apply Htfun23].
  Qed.

  Global Instance tele_pointwise_rewrite_relation {TT T} (rel : relation T) :
    RewriteRelation rel →
    RewriteRelation (tele_pointwise TT rel).
  Proof. Qed.

  Global Instance pointwise_empty_tele_subrelation_to {T : Type} (rel : relation T) :
    subrelation rel (tele_pointwise [tele] rel) | 50.
  Proof. done. Qed.

  Global Instance pointwise_empty_tele_subrelation_from {T : Type} (rel : relation T) :
    subrelation (tele_pointwise [tele] rel) rel | 50.
  Proof. done. Qed.

  Global Instance tele_pointwise_subrelation_mono {TT T} (rel1 rel2 : relation T) :
    subrelation rel1 rel2 →
    subrelation (tele_pointwise TT rel1) (tele_pointwise TT rel2).
  Proof.
    rewrite /subrelation => Hrel12 f g /tforall_forall Hfg.
    apply tforall_forall => x.
    apply Hrel12, Hfg.
  Qed.

  Global Instance tele_pointwise_flip_subrelation {TT T} (rel1 rel2 : relation T) :
    subrelation rel1 rel2 →
    subrelation (tele_pointwise TT (flip rel1)) (flip $ tele_pointwise TT rel2).
  Proof.
    rewrite /subrelation => Hrel12 f g /tforall_forall Hfg /=.
    apply tforall_forall => x.
    apply Hrel12, Hfg.
  Qed.

  Global Instance tele_pointwise_flip_subrelation2 {TT T} (rel1 rel2 : relation T) :
    subrelation rel1 rel2 →
    subrelation (flip $ tele_pointwise TT rel1) (tele_pointwise TT (flip rel2)).
  Proof.
    rewrite /subrelation => Hrel12 f g /tforall_forall Hfg /=.
    apply tforall_forall => x.
    apply Hrel12, Hfg.
  Qed.

  Polymorphic Lemma tele_pointwise_app {TT T} (rel : relation T) funL funR : 
    tele_pointwise TT rel funL funR →
    ∀ tt, rel (tele_app funL tt) (tele_app funR tt).
  Proof. by rewrite -tforall_forall. Qed.

  Definition dep_eval {A : Type} {B : A → Type} : ∀ (a : A), (∀ (a : A), B a) → B a
    := λ a f, f a. (* these two are best *)

  Polymorphic Definition dep_eval_tele {TT : tele} {B : TT → Prop} : ∀ (tt : TT), (∀.. (tt : TT), B tt) → B tt.
  Proof. move => tt. by rewrite tforall_forall. Defined.

End tele_lemmas.

Section other_instances.
  Global Instance inhabited_empty_tele : Inhabited [tele] := populate [tele_arg].

  Global Instance inhabited_teleS_nondep {A} {TT : tele} :
    Inhabited A →
    Inhabited TT →
    Inhabited (TeleS (λ _ : A, TT)) :=
    λ a tt, populate $ TargS inhabitant inhabitant.
  (* this is better than a match definition since it also reduces for an abstract Inhabited *)

End other_instances.

Section bi_helpers.
  Context {PROP : bi}.

  Lemma bi_texist_mono {TT : tele} (P Q : TT → PROP) :
    (∀ tt, P tt ⊢ Q tt) →
    (∃.. tt, P tt) ⊢ (∃.. tt, Q tt).
  Proof.
    rewrite !bi_texist_exist => HPQ.
    by apply bi.exist_mono.
  Qed.

  Lemma bi_tforall_mono {TT : tele} (P Q : TT → PROP) :
    (∀ tt, P tt ⊢ Q tt) →
    (∀.. tt, P tt) ⊢ (∀.. tt, Q tt).
  Proof.
    rewrite !bi_tforall_forall => HPQ.
    by apply bi.forall_mono.
  Qed.

  Lemma bi_texist_elim {TT : tele} P Q :
    (∀ tt, P tt ⊢ Q) →
    (∃.. tt : TT, P tt) ⊢@{PROP} Q.
  Proof.
    rewrite bi_texist_exist => HPQ.
    by apply bi.exist_elim.
  Qed.

  Lemma bi_tforall_elim {TT : tele} {P} tt :
    (∀.. tt : TT, P tt) ⊢@{PROP} P tt.
  Proof.
    by rewrite bi_tforall_forall (bi.forall_elim tt).
  Qed.

  Lemma dependent_transport `{TTf : A → tele} (a : A) (S : TTf a -t> PROP) :
    TransportTeleProp a S (λ a', tele_bind (λ tt, ∃ (H : a' = a), tele_app (eq_rect_r (λ a'', TTf a'' -t> PROP) S H) tt)%I).
  Proof.
    rewrite /TransportTeleProp => a' tt.
    by rewrite tele_app_bind.
  Qed.

  Lemma independent_transport {A} {TT} (a : A) (S : TT -t> PROP) :
    TransportTeleProp (TTf := λ _ : A, TT) a S (λ a', tele_map (bi_sep $ <affine> ⌜a' = a⌝)%I S).
  Proof.
    rewrite /TransportTeleProp => a' tt.
    rewrite !tele_map_app.
    rewrite -bi.persistent_and_affinely_sep_l.
    apply (anti_symm _).
    - apply bi.pure_elim_l => ->.
      by rewrite -(bi.exist_intro eq_refl).
    - apply bi.exist_elim => H; subst => /=.
      apply bi.and_intro => //.
      by apply bi.pure_intro.
  Qed.

End bi_helpers.


Global Hint Extern 4 (TransportTeleProp _ _ _) =>
  refine (independent_transport _ _) || refine (dependent_transport _ _) : typeclass_instances.



Ltac2 make_lambda (bind : binder) (c : constr) : constr :=
  Constr.Unsafe.make (Constr.Unsafe.Lambda bind c).

Ltac2 safe_apply (head : constr) (args : constr list) : constr :=
  Constr.Unsafe.make (Constr.Unsafe.App head (Array.of_list args)).

(* given a quantifier Q on type T
    (i.e., Q : ∀ A, (A → T) → T)
  we want to turn a function
  (λ x, Q X (λ y, Q Y (λ z, ..., f x y z))) 
  into
  (λ x y z, f x y z) 
  along with typing information
  [tele X; Y; Z]
*)

Ltac2 rec get_repeated_quantifier_function
  (input_fun : constr) :
  constr * constr :=
  match Constr.Unsafe.kind input_fun with
  | Constr.Unsafe.Lambda bind body =>
    lazy_match! body with
    (* this should work for arbitrary quantifiers Q,
      but match on terms does not support that. probably would need
      to resort to the Constr.Unsafe API for that *)
    | @bi_exist ?prop ?quant_type ?inner_fun =>
      (* there is inner quantification, recurse *)
      let (rec_fun, rec_tele) := get_repeated_quantifier_function inner_fun in
      (make_lambda bind rec_fun, safe_apply '@TeleS@{bi.Quant} [Constr.Binder.type bind;
        make_lambda bind rec_tele])
    | _ =>
      (* base case *)
      (input_fun, safe_apply '@TeleS@{bi.Quant} [Constr.Binder.type bind; make_lambda bind 'TeleO@{bi.Quant}])
    end
  | _ => Control.throw Not_found
  end.


Global Instance extract_dep_empty_tele {T : Type} (P : [tele] -t> T) : ExtractDep P [tele] P [tele] [tele_arg] | 0.
Proof. done. Qed.


Ltac2 rec get_tele_depth (in_tele : constr) : int :=
  lazy_match! in_tele with
  | TeleO => 0
  | TeleS ?ntele =>
    match Constr.Unsafe.kind ntele with
    | Constr.Unsafe.Lambda bind inner_tele =>
      Int.add (get_tele_depth inner_tele) 1
    | _ => Control.throw Not_found
    end
  end.


Ltac2 rec map_under_binders (the_fun : constr) (depth : int) (f : constr -> constr) : constr :=
  if Int.equal depth 0 then
    f the_fun
  else
    match Constr.Unsafe.kind the_fun with
    | Constr.Unsafe.Lambda bind ret_val =>
      let mapped := map_under_binders ret_val (Int.sub depth 1) f in
      make_lambda bind mapped
    | _ => Control.throw Not_found
    end.

Ltac2 rec compute_under_binders (the_fun : constr) (depth : int) (f : constr -> 'a) : 'a :=
  if Int.equal depth 0 then
    f the_fun
  else
    match Constr.Unsafe.kind the_fun with
    | Constr.Unsafe.Lambda bind ret_val =>
      compute_under_binders ret_val (Int.sub depth 1) f
    | _ => Control.throw Not_found
    end.

Ltac2 rec extract_dep_int (depth : int) (in_fun : constr) (offset : int) (bound : Free.t) :
  (* out_tele * out_fun * rem_tele * embed_fun *)
       constr * constr  * constr   * constr :=
  (* where we want: out_tele : tele
                    out_fun : out_tele -t> ?codomain
                    rem_tele : out_tele -t> tele
                    embed_fun : out_tele -td> rem_tele -c> tele *)
  if Int.equal depth 0 then
    ('TeleO@{bi.Quant}, in_fun, 'TeleO@{bi.Quant}, 'tt)
  else (
    match Constr.Unsafe.kind in_fun with
    | Constr.Unsafe.Lambda bind ret_val_base =>
      (* we need to replace the de Bruijn REL variable with a fresh variable,
         otherwise we cannot reference it directly *)
      let fresh_ident :=
        (let base_ident :=
          (match (Constr.Binder.name bind) with
          | None => Option.get (Ident.of_string "x")
          | Some n => n
          end) 
         in
         Fresh.fresh bound base_ident
        ) in
      let fresh_var := Constr.Unsafe.make (Constr.Unsafe.Var fresh_ident) in
      (* now substitute it into the body of the function. This means we must undo this step later! *)
      let ret_val := Constr.Unsafe.substnl [fresh_var] 0 ret_val_base in
      (* do the recursive call *)
      let result := extract_dep_int (Int.sub depth 1) ret_val (Int.add offset 1) (Fresh.Free.union bound (Fresh.Free.of_ids [fresh_ident])) in
      let (out_tele1, out_fun1, rem_tele1, embed_fun1) := result in
      (* create a closure which substitutes the de Bruijn variable back for our new variable *)
      let close_vars (c : constr) := Constr.Unsafe.closenl [fresh_ident] 1 c in
      (* We bind on [out_fun1], so we can check the dependency later *)
      let bound_out := make_lambda bind (close_vars out_fun1) in

      let bound_type := (Constr.Binder.type bind) in
      let out_tele_depth := get_tele_depth out_tele1 in
      let rem_tele_depth := compute_under_binders rem_tele1 out_tele_depth get_tele_depth in

      (* change_embed takes care of wrapping the inner tele_arg in a TargS *)
      let change_embed (body : constr) :=
        let depfun : constr :=
          lazy_match! body with
          | tt => make_lambda bind 'unit
          | @TeleArgCons ?hdtype ?deptype ?hd ?dep =>
            (* this is a kind of 'simultaneous' [close_vars] on hdtype and deptype *)
            let hdtype' := Constr.Unsafe.closenl [fresh_ident] 1 hdtype in
            let deptype' := Constr.Unsafe.closenl [fresh_ident] 1 deptype in
            make_lambda bind (safe_apply '@tele_arg_cons@{bi.Quant bi.Quant} [hdtype'; deptype'])
          end
        in
        safe_apply '@TeleArgCons@{bi.Quant bi.Quant} [bound_type; depfun; fresh_var; body]
      in

      lazy_match! bound_out with
      | (fun _ => ?t) => 
        (* variable is not used, so the result should be:
            out_tele := out_tele
            out_fun := t
            rem_tele := (λ.. tt : out_tele, TeleS (λ x : ?, rem_tele))
            embed_fun := (λ.. tt : out_tele, λ x : ?, λ.. : rem_tele, TargS x embed_fun)
         *) 
        let rem_tele := map_under_binders rem_tele1 out_tele_depth (fun c => 
          safe_apply '@TeleS@{bi.Quant} 
            [bound_type;
             (* the body [c] contains [out_tele_depth] de Bruijn vars.
                They should be upshifted by one, before we replace our fresh ident with
                our original de Bruijn variable. After binding, [c] functions like it did,
                but is now bound in a lambda *)
             make_lambda bind (close_vars (Constr.Unsafe.liftn 1 1 c))]
          ) in
        let embed_fun := map_under_binders embed_fun1 out_tele_depth 
          (fun ef => 
            (* under the lambda, we first wrap the body in TargS.. *)
            let intermediate := map_under_binders ef rem_tele_depth change_embed in
            (* then we wrap the entire telescopic lambda in another lambda *)
            let result := make_lambda bind (close_vars (Constr.Unsafe.liftn 1 1 intermediate)) in
            result
          ) in
        (close_vars out_tele1, t, rem_tele, embed_fun)
      | _ => 
        (* variable is used, so the result should be:
            out_tele := TeleS (λ x : ?, out_ele)
            out_fun := λ x : ?, out_fun
            rem_tele := λ x : ?, rem_tele
            embed_fun := λ x : ?, λ.. tt : out_tele, λ.. : rem_tele, TargS x embed_fun
           this one is 'easier' in that we do not have to mess around with shifting de Bruijn variables
        *)
        let bound_out_tele := make_lambda bind (close_vars out_tele1) in
        let out_tele := safe_apply '@TeleS@{bi.Quant} [bound_type; bound_out_tele] in
        let rem_tele := make_lambda bind (close_vars rem_tele1) in
        let embed_fun := make_lambda bind (close_vars (map_under_binders embed_fun1 out_tele_depth 
          (fun ef => 
            map_under_binders ef rem_tele_depth change_embed
          )
        )) in
        (out_tele, bound_out, rem_tele, embed_fun)
      end
    | _ => Control.throw Not_found
    end
  ).


Ltac2 rec get_binder_types (tele : constr) : constr list :=
  lazy_match! tele with
  | TeleO => []
  | TeleS ?ntele =>
    match Constr.Unsafe.kind ntele with
    | Constr.Unsafe.Lambda bind inner_tele =>
      Constr.Binder.type bind :: get_binder_types inner_tele
    | _ => Control.throw Not_found
    end
  end.


Ltac2 rec ensure_lambda_tele_depth (in_fun : constr) (tele : constr) :=
  lazy_match! tele with
  | TeleO => in_fun
  | TeleS ?ntele =>
    match Constr.Unsafe.kind ntele with
    | Constr.Unsafe.Lambda bind_tele inner_tele =>
      match Constr.Unsafe.kind in_fun with
      | Constr.Unsafe.Lambda bind_fun fun_body =>
        make_lambda bind_fun (ensure_lambda_tele_depth fun_body inner_tele)
      | _ => (* not syntactically a lambda, but should still be a function *)
        let boundc := { contents := Free.of_goal () } in 
        let base := Option.get (Ident.of_string "x") in
        let depth := get_tele_depth tele in
        (* we first create [depth] different fresh idents, for the binders *)
        let ident_args := List.init depth (fun _ => 
          let result := fresh (boundc.(contents)) base in
          boundc.(contents) := Free.union (boundc.(contents)) (Free.of_ids [result]);
          result
        ) in
        (* now we create the body of the function: 
            - first lift any de Bruijn indices by [depth], amount of new lambdas
            - then apply this term to the new [Rel] arguments *)
        let body0 := safe_apply (Constr.Unsafe.liftn depth 1 in_fun)
          (List.mapi (fun i _ => Constr.Unsafe.make (Constr.Unsafe.Rel (Int.sub depth i))) ident_args) in
        let result := fold_right2 (fun i t body =>
          Constr.Unsafe.make (Constr.Unsafe.Lambda (Constr.Binder.unsafe_make (Some i) Constr.Binder.Relevant t) body)
        ) ident_args (get_binder_types tele) body0 in
        result
      end
    | _ => Control.throw Not_found
    end
  end.


Ltac2 extract_dep (in_tele : constr) (in_fun : constr) :
  (* out_tele * out_fun * rem_tele * embed_fun *)
       constr * constr  * constr   * constr :=
  let depth := get_tele_depth in_tele in
  let in_fun' := ensure_lambda_tele_depth in_fun in_tele in
  extract_dep_int depth in_fun' 0 (Free.of_goal ()).


#[global] Hint Extern 4 (ExtractDep _ _ _ _ _) =>
  ltac2:(
    lazy_match! goal with
    | [ |- @ExtractDep ?ret_type ?in_tele ?in_fun ?out_tele1 ?out_fun1 ?dep_tele1 ?embed_fun1] =>
      let (out_tele, out_fun, rem_tele, embed_fun) := extract_dep in_tele in_fun in
      unify $out_tele1 $out_tele;
      unify $out_fun1 $out_fun;
      unify $dep_tele1 $rem_tele;
      unify $embed_fun1 $embed_fun;
      cbn [ExtractDep tforall tele_bind tele_fold tele_app tele_arg_rect tele_appd_dep];
      intros; exact (eq_refl _)
    end
  ) : typeclass_instances.



Fixpoint tele_eq TT : TT -t> TT -t> Prop :=
  match TT with
  | TeleO => True
  | @TeleS X b => (λ x1 : X, tele_map (λ f x2, 
   tele_bind (λ tt2, ∃ (H : x2 = x1), tele_app f (eq_rect x2 b tt2 _ H))) (tele_eq (b x1)))
  end.

Lemma tele_eq_eq_1 {TT : tele} (y y' : TT) :
  tele_app (tele_app (tele_eq TT) y) y' → y = y'.
Proof.
  induction TT; destruct y.
  - rewrite (tele_arg_O_inv y') //.
  - destruct (tele_arg_S_inv y') as [y1 [y2 ->]] => /=.
    rewrite tele_map_app /=.
    rewrite tele_app_bind /=.
    case => H'. subst.
    move => /H -> //.
Qed.

Lemma tele_eq_eq_2 {TT : tele} (y y' : TT) :
  y = y' → tele_app (tele_app (tele_eq TT) y) y'.
Proof.
  move => -> {y}.
  induction TT; destruct y' => //=.
  rewrite tele_map_app tele_app_bind.
  exists eq_refl => /=.
  by apply H.
Qed.

Lemma tele_eq_app_both {TT : tele} (tt : TT) : tele_app (tele_app (tele_eq TT) tt) tt.
Proof. by apply tele_eq_eq_2. Qed.

Class SimplTeleEq (TT : tele) (teq : TT -t> TT -t> Prop) :=
  teq_spec : (∀.. tt1 tt2, tele_app (tele_app teq tt1) tt2 ↔ tele_app (tele_app (tele_eq TT) tt1) tt2).

Global Instance simpl_teleO_eq : SimplTeleEq [tele] True | 50.
Proof. rewrite /SimplTeleEq //=. Qed.

Global Instance simpl_teleS_nondep_eq (A : Type) (TT : tele) teq : 
  SimplTeleEq TT teq →
  SimplTeleEq (TeleS (λ _ : A, TT)) (λ a1, tele_bind (λ tt1, λ a2, tele_bind (λ tt2, a1 = a2 ∧ tele_app (tele_app teq tt1) tt2))) | 50.
Proof.
  rewrite /SimplTeleEq => /tforall_forall Hteq /= a1.
  apply tforall_forall => tt1 a2.
  apply tforall_forall => tt2.
  rewrite !tele_map_app !tele_app_bind.
  specialize (Hteq tt1). 
  apply (dep_eval_tele tt2) in Hteq; simpl in Hteq.
  split.
  - case => H /Hteq Heq. subst. by exists eq_refl.
  - case => H. subst => /Hteq Heq. eauto.
Qed.

Global Instance simpl_teleS_dep_eq (A : Type) (TTf : A → tele) teq :
  (∀ a, SimplTeleEq (TTf a) (teq a)) →
  SimplTeleEq (TeleS TTf) (λ a1, tele_bind (λ tt1, λ a2, tele_bind (λ tt2, ∃ (H : a2 = a1), tele_app (tele_app (teq a1) tt1) (eq_rect a2 TTf tt2 _ H)))) | 100.
Proof.
  rewrite /SimplTeleEq => + /= a1.
  move => /(dep_eval a1) Heq.
  apply tforall_forall => tt1 a2.
  apply tforall_forall => tt2.
  revert Heq; move => /(dep_eval_tele tt1) Heq.
  rewrite !tele_map_app !tele_app_bind.
  split.
  - case => H. 
    subst => /=. 
    apply (dep_eval_tele tt2) in Heq. simpl in Heq.
    move => /Heq Ht.
    by exists eq_refl.
  - case => H. 
    subst => /=. 
    apply (dep_eval_tele tt2) in Heq. simpl in Heq.
    move => /Heq Ht.
    by exists eq_refl.
Qed.

Global Hint Extern 0 (tforall _) =>
  progress cbn [TeleConcatType tele_appd_dep] : typeclass_instances.


Class TCTForall {TT : tele} (P : TT → Prop) := 
  tctforall : ∀.. (tt : TT), P tt
.

Global Hint Mode TCTForall ! ! : typeclass_instances.

Global Hint Extern 4 (TCTForall _) =>
  unfold TCTForall;
  progress cbn [tforall tele_fold tele_bind tele_app TelePairType TeleConcatType tele_appd_dep tele_arg_rect];
  intros : typeclass_instances.


Notation "'TC∀..' x .. y , P" := (TCTForall (λ x, .. (TCTForall (λ y, P)) .. ))
  (at level 200, x binder, y binder, right associativity,
  format "TC∀..  x  ..  y ,  P") : stdpp_scope.


Set Universe Polymorphism.

(* 'hack' to get around universe constraints on the regular option type *)
Inductive option2 (A : Type) :=
  | Some2 : A → option2 A
  | None2 : option2 A.

Arguments None2 {A}.
Arguments Some2 {A} _.

Unset Universe Polymorphism.









