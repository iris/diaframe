From iris.proofmode Require Import base coq_tactics reduction tactics environments.
From iris.bi Require Import bi telescopes.


(* This file defines various utility typeclasses used throughout Diaframe. It also
   comes with related lemmas and Proper instances. Instances for the typeclasses defined here
   can be found in util_instances.v *)

Import bi.

Section modalities.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  (* Mainly used to check that modalities are introducable, ie [ModWeaker id M] *)
  Class ModWeaker M1 M2 :=
    mod_weaker P : M1 P ⊢ M2 P.

  Class ModalityMono M :=
    modality_mono P Q : (P ⊢ Q) → (M P ⊢ M Q).

  Class ModalityStrongMono M := {
    #[global] modality_strong_is_mono :: ModalityMono M;
    modality_strong_frame_l P Q : M P ∗ Q ⊢ M (P ∗ Q)
  }.

  Class ModalityCompat3 M1 M2 M3 :=
    modality_compat3 P : M2 $ M3 P ⊢ M1 P.

  (* Split off modality M2 from M, keeps M *)
  Class SplitModality3 M M1 M2 := {
    split_modality3_compat : ModalityCompat3 M M1 M2;
    split_modality3_left : ModalityStrongMono M1;
    split_modality3_right : ModalityStrongMono M2;
  }.

  (* This is the same as above, but M1 and M2 are output *)
  Class SplitLeftModality3 M M1 M2 := split_left_modality3 : SplitModality3 M M1 M2.
  (* the same as above, but saving a syntactic reference to the original M *)
  Class SplitLeftModality3_Save M M1 M2 := split_left_modality3_save : SplitLeftModality3 M M1 M2.
  Class StripSaved M M1 := strip_saved_mod P : M P ⊣⊢ M1 P.

  Class ModalityCompat4 M1 M2 M3 M4 :=
    modality_compat4 P : M2 $ M3 $ M4 P ⊢ M1 P.

  (* Used internally in recursive hint search *)
  Class SplitModality4 M M1 M2 M3 := {
    split_modality4_compat : ModalityCompat4 M M1 M2 M3;
    split_modality4_left : ModalityStrongMono M1;
    split_modality4_mid : ModalityStrongMono M2;
    split_modality4_right : ModalityStrongMono M3;
  }.

  Class NotIntroducable M := {
    mod_not_introducable : True
  }.

  (* Used to gather modalities around PROPS that are 'free' to get *)
  Class PrependModality B M B' :=
    prepend_modality : M B' ⊣⊢ B.

  (* Used to cover some cases for disjunctive hints. *)
  Class CoIntroducable M1 M2 :=
    co_introducable P : P ⊢ M1 $ M2 P.

  Class CombinedModalitySafe M1 M2 M :=
    combined_modality_safe P : M P ⊣⊢ M1 $ M2 P.

  Class CombinedModality M1 M2 M :=
    combined_modality P : M P ⊢ M1 $ M2 P.

  Class Collect1Modal P M P' :=
    collect_one_modal : M P' ⊢@{PROP} P.
End modalities.

Global Notation IntroducableModality M := (ModWeaker id M).

Global Hint Mode NotIntroducable + + : typeclass_instances.
Global Hint Mode ModalityCompat3 + ! - ! : typeclass_instances.
Global Hint Mode ModalityCompat3 + ! ! - : typeclass_instances.
Global Hint Mode ModalityCompat3 + - ! ! : typeclass_instances.
Global Hint Mode ModalityCompat4 + ! - ! - : typeclass_instances.
Global Hint Mode ModalityStrongMono + ! : typeclass_instances.
Global Hint Mode ModalityMono + ! : typeclass_instances.
Global Hint Mode SplitModality3 + ! - ! : typeclass_instances.
Global Hint Mode SplitModality3 + ! ! - : typeclass_instances.
Global Hint Mode SplitModality4 + ! - ! - : typeclass_instances.
Global Hint Mode SplitLeftModality3 + ! - - : typeclass_instances.
Global Hint Mode SplitLeftModality3_Save + ! - - : typeclass_instances.
Global Hint Mode StripSaved + ! - : typeclass_instances.
Global Hint Mode CoIntroducable + ! - : typeclass_instances.
Global Hint Mode PrependModality + ! - - : typeclass_instances.
Global Hint Mode CombinedModality + ! ! - : typeclass_instances.
Global Hint Mode Collect1Modal + ! - - : typeclass_instances.


(* Definitions of markers [ε₀], [ε₁] and [χ] *)

Definition empty_hyp_goal_def {PROP : bi} : PROP := emp%I.
(* True would also be an option, but then FindInExtendedContext only works in affine logics. *)

Definition empty_hyp_first_aux : seal (@empty_hyp_goal_def). Proof. by eexists. Qed.
Definition empty_hyp_first := empty_hyp_first_aux.(unseal).
Global Opaque empty_hyp_first.
Global Arguments empty_hyp_first {_} : simpl never.
Lemma empty_hyp_first_eq {PROP : bi} : @empty_hyp_first PROP = emp%I.
Proof. fold (@empty_hyp_goal_def PROP). by rewrite -empty_hyp_first_aux.(seal_eq). Qed.
Global Notation "'ε₀'" := (empty_hyp_first) (at level 100) : bi_scope.

(* Currently unused marker. TODO: delete? *)
Definition empty_hyp_last_spatial_aux : seal (@empty_hyp_goal_def). Proof. by eexists. Qed.
Definition empty_hyp_last_spatial := empty_hyp_last_spatial_aux.(unseal).
Global Opaque empty_hyp_last_spatial.
Global Arguments empty_hyp_last_spatial {_} : simpl never.
Lemma empty_hyp_last_spatial_eq {PROP : bi} : @empty_hyp_last_spatial PROP = emp%I.
Proof. fold (@empty_hyp_goal_def PROP). by rewrite -empty_hyp_last_spatial_aux.(seal_eq). Qed.

Definition empty_hyp_last_aux : seal (@empty_hyp_goal_def). Proof. by eexists. Qed.
Definition empty_hyp_last := empty_hyp_last_aux.(unseal).
Global Opaque empty_hyp_last.
Global Arguments empty_hyp_last {_} : simpl never.
Lemma empty_hyp_last_eq {PROP : bi} : @empty_hyp_last PROP = emp%I.
Proof. fold (@empty_hyp_goal_def PROP). by rewrite -empty_hyp_last_aux.(seal_eq). Qed.
Global Notation "'ε₁'" := (empty_hyp_last) (at level 100) : bi_scope.

Definition empty_goal_aux : seal (@empty_hyp_goal_def). Proof. by eexists. Qed.
Definition empty_goal := empty_goal_aux.(unseal).
Global Opaque empty_goal.
Global Arguments empty_goal {_} : simpl never.
Lemma empty_goal_eq {PROP : bi} : @empty_goal PROP = emp%I.
Proof. fold (@empty_hyp_goal_def PROP). by rewrite -empty_goal_aux.(seal_eq). Qed.
Global Notation "'χ'" := (empty_goal) (at level 100) : bi_scope.


(* cfupd Ec E1 E2 somehow can only be opened if the left-most mask contains Ec.
    it says that this resource has 'exclusive' ownership of the Ec mask. So to open it, the mask must still have Ec.
    No one else can get a closing mask.
   The main function of [cfupd] is to prevent trying to open invariants again, when the proof obligation of
   the closing view shift indicates the resources inside the invariants are relevant. Without [cfupd],
   it only becomes apparent that this is impossible when it is already to late, and may cause loops.  *)
Definition cfupd_def `{!FUpd PROP} (Ec : coPset.coPset) E1 E2 : PROP → PROP := fupd E1 E2.
Definition cfupd_aux : seal (@cfupd_def). Proof. by eexists. Qed.
Definition cfupd := cfupd_aux.(unseal). 
Global Opaque cfupd.
Global Arguments cfupd {_ _} Ec E1 E2 : simpl never.
Lemma cfupd_eq `{!FUpd PROP} Ec : cfupd (PROP := PROP) Ec = fupd.
Proof. change (fupd) with (cfupd_def (PROP := PROP) Ec). by rewrite -cfupd_aux.(seal_eq). Qed.

Global Notation "|=c{ Ec }={ E1 , E2 }=> Q" := (cfupd Ec E1 E2 Q) 
    (at level 99, Ec, E1, E2 at level 50, Q at level 200) : bi_scope.
Global Notation "P =c{ Ec }={ E1 , E2 }=∗ Q" := (P -∗ |=c{Ec}={E1,E2}=> Q)%I 
    (at level 99, Ec, E1, E2 at level 50, Q at level 200) : bi_scope.

(* In the recursive rules for abduction hints, we sometimes need to keep around a modality for the
   remaining goal. We could use just SplitLeftModality3, but this means we lose knowledge of the
   left-most mask. This is problematic, because it means cfupd above will fail, while we do 
   want to allow opening invariants for single goals. *)
Definition saved_left_mod_def {PROP : bi} (Ml M : PROP → PROP) : PROP → PROP := M.
Definition saved_left_mod_aux : seal (@saved_left_mod_def). Proof. by eexists. Qed.
Definition saved_left_mod := saved_left_mod_aux.(unseal).
Global Opaque saved_left_mod.
Global Arguments saved_left_mod {_} _ _ : simpl never.
Lemma saved_left_mod_eq : @saved_left_mod = (λ PROP _ M, M).
Proof. etrans. apply saved_left_mod_aux.(seal_eq). done. Qed.


(* Utility classes to find a hypothesis satisfying some typeclass PTC. 
  [FindInContext] only looks through hypotheses that are actually present. *)
Class FindInContext {PROP : bi} (Δ : envs PROP) (PTC : bool → PROP → Prop) (i : ident) (p : bool) (P : PROP) := {
  fic_lookup : envs_lookup i Δ = Some (p, P);
  fic_satisfies : PTC p P
}.

Global Arguments fic_satisfies {PROP Δ PTC i p P}.

(* [FindInExtendedContext] also looks at the markers [ε₀] and [ε₁] *)
Inductive FindInExtendedContext {PROP : bi} (Δ : envs PROP) (PTC : bool → PROP → Prop) : option ident → bool → PROP → Prop := 
| fic_ext_lookup_Some i p P : envs_lookup i Δ = Some (p, P) → PTC p P → FindInExtendedContext Δ PTC (Some i) p P
| fic_ext_lookup_empty_first : PTC false (ε₀)%I → FindInExtendedContext Δ PTC None false empty_hyp_first
| fic_ext_lookup_empty_last_spatial : PTC false empty_hyp_last_spatial → FindInExtendedContext Δ PTC None false empty_hyp_last_spatial
| fic_ext_lookup_empty_last : PTC false (ε₁)%I → FindInExtendedContext Δ PTC None false empty_hyp_last.

Existing Class FindInExtendedContext.

Definition envs_option_delete {PROP : bi} (rp : bool) (mi : option ident) (p : bool) (Δ : envs PROP) : envs PROP :=
  match mi with
  | Some i => envs_delete rp i p Δ
  | None => Δ
  end.


(* Now come utility classes for inspecting hypotheses. Iris's native [FromSep] classes are sometimes a bit
   too clever for our purposes -- this is because Iris's [From..] and [Into..] classes indicate that the
   user wishes to see the PROP as a separating conjunction or forall, while our classes should only find an
   instances if that is (almost) syntactically the case *)

Class FromSepCareful {PROP : bi} (P Q1 Q2 : PROP) :=
  from_sep_careful : Q1 ∗ Q2 ⊢ P.
Global Hint Mode FromSepCareful + ! - - : typeclass_instances.

Class FromOrCareful {PROP : bi} (P Q1 Q2 : PROP) :=
  from_or_careful : Q1 ∨ Q2 ⊢ P.
Global Hint Mode FromOrCareful + ! - - : typeclass_instances.

Class FromExistCareful {PROP : bi} {A} (P : PROP) (Φ : A → PROP) :=
  from_exist_careful : (∃ x, Φ x) ⊢ P.
Global Arguments FromExistCareful {_ _} _%_I _%_I : simpl never.
Global Arguments from_exist_careful {_ _} _%_I _%_I {_}.
Global Hint Mode FromExistCareful + - ! - : typeclass_instances.

Class FromTExist {PROP : bi} (Q : PROP) (TT : tele) (Q' : TT -t> PROP) :=
  from_texist : (∃.. tt, tele_app Q' tt) ⊢ Q.
Global Hint Mode FromTExist + ! - - : typeclass_instances.

Class FromTExistDirect {PROP : bi} (Q : PROP) (TT : tele) (Q' : TT -t> PROP) :=
  #[global] from_texist_direct :: FromTExist Q TT Q'.
Global Hint Mode FromTExistDirect + ! - - : typeclass_instances.

Class FromAndCareful {PROP : bi} (P Q1 Q2 : PROP) :=
  from_and_careful : Q1 ∧ Q2 ⊢ P.
Global Hint Mode FromAndCareful + ! - - : typeclass_instances.

Class IntoTExist {PROP : bi} (Q : PROP) (TT : tele) (Q' : TT -t> PROP) :=
  into_texist : Q ⊢ (∃.. tt, tele_app Q' tt).
Global Hint Mode IntoTExist + ! - - : typeclass_instances.

Class IntoExistCareful {PROP : bi} (Q : PROP) {A} (Q' : A → PROP) :=
  into_exist_careful : Q ⊢ (∃ a, Q' a). (* used on introduction *)
Global Hint Mode IntoExistCareful + ! - - : typeclass_instances.

Class IntoExistCareful2 {PROP : bi} (Q : PROP) {A} (Q' : A → PROP) :=
  into_exist_careful2 : IntoExistCareful Q Q'. (* used in recursive biabduction search *)
Global Hint Mode IntoExistCareful2 + ! - - : typeclass_instances.

Class IntoForallCareful {PROP : bi} (Q : PROP) {A} (Q' : A → PROP) :=
  into_forall_careful : Q ⊢ (∀ a, Q' a).
Global Hint Mode IntoForallCareful + ! - - : typeclass_instances.

Class IntoTExistExact {PROP : bi} (Q : PROP) (TT : tele) (Q' : TT -t> PROP) :=
  #[global] into_texist_exact :: IntoTExist Q TT Q'.
Global Hint Mode IntoTExistExact + ! - - : typeclass_instances.

Class LaterToExcept0 {PROP : bi} (P P' : PROP) :=
  as_except_0 : ▷ P ⊢ ◇ P'.
Global Hint Mode LaterToExcept0 + ! - : typeclass_instances.

Class IntoSepCareful {PROP : bi} (P Q1 Q2 : PROP) :=
  into_sep_care : P ⊢ Q1 ∗ Q2.
Global Hint Mode IntoSepCareful + ! - - : typeclass_instances.

Class IntoWand2 {PROP : bi} p (P V P' : PROP) := 
  into_wand2 : □?p P ⊢ V -∗ P'.
Global Hint Mode IntoWand2 + ! ! - - : typeclass_instances.

Class NormalizeProp {PROP : bi} P Q (made_progress : bool) :=
  normalize_prop : P ⊣⊢@{PROP} Q.
Global Hint Mode NormalizeProp + ! - + : typeclass_instances.
Global Hint Mode NormalizeProp + ! - - : typeclass_instances.

Class NormalizedProp {PROP : bi} (P Q : PROP) (made_progress : bool) :=
  normalized_prop : NormalizeProp P Q made_progress.
Global Hint Mode NormalizedProp + ! - + : typeclass_instances.
Global Hint Mode NormalizedProp + ! - - : typeclass_instances.

Class TCOrB (b1 b2 b3 : bool) :=
  tc_orb_eq : b1 || b2 = b3.
Global Hint Mode TCOrB + + - : typeclass_instances.

Class SimplifyPropBinOp {PROP : bi} (op : PROP → PROP → PROP) P Q R p :=
  simplify_prop_op : NormalizeProp (op P Q) R p.
Global Hint Mode SimplifyPropBinOp + + ! ! - + : typeclass_instances.
Global Hint Mode SimplifyPropBinOp + + ! ! - - : typeclass_instances.

Class SolveSepSideCondition (φ : Prop) :=
  side_condition_holds : φ.
Global Hint Mode SolveSepSideCondition + : typeclass_instances. 
(* The + instead of ! is crucial here: we rely on the fact that SolveSepSideCondition does not unify evars *)

Class SimplifyPureHyp (φin φout : Prop) :=
  simplify_pure_hyp : φin → φout.
Global Hint Mode SimplifyPureHyp + - : typeclass_instances.

Class SimplifyPureHypSafe (φin φout : Prop) :=
  simplify_pure_hyp_safe : φin ↔ φout.
Global Hint Mode SimplifyPureHypSafe + - : typeclass_instances.

Class FromLaterN {PROP : bi} Q n Q' :=
  from_latern : ▷^n Q' ⊢@{PROP} Q.
Global Hint Mode FromLaterN + ! ! - : typeclass_instances.

Class IntoLaterNMax {PROP : bi} P n P' :=
  into_latern : P ⊢@{PROP} ▷^n P'.
Global Hint Mode IntoLaterNMax ! ! - - : typeclass_instances.

Class FromLaterNMax {PROP : bi} P n P' :=
  from_laternmax : ▷^n P' ⊢@{PROP} P.
Global Hint Mode FromLaterNMax ! ! - - : typeclass_instances.

Class IntoModal {PROP : bi} p (P : PROP) M (Q : PROP) :=
  into_modal : □?p P ⊢@{PROP}  M Q.
Global Hint Mode IntoModal + ! ! - - : typeclass_instances.


Class AtomIntoConnective {PROP : bi} (P : PROP) (Q : PROP) := 
  atom_into_connective : P ⊢ Q.

Global Hint Mode AtomIntoConnective + ! - : typeclass_instances.

Class AtomAndConnective {PROP : bi} (p : bool) (P : PROP) := {
  atom_and_connective : True
}.
Global Hint Mode AtomAndConnective + ! ! : typeclass_instances.

Class AtomIntoWand {PROP : bi} p (P V P' : PROP) := 
  atom_into_wand : IntoWand2 p P V P'.
Global Existing Instance atom_into_wand | 0.
Global Hint Mode AtomIntoWand + ! ! - - : typeclass_instances.

Class AtomIntoForall {PROP : bi} (Q : PROP) {A} (Q' : A → PROP) :=
  atom_into_forall : IntoForallCareful Q Q'.
Global Existing Instance atom_into_forall | 0.
Global Hint Mode AtomIntoForall + ! - - : typeclass_instances.

Class AtomIntoExist {PROP : bi} (Q : PROP) {A} (Q' : A → PROP) :=
  atom_into_exist : IntoExistCareful2 Q Q'.
Global Existing Instance atom_into_exist | 0.
Global Hint Mode AtomIntoForall + ! - - : typeclass_instances.
(* Not using :: syntax to explicitly declare precedence level *)

(* This class is used to determine whether some goals Q, if proving M Q, should be tried as M (Q ∗ emp).
   Current only use is in the recursive or rules for constructing Abduction hints. *)
Class IsEasyGoal {PROP : bi} (Q : PROP) :=
  is_easy_goal : True.
Global Hint Mode IsEasyGoal + ! : typeclass_instances.

Class FracSub (p q : Qp) mr :=
  frac_sub : match mr with | Some r => (r + q)%Qp | None => q end = p.
Global Hint Mode FracSub ! ! - : typeclass_instances.

Class ExclusiveProp {PROP : bi} (Q : PROP) :=
  exclusive_prop : Q ∗ Q ⊢ False.
Global Hint Mode ExclusiveProp + ! : typeclass_instances.

Class AsEmpValidWeak {PROP : bi} (φ : Prop) (P : PROP) :=
  as_emp_valid_weak : (⊢ P) → φ.
Global Hint Mode AsEmpValidWeak - ! - : typeclass_instances.



(* miscellaneous other stuff *)
Class IsEvarFree {A : Type} (a : A) := { has_evar : True }.
Global Hint Mode IsEvarFree + + : typeclass_instances.
(* This Hint Mode does all the work, precludes the need for the has_evar tactic *)

(* Some additional stuff on relations, used for Proper instances and rewriting *)

Section relation_instances.
  Context {A : Type}.

  (* relation_conjunction is from Coq's RelationClasses, but this instance is missing. *)
  Global Instance conj_proper (R1 R2 : relation A) a :
    ProperProxy R1 a →
    ProperProxy R2 a → (* By requiring ProperProxy instances, we force R1 and R2 not to be evars, which avoids a loop here. *)
    Proper (relation_conjunction R1 R2) a.
  Proof. rewrite /Proper /ProperProxy => HR1 HR2; by split. Qed.

  (* [either_holds] is used as a condition for some Proper instances *)
  Definition either_holds (pred : A → Prop) : relation A
    := (λ l r, pred l ∨ pred r).

  Global Arguments either_holds pred _ _ /.

  (* pred will be a typeclass, so this works *)
  Global Instance either_holds_proper (pred : A → Prop) a :
    pred a → Proper (either_holds pred) a.
  Proof. rewrite /Proper /=; by left. Qed.

  (* This is also sometimes used as a condition for Proper instances *)
  Global Instance always_proper b : ProperProxy (λ _ _ : bool, True) b.
  Proof. done. Qed.
End relation_instances.


(* Lemmas *)
Section class_lemmas.
  Context {PROP : bi}.
  Implicit Type M : PROP → PROP.

  Proposition modality_strong_frame_r M P Q : 
    ModalityStrongMono M → (P ∗ M Q ⊢ M (Q ∗ P)).
  Proof.
    rewrite bi.sep_comm => HM.
    by rewrite modality_strong_frame_l.
  Qed.

  Lemma modality_strong_mono_equiv_1 M : ModalityStrongMono M → ∀ P Q, (P -∗ Q) ⊢ M P -∗ M Q.
  Proof.
    case => HM1 HM2 P Q.
    apply bi.wand_intro_l.
    rewrite HM2. apply HM1.
    by apply bi.wand_elim_r'.
  Qed.

  Lemma modality_strong_mono_equiv_2 M : (∀ P Q, (P -∗ Q) ⊢ M P -∗ M Q) → ModalityStrongMono M.
  Proof.
    split => P Q.
    - move => HPQ.
      iStartProof. iApply H. iApply HPQ.
    - iIntros "[HMP HQ]".
      iAssert (P -∗ P ∗ Q)%I with "[$HQ]" as "Hw"; first eauto.
      by iApply (H with "Hw").
  Qed.

  Global Instance mod_weaker_subrelation :
    subrelation ModWeaker (pointwise_relation _ (⊢@{PROP})).
  Proof. done. Qed.

  Global Instance modality_mono_proper_equiv : 
    Proper (pointwise_relation _ (⊣⊢@{PROP}) ==> (iff)) ModalityMono.
  Proof.
    move => M M' HM'.
    split => HM P Q HPQ; (rewrite -HM' || rewrite HM'); by apply HM.
  Qed.

  Global Instance modality_ec_proper :
    Proper (pointwise_relation _ (⊣⊢@{PROP}) ==> (iff)) ModalityStrongMono.
  Proof.
    move => M M' HM'.
    split => HM; split; try (rewrite -HM' || rewrite HM'; apply _); move => P Q;
    (rewrite -HM' || rewrite HM'); by apply HM.
  Qed.

  Lemma different_modalities_mono M M' : 
    relation_conjunction (pointwise_relation _ (⊢@{PROP})) (either_holds ModalityMono) M M' →
    ∀ P Q, (P ⊢ Q) → M P ⊢ M' Q.
  Proof.
    move => [HM [HMl | HMr]] P Q HPQ.
    - rewrite -(HM Q); by apply HMl.
    - rewrite HM; by apply HMr.
  Qed.

  Lemma different_modalities_mono_flip M M' : 
    relation_conjunction (pointwise_relation _ (flip (⊢@{PROP}))) (either_holds ModalityMono) M M' →
    ∀ P Q, (P ⊢ Q) → M' P ⊢ M Q.
  Proof.
    move => [HM [HMl | HMr]] P Q HPQ.
    - rewrite HM; by apply HMl.
    - rewrite -HM; by apply HMr.
  Qed.

  Lemma different_modalities_mono_equiv M M' : 
    relation_conjunction (pointwise_relation _ (⊣⊢@{PROP})) (either_holds ModalityMono) M M' →
    ∀ P Q, (P ⊣⊢ Q) → M P ⊣⊢ M' Q.
  Proof.
    move => [HM [HMl | HMr]] P Q HPQ; [rewrite -HM | rewrite HM];
    apply (anti_symm _); (apply HMl || apply HMr); by rewrite HPQ.
  Qed.

  Lemma findincontext_spec rp PTC (Δ : envs PROP) i p P :
    FindInContext Δ PTC i p P →
    of_envs Δ ⊢ □?p P ∗ of_envs (envs_delete rp i p Δ).
  Proof.
    case => HΔi HpP.
    rewrite envs_lookup_sound' //.
  Qed.

  Lemma findinextcontext_spec rp PTC (Δ : envs PROP) i p P :
    FindInExtendedContext Δ PTC i p P →
    of_envs Δ ⊢ □?p P ∗ of_envs (envs_option_delete rp i p Δ).
  Proof.
    case => [{i p P}| | | ].
    - move => i p P HΔi HpP.
      rewrite envs_lookup_sound' //=.
    - move => _ /=.
      by rewrite empty_hyp_first_eq left_id.
    - move => _ /=.
      by rewrite empty_hyp_last_spatial_eq left_id.
    - move => _ /=.
      by rewrite empty_hyp_last_eq left_id.
  Qed.

  Lemma ficext_satisfies (Δ : envs PROP) PTC i p P :
    FindInExtendedContext Δ PTC i p P →
    PTC p P.
  Proof. case => //. Qed.

  Lemma ficext_none_free (Δ : envs PROP) PTC p P :
    FindInExtendedContext Δ PTC None p P →
    emp ⊣⊢ P.
  Proof.
    move => HΔ. inversion HΔ.
    - by rewrite empty_hyp_first_eq.
    - by rewrite empty_hyp_last_spatial_eq.
    - by rewrite empty_hyp_last_eq.
  Qed.
End class_lemmas.





