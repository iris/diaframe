From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import tele_utils util_classes.
From iris.bi Require Export bi telescopes.

Import bi.

(* This file defines the goal and hint formats used by Diaframe's automation.
   Instead of operating directly on IPM goals, Diaframe first wraps goals in a 
   separately defined goal format. This makes it possible to execute steps of 
   the automation more efficiently, and in a stepwise fashion.

   Some of the definitions are 'sealed', to prevent Coq from being too smart.
   This file also contains Proper instances which are sometimes useful.
*)


(* First: the goals targeted by the automation *)

Definition SolveSep_def {PROP : bi} {TT} (M : PROP → PROP) (P Q : TT -t> PROP) : PROP :=
  M (∃.. (tt : TT), tele_app P tt ∗ tele_app Q tt)%I.

Definition SolveSep_aux : seal (@SolveSep_def). Proof. by eexists. Qed.
Definition SolveSep := SolveSep_aux.(unseal).
Global Opaque SolveSep.
Global Arguments SolveSep {_ _} _ _ _ : simpl never.
Lemma SolveSep_eq : @SolveSep = @SolveSep_def.
Proof. rewrite -SolveSep_aux.(seal_eq) //. Qed.


Definition SolveOne_def {PROP : bi} {TT} (M : PROP → PROP) (P : TT -t> PROP) : PROP :=
  M (∃.. (tt : TT), tele_app P tt)%I.

Definition SolveOne_aux : seal (@SolveOne_def). Proof. by eexists. Qed.
Definition SolveOne := SolveOne_aux.(unseal).
Global Opaque SolveOne.
Global Arguments SolveOne {_ _} _ _ : simpl never.
Lemma SolveOne_eq : @SolveOne = @SolveOne_def.
Proof. rewrite -SolveOne_aux.(seal_eq) //. Qed.


Definition IntroduceHyp_def {PROP : bi} (P Q : PROP) : PROP :=
  (P -∗ Q)%I.

Definition IntroduceHyp_aux : seal (@IntroduceHyp_def). Proof. by eexists. Qed.
Definition IntroduceHyp := IntroduceHyp_aux.(unseal).
Global Opaque IntroduceHyp.
Global Arguments IntroduceHyp {_} _ _ : simpl never.
Lemma IntroduceHyp_eq : @IntroduceHyp = @IntroduceHyp_def.
Proof. rewrite -IntroduceHyp_aux.(seal_eq) //. Qed.


Definition IntroduceVars_def {PROP : bi} {TT : tele@{universes.Quant}} (P : TT -t> PROP) : PROP :=
  (∀.. tt, tele_app P tt)%I.

Definition IntroduceVars_aux : seal (@IntroduceVars_def). Proof. by eexists. Qed.
Definition IntroduceVars := IntroduceVars_aux.(unseal).
Global Opaque IntroduceVars.
Global Arguments IntroduceVars {_ _} _ : simpl never.
Lemma IntroduceVars_eq : @IntroduceVars = @IntroduceVars_def.
Proof. rewrite -IntroduceVars_aux.(seal_eq) //. Qed.


Definition Transform_def {PROP : bi} (P : PROP) : PROP := P.
Definition Transform_aux : seal (@Transform_def). Proof. by eexists. Qed.
Definition Transform := Transform_aux.(unseal).
Global Opaque Transform.
Global Arguments Transform {_} _ : simpl never.
Lemma Transform_eq : @Transform = @Transform_def.
Proof. rewrite -Transform_aux.(seal_eq) //. Qed.


Definition SolveAnd_def {PROP : bi} (P Q : PROP) : PROP := P ∧ Q.

Definition SolveAnd_aux : seal (@SolveAnd_def). Proof. by eexists. Qed.
Definition SolveAnd := SolveAnd_aux.(unseal).
Global Opaque SolveAnd.
Global Arguments SolveAnd {_} _ _ : simpl never.
Lemma SolveAnd_eq : @SolveAnd = @SolveAnd_def.
Proof. rewrite -SolveAnd_aux.(seal_eq) //. Qed.


Definition SolveSepFoc_def {PROP : bi} {TT} (M : PROP → PROP) (P Q : TT -t> PROP) D : PROP :=
  M ((∃.. (tt : TT), tele_app P tt ∗ tele_app Q tt) ∨ D)%I.

Definition SolveSepFoc_aux : seal (@SolveSepFoc_def). Proof. by eexists. Qed.
Definition SolveSepFoc := SolveSepFoc_aux.(unseal).
Global Opaque SolveSepFoc.
Global Arguments SolveSepFoc {_ _} _ _ _ _ : simpl never.
Lemma SolveSepFoc_eq : @SolveSepFoc = @SolveSepFoc_def.
Proof. rewrite -SolveSepFoc_aux.(seal_eq) //. Qed.

Definition SolveOneFoc_def {PROP : bi} {TT} (M : PROP → PROP) (P : TT -t> PROP) D : PROP :=
  M ((∃.. (tt : TT), tele_app P tt) ∨ D)%I.

Definition SolveOneFoc_aux : seal (@SolveOneFoc_def). Proof. by eexists. Qed.
Definition SolveOneFoc := SolveOneFoc_aux.(unseal).
Global Opaque SolveOneFoc.
Global Arguments SolveOneFoc {_ _} _ _ _ : simpl never.
Lemma SolveOneFoc_eq : @SolveOneFoc = @SolveOneFoc_def.
Proof. rewrite -SolveOneFoc_aux.(seal_eq) //. Qed.


Ltac unseal_solvesep := rewrite ?SolveSep_eq ?/SolveSep_def.
Ltac unseal_solveone := rewrite ?SolveOne_eq ?/SolveOne_def.
Ltac unseal_introhyp := rewrite ?IntroduceHyp_eq ?/IntroduceHyp_def.
Ltac unseal_introvar := rewrite ?IntroduceVars_eq ?/IntroduceVars_def.
Ltac unseal_transform := rewrite ?Transform_eq ?/Transform_def.
Ltac unseal_solveand := rewrite ?SolveAnd_eq ?/SolveAnd_def.
Ltac unseal_solvesepfoc := rewrite ?SolveSepFoc_eq ?/SolveSepFoc_def.
Ltac unseal_solveonefoc := rewrite ?SolveOneFoc_eq ?/SolveOneFoc_def.

Ltac unseal_diaframe := unseal_solvesep; unseal_solveone; unseal_introhyp; unseal_introvar; unseal_transform; 
    unseal_solveand; unseal_solvesepfoc; unseal_solveonefoc.


(* next: some helper classes to go to the goals targeted by the automation *)

Class CollectModal {PROP : bi} (P : PROP) (M : PROP → PROP) (P' : PROP) :=
  collect_modal : M P' ⊢@{PROP} P.

Global Hint Mode CollectModal + ! - - : typeclass_instances.

Class AsSolveGoal {PROP : bi} (M : PROP → PROP) (Pin Pout : PROP) :=
  as_solve_goal : Pout ⊢ M Pin.

Global Hint Mode AsSolveGoal + ! ! - : typeclass_instances.

Class AsSolveGoalInternal {PROP : bi} (Pin Pout : PROP) :=
  as_solve_goal_internal: Pout ⊢ Pin.

Global Hint Mode AsSolveGoalInternal + ! - : typeclass_instances.

(* this class is used to incorporate left-over modalities in biabddisj hint applications *)
Class AbsorbModal {PROP : bi} (P : PROP) (M : PROP → PROP) (P' : PROP) :=
  absorb_modal : P' ⊢ M P.

Global Hint Mode AbsorbModal + ! ! - : typeclass_instances.

(* this class is used to reabsorb existentials if some branch of disjunct did not use them *)
Class AbsorbExists {PROP : bi} {TT : tele} (P : TT -t> PROP) (P' : PROP) :=
  absorb_exists : P' ⊢ ∃.. (tt : TT), tele_app P tt.

Global Hint Mode AbsorbExists + ! ! - : typeclass_instances.

(* this class is used to determine whether we should choose it if it appears alone as the left-most disjunct,
   without existentials quantification *)
Class CommitLeftDisjunct {PROP : bi} (P : PROP) :=
  { commit_left_disjunct : True }.

Global Hint Mode CommitLeftDisjunct + ! : typeclass_instances.


(* finally: the classes used to prove the goals targeted by the automation *)

Universe diaframe_telefun.

(* Users give instances of [BiAbd] to help prove [SolveSep] goals *)
Class BiAbd {PROP : bi} {TTl TTr : tele@{bi.Quant}} p 
    P (Q : tele_fun@{bi.Quant bi.Logic diaframe_telefun} TTr PROP)
      (M : PROP → PROP) 
    (R : tele_fun@{bi.Quant bi.Logic diaframe_telefun} TTl PROP) 
    (S : TTl -t> (tele_fun@{bi.Quant bi.Logic diaframe_telefun} TTr PROP)) :=
  bi_abd : (∀.. ml : TTl, □?p P ∗ tele_app R ml ⊢ 
              M (∃.. mr : TTr, tele_app Q mr ∗ tele_app (tele_app S ml) mr)).


Global Hint Mode BiAbd + - ! - ! ! - - - : typeclass_instances. 

(* The recursive hint search procedure uses [BiAbdMod] to deal with input modality M. *)
Class BiAbdMod {PROP : bi} {TTl TTr : tele@{bi.Quant}} p P Q (M M1 M2 : PROP → PROP) R S := {
  biabd_mod_biabd : BiAbd (TTl := TTl) (TTr := TTr) p P Q M2 R S;
  biabd_mod_split_modality : SplitModality3 M M1 M2;
}.

Global Hint Mode BiAbdMod + - ! - ! ! ! - - - - : typeclass_instances.


(* disjunction extension of bi-abduction *)
Class BiAbdDisj {PROP : bi} {TTl TTr : tele@{bi.Quant}} p P Q (M : PROP → PROP) R S D :=
  bi_abd_disj : (∀.. ml : TTl, □?p P ∗ tele_app R ml ⊢
              M ((∃.. mr : TTr, tele_app Q mr ∗ tele_app (tele_app S ml) mr) ∨ 
                  (match tele_app D ml with | None2 => False | Some2 D' => D' end))).

Global Hint Mode BiAbdDisj + - ! - ! ! - - - - : typeclass_instances.

(* and its input modality-aware extension *)
Class BiAbdDisjMod {PROP : bi} {TTl TTr : tele@{bi.Quant}} p P Q (M M1 M2 : PROP → PROP) R S D := {
  biabd_disj_mod_biabd : BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 R S D;
  biabd_disj_mod_split_modality : SplitModality3 M M1 M2;
}.

Global Hint Mode BiAbdDisjMod + - ! - ! ! ! - - - - - : typeclass_instances.


(* Users give instances of [Abduct] *)
Class Abduct {PROP : bi} {TT : tele} p P Q (M : PROP → PROP) R :=
  abduct : □?p P ∗ R ⊢ M (∃.. tt : TT, tele_app Q tt).

Global Hint Mode Abduct + + - ! ! - - : typeclass_instances.
(* Both P and Q may accumulate evars when investigating under quantifiers *)

(* [AbductModInput] is like [Abduct], but the modality M is considered an input:
   it is set to be the modality appearing in the goal. The recursive hint
   search procedure generates [AbductModInput]s. *)
Definition AbductModInput {PROP : bi} := @Abduct PROP.

Existing Class AbductModInput.

Global Hint Mode AbductModInput + + - ! ! ! - : typeclass_instances.

(* [AbductModRec] is used internally in the recursive hint search procedure.
   The additional modality Ml is used to absorb modalities into R at the right time *)
Definition AbductModRec {PROP : bi} {TT : tele} p P Q (Mr Ml : PROP → PROP) R :=
  Ml (□?p P ∗ R) ⊢ Mr (∃.. tt : TT, tele_app Q tt).

(* FocDisjEx is used internally for constructing hints on SolveOneFoc goals *)
Definition FocDisjEx_def {PROP : bi} {TT : tele} Q D : PROP :=
  (∃.. (tt : TT), tele_app Q tt) ∨ D. 

Definition FocDisjEx_aux : seal (@FocDisjEx_def). Proof. by eexists. Qed.
Definition FocDisjEx := FocDisjEx_aux.(unseal).
Global Opaque FocDisjEx.
Global Arguments FocDisjEx {_ _} _ _ : simpl never.
Lemma FocDisjEx_eq : @FocDisjEx = @FocDisjEx_def.
Proof. rewrite -FocDisjEx_aux.(seal_eq) //. Qed.

(* If a disjunction is used to construct an Abduction hint, the same goal will appear
   on the right hand side of a ∧. To avoid loops, we need to prepare these goals for 
   retrying, sometimes making them easier (using IsEasyGoal). *)
Class PrepareForRetry {PROP : bi} {TT : tele} (Q Q' : TT -t> PROP) :=
  prepare_for_retry tt : tele_app Q' tt ⊢ tele_app Q tt.

Global Hint Mode PrepareForRetry + ! ! - : typeclass_instances.

(* If we try opening an invariant behind a disjunction, we will need to prove
  |={⊤, ?E} (⌜↑N ⊆ ?E⌝ ∗ R) ∨ D. At that point, needing to prove
    ⌜↑N ⊆ E⌝ indicates we should unify E with ⊤. We add a syntactic
  reference to the outer modality to the term ↑N ⊆ E, so that pure solvers
  can match on this knowledge *)
Definition BehindModal {PROP : bi} (M : PROP → PROP) (φ : Prop) := φ.


(* The [Mergable] classes are responsible for merging relevant hypotheses.
   They are written in CPS style for efficiency reasons (see issue #13).
   Instances of [MergableConsume] can be used to replace [P2 ⊢ P1 -∗ G] with
   [P ⊢ G], consuming [P2] and [P1] to produce [P].
   The parameter [rp] for _r_emove _p_ersistent indicates whether to delete
   [P2] if it is persistent. By default, set this to true: you might otherwise
   inadvertently cause loops in the proof search. *)
Class MergableConsume {PROP : bi} (P1 : PROP) (rp : bool) (C : bool → PROP → PROP → Prop) :=
  mergable_consume p P2 P3 : C p P2 P3 → P1 ∗ □?p P2 ⊢ P3.

(* Instances of [MergablePersist] can be used to replace P1 ⊢ P2 -∗ G with
   P1 ∗ P2 ⊢ □ P3 -∗ G, learning P3 for free. If more than one instance is found,
   [MergableEnv] in introduce_hyp will also learn those. *)
Class MergablePersist {PROP : bi} (P1 : PROP) (C : bool → PROP → PROP → Prop) :=
  mergable_persist p P2 P3 : C p P2 P3 → P1 ∗ □?p P2 ⊢ <pers> P3.


(* [DropModal] is used when introducing hypotheses into the context -- it 
   strips modalities which goal G allows us to strip. For example, it strips
   ▷ modalities of timeless propositions if G is [IsExcept0]. The relevant instance
   sets C equal to Timeless. *)
Class DropModal {PROP : bi} (M : PROP → PROP) (G : PROP) (C : PROP → Prop) :=
  drop_modal P : C P → (P -∗ G) ⊢ M P -∗ G.

Global Hint Mode DropModal + ! ! - : typeclass_instances.


(* [TransformHyp] and [TransformCtx] are used to prove [Transform] goals.
   Instances of [TransformHyp] are sought first, and when none can be found,
   then instances of [TransformCtx] are sought. For [TransformHyp] instances,
   we do not recursively inspect hypothesis [H] *)
Class TransformHyp {PROP : bi} p (H T T' : PROP) :=
  transform_hyp : T' ⊢ □?p H -∗ T.

Class TransformCtx {PROP : bi} (Δ : envs PROP) (T G : PROP) :=
  transform_ctx : envs_entails Δ G → envs_entails Δ T.

Global Hint Mode TransformHyp + + ! ! - : typeclass_instances.
Global Hint Mode TransformCtx + ! ! - : typeclass_instances.


Section proper_instances.
  Context {PROP : bi}.

  Section solve_sep.
    Global Polymorphic Instance solve_sep_proper_equiv {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (⊣⊢)) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊣⊢)) ==> 
              (tele_pointwise _ (⊣⊢)) ==> (⊣⊢)) 
        (SolveSep (PROP := PROP) (TT := TT)).
    Proof. 
      unseal_diaframe => M M' /different_modalities_mono_equiv HM 
        P P' /tforall_forall HPP' R R' /tforall_forall HRR'.
      apply HM.
      apply (anti_symm _); apply bi_texist_mono => a; by rewrite HPP' HRR'.
    Qed.

    Global Polymorphic Instance solve_sep_proper_ent {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (⊢@{PROP})) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊢)) ==> 
              (tele_pointwise _ (⊢)) ==> (⊢)) (SolveSep (PROP := PROP) (TT := TT)).
    Proof.
      unseal_diaframe => M M' /different_modalities_mono HM 
        P P' /tforall_forall HPP' R R' /tforall_forall HRR'.
      apply HM, bi_texist_mono => a.
      by rewrite HPP' HRR'.
    Qed.

    Global Polymorphic Instance solve_sep_proper_tne {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (flip (⊢@{PROP}))) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊢)) --> 
              (tele_pointwise _ (⊢)) --> (flip (⊢))) (SolveSep (PROP := PROP) (TT := TT)).
    Proof.
      unseal_diaframe => M M' /different_modalities_mono_flip HM
        P P' /tforall_forall HPP' R R' /tforall_forall HRR' /=.
      apply HM, bi_texist_mono => a.
      by rewrite HPP' HRR'.
    Qed.

    (* FIXME: I tried making these polymorphic to get HeapLang's [empty_array_solve]
      HINT to not cause universe constraints (bi.Quant <= prod.u0), but to no avail *)

  End solve_sep.

  Section solve_one.
    Global Instance solve_one_proper_equiv {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (⊣⊢)) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊣⊢)) ==> (⊣⊢)) 
        (SolveOne (PROP := PROP) (TT := TT)).
    Proof.
      unseal_diaframe => M M' /different_modalities_mono_equiv HM 
        P P' /tforall_forall HPP'.
      apply HM.
      apply (anti_symm _); apply bi_texist_mono => a; by rewrite HPP'.
    Qed.

    Global Instance solve_one_proper_ent {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (⊢@{PROP})) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊢)) ==> (⊢)) (SolveOne (PROP := PROP) (TT := TT)).
    Proof.
      unseal_diaframe => M M' /different_modalities_mono HM 
        P P' /tforall_forall HPP'.
      apply HM, bi_texist_mono => a.
      by rewrite HPP'.
    Qed.

    Global Instance solve_one_proper_tne {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (flip (⊢@{PROP}))) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊢)) --> (flip (⊢))) (SolveOne (PROP := PROP) (TT := TT)).
    Proof.
      unseal_diaframe => M M' /different_modalities_mono_flip HM
        P P' /tforall_forall HPP' /=.
      apply HM, bi_texist_mono => a.
      by rewrite HPP'.
    Qed.

  End solve_one.

  Section solve_sep_foc.
    Global Instance solve_sep_foc_proper_equiv {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (⊣⊢)) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊣⊢)) ==> 
              (tele_pointwise _ (⊣⊢)) ==>
              (⊣⊢) ==>  (⊣⊢)) 
        (SolveSepFoc (PROP := PROP) (TT := TT)).
    Proof. 
      unseal_diaframe => M M' /different_modalities_mono_equiv HM 
        P P' /tforall_forall HPP' R R' /tforall_forall HRR' D D' HD.
      apply HM. rewrite HD.
      apply (anti_symm _); apply or_mono; try done; apply bi_texist_mono => a; by rewrite HPP' HRR'.
    Qed.

    Global Instance solve_sep_foc_proper_ent {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (⊢@{PROP})) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊢)) ==> 
              (tele_pointwise _ (⊢)) ==>
               (⊢) ==> (⊢)) (SolveSepFoc (PROP := PROP) (TT := TT)).
    Proof.
      unseal_diaframe => M M' /different_modalities_mono HM 
        P P' /tforall_forall HPP' R R' /tforall_forall HRR' D D' HD.
      apply HM, or_mono; last done. apply bi_texist_mono => a.
      by rewrite HPP' HRR'.
    Qed.

    Global Instance solve_sep_foc_proper_tne {TT} :
      Proper ((relation_conjunction (pointwise_relation _ (flip (⊢@{PROP}))) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊢)) --> 
              (tele_pointwise _ (⊢)) -->
              (⊢) --> (flip (⊢))) (SolveSepFoc (PROP := PROP) (TT := TT)).
    Proof.
      unseal_diaframe => M M' /different_modalities_mono_flip HM
        P P' /tforall_forall HPP' R R' /tforall_forall HRR' /= D D' HD.
      apply HM, or_mono; last done. apply bi_texist_mono => a.
      by rewrite HPP' HRR'.
    Qed.

  End solve_sep_foc.

  Section introduce_hyp.
    Global Instance introduce_hyp_proper_equiv :
      Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢))
        (IntroduceHyp (PROP := PROP)).
    Proof. unseal_diaframe; solve_proper. Qed.

    Global Instance introduce_hyp_proper_ent :
      Proper ((⊢) -->  (⊢) ==> (⊢))
        (IntroduceHyp (PROP := PROP)).
    Proof. unseal_diaframe; solve_proper. Qed.

    Global Instance introduce_hyp_proper_tne :
      Proper ((⊢) ==> (⊢) --> (flip (⊢)))
        (IntroduceHyp (PROP := PROP)).
    Proof. unseal_diaframe; solve_proper. Qed.

  End introduce_hyp.

  Section abduct.
    Global Instance abduct_proper_ent {TT : tele} : 
      Proper ((eq) ==> 
              (⊢@{PROP}) --> 
              (tele_pointwise _ (⊢)) ==> 
              (relation_conjunction (pointwise_relation _ (⊢)) (either_holds ModalityMono)) ==> 
              (⊢) --> (impl)) (Abduct (TT := TT)).
    Proof.
      move => p p' <- {p'} P P' HP Q Q' /tforall_forall HQ' M M' /different_modalities_mono HMM' R R' HR.
      rewrite /Abduct /= HP HR => ->.
      apply HMM', bi_texist_mono => tt.
      by rewrite HQ'.
    Qed.

    Global Instance abduct_proper_equiv {TT : tele} : 
      Proper ((eq) ==>
             (⊣⊢@{PROP}) ==>
             (tele_pointwise _ (⊣⊢)) ==>
             (relation_conjunction (pointwise_relation _ (⊣⊢)) (either_holds ModalityMono)) ==> 
             (⊣⊢) ==> (iff)) (Abduct (TT := TT)).
    Proof.
      move => p p' <- {p'} P P' HP Q Q' /tforall_forall HQ' M M' [HMM' HMR] R R' HR.
      assert (ModalityMono M') as HM' by (destruct HMR => //; by rewrite -HMM').
      split => HPS.
      - (eapply abduct_proper_ent; last (apply HPS)) => //=.
        * by rewrite HP.
        * apply tforall_forall => x. by rewrite HQ'.
        * split => // x. by rewrite HMM'.
        * by rewrite HR.
      - (eapply abduct_proper_ent; last (apply HPS)) => //=.
        * by rewrite HP.
        * apply tforall_forall => x. by rewrite HQ'.
        * split => // x. by rewrite HMM'.
        * by rewrite HR.
    Qed.

    Global Instance abduct_proper_tne {TT : tele} : 
      Proper ((eq) ==>
              (⊢@{PROP}) ==> 
              (tele_pointwise _ (flip (⊢))) ==> 
              (relation_conjunction (pointwise_relation _ (flip (⊢))) (either_holds ModalityMono)) ==> 
              (⊢) ==> (flip impl)) (Abduct (TT := TT)).
    Proof.
      move => p p' <- {p'} P P' HP Q Q' /tforall_forall HQ' M M' /different_modalities_mono_flip HMM' R R' HR.
      rewrite /Abduct /= HP HR => ->.
      apply HMM', bi_texist_mono => tt.
      by rewrite HQ'.
    Qed.

  End abduct.

  Section biabd.
    Global Instance biabd_proper_ent {TTl TTr : tele}: 
      Proper ((eq) ==> 
              (flip (⊢)) ==> 
              (tele_pointwise _ (⊢)) ==> 
              (relation_conjunction (pointwise_relation _ (⊢)) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (flip (⊢))) ==> 
              (tele_pointwise _ (tele_pointwise _ (⊢))) ==> (impl))
        (BiAbd (TTl := TTl) (TTr := TTr) (PROP := PROP)).
    Proof.
      move => p p' <- {p'} P P' HP Q Q' /tele_pointwise_app HQ 
        M M' /different_modalities_mono HMM' S S' /tele_pointwise_app HS 
          T T' /tele_pointwise_app HT /tforall_forall HPQST.
      rewrite /BiAbd tforall_forall => ttl. 
      rewrite HS HP {HS HP} HPQST.
      apply HMM', bi_texist_mono => ttr.
      rewrite HQ; revert HT. 
      move => /(dep_eval ttl) /tforall_forall -> //.
    Qed.

    Global Instance biabd_proper_equiv {TTl TTr : tele} : 
      Proper ((eq) ==> 
              (⊣⊢) ==> 
              (tele_pointwise _ (⊣⊢)) ==> 
              (relation_conjunction (pointwise_relation _ (⊣⊢)) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊣⊢)) ==> 
              (tele_pointwise _ (tele_pointwise _ (⊣⊢))) ==> (iff))
        (BiAbd (TTl := TTl) (TTr := TTr) (PROP := PROP)).
    Proof.
      move => p p' <- {p'} P P' HP Q Q' /tele_pointwise_app HQ 
        M M' [HMM' HMR] S S' /tele_pointwise_app HS T T' /tele_pointwise_app HT.
      rewrite /BiAbd !tforall_forall.
      assert (ModalityMono M') as HM' by (destruct HMR => //; by rewrite -HMM').

      split => HPS ttl;
      [rewrite -HS -HP | rewrite HS HP]; rewrite HPS HMM'; apply HM', bi_texist_mono => tt;
      revert HT => /(dep_eval ttl) /tforall_forall ->;
      by rewrite HQ.
    Qed.

    Global Instance biabd_proper_tne {TTl TTr : tele}: 
      Proper ((eq) ==> 
              (⊢) ==> 
              (tele_pointwise _ (flip (⊢))) ==> 
              (relation_conjunction (pointwise_relation _ (flip (⊢))) (either_holds ModalityMono)) ==>
              (tele_pointwise _ (⊢)) ==> 
              (tele_pointwise _ (tele_pointwise _ (flip (⊢)))) ==> (flip (impl)))
        (BiAbd (TTl := TTl) (TTr := TTr) (PROP := PROP)).
    Proof.
      move => p p' <- {p'} P P' HP Q Q' /tele_pointwise_app HQ 
        M M' /different_modalities_mono_flip HMM' S S' /tforall_forall HS T T' /tele_pointwise_app HT 
        /tforall_forall HPQST.
      rewrite /BiAbd tforall_forall => ttl.
      rewrite HS HP {HS HP} HPQST.
      apply HMM', bi_texist_mono => tt.
      rewrite HQ; revert HT. 
      move => /(dep_eval ttl) /tforall_forall -> //.
    Qed.

  End biabd.

  Section introduce_vars.
    Implicit Types TT : tele.
    Global Instance intro_vars_proper_ent {TT} :
      Proper ((tele_pointwise _ (⊢)) ==> (⊢)) (IntroduceVars (TT := TT) (PROP := PROP)).
    Proof.
      unseal_diaframe => P Q /tforall_forall HPQ.
      apply bi_tforall_mono => tt.
      by rewrite HPQ.
    Qed.

    Global Instance intro_vars_proper_tne {TT} :
      Proper ((tele_pointwise _ (flip (⊢))) ==> (flip (⊢))) (IntroduceVars (TT := TT) (PROP := PROP)).
    Proof.
      unseal_diaframe => P Q /tforall_forall HPQ.
      apply bi_tforall_mono => tt.
      by rewrite HPQ.
    Qed.

    Global Instance intro_vars_proper_equiv {TT} :
      Proper ((tele_pointwise _ (⊣⊢)) ==> (⊣⊢)) (IntroduceVars (TT := TT) (PROP := PROP)).
    Proof.
      unseal_diaframe => P Q /tforall_forall HPQ.
      apply (anti_symm _);
      apply bi_tforall_mono => tt;
      by rewrite HPQ.
    Qed.

  End introduce_vars.

End proper_instances.



Global Notation "'HINT1' □⟨ p ⟩ H ✱ [ N ] ⊫ [ M ] ; A" :=
  (Abduct 
    (TT := TeleO)
    p
    H%I
    A%I
    M
    N%I
  ) (at level 20, format
  "'[hv' 'HINT1'  □⟨ p ⟩  H  ✱  [ '[hv' N ']' ] '/ '  ⊫  [ M ] ;  A ']'"
).

Global Notation "'HINT1' H ✱ [ N ] ⊫ [ M ] ; A" :=
  (Abduct 
    (TT := TeleO)
    false
    H%I
    A%I
    M
    N%I
  ) (at level 20, format
  "'[hv' 'HINT1'  H  ✱  [ '[hv' N ']' ] '/ '  ⊫  [ M ] ;  A ']'"
).

Global Notation "'HINT1' □⟨ p ⟩ H ✱ [ N ] ⊫ [ M ] x1 .. xn ; A" :=
  (Abduct 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    p
    H%I
    (λ x1, .. (λ xn, A) .. )%I
    M
    N%I
  ) (at level 20, x1 closed binder, xn closed binder, format
  "'[hv' 'HINT1'  □⟨ p ⟩  H  ✱  [ '[hv' N ']' ] '/ '  ⊫  [ M ]  x1 .. xn ;  A ']'"
).

Global Notation "'HINT1' H ✱ [ N ] ⊫ [ M ] x1 .. xn ; A" :=
  (Abduct 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    false
    H%I
    (λ x1, .. (λ xn, A) .. )%I
    M
    N%I
  ) (at level 20, x1 closed binder, xn closed binder, format
  "'[hv' 'HINT1'  H  ✱  [ '[hv' N ']' ] '/ '  ⊫  [ M ]  x1 .. xn ;  A ']'"
).


(* the - ; notation is only for parsing, since the beginning of L might look like binders to Coq, at which case it will decide to parse it only as binders, and then fail *)
Global Notation "'HINT' □⟨ p ⟩ H ✱ [ - ; L ] ⊫ [ M ] ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleO)
    (TTr := TeleO)
    p
    H%I
    A%I
    M
    L%I
    U%I
  ) (at level 20, format
  "'[hv' 'HINT'  □⟨ p ⟩  '/  ' H  ✱  [  -  ;  '[hv' L ']' ]  '/    ' ⊫  [ M ] ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'HINT' H ✱ [ - ; L ] ⊫ [ M ] ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleO)
    (TTr := TeleO)
    false
    H%I
    A%I
    M
    L%I
    U%I
  ) (at level 20, format
  "'[hv' 'HINT'  '/  ' H  ✱  [  -  ;  '[hv' L ']' ]  '/    ' ⊫  [ M ] ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'HINT' □⟨ p ⟩ H ✱ [ - ; L ] ⊫ [ M ] x1 .. xn ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleO)
    (TTr := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    p
    H%I
    (λ x1, .. (λ xn, A) .. )%I
    M
    L%I
    (λ x1, .. (λ xn, U) .. )%I
  ) (at level 20, x1 closed binder, xn closed binder, format
  "'[hv' 'HINT'  □⟨ p ⟩  '/  ' H  ✱  [  -  ;  '[hv' L ']' ]  '/    ' ⊫  [ M ]  x1 .. xn ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'HINT' H ✱ [ - ; L ] ⊫ [ M ] x1 .. xn ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleO)
    (TTr := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    false
    H%I
    (λ x1, .. (λ xn, A) .. )%I
    M
    L%I
    (λ x1, .. (λ xn, U) .. )%I
  ) (at level 20, x1 closed binder, xn closed binder, format
  "'[hv' 'HINT'  '/  ' H  ✱  [  -  ;  '[hv' L ']' ]  '/    ' ⊫  [ M ]  x1 .. xn ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'HINT' □⟨ p ⟩ H ✱ [ y1 .. yn ; L ] ⊫ [ M ] ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleS (λ y1, .. (TeleS (λ yn, TeleO)) ..))
    (TTr := TeleO)
    p
    H%I
    A%I
    M
    (λ y1, .. (λ yn, L) .. )%I
    (λ y1, .. (λ yn, U) .. )%I
  ) (at level 20, y1 closed binder, yn closed binder, format
  "'[hv' 'HINT'  □⟨ p ⟩  '/  ' H  ✱  [ '[hv' y1 .. yn ;  L ']' ]  '/    ' ⊫  [ M ] ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'HINT' H ✱ [ y1 .. yn ; L ] ⊫ [ M ] ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleS (λ y1, .. (TeleS (λ yn, TeleO)) ..))
    (TTr := TeleO)
    false
    H%I
    A%I
    M
    (λ y1, .. (λ yn, L) .. )%I
    (λ y1, .. (λ yn, U) .. )%I
  ) (at level 20, y1 closed binder, yn closed binder, format
  "'[hv' 'HINT'  '/  ' H  ✱  [ '[hv' y1 .. yn ;  L ']' ]  '/    ' ⊫  [ M ] ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'HINT' □⟨ p ⟩ H ✱ [ y1 .. yn ; L ] ⊫ [ M ] x1 .. xn ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleS (λ y1, .. (TeleS (λ yn, TeleO)) ..))
    (TTr := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    p
    H%I
    (λ x1, .. (λ xn, A) .. )%I
    M
    (λ y1, .. (λ yn, L) .. )%I
    U%I
  ) (at level 20, x1 closed binder, xn closed binder, y1 closed binder, yn closed binder, only printing, format
  "'[hv' 'HINT'  □⟨ p ⟩  '/  ' H  ✱  [ y1 .. yn ;  '[hv' L ']' ]  '/    ' ⊫  [ M ]  x1 .. xn ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
). (* this is only printing since the double telescopes usually confuse Coq. See also SPEC2 *)

Global Notation "'HINT' □⟨ p ⟩ H ✱ [ y1 .. yn ; L ] ⊫ [ M ] x1 .. xn ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleS (λ y1, .. (TeleS (λ yn, TeleO)) ..))
    (TTr := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    p
    H%I
    (λ x1, .. (λ xn, A) .. )%I
    M
    (λ y1, .. (λ yn, L) .. )%I
    (λ y1, .. (λ yn, (λ x1, .. (λ xn, U) ..)) ..)%I
  ) (at level 20, x1 closed binder, xn closed binder, y1 closed binder, yn closed binder, format
  "'[hv' 'HINT'  □⟨ p ⟩  '/  ' H  ✱  [ y1 .. yn ;  '[hv' L ']' ]  '/    ' ⊫  [ M ]  x1 .. xn ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'HINT' H ✱ [ y1 .. yn ; L ] ⊫ [ M ] x1 .. xn ; A ✱ [ U ]" :=
  (BiAbd 
    (TTl := TeleS (λ y1, .. (TeleS (λ yn, TeleO)) ..))
    (TTr := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    false
    H%I
    (λ x1, .. (λ xn, A) .. )%I
    M
    (λ y1, .. (λ yn, L) .. )%I
    (λ y1, .. (λ yn, (λ x1, .. (λ xn, U) ..)) ..)%I
  ) (at level 20, x1 closed binder, xn closed binder, y1 closed binder, yn closed binder, format
  "'[hv' 'HINT'  '/  ' H  ✱  [ y1 .. yn ;  '[hv' L ']' ]  '/    ' ⊫  [ M ]  x1 .. xn ;  '/  ' A  ✱  [ '[hv' U ']' ] ']'"
).

Global Notation "'TRANSFORM' □⟨ p ⟩ H , T '=[hyp]=>' T'" :=
  (TransformHyp p H T T')
  (at level 20, format
  "'TRANSFORM'  □⟨ p ⟩  H ,  T  '=[hyp]=>'  T'").


Global Notation "'TRANSFORM' Δ , T '=[ctx]=>' G" :=
  (TransformCtx Δ T G)
  (at level 20, format
  "'TRANSFORM'  Δ ,  T  '=[ctx]=>'  G").





















