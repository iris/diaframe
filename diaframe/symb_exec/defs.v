From iris.bi Require Export bi telescopes.
From diaframe Require Import tele_utils util_classes solve_defs.
Import bi.

(* TODO: this needs refactoring. Some things are classes while they shouldnt be. *)

(* Constructs a kind of abstract symbolic execution interface.
   An `ExecuteOp` interprets a PROP as an execution operation of some expression e.
   This expression is decomposed into e = K e', and we look for a ReductionCondition on e.
   This produces a new expression e' (possibly depending on an a), and a 'template'
   M : (A → PROP) → PROP. The idea of this template is that (if the ReductionCondition and
   execution are suitable compatible) we (sort of) have:
   M (execute (K ∘ e')) ⊢ execute e
   The idea of the template M is that one does not need to fix a particular shape of proving
   pre and postcondition: all you need is some template, which may need to satisfy some properties
   to be compatible. This idea mainly shone when we wanted to add support for logically atomic specs:
   these dont fit the regular |={E1,E2}=> pre ∗ (post -∗ newgoal) pattern, but cán be seen as just
   another template. *)

Section base_execution_classes.

  Implicit Types PROP : bi.
  Implicit Types E V W A B : Type.

  Class ExecuteOp PROP E V := execute_op : E → V → PROP.
  Class ReductionCondition PROP E W := 
    reduction_condition A : W → E → (A → E) → ((A → PROP) → PROP) → PROP.
  Class ContextCondition E := context_condition_op : (E → E) → Prop.
  Class TemplateCondition PROP V :=
    template_condition A : V → ((A → PROP) → PROP) → V → ((A → PROP) → PROP) → Prop.

  Global Arguments reduction_condition {_ _ _}.
  Global Arguments template_condition {_ _}.

  Class ExecuteReductionCompatibility `(execute : ExecuteOp PROP E V)
                                       `(env_args : V → W)
                                       (red_cond : ReductionCondition PROP E W) 
                                       (context_cond : ContextCondition E)
                                       (template_cond : TemplateCondition PROP V) :=
    reduction_compatibility : ∀ K e A e' M,
      context_cond K →
      ∀ R R' M', template_cond A R M R' M' →
      red_cond A (env_args R) e e' M ∗
      M' $ flip execute R' ∘ K ∘ e' ⊢@{PROP} execute (K e) R.

End base_execution_classes.

Global Hint Mode ExecuteReductionCompatibility + + + + - - - - - : typeclass_instances.

Section proof_search_classes.
  Implicit Types PROP : bi.
  Implicit Types E V W : Type.
  Implicit Types TT : tele.

  Class AsExecutionOf {PROP} (P : PROP) `(execute : ExecuteOp PROP E V) e R :=
    as_execution_of : P ≡@{PROP} execute e R.

  Class ReductionTemplateStep `(condition : ReductionCondition PROP E W) T (pre : PROP) w e e' M :=
    reduction_temp_step : pre ⊢ condition T w e e' M.

  (* Copy of the product type, to not cause unwanted Universe constraints.. *)
  Record qprod {A B : Type@{bi.Quant}} := QPair {
    qfst : A;
    qsnd : B
  }.
  Global Arguments qprod (_ _) : clear implicits.
  Global Arguments QPair {_ _} _ _.

  Definition template_M {PROP : bi} n (M1 M2 : PROP → PROP) TT1 TT2 Ps Ps': (qprod TT1 TT2 → PROP) → PROP
    := (λ P, M1 $ ∃.. (tt1 : TT1), tele_app Ps tt1 ∗ ▷^n (∀.. (tt2 : TT2), 
          tele_app (tele_app Ps' tt1) tt2 -∗ M2 (P $ QPair tt1 tt2)))%I.

  Global Arguments template_M {_} n M1 M2 TT1 TT2 Ps Ps' P /.

  (* We make the [e] and [e'] arguments appear later, to force Coq to first parse the pre and postconditions
    [Ps] and [Ps']. There is no nicer way to accomplish this, as far as I know *)
  Class ReductionStep' `(condition : ReductionCondition PROP E W ) (pre : PROP) n M1 M2
      TT1 TT2 (Ps : TT1 -t> PROP) (Ps' : TT1 -t> TT2 -t> PROP) e (e' : TT1 -t> TT2 -t> E) w :=
    #[global] as_template_step :: ReductionTemplateStep condition (qprod TT1 TT2) pre w e (λ pr, tele_app (tele_app e' $ qfst pr) $ qsnd pr) 
                        (template_M n M1 M2 TT1 TT2 Ps Ps').

  Class SatisfiesContextCondition `(condition : ContextCondition E) K :=
    satisfies_context_cond : condition K.

  Class SatisfiesTemplateCondition `(template_cond : TemplateCondition PROP V) {A} R M R' M' :=
    satisfies_mod_cond : template_cond A R M R' M'.

  Class ReshapeExprAnd E (e : E) (K : E → E) (e' : E) (A : Type) := ConstructReshape {
    ReshapeExpr_equality : e = K e';
    ReshapeExpr_memberA : A;
  }.

  Global Arguments ConstructReshape {_} _ _ _ _ _ _.

End proof_search_classes.

Global Hint Mode AsExecutionOf + ! - - - - - : typeclass_instances.
Global Hint Mode AsExecutionOf + ! ! ! ! ! ! : typeclass_instances.
Global Hint Mode ReductionStep' + + + + ! - - - - - - - + - ! : typeclass_instances.
Global Hint Mode SatisfiesContextCondition + + + : typeclass_instances.
Global Hint Mode ReshapeExprAnd + + - - ! : typeclass_instances.

Global Arguments ReductionStep' : simpl never.
Global Arguments ReductionTemplateStep : simpl never.

Section template_lemmas.
  Context {PROP : bi}.

  Definition template_mono `(M : (A → PROP) → PROP) : Prop
    := ∀ P Q, (∀ a, P a ⊢ Q a) → M P ⊢ M Q.

  Definition template_strong_mono `(M : (A → PROP) → PROP) : Prop
    := ∀ P Q, (∀ a, P a -∗ Q a) ⊢ M P -∗ M Q.

  Definition template_absorbing `(M : (A → PROP) → PROP) : Prop
    := ∀ P Q, M P ∗ Q ⊢ M (λ a, P a ∗ Q).

  Definition template_conditionally_absorbing `(M : (A → PROP) → PROP) (HC : PROP → Prop) : Prop
    := ∀ P Q, HC Q → (M P ∗ Q ⊢ M (λ a, P a ∗ Q)).

  Implicit Types M : PROP → PROP.

  Lemma strong_mono_stronger `(M : (A → PROP) → PROP) :
    template_strong_mono M → template_mono M.
  Proof.
    rewrite /template_strong_mono /template_mono => HM P Q HPQ.
    specialize (HM P Q).
    enough (emp ⊢ (∀ a, P a -∗ Q a)).
    { revert HM. rewrite -H.
      move => /bi.wand_elim_l'.
      rewrite left_id //. }
    apply bi.forall_intro => a.
    apply bi.wand_intro_l.
    rewrite right_id.
    apply HPQ.
  Qed.

  Lemma template_M_is_mono n M1 M2 TT1 TT2 Ps Qs :
    ModalityMono M1 → 
    ModalityMono M2 → 
    template_mono (template_M n M1 M2 TT1 TT2 Ps Qs).
  Proof.
    move => HM1 HM2 S T HST /=.
    apply HM1 => {HM1}.
    apply bi_texist_mono => tt1.
    apply bi.sep_mono_r, bi.laterN_mono.
    apply bi_tforall_mono => tt2.
    apply bi.wand_mono => //.
    apply HM2, HST.
  Qed.

  Lemma template_M_is_absorbing n M1 M2 TT1 TT2 Ps Qs:
    ModalityStrongMono M1 →
    ModalityStrongMono M2 →
    template_absorbing (template_M n M1 M2 TT1 TT2 Ps Qs).
  Proof.
    move => HM1 HM2 P Q /=.
    rewrite modality_strong_frame_l.
    apply modality_mono.
    rewrite !bi_texist_exist bi.sep_exist_r.
    apply bi.exist_mono => tt1.
    rewrite -assoc.
    apply bi.sep_mono_r.
    rewrite {1}(bi.laterN_intro n Q) -bi.laterN_sep.
    apply bi.laterN_mono.
    rewrite !bi_tforall_forall bi.sep_forall_r.
    apply bi.forall_mono => tt2.
    apply bi.wand_intro_l.
    rewrite assoc bi.wand_elim_r.
    by rewrite modality_strong_frame_l.
  Qed.

  Lemma absorbing_mono_gives_stronger `(M : (A → PROP) → PROP) :
    template_mono M →
    template_absorbing M →
    template_strong_mono M.
  Proof.
    rewrite /template_mono /template_absorbing /template_strong_mono => Hmon Habs P Q.
    specialize (Habs P (∀ a, P a -∗ Q a)%I).
    apply bi.wand_intro_l.
    rewrite Habs.
    apply Hmon => a.
    rewrite (bi.forall_elim a) bi.wand_elim_r //.
  Qed.

  Lemma strong_mono_gives_absorbing `(M : (A → PROP) → PROP) :
    template_strong_mono M → template_absorbing M.
  Proof.
    rewrite /template_strong_mono /template_absorbing => Hmon P Q.
    apply bi.wand_elim_r'.
    rewrite -Hmon.
    apply bi.forall_intro => a.
    by apply bi.wand_intro_l.
  Qed.

  Lemma unconditionally_absorbing_to_absorbing `(M : (A → PROP) → PROP) :
    template_conditionally_absorbing M (λ _ , True) → template_absorbing M.
  Proof.
    rewrite /template_absorbing /template_conditionally_absorbing.
    move => HM P Q. by apply HM.
  Qed.

  Lemma absorbing_to_conditionally_absorbing `(M : (A → PROP) → PROP) HC :
    template_absorbing M → template_conditionally_absorbing M HC.
  Proof.
    rewrite /template_absorbing /template_conditionally_absorbing.
    move => HM P Q HQ. by apply HM.
  Qed.

End template_lemmas.

Section class_lemmas.
  Implicit Types PROP : bi.
  Implicit Types E V W : Type.

  Proposition equiv_executions_compat_enough `{red_cond : ReductionCondition PROP E W} 
    {context_cond : ContextCondition E} {V1 V2} 
    {tmp_cond1 : TemplateCondition PROP V1} {tmp_cond2 : TemplateCondition PROP V2} (embed : V2 → V1)
    (exec1 : ExecuteOp PROP E V1) (env_args1 : V1 → W) 
    (exec2 : ExecuteOp PROP E V2) (env_args2 : V2 → W) :
    ExecuteReductionCompatibility exec1 env_args1 red_cond context_cond tmp_cond1 →
    (∀ e v, exec1 e (embed v) ⊣⊢ exec2 e v) →
    (∀ v2, env_args2 v2 = env_args1 $ embed v2) →
    (∀ A v2 M v2' M', tmp_cond2 A v2 M v2' M' → tmp_cond1 A (embed v2) M (embed v2') M') → 
    (∀ A v2 M v2' M', tmp_cond2 A v2 M v2' M' → template_mono M') →
    ExecuteReductionCompatibility exec2 env_args2 red_cond context_cond tmp_cond2.
  Proof.
    move => Hcompat Hequiv Henv_args Htmp_cond Htmp_mono K e A e' M HK R R' M' HM.
    erewrite <-Hequiv, <-Hcompat; [ | done | by apply Htmp_cond].
    rewrite Henv_args.
    apply bi.sep_mono_r.
    eapply Htmp_mono => // => a /=.
    by rewrite Hequiv.
  Qed.

  (* straight equality is the best we can do, since we know nothing about red_cond *)
  Global Instance reductionstep_proper_tne_impl `{red_cond : ReductionCondition PROP E W} T :
    Proper ((⊢) --> (=) ==> (=) ==> (=) ==> (=) ==> (impl)) (ReductionTemplateStep red_cond T).
  Proof. solve_proper. Qed. 

  Global Instance reductionstep_proper_ent_flipimpl `{red_cond : ReductionCondition PROP E W} T :
    Proper ((⊢) ==> (=) ==> (=) ==> (=) ==> (=) ==> (flip impl)) (ReductionTemplateStep red_cond T).
  Proof. solve_proper. Qed. 

  Global Instance reductionstep_proper_iff `{red_cond : ReductionCondition PROP E W} T :
    Proper ((⊣⊢) ==> (=) ==> (=) ==> (=) ==> (=) ==> (iff)) (ReductionTemplateStep red_cond T).
  Proof. solve_proper. Qed.

End class_lemmas. 

Section abd_from_redstep.
  Context `(red_cond : ReductionCondition PROP E W ).
  Context `(execute : ExecuteOp PROP E V).
  Context (context_cond : ContextCondition E).
  Context (tmp_cond : TemplateCondition PROP V).

  Lemma execution_abduct_lem H P e R K e_in' T e_out' (MT MT' : (T → PROP) → PROP) R' env_args :
    AsExecutionOf P execute e R →
    ExecuteReductionCompatibility execute env_args red_cond context_cond tmp_cond →
    ReshapeExprAnd E e K e_in' (ReductionTemplateStep red_cond T H (env_args R) e_in' e_out' MT) →
    SatisfiesContextCondition context_cond K →
    SatisfiesTemplateCondition tmp_cond R MT R' MT' →
    HINT1 H ✱ [MT' $ flip execute R' ∘ K ∘ e_out'] ⊫ [id]; P.
  Proof.
    rewrite /AsExecutionOf /Abduct bi.intuitionistically_if_elim /= => HPR Hcompat [HeK Hred] HK HM1M2.
    rewrite HPR {HPR} HeK {HeK}.
    rewrite -Hcompat //.
    by erewrite <-Hred.
  Qed.

End abd_from_redstep.

Notation "'ReductionStep' '(' red_cond , w ')' e ⊣ ⟨ M1 ⟩ ∃ x1 .. xn , Qs ; pre =[ ▷^ n ]=> ∃ y1 .. yn , ⟨ M2 ⟩ e' ⊣ mQsout" :=
  (ReductionStep' 
    red_cond
    pre%I
    n
    M1
    M2
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    (λ x1, .. (λ xn, Qs%I) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, mQsout%I) .. )) .. )
    e
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, e') .. )) .. )
    w)
  (at level 20, n, M1, M2 at level 9, x1 binder, xn binder, y1 binder, yn binder, red_cond, w, e, Qs, pre, e', mQsout at level 200, format
  "ReductionStep  ( red_cond ,  w )  e  ⊣  ⟨ M1 ⟩  ∃  x1 .. xn ,  Qs  ;  pre  =[ ▷^ n ]=>  ∃  y1 .. yn ,  ⟨ M2 ⟩  e'  ⊣  mQsout"
).

Notation "'ReductionStep' '(' red_cond , w ')' e ⊣ ⟨ M1 ⟩ Qs ; pre =[ ▷^ n ]=> ∃ y1 .. yn , ⟨ M2 ⟩ e' ⊣ mQsout" :=
  (ReductionStep' 
    red_cond
    pre%I
    n
    M1
    M2
    TeleO
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    Qs%I
    (λ y1, .. (λ yn, mQsout%I) .. )
    e
    (λ y1, .. (λ yn, e') .. )
    w)
  (at level 20, n, M1, M2 at level 9, y1 binder, yn binder, red_cond, w, e, Qs, pre, e', mQsout at level 200, format
  "ReductionStep  ( red_cond ,  w )  e  ⊣  ⟨ M1 ⟩  Qs  ;  pre  =[ ▷^ n ]=>  ∃  y1 .. yn ,  ⟨ M2 ⟩  e'  ⊣  mQsout"
).

Notation "'ReductionStep' '(' red_cond , w ')' e ⊣ ⟨ M1 ⟩ ∃ x1 .. xn , Qs ; pre =[ ▷^ n ]=> ⟨ M2 ⟩ e' ⊣ mQsout" :=
  (ReductionStep' 
    red_cond
    pre%I
    n
    M1
    M2
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    TeleO
    (λ x1, .. (λ xn, Qs%I) .. )
    (λ x1, .. (λ xn, mQsout%I) .. )
    e
    (λ x1, .. (λ xn, e') .. )
    w)
  (at level 20, n, M1, M2 at level 9, x1 binder, xn binder, red_cond, w, e, Qs, pre, e', mQsout at level 200, format
  "ReductionStep  ( red_cond ,  w )  e  ⊣  ⟨ M1 ⟩  ∃  x1 .. xn ,  Qs  ;  pre  =[ ▷^ n ]=>  ⟨ M2 ⟩  e'  ⊣  mQsout"
).

Notation "'ReductionStep' '(' red_cond , w ')' e ⊣ ⟨ M1 ⟩ Qs ; pre =[ ▷^ n ]=> ⟨ M2 ⟩ e' ⊣ mQsout" :=
  (ReductionStep' 
    red_cond
    pre%I
    n
    M1
    M2
    TeleO
    TeleO
    Qs%I
    mQsout%I
    e
    e'
    w)
  (at level 20, n, M1, M2 at level 9, red_cond, w, e, Qs, pre, e', mQsout at level 200, format
  "ReductionStep  ( red_cond ,  w )  e  ⊣  ⟨ M1 ⟩  Qs  ;  pre  =[ ▷^ n ]=>  ⟨ M2 ⟩  e'  ⊣  mQsout"
).

















