From iris.proofmode Require Import base coq_tactics reduction tactics environments.
From diaframe Require Import tele_utils util_classes utils_ltac2.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file contains instances (and some Hint Externs to find instances) for the
   classes defined in util_classes.v *)


Section prop_instances.
  Context {PROP : bi}.

  Section modalities.
    Implicit Type M : PROP → PROP.

    (* ModalityStrongMono instances *)
    Global Instance id_modality_ec : ModalityStrongMono (PROP := PROP) id.
    Proof. by split => P Q /=. Qed.

    Global Instance fupd_strong_modality `{!BiFUpd PROP} E1 E2 : ModalityStrongMono (PROP := PROP) (fupd E1 E2).
    Proof.
      split => [P Q | P Q].
      - by move => ->.
      - by rewrite fupd_frame_r.
    Qed.

    Global Instance cfupd_strong_modality `{!BiFUpd PROP} Ec E1 E2 : ModalityStrongMono (PROP := PROP) (cfupd Ec E1 E2) | 80.
    Proof. rewrite cfupd_eq; tc_solve. Qed.

    Global Instance bupd_ec_mod `{!BiBUpd PROP} : ModalityStrongMono (PROP := PROP) bupd | 100.
    Proof.
      split.
      - move => P Q -> //.
      - move => P Q.
        by rewrite bupd_frame_r.
    Qed.

    Global Instance modality_ec_compose M1 M2 :
      ModalityStrongMono M1 →
      ModalityStrongMono M2 →
      ModalityStrongMono (M1 ∘ M2). (* this recursive instance is sometimes useful *)
    Proof.
      intros; split => P Q /=.
      - move => HPQ.
        apply H, H0, HPQ.
      - rewrite modality_strong_frame_l.
        apply H.
        apply H0.
    Qed.

    Global Instance laterN_ec_modality n : ModalityStrongMono (PROP := PROP) (bi_laterN n) | 200.
    Proof.
      split.
      - move => P Q -> //.
      - move => P Q. by iIntros "[$ $]".
    Qed.

    Global Instance later_ec_modality : ModalityStrongMono (PROP := PROP) (bi_later) | 200.
    Proof. change bi_later with (bi_laterN (PROP := PROP) 1). tc_solve. Qed.


    (* ModalityCompat3 instances *)
    (* with id on the right: *)
    Global Instance compat3_right_id M :
      ModalityCompat3 M M id | 80.
    Proof. move => P //=. Qed.

    Global Instance subtract_composition_later M M1 M2 n :
      ModalityCompat3 M M1 M2 →
      ModalityMono M1 →
      ModalityMono M2 →
      ModalityCompat3 (M ∘ bi_laterN n) M1 M2 | 150.
    (* priority is higher than above, to keep laters around when possible.
       Trying to subtract, for example, a bupd from a (M ∘ later) is a clear incentive
       of applying laterN_intro *)
    Proof. move => HMs HM1 HM2 P /=. rewrite -HMs. apply HM1, HM2, laterN_intro. Qed.

    (* fupd first *)
    Global Instance fupd_compat3 `{!BiFUpd PROP} E1 E2 E3 : 
      ModalityCompat3 (PROP := PROP) (fupd E1 E2) (fupd E1 E3) (fupd E3 E2) | 20.
    Proof.
      rewrite /ModalityCompat3 => P.
      apply fupd_trans.
    Qed.

    Global Instance fupd_cfupd_compat3 `{!BiFUpd PROP} E1 E2 E3 Ec :
      SolveSepSideCondition (Ec ⊆ E1) → (* this is what makes cfupd work as intended *)
      ModalityCompat3 (PROP := PROP) (fupd E1 E2) (fupd E1 E3) (cfupd Ec E3 E2) | 50.
    Proof. move => _. rewrite cfupd_eq. tc_solve. Qed.

    Global Instance fupd_cfupd_l_compat3 `{!BiFUpd PROP} E1 E2 E3 Ec :
      SolveSepSideCondition (Ec ⊆ E1) →
      ModalityCompat3 (PROP := PROP) (fupd E1 E2) (cfupd Ec E1 E3) (fupd E3 E2) | 50.
    Proof. move => _. rewrite cfupd_eq. tc_solve. Qed.

    Global Instance fupd_bupd_compat3 `{BiBUpdFUpd PROP} E1 E2 :
      ModalityCompat3 (PROP := PROP) (fupd E1 E2) (fupd E1 E2) bupd | 100.
    Proof. move => P. by iMod 1 as "H". Qed.

    Global Instance fupd_bupd_compat3' `{BiBUpdFUpd PROP} E1 E2 :
      ModalityCompat3 (PROP := PROP) (fupd E1 E2) bupd (fupd E1 E2) | 120. 
      (* used in some tricky situations, drop once we have CombinedModality *)
    Proof. move => P. by iMod 1 as "H". Qed.

    (* bupd first *)
    Global Instance bupd_bupd_ec_compat3 `{BiBUpd PROP} :
      ModalityCompat3 (PROP := PROP) bupd bupd bupd | 51.
    Proof. move => P. by iMod 1 as ">$". Qed.

    (* id first only accepts id, so takes compat3_right_id *)

    (* extra instances to support composition of modalities *)
    Global Instance compat_compose_trans M1 M2 M3 M4 M' :
      ModalityCompat3 M' M3 M4 →
      ModalityCompat3 M1 M2 M' →
      ModalityMono M2 →
      ModalityCompat3 M1 M2 (M3 ∘ M4).
    Proof.
      rewrite /ModalityCompat3 /= => HM' HM1 HM2 P.
      rewrite -HM1.
      by apply HM2.
    Qed.

    Global Existing Instance Build_SplitModality3.


    (* SplitModality4 instance *)
    Global Instance split_modality_4_instance M M1 M2 M3 M' :
      SplitLeftModality3 M M' M3 →
      SplitModality3 M' M1 M2 →
      SplitModality4 M M1 M2 M3.
    Proof.
      case => HMM'3 HM' HM3.
      case => HM'12 HM1 HM2.
      split => //.
      move => P.
      rewrite -HMM'3 -HM'12 //.
    Qed.


    (* ModWeaker, NotIntroducable instances *)
    Global Instance id_introducable : ModWeaker (PROP:=PROP) id id | 1.
    Proof. by rewrite /ModWeaker. Qed.

    Global Instance fupd_weaker_than_id `{!BiFUpd PROP} E : 
      ModWeaker (PROP := PROP) id (fupd E E) | 0. 
    (* in affine logics, id is weaker than [fupd E1 E2] if [E2 ⊆ E1]. But we never want to use this *)
    Proof. move => P /=. apply fupd_intro. Qed.

    Global Instance bupd_weaker_than_id `{BiBUpd PROP} :
      ModWeaker (PROP := PROP) id bupd | 10.
    Proof. move => P /=. apply bupd_intro. Qed.

    Global Instance mod_weaker_refl M : ModWeaker (PROP := PROP) M M.
    Proof. by rewrite /ModWeaker => P. Qed.

    Global Instance later_weaker_than_id : ModWeaker (PROP:= PROP) id bi_later | 2.
    Proof. move => P. apply later_intro. Qed.

    Global Instance laterN_weaker_than_id n : ModWeaker (PROP := PROP) id (bi_laterN n) | 2.
    Proof. move => P. apply laterN_intro. Qed.

    Global Instance mod_weaker_id_comp (M1 M2 : PROP → PROP) : 
      ModWeaker id M1 → ModWeaker id M2 → ModWeaker id (M1 ∘ M2) | 80.
    Proof. move => HM1 HM2 P /=. rewrite -HM1 /= -HM2 //. Qed.

    Global Instance not_introducable (M : PROP → PROP): 
      TCIf (IntroducableModality M) False TCTrue →
      NotIntroducable M. 
    (* because of the hint modes in NotIntroducable, this will never unify masks in M *)
    Proof. by constructor. Qed.


    (* SplitLeftModality instances *)
    Global Instance split_mod_fupd `{!BiFUpd PROP} E1 E2 E3 :
      SplitLeftModality3 (PROP := PROP) (fupd E1 E2) (fupd E1 E3) (fupd E3 E2) | 20.
    Proof. split; tc_solve. Qed.

    Global Instance split_mod_id :
      SplitLeftModality3 (PROP := PROP) id id id | 20.
    Proof. split; tc_solve. Qed.

    Global Instance split_mod_bupd `{BiBUpd PROP} :
      SplitLeftModality3 (PROP := PROP) bupd bupd bupd.
    Proof. split; tc_solve. Qed.

    Global Instance split_left_compose_later (M : PROP → PROP) M1 M2 n :
      SplitLeftModality3 M M1 M2 →
      SplitLeftModality3 (M ∘ bi_laterN n) M1 (M2 ∘ bi_laterN n).
    Proof. 
      split; try apply H.
      - move => P /=. apply H.
      - apply modality_ec_compose;last tc_solve. apply H.
    Qed.

    (* By default, we can assume the modality id around any proposition *)
    Global Instance prepend_identity (B : PROP) :
      PrependModality B id B | 99. (* High precedence to allow overriding, like for WP *)
    Proof. rewrite /PrependModality //. Qed.


    (* TODO: this is a bad instance.. improve *)
    Global Instance modal_into_modal p P (M : PROP → PROP) : 
      ModalityStrongMono M →
      IntoModal p (M P) M P | 20.
    Proof. by rewrite /IntoModal intuitionistically_if_elim. Qed.


    (* CoIntroducable instances *)
    Global Instance co_intro_from_into M :
      IntroducableModality M →
      CoIntroducable M id | 10.
    Proof. rewrite /ModWeaker /CoIntroducable //=. Qed.

    Global Instance fupd_co_intro `{!BiFUpd PROP} E1 E2:
      SolveSepSideCondition (E2 ⊆ E1) →
      CoIntroducable (PROP := PROP) (fupd E1 E2) (fupd E2 E1) | 20.
    Proof.
      rewrite /SolveSepSideCondition /CoIntroducable => HE P.
      by apply fupd_mask_intro_subseteq.
    Qed.


    (* Collect1Modal instances should look for _explicit_ prepended modalities *)
    Lemma collect_one_modal_modal (P : PROP) M :
      ModalityStrongMono M →
      Collect1Modal (M P) M P.
    Proof. rewrite /Collect1Modal //. Qed.

    Global Instance collect_modal_bupd (P : PROP) `{BiBUpd PROP } :
      Collect1Modal (|==> P) bupd P | 50.
    Proof. apply: collect_one_modal_modal. Qed.

    Global Instance collect_modal_fupd (P : PROP) `{BiFUpd PROP } E1 E2 :
      Collect1Modal (|={E1,E2}=> P) (fupd E1 E2) P | 50.
    Proof. apply: collect_one_modal_modal. Qed.


    (* CombinedModality instances. These are the 'reverse' of splitting, in a way *)
    Global Instance combined_from_safe M1 M2 Mc :
      CombinedModalitySafe M1 M2 Mc → CombinedModality M1 M2 Mc.
    Proof. by move => + P => <-. Qed.

    Global Instance combined_id_l M :
      CombinedModalitySafe M id M | 20.
    Proof. move => P //. Qed.

    Global Instance combined_id_r M :
      CombinedModalitySafe id M M | 20.
    Proof. move => P //. Qed.

    Global Instance modal_compose_bupd_bupd `{BiBUpd PROP } :
      CombinedModalitySafe (bupd (PROP := PROP)) bupd bupd | 45.
    Proof.
      move => P. eapply (anti_symm _). 
      - by iIntros "$".
      - by iIntros ">$". 
    Qed.

    Global Instance combined_bupd_fupd E1 E2 `{BiBUpdFUpd PROP} :
      CombinedModalitySafe (bupd (PROP := PROP)) (fupd E1 E2) (fupd E1 E2) | 45.
    Proof.
      move => P. eapply (anti_symm _). 
      - by iIntros "$".
      - by iIntros ">$". 
    Qed.

    Global Instance combined_fupd_bupd E1 E2 `{BiBUpdFUpd PROP} :
      CombinedModalitySafe (fupd (PROP := PROP) E1 E2) bupd (fupd E1 E2) | 45.
    Proof.
      move => P. eapply (anti_symm _). 
      - by iIntros ">$". 
      - by iIntros ">>$".
    Qed.

    (* these are applied with hints below, to avoid too early unification of evar masks *)
    Lemma combined_fupd_fupd_first_mask_same E1 E2 `{BiFUpd PROP} :
      CombinedModalitySafe (fupd (PROP := PROP) E1 E1) (fupd E1 E2) (fupd E1 E2).
    Proof.
      move => P. eapply (anti_symm _). 
      - by iIntros "$". 
      - by iIntros ">>$".
    Qed.

    Lemma combined_fupd_fupd_last_mask_same E1 E2 `{BiFUpd PROP} :
      CombinedModalitySafe (fupd (PROP := PROP) E1 E2) (fupd E2 E2) (fupd E1 E2).
    Proof.
      move => P. eapply (anti_symm _). 
      - by iIntros ">$". 
      - by iIntros ">>$".
    Qed.

    Global Instance combined_fupd_fupd_last_top E1 E2 E3 `{BiFUpd PROP} :
      SolveSepSideCondition (E3 = ⊤) → (* prevents unification with ⊤ *)
      CombinedModalitySafe (fupd (PROP := PROP) E1 E2) (fupd E2 E3) (fupd E1 E3) | 96.
    Proof.
      move => -> P.
      eapply (anti_symm _).
      - iIntros ">HP". by iApply fupd_mask_intro_subseteq.
      - apply fupd_trans.
    Qed.

    Global Instance combined_fupd_fupd_subseteq E1 E2 E3 `{BiFUpd PROP} :
      IsEvarFree (fupd (PROP := PROP) E2 E3) →
      TCEq E1 E3 →
      SolveSepSideCondition (E2 ⊆ E1) → 
      CombinedModalitySafe (fupd (PROP := PROP) E1 E2) (fupd E2 E3) (fupd E1 E1) | 97.
    Proof.
      rewrite /SolveSepSideCondition => _ -> HE P'.
      eapply (anti_symm _).
      - iIntros ">HP". by iApply fupd_mask_intro_subseteq.
      - apply fupd_trans.
    Qed.

    Global Instance combined_cfupd_from_fupd Ec E1 E2 M Mc `{BiFUpd PROP} :
      CombinedModalitySafe (fupd E1 E2) M Mc →
      CombinedModalitySafe (cfupd Ec E1 E2) M Mc | 5.
      (* this is used in biabd hint construction *)
    Proof. by rewrite cfupd_eq. Qed.

    Global Instance combined_fupd_fupd_destructive E1 E2 E3 `{BiFUpd PROP}  :
      CombinedModality (fupd (PROP := PROP) E3 E3) (fupd E1 E2) (fupd E1 E2) | 95.
    Proof. by iIntros (P) "$". Qed. (* not safe! *)


    (* SplitLeftModality_Save instances *)
    (* base instance *)
    Global Instance split_left_save_base_1 M M1 M2 :
      SplitLeftModality3 M M1 M2 → SplitLeftModality3_Save M M1 (saved_left_mod M M2) | 99.
    Proof. rewrite saved_left_mod_eq. done. Qed.

    (* base instance if M is already a saved_left_mod *)
    Global Instance split_left_save_base_saved_left M M1 M2 Ml :
      SplitLeftModality3 M M1 M2 → SplitLeftModality3_Save (saved_left_mod Ml M) M1 (saved_left_mod Ml M2) | 90.
    Proof. rewrite saved_left_mod_eq. done. Qed.

    Global Instance mod_compat_saved_left_base Ml M M1 M2 :
      ModalityCompat3 M M1 M2 → ModalityCompat3 (saved_left_mod Ml M) M1 M2 | 79. (* < 80, right id instance *)
    Proof. rewrite saved_left_mod_eq. done. Qed.

    (* this instances makes cfupd work as intended for abduction hints *)
    Global Instance cfupd_mod_compat_saved_left `{!BiFUpd PROP} E1 E2 E3 Ec El1 El2 :
      SolveSepSideCondition (Ec ⊆ El1) → 
      ModalityCompat3 (PROP := PROP) (saved_left_mod (fupd El1 El2) $ fupd E1 E2) (fupd E1 E3) (cfupd Ec E3 E2) | 50.
    Proof. move => _. rewrite cfupd_eq. tc_solve. Qed.

    (* needed for abd-hyp-mod recursive rule *)
    Global Instance split_left_saved_mod M M1 M2 Ml :
      SplitLeftModality3 M M1 M2 → 
      SplitLeftModality3 (saved_left_mod Ml M) (saved_left_mod Ml M1) M2 | 60.
    Proof. rewrite saved_left_mod_eq. done. Qed.

    (* needed elsewhere *)
    Global Instance saved_left_strong_mono Ml M : 
      ModalityStrongMono M → ModalityStrongMono (saved_left_mod Ml M).
    Proof. rewrite saved_left_mod_eq. done. Qed.
    Global Instance saved_left_mono Ml M : 
      ModalityMono M → ModalityMono (saved_left_mod Ml M).
    Proof. rewrite saved_left_mod_eq. done. Qed.
    Global Instance saved_left_mod_weaker Ml M Mw : 
      ModWeaker Mw M → ModWeaker Mw (saved_left_mod Ml M).
    Proof. rewrite saved_left_mod_eq. done. Qed.

    (* strip saved instances *)
    Global Instance strip_saved_base M : StripSaved M M | 90.
    Proof. by move => P. Qed.
    Global Instance strip_saved_strip Ml M : StripSaved (saved_left_mod Ml M) M | 80.
    Proof. move => P. by rewrite saved_left_mod_eq. Qed.
  End modalities.


  Section later.
    Implicit Types P Q : PROP.

    Global Instance later_from_latern Q : FromLaterN (▷ Q)%I 1 Q.
    Proof. by rewrite /FromLaterN /=. Qed.

    Global Instance later_from_latern_cancel Q n m n': 
      nat_cancel.NatCancel m n 0 n' → (* m + n' = 0 + n *)
      FromLaterN (▷^n Q)%I m (▷^n' Q)%I.
    Proof.
      by rewrite /nat_cancel.NatCancel /FromLaterN -laterN_add => <- /=.
    Qed.

    Global Instance later_into_latern_max P : IntoLaterNMax (▷ P)%I 1 P.
    Proof. by rewrite /IntoLaterNMax /=. Qed.

    Global Instance latern_into_latern_max P n : IntoLaterNMax (▷^n P)%I n P.
    Proof. by rewrite /IntoLaterNMax /=. Qed.

    Global Instance later_from_latern_max P : FromLaterNMax (▷ P)%I 1 P.
    Proof. by rewrite /FromLaterNMax /=. Qed.

    Global Instance latern_from_latern_max P n : FromLaterNMax (▷^n P)%I n P.
    Proof. by rewrite /FromLaterNMax /=. Qed.

  End later.




  Section sep.
    Implicit Types P Q : PROP.

    Global Instance sep_fromsepcareful Q1 Q2 :
      FromSepCareful (Q1 ∗ Q2)%I Q1 Q2.
    Proof. by rewrite /FromSepCareful /=. Qed.

    Global Instance fromsepcareful_later (P Q1 Q2 : PROP) :
      FromSepCareful P Q1 Q2 → (FromSepCareful (▷ P) (▷ Q1) (▷ Q2))%I.
    Proof. rewrite /FromSepCareful /= -later_sep => -> //. Qed.

    Global Instance fromsep_and (P1 P2 : PROP) :
      TCOr (TCAnd (Affine P1) (Persistent P1)) (TCAnd (Affine P2) (Persistent P2)) → (* when we wánt to do it *)
      TCOr (Affine P1) (Absorbing P2) → (* when we can do it *)
      TCOr (Absorbing P1) (Affine P2) →
      FromSepCareful (P1 ∧ P2)%I P1 P2.
    Proof.
      rewrite /FromSepCareful => [[[? ?]|[? ?]]] [?|?] [?|?];
      apply bi.and_intro;
      (eapply bi.sep_elim_l, _) || (eapply bi.sep_elim_r, _).
    Qed.

    Global Instance fromsep_pure_and (P1 P2 : Prop) :
      FromSepCareful (PROP := PROP) (⌜P1 ∧ P2⌝)%I ⌜P1⌝%I ⌜P2⌝%I.
    Proof.
      rewrite /FromSepCareful; eauto.
    Qed.

    Global Instance from_sep_pair_eq {A B : Type} (pr1 pr2 : A * B) : 
      FromSepCareful (PROP := PROP) ⌜pr1 = pr2⌝%I ⌜pr1.1 = pr2.1⌝%I ⌜pr1.2 = pr2.2⌝%I.
    Proof.
      rewrite /FromSepCareful => -.
      destruct pr1; destruct pr2; simpl.
      by iIntros "[-> ->]".
    Qed.



    Global Instance into_sep_careful_sep P Q : IntoSepCareful (P ∗ Q)%I P Q.
    Proof. by rewrite /IntoSepCareful. Qed.

    Global Instance into_sep_later P Q1 Q2 :
      IntoSepCareful P Q1 Q2 → IntoSepCareful (▷ P)%I (▷ Q1)%I (▷ Q2)%I.
    Proof. by rewrite /IntoSepCareful -later_sep => <-. Qed.

    Global Instance into_sep_and_l P1 P2 :
      Persistent P1 →
      IntoSepCareful (P1 ∧ P2)%I (<affine> P1) P2 | 20.
    Proof.
      rewrite /IntoSepCareful => HP.
      by rewrite persistent_and_affinely_sep_l_1.
    Qed.

    Global Instance into_sep_and_r P1 P2 :
      Persistent P2 →
      IntoSepCareful (P1 ∧ P2)%I P1 (<affine> P2) | 20.
    Proof.
      rewrite /IntoSepCareful => HP.
      by rewrite persistent_and_affinely_sep_r_1.
    Qed.

    Global Instance into_sep_and_both P1 P2 :
      Persistent P1 → Persistent P2 →
      Affine P2 →
      IntoSepCareful (P1 ∧ P2)%I (<affine> P1) P2 | 19.
    Proof. apply _. Qed.
    (* The order matters of these instances matters. We distinguish the following cases:
      - Both resources are affine. We don't care about the instance in this case.
      - Both are persistent, only one is affine. The other one should get the <affine> modality in this case.
        If [P2] is affine and P1 isnt, [into_sep_and_both] triggers.
        Otherwise, [into_sep_and_r] triggers.
      - Only one of them is persistent. In this case the appropriate one of [into_sep_and_l] and [into_sep_and_r] triggers. *)

    Global Instance into_sep_and_pure φ1 φ2 :
      IntoSepCareful (PROP := PROP) (⌜φ1 ∧ φ2⌝)%I ⌜φ1⌝%I ⌜φ2⌝%I.
    Proof.
      rewrite /IntoSepCareful.
      iIntros "[% %]"; eauto.
    Qed.

    Global Instance into_sep_careful_box P P1 P2:
      IntoAnd false P P1 P2 →
      IntoSepCareful (PROP := PROP) (□ P)%I (□ P1)%I (□ P2)%I.
    Proof.
      rewrite /IntoSepCareful /IntoAnd /= => ->.
      iIntros "#[HP1 HP2]"; iFrame "#".
    Qed.

    Global Instance into_sep_careful_box_positive P P1 P2:
      IntoSepCareful P P1 P2 →
      BiPositive PROP →
      IntoSepCareful (PROP := PROP) (□ P)%I (□ P1)%I (□ P2)%I.
    Proof.
      rewrite /IntoSepCareful /= => -> HPROP.
      iIntros "#[HP1 HP2]"; iFrame "#".
    Qed.
  End sep.



  Section and.
    Implicit Types P Q : PROP.

    Global Instance and_fromandcareful P Q :
      FromAndCareful (P ∧ Q) P Q.
    Proof. rewrite /FromAndCareful //. Qed.

    Global Instance later_fromandcareful PQ P Q :
      FromAndCareful PQ P Q →
      FromAndCareful (▷ PQ) (▷ P)%I (▷ Q)%I.
    Proof. rewrite /FromAndCareful -later_and => <- //. Qed.

    Global Instance pure_fromandcareful φ ψ :
      FromAndCareful (PROP := PROP) (⌜φ ∧ ψ⌝) (⌜φ⌝)%I (⌜ψ⌝)%I.
    Proof. by rewrite /FromAndCareful bi.pure_and. Qed.
  End and.


  Section or.
    Implicit Types P Q : PROP.

    Global Instance or_fromorcareful P Q :
      FromOrCareful (P ∨ Q) P Q.
    Proof. rewrite /FromOrCareful //. Qed.

    Global Instance later_fromorcareful PQ P Q :
      FromOrCareful PQ P Q →
      FromOrCareful (▷ PQ) (▷ P)%I (▷ Q)%I.
    Proof. rewrite /FromOrCareful -later_or => <- //. Qed.
  End or.




  Section exist.
    Implicit Types TT : tele.

    Global Instance into_texist_later P {TT} P' :
      IntoTExist P TT P' →
      Inhabited TT →
      IntoTExist (▷ P)%I TT (tele_map (bi_later (PROP := PROP)) P').
    Proof.
      rewrite /IntoTExist bi_texist_exist => HP HTT.
      rewrite bi_texist_exist.
      rewrite HP.
      rewrite later_exist.
      apply exist_mono => tt /=.
      by rewrite tele_map_app.
    Qed.

    Global Instance from_texist_later P {TT} P' :
      FromTExist P TT P' →
      FromTExist (▷ P)%I TT (tele_map (bi_later (PROP := PROP)) P') | 5.
    Proof.
      rewrite /FromTExist !bi_texist_exist => <-.
      rewrite -later_exist_2.
      apply exist_mono => tt.
      by rewrite tele_map_app.
    Qed.

    Global Instance from_texist_behind_def P `(P' : A → PROP) {TT} P'' :
      FromExistCareful P P' →
      FromTExistDirect (bi_exist P') TT P'' →
      FromTExist P TT P'' | 15.
    Proof.
      rewrite /FromExistCareful /FromTExistDirect /FromTExist !bi_texist_exist => <- //.
    Qed.

    Lemma from_exist_careful_exist {A} (Φ : A → PROP) : FromExistCareful (∃ a, Φ a) Φ.
    Proof. by rewrite /FromExistCareful. Qed.

    Global Instance into_exist_careful_default `(Q : A → PROP) :
      IntoExistCareful (∃ a, Q a)%I Q.
    Proof. by rewrite /IntoExistCareful. Qed.

    Global Instance into_exist_careful_pure `(Q : A → Prop) :
      IntoExistCareful (PROP := PROP) (⌜∃ a, Q a⌝)%I (λ a, ⌜Q a⌝)%I.
    Proof. rewrite /IntoExistCareful. iDestruct 1 as (a) "%". eauto. Qed.

    Global Instance into_exist_careful_later Q `(Q' : A → PROP) :
      IntoExistCareful Q Q' →
      Inhabited A →
      IntoExistCareful (▷ Q)%I (λ a, ▷ Q' a)%I.
    Proof. 
      rewrite /IntoExistCareful => HQ HA.
      rewrite HQ later_exist //.
    Qed.

    Global Instance into_exist_careful_box Q `(Q' : A → PROP) :
      IntoExistCareful Q Q' →
      IntoExistCareful (□ Q)%I (λ a, □ Q' a)%I.
    Proof.
      rewrite /IntoExistCareful => ->.
      iDestruct 1 as (a) "HQ".
      by iExists _.
    Qed.

    Global Instance into_exist_careful2_from1 P `(Q : A → PROP) :
      IntoExistCareful P Q →
      IntoExistCareful2 P Q.
    Proof. done. Qed.
  End exist.




  Section normalize.

    Implicit Types (op : PROP → PROP → PROP).
    Implicit Types R : PROP.

    Global Instance normalize_prop_proper : Proper ((⊣⊢) ==> (⊣⊢) ==> (λ _ _, True) ==> (iff)) (NormalizeProp (PROP := PROP)).
    Proof. solve_proper. Qed.

    Global Instance simplify_prop_bin_op_proper op :
      Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢)) op →
      Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢) ==> (λ _ _, True) ==> (iff)) (SimplifyPropBinOp op).
    Proof. solve_proper. Qed.

    Lemma normalize_prop_refl (P : PROP) p : NormalizeProp P P p.
    Proof. rewrite /NormalizeProp //. Qed.

    Hint Immediate normalize_prop_refl : core.

    Lemma simplify_left_id op L R :
      LeftId (⊣⊢) L op →
      SimplifyPropBinOp op L R R true.
    Proof. rewrite /SimplifyPropBinOp => ->. eauto. Qed.

    Lemma simplify_right_id op L R :
      RightId (⊣⊢) R op →
      SimplifyPropBinOp op L R L true.
    Proof. rewrite /SimplifyPropBinOp => ->. eauto. Qed.

    Lemma simplify_left_absorb op L R :
      LeftAbsorb (⊣⊢) L op →
      SimplifyPropBinOp op L R L true.
    Proof. rewrite /SimplifyPropBinOp => ->. eauto. Qed.

    Lemma simplify_right_absorb op L R :
      RightAbsorb (⊣⊢) R op →
      SimplifyPropBinOp op L R R true.
    Proof. rewrite /SimplifyPropBinOp => ->. eauto. Qed.

    Lemma simplify_binop_default op L R :
      SimplifyPropBinOp op L R (op L R) false.
    Proof. rewrite /SimplifyPropBinOp. eauto. Qed.

    Global Instance simplify_sep_left_emp R : SimplifyPropBinOp bi_sep bi_emp R R true | 10.
    Proof. apply: simplify_left_id. Qed.

    Global Instance simplify_sep_right_emp R : SimplifyPropBinOp bi_sep R bi_emp R true | 10.
    Proof. apply: simplify_right_id. Qed.

    Global Instance simplify_sep_left_false R : SimplifyPropBinOp bi_sep False%I R False%I true | 10.
    Proof. apply: simplify_left_absorb. Qed.

    Global Instance simplify_sep_right_false R : SimplifyPropBinOp bi_sep R False%I False%I true | 10.
    Proof. apply: simplify_right_absorb. Qed.

    Global Instance simplify_sep_default L R : SimplifyPropBinOp bi_sep L R (L ∗ R) false | 30.
    Proof. apply simplify_binop_default. Qed.

    Global Instance simplify_or_left_emp R : SimplifyPropBinOp bi_or False%I R R true | 10.
    Proof. apply: simplify_left_id. Qed.

    Global Instance simplify_or_right_emp R : SimplifyPropBinOp bi_or R False%I R true | 10.
    Proof. apply: simplify_right_id. Qed.

    Global Instance simplify_or_left_false R : SimplifyPropBinOp bi_or True%I R True%I true | 10.
    Proof. apply: simplify_left_absorb. Qed.

    Global Instance simplify_or_right_false R : SimplifyPropBinOp bi_or R True%I True%I true | 10.
    Proof. apply: simplify_right_absorb. Qed.

    Global Instance simplify_or_default L R : SimplifyPropBinOp bi_or L R (L ∨ R) false | 30.
    Proof. apply simplify_binop_default. Qed.

    Global Instance simplify_and_left_True R : SimplifyPropBinOp bi_and True%I R R true | 10.
    Proof. apply: simplify_left_id. Qed.

    Global Instance simplify_and_right_True R : SimplifyPropBinOp bi_and R True%I R true | 10.
    Proof. apply: simplify_right_id. Qed.

    Global Instance simplify_and_left_false R : SimplifyPropBinOp bi_and False%I R False%I true | 10.
    Proof. apply: simplify_left_absorb. Qed.

    Global Instance simplify_and_right_false R : SimplifyPropBinOp bi_and R False%I False%I true | 10.
    Proof. apply: simplify_right_absorb. Qed.

    Global Instance simplify_and_default L R : SimplifyPropBinOp bi_and L R (L ∧ R) false | 30.
    Proof. apply simplify_binop_default. Qed.

    Lemma normalize_op op L L' R R' P pl pr po pa pt:
      Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢)) op →
      TCNoBackTrack (NormalizeProp L L' pl) →
      TCNoBackTrack (NormalizeProp R R' pr) →
      TCNoBackTrack (SimplifyPropBinOp op L' R' P po) →
      TCOrB pl pr pa →
      TCOrB pa po pt →
      NormalizeProp (op L R) P pt.
    Proof. rewrite /SimplifyPropBinOp {1 2 3}/NormalizeProp => Hop [->] [->] [->]. eauto. Qed.

    Global Instance normalize_sep_op L L' R R' P pl pr po pa pt:
      TCNoBackTrack (NormalizeProp L L' pl) →
      TCNoBackTrack (NormalizeProp R R' pr) →
      TCNoBackTrack (SimplifyPropBinOp bi_sep L' R' P po) →
      TCOrB pl pr pa →
      TCOrB pa po pt →
      NormalizeProp (L ∗ R)%I P pt | 10.
    Proof. apply: normalize_op. Qed.

    Global Instance normalize_and_op L L' R R' P pl pr po pa pt:
      TCNoBackTrack (NormalizeProp L L' pl) →
      TCNoBackTrack (NormalizeProp R R' pr) →
      TCNoBackTrack (SimplifyPropBinOp bi_and L' R' P po) →
      TCOrB pl pr pa →
      TCOrB pa po pt →
      NormalizeProp (L ∧ R)%I P pt | 10.
    Proof. apply: normalize_op. Qed.

    Global Instance normalize_or_op L L' R R' P pl pr po pa pt:
      TCNoBackTrack (NormalizeProp L L' pl) →
      TCNoBackTrack (NormalizeProp R R' pr) →
      TCNoBackTrack (SimplifyPropBinOp bi_or L' R' P po) →
      TCOrB pl pr pa →
      TCOrB pa po pt →
      NormalizeProp (L ∨ R)%I P pt | 10.
    Proof. apply: normalize_op. Qed.

    Global Instance normalize_later_gen (P P' P'' : PROP) p : 
      TCNoBackTrack (NormalizeProp P P' p) → 
      TCIf (TCEq P' True%I) (TCEq P'' True%I)
        (TCIf (TCAnd (TCEq P' emp%I) (BiAffine PROP)) (TCEq P'' emp%I)
          (TCEq P'' (▷ P')%I)) →
      NormalizeProp (▷ P)%I (P'')%I p | 10.
    Proof. 
      rewrite /NormalizeProp => [[->]].
      case => [-> -> | +].
      { by rewrite later_True. }
      case => [[-> HP] -> | -> //].
      by rewrite later_emp.
    Qed.

    Global Arguments eq_rect_r [A x] _ _ [y] !_ /. (* causes simpl to reduce eq_rect_r, like cbn will do *)

    Global Instance normalize_pure (φ : Prop) :
      TCIf (TCEq φ True) False TCTrue → (* guards against the NormalizeProp True True true instances *)
      SolveSepSideCondition φ → NormalizeProp (PROP := PROP) (⌜φ⌝)%I True%I true | 5.
    Proof.
      rewrite /NormalizeProp => _ Hφ.
      by apply pure_True.
    Qed.

    Global Instance normalize_pure_false (φ : Prop) :
      TCIf (TCEq φ False) False TCTrue → (* guards against the NormalizeProp False False true instances *)
      SolveSepSideCondition (¬φ) → NormalizeProp (PROP := PROP) (⌜φ⌝)%I False%I true | 5.
    Proof.
      rewrite /NormalizeProp => Hφ.
      by apply pure_False.
    Qed.

    Global Instance normalize_refl (P : PROP) : NormalizeProp P P false | 50.
    Proof. eauto. Qed.
  End normalize.

  Section normalized.
    Global Instance wrap_through_eq (P Q Q' : PROP) b: 
      TCNoBackTrack (NormalizeProp P Q' b) →
      TCEq Q Q' → (* this indirection allows us to use this typeclass with (tele_app Q' _) as Q *)
      NormalizedProp P Q b.
    Proof. case. by move => HPQ ->. Qed.
  End normalized.




  Section all.

    Global Instance into_forall_careful_default `(Q : A → PROP) :
      IntoForallCareful (∀ a, Q a)%I Q | 10.
    Proof. by rewrite /IntoForallCareful. Qed.

    Lemma into_forall_careful_later Q `(Q' : A → PROP) :
      IntoForallCareful Q Q' →
      IntoForallCareful (▷ Q)%I (λ a, ▷ Q' a)%I.
    Proof.
      rewrite /IntoForallCareful => ->.
      iIntros "H" (a) "!>".
      by iApply "H".
    Qed.

  End all.




  Section later_except0.

    Global Instance timeless_later_to_except0 (P : PROP) : 
      Timeless P →
      LaterToExcept0 P P | 50.
    Proof. rewrite /LaterToExcept0 /Timeless //. Qed.

    Global Instance later_to_except0_default (P : PROP) :
      LaterToExcept0 P (▷ P)%I | 51.
    Proof. rewrite /LaterToExcept0. apply except_0_intro. Qed.

    Global Instance later_to_except0_sep (P P1 P1' P2 P2' : PROP) : 
      IntoSepCareful P P1 P2 →
      LaterToExcept0 P1 P1' →
      LaterToExcept0 P2 P2' →
      LaterToExcept0 P (P1' ∗ P2')%I | 10.
    Proof.
      rewrite /IntoSepCareful /LaterToExcept0 => ->.
      rewrite later_sep except_0_sep => -> -> //.
    Qed.

    Global Instance exist_later_to_except_0_inh (P : PROP) {A} (R R': A → PROP) :
      IntoExistCareful P R → (* not IntoExist, Careful should have fewer instances and thus be faster *)
      Inhabited A →
      (∀ a : A, LaterToExcept0 (R a) (R' a)) → (* we can always find this by later_to_except0_default *)
      LaterToExcept0 P (∃ a : A, R' a)%I | 30.
    Proof.
      rewrite /LaterToExcept0 /IntoExistCareful => HPR HA HR1.
      rewrite HPR.
      rewrite later_exist.
      rewrite -except_0_exist_2.
      apply exist_mono => //.
    Qed.

    Global Instance later_to_except_0_disj (P1 P2 P1' P2' : PROP) :
      LaterToExcept0 P1 P1' →
      LaterToExcept0 P2 P2' →
      LaterToExcept0 (P1 ∨ P2)%I (P1' ∨ P2')%I | 40.
    Proof.
      rewrite /LaterToExcept0 bi.later_or bi.except_0_or => -> -> //.
    Qed.

  End later_except0.




  Section wand.

    Global Instance wand_into_wand2 p (R V : PROP) : IntoWand2 p (R -∗ V)%I R V.
    Proof. by rewrite /IntoWand2 intuitionistically_if_elim //. Qed.

    Global Instance impl_into_wand2 p (R V : PROP) : 
      (TCOr (Affine R) (Absorbing (□?p (R → V)))) →
      (TCOr (TCOr (TCEq p true) (Affine (R → V))) (Absorbing R)) →
      IntoWand2 p (R → V)%I R V.
    Proof. 
      rewrite /IntoWand2 => HR1 HR2.
      apply wand_intro_l.
      erewrite sep_and; first last =>//.
      - case: HR2 => [HRV|HR]; [  | by (left + right) ].
        case: HRV => [-> | HRV] => /=; apply _.
      - rewrite intuitionistically_if_elim.
        by rewrite impl_elim_r.
    Qed.

    Global Instance box_wand_into_wand (P R V : PROP) :
      IntoWand2 true P R V →
      IntoWand2 true (□ P)%I R V.
    Proof. rewrite /IntoWand2 /= intuitionistically_idemp //. Qed.
  End wand.


  Section findincontext.

    Lemma findinextcontext_from_reg (Δ : envs PROP) PTC i p P :
      FindInContext Δ PTC i p P →
      FindInExtendedContext Δ PTC (Some i) p P.
    Proof.
      case => HΔi HpP.
      by apply fic_ext_lookup_Some.
    Qed.

    Lemma findinextcontext_from_spat (Δ : envs PROP) PTC i P :
      FindInContext Δ PTC i false P →
      FindInExtendedContext Δ PTC (Some i) false P.
    Proof. apply findinextcontext_from_reg. Qed.

    Lemma findinextcontext_from_pers (Δ : envs PROP) PTC i P :
      FindInContext Δ PTC i true P →
      FindInExtendedContext Δ PTC (Some i) true P.
    Proof. apply findinextcontext_from_reg. Qed.

  End findincontext.


  Section tcorb.
    Global Instance left_true p : TCOrB true p true.
    Proof. done. Qed.

    Global Instance right_true p : TCOrB p true true.
    Proof. case: p => //. Qed.

    Global Instance both_false : TCOrB false false false.
    Proof. done. Qed.

  End tcorb.

  Section fracsub.
    Global Instance frac_sub_all p :
      FracSub p p None | 50.
    Proof. done. Qed.

    Global Instance frac_sub_half (p : Qp) :
      FracSub p (p/2)%Qp (Some $ p/2)%Qp | 45.
    Proof. rewrite /FracSub Qp.div_2 //. Qed.
    (* better instances are provided by the CmraSubtract infrastructure in lib/own_hints.v *)
  End fracsub.


  Section atom_and_connective.
    Implicit Types P : PROP.

    Global Instance wand_atom_connective p P V P' :
        AtomIntoWand p P V P' → AtomAndConnective p P.
    Proof. by split. Qed.

    Global Instance forall_atom_connective p P {A} (Q : A → PROP) :
        AtomIntoForall P Q → AtomAndConnective p P.
    Proof. by split. Qed.

    Global Instance exist_atom_connective p P {A} (Q : A → PROP) :
        AtomIntoExist P Q → AtomAndConnective p P.
    Proof. by split. Qed.
  End atom_and_connective.


  Section atom_into_connective_instances.
    Implicit Types A B C : PROP.

    Global Instance wand_into_connective A B C :
      AtomIntoWand false A B C → AtomIntoConnective A (B -∗ C) := λ x, x.

    Global Instance forall_into_connective A `(Φ : T → PROP):
      AtomIntoForall A Φ → AtomIntoConnective A (∀ (a : T), Φ a) := λ x, x.

    Global Instance exist_into_connective A `(Φ : T → PROP):
      AtomIntoExist A Φ → AtomIntoConnective A (∃ (a : T), Φ a) := λ x, x.
  End atom_into_connective_instances.


  Section intopure.
    (* these instances ensure that when introducing empty_hyp_first/last, 
        they dont end up in the spatial context *)
    Global Instance first_hyp_pure : 
      IntoPure (PROP := PROP) (ε₀)%I True.
    Proof. rewrite /IntoPure empty_hyp_first_eq. eauto. Qed.

    Global Instance last_hyp_pure : 
      IntoPure (PROP := PROP) (ε₁)%I True.
    Proof. rewrite /IntoPure empty_hyp_last_eq. eauto. Qed.

    Global Instance last_hyp_spatial_pure :
      IntoPure (PROP := PROP) empty_hyp_last_spatial True.
    Proof. rewrite /IntoPure empty_hyp_last_spatial_eq. eauto. Qed.

    Global Instance empty_goal_pure :
      IntoPure (PROP := PROP) (χ)%I True.
    Proof. rewrite /IntoPure empty_goal_eq. eauto. Qed.

    Global Instance empty_goal_timeless : 
      Timeless (PROP := PROP) emp → Timeless (PROP := PROP) (χ)%I.
    Proof. by rewrite empty_goal_eq. Qed.

    (* Affine instances so that introduction of ε₀, ε₁ and χ in linear logics
      do not cause problems *)
    Global Instance first_hyp_affine : Affine (PROP := PROP) (ε₀)%I.
    Proof. rewrite empty_hyp_first_eq. apply _. Qed.
    Global Instance last_hyp_affine : Affine (PROP := PROP) (ε₁)%I.
    Proof. rewrite empty_hyp_last_eq. apply _. Qed.
    Global Instance last_hyp_spatial_affine : Affine (PROP := PROP) (empty_hyp_last_spatial)%I.
    Proof. rewrite empty_hyp_last_spatial_eq. apply _. Qed.
    Global Instance empty_goal_affine : Affine (PROP := PROP) (χ)%I.
    Proof. rewrite empty_goal_eq. apply _. Qed.
  End intopure.


  Section simplify_pure_hyp.
    Global Instance safe_gives_simplify φin φout :
      SimplifyPureHypSafe φin φout → SimplifyPureHyp φin φout | 10.
    Proof. rewrite /SimplifyPureHypSafe /SimplifyPureHyp => -> //. Qed.

    Global Instance simplify_neq_false b :
      SimplifyPureHypSafe (b ≠ false) (b = true).
    Proof. rewrite /SimplifyPureHypSafe. destruct b; split; eauto. Qed.

    Global Instance simplify_neq_true b :
      SimplifyPureHypSafe (b ≠ true) (b = false).
    Proof.
      rewrite /SimplifyPureHypSafe. destruct b; split; eauto. 
      move => H. by contradict H. 
    Qed.

    Global Instance simplify_neq_nil `(xs : list A) :
      SimplifyPureHypSafe (xs ≠ []) (∃ x xs', xs = x :: xs').
    Proof.
      rewrite /SimplifyPureHypSafe. destruct xs; split; eauto.
      - move => H. by contradict H.
      - move => [x [xs' Hx]]. contradict Hx. done.
    Qed.
  End simplify_pure_hyp.


  Section misc.
    Global Instance is_evar_free_inst {A : Type} (a : A) : IsEvarFree a.
    Proof. by constructor. Qed.
  End misc.
End prop_instances.

(* Ltac instances *)

Global Hint Extern 0 (FromExistCareful ?L ?R) =>
  let rec get_head trm :=
    lazymatch trm with
    | (?H _) => get_head H
    | _ => trm
    end in
  let H := fresh "H" in
  assert (FromExistCareful L R) as H 
    by notypeclasses refine (from_exist_careful_exist _);
  let rec solve :=
    lazymatch goal with
    | |- FromExistCareful ?L _ =>
      let left_head := get_head L in
      lazymatch left_head with
      | @bi_exist => exact H
      | _ => first
        [ let body := eval unfold left_head in left_head in
          assert_succeeds (assert (TCEq left_head body) by tc_solve);
          progress (unfold left_head; simpl); solve
        | fail 2  "Head term" left_head " is opaque, so not unfolded to an existential"]
      end
    end in 
  solve : typeclass_instances.

Lemma exactly_entails {PROP : bi} (P : PROP) : P ⊢ P.
Proof. done. Qed.

Ltac2 unify_repeated_existentials_from_ltac1
    (inputf1 : Ltac1.t) (resulttele1 : Ltac1.t) (resultfun1 : Ltac1.t) : unit :=
  let inputf := Option.get (Ltac1.to_constr inputf1) in
  let resulttele := Option.get (Ltac1.to_constr resulttele1) in
  let resultfun := Option.get (Ltac1.to_constr resultfun1) in
  let (output, output_tele) := get_repeated_quantifier_function inputf in
  unify $resulttele $output_tele;
  unify $resultfun $output;
  refine '(exactly_entails _).

Global Hint Extern 4 (FromTExistDirect (bi_exist ?input_fun) ?TT ?Q') =>
  let f := ltac2:(inputf1 resulttele1 resultfun1 |- 
    unify_repeated_existentials_from_ltac1 inputf1 resulttele1 resultfun1
  ) in
  f input_fun TT Q' : typeclass_instances.

Global Hint Extern 4 (IntoTExistExact (bi_exist ?input_fun) ?TT ?Q') =>
  let f := ltac2:(inputf1 resulttele1 resultfun1 |- 
    unify_repeated_existentials_from_ltac1 inputf1 resulttele1 resultfun1
  ) in
  f input_fun TT Q' : typeclass_instances.

Global Hint Extern 4 (FindInContext ?Δ ?PTC ?k ?r ?R) =>
  let rec find Γ i p :=
    lazymatch Γ with
    | Esnoc ?Γ ?j ?Q => first 
      [let hole := eval cbn beta in (PTC p Q) in eassert hole by tc_solve ; unify i j; unify R Q
      |find Γ i p]
    end in
  let Δ' := eval cbn [envs_option_delete envs_delete env_delete ident_beq string_beq ascii_beq beq] in Δ in
  first [
    is_evar k; match Δ' with | Envs ?Γp ?Γs _ => first [unify r false; find Γs k false | unify r true; find Γp k true]; split; [pm_reflexivity|done] end
    | split; [pm_reflexivity|tc_solve]
  ] : typeclass_instances.

Global Hint Extern 4 (FindInExtendedContext ?Δ ?PTC ?mi ?r ?R) =>
  first 
  [ eapply fic_ext_lookup_empty_first; cbn beta; tc_solve
  | eapply findinextcontext_from_spat; tc_solve
  | eapply fic_ext_lookup_empty_last_spatial; cbn beta; tc_solve
  | eapply findinextcontext_from_pers; tc_solve
  | eapply fic_ext_lookup_empty_last; cbn beta; tc_solve] : typeclass_instances.

(* first masks are syntactically equal, even though they might be evars *)
Global Hint Extern 45 (CombinedModalitySafe (fupd ?E1 ?E1) (fupd ?E3 ?E2) _) =>
  eapply combined_fupd_fupd_first_mask_same : typeclass_instances. (* this will try to unify E3 with E1 *)

Global Hint Extern 45 (CombinedModalitySafe (fupd ?E1 ?E2) (fupd ?E3 ?E3) _) =>
  eapply combined_fupd_fupd_last_mask_same : typeclass_instances. (* this will try to unify E2 with E3 *)

Global Hint Extern 60 (CombinedModalitySafe (fupd ?E1 ?E2) (fupd ?E2 ?E3) _) => (* the middle modality is equal, but which one to unify with? *)
      (* first check if there is only one option *)
  (assert_fails (unify E2 E3); unify E1 E2; eapply combined_fupd_fupd_first_mask_same) ||
  (assert_fails (unify E1 E2); unify E2 E3; eapply combined_fupd_fupd_last_mask_same) ||
  (is_evar E2; assert_fails (is_evar E3) (* then probably introduce last *); unify E2 E3; eapply combined_fupd_fupd_last_mask_same) ||
  (is_evar E2; assert_fails (is_evar E1) (* then probably introduce first *); unify E2 E1; eapply combined_fupd_fupd_first_mask_same)
       : typeclass_instances.

(* fixes for Iris (upstream?) *)

Global Existing Instance class_instances.from_forall_forall | 0.
(* see tests/to_solve_goal: tforall_simplifies *)






















