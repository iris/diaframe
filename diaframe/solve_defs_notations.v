From diaframe Require Import solve_defs.
From stdpp Require Import base.



(* SolveSep *)
Notation "'‖' M '‖' ∃ x1 .. xn '‖' L '(∗)' R" :=
  (SolveSep 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    M
    (λ x1, .. (λ xn, L%I) .. )
    (λ x1, .. (λ xn, R%I) .. )
  )
  (at level 20, M at level 9, x1 binder, xn binder, L, R at level 200, only printing, format
  "‖  M  ‖  ∃  x1 .. xn  ‖  L  (∗)  R"
).

Notation "'‖' M '‖' L '(∗)' R" :=
  (SolveSep 
    (TT := TeleO)
    M
    L
    R
  )
  (at level 20, M at level 9, L, R at level 200, only printing, format
  "‖  M  ‖  L  (∗)  R"
).

Notation "'‖' ∃ x1 .. xn '‖' L '(∗)' R" :=
  (SolveSep 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    id
    (λ x1, .. (λ xn, L%I) .. )
    (λ x1, .. (λ xn, R%I) .. )
  )
  (at level 20, x1 binder, xn binder, L, R at level 200, only printing, format
  "‖  ∃  x1 .. xn  ‖  L  (∗)  R"
).

Notation "'‖' L '(∗)' R" :=
  (SolveSep 
    (TT := TeleO)
    id
    L
    R
  )
  (at level 20, L, R at level 200, only printing, format
  "‖  L  (∗)  R"
).


(* SolveOne *)
Notation "'‖' M '‖' ∃ x1 .. xn '‖' L" :=
  (SolveOne 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    M
    (λ x1, .. (λ xn, L%I) .. )
  )
  (at level 20, M at level 9, x1 binder, xn binder, L at level 200, only printing, format
  "‖  M  ‖  ∃  x1 .. xn  ‖  L"
).

Notation "'‖' M '‖' L" :=
  (SolveOne 
    (TT := TeleO)
    M
    L
  )
  (at level 20, M at level 9, L at level 200, only printing, format
  "‖  M  ‖  L"
).

Notation "'‖' ∃ x1 .. xn '‖' L" :=
  (SolveOne 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    id
    (λ x1, .. (λ xn, L%I) .. )
  )
  (at level 20, x1 binder, xn binder, L at level 200, only printing, format
  "‖  ∃  x1 .. xn  ‖  L"
).

Notation "'‖' L" :=
  (SolveOne 
    (TT := TeleO)
    id
    L
  )
  (at level 20, L at level 200, only printing, format
  "‖  L"
).


(* SolveSepFoc *)
Notation "'‖' M '‖' ∃ x1 .. xn '‖' L '(∗)' R '(∨)' G" :=
  (SolveSepFoc 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    M
    (λ x1, .. (λ xn, L%I) .. )
    (λ x1, .. (λ xn, R%I) .. )
    G
  )
  (at level 20, M at level 9, x1 binder, xn binder, L, R, G at level 200, only printing, format
  "‖  M  ‖ '//' '[   '   ∃  x1 .. xn  ‖  L  (∗)  R ']' '//' (∨) '//' '[   '   G ']'"
).

Notation "'‖' M '‖' L '(∗)' R '(∨)' G" :=
  (SolveSepFoc 
    (TT := TeleO)
    M
    L
    R
    G
  )
  (at level 20, M at level 9, L, R, G at level 200, only printing, format
  "‖  M  ‖ '//' '[   '   L  (∗)  R ']' '//' (∨) '//' '[   '   G ']'"
).

Notation "'‖' ∃ x1 .. xn '‖' L '(∗)' R '(∨)' G" :=
  (SolveSepFoc 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    id
    (λ x1, .. (λ xn, L%I) .. )
    (λ x1, .. (λ xn, R%I) .. )
    G
  )
  (at level 20, x1 binder, xn binder, L, R, G at level 200, only printing, format
  "‖ '//' '[   '   ∃  x1 .. xn  ‖  L  (∗)  R ']' '//' (∨) '//' '[   '   G ']'"
).

Notation "'‖' L '(∗)' R '(∨)' G" :=
  (SolveSepFoc 
    (TT := TeleO)
    id
    L
    R
    G
  )
  (at level 20, L, R, G at level 200, only printing, format
  "‖ '//' '[   '   L  (∗)  R ']' '//' (∨) '//' '[   '   G ']'"
).


(* SolveOneFoc *)
Notation "'‖' M '‖' ∃ x1 .. xn '‖' L '(∨)' G" :=
  (SolveOneFoc 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    M
    (λ x1, .. (λ xn, L%I) .. )
    G
  )
  (at level 20, M at level 9, x1 binder, xn binder, L, G at level 200, only printing, format
  "‖  M  ‖ '//' '[   '   ∃  x1 .. xn  ‖  L ']' '//' '(∨)' '//' '[   '   G ']'"
).

Notation "'‖' M '‖' L '(∨)' G" :=
  (SolveOneFoc
    (TT := TeleO)
    M
    L
    G
  )
  (at level 20, M at level 9, L, G at level 200, only printing, format
  "‖  M  ‖ '//' '[   '   L ']' '//' (∨) '//' '[   '   G ']'"
).

Notation "'‖' ∃ x1 .. xn '‖' L '(∨)' G" :=
  (SolveOneFoc 
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    id
    (λ x1, .. (λ xn, L%I) .. )
    G
  )
  (at level 20, x1 binder, xn binder, L, G at level 200, only printing, format
  "‖ '//' '[   '   ∃  x1 .. xn  ‖  L ']' '//' '(∨)' '//' '[   '   G ']'"
).

Notation "'‖' L '(∨)' G" :=
  (SolveOneFoc
    (TT := TeleO)
    id
    L
    G
  )
  (at level 20, L, G at level 200, only printing, format
  "‖ '//' '[   '   L ']' '//' (∨) '//' '[   '   G ']'"
).


(* IntroduceVars *)
Notation "'‖' ∀ x1 .. xn ',' G" :=
  (IntroduceVars
    (TT := TeleS (λ x1, .. (TeleS (λ xn, TeleO)) ..))
    (λ x1, .. (λ xn, G%I) .. )
  )
  (at level 20, x1 binder, xn binder, G at level 200, format
  "‖  ∀  x1 .. xn  ,  G"
).

Notation "'‖' ∀ '-' ',' G" :=
  (IntroduceVars
    (TT := TeleO)
    G
  )
  (at level 20, G at level 200, format
  "‖  ∀  -  ,  G"
).


(* IntroduceHyp *)
Notation "'‖' L (-∗) R" :=
  (IntroduceHyp L R)
  (at level 20, L, R at level 200, format
  "‖  L  (-∗)  R"
).


