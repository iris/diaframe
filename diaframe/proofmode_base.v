From diaframe Require Import util_instances.
From diaframe Require Export util_classes tele_utils solve_defs solve_defs_notations solve_defs_ipm_support.
From diaframe.hint_search Require Export search_biabd search_abd instances_base search_biabd_disj biabd_disj_from_path.
From diaframe.steps Require Export tactics small_steps.

(* Importing this file should give you access to the Diaframe's automation,
   without logic/language specific hints. To expand the automation, define
   your own hints and/or import hint libraries in diaframe.lib
 *)