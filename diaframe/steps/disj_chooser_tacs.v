From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes utils_ltac2.
From diaframe.steps Require Import automation_state ltac2_store diaframe_options.


(* At some points, Diaframe can no longer postpone the choice of the side of a disjunction.
   Instead of hardwiring a particular side-choosing strategy, we make the tactics parametric 
   in such strategies.

   This file defines an interface to define these strategies easily. *)


(* abusing modules as 'namespaces' *)
Module PickSide.
  Ltac2 Type t := [ Left | Right | Stop ].
End PickSide.

Module PickEvent.
  Ltac2 Type t := [ NoHintSep | LeftEasier | NoHintOne | LeftModOne ].
End PickEvent.


Ltac2 chooser_always_left : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit :=
  fun _ _ e c => c PickSide.Left.

Ltac2 chooser_safe : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit :=
  fun _ _ e c => 
    match e with
    | PickEvent.NoHintSep => c PickSide.Left
    | PickEvent.NoHintOne => c PickSide.Left
    | _ => c PickSide.Stop
    end.

Ltac2 chooser_safest : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit :=
  fun _ _ e c => c PickSide.Stop.

Ltac2 chooser_smash : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit :=
  fun state gid e c => 
    let store' := store.copy (automation_state.get_rp_store_for state gid) in
    let steps' := automation_state.get_steps_for state gid in
    Control.plus 
      (fun _ => c PickSide.Left)
      (fun _ => 
        automation_state.set_rp_store_for state gid store';
        automation_state.set_steps_for state gid steps';
        c PickSide.Right
      )
  .

(* We define the options *)
Ltac2 Type diaframe_option ::= [ DisjChooser (automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit) ].


(* default option, but explicit *)
Tactic Notation "--disj-daring" :=
  ltac2:(throw_option (DisjChooser chooser_always_left)).

Tactic Notation "--safe" :=
  ltac2:(throw_option (DisjChooser chooser_safe)).

Tactic Notation "--safest" :=
  ltac2:(throw_option (DisjChooser chooser_safest)).

(* We would maybe also like to have a way of giving a
  sequence of PickSides to choose. This could in principle be
  done in Ltac2, since we have mutable state :) *)

(* There is no "--smash" option - one should use iSmash for that! *)



