From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import solve_defs utils_ltac2.
From diaframe.steps Require Import automation_state reductions introduce_prop.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file contains 'case 1' of Diaframe's proof search strategy: introducing foralls. *)


Section introduce_var.
  Context {PROP : bi}.
  Implicit Types TT : tele.

  Lemma introduce_vars {TT} Q :
    (∀.. (tt : TT), tele_app Q tt) ⊢ IntroduceVars (PROP := PROP) (TT := TT) Q.
  Proof. by unseal_diaframe. Qed.

  Lemma tac_introduce_vars_teleS {TT : tele@{universes.Quant}} Δ Q :
    (my_id (λ Δ', ∀.. (tt : TT), envs_entails Δ' $ tele_app Q tt) Δ) →
    envs_entails Δ $ IntroduceVars (PROP := PROP) (TT := TT) Q.
  Proof.
    rewrite /my_id -introduce_vars envs_entails_unseal bi_tforall_forall tforall_forall => Htt.
    apply forall_intro => tt.
    apply Htt.
  Qed.

  (* This rule is a bit silly, of course. You may wonder why [IntroduceVars] takes a telescope,
     instead of just a type. Well, [BiAbd]s have a telescope as output, which becomes the argument
     to [IntroduceVars] -- which means we need it. *)
  Lemma tac_introduce_vars_teleO Δ Q :
    envs_entails Δ $ Q →
    envs_entails Δ $ IntroduceVars (PROP := PROP) (TT := [tele]) Q.
  Proof. by unseal_diaframe. Qed.

End introduce_var.


Ltac2 introduceVarStep (state : automation_state.t) (gid : goal_id)
    (Δ : constr) (tt : constr) (q : constr) : goal_id list :=
  lazy_match! tt with
  | TeleO => apply tac_introduce_vars_teleO; [gid]
  | TeleS ?tf =>
    apply tac_introduce_vars_teleS;
    ltac1:(simpl_diaframe_var_intro);
    (* in case the telescope contains Prop things, we want to simplify them.
      But the cost of always checking is quite high:
      - intros cán introduce a bunch of hyps
      - calling Fresh and Std.intro is annoying because the names will be awful
      - we could use our evar trick to catch all new names but that also seems wasteful
      Therefore we only check for the often occurring case of a single type, and
      check if that type is a Proposition. *)
    lazy_match! tf with
    | (λ _ : ?vt, TeleO) =>
      lazy_match! (Constr.type vt) with
      | Prop => (* we call simplification machinery *)
        let store := automation_state.get_rp_store_for state gid in
        introduce_one_prop store; (* this could kill the goal! *)
        ifcatch fail0 (fun _ => []) (fun _ => [gid])
      | _ => intros; [gid]
      end
    | _ => intros; [gid]
    end
    (*; subst; simpl *)
  end.


Ltac introduceVarStep :=
  ltac2:(
    lazy_match! goal with
    | [|- envs_entails ?Δ (@IntroduceVars _ ?tt ?p)] =>
      let (state, gid) := automation_state.new () in
      let _ := introduceVarStep state gid Δ tt p in
      ()
    end
  ).