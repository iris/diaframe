From diaframe Require Import utils_ltac2 solve_defs.
From diaframe.steps Require Import diaframe_options automation_state ltac2_store.
From iris.proofmode Require Import proofmode environments.


Ltac2 Type diaframe_option ::= [ GoalInspector (goal_id -> goal_id list -> (goal_id -> unit) -> goal_id list) ].


(* Some utility functions *)
Ltac2 incr (s : ('a, int) store.t) (k : 'a) : unit :=
  (* returns whether this is a new goal *)
  match store.get s k with
  | None => store.set s k 1
  | Some i => store.set s k (Int.add i 1)
  end.

Ltac2 rec get_goal_family (g : goal_id) : goal_id list :=
  let i := g.(goal_id_internal) in
  if Int.equal i 1 then
    [g]
  else
    g :: get_goal_family { goal_id_internal := (Int.div i 2) }
  .

Ltac2 rec get_family_total (s : (goal_id, int) store.t) (g : goal_id) : int :=
  let subgoals := get_goal_family g in
  fold_left Int.add 0 (List.map (Option.default 0) (List.map (store.get s) subgoals)).


Ltac2 rec find_index (xs : 'a list) (f : 'a -> bool) : int option :=
  match xs with
  | [] => None
  | x :: xs => 
    if f x then 
      Some 0
    else
      match find_index xs f with
      | None => None
      | Some i => Some (Int.add i 1)
      end
  end.



(* A goal inspector that prints the goal after every step. A closure since it uses
some mutable state to keep track of things. *)
Ltac2 make_goal_printer (_ : unit) : goal_id -> goal_id list -> 'a -> goal_id list :=
  let step_store : (goal_id, int) store.t := store.new goal_id_eq in
  let active_goals : (goal_id, unit) store.t := store.new goal_id_eq in
  let initial_gid := { goal_id_internal := 1 } in
  store.set step_store initial_gid 0;
  store.set active_goals initial_gid ();
  let has_detected_split := { contents := false } in
  let active_goal_ordinal : goal_id -> int :=
    (fun gid =>
      let all_active_gids := List.map (fun pr => let (p, _) := pr in p) (store.to_list active_goals) in
      let sorted_gids := List.sort (fun g1 g2 => Int.compare (g1.(goal_id_internal)) (g2.(goal_id_internal))) all_active_gids in
      Int.add 1 (Option.get (find_index sorted_gids (fun ogid => goal_id_eq gid ogid)))
    ) in
  (fun gid gids _ =>
    (if Int.gt (List.length gids) 1 then
       has_detected_split.(contents) := true
     else
       ()
    );
    (if List.equal goal_id_eq [gid] gids then (* triggers both when split and when solved *)
       ()
     else
       store.remove active_goals gid;
       List.iter (fun new_gid => store.set active_goals new_gid ()) gids
    );
    incr step_store gid;
    let total_steps := get_family_total step_store gid in
    (if Int.equal (List.length gids) 0 then
       if has_detected_split.(contents) then
         printf "Goal with id=%i was solved in %i steps" (gid.(goal_id_internal)) total_steps
       else
         printf "Goal was solved in %i steps" total_steps
     else
       ()
    );
    let num_active_goals := List.length (store.to_list active_goals) in
    Control.dispatch (List.map (fun ngid _ =>
      lazy_match! goal with
      | [|- envs_entails _ ?g] =>
        lazy_match! g with
        | bi_forall _ => () (* don't print these, they are not important *)
        | bi_wand _ _ => () (* don't print these, they are not important *)
        | _ =>
          if has_detected_split.(contents) then
            printf "Goal %i/%i (id=%i), step %i:  %t" (active_goal_ordinal ngid) num_active_goals (ngid.(goal_id_internal)) total_steps g
          else
            printf "Step %i:  %t" total_steps g
        end
      | [|- _] => ()
      end
    ) gids);
    gids
  ).

Tactic Notation "--print-goals" :=
  ltac2:(throw_option
    (GoalInspector (make_goal_printer ()))
  ).



(* A goal inspector that shelves goals matching a provided pattern.
Unfortunately, there is no way to make Ltac parse patterns.
We really do need patterns instead of open_constrs, since the goal will very often
mention variables that are not yet in scope.

After some testing, ltac2 patterns also do not work as expected: I think it might have
something to do with Funclass coercions.. See the bottom of this file for a test
case in HeapLang.

So we choose the next best thing: thunked open_constrs. Since the evars are only 
generated when the thunk is called, this still works behind variables not yet in scope.

And in the end, we are then able to stay in Ltac1. To get a thunked open_constr,
we wrap the open_constr pattern in another tactic, which will generate the evars
only when we execute it.

We therefore build an ltac2 intermediate tactic. *)

Ltac2 disable_selected_goals (selector : unit -> bool) : goal_id -> goal_id list -> (goal_id -> unit) -> goal_id list :=
  fun gid gids gid_disabler =>
    Control.dispatch (List.map (fun ngid _ =>
      if selector () then
        gid_disabler ngid
      else
        ()
    ) gids);
    gids
  .


Tactic Notation "goal-matches" open_constr(pat) :=
  lazymatch goal with
  | |- envs_entails _ ?G =>
    eunify G pat
  end.

Tactic Notation "--until" tactic1(tac) :=
  let f := ltac2:(tac1 |-
    throw_option (GoalInspector (disable_selected_goals (fun _ =>
      plus_once (fun _ =>
        Ltac1.run tac1;
        true
      ) (fun _ =>
        false
      )
    )))
  ) in
  f tac.


(** In HeapLang, it seems we cannot use the (_ ;; _)%E pattern with match,
only with unify
Goal ((! #0 ;; ! #1)%E = (! #0 ;; ! #1)%E).
Proof.
lazymatch goal with
| |- (?l = _) =>
  idtac l;
  eunify l (_ ;; _)%E
end.
Check (Lam BAnon NONE NONE)%E.
lazymatch goal with
| |- (?l = _) =>
  idtac l;
  match l with
  | (_ ;; _)%E => idtac "yay1"
  | (Lam BAnon _ _)%E => idtac "yay2"
  | (App (Lam BAnon _) _) => idtac "yay2.5"
  | (App (Rec BAnon BAnon _) _)%E => idtac "yay3"
  | (Rec BAnon BAnon (Load _) (Load _))%E => idtac "yay4"
  | (Rec BAnon BAnon (Load (LitV _)) (Load (LitV _)))%E => idtac "yay5"
  | (Rec BAnon BAnon (Load (LitV 1%Z)) (Load (LitV 0%Z)))%E => idtac "yay5"
  | (Rec BAnon BAnon (Load (LitV 1%Z)) (Load (LitV 0%Z))) => idtac "yay6"
  end
end.
*)

(* Some Ltac2 function for matching on patterns and thunked open_constrs.
We did not use them in the end *)
Ltac2 pattern_has_match (p : Pattern.t) (c : constr) : bool :=
  match Control.case (fun _ => Pattern.matches p c) with
  | Val _ => true
  | Err Match_failure => false
  | Err _ => Control.throw Assertion_failure
  end.

Ltac2 thunked_constr_has_match (p : unit -> constr) (c : constr) : bool :=
  plus_once (fun _ =>
    ltac1:(c1 c2 |- eunify c1 c2) (Ltac1.of_constr (p ())) (Ltac1.of_constr c);
    true
  ) (fun _ =>
    false
  ).