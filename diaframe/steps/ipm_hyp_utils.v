From diaframe Require Import utils_ltac2.
From iris.proofmode Require Import proofmode environments string_ident.


(* Ltac2 representation of IPM idents *)
Ltac2 Type ipm_ident := [
  | NamedIdent (string)
  | AnonIdent (int)
].


(* Now some utility functions for constructing these *)
Ltac2 get_nth_coq_byte (i : int) : constr :=
  let the_inst : instance := 
  (* I dont know another way to get something of type instance! *)
    let k := Constr.Unsafe.kind 'Byte.x00 in
    match k with
    | Constr.Unsafe.Constructor const inst =>
      inst
    | _ => Control.throw Not_found
    end in
  let k := Constr.Unsafe.kind 'Byte.byte in
  match k with
  | Constr.Unsafe.Ind ind _ => 
    let first_cons := Constr.Unsafe.constructor ind i in
    Constr.Unsafe.make (Constr.Unsafe.Constructor first_cons the_inst)
  | _ => Control.throw Not_found 
  end.

Ltac2 char_to_coq_byte (c : char) : constr :=
  get_nth_coq_byte (Char.to_int c).

Ltac2 rec list_to_coq_list (cs : constr list) : constr :=
  match cs with
  | [] => '([]) 
  | c :: cs' =>
    let tail := list_to_coq_list cs' in
    '($c :: $tail)
  end.

Ltac2 rec coq_list_to_list (cs : constr) : constr list :=
  lazy_match! cs with
  | [] => []
  | ?c :: ?cs' => c :: coq_list_to_list cs'
  end.

Ltac2 string_to_coq_string (s : string) : constr :=
  let coq_byte_list : constr list 
    := List.init (String.length s) (fun i => char_to_coq_byte (String.get s i)) in
  let coq_bytes : constr := list_to_coq_list coq_byte_list in
  let raw_result : constr := '(String.string_of_list_byte $coq_bytes) in
  let result := Std.eval_cbv cbv_flags raw_result in
  result.

(* xH = 1
   xO p = 2*p
   xI p = 2*p + 1
*)
Ltac2 rec int_to_coq_positive (i : int) : constr :=
  Control.assert_valid_argument "int_to_coq_positive" (Int.gt i 0);
  if Int.equal i 1 then 'xH else (
  (* i ≥ 2 *)
  let p : int := Int.asr i 1 in 
  let p_constr : constr := int_to_coq_positive p in
  if Int.equal (Int.mod i 2) 0 then
    (* i = 2 * p *)
    '(xO $p_constr)
  else
    (* i = 2 * p + 1 *)
    '(xI $p_constr)).

Ltac2 rec coq_positive_to_int (p : constr) : int :=
  lazy_match! p with
  | xH => 1
  | xO ?p' => Int.lsl (coq_positive_to_int p') 1 
  | xI ?p' => Int.add (Int.lsl (coq_positive_to_int p') 1) 1
  end.

(* Putting this together *)
Ltac2 ipm_ident_to_constr (ipm_i : ipm_ident) : constr :=
  match ipm_i with
  | NamedIdent s => let coqs := string_to_coq_string s in '(INamed $coqs)
  | AnonIdent i => let coqp := int_to_coq_positive i in '(IAnon $coqp)
  end.

Ltac2 ipm_ident_of_constr (c : constr) : ipm_ident :=
  lazy_match! c with
  | INamed ?j => (NamedIdent (StringToIdent.coq_string_to_string j))
  | IAnon ?p => (AnonIdent (coq_positive_to_int p))
  end.



Ltac2 rec read_ipm_hyp_names_from_env (env : constr) : (ipm_ident * constr) list :=
  lazy_match! env with
  | Enil => []
  | Esnoc ?env' ?coqi ?prop => (ipm_ident_of_constr coqi, prop) :: (read_ipm_hyp_names_from_env env')
  end.

(* We reverse the result since Env are snoc-lists *)
Ltac2 read_ipm_hyp_names (delta : constr) : (ipm_ident * constr) list :=
  lazy_match! delta with
  | Envs ?gamma_i ?gamma_s _ =>
    List.rev (List.append (read_ipm_hyp_names_from_env gamma_s) (read_ipm_hyp_names_from_env gamma_i))
  end.

Ltac2 get_ipm_hyp_names (_ : unit) : (ipm_ident * constr) list :=
  lazy_match! goal with
  | [ |- envs_entails ?delta _ ] =>
    read_ipm_hyp_names delta
  | [ |- _ ] => []
  end.

Ltac2 string_equal (left : string) (right : string) : bool :=
  let llength := (String.length left) in
  if Int.equal llength (String.length right) then
    let rec cmp (i : int) : bool :=
      if Int.ge i llength then true else
      let cl := Char.to_int (String.get left i) in
      let cr := Char.to_int (String.get right i) in
      if Int.equal cl cr then cmp (Int.add i 1) else false
    in cmp 0
  else false.

Ltac2 ipm_ident_equal (li : ipm_ident) (ri : ipm_ident) : bool :=
  match li with
  | NamedIdent sl =>
    match ri with
    | NamedIdent sr =>
      string_equal sl sr
    | _ => false
    end
  | AnonIdent pl =>
    match ri with
    | AnonIdent pr =>
      Int.equal pl pr
    | _ => false
    end
  end.

