From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import env_utils util_classes solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import pure_solver automation_state ipm_hyp_utils ltac2_store reductions.
From iris.bi Require Import bi telescopes.


(* Now comes some infrastructure to revert hypotheses when a variable occurring in that hypothesis
   has been used for a rewrite *)
Fixpoint revert_idents {PROP : bi} (js : list ident) Δ G : option (envs PROP * PROP) :=
  match js with
  | [] => Some (Δ, G)
  | j :: js =>
    (* 
    '(Δ', G') ← revert_idents js Δ G;
    '(p, H) ← envs_lookup j Δ';
    Some (envs_delete true j p Δ', IntroduceHyp H G')
    *)
    pm_option_bind (λ '(Δ', G'),
      pm_option_bind (λ '(p, H),
        Some (envs_delete true j p Δ', IntroduceHyp H G')
      ) (envs_lookup j Δ')
    ) (revert_idents js Δ G)
  end.

Lemma revert_idents_sound {PROP : bi} (js : list ident) Δ (G : PROP) :
  match revert_idents js Δ G with
  | Some (Δ', G') => envs_entails Δ' G'
  | _ => False
  end →
  envs_entails Δ G.
Proof.
  rewrite envs_entails_unseal.
  revert Δ G.
  induction js as [|j js] => //.
  move => Δ G. cbn.
  destruct (revert_idents _ _ _) as [[Δ' G']|] eqn:HΔG => //=.
  destruct (envs_lookup _ _) as [[p P]|] eqn:HjΔ => //=.
  move => HΔP.
  apply IHjs. rewrite HΔG.
  rewrite envs_lookup_sound //.
  apply bi.wand_elim_r'.
  rewrite HΔP. unseal_diaframe.
  by rewrite bi.intuitionistically_if_elim.
Qed.


(* We declare a reduction for reducing revert_idents:
  we encountered a case where [simpl] diverged for unclear reasons *)
Declare Reduction revert_idents_eval := cbv [
  (* base *)
  base.beq base.ident_beq base.positive_beq base.string_beq
  base.ascii_beq base.pm_option_bind 
  (* env stuff *)
  envs_delete env_delete
  envs_lookup env_lookup eqb
  (* revert_idents *)
  revert_idents
].
Ltac revert_idents_eval t :=
  eval revert_idents_eval in t.
Ltac revert_idents_simpl :=
  (* Use [change_no_check] instead of [change] to avoid performing the
  conversion check twice. *)
  match goal with |- ?u => let v := revert_idents_eval u in change_no_check v end.

(* some ltacs to collect hypotheses which should be reintroduced *)
Ltac collect_env_occs x Γi acci :=
  let rec mthd Γ acc :=
    lazymatch Γ with
    | Enil => acc
    | Esnoc ?Γ' ?i ?H =>
      lazymatch H with
      | context [x] => 
        let result := mthd Γ' acc in
        constr:(i::result)
      | _ => mthd Γ' acc
      end
    end
  in mthd Γi acci.

Ltac collect_envs_occs x Δ :=
  let init := constr:(@nil ident) in
  let Γs := eval cbn in (env_spatial Δ) in
  let result_s := collect_env_occs x Γs init in
  let Γp := eval cbn in (env_intuitionistic Δ) in
  collect_env_occs x Γp result_s.

Ltac2 my_subst2 store x :=
  if Constr.is_var x then
    lazy_match! goal with
    | [|- envs_entails ?Δ _] =>
      ltac1:(Δ x1 cont |-
        let result := collect_envs_occs x1 Δ in
        refine (revert_idents_sound result Δ _ _);
        revert_idents_simpl;
        subst x1; try (progress simpl); (* <- simpl here *)
        cont result
      ) (Ltac1.of_constr Δ) (Ltac1.of_constr x) (Ltac1.lambda (fun result =>
        let js_coq := Option.get (Ltac1.to_constr result) in
        let js := of_coq_list js_coq in
        let idents := List.map ipm_ident_of_constr js in
        List.iter (fun i => store.remove store i) idents; ltac1val:(idtac)
      ))
    end
  else Control.zero Assertion_failure.

Lemma do_simplify_hyp (φin φout : Prop) (Hφ : φin) (G : Prop) : SimplifyPureHyp φin φout → (φout → G) → G.
Proof. rewrite /SimplifyPureHyp. eauto. Qed.

(* adapted from stdpp's simplify_eq *)
Ltac2 simplify_hyp_eq_step (h : ident) (cont : ident -> unit) (subster : constr -> unit) : unit := 
  let hc := Control.hyp h in
  let h1 := Ltac1.of_constr hc in
  let disprove () :=
    ltac1:(φ |- let H := fresh "H" in assert (¬φ) as H by trySolvePure; by contradiction) (Ltac1.of_constr (Constr.type hc))
  in
  match! (Constr.type hc) with
  | _ ∧ _ => ltac1:(H |- case H; try clear H) h1;
    let h0 := Fresh.in_goal @H in 
    let h1 := Fresh.in_goal @G in 
    Std.intro (Some h0) None;
    Std.intro (Some h1) None;
    cont h0;
    (* cont h0 may vacuously solve the goal *)
    ifcatch fail0 (fun _ => ()) (fun _ => cont h1)
  | ∃ _, _ => ltac1:(H |- case H; try clear H; intros ?) h1; 
      let h := Fresh.in_goal @H in
      Std.intro (Some h) None;
      cont h
  (* ∧ and ∃ cannot be given directly to [simplify_hyp_eq], but they can appear through
    do_simplify_hyp (i.e., with the [SimplifyPureHyp] typeclass *)
  | _ ≠ _ => ltac1:(H |- by case H; try clear H) h1
  | _ = _ → False => ltac1:(H |- by case H; try clear H) h1
  | ?x = _ => first [ disprove () | subster x ]
  | _ = ?x => first [ disprove () | subster x ]
  | _ = _ => ltac1:(H |- discriminate H) h1
  | _ ≡ _ => ltac1:(H |- apply leibniz_equiv in H) h1; cont h
  | ?f _ = ?f _ => ltac1:(H f |- apply (inj f) in H) h1 (Ltac1.of_constr f); cont h
  | ?f _ _ = ?f _ _ => 
    let h0 := Fresh.in_goal @H in 
    let h1 := Fresh.in_goal @G in

(*      apply (inj2 $f) in h as [h0 h1] becomes the horrendous *)
    Std.apply true false [fun _ => '(inj2 _), Std.NoBindings] (Some (h, 
        Some (Std.IntroAction (Std.IntroOrAndPattern
    (Std.IntroAndPattern [Std.IntroNaming (Std.IntroIdentifier h0); Std.IntroNaming (Std.IntroIdentifier h1)])))));

    cont h0; 
    (* cont h0 may vacuously solve the goal *)
    ifcatch fail0 (fun _ => ()) (fun _ => cont h1)
  | _ => 
    let (get_is, cleanup) := find_fresh_idents_between () in
    ltac1:(H |- progress injection H as) h1; (* spawns an unknown amount of new hypotheses *)
    let idents := get_is () in
    cleanup ();
    List.iter (fun i => ifcatch fail0 (fun _ => ()) (fun _ => cont i)) idents
  | ?x = ?x => Std.clear [h]
  | @existT ?a _ _ _ = existT _ _ =>
    ltac1:(H A |- apply (Eqdep_dec.inj_pair2_eq_dec _ (decide_rel (=@{A}))) in H) h1 (Ltac1.of_constr a);
    cont h
  | ?thetype => 
    plus_once (fun _ => 
      ltac1:(H |- notypeclasses refine (do_simplify_hyp _ _ H _ _ _); first tc_solve; try clear H) h1; 
      let h2 := Fresh.in_goal @H in
      Std.intro (Some h2) None;
      cont h2
    ) (fun _ => 
      (* reached an atom: try to disprove it *)
      try (disprove ())
    )
  end.

Ltac2 rec simplify_hyp_eq store (h : ident) : unit := 
  simplify_hyp_eq_step h (simplify_hyp_eq store) (my_subst2 store).


Ltac2 introduce_one_prop store : unit :=
  let h := Fresh.in_goal @H in
  Std.intro (Some h) None;
  simplify_hyp_eq store h. 
  (* NOTE: this can kill the goal, when introducing contradictory hypotheses *)

