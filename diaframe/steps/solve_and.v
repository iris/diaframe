From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import solve_defs utils_ltac2.
From diaframe.steps Require Import automation_state.
From iris.bi Require Import bi telescopes.

Import bi.

Section solve_and.
  Context {PROP : bi}.
  Implicit Types P : PROP.

  Lemma solve_and_and P1 P2 : P1 ∧ P2 ⊢ SolveAnd P1 P2.
  Proof. by unseal_diaframe. Qed.

  Lemma tac_solve_and Δ P1 P2 : 
    envs_entails Δ P1 →
    envs_entails Δ P2 →
    envs_entails Δ (SolveAnd P1 P2).
  Proof.
    unseal_diaframe; rewrite envs_entails_unseal => <- <-.
    by apply and_intro.
  Qed.
End solve_and.


Ltac2 mutable solveAndStep 
    (state : automation_state.t)
    (gid : goal_id) 
     : goal_id list :=
  apply tac_solve_and;
  let (g1, g2) := automation_state.split_goal state gid in
  [g1; g2].