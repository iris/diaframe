From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import env_utils util_classes solve_defs utils_ltac2.
From diaframe.steps Require Import pure_solver solve_sep transform introduce_hyp introduce_var solve_one 
      solve_and solve_sep_foc solve_one_foc disj_chooser_tacs pure_solver_utils automation_state ltac2_store.
From iris.bi Require Import bi telescopes.

(* Apart from iStep, Diaframe also defines a (more fine-grained (😉) ) solveStep tactic,
   which executes precisely one instruction of Diaframe's proof search strategy.

   If a hint you expect to be applied is not applied or found, this file comes with a 'solveStep with H' tactic.
   The next goals will be:
   - prove a hint from hypothesis H to the current focused atom
   - continuation goal after applying this hint.
*)


Ltac introStep := (* used for iDecompose tactic *)
  ltac2:(
    lazy_match! goal with
    | [|- envs_entails ?Δ ?r] =>
      lazy_match! r with
      | (IntroduceVars _) => ltac1:(introduceVarStep)
      | (IntroduceHyp ?p ?g) =>
          let (state, gid) := automation_state.new () in
          let _ := introduceHypStep state gid Δ p g
          in ()
      end
    end
  ).


Ltac2 solveStepWithOptTacs 
    (chooser_tac : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit)
    (pure_solve_result_handler : pure_solver_result -> bool)
    (state : automation_state.t)
    (gid : goal_id)
      : goal_id list :=
  lazy_match! goal with
  | [|- envs_entails ?Δ ?r] =>
    lazy_match! r with
    | (@SolveSep _ ?tt ?m ?p ?q) =>       solveSepStep pure_solve_result_handler state gid Δ tt m p q
    | (Transform _) =>          transformStep state gid; [gid]
    | (@IntroduceVars _ ?tt ?q) =>      introduceVarStep state gid Δ tt q
    | (IntroduceHyp ?p ?g) =>     introduceHypStep state gid Δ p g
    | (@SolveOne _ ?tt ?m ?p) =>         solveOneStep pure_solve_result_handler state gid Δ tt m p
    | (SolveAnd _ _) =>         solveAndStep state gid
    | (SolveSepFoc _ _ _ _) =>  solveSepFocStep chooser_tac state gid
    | (SolveOneFoc _ _ _) =>    solveOneFocStep chooser_tac state gid
    end
  end.

Ltac2 rec solveStepsWithOptTacs 
    (chooser_tac : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit)
    (pure_solve_result_handler : pure_solver_result -> bool)
    (state : automation_state.t)
    (gid : goal_id)
      : goal_id list :=
  (* assumes a single goal goal is focused! *)
  let cont : unit -> goal_id list :=
    plus_once (fun _ =>
      let gids' := solveStepWithOptTacs chooser_tac pure_solve_result_handler state gid in
      fun _ => dispatch_and_join gids' (solveStepsWithOptTacs chooser_tac pure_solve_result_handler state)
    ) (fun _ =>
      (fun _ => [gid])
    )
  in
  cont ().


Ltac solveStep_with mi post_tac := 
  lazymatch goal with
  | |- environments.envs_entails _ (SolveSep _ _ _) =>
    notypeclasses refine (tac_solve_sep_ext_biabd_disj _ mi _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _);
        [tc_solve | post_tac () | idtac..]
  | |- environments.envs_entails _ (SolveOne _ _) =>
    notypeclasses refine (solve_one.tac_solve_one_ext_abd_input _ mi _ _ _ _ _ _ _); 
      [post_tac () | idtac..]
  | |- environments.envs_entails _ (SolveSepFoc _ _ _ _) =>
    notypeclasses refine (tac_solve_sep_foc_biabd_disj _ mi _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _);
      [tc_solve | post_tac () | idtac..]
  | |- environments.envs_entails _ (SolveOneFoc _ _ _) => 
    notypeclasses refine (tac_solve_one_foc_abduct_foc_disj_ex _ mi _ _ _ _ _ _ _ _); 
      [post_tac () | idtac ]
  end.

Tactic Notation "solveStep" "with" constr(name) :=
  let mi := constr:(Some $ INamed name) in
  let post_tac _ := eapply fic_ext_lookup_Some; first reflexivity in
  solveStep_with mi post_tac.

Tactic Notation "solveStep" "with" "empty_hyp_first" :=
  let mi := constr:(@None ident) in
  let post_tac _ := (eapply fic_ext_lookup_empty_first) in
  solveStep_with mi post_tac.

Tactic Notation "solveStep" "with" "ε₀" := solveStep with empty_hyp_first.

Tactic Notation "solveStep" "with" "empty_hyp_last" :=
  let mi := constr:(@None ident) in
  let post_tac _ := (eapply fic_ext_lookup_empty_last) in
  solveStep_with mi post_tac.

Tactic Notation "solveStep" "with" "ε₁" := solveStep with empty_hyp_last.

Ltac solveStep := 
  ltac2:(
    let (state, gid) := automation_state.new () in
    let _ := solveStepWithOptTacs chooser_always_left handle__shelve_unsolved_noevars state gid in
    ()
  ).

Ltac solveSteps := 
  ltac2:(
    let (state, gid) := automation_state.new () in
    let _ := solveStepsWithOptTacs chooser_always_left handle__shelve_unsolved_noevars state gid in
    ()
  ).

