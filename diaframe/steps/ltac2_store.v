From diaframe Require Import utils_ltac2.

(** FIXME: replace with native Ltac2.FMap datatype *)

Module store.
  Ltac2 Type rec ('k, 'v) store_int := { 
    key : 'k;
    mutable value : 'v;
    mutable next : ('k, 'v) store_int option
  }.

  Ltac2 Type ('k, 'v) t := {
    mutable the_store : ('k, 'v) store_int option;
    equality : 'k -> 'k -> bool
  }.

  Ltac2 new (eq : 'k -> 'k -> bool) :
    ('k, 'v) t := { the_store := None; equality := eq }.
  Ltac2 clear (s : ('k, 'v) t) : unit := s.(the_store) := None.

  Ltac2 rec copy_int (s : ('k, 'v) store_int) : ('k, 'v) store_int :=
    let next_val :=
      match s.(next) with
      | None => None
      | Some s' => Some (copy_int s')
      end in
    { key := s.(key); value := s.(value); next := next_val }.
  Ltac2 copy (s : ('k, 'v) t) : ('k, 'v) t :=
    let store_val :=
      match s.(the_store) with
      | None => None
      | Some s' => Some (copy_int s')
      end in
    { the_store := store_val; equality := s.(equality) }.


  Ltac2 rec get_int (s : ('k, 'v) store_int) (eq : 'k -> 'k -> bool) (key : 'k) : 'v option :=
    if eq key (s.(key)) then
      Some (s.(value))
    else
      match s.(next) with
      | Some s' =>
        get_int s' eq key
      | None =>
        None
      end
    .

  Ltac2 get (s : ('k, 'v) t) (key : 'k) : 'v option :=
    match (s.(the_store)) with
    | Some i => get_int i (s.(equality)) key
    | None => None
    end.


  Ltac2 rec set_int (s : ('k, 'v) store_int) (eq : 'k -> 'k -> bool) (key : 'k) (val : 'v) : unit :=
    if eq key (s.(key)) then
      s.(value) := val
    else
      match s.(next) with
      | Some s' => set_int s' eq key val
      | None =>
        s.(next) := Some { key := key; value := val; next := None}
      end.

  Ltac2 set (s : ('k, 'v) t) (key : 'k) (val : 'v) : unit :=
    match s.(the_store) with
    | Some i => set_int i (s.(equality)) key val
    | None =>
      s.(the_store) := Some { key := key; value := val; next := None }
    end.


  Ltac2 rec remove_int (s : ('k, 'v) store_int) (eq : 'k -> 'k -> bool) (key : 'k) : bool :=
    (* return value indicates whether to replace this node with its child *)
    if eq key (s.(key)) then
      true
    else
      match s.(next) with
      | None => false
      | Some s' =>
        if remove_int s' eq key then 
          s.(next) := s'.(next)
        else ();
        false
      end.

  Ltac2 remove (s : ('k, 'v) t) (key : 'k) : unit :=
    match s.(the_store) with
    | None => ()
    | Some s' =>
      if remove_int s' (s.(equality)) key then
        s.(the_store) := s'.(next)
      else
        ()
    end.


  Ltac2 rec get_or_set_int (s : ('k, 'v) store_int) (eq : 'k -> 'k -> bool) (key : 'k) (gen : 'k -> 'v) : 'v :=
    if eq key (s.(key)) then
      s.(value)
    else
      match s.(next) with
      | Some s' => get_or_set_int s' eq key gen
      | None =>
        let result := gen key in
        s.(next) := Some { key := key; value := result; next := None};
        result (* changing result with () does not break type-checking..?*)
      end.

  Ltac2 get_or_set (s : ('k, 'v) t) (key : 'k) (gen : 'k -> 'v) : 'v :=
    match s.(the_store) with
    | Some s' => get_or_set_int s' (s.(equality)) key gen
    | None =>
      let result := gen key in
      s.(the_store) := Some {key := key; value := result; next := None };
      result
    end.


  Ltac2 rec to_list_int (s : ('k, 'v) store_int) : ('k * 'v) list :=
    let tail :=
      match s.(next) with
      | None => []
      | Some s' => to_list_int s'
      end 
    in
    (s.(key), s.(value)) :: tail.

  Ltac2 to_list (s : ('k, 'v) t) : ('k * 'v) list :=
    match s.(the_store) with
    | Some s' => to_list_int s'
    | None => []
    end.


  Ltac2 rec size_int (s : ('k, 'v) store_int) : int :=
    match s.(next) with
    | None => 1
    | Some s' => Int.add (size_int s') 1
    end.

  Ltac2 size (s : ('k, 'v) t) : int :=
    match s.(the_store) with
    | None => 0
    | Some s' => size_int s'
    end.
End store.

(*
From diaframe.steps Require Import ipm_hyp_utils.

Goal True.
ltac2:(
  let store : (ipm_ident, int) store.t := store.new ipm_ident_equal in
  store.set store (AnonIdent 3) 5;
  store.set store (AnonIdent 4) 6;
  store.set store (AnonIdent 5) 7;
  store.set store (AnonIdent 6) 8;
  printf "result: %i" (Option.get (store.get store (AnonIdent 5)));
  store.set store (AnonIdent 5) 0;
  printf "result: %i" (Option.get (store.get store (AnonIdent 5)));
  printf "result: %i" (store.get_or_set store (AnonIdent 4) (fun _ => printf "genning!"; -8));
  store.remove store (AnonIdent 4);
  printf "result: %i" (store.get_or_set store (AnonIdent 4) (fun _ => printf "genning!"; -8));
  printf "result: %i" (store.get_or_set store (AnonIdent 4) (fun _ => printf "genning!"; -8))
).
Abort.
*)

