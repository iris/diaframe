From diaframe Require Import utils_ltac2.


Ltac2 Type diaframe_option := [ .. ].

(* The string argument is just there to throw a somewhat readable error when
used directly (and thus inappropriately). *)
Ltac2 Type exn ::= [ Diaframe_option_exn (string, diaframe_option) ].

Ltac2 make_option_exn (o : diaframe_option) : exn :=
  Diaframe_option_exn "

This tactic sets an option for Diaframe's tactics.
It cannot be used directly in proof scripts.

." o.


Ltac2 throw_option (o : diaframe_option) : unit :=
  Control.zero (make_option_exn o).