From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import env_utils util_classes solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import disj_chooser_tacs pure_solver_utils automation_state ltac2_store.
From diaframe.hint_search Require Import biabd_disj_from_path.
From iris.bi Require Import bi telescopes.

Import bi.

Section solve_sep_foc_rules.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma solve_sep_foc_fromsep {TT : tele} M L L1 L2 G D TT1 TT2 L1' h :
    (TC∀.. (tt : TT), FromSepCareful (tele_app L tt) (tele_app L1 tt) (tele_app L2 tt)) →
    ExtractDep L1 TT1 L1' TT2 h →
    ModalityMono M →
    SolveSepFoc (TT := TT1) M L1'   (* Note that one might need to redivide modalities (or existentials!) over these goals.
                                       This used to be problematic, but should work now *)
      (tele_bind (λ tt1, SolveSep id (TT := tele_app TT2 tt1) (tele_bind (λ tt2, tele_app L2 (tele_app (tele_appd_dep h tt1) tt2)))
                                                 (tele_bind (λ tt2, tele_app G (tele_app (tele_appd_dep h tt1) tt2))))) 
    D ⊢ SolveSepFoc (TT := TT) M L G D.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromSepCareful /ExtractDep !tforall_forall => HL HL1 HM.
    apply HM, or_mono => {HM} //=.
    apply bi_texist_elim => tt1.
    rewrite bi_texist_exist !tele_app_bind.
    apply wand_elim_r', bi_texist_elim => tt2.
    apply wand_intro_l.
    rewrite -(exist_intro (tele_app (tele_appd_dep h tt1) tt2)) !tele_app_bind assoc.
    apply sep_mono_l.
    rewrite -HL //.
    apply sep_mono_l.
    revert HL1 => /(dep_eval tt1) /(dep_eval_tele tt2) -> //.
  Qed.

  Lemma solve_sep_foc_fromdisj {TT : tele} M D L L' R G G' b TT1 L'' TT2 h G'' TTr1 R' TTr2 h' G''' :
    (TC∀.. (tt : TT), FromOrCareful (tele_app D tt) (tele_app L tt) (tele_app R tt)) →
    (TC∀.. (tt : TT), NormalizeProp (tele_app L tt) (tele_app L' tt) b ) →
      (* this tries to make some branches false before investigating deeper, improves iStep behaviour *)
    ExtractDep L' TT1 L'' TT2 h →
    (TC∀.. (tt1 : TT1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h tt1)) (tele_app G'' tt1)) →
    ModalityMono M →
    ExtractDep R TTr1 R' TTr2 h' →
    (TC∀.. (tt1 : TTr1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h' tt1)) (tele_app G''' tt1)) →
    SolveSepFoc (TT := TT1) M L'' G'' ((∃.. (tt : TTr1), tele_app R' tt ∗ tele_app G''' tt) ∨ G') ⊢
    SolveSepFoc (TT := TT) M D G G'.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromOrCareful /NormalizeProp !tforall_forall => H HL HL' HG'' HM HR HG'''.
    apply HM => {HM} /=.
    apply or_elim, or_mono => //; [rewrite -or_intro_l | ].
    - apply bi_texist_elim => tt1.
      rewrite HG''. apply bi.wand_elim_r', bi_texist_elim => tt2.
      apply bi.wand_intro_l. 
      revert HL' => /(dep_eval_tele tt1) /(dep_eval_tele tt2) ->.
      by rewrite -HL bi_texist_exist -bi.exist_intro -H -or_intro_l tele_fun_compose_eq'.
    - apply bi_texist_elim => tt1.
      rewrite HG'''. apply bi.wand_elim_r', bi_texist_elim => tt2.
      apply bi.wand_intro_l. 
      revert HR => /(dep_eval_tele tt1) /(dep_eval_tele tt2) ->.
      by rewrite bi_texist_exist -bi.exist_intro -H -or_intro_r tele_fun_compose_eq'.
  Qed.

  Lemma solve_sep_foc_fromexist {TT1 : tele} {TT2} M D G G' P :
    (∀.. (tt : TT1), FromTExist (tele_app D tt) (tele_app TT2 tt) (tele_appd_dep P tt)) →
    ModalityMono M →
    SolveSepFoc (TT := TeleConcatType TT1 TT2) M (tele_curry P) (tele_curry $ tele_dep_bind (λ tt1, tele_bind (λ tt2, tele_app G tt1)))
    G' ⊢ SolveSepFoc (TT := TT1) M D G G'.
  Proof.
    unseal_diaframe => /tforall_forall HDP HM /=.
    apply HM, or_mono; last done. apply bi_texist_elim => tt.
    rewrite (tele_arg_concat_inv_eq tt).
    destruct (tele_arg_concat_inv_strong_hd tt) as [tt1 tt2].
    rewrite bi_texist_exist -(exist_intro tt1).
    rewrite -HDP !bi_texist_exist.
    rewrite -(exist_intro tt2).
    rewrite tele_uncurry_curry_compose.
    rewrite -tele_curry_uncurry_relation.
    apply sep_mono_r.
    rewrite tele_curry_uncurry_relation.
    rewrite -tele_uncurry_curry_compose.
    rewrite tele_dep_appd_bind.
    by rewrite tele_app_bind.
  Qed.

  Lemma solve_sep_foc_biabd_disj {TTl TTr} Q S R T D D'' M p P M1 M2 M' Mr R' DMS :
    SplitLeftModality3 M M' Mr →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P Q M2 S T D'' →
    SplitModality3 M' M1 M2 →
    AsSolveGoalInternal (M2 $ Mr D) DMS →
    (TC∀.. (ttr : TTr), AbsorbModal (tele_app R ttr) Mr (tele_app R' ttr)) →
    □?p P ∗ SolveSepFoc (TT := TTl) M1 S 
      (tele_bind $ λ ttl,
        match tele_app D'' ttl with
        | Some2 D' =>
          SolveAnd (
            IntroduceVars (TT := TTr) $ tele_bind $ λ ttr, 
              IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
          ) $ IntroduceHyp D' $ SolveSepFoc (TT := TTr) Mr Q R D
        | None2 =>
            IntroduceVars (TT := TTr) $ tele_bind $ λ ttr, 
              IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
        end )
      (if p then DMS else P -∗ DMS)
    ⊢ SolveSepFoc (TT := TTr) M Q R D.
  Proof.
    unseal_diaframe; rewrite /BiAbdDisj /AsSolveGoalInternal /TCTForall /AbsorbModal => 
      [[HMM' HM' HMr]] /tforall_forall HPQ [HM'' HM1 HM2] HDMS /tforall_forall HRR'.
    rewrite -HMM' -HM'' modality_strong_frame_r.
    apply HM1 => {HM1}.
    apply wand_elim_l', or_elim; last first.
    { destruct p; rewrite HDMS /=; last first. 
      - apply wand_mono => //. apply HM2, HMr, or_intro_r.
      - rewrite /bi_intuitionistically /bi_affinely and_elim_l left_id.
        apply HM2, HMr, or_intro_r. }
    apply bi_texist_elim => ttl.
    rewrite !tele_app_bind.
    apply bi.wand_intro_l. 
    rewrite assoc HPQ modality_strong_frame_l.
    apply HM2, bi.wand_elim_l'.
    destruct (tele_app D'' ttl) as [D' | ]; last first.
    { rewrite right_id.
      apply bi_texist_elim => ttr.
      apply bi.wand_intro_r.
      rewrite (bi_tforall_elim ttr) tele_app_bind -assoc bi.wand_elim_r HRR'
        modality_strong_frame_r.
      apply HMr.
      rewrite -or_intro_l bi_texist_exist -(exist_intro ttr) comm //. }
    apply or_elim; last first.
    { rewrite and_elim_r. 
      apply bi.wand_intro_l. rewrite bi.wand_elim_l //. }
    apply bi_texist_elim => ttr.
    apply bi.wand_intro_r.
    rewrite bi.and_elim_l (bi_tforall_elim ttr) tele_app_bind -assoc bi.wand_elim_r HRR'
      modality_strong_frame_r.
    apply HMr.
    rewrite -or_intro_l bi_texist_exist -(exist_intro ttr) comm //.
  Qed.

  Lemma solve_sep_foc_unfocus {TT} M L L' D G :
    AsSolveGoalInternal (M D) G →
    ModalityMono M →
    G ⊢ SolveSepFoc (TT := TT) M L L' D.
  Proof.
    unseal_diaframe; rewrite /AsSolveGoalInternal => -> HM.
    apply HM => {HM} /=.
    apply or_intro_r.
  Qed.

End solve_sep_foc_rules.


Section solve_sep_foc_tacs.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma tac_solve_sep_foc_from_sep {TT : tele} Δ M L L1 L2 G D TT1 TT2 L1' h :
    (TC∀.. (tt : TT), FromSepCareful (tele_app L tt) (tele_app L1 tt) (tele_app L2 tt)) →
    ExtractDep L1 TT1 L1' TT2 h →
    ModalityMono M →
    envs_entails Δ (SolveSepFoc (TT := TT1) M L1' 
      (tele_bind (λ tt1, SolveSep id (TT := tele_app TT2 tt1) (tele_bind (λ tt2, tele_app L2 (tele_app (tele_appd_dep h tt1) tt2)))
                                                 (tele_bind (λ tt2, tele_app G (tele_app (tele_appd_dep h tt1) tt2))))) 
       D)%I → 
    envs_entails Δ $ SolveSepFoc (TT := TT) M L G D.
  Proof.
    rewrite envs_entails_unseal => H HL HM.
    rewrite solve_sep_foc_fromsep //.
  Qed.

  Lemma tac_solve_sep_foc_pure (Δ : envs PROP) {TT : tele} b M P L D φ G :
    (∀.. tt : TT, FromPure b (tele_app P tt) (tele_app φ tt)) →
    ModalityMono M →
    (TC∀.. tt : TT, AsSolveGoalInternal (M $ (tele_app L tt ∨ D)%I) (tele_app G tt)) →
    (∃.. tt, BehindModal M (tele_app φ tt) ∧ (envs_entails Δ $ tele_app G tt)) →
    envs_entails Δ $ SolveSepFoc (TT := TT) M P L D.
  Proof.
    rewrite envs_entails_unseal /TCTForall /BehindModal texist_exist => /tforall_forall HPφ HM /tforall_forall + [tt [Hφ ->]].
    unseal_diaframe => ->. apply HM. apply or_mono; last done.
    rewrite bi_texist_exist -(exist_intro tt).
    rewrite -{1}(left_id _ bi_sep (tele_app L tt)). apply sep_mono_l.
    rewrite -HPφ /= pure_True //=.
    destruct b => /=; eauto.
  Qed.

  Lemma tac_solve_sep_foc_fromdisj {TT : tele} Δ M D L L' R G G' b TT1 L'' TT2 h G'' TTr1 R' TTr2 h' G''' :
    (TC∀.. (tt : TT), FromOrCareful (tele_app D tt) (tele_app L tt) (tele_app R tt)) →
    (TC∀.. (tt : TT), NormalizeProp (tele_app L tt) (tele_app L' tt) b ) → 
    ExtractDep L' TT1 L'' TT2 h →
    (TC∀.. (tt1 : TT1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h tt1)) (tele_app G'' tt1)) →
    ModalityMono M →
    ExtractDep R TTr1 R' TTr2 h' →
    (TC∀.. (tt1 : TTr1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h' tt1)) (tele_app G''' tt1)) →
    envs_entails Δ $ SolveSepFoc (TT := TT1) M L'' G'' ((∃.. (tt : TTr1), tele_app R' tt ∗ tele_app G''' tt) ∨ G') →
    envs_entails Δ $ SolveSepFoc (TT := TT) M D G G'.
  Proof.
    rewrite envs_entails_unseal => HD HL HL' HG HM HR HG''' ->.
    by eapply solve_sep_foc_fromdisj.
  Qed.

  Lemma tac_solve_sep_foc_fromexist {TT1 : tele} {TT2} Δ M D G G' P :
    (TC∀.. (tt : TT1), FromTExist (tele_app D tt) (tele_app TT2 tt) (tele_appd_dep P tt)) →
    ModalityMono M →
    envs_entails Δ $ SolveSepFoc (TT := TeleConcatType TT1 TT2) M (tele_curry P) 
          (tele_curry $ tele_dep_bind (λ tt1, tele_bind (λ tt2, tele_app G tt1))) G' →
    envs_entails Δ $ SolveSepFoc (TT := TT1) M D G G'.
  Proof.
    rewrite envs_entails_unseal => HD HM ->.
    by apply solve_sep_foc_fromexist.
  Qed.

  Lemma tac_solve_sep_foc_biabd_disj {TTl TTr} Δ mi Q S R T D D'' M p P1 M1 M2 M' Mr R' DMS :
    SplitLeftModality3 M M' Mr →
    FindInExtendedContext Δ (λ p P, BiAbdDisjMod (TTl := TTl) (TTr := TTr) p P Q M' M1 M2 S T D'') mi p P1 →
    (TC∀.. (ttr : TTr), AbsorbModal (tele_app R ttr) Mr (tele_app R' ttr)) →
    AsSolveGoalInternal (M2 $ Mr D) DMS →
    envs_entails (envs_option_delete false mi p Δ) $ SolveSepFoc (TT := TTl) M1 S 
      (tele_bind $ λ ttl,
        match tele_app D'' ttl with
        | Some2 D' =>
          SolveAnd (
            IntroduceVars (TT := TTr) $ tele_bind $ λ ttr, 
              IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
          ) $ IntroduceHyp D' $ SolveSepFoc (TT := TTr) Mr Q R D
        | None2 =>
            IntroduceVars (TT := TTr) $ tele_bind $ λ ttr, 
              IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
        end )
      (if p then DMS else match mi with None => DMS | Some _ => P1 -∗ DMS end)%I →
    envs_entails Δ $ SolveSepFoc (TT := TTr) M Q R D.
  Proof.
    rewrite envs_entails_unseal => HMM' HΔiP HR HD HΔ'.
    rewrite (findinextcontext_spec false) // HΔ'.
    eassert (_) as HΔiP' by exact HΔiP.
    apply ficext_satisfies in HΔiP as [HPST HMs].
    etrans; last first.
    eapply solve_sep_foc_biabd_disj; try done.
    apply sep_mono_r.
    apply solve_sep_foc_proper_ent; try done.
    - split. done. left. apply HMs.
    - destruct p; first done.
      destruct mi; first done.
      apply ficext_none_free in HΔiP' as <-.
      by rewrite left_id.
  Qed.

  Lemma tac_solve_sep_foc_unfocus {TT} Δ M L L' D G :
    AsSolveGoalInternal (M D) G →
    ModalityMono M →
    envs_entails Δ G → 
    envs_entails Δ $ SolveSepFoc (TT := TT) M L L' D.
  Proof.
    rewrite envs_entails_unseal => HG HM ->.
    by apply solve_sep_foc_unfocus.
  Qed.

End solve_sep_foc_tacs.


Ltac2 Type solve_sep_foc_step_type := [
  | SolveSepFocSep
  | SolveSepFocPure
  | SolveSepFocOr
  | SolveSepFocExist
  | SolveSepFocBiAbd
  | SolveSepFocNoHint
].


Ltac2 solveSepFocStep (chooser_tac : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit)
    (state : automation_state.t) (gid : goal_id) : goal_id list :=
  (* solveSepFocStep is used when the goal has shape ((L ∗ G) ∨ D), possibly behind existential quantifiers and a modality.
    We will refer to L as the left-most goal *)
  let cont := plus_once_list
    [ (* left-most goal has shape (∃ L) *)
      ltac1:(notypeclasses refine (tac_solve_sep_foc_fromexist _ _ _ _ _ _ _ _ _);
        [tc_solve.. | simpl]); fun _ => ()
    | (* left-most goal has shape (L1 ∗ L2) *)
      ltac1:(notypeclasses refine (tac_solve_sep_foc_from_sep _ _ _ _ _ _ _ _ _ _ _ _ _ _ _);
        [tc_solve.. | simpl]); fun _ => ()
    | (* left-most goal has shape (L1 ∨ L2) *)
      ltac1:(notypeclasses refine (tac_solve_sep_foc_fromdisj _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _);
        [tc_solve.. | cbn; simpl]); fun _ => ()
    | (* left-most goal has shape ⌜L⌝ (a pure proposition) *)
      ltac1:(notypeclasses refine (tac_solve_sep_foc_pure _ _ _ _ _ _ _ _ _ _ _ _)) >
        [tc_solve ().. | simpl ]; 
          progress (try_solve_pure_on
            (fun c => ltac1:(repeat_eexists); split > [ c () | ])
            handle__rollback_unsolved
          ); fun _ => ()
      (* solvePure _must_ fail when it does not solve the goal, to cause backtracking to other options *)
    | (* left-most goal is an atom: find a connection *)
      ltac1:(notypeclasses refine (tac_solve_sep_foc_biabd_disj _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _))>
        [tc_solve () | | | | ] > [ 
            find_biabd_disj_in_envs_on_goal (automation_state.get_rp_store_for state gid)
          | | | ] > [ tc_solve ().. | ];
          handle_prop_deletion_on_goal state gid;
          simpl; (fun _ => ())
    | (* left-most goal is an atom, and no connection was found.
        Ask the chooser how to continue: either try proving D, or stop *)
      fun _ => chooser_tac state gid PickEvent.NoHintSep (fun side =>
        match side with
        | PickSide.Left =>
            ltac1:(notypeclasses refine (tac_solve_sep_foc_unfocus _ _ _ _ _ _ _ _ _);
             [tc_solve.. | simpl])
        | _ =>
            fail (* do nothing *)
        end
      )
    ] in
  cont (); [gid].





