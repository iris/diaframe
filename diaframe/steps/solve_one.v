From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import pure_solver_utils automation_state reductions.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file contains 'case 4' of Diaframe's proof search strategy: making progress on atoms behind modalities.
   Lone atoms cause Diaframe to look for an abduction hint.
*)

Section solve_one_rules.

  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma solve_one_fromsep {TT : tele} M P P1 P2 {TT1} P1' {TT2} h M1 M2 P2a :
    (TC∀.. (tt : TT), FromSepCareful (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ExtractDep P1 TT1 P1' TT2 h →
    SplitLeftModality3 M M1 M2 →
    (TC∀.. (tt1 : TT1), AbsorbModal (∃.. tt2, tele_app P2 (tele_app (tele_appd_dep h tt1) tt2))%I
        M2 (tele_app P2a tt1)) → 
    SolveSep M1 P1' P2a
    ⊢ SolveOne M P.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromSepCareful /ExtractDep /AbsorbModal !tforall_forall => 
        HP HP1 [HM HM1 HM2] HP2a.
    rewrite -HM => {HM}.
    apply HM1 => {HM1}.
    apply bi_texist_elim => tt1.
    move: HP1 => /(dep_eval tt1) /tforall_forall HP1.
    rewrite HP2a modality_strong_frame_r.
    apply modality_strong_is_mono => {HM2}.
    apply wand_elim_l', bi_texist_elim => tt2.
    apply wand_intro_l.
    erewrite HP1, HP.
    by rewrite bi_texist_exist -(exist_intro _).
  Qed.

  Lemma solve_one_fromor {TT : tele} M Q L1 L2 :
    (TC∀.. (tt : TT), FromOrCareful (tele_app Q tt) (tele_app L1 tt) (tele_app L2 tt)) →
    (* TODO: extractdep here? *)
    ModalityMono M →
    SolveOneFoc (TT := TT) M L1 (∃.. (tt: TT), tele_app L2 tt) ⊢ SolveOne (TT := TT) M Q.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromOrCareful => /tforall_forall HQ HM.
    apply HM, or_elim; apply bi_texist_mono => tt; rewrite -HQ; eauto.
  Qed.

  Lemma solve_one_normalize {TT : tele} M Q Q' :
    (TC∀.. (tt : TT), NormalizedProp (tele_app Q tt) (tele_app Q' tt) true) →
    ModalityMono M →
    SolveOne M Q' ⊢ SolveOne M Q.
  Proof.
    unseal_diaframe; rewrite /TCTForall => /tforall_forall HQ HM.
    apply HM, bi_texist_mono => tt. by rewrite -HQ.
  Qed.

  Lemma solve_one_fromexist_nested {TT1 : tele} {TT2} M P P' :
    (∀.. (tt : TT1), FromTExist (tele_app P tt) (tele_app TT2 tt) (tele_appd_dep P' tt)) →
    ModalityMono M →
    SolveOne (TT := TeleConcatType TT1 TT2) M (tele_curry P') 
    ⊢ SolveOne (TT := TT1) M P.
  Proof.
    unseal_diaframe => /tforall_forall HPP' HM /=.
    apply HM, bi_texist_elim => tt.
    rewrite (tele_arg_concat_inv_eq tt).
    destruct (tele_arg_concat_inv_strong_hd tt) as [tt1 tt2].
    rewrite bi_texist_exist -(exist_intro tt1).
    rewrite -HPP' !bi_texist_exist.
    rewrite -(exist_intro tt2).
    rewrite tele_uncurry_curry_compose.
    by rewrite -tele_curry_uncurry_relation.
  Qed.

  Lemma solve_one_modal M1 M2 M Q Q' :
    Collect1Modal Q M2 Q' →
    CombinedModality M1 M2 M →
    ModalityMono M1 →
    SolveOne (TT := [tele]) M Q' ⊢ SolveOne (TT := [tele]) M1 Q.
    (* this one is necessary if two modalities can only be combined after some evars have been instantiated at a later point
       concretely, this happens if  *)
  Proof.
    unseal_diaframe => /=.
    rewrite /Collect1Modal /CombinedModality => HQ -> HM.
    by apply HM.
  Qed.

  Lemma solve_one_not_introducable {TT'} M Q M1 M2 :
    NotIntroducable M →
    SplitLeftModality3 M M1 M2 →  
    (* TODO: if Q is IntroduceHyp/IntroduceVar, this might cause awkward |={⊤}=> ‖ ∀ -, G goals *)
    SolveSep (TT := [tele]) M1 empty_goal (M2 (∃.. tt, tele_app Q tt)) ⊢
    SolveOne (TT := TT') M Q.
  Proof.
    unseal_diaframe => _ [HM HM' HM''].
    rewrite -HM /=.
    apply HM'.
    rewrite empty_goal_eq left_id //.
  Qed.

  Lemma solve_one_abd {TT} p P M Q R M1 M2 :
    Abduct (TT := TT) p P Q M2 R →
    SplitModality3 M M1 M2 →
    □?p P ∗ M1 R ⊢ SolveOne (TT := TT) M Q.
  Proof.
    unseal_diaframe; rewrite /Abduct => HPR [HM HM1 HM2].
    rewrite -HM modality_strong_frame_r.
    apply HM1 => {HM1} /=.
    by rewrite comm HPR.
  Qed.

  Lemma solve_one_abd_mod_input {TT} p P M Q R :
    AbductModInput TT p P Q M R →
    □?p P ∗ R ⊢ SolveOne (TT := TT) M Q.
  Proof.
    unseal_diaframe => /=. 
    rewrite /AbductModInput /Abduct => HPR //.
  Qed.
End solve_one_rules.


Section coq_tactics.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma tac_solve_one_fromsep {TT : tele} Δ M P P1 P2 {TT1} P1' {TT2} h :
    (TC∀.. (tt : TT), FromSepCareful (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ExtractDep P1 TT1 P1' TT2 h →
    ModalityStrongMono M →
    envs_entails Δ $ SolveSep M P1' (tele_bind (λ tt1, (∃.. tt2, tele_app P2 (tele_app (tele_appd_dep h tt1) tt2))%I)) →
    envs_entails Δ $ SolveOne (TT := TT) M P.
  Proof.
    rewrite envs_entails_unseal => HP1P2 HP1P1' HM ->.
    eapply (solve_one_fromsep _ _ _ _ _ _ _ id); try done.
    split; try apply _.
    - move => ? //=.
    - split; move => ?//=.
    - rewrite /TCTForall. apply tforall_forall => tt1.
      rewrite tele_app_bind. rewrite /AbsorbModal /=. reflexivity.
  Qed.

  Lemma tac_solve_one_fromexist_nested {TT1 : tele} Δ {TT2} M P P' :
    (TC∀.. (tt : TT1), FromTExist (tele_app P tt) (tele_app TT2 tt) (tele_appd_dep P' tt)) →
    ModalityMono M →
    envs_entails Δ $ SolveOne (TT := TeleConcatType TT1 TT2) M (tele_curry P') →
    envs_entails Δ $ SolveOne (TT := TT1) M P.
  Proof.
    rewrite envs_entails_unseal => HPP' HM ->.
    by apply solve_one_fromexist_nested.
  Qed.

  Lemma tac_solve_one_ext_abd_input Δ i M {TT} Q R p P :
    FindInExtendedContext Δ (λ p P, AbductModInput TT p P Q M R) i p P →
    envs_entails (envs_option_delete false i p Δ) R →
    envs_entails Δ $ SolveOne (TT := TT) M Q.
  Proof.
    rewrite envs_entails_unseal => HΔiP.
    rewrite (findinextcontext_spec false) => ->.
    apply ficext_satisfies in HΔiP as HPQ.
    rewrite <-solve_one_abd_mod_input => //.
  Qed.

  Lemma tac_solve_one_pure (Δ : envs PROP) {TT : tele} b M P φ :
    (TC∀.. tt : TT, FromPure b (tele_app P tt) (tele_app φ tt)) →
    IntroducableModality M →
    (TCIf (TCEq b true) (TCForall Affine (env_spatial Δ)) (TCEq b false)) →
    (* we abstract out the environment Δ so that we can reduce the telescopes without affecting Δ.
      Then we wrap this in [id] so that Coq does not β-reduce.. See also
    https://stackoverflow.com/questions/46622936/is-there-a-good-way-to-block-beta-reduction-from-automatically-occurring-in-goal *)
    (my_id (λ Δ', ∃.. tt, envs_entails Δ' ⌜tele_app φ tt⌝)) Δ →
    envs_entails Δ $ SolveOne (TT := TT) M P.
  Proof.
    cbn. rewrite envs_entails_unseal texist_exist => /tforall_forall HPφ HM + [tt Hφ].
    unseal_diaframe; rewrite -HM /= bi_texist_exist -(exist_intro tt) -HPφ /=.
    rewrite -(and_idem (of_envs Δ)) {1}Hφ {Hφ}.
    case => [-> HΔ | ->] /=; apply pure_elim_l => Hφ; eauto.
  Qed.

  Lemma tac_solve_one_not_introducable {TT'} Δ M Q M1 M2 :
    NotIntroducable M →
    SplitLeftModality3 M M1 M2 →
    envs_entails Δ $ SolveSep (TT := [tele]) M1 empty_goal (M2 (∃.. tt, tele_app Q tt))%I →
    envs_entails Δ $ SolveOne (TT := TT') M Q.
  Proof.
    rewrite envs_entails_unseal => HM HM' ->.
    apply solve_one_not_introducable => //.
  Qed.

  Lemma tac_solve_one_normalize_or {TT : tele} Δ M Q Q' :
    (TC∀.. (tt : TT), NormalizedProp (tele_app Q tt) (tele_app Q' tt) true) →
    ModalityMono M →
    envs_entails Δ $ SolveOne (TT := TT) M Q' →
    envs_entails Δ $ SolveOne (TT := TT) M Q.
  Proof.
    rewrite envs_entails_unseal => HQ HM ->.
    by apply solve_one_normalize.
  Qed.

  Lemma tac_solve_one_fromor {TT : tele} Δ M Q L1 L2 :
    (TC∀.. (tt : TT), FromOrCareful (tele_app Q tt) (tele_app L1 tt) (tele_app L2 tt)) →
    ModalityMono M →
    envs_entails Δ $ SolveOneFoc (TT := TT) M L1 (∃.. (tt: TT), tele_app L2 tt) →
    envs_entails Δ $ SolveOne (TT := TT) M Q.
  Proof.
    rewrite envs_entails_unseal => HQ HM ->.
    by apply solve_one_fromor.
  Qed.

  Lemma tac_solve_one_modal Δ M1 M2 M Q Q' :
    Collect1Modal Q M2 Q' →
    CombinedModality M1 M2 M →
    ModalityMono M1 →
    envs_entails Δ $ SolveOne (TT := [tele]) M Q' →
    envs_entails Δ $ SolveOne (TT := [tele]) M1 Q.
  Proof.
    rewrite envs_entails_unseal => HQ HMs HM1 ->.
    by eapply solve_one_modal.
  Qed.
End coq_tactics.


Ltac2 Type solve_one_step_type := [
  | SolveOneExist (constr)
  | SolveOneSep (constr)
  | SolveOnePure (constr)
  | SolveOneNormalizeOr (constr)
  | SolveOneOr (constr)
  | SolveOneModal (constr)
  | SolveOneNotIntroducable (constr)
  | SolveOneAbduct
].


Section fast_path_instances.
  Context {PROP : bi}.

  (* This instance would never work for typeclass search. But it works in combination with
    [tele_utils]' [get_repeated_quantifier_function] *)
  Lemma fast_path_fromtexist (TT : tele@{bi.Quant}) (P : TT -t> PROP) : FromTExist (bi_texist (tele_app P)) TT P.
  Proof. by red. Qed.

  Lemma fast_path_from_pureexist {A : Type} (P : A → Prop) : FromTExist (PROP := PROP) (⌜ex P⌝%I) [tele_pair A] (fun a => ⌜P a⌝%I).
  Proof. red. eauto. Qed.

  Lemma fast_path_fromsepcareful (P Q : PROP) : FromSepCareful (P ∗ Q) P Q.
  Proof. by red. Qed.

  Lemma fast_path_from_pureand (P Q : Prop) : FromSepCareful (PROP := PROP) ⌜P ∧ Q⌝%I ⌜P⌝%I ⌜Q⌝%I.
  Proof. red. eauto. Qed.

  Lemma fast_path_frompure (P : Prop) : FromPure (PROP := PROP) false ⌜P⌝%I P.
  Proof. by red. Qed.

  Lemma fast_path_emp_frompure : FromPure (PROP := PROP) true emp%I True.
  Proof. red. simpl. eauto. Qed.

  Lemma fast_path_fromorcareful (P Q : PROP) : FromOrCareful (P ∨ Q) P Q.
  Proof. by red. Qed.

  Lemma fast_path_notintroducable (M : PROP → PROP) : NotIntroducable M.
  Proof. by constructor. Qed.
End fast_path_instances.

Ltac2 fast_path_fromtexist (_ : unit) :=
  (* does something smart to get all the repeated existentials in one go. *)
  lazy_match! goal with
  | [|- FromTExist (bi_exist ?input) _ _] =>
    let (output, output_tele) := get_repeated_quantifier_function input in
    exact (fast_path_fromtexist $output_tele $output)
  end.


Ltac2 tc_apply0 (c : constr) : unit :=
  let f := ltac1:(i |- autoapply i with typeclass_instances) in
  f (Ltac1.of_constr c).

Ltac2 Notation "tc_apply" c(constr) := tc_apply0 c.

(* Note:
  Actually passing around the witnesses of succesful typeclass search seems to incur a 1% performance cost:
https://coq-speed.mpi-sws.org/d/Ne7jkX6kk/coq-speed?orgId=1&var-metric=instructions&var-project=diaframe&var-branch=ci%2Fdev-ike&var-config=All&var-group=%28%29.%2A&from=1710239293000&to=1710439018000
  This performance cost is paid off by establishing these witnesses faster (1.3% performance gain),
  but it means that launching tactic mode to build a constr is costly.
  In other words, one should avoid the [let f := constr:(ltac2:(tac) : type)] pattern. *)
Ltac2 infer_step_type (tt : constr) (m : constr) (p : constr) : solve_one_step_type :=
  (* all queries are under telescopes, so under TCTForall *)
  let tc_forall_assert_type (q : constr) : constr :=
    '(@TCTForall $tt $q) in
  let tcforall_intros (_ : unit) : unit :=
    ltac1:(simpl_diaframe_tele_tcforall; intros)
  in
  (* convenience tactic where one only specifies the tactic after telescope reduction *)
  let build_tcforall (tac : unit -> unit) (term : constr) : constr :=
    let witness_type := tc_forall_assert_type term in
    '(ltac2:(tcforall_intros (); tac ()) : $witness_type)
  in
  let tc_forall_assert (q : constr) : unit :=
    Std.assert (Std.AssertType None '(@TCTForall $tt $q) (Some tc_solve))
  in
  (* checks if a disjunction can be further normalized *)
  let choose_or (or_wit : constr) :=
    plus_once_list
      [ let p' := open_constr:(_) in
        let wit := build_tcforall tc_solve '(fun t => NormalizedProp (tele_app (TT := $tt) $p t) (tele_app $p' t) true) in
        SolveOneNormalizeOr wit
      | SolveOneOr or_wit ]
  in
  (* constructs premise for ∃ case *)
  let exist_premise (tac : unit -> unit) : constr :=
    let tt2 := open_constr:(_) in
    let p2 := open_constr:(_) in
    build_tcforall tac '(fun t => FromTExist (tele_app (TT := $tt) $p t) (tele_app $tt2 t) (tele_appd_dep $p2 t))
  in
  (* constructs premise for ∗ case *)
  let sep_premise (tac : unit -> unit) : constr :=
    let p1 := open_constr:(_) in
    let p2 := open_constr:(_) in
    build_tcforall tac '(fun t => FromSepCareful (tele_app (TT := $tt) $p t) (tele_app $p1 t) (tele_app $p2 t))
  in
  (* constructs premise for ⌜⌝ case *)
  let pure_premise (tac : unit -> unit) : constr :=
    let b := open_constr:(_) in
    let φ := open_constr:(_) in
    build_tcforall tac '(fun t => FromPure $b (tele_app (TT := $tt) $p t) (tele_app $φ t))
  in
  (* constructs premise for ∨ case *)
  let or_premise (tac : unit -> unit) : constr :=
    let p1 := open_constr:(_) in
    let p2 := open_constr:(_) in
    build_tcforall tac '(fun t => FromOrCareful (tele_app (TT := $tt) $p t) (tele_app $p1 t) (tele_app $p2 t))
  in
  let check_introducable_then (s : solve_one_step_type) : solve_one_step_type :=
    plus_once_list
      [ assert_succeeds (fun () =>
          Std.assert (Std.AssertType None '(IntroducableModality $m) (Some tc_solve))
        ); s
      | SolveOneNotIntroducable '(fast_path_notintroducable $m) ]
  in
  let (head, args, bs) := get_head_term_n_args p in
  (* FIXME: It's a bit annoying that we have to use [safe_apply] to guard against the case where
    the de Bruijn variable is the head symbol of an expression. Instead of an open_constr we could use
    preterm, but how to put lambdas around those? *)
  lazy_match! head with
  | @bi_exist _ =>
    let input := List.nth args 1 in
    let (output, output_tele) := get_repeated_quantifier_function input in
    SolveOneExist (rebind (safe_apply 'fast_path_fromtexist [output_tele; output]) bs)
  | @bi_sep _  =>
    let left_c := List.nth args 0 in
    let right_c := List.nth args 1 in
    SolveOneSep (rebind (safe_apply 'fast_path_fromsepcareful [left_c; right_c]) bs)
  | @bi_pure ?proptype =>
    let phi := (List.hd args) in
    lazy_match! phi with
    | ex ?phi_ex =>
      SolveOneExist (rebind (safe_apply 'fast_path_from_pureexist [phi_ex]) bs)
    | and ?lphi ?rphi =>
      SolveOneSep (rebind (safe_apply 'fast_path_from_pureand [lphi; rphi]) bs)
    | _ =>
      check_introducable_then (SolveOnePure (rebind (safe_apply '@fast_path_frompure [proptype; phi]) bs))
    end
  | @bi_or ?proptype =>
    let left_c := List.nth args 0 in
    let right_c := List.nth args 1 in
    choose_or (rebind (safe_apply '@fast_path_fromorcareful [proptype; left_c; right_c]) bs)
  | @bi_emp ?proptype => check_introducable_then (SolveOnePure (rebind '(@fast_path_emp_frompure $proptype) bs))
    (* only choose pure if the modality is introducable! *)
  | _ =>
    plus_once_list
      [ SolveOneExist (exist_premise tc_solve)
      | SolveOneSep (sep_premise tc_solve)
      | check_introducable_then (SolveOnePure (pure_premise tc_solve))
      | choose_or (or_premise tc_solve)
      | lazy_match! tt with
        | TeleO =>
          let witness := '(ltac:(tc_solve) : (Collect1Modal $p _ _)) in
          SolveOneModal witness
        end
      | let witness := '(ltac:(tc_solve) : NotIntroducable $m) in
        SolveOneNotIntroducable witness
      | SolveOneAbduct ]
  end.

Ltac2 solveOneExist (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- simple notypeclasses refine (tac_solve_one_fromexist_nested (TT1 := tt) _ _ _ _ wit _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [tc_solve () | (*FIXME*) ltac1:(simpl_diaframe_tele)].

Ltac2 solveOneSep (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- notypeclasses refine (tac_solve_one_fromsep (TT := tt) _ _ _ _ _ _ _ wit _ _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [tc_solve ().. | (*FIXME*) ltac1:(simpl_diaframe_tele)].

(* TODO: introducable modality is also queried twice? *)
Ltac2 solveOnePure (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- notypeclasses refine (tac_solve_one_pure (TT := tt) _ _ _ _ _ wit _ _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [tc_solve ().. | (*FIXME*) ltac1:(simpl_diaframe_pure_tele); eauto ].
(* we call regular eauto on lone pure goals ^ *)

Ltac2 solveOneNormalizeOr (witness : constr) :=
  ltac1:(wit |- notypeclasses refine (tac_solve_one_normalize_or _ _ _ _ wit _ _)) (Ltac1.of_constr witness) >
    [tc_solve ().. | simpl].

Ltac2 solveOneOr (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- notypeclasses refine (tac_solve_one_fromor (TT := tt) _ _ _ _ _ wit _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [tc_solve().. | ].

Ltac2 solveOneModal (witness : constr) :=
  ltac1:(wit |- notypeclasses refine (tac_solve_one_modal _ _ _ _ _ _ wit _ _ _)) (Ltac1.of_constr witness) >
    [tc_solve().. | ].

Ltac2 solveOneNotIntroducable (witness : constr) :=
  ltac1:(wit |- notypeclasses refine (tac_solve_one_not_introducable _ _ _ _ _ wit _ _)) (Ltac1.of_constr witness) >
    [tc_solve().. | ].

Ltac2 solveOneAbduct (state : automation_state.t) (gid : goal_id) :=
  ltac1:(notypeclasses refine (tac_solve_one_ext_abd_input _ _ _ _ _ _ _ _ _)) >
    [tc_solve() | ];
    handle_prop_deletion_on_goal state gid;
    ltac1:(simpl_diaframe_post_hint).


Ltac2 solveOneStep (pure_solve_result_handler : pure_solver_result -> bool)
    (state : automation_state.t) (gid : goal_id)
    (Δ : constr) (tt : constr) (m : constr) (p : constr) : goal_id list :=
  (* solveOneStep is used when the goal has shape G, where G is not a wand and not a forall,
    and G is possibly behind existential quantifiers and a modality M.
    We distinguish the following cases:
    - G has shape (∃ G)
    - G has shape (L ∗ G)
    - G has shape ⌜G⌝ (a pure proposition)
    - G has shape (G1 ∨ G2).
        Here, we first try to simplify both sides ('normalize') - i.e. if one branch is contradictory, we drop it.
        If after normalization this is still a disjunction, we have automation for that with SolveOneFoc
    - G has shape (M' G) with M' a modality and no existential quantifiers. Move the modality to the
        SolveOne construct.
    - The surrounding modality M is not introducable. This indicates the need to close an invariant:
        set the dummy resource χ as the new goal, to start search for a closing resource
    - G is an atom. Find an (abduction) connection. *)
  let pure_post_process (_ : unit) :=
    Control.enter (fun _ => plus_once (fun _ =>
      progress (try_solve_pure_on (fun c => ltac1:(repeat_eexists); c ()) pure_solve_result_handler)
    ) (fun _ =>
      (* should solve this goal, but cannot *)
      Control.zero Assertion_failure
    ) )
  in
  match infer_step_type tt m p with
  | SolveOneExist wit => solveOneExist tt wit; [gid]
  | SolveOneSep wit => solveOneSep tt wit; [gid]
  | SolveOnePure wit => solveOnePure tt wit; pure_post_process(); []
  | SolveOneNormalizeOr wit => solveOneNormalizeOr wit; [gid]
  | SolveOneOr wit => solveOneOr tt wit; [gid]
  | SolveOneModal wit => solveOneModal wit; [gid]
  | SolveOneNotIntroducable wit => solveOneNotIntroducable wit; [gid]
  | SolveOneAbduct => solveOneAbduct state gid; [gid]
  end.

Ltac solveOneStep :=
  ltac2:(
    lazy_match! goal with
    | [|- envs_entails ?Δ (@SolveOne _ ?tt ?m ?p)] =>
      let (state, gid) := automation_state.new () in
      let _ := solveOneStep handle__shelve_unsolved_noevars state gid Δ tt m p in
      ()
    end
  ).





