From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import env_utils util_classes solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import pure_solver automation_state ipm_hyp_utils ltac2_store reductions introduce_prop.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file contains 'case 2' of Diaframe's proof search strategy: introducing wands.
   In addition to the paper, this also includes using MergableConsume and
   MergablePersist instances to detect compatibilities between hypotheses in the context *)

Section mergable.
  Context {PROP : bi}.
  Implicit Types P : PROP.

  Lemma merge_and_consume rp P1 C p P2 P3 R:
    MergableConsume P1 rp C →
    C p P2 P3 →
    □?p P2 ∗ (P3 -∗ R) ⊢ P1 -∗ R.
  Proof.
    move => Hmerge HC /=.
    iIntros "[HP2 HPR] HP1".
    iApply "HPR".
    iApply mergable_consume; [done | iFrame].
  Qed.

  Lemma merge_no_consume P1 C p P2 P3 R :
    MergablePersist P1 C →
    C p P2 P3 →
    □?p P2 ∗ (□?p P2 ∗ P1 -∗ □ P3 -∗ R) ⊢ P1 -∗ R.
  Proof.
    move => Hmerge HC /=.
    iIntros "[HP2 HPR] HP1".
    iAssert (<pers> P3)%I with "[HP1 HP2]" as "#HP3".
    { iApply mergable_persist; [done | iFrame]. }
    by iApply ("HPR" with "[$HP1 $HP2]").
  Qed.

End mergable.


Section mergable_env.
  Context {PROP : bi}.

  (* MergablePersist will only detect one hypothesis with a persistent compatibility.
     Sometimes there are more. The [MergableEnv] construct gathers all of them are found *)
  Class MergableEnv (Δ : envs PROP) (P : PROP) (M : option PROP) :=
    mergable_env : (of_envs Δ) ∗ P ⊢ <pers> default emp M.

  Lemma mergable_env_base Δ P :
    MergableEnv Δ P None.
  Proof. rewrite /MergableEnv; eauto. Qed.

  Class MergablePersistTwo p P Pn Po :=
    mergable_persist_two : Pn ∗ □?p P ⊢@{PROP} <pers> Po.

  Instance mergable_persist_two_from_mergable_persist C p Pn P Po:
    MergablePersist Pn C →
    C p P Po →
    MergablePersistTwo p P Pn Po.
  Proof. rewrite /MergablePersist /MergablePersistTwo => HPC => /HPC //. Qed.

  Lemma mergeable_env_add_free Δ P P0 mP E :
    MergablePersistTwo false E P P0 →
    (⊢ E) →
    MergableEnv Δ P mP →
    MergableEnv Δ P (Some $ match mP with | Some P' => P0 ∗ P' | _ => P0 end)%I.
  Proof.
    rewrite /MergableEnv /MergablePersistTwo => /= HP HE HΔ.
    iIntros "[HΔ HP]".
    iAssert (□ P0)%I as "#HP0".
    { iClear "HΔ". iApply HP. iFrame. iApply HE. }
    iCombine "HΔ HP" as "HΔP".
    rewrite HΔ. iDestruct "HΔP" as "#HΔP".
    iIntros "!>". destruct mP as [P' |]; simpl; iFrame "#".
  Qed.

  Instance mergeable_env_add_empty_hyp_first Δ P P0 mP :
    MergablePersistTwo false (ε₀)%I P P0 →
    MergableEnv Δ P mP →
    MergableEnv Δ P (Some $ match mP with | Some P' => P0 ∗ P' | _ => P0 end)%I.
  Proof.
    intros. eapply mergeable_env_add_free; try eassumption.
    rewrite empty_hyp_first_eq. eauto.
  Qed.

  Instance mergeable_env_add_empty_hyp_last Δ P P0 mP :
    MergablePersistTwo false (ε₁)%I P P0 →
    MergableEnv Δ P mP →
    MergableEnv Δ P (Some $ match mP with | Some P' => P0 ∗ P' | _ => P0 end)%I.
  Proof.
    intros. eapply mergeable_env_add_free; try eassumption.
    rewrite empty_hyp_last_eq. eauto.
  Qed.

  Lemma mergable_env_star Δ P P1 P2 :
    MergableEnv Δ P (Some P1) →
    MergableEnv Δ P (Some P2) →
    MergableEnv Δ P (Some (P1 ∗ P2))%I.
  Proof.
    rewrite /MergableEnv /= => HP1 HP2.
    iIntros "HR".
    iAssert (□ P1)%I as "#HP1".
    { by iApply HP1. }
    iAssert (□ P2)%I as "#HP2".
    { by iApply HP2. }
    iClear "HR". iFrame "#".
  Qed.

  Instance mergeable_env_do_lookup Δ Pn (js : list ident) (prs : list (bool * PROP * PROP)):
    Forall2 (λ j '(p, P, Po), 
      TCAnd (envs_lookup j Δ = Some (p, P))
            (MergablePersistTwo p P Pn Po)
    ) js prs →
    MergableEnv Δ Pn (Some $ right_big_sep (my_map (fun '(_, _, Po) => Po) prs))%I.
  Proof.
    revert prs. induction js as [|j js' IHjs].
    - rewrite /MergableEnv => prs /Forall2_nil_inv_l => -> /=. eauto.
    - move => prs Hforall. inversion_clear Hforall.
      specialize (IHjs _ H0). destruct y as [[p P] Po].
      unfold MergableEnv. rewrite right_big_sep_equiv_big_sepL.
      simpl. change (MergableEnv Δ Pn (Some (Po ∗ ([∗ list] '(_, _, Po0) ∈ l', Po0))%I)).
      apply mergable_env_star; last first.
      { unfold MergableEnv; rewrite -right_big_sep_equiv_big_sepL; exact IHjs. }
      clear -H. revert H.
      rewrite /MergableEnv /MergablePersistTwo => /= [[H1 H2]] /=.
      rewrite envs_lookup_sound; last done.
      iIntros "[[HP _] HPn]"; iStopProof. by rewrite comm.
  Qed.
End mergable_env.

(* TODO: get rid of MergablePersist! We're actually doing pairwise merging now*)

Ltac2 get_merged_two (pers : constr) (p : constr) (pn : constr) : constr :=
  let po := open_constr:(_) in
  let result := constr:(ltac:(tc_solve) : MergablePersistTwo $pers $p $pn $po) in
  result.

Ltac2 get_idents_of_envs (Δ : constr) : constr list :=
  let is_coq := eval cbv in (envs_dom $Δ) in
  of_coq_list is_coq.

Ltac2 get_merged_two_lookup (pn : constr) (j : constr) (Δ : constr) :
    constr * constr :=
    (* (p , P, Po) pair , proof_term *)
  let pers := open_constr:(_) in
  let p := open_constr:(_) in
  let po := open_constr:(_) in
  let construct_tac : unit -> unit := 
    fun _ => 
      split > [reflexivity | ];
      tc_solve ()
  in
  let result := constr:(
        ltac2:(construct_tac ()) : TCAnd (envs_lookup $j $Δ = Some ($pers, $p))
                                         (MergablePersistTwo $pers $p $pn $po)
  ) in 
  ( '($pers, $p, $po), result).

Ltac2 rec construct_mergable_env_lookup_int
  (pn : constr) (idents : constr list) (Δ : constr)
    : constr * constr := 
  match idents with
  | [] => ('(@nil ident), '(List.Forall2_nil (λ j '(p, P, Po), 
      TCAnd (envs_lookup j $Δ = Some (p, P))
            (MergablePersistTwo p P $pn Po)
    )))
  | i :: idents' =>
    let (is', args) := construct_mergable_env_lookup_int pn idents' Δ in
    plus_once (fun _ => 
      let (pr, arg) := get_merged_two_lookup pn i Δ in
      (
        '(cons $i $is'),
        '(List.Forall2_cons $i $pr $arg $args)
      )
    ) (fun _ =>
      (is', args)
    )
  end.

Ltac2 construct_mergable_env_lookup 
  (Δ : constr) (pn : constr)
    : unit := 
  let idents := get_idents_of_envs Δ in
  let (is', args) := construct_mergable_env_lookup_int pn idents Δ in
  lazy_match! is' with
  | [] => apply mergable_env_base
  | _ => apply (mergeable_env_do_lookup _ $pn $is' _ $args)
  end.

Ltac2 construct_mergable_env (Δ : constr) (pn : constr) 
  : unit := 
  plus_once_list 
    [ assert_succeeds (fun _ => assert (∃ C, MergablePersist $pn C) by (eexists; tc_solve ()));
      plus_once (fun _ =>
        let eps0_merge := (get_merged_two 'false '(ε₀)%I pn) in
        ltac1:(arg |- 
          notypeclasses refine (mergeable_env_add_empty_hyp_first _ _ _ _ arg _)
        ) (Ltac1.of_constr eps0_merge)
      ) (fun _ => 
        ()
      );
      plus_once (fun _ =>
        let eps1_merge := get_merged_two 'false '(ε₁)%I pn in
        ltac1:(arg |-
          notypeclasses refine (mergeable_env_add_empty_hyp_last _ _ _ _ arg _)
        ) (Ltac1.of_constr eps1_merge)
      ) (fun _ => 
        ()
      );
      construct_mergable_env_lookup Δ pn
    | apply mergable_env_base].


Global Hint Extern 4 (MergablePersistTwo _ _ _ _) =>
  once (autoapply @mergable_persist_two_from_mergable_persist with typeclass_instances;
  [typeclasses eauto | ];
  cbn beta;
  tc_solve) : typeclass_instances.

Global Hint Extern 4 (MergableEnv ?Δ ?Pn _) =>
  let f := ltac2:(Δ1 pn1 |-
    let Δ := Option.get (Ltac1.to_constr Δ1) in
    let pn := Option.get (Ltac1.to_constr pn1) in
    construct_mergable_env Δ pn
  ) in
  f Δ Pn : typeclass_instances.


Section lemmas.
  Context {PROP : bi}.

  Implicit Types P Q : PROP.

  Lemma introduce_hyp_premise_sep P P1 P2 Q :
    IntoSepCareful P P1 P2 →
    IntroduceHyp P1 (IntroduceHyp P2 Q) ⊢ IntroduceHyp P Q.
  Proof.
    unseal_diaframe; rewrite /IntoSepCareful /= => ->.
    iIntros "HPQ [HP1 HP2]".
    by iApply ("HPQ" with "HP1").
  Qed.

  Lemma introduce_hyp_premise_tele_exist {TT} P R Q :
    IntoTExist P TT R →
    IntroduceVars (TT := TT) $ tele_map (λ R, IntroduceHyp R Q) R ⊢ IntroduceHyp P Q.
  Proof.
    unseal_diaframe; rewrite /IntoTExist /= => ->.
    iIntros "HRQ".
    iDestruct 1 as (tt) "HR".
    iSpecialize ("HRQ" $! tt).
    rewrite tele_map_app.
    by iApply "HRQ".
  Qed.

  (* Currently, IntoTExist cannot handle dependent exists. 
  For IntroduceHyp, we can overcome this by introducing the exists one by one: *)

  Lemma introduce_hyp_premise_exist (P Q : PROP) {A : Type} (R : A → PROP) :
    IntoExistCareful P R →
    IntroduceVars (TT := [tele_pair A]) (λ a, IntroduceHyp (R a) Q) ⊢ IntroduceHyp P Q.
  Proof.
    unseal_diaframe; rewrite /IntoExistCareful /= => ->.
    iIntros "HRQ".
    iDestruct 1 as (r) "HR".
    by iApply "HRQ".
  Qed.

  Lemma introduce_hyp_premise_or P P1 P2 Q : 
    IntoOr P P1 P2 →
    IntroduceHyp P1 Q ∧ IntroduceHyp P2 Q ⊢ IntroduceHyp P Q.
  Proof. 
    unseal_diaframe; rewrite /IntoOr /= => ->. 
    iIntros "HPQ [HP1|HP2]".
    - iDestruct "HPQ" as "[HPQ1 _ ]".
      by iApply "HPQ1".
    - iDestruct "HPQ" as "[_ HPQ2]".
      by iApply "HPQ2".
  Qed.

  Lemma introduce_hyp_mergable_consume p rp P C Q R S :
    MergableConsume Q rp C →
    C p P R →
    □?p P ∗ IntroduceHyp R S ⊢ IntroduceHyp Q S.
  Proof.
    unseal_diaframe => Hmerge HC.
    eapply merge_and_consume => //.
  Qed.

  Lemma introduce_hyp_mergable_no_consume p P C Q R S :
    MergablePersist Q C →
    C p P R →
    □?p P ∗ (□?p P ∗ Q -∗ IntroduceHyp (□ R) S) ⊢ IntroduceHyp Q S.
  Proof.
    unseal_diaframe => /= Hmerge HC.
    eapply merge_no_consume => //.
  Qed.

  Lemma introduce_hyp_premise_intuit P P' Q :
    IntoPersistent false P P' →
    Affine P →
    (□ P' -∗ Q) ⊢ IntroduceHyp P Q.
  Proof.
    unseal_diaframe; rewrite /IntoPersistent /= => -.
    iIntros (HPP' HP) "HP'Q HP".
    iApply "HP'Q".
    iStopProof.
    rewrite /bi_intuitionistically.
    by apply affinely_intro.
  Qed.

  Lemma introduce_hyp_drop_modal P M P' C Q :
    IntoModal false P M P' →
    DropModal M Q C →
    C P' →
    IntroduceHyp P' Q ⊢ IntroduceHyp P Q.
  Proof.
    unseal_diaframe; rewrite /IntoModal /DropModal /= => -> HQ /HQ //.
  Qed.

End lemmas.

Section coq_tactics.
  Context {PROP : bi}.

  Implicit Types P Q : PROP.

  Lemma tac_introduce_hyp_premise_sep Δ P P1 P2 Q :
    IntoSepCareful P P1 P2 →
    envs_entails Δ $ IntroduceHyp P1 (IntroduceHyp P2 Q) →
    envs_entails Δ $ IntroduceHyp P Q.
  Proof.
    rewrite envs_entails_unseal => HΔΔ' ->.
    apply introduce_hyp_premise_sep, _.
  Qed.

  Lemma tac_introduce_hyp_premise_exist Δ P {A} (R : A → PROP) Q:
    IntoExistCareful P R →
    envs_entails Δ $ IntroduceVars (TT := [tele_pair A]) (λ a, IntroduceHyp (R a) Q) →
    envs_entails Δ $ IntroduceHyp P Q.
  Proof.
    rewrite envs_entails_unseal => HPR ->.
    by eapply introduce_hyp_premise_exist.
  Qed.

  Lemma tac_introduce_hyp_premise_or Δ P P1 P2 Q :
    IntoOr P P1 P2 →
    envs_entails Δ $ IntroduceHyp P1 Q →
    envs_entails Δ $ IntroduceHyp P2 Q →
    envs_entails Δ $ IntroduceHyp P Q.
  Proof.
    rewrite envs_entails_unseal => HP HΔ1 HΔ2.
    rewrite -introduce_hyp_premise_or.
    by apply and_intro.
  Qed.

  Lemma tac_introduce_hyp_premise_modal Δ P M P' C Q :
    IntoModal false P M P' →
    DropModal M Q C →
    C P' →
    envs_entails Δ $ IntroduceHyp P' Q →
    envs_entails Δ $ IntroduceHyp P Q.
  Proof.
    rewrite envs_entails_unseal => HP HQC HCP ->.
    by eapply introduce_hyp_drop_modal.
  Qed.

  Lemma tac_introduce_hyp_merge_consume Δ P1 R C mi p rp P2 P3 :
    TCAnd (MergableConsume P1 rp C) (FindInExtendedContext Δ (λ p P2, C p P2 P3) mi p P2) →
    envs_entails (envs_option_delete rp mi p Δ) $ IntroduceHyp P3 R →
    envs_entails Δ $ IntroduceHyp P1 R.
  Proof.
    rewrite envs_entails_unseal => [[HP1 HP2]] HΔ.
    rewrite (findinextcontext_spec rp) HΔ.
    eapply introduce_hyp_mergable_consume => //.
    by eapply ficext_satisfies in HP2.
  Qed.

  Lemma tac_introduce_hyp_merge_persist j Δ P1 a P1' R C mi p P2 P3 :
    TCAnd (MergablePersist P1 C) (FindInExtendedContext Δ (λ p P2, C p P2 P3) mi p P2) →
    TCIf (TCAnd (IntoPersistent false P1 P1') (Affine P1))
        (TCEq a true) (TCAnd (TCEq a false) (TCEq P1' P1)) →
    match envs_add_fresh a j P1' Δ with
    | Some Δ' => envs_entails Δ' $ IntroduceHyp (□ P3)%I R
    | _ => False
    end →
    envs_entails Δ $ IntroduceHyp P1 R.
  Proof.
    unseal_diaframe; rewrite envs_entails_unseal => [[HP1 HP2]] HP1p' HΔR.
    destruct (envs_add_fresh a j P1' Δ) as [Δ'| ] eqn:HΔ''; last done.
    iIntros "HΔ' HP1".
    assert (P1 ⊢ □?a P1') as HP1_ent.
    { revert HP1p'. case => [[HP1P1' HP1a] ->| [-> ->] ] //=.
      unfold bi_intuitionistically.
      rewrite -HP1P1' /=. unfold bi_affinely. iIntros "H"; iSplit => //. }
    iAssert (□ P3)%I as "#HP3"; last first. (* 'iEnough' *)
    { rewrite (envs_add_fresh_sound Δ) //.
      rewrite HP1_ent.
      iSpecialize ("HΔ'" with "HP1"). 
      by iApply (HΔR with "HΔ'"). }
    rewrite (findinextcontext_spec true).
    iDestruct "HΔ'" as "[HP2 _]".
    iCombine "HP1 HP2" as "HP".
    rewrite mergable_persist; last first.
    { by eapply ficext_satisfies in HP2. }
    done.
  Qed.

  Lemma tac_introduce_hyp_merge_env j Δ P1 a P1' R mP3 :
    MergableEnv Δ P1 mP3 →
    TCIf (TCAnd (IntoPersistent false P1 P1') (Affine P1))
        (TCEq a true) (TCAnd (TCEq a false) (TCEq P1' P1)) →
    match envs_add_fresh a j P1' Δ with
    | Some Δ' => 
      match mP3 with
      | None => envs_entails Δ' R
      | Some P3 =>
        envs_entails Δ' $ IntroduceHyp (□ P3)%I R
      end
    | _ => False
    end →
    envs_entails Δ $ IntroduceHyp P1 R.
  Proof.
    unseal_diaframe; rewrite envs_entails_unseal => HΔ HP1p' HΔR.
    destruct (envs_add_fresh a j P1' Δ) as [Δ'| ] eqn:HΔ''; last done.
    iIntros "HΔ' HP1".
    assert (P1 ⊢ □?a P1') as HP1_ent.
    { revert HP1p'. case => [[HP1P1' HP1a] ->| [-> ->] ] //=.
      unfold bi_intuitionistically.
      rewrite -HP1P1' /=. unfold bi_affinely. iIntros "H"; iSplit => //. }
    destruct mP3 as [P3|]; last first.
    { rewrite envs_add_fresh_sound // -HΔR. rewrite -HP1_ent. by iApply "HΔ'". }
    iAssert (□ P3)%I as "#HP3"; last first. (* 'iEnough' *)
    { rewrite (envs_add_fresh_sound Δ) //.
      rewrite -HP1_ent.
      iSpecialize ("HΔ'" with "HP1"). 
      by iApply (HΔR with "HΔ'"). }
    iCombine "HΔ' HP1" as "H".
    rewrite HΔ.
    eauto.
  Qed.

  (* TODO: move these to solve_instances? *)
  Global Instance mergable_persist_from_box P C :
    MergablePersist P C →
    MergablePersist (□ P)%I C | 5.
  Proof.
    rewrite /MergablePersist => HPC p P2 P3 /HPC <-.
    iIntros "[#$ $]".
  Qed.
  Global Instance mergable_consume_from_box P rp C :
    MergableConsume P rp C →
    MergableConsume (□ P)%I rp C | 5.
  Proof.
    rewrite /MergableConsume => HPC p P2 P3 /HPC <-.
    iIntros "[#$ $]".
  Qed.

  Lemma tac_introduce_hyp_not_mergable j Δ P1 a P1' R : 
    TCIf (TCAnd (IntoPersistent false P1 P1') (Affine P1))
        (TCEq a true) (TCAnd (TCEq a false) (TCEq P1' P1)) →
    match envs_add_fresh a j P1' Δ with
    | Some Δ' => envs_entails Δ' $ R
    | _ => False
    end →
    envs_entails Δ $ IntroduceHyp P1 R.
  Proof.
    unseal_diaframe; rewrite envs_entails_unseal => HP1p' HΔR.
    destruct (envs_add_fresh a j P1' Δ) as [Δ'| ] eqn:HΔ''; last done.
    rewrite (envs_add_fresh_sound Δ) // HΔR.
    apply bi.wand_mono => //.
    revert HP1p'. case => [[HP1P1' HP1a] ->| [-> ->] ] //=.
    unfold bi_intuitionistically.
    rewrite -HP1P1' /=. unfold bi_affinely. iIntros "H"; iSplit => //.
  Qed.

  Lemma tac_introduce_hyp_premise_pure Δ P φ Q ba :
    IntoPure P φ →
    TCIf (Affine P) (TCEq ba true) (TCEq ba false) →
    match (if ba then Some Δ else envs_add_fresh false None True%I Δ) with
    | Some Δ' => φ → envs_entails Δ' Q
    | _ => False
    end →
    envs_entails Δ $ IntroduceHyp P Q.
  Proof.
    rewrite envs_entails_unseal => HPφ HP.
    destruct HP as [HP ->| ->].
    - assert (IntoPersistent false P ⌜φ⌝).
      { rewrite /IntoPersistent /= HPφ.
        by rewrite -(persistently_if_pure true φ) /= persistently_idemp. }
      rewrite -introduce_hyp_premise_intuit => HΔφ.
      iIntros "HΔ %".
      by iApply HΔφ.
    - unseal_diaframe.
      destruct (envs_add_fresh false None True Δ) as [Δ'| ] eqn:HΔ''; last done.
      rewrite HPφ => Hφ.
      apply bi.wand_intro_l.
      apply bi.wand_elim_l', bi.pure_elim' => /Hφ.
      apply envs_add_fresh_sound in HΔ''.
      rewrite HΔ'' /= => <-.
      apply bi.wand_intro_r.
      rewrite bi.wand_elim_r //.
  Qed.

  Lemma tac_introduce_hyp_premise_emp Δ Q :
    envs_entails Δ Q →
    envs_entails Δ $ IntroduceHyp emp%I Q.
  Proof. 
    unseal_diaframe; rewrite envs_entails_unseal => ->.
    by rewrite left_id. 
  Qed.

End coq_tactics.


Ltac2 introduceHypProp store := 
  lazy_match! goal with
  | [|- ?φ → ?p] =>
    (* FIXME: we call simpl here, wanted? *)
    let φ' := eval simpl in $φ in
    lazy_match! φ' with
    | True => intros _
    | ?a = ?a => intros _
    | _ => 
      introduce_one_prop store
    end
  end.

Ltac introduceHypEmp := 
  notypeclasses refine (tac_introduce_hyp_premise_emp _ _ _ ).

Ltac2 introduceHypPure (witness : constr) store :=
  ltac1:(wit |- notypeclasses refine (tac_introduce_hyp_premise_pure _ _ _ _ _ wit _ _)) (Ltac1.of_constr witness) >
  [ tc_solve () | ltac1:(simpl_diaframe_envs); introduceHypProp store ].

Ltac introduceHypSepWit wit :=
  notypeclasses refine (tac_introduce_hyp_premise_sep _ _ _ _ _ wit _).

Ltac introduceHypSep := 
  (unshelve introduceHypSepWit open_constr:(_)); shelve_unifiable;
  [tc_solve | ].

Ltac2 introduceHypSep (witness : constr) :=
  ltac1:(wit |- introduceHypSepWit wit) (Ltac1.of_constr witness).

Ltac introduceHypExistWit wit :=
  notypeclasses refine (tac_introduce_hyp_premise_exist _ _ _ _ wit _).

Ltac introduceHypExist := 
  (unshelve introduceHypExistWit open_constr:(_)); shelve_unifiable;
  [tc_solve | ].

Ltac2 introduceHypExist (witness : constr) :=
  ltac1:(wit |- introduceHypExistWit wit) (Ltac1.of_constr witness).

Ltac introduceHypOrWit wit :=
  notypeclasses refine (tac_introduce_hyp_premise_or _ _ _ _ _ wit _ _ ).

Ltac introduceHypOr :=
  (unshelve introduceHypOrWit open_constr:(_)); shelve_unifiable;
  [tc_solve | | ].

Ltac2 introduceHypOr (witness : constr) :=
  ltac1:(wit |- introduceHypOrWit wit) (Ltac1.of_constr witness).

Ltac introduceHypModalWit wit1 wit2 wit3 :=
  notypeclasses refine (tac_introduce_hyp_premise_modal _ _ _ _ _ _ wit1 wit2 wit3 _).

Ltac introduceHypModal :=
  (unshelve introduceHypModalWit open_constr:(_) open_constr:(_) open_constr:(_));
  shelve_unifiable;
    [ tc_solve
    | tc_solve
    | cbn beta; tc_solve
      (* We have a higher order type class C here, which we want to beta-reduce *)
    | ].

Ltac2 introduceHypModal (wit1 : constr) (wit2 : constr) (wit3 : constr) :=
  ltac1:(w1 w2 w3 |- introduceHypModalWit w1 w2 w3)
    (Ltac1.of_constr wit1)
    (Ltac1.of_constr wit2)
    (Ltac1.of_constr wit3).

Ltac introduceHypMergeConsumeWit wit :=
  notypeclasses refine (tac_introduce_hyp_merge_consume _ _ _ _ _ _ _ _ _ wit _).

Ltac introduceHypMergeConsume :=
  (unshelve introduceHypMergeConsumeWit open_constr:(_)); shelve_unifiable;
  [tc_solve | simpl_diaframe_envs].

Ltac2 introduceHypMergeConsume (witness : constr) :=
  ltac1:(wit1 |- introduceHypMergeConsumeWit wit1; simpl_diaframe_envs) (Ltac1.of_constr witness).

Ltac introduceHypMergeEnv :=
  notypeclasses refine (tac_introduce_hyp_merge_env (@None ident) _ _ _ _ _ _ _ _ _);
    [tc_solve | tc_solve | simpl_diaframe_envs ].

Ltac2 Type intro_hyp_step_type := [
  | IntroHypEmp
  | IntroHypSep (constr)
  | IntroHypEx (constr)
  | IntroHypOr (constr)
  | IntroHypPure (constr)
  | IntroHypModal (constr, constr, constr)
  | IntroHypMergeConsume (constr)
  | IntroHypMergeEnv
].

(* This is a bit ugly, but we need access to fast-path instances *)
Section fast_path_instances.
  Context {PROP : bi}.

  Lemma fast_path_sep (P Q : PROP) : IntoSepCareful (P ∗ Q) P Q.
  Proof. by red. Qed.

  Lemma fast_path_exist {A : Type} (Q : A → PROP) : IntoExistCareful (∃ a, Q a) Q.
  Proof. by red. Qed.

  Lemma fast_path_or (P Q : PROP) : IntoOr (P ∨ Q) P Q.
  Proof. by red. Qed.

  Lemma fast_path_pure_and (P Q : Prop) : IntoSepCareful (PROP := PROP) ⌜P ∧ Q⌝ ⌜P⌝ ⌜Q⌝.
  Proof. red. eauto. Qed.

  Lemma fast_path_pure_or (P Q : Prop) : IntoOr (PROP := PROP) ⌜P ∨ Q⌝ ⌜P⌝ ⌜Q⌝.
  Proof. red. eauto. Qed.

  Lemma fast_path_pure_exist {A : Type} (Q : A → Prop) : IntoExistCareful (PROP := PROP) ⌜∃ a, Q a⌝%I (λ a, ⌜Q a⌝%I).
  Proof. red. eauto. Qed.

  Lemma fast_path_pure (P : Prop) : IntoPure (PROP := PROP) ⌜P⌝ P.
  Proof. by red. Qed.

  Lemma fast_path_affine_pure (φ : Prop) : IntoPure (PROP := PROP) (<affine> ⌜φ⌝) φ.
  Proof. red. by rewrite bi.affinely_elim. Qed.

  Lemma fast_path_affine_pure_and (P Q : Prop) : IntoSepCareful (PROP := PROP) (<affine> ⌜P ∧ Q⌝) (<affine> ⌜P⌝) (<affine> ⌜Q⌝).
  Proof. red. eauto. Qed.

  Lemma fast_path_affine_pure_or (P Q : Prop) : IntoOr (PROP := PROP) (<affine> ⌜P ∨ Q⌝)%I (<affine>⌜P⌝) (<affine>⌜Q⌝).
  Proof. red. eauto. Qed.

  Lemma fast_path_affine_pure_exist {A : Type} (Q : A → Prop) : IntoExistCareful (PROP := PROP) (<affine> ⌜∃ a, Q a⌝)%I (λ a, <affine> ⌜Q a⌝)%I.
  Proof. red. eauto. Qed.
End fast_path_instances.


Ltac2 infer_step_type (Δ : constr) (p : constr) (g : constr) : intro_hyp_step_type :=
  lazy_match! p with
  | bi_emp => IntroHypEmp
  | bi_sep ?q1 ?q2 => IntroHypSep '(fast_path_sep $q1 $q2)
  | bi_exist ?q => IntroHypEx '(fast_path_exist $q)
  | bi_or ?q1 ?q2 => IntroHypOr '(fast_path_or $q1 $q2)
  | bi_pure ?φ =>
    lazy_match! φ with
    | ?q1 ∧ ?q2 => IntroHypSep '(fast_path_pure_and $q1 $q2)
    | ?q1 ∨ ?q2 => IntroHypOr '(fast_path_pure_or $q1 $q2)
    | ex ?q => IntroHypEx '(fast_path_pure_exist $q)
    | _ => IntroHypPure '(fast_path_pure $φ)
    end
  | bi_affinely (bi_pure ?φ) =>
    lazy_match! φ with
    | ?q1 ∧ ?q2 => IntroHypSep '(fast_path_affine_pure_and $q1 $q2)
    | ?q1 ∨ ?q2 => IntroHypOr '(fast_path_affine_pure_or $q1 $q2)
    | ex ?q => IntroHypEx '(fast_path_affine_pure_exist $q)
    | _ => IntroHypPure '(fast_path_affine_pure $φ)
    end
  | _ =>
    plus_once_list
      [ Std.unify p ('bi_emp); IntroHypEmp
      | let witness := constr:(ltac:(tc_solve) : IntoSepCareful $p _ _) in
        IntroHypSep witness
      | let witness := constr:(ltac:(tc_solve) : IntoExistCareful $p _) in
        IntroHypEx witness
      | let witness := constr:(ltac:(tc_solve) : IntoOr $p _ _) in
        IntroHypOr witness
      | let m := open_constr:(_) in
        let p' := open_constr:(_) in
        let c := open_constr:(_) in
        let wit1 := constr:(ltac:(tc_solve) : IntoModal false $p $m $p') in
        let wit2 := constr:(ltac:(tc_solve) : DropModal $m $g $c) in
        let wit3 := constr:(ltac:(cbn beta; tc_solve) : $c $p') in
        IntroHypModal wit1 wit2 wit3
      | let witness := constr:(ltac:(tc_solve) : IntoPure $p _) in
        IntroHypPure witness
      | let c := open_constr:(_) in
        let p3 := open_constr:(_) in
        let witness := constr:(ltac:(tc_solve) :
          TCAnd (MergableConsume $p _ $c)
                (FindInExtendedContext $Δ (λ (p0 : bool) P4, $c p0 P4 $p3) _ _ _)
        ) in
        IntroHypMergeConsume witness
      | IntroHypMergeEnv]
  end.


Ltac2 introduceHypStep
    (state : automation_state.t)
    (gid : goal_id)
    (Δ : constr) (p : constr) (g : constr)
      : goal_id list :=
  let store := automation_state.get_rp_store_for state gid in

  match infer_step_type Δ p g with
  | IntroHypEmp => ltac1:(introduceHypEmp); [gid]
  | IntroHypSep wit => introduceHypSep wit; [gid]
  | IntroHypEx wit => introduceHypExist wit; [gid]
  | IntroHypOr wit =>
      introduceHypOr wit;
      let (g1, g2) := automation_state.split_goal state gid in
      [g1; g2]
  | IntroHypModal wit1 wit2 wit3 =>
      introduceHypModal wit1 wit2 wit3;
      [gid]
    (* rules below this do actual introduction of separation logic premises *)
  | IntroHypPure wit =>
      introduceHypPure wit store; (* this could kill the goal! *)
      ifcatch fail0 (fun _ => []) (fun _ => [gid])
  | IntroHypMergeConsume wit =>
      introduceHypMergeConsume wit;
      [gid]
  | IntroHypMergeEnv =>
    (* MergeEnv covers the case where no merging needs to be done, so this rule is no longer necessary.
       However, it would be faster than MergeEnv for hypotheses that dont need merging. *)
      ltac1:(introduceHypMergeEnv);
      [gid]
  end.

Ltac introduceHypStep :=
  ltac2:(
    lazy_match! goal with
    | [|- envs_entails ?Δ (IntroduceHyp ?p ?g)] =>
      let (state, gid) := automation_state.new () in
      let _ := introduceHypStep state gid Δ p g in
      ()
    end
  ).
