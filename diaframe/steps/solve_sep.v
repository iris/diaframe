From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import env_utils util_classes solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import pure_solver_utils automation_state ltac2_store reductions.
From diaframe.hint_search Require Import biabd_disj_from_path.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file contains 'case 5' of Diaframe's proof search strategy.
*)

Section solve_sep_rules.

  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma solve_sep_mod_weaker M1 M2 {TT} Q R :
    ModWeaker M1 M2 →
    SolveSep (TT := TT) M1 Q R ⊢ SolveSep (TT := TT) M2 Q R.
  Proof. by unseal_diaframe. Qed.

  Lemma solve_sep_id_emp (R : PROP) :
    R ⊢ SolveSep (TT := [tele]) id emp%I R.
  Proof. unseal_diaframe => /=. eauto. Qed.

  Lemma solve_sep_fromsep {TT : tele} M P R P1 P2 {TT1} P1' {TT2} h M1 M2 :
    (TC∀.. (tt : TT), FromSepCareful (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ExtractDep P1 TT1 P1' TT2 h →
    SplitLeftModality3 M M1 M2 →
    SolveSep M1 P1' $ 
      tele_bind (λ tt1, SolveSep M2 (tele_fun_compose' P2 (tele_appd_dep h tt1)) $ tele_fun_compose' R (tele_appd_dep h tt1)) 
    ⊢ SolveSep M P R.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromSepCareful /ExtractDep !tforall_forall => HP HP1 [HM HM1 HM2].
    rewrite -HM => {HM}.
    apply HM1 => {HM1}.
    apply bi_texist_elim => tt1.
    move: HP1 => /(dep_eval tt1) /tforall_forall HP1.
    rewrite tele_app_bind modality_strong_frame_r.
    apply modality_strong_is_mono => {HM2}.
    apply wand_elim_l', bi_texist_elim => tt2.
    apply wand_intro_l.
    rewrite !tele_fun_compose_eq' assoc.
    erewrite HP1, HP.
    by rewrite bi_texist_exist -(exist_intro _).
  Qed.

  Lemma solve_sep_fromexist_nested {TT1 : tele} {TT2} M P P' R :
    (∀.. (tt : TT1), FromTExist (tele_app P tt) (tele_app TT2 tt) (tele_appd_dep P' tt)) →
    ModalityMono M →
    SolveSep (TT := TeleConcatType TT1 TT2) M (tele_curry P') (tele_curry $ tele_dep_bind (λ tt1, tele_bind (λ tt2, tele_app R tt1))) 
    ⊢ SolveSep (TT := TT1) M P R.
  Proof.
    unseal_diaframe => /tforall_forall HPP' HM /=.
    apply HM, bi_texist_elim => tt.
    rewrite (tele_arg_concat_inv_eq tt).
    destruct (tele_arg_concat_inv_strong_hd tt) as [tt1 tt2].
    rewrite bi_texist_exist -(exist_intro tt1).
    rewrite -HPP' !bi_texist_exist.
    rewrite -(exist_intro tt2).
    rewrite tele_uncurry_curry_compose.
    rewrite -tele_curry_uncurry_relation.
    apply sep_mono_r.
    rewrite tele_curry_uncurry_relation.
    rewrite -tele_uncurry_curry_compose.
    rewrite tele_dep_appd_bind.
    by rewrite tele_app_bind.
  Qed.

  Lemma solve_sep_biabd {TTl TTr} M P2 R S T M2 p P1 M1 :
    BiAbd (TTl := TTl) (TTr := TTr) p P1 P2 M2 S T →
    SplitModality3 M M1 M2 →
    □?p P1 ∗ SolveSep (TT := TTl) M1 S $ tele_bind (λ ttl,
      IntroduceVars (TT := TTr) $ tele_bind (λ ttr,
        IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R ttr)
    )) ⊢ SolveSep (TT := TTr) M P2 R.
  Proof.
    unseal_diaframe; rewrite /BiAbd => /tforall_forall HP1P2 [HM HM1 HM2].
    rewrite -HM modality_strong_frame_r.
    apply HM1 => {HM1} /=.
    apply wand_elim_l', bi_texist_elim => ttl.
    apply wand_intro_l.
    rewrite tele_app_bind sep_assoc HP1P2 modality_strong_frame_l.
    apply HM2 => {HM2} /=.
    apply wand_elim_l', bi_texist_elim => ttr.
    apply wand_intro_r.
    rewrite (bi_tforall_elim ttr) !tele_app_bind bi_texist_exist -(exist_intro ttr).
    by rewrite -assoc wand_elim_r.
  Qed.

  Lemma solve_sep_biabd_disj {TTl TTr} M P2 R S T M2 p P1 M1 M' Mr D R' :
    SplitLeftModality3 M M' Mr →
    BiAbdDisj (TTl := TTl) (TTr := TTr) p P1 P2 M2 S T D →
    SplitModality3 M' M1 M2 →
    (TC∀.. (ttr : TTr), AbsorbModal (tele_app R ttr) Mr (tele_app R' ttr)) →
    □?p P1 ∗ SolveSep (TT := TTl) M1 S $ tele_bind (λ ttl, 
      match tele_app D ttl with
      | Some2 D' =>
          SolveAnd (
            IntroduceVars (TT := TTr) $ tele_bind $ λ ttr,
              IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
          ) $ IntroduceHyp D' $ SolveSep (TT := TTr) Mr P2 R
      | None2 => IntroduceVars (TT := TTr) $ tele_bind $ λ ttr,
              IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
      end
    ) ⊢ SolveSep (TT := TTr) M P2 R.
  Proof.
    unseal_diaframe; rewrite /BiAbdDisj /TCTForall => - [HMM' HM' HMr] /tforall_forall HP1P2 [HM'' HM1 HM2] /tforall_forall HRR'.
    rewrite -HMM' -HM'' modality_strong_frame_r.
    apply HM1 => {HM1} /=.
    apply wand_elim_l', bi_texist_elim => ttl.
    apply wand_intro_l.
    rewrite tele_app_bind sep_assoc HP1P2 modality_strong_frame_l.
    apply HM2 => {HM2} /=.
    destruct (tele_app D ttl) as [D'|].
    - apply wand_elim_l', or_elim, wand_intro_r.
      * apply bi_texist_elim => ttr.
        apply wand_intro_r.
        rewrite and_elim_l (bi_tforall_elim ttr) !tele_app_bind.
        rewrite -assoc wand_elim_r.
        rewrite HRR' modality_strong_frame_r.
        apply HMr.
        rewrite bi_texist_exist -bi.exist_intro comm //.
      * rewrite and_elim_r wand_elim_r //.
    - rewrite right_id. 
      apply wand_elim_l', bi_texist_elim => ttr.
      apply wand_intro_r.
      rewrite (bi_tforall_elim ttr) !tele_app_bind.
      rewrite -assoc wand_elim_r.
      rewrite HRR' modality_strong_frame_r.
      apply HMr.
      rewrite bi_texist_exist -bi.exist_intro comm //.
  Qed.

  Lemma solve_sep_from_or {TT : tele} D L R G M TT1 L' TT2 h G' TTr1 R' TTr2 h' G'' :
    (TC∀.. (tt : TT), FromOrCareful (tele_app D tt) (tele_app L tt) (tele_app R tt)) →
    ExtractDep L TT1 L' TT2 h →
    (TC∀.. (tt1 : TT1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h tt1)) (tele_app G' tt1)) →
    ModalityMono M →
    ExtractDep R TTr1 R' TTr2 h' →
    (TC∀.. (tt1 : TTr1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h' tt1)) (tele_app G'' tt1)) →
    SolveSepFoc (TT := TT1) M L' G' (∃.. (tt : TTr1), tele_app R' tt ∗ tele_app G'' tt) ⊢
    SolveSep (TT := TT) M D G.
  Proof.
    unseal_diaframe; rewrite /TCTForall => /tforall_forall HDLR HL /tforall_forall HG HM HR /tforall_forall HG''.
    apply HM, or_elim.
    - apply bi_texist_elim => tt1. rewrite HG.
      apply wand_elim_r', bi_texist_elim => tt2. apply wand_intro_l.
      revert HL. move => /(dep_eval_tele tt1) /(dep_eval_tele tt2) ->.
      rewrite bi_texist_exist -bi.exist_intro. rewrite tele_fun_compose_eq'.
      apply sep_mono_l. rewrite -HDLR. apply or_intro_l.
    - apply bi_texist_elim => tt1. rewrite HG''.
      apply wand_elim_r', bi_texist_elim => tt2. apply wand_intro_l.
      revert HR. move => /(dep_eval_tele tt1) /(dep_eval_tele tt2) ->.
      rewrite bi_texist_exist -bi.exist_intro. rewrite tele_fun_compose_eq'.
      apply sep_mono_l. rewrite -HDLR. apply or_intro_r.
  Qed.

  Lemma solve_sep_normalize {TT : tele} D G M D' TT1 D'' TT2 h G' :
    (TC∀.. (tt : TT), NormalizedProp (tele_app D tt) (tele_app D' tt) true) →
    ModalityMono M → (* only used if D is a disjunction *)
    ExtractDep D' TT1 D'' TT2 h →
    (TC∀.. (tt1 : TT1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h tt1)) (tele_app G' tt1)) →
    SolveSep (TT := TT1) M D'' G' ⊢
    SolveSep (TT := TT) M D G.
  Proof.
    unseal_diaframe; rewrite /TCTForall !tforall_forall => HD' HM HD'' HG.
    apply HM, bi_texist_elim => tt1.
    rewrite HG. apply wand_elim_r', bi_texist_elim => tt2. apply wand_intro_l.
    revert HD''. move => /(dep_eval_tele tt1) /(dep_eval_tele tt2) ->.
    rewrite bi_texist_exist -bi.exist_intro. rewrite tele_fun_compose_eq'.
    apply sep_mono_l. by rewrite -HD'.
  Qed.

End solve_sep_rules.

Section solve_sep_tacs.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma tac_solve_sep_emp Δ M R :
    IntroducableModality M →
    envs_entails Δ R →
    envs_entails Δ $ SolveSep (TT := [tele]) M emp%I R.
  Proof.
    rewrite envs_entails_unseal /= => HM ->.
    by rewrite -solve_sep_mod_weaker -solve_sep_id_emp.
  Qed.

  Lemma tac_solve_sep_fromsep {TT : tele} Δ M P R P1 P2 {TT1} P1' {TT2} h :
    (TC∀.. (tt : TT), FromSepCareful (tele_app P tt) (tele_app P1 tt) (tele_app P2 tt)) →
    ExtractDep P1 TT1 P1' TT2 h →
    ModalityStrongMono M →
    envs_entails Δ $ SolveSep M P1' $ 
      tele_bind (λ tt1, SolveSep id (tele_fun_compose' P2 (tele_appd_dep h tt1)) $ tele_fun_compose' R (tele_appd_dep h tt1)) →
    envs_entails Δ $ SolveSep M P R.
  Proof.
    rewrite envs_entails_unseal => HP1P2 HP1P1' HM ->.
    eapply solve_sep_fromsep; try done. split; try tc_solve. (* move => ? //=. split => ?? => //=.*)
  Qed.

  Lemma tac_solve_sep_fromexist_nested {TT1 : tele} Δ {TT2} M P P' R :
    (TC∀.. (tt : TT1), FromTExist (tele_app P tt) (tele_app TT2 tt) (tele_appd_dep P' tt)) →
    ModalityMono M →
    envs_entails Δ $ SolveSep (TT := TeleConcatType TT1 TT2) M (tele_curry P') 
                      (tele_curry $ tele_dep_bind (λ tt1, tele_bind (λ tt2, tele_app R tt1))) →
    envs_entails Δ $ SolveSep (TT := TT1) M P R.
  Proof.
    rewrite envs_entails_unseal => HPP' HM ->.
    by apply solve_sep_fromexist_nested.
  Qed.

  Lemma tac_solve_sep_biabd Δ i M {TTl TTr} P2 R S T M2 p P1 M1 :
    FindInContext Δ (λ p P, BiAbdMod (TTl := TTl) (TTr := TTr) p P P2 M M1 M2 S T) i p P1 →
    envs_entails (envs_delete false i p Δ) 
      (SolveSep (TT := TTl) M1 S (tele_bind $ λ ttl, IntroduceVars (TT := TTr) $ tele_bind (λ ttr, IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R ttr)))) →
    envs_entails Δ $ SolveSep (TT := TTr) M P2 R.
  Proof.
    rewrite envs_entails_unseal => HΔiP HΔ'.
    rewrite (findincontext_spec false) HΔ'.
    apply fic_satisfies in HΔiP as [HPST [HMs HM1 HM2]].
    erewrite solve_sep_biabd => //.
  Qed.

  Lemma tac_solve_sep_ext_biabd Δ mi M {TTl TTr} P2 R S T M2 p P1 M1  :
    FindInExtendedContext Δ (λ p P, BiAbdMod (TTl := TTl) (TTr := TTr) p P P2 M M1 M2 S T) mi p P1 →
    envs_entails (envs_option_delete false mi p Δ)
      (SolveSep (TT := TTl) M1 S (tele_bind $ λ ttl, IntroduceVars (TT := TTr) $ tele_bind (λ ttr, IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R ttr)))) →
    envs_entails Δ $ SolveSep (TT := TTr) M P2 R.
  Proof.
    rewrite envs_entails_unseal => HΔiP HΔ'.
    rewrite (findinextcontext_spec false) // HΔ'.
    apply ficext_satisfies in HΔiP as [HPST [HMs HM1 HM2]].
    erewrite solve_sep_biabd => //.
  Qed.

  Lemma tac_solve_sep_pure_intro Δ {TT : tele} b M P φ Q :
    (TC∀.. tt : TT, FromPure b (tele_app P tt) (tele_app φ tt)) →
    IntroducableModality M →
    (∃.. tt, tele_app φ tt ∧ envs_entails Δ (tele_app Q tt)) →
    envs_entails Δ $ SolveSep (TT := TT) M P Q.
  Proof.
    rewrite envs_entails_unseal texist_exist => /tforall_forall HPφ HM [tt [Hφ ->]].
    unseal_diaframe; rewrite -HM /= bi_texist_exist -(exist_intro tt) -HPφ /= pure_True //=.
    destruct b => /=;
    iIntros "HQ";
    by iSplitR.
  Qed.

  Lemma tac_solve_sep_pure_intro_2 Δ {TT : tele} b M P φ Q :
    (TC∀.. tt : TT, FromPure b (tele_app P tt) (tele_app φ tt)) →
    IntroducableModality M →
      (* The user might have run iStep with Δ not simplified.
        In that case, it is better to display the original entailment, 
        so that the user can still solve it. *)
    (∃.. tt, envs_entails Δ ⌜(tele_app φ tt)⌝ ∧ envs_entails Δ (tele_app Q tt)) →
    envs_entails Δ $ SolveSep (TT := TT) M P Q.
  Proof.
    rewrite envs_entails_unseal texist_exist => /tforall_forall HPφ HM [tt [Hφ HQ]].
    rewrite -(and_idem (of_envs Δ)) {1}Hφ {Hφ} HQ.
    apply pure_elim_l => Hφ.
    unseal_diaframe; rewrite -HM /= bi_texist_exist -(exist_intro tt) -HPφ /= pure_True //=.
    destruct b => /=;
    iIntros "HQ";
    by iSplitR.
  Qed.

  (* this one is necessary since |={A,B}=> P ∗ (Q -∗ R) now becomes 
      ‖ |={A,B}=> ‖ P (∗) (Q -∗ R)  since the wand cannot do anything with fupd *)
  Lemma tac_solve_sep_pure_non_intro Δ {TT : tele} b M P φ Q Q' :
    (TC∀.. tt : TT, FromPure b (tele_app P tt) (tele_app φ tt)) →
    ModalityMono M → 
    (TC∀.. tt : TT, AbsorbModal (tele_app Q tt) M (tele_app Q' tt)) →
    (∃.. tt, tele_app φ tt ∧ envs_entails Δ (tele_app Q' tt)) →
    envs_entails Δ $ SolveSep (TT := TT) M P Q.
  Proof.
    rewrite envs_entails_unseal texist_exist /TCTForall !tforall_forall => HPφ HM HQ [tt [Hφ ->]].
    unseal_diaframe; rewrite HQ; apply HM. 
    rewrite /= bi_texist_exist -(exist_intro tt) -HPφ /= pure_True //=.
    destruct b => /=;
    iIntros "HQ";
    by iSplitR.
  Qed.

  (* this one is necessary since |={A,B}=> P ∗ (Q -∗ R) now becomes 
      ‖ |={A,B}=> ‖ P (∗) (Q -∗ R)  since the wand cannot do anything with fupd *)
  Lemma tac_solve_sep_pure_non_intro_2 Δ {TT : tele} b M P φ Q Q' :
    (TC∀.. tt : TT, FromPure b (tele_app P tt) (tele_app φ tt)) →
    ModalityMono M → 
    (TC∀.. tt : TT, AbsorbModal (tele_app Q tt) M (tele_app Q' tt)) →
    (* we abstract out the environment Δ so that we can reduce the telescopes without affecting Δ.
      Then we wrap this in [id] so that Coq does not β-reduce.. See also
    https://stackoverflow.com/questions/46622936/is-there-a-good-way-to-block-beta-reduction-from-automatically-occurring-in-goal *)
    (my_id $ λ Δ', ∃.. tt, envs_entails Δ' ⌜BehindModal M (tele_app φ tt)⌝ ∧ envs_entails Δ' (tele_app Q' tt)) Δ →
    envs_entails Δ $ SolveSep (TT := TT) M P Q.
  Proof.
    cbn. rewrite envs_entails_unseal texist_exist /TCTForall /BehindModal !tforall_forall => HPφ HM HQ [tt [Hφ HQ']].
    rewrite -(and_idem (of_envs Δ)) {1}Hφ {Hφ} HQ'.
    apply pure_elim_l => Hφ.
    unseal_diaframe; rewrite HQ; apply HM. 
    rewrite /= bi_texist_exist -(exist_intro tt) -HPφ /= pure_True //=.
    destruct b => /=;
    iIntros "HQ";
    by iSplitR.
  Qed.

  Lemma tac_solve_sep_ext_biabd_disj Δ mi M {TTl TTr} P2 R S T M2 p P1 M1 M' Mr D R' :
    SplitLeftModality3 M M' Mr →
    FindInExtendedContext Δ (λ p P, BiAbdDisjMod (TTl := TTl) (TTr := TTr) p P P2 M' M1 M2 S T D) mi p P1 →
    (TC∀.. (ttr : TTr), AbsorbModal (tele_app R ttr) Mr (tele_app R' ttr)) →
    envs_entails (envs_option_delete false mi p Δ)
      (SolveSep (TT := TTl) M1 S $ tele_bind (λ ttl, 
        match tele_app D ttl with
        | Some2 D' =>
            SolveAnd (
              IntroduceVars (TT := TTr) $ tele_bind $ λ ttr, 
                IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
            ) $ IntroduceHyp D' $ SolveSep (TT := TTr) Mr P2 R
        | None2 => IntroduceVars (TT := TTr) $ tele_bind $ λ ttr, 
                IntroduceHyp (tele_app (tele_app T ttl) ttr) (tele_app R' ttr)
        end
    )) →
    envs_entails Δ $ SolveSep (TT := TTr) M P2 R.
  Proof.
    rewrite envs_entails_unseal => HMM' HΔiP HR HΔ'.
    rewrite (findinextcontext_spec false) // HΔ'.
    apply ficext_satisfies in HΔiP as [HPST [HMs HM1 HM2]].
    erewrite solve_sep_biabd_disj => // {HΔ'}.
  Qed.

  Lemma tac_solve_sep_from_or Δ {TT : tele} D L R G M TT1 L' TT2 h G' TTr1 R' TTr2 h' G'' :
    (TC∀.. (tt : TT), FromOrCareful (tele_app D tt) (tele_app L tt) (tele_app R tt)) →
    ExtractDep L TT1 L' TT2 h →
    (TC∀.. (tt1 : TT1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h tt1)) (tele_app G' tt1)) →
    ModalityMono M →
    ExtractDep R TTr1 R' TTr2 h' →
    (TC∀.. (tt1 : TTr1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h' tt1)) (tele_app G'' tt1)) →
    envs_entails Δ (SolveSepFoc (TT := TT1) M L' G' (∃.. (tt : TTr1), tele_app R' tt ∗ tele_app G'' tt)%I) →
    envs_entails Δ $ SolveSep (TT := TT) M D G.
  Proof.
    rewrite envs_entails_unseal => HD HL HG HM HR HG' ->.
    by eapply solve_sep_from_or.
  Qed.

  Lemma tac_solve_sep_normalize_or Δ {TT : tele} D G M D' TT1 D'' TT2 h G' :
    (* Only used if D is a disjunction *)
    (TC∀.. (tt : TT), NormalizedProp (tele_app D tt) (tele_app D' tt) true) →
    ModalityMono M →
    ExtractDep D' TT1 D'' TT2 h →
    (TC∀.. (tt1 : TT1), AbsorbExists (tele_fun_compose' G (tele_appd_dep h tt1)) (tele_app G' tt1)) →
    envs_entails Δ (SolveSep (TT := TT1) M D'' G') →
    envs_entails Δ $ SolveSep (TT := TT) M D G.
  Proof.
    rewrite envs_entails_unseal => HD' HM HD'' HG' ->.
    by eapply solve_sep_normalize.
  Qed.

End solve_sep_tacs.


Ltac2 Type solve_sep_step_type := [
  | SolveSepExist (constr)
  | SolveSepSep (constr)
  | SolveSepPure (constr)
  | SolveSepNormalizeOr (constr)
  | SolveSepOr (constr)
  | SolveSepBiAbd
].


Section fast_path_instances.
  Context {PROP : bi}.

  (* This instance would never work for typeclass search. But it works in combination with
    [tele_utils]' [get_repeated_quantifier_function] *)
  Lemma fast_path_fromtexist (TT : tele@{bi.Quant}) (P : TT -t> PROP) : FromTExist (bi_texist (tele_app P)) TT P.
  Proof. by red. Qed.

  Lemma fast_path_from_pureexist {A : Type} (P : A → Prop) : FromTExist (PROP := PROP) (⌜ex P⌝%I) [tele_pair A] (fun a => ⌜P a⌝%I).
  Proof. red. eauto. Qed.

  Lemma fast_path_fromsepcareful (P Q : PROP) : FromSepCareful (P ∗ Q) P Q.
  Proof. by red. Qed.

  Lemma fast_path_from_pureand (P Q : Prop) : FromSepCareful (PROP := PROP) ⌜P ∧ Q⌝%I ⌜P⌝%I ⌜Q⌝%I.
  Proof. red. eauto. Qed.

  Lemma fast_path_frompure (P : Prop) : FromPure (PROP := PROP) false ⌜P⌝%I P.
  Proof. by red. Qed.

  Lemma fast_path_emp_frompure : FromPure (PROP := PROP) true emp%I True.
  Proof. red. simpl. eauto. Qed.

  Lemma fast_path_fromorcareful (P Q : PROP) : FromOrCareful (P ∨ Q) P Q.
  Proof. by red. Qed.
End fast_path_instances.

Ltac2 fast_path_fromtexist (_ : unit) :=
  (* does something smart to get all the repeated existentials in one go. *)
  lazy_match! goal with
  | [|- FromTExist (bi_exist ?input) _ _] =>
    let (output, output_tele) := get_repeated_quantifier_function input in
    exact (fast_path_fromtexist $output_tele $output)
  end.


Ltac2 tc_apply0 (c : constr) : unit :=
  let f := ltac1:(i |- autoapply i with typeclass_instances) in
  f (Ltac1.of_constr c).

Ltac2 Notation "tc_apply" c(constr) := tc_apply0 c.


(* Note:
  Actually passing around the witnesses of succesful typeclass search seems to incur a 1% performance cost:
https://coq-speed.mpi-sws.org/d/Ne7jkX6kk/coq-speed?orgId=1&var-metric=instructions&var-project=diaframe&var-branch=ci%2Fdev-ike&var-config=All&var-group=%28%29.%2A&from=1710239293000&to=1710439018000
  This performance cost is paid off by establishing these witnesses faster (1.3% performance gain),
  but it means that launching tactic mode to build a constr is costly.
  In other words, one should avoid the [let f := constr:(ltac2:(tac) : type)] pattern. *)
Ltac2 infer_step_type (tt : constr) (p : constr) : solve_sep_step_type :=
  (* all queries are under telescopes, so under TCTForall *)
  let tc_forall_assert_type (q : constr) : constr :=
    '(@TCTForall $tt $q) in
  let tcforall_intros (_ : unit) : unit :=
    ltac1:(simpl_diaframe_tele_tcforall; intros)
  in
  (* convenience tactic where one only specifies the tactic after telescope reduction *)
  let build_tcforall (tac : unit -> unit) (term : constr) : constr :=
    let witness_type := tc_forall_assert_type term in
    '(ltac2:(tcforall_intros (); tac ()) : $witness_type)
  in
  (* checks if a disjunction can be further normalized *)
  let choose_or (or_wit : constr) :=
    plus_once_list 
      [ let p' := open_constr:(_) in
        let wit := build_tcforall tc_solve '(fun t => NormalizedProp (tele_app (TT := $tt) $p t) (tele_app $p' t) true) in
        SolveSepNormalizeOr wit
      | SolveSepOr or_wit ]
  in
  (* constructs premise for ∃ case *)
  let exist_premise (tac : unit -> unit) : constr :=
    let tt2 := open_constr:(_) in
    let p2 := open_constr:(_) in
    build_tcforall tac '(fun t => FromTExist (tele_app (TT := $tt) $p t) (tele_app $tt2 t) (tele_appd_dep $p2 t))
  in
  (* constructs premise for ∗ case *)
  let sep_premise (tac : unit -> unit) : constr :=
    let p1 := open_constr:(_) in
    let p2 := open_constr:(_) in
    build_tcforall tac '(fun t => FromSepCareful (tele_app (TT := $tt) $p t) (tele_app $p1 t) (tele_app $p2 t))
  in
  (* constructs premise for ⌜⌝ case *)
  let pure_premise (tac : unit -> unit) : constr :=
    let b := open_constr:(_) in
    let φ := open_constr:(_) in
    build_tcforall tac '(fun t => FromPure $b (tele_app (TT := $tt) $p t) (tele_app $φ t))
  in
  (* constructs premise for ∨ case *)
  let or_premise (tac : unit -> unit) : constr :=
    let p1 := open_constr:(_) in
    let p2 := open_constr:(_) in
    build_tcforall tac '(fun t => FromOrCareful (tele_app (TT := $tt) $p t) (tele_app $p1 t) (tele_app $p2 t))
  in

  (* now the actual program: *)
  let (head, args, bs) := get_head_term_n_args p in
  lazy_match! head with
  | @bi_exist _ =>
    let input := List.nth args 1 in
    let (output, output_tele) := get_repeated_quantifier_function input in
    SolveSepExist (rebind (safe_apply 'fast_path_fromtexist [output_tele; output]) bs)
  | @bi_sep _  =>
    let left_c := List.nth args 0 in
    let right_c := List.nth args 1 in
    SolveSepSep (rebind (safe_apply 'fast_path_fromsepcareful [left_c; right_c]) bs)
  | @bi_pure ?proptype =>
    let phi := (List.hd args) in
    lazy_match! phi with
    | ex ?phi_ex => SolveSepExist (rebind (safe_apply 'fast_path_from_pureexist [phi_ex]) bs)
    | and ?lphi ?rphi => SolveSepSep (rebind (safe_apply 'fast_path_from_pureand [lphi; rphi]) bs)
    | _ => SolveSepPure (rebind (safe_apply '@fast_path_frompure [proptype; phi]) bs)
    end
  | @bi_or ?proptype =>
    let left_c := List.nth args 0 in
    let right_c := List.nth args 1 in
    choose_or (rebind (safe_apply '@fast_path_fromorcareful [proptype; left_c; right_c]) bs)
  | @bi_emp ?proptype => SolveSepPure (rebind '(@fast_path_emp_frompure $proptype) bs)
  | _ =>
    plus_once_list 
      [ SolveSepExist (exist_premise tc_solve)
      | SolveSepSep (sep_premise tc_solve)
      | SolveSepPure (pure_premise tc_solve)
      | choose_or (or_premise tc_solve)
      | SolveSepBiAbd]
  end.

Ltac2 solveSepExist (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- simple notypeclasses refine (tac_solve_sep_fromexist_nested (TT1 := tt) _ _ _ _ _ wit _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [ tc_solve () | (* FIXME *) ltac1:(simpl_diaframe_tele)].

Ltac2 solveSepSep (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- notypeclasses refine (tac_solve_sep_fromsep (TT := tt) _ _ _ _ _ _ _ _ wit _ _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [tc_solve ().. | (* FIXME *) ltac1:(simpl_diaframe_tele)].

Ltac2 solveSepPure (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- notypeclasses refine (tac_solve_sep_pure_non_intro_2 (TT := tt) _ _ _ _ _ _ _ wit _ _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [tc_solve ().. | (* FIXME *) ltac1:(simpl_diaframe_pure_tele)].
    (* remaining goal after this is:
         (∃.. (tt : TT), (Δ ⊢ ⌜BehindModal (φ tt)⌝) ∧ (Δ ⊢ G))
        Wanted behavior in various cases:
          tt = TeleO:
            φ cannot be solved: shelve goal, and unfold BehindModal
          tt ≠ TeleO:
            φ cannot be solved: revert, and present user with (Δ ⊢ SolveSep (TT := TT) ⌜φ⌝ G)
              possibly complain that we cannot determine a good witness *)

Ltac2 solveSepNormalize (witness : constr) :=
  ltac1:(wit |- notypeclasses refine (tac_solve_sep_normalize_or _ _ _ _ _ _ _ _ _ _ wit _ _ _ _)) (Ltac1.of_constr witness) >
    [tc_solve ().. | (* FIXME *) ltac1:(simpl_diaframe_tele)].

Ltac2 solveSepOr (tt : constr) (witness : constr) :=
  ltac1:(tt wit |- notypeclasses refine (tac_solve_sep_from_or (TT := tt) _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ wit _ _ _ _ _ _)) (Ltac1.of_constr tt) (Ltac1.of_constr witness) >
    [tc_solve ().. | (* FIXME *) ltac1:(simpl_diaframe_tele)].

Ltac2 solveSepBiAbd (state : automation_state.t) (gid : goal_id) :=
  ltac1:(notypeclasses refine (tac_solve_sep_ext_biabd_disj _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)) >
    [tc_solve () | | | ] >
    [find_biabd_disj_in_envs_on_goal (automation_state.get_rp_store_for state gid) | | ] >
    [ tc_solve () | ];
    handle_prop_deletion_on_goal state gid;
    ltac1:(simpl_diaframe_post_hint).

Ltac2 solveSepStep (pure_solve_result_handler : pure_solver_result -> bool)
    (state : automation_state.t) (gid : goal_id)
    (Δ : constr) (tt : constr) (m : constr) (p : constr) (q : constr) : goal_id list :=
  (* solveSepStep is used when the goal has shape (L ∗ G), possibly behind existential quantifiers and a modality.
    We will refer to L as the left-most goal. We distinguish the following cases:
    - left-most goal has shape (∃ L)
    - left-most goal has shape (L1 ∗ L2)
    - left-most goal has shape (⌜L⌝) (a pure proposition)
    - left-most goal has shape (L1 ∨ L2).
        Here, we first try to simplify both sides ('normalize') - i.e. if one branch is contradictory, we drop it.
        If after normalization this is still a disjunction, we have automation for that with SolveSepFoc
    - left-most goal is an atom A: try to find a connection *)
  let pure_post_process (_ : unit) :=
    plus_once (fun _ =>
      progress (try_solve_pure_on (fun c => ltac1:(repeat_eexists); split > [c () | ]) pure_solve_result_handler)
    ) (fun _ =>
      (* should solve this pure goal, but cannot *)
      Control.zero Assertion_failure
    )
  in
  match infer_step_type tt p with
  | SolveSepExist wit => solveSepExist tt wit
  | SolveSepSep wit => solveSepSep tt wit
  | SolveSepPure wit => solveSepPure tt wit; pure_post_process ()
  | SolveSepNormalizeOr wit => solveSepNormalize wit
  | SolveSepOr wit => solveSepOr tt wit
  | SolveSepBiAbd => solveSepBiAbd state gid
  end;
  [gid].

Ltac solveSepStep :=
  ltac2:(
    lazy_match! goal with
    | [|- envs_entails ?Δ (@SolveSep _ ?tt ?m ?p ?q)] =>
      let (state, gid) := automation_state.new () in
      let _ := solveSepStep handle__shelve_unsolved_noevars state gid Δ tt m p q in
      ()
    end
  ).



















