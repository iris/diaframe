From diaframe Require Export utils_ltac2.
From diaframe.steps Require Import ipm_hyp_utils.
From iris.proofmode Require Import proofmode environments string_ident intro_patterns.

Import intro_pat.


Ltac2 ipm_hyp_renamer 
  (register_closure : (unit -> unit) -> unit)
  (initial_names : ipm_ident list)
  (new_names : constr list) : unit -> unit :=
  fun _ =>
    let current_names := get_ipm_hyp_names () in

    let to_rename := List.filter (fun pr => 
      let (i, _) := pr in
      Bool.neg (List.mem ipm_ident_equal i initial_names)
    ) current_names in

    let new_names_len := List.length new_names in
    let to_rename_len := List.length to_rename in
    let too_many_names := Int.gt new_names_len to_rename_len in
    let too_little_names := Int.lt new_names_len to_rename_len in

    (* rename fresh IPM hyps as Hs, by repeatedly calling iDestructHyp *)
    List.iter (fun pr =>
      let (leftel, j) := pr in
      let (i, _) := leftel in
      let h1 := Ltac1.of_constr (ipm_ident_to_constr i) in
      let h2 := Ltac1.of_constr j in
      ltac1:(H1 H2 |- 
        once (
          iDestructHyp H1 as H2 
        + (idtac "Renaming"H1"into"H2"failed! Is"H2"already taken?"))
      ) h1 h2
    ) (zip to_rename new_names);

    match too_many_names with
    | true =>
      register_closure (fun _ => printf "Too many IPM names were supplied!");
      (* we cannot print/access [new_names] inside register_closure, since that will be executed in possibly a different goal
        so we must precompute the list of messages here, and let it get printed inside [register_closure] *)
      let ms : message list := (List.map (fun c => fprintf "%t, " c) (List.skipn to_rename_len new_names)) in
      register_closure (fun _ =>
        printf "Please remove %a" (fun _ _ => fold_right Message.concat ms (fprintf "")) ()
      )
    | _ => 
      match too_little_names with
      | true =>
        register_closure (fun _ => printf "Too little IPM names were supplied! Please supply a name for:");
        List.iter (fun (i, p) =>
          let msg := fprintf "  %t : %t" (ipm_ident_to_constr i) p in
          register_closure (fun _ => Message.print msg)
        ) (List.skipn new_names_len to_rename)
      | _ => ()
      end
    end.


Ltac2 rename_ipm_hyps_after_base 
  (prob : NamingProblems)
  (the_tac : unit -> unit) (* thunked tactic to run *)
  (raw_new_names_list : constr list list) :=
  (* make a snapshot of current IPM hyp names, as a list *)
  let initial_names : ipm_ident list := List.map (fun pr => let (i, _ ) := pr in i) (get_ipm_hyp_names ()) in

  (* run the_tac *)
  the_tac ();

  initialize_subgoal_problems prob;

  let new_names_list := to_list_of_size raw_new_names_list (count_goals ()) []
      (fun i => add_global_problem prob (fun _ => 
        printf "Too little branches of IPM names supplied! Missing %i branches" i 
      ))
      (fun i => add_global_problem prob (fun _ =>
        printf "Too many branches of IPM names supplied! Dropping %i branches" i
      ))
  in

  (* need to use Control.enter to force application of below to single goals. 
      If no goals remain, this is essentially unit. *)
  Control.dispatch (List.mapi (fun i => ipm_hyp_renamer (add_subgoal_problem prob i) initial_names) new_names_list).

(* mirroring Iris's intro_patterns.parse, which is in Ltac *)
Ltac2 parse_intro_pats (s : constr) : constr list :=
  lazy_match! (Constr.type s) with
  | list intro_pat => coq_list_to_list s
  | intro_pat => [s]
  | list string =>
     lazy_match! (eval vm_compute in (mjoin <$> mapM parse $s)) with
     | Some ?pats => coq_list_to_list pats
     | _ => Control.backtrack_tactic_failure 
      "parse_intro_pats: cannot parse argument as introduction pattern"
     end
  | string =>
     lazy_match! eval vm_compute in (parse $s) with
     | Some ?pats => coq_list_to_list pats
     | _ => Control.backtrack_tactic_failure "parse_intro_pats: cannot parse argument as introduction pattern"
     end
  | ident => [constr:(IIdent $s)]
  | ?x => Control.backtrack_tactic_failure "parse_intro_pats: the argument
     is expected to be an introduction pattern
     (usually a string),
     but received unexpected type"
  end.

Ltac2 ltac1_constr_string_to_pat_list (c : Ltac1.t) : constr list :=
  let ltac2_constr := Option.get (Ltac1.to_constr c) in
  parse_intro_pats ltac2_constr.

Ltac2 rename_multi_ipm_hyps_after_closure 
  (prob : NamingProblems)
  (new_names_list_raw : Ltac1.t)
  (the_tac : unit -> unit) :=
  rename_ipm_hyps_after_base prob the_tac (List.map ltac1_constr_string_to_pat_list (Option.get (Ltac1.to_list new_names_list_raw))).

Ltac2 rename_ipm_hyps_after_closure 
  (prob : NamingProblems)
  (new_names_raw : Ltac1.t)
  (the_tac : unit -> unit) :=
  rename_ipm_hyps_after_base prob the_tac [ltac1_constr_string_to_pat_list new_names_raw].


