From iris Require Import options.
From iris.algebra Require Import numbers auth lib.frac_auth.
From iris.base_logic Require Import iprop.
From stdpp Require Import namespaces.

From diaframe Require Import util_classes solve_defs.

(* This file defines the 'pure solver' that is used to handle cases 4b and 5a of 
   Diaframe's proof search strategy. Additionally, it can be called during typeclass search,
   for goals of the form `SolveSepSideCondition φ` *)


(* To extend the pure_solver:
   - for equalities: add hints to the [solve_pure_eq_add] database
   - for other goals: add hints to the [solve_pure_add] database *)

Require Import ZifyClasses.

Lemma of_pos_to_pos_eq : forall z, Zpos (Z.to_pos z) = Z.max 1 z.
Proof.
  intros x; destruct x.
  - reflexivity.
  - apply eq_sym, Zpos_max_1.
  - reflexivity.
Qed.

Global Instance Op_Z_to_pos : UnOp Z.to_pos := { TUOp := (λ z, 1 `max` z)%Z ; TUOpInj := of_pos_to_pos_eq }.
Add Zify UnOp Op_Z_to_pos.

(* missing from std library. TODO: Coq PR ? *)

Lemma dneg_involutive (Q : Prop) : Q → ¬¬ Q.
Proof. move => HQ HNQ. by apply HNQ. Qed.


Lemma z_of_nat_equality (z : Z) (n : nat) :
  (0 ≤ z)%Z →
  (Z.to_nat z = n) →
  (Z.of_nat n = z).
Proof. lia. Qed.

Lemma z_pos_equality (z : Z) (n : positive) :
  (0 < z)%Z →
  (Z.to_pos z = n) →
  (Z.pos n = z).
Proof. lia. Qed.

Lemma z_plus_eq_remove_r (a b z : Z) :
  (a = z - b)%Z →
  (a + b = z)%Z.
Proof. lia. Qed.

Lemma z_plus_eq_remove_l (a b z : Z) :
  (b = z - a)%Z →
  (a + b = z)%Z.
Proof. lia. Qed.


(* Boost Zify to do some modular arithmetic. Only really works with non-symbolic divisors.
More info here https://coq.inria.fr/refman/addendum/micromega.html#coq:tacn.zify *)
Ltac Zify.zify_post_hook ::= Z.to_euclidean_division_equations.

Open Scope Z_scope.

Lemma mult_mod_compat_left_evar_nz (x y z : Z) :
  z ≠ 0 →
  y `mod` z = 0 →
  x = y `div` z →
  x * z = y.
Proof. lia. Qed.

Lemma mult_mod_compat_left_evar_z (x y z : Z) :
  z = 0 → y = 0 →
  x * z = y.
Proof. lia. Qed.

Lemma mult_mod_compat_right_evar_nz (x y z : Z) :
  z ≠ 0 →
  y `mod` z = 0 →
  x = y `div` z →
  z * x = y.
Proof. lia. Qed.

Lemma mult_mod_compat_right_evar_z (x y z : Z) :
  z = 0 → y = 0 →
  z * x = y.
Proof. lia. Qed.

Close Scope Z_scope.

Ltac solveZEq := 
  lazymatch goal with
  | |- ?l = ?r =>
    first 
    [has_evar r; has_evar l; fail 3 "Cannot solve Z equation"l"="r": contains multiple evars"
    |has_evar r; apply eq_sym; solveZEq
    |is_evar l; reflexivity
    |has_evar l; 
      lazymatch l with
      | Z.of_nat ?n => is_evar n; refine (z_of_nat_equality _ _ _ _); [lia | reflexivity]
      | Z.pos ?p => is_evar p; refine (z_pos_equality _ _ _ _); [lia | reflexivity]
      | (?a + ?b)%Z =>
        first
        [has_evar a; has_evar b; fail 3 "Cannot solve Z equation"l"="r": contains multiple evars"
        |has_evar a; refine (z_plus_eq_remove_r _ _ _ _); solveZEq
        |has_evar b; refine (z_plus_eq_remove_l _ _ _ _); solveZEq ]
      | (?a * ?b)%Z =>
        first
        [has_evar a; has_evar b; fail 3 "Cannot solve Z equation"l"="r": contains multiple evars"
        |has_evar a;
          first 
          [ apply mult_mod_compat_left_evar_nz; [lia | lia | solveZEq ]
          | apply mult_mod_compat_left_evar_z; lia 
          | fail 4 "Cannot solve Z equation"l"="r": multiplier"b" is not definitely non-zero"]
        |has_evar b; 
          first 
          [ apply mult_mod_compat_right_evar_nz; [lia | lia | solveZEq ]
          | apply mult_mod_compat_right_evar_z; lia 
          | fail 4 "Cannot solve Z equation"l"="r": multiplier"a" is not definitely non-zero"] ]
      end
    |lia; fail 3"Lia cannot solve this equation?"]
  end.



Lemma pair_fst_instantiate {A B : Type} pr (a : A) (b : B) :
    pr = (a, b) → pr.1 = a.
Proof. by move => ->. Qed.

Lemma pair_snd_instantiate {A B : Type} pr (a : A) (b : B) :
    pr = (a, b) → pr.2 = b.
Proof. by move => ->. Qed.

Ltac trySolvePure := fail.

Ltac bool_solver :=
  repeat (progress (
    lazymatch goal with
    | |- @eq bool ?b1 ?b2 =>
      let both := constr:(andb b1 b2) in
      match both with
      | context [?term] =>
        (is_constructor term; fail 1) || (
        lazymatch term with
        | negb _ => fail
        | orb _ _ => fail
        | andb _ _ => fail
        | _ =>
          let term_type := type of term in
          unify term_type bool;
          destruct term
        end)
      end
    end)
  ); try discriminate; reflexivity.


Lemma pair_eq_from_proj_eqs {A B : Type} (pr1 pr2 : A * B) :
  pr1.1 = pr2.1 →
  pr1.2 = pr2.2 →
  pr1 = pr2.
Proof. destruct pr1; destruct pr2; naive_solver. Qed.


Create HintDb solve_pure_eq_add.

Ltac trySolvePureEq :=
  reflexivity || eassumption || (
    lazymatch goal with
    | |- @eq ?T ?l ?r =>
      match constr:((T, l, r)) with
      | (Z, _, _) => solveZEq
      | (nat, _, _) => lia
      | (cmra_car positiveR, _, _) => lia
      | (positive, _, _) => lia
      | (cmra_car natR, _, _) => lia
      | (ofe_car natO, _, _) => lia
      | (_, ?pr.1, _) => has_evar pr; eapply pair_fst_instantiate; trySolvePureEq
      | (_, ?pr.2, _) => has_evar pr; eapply pair_snd_instantiate; trySolvePureEq
      | (_, _, ?pr.1) => has_evar pr; symmetry; eapply pair_fst_instantiate; trySolvePureEq
      | (_, _, ?pr.2) => has_evar pr; symmetry; eapply pair_snd_instantiate; trySolvePureEq
      | (_, bool_decide ?φ, _) => is_constructor r; solve
        [rewrite bool_decide_eq_false_2; trySolvePure
        |rewrite bool_decide_eq_true_2; trySolvePure ]
      | (ofe_car boolO, _, _) => progress (change (ofe_car boolO) with bool in * ); solve [trySolvePure]
      | (bool, _, _) => solve [bool_solver]
      | (prod _ _, _, _) => eapply pair_eq_from_proj_eqs; solve [trySolvePureEq]
      | _ => eauto with nocore solve_pure_eq_add
      end
    end
  ).

Create HintDb solve_pure_add.

Global Arguments Qp.le : simpl never.
Global Arguments Qp.lt : simpl never.

Ltac preprocess_pure_goals := idtac.

Ltac print_goals_preprocessor := 
  lazymatch goal with
  | |- True => idtac
  | |- ¬ True => idtac
  | |- ¬ False => idtac
  | |- _ ⊆ ⊤ => idtac
  | |- _ ⊈ ⊤ => idtac
  | |- _ =>
    let is_interesting_var term :=
      is_var term; assert_fails (unfold term); 
      lazymatch type of term with
      | iprop.gFunctors => fail
      | _ =>  idtac
      end
    in
    let rec print_rel arg :=
      match goal with
      | H : context [?term] |- context [?term] => 
        is_interesting_var term;
        let Htype := type of H in idtac H":"Htype; try clear H;
        print_rel ()
      | |- ?G => idtac "----------";idtac G; idtac ""
      end
    in
    try (once (print_rel ()); fail)
  end.

(* Use the following command:

Ltac pure_solver.preprocess_pure_goals ::= pure_solver.print_goals_preprocessor.

to set the pure goal preprocessor to print pure goals before attempting to solve them. *)

Ltac trySolvePure ::=
  preprocess_pure_goals;
  match goal with
  | |- BehindModal _ _ => (solve [eauto with nocore solve_pure_add]) || (unfold BehindModal; trySolvePure)
  | |- @eq _ _ _ => trySolvePureEq
  | |- ∅ ⊆ ?E => apply namespaces.coPset_empty_subseteq
  | |- ?E ⊆ ⊤ => apply coPset.coPset_top_subseteq        (* it takes solve_ndisj about .25 seconds to figure out this is not provable -_- *)
  | |- ?E1 ⊆ ?E2 => (has_evar E2; fail 2 "not solving..") || (lazymatch E2 with ?E3 ∖ E1 => fail 2 "not solving.." | _ => solve_ndisj end)
      (* this is to prevent eauto instantiating E2 with ⊤, which is possibly bad *)
  | |- _ ≡ _ => (progress fold_leibniz; trySolvePure) || reflexivity
  | |- ¬ (_ ≡ _) => progress fold_leibniz; trySolvePure

  | |- (_ < _)%positive => lia
  | |- (_ ≤ _)%positive => lia
  | |- (_ > _)%positive => lia
  | |- (_ ≥ _)%positive => lia
  | |- (_ < _)%nat => lia
  | |- (_ ≤ _)%nat => lia
  | |- (_ > _)%nat => lia
  | |- (_ ≥ _)%nat => lia
  | |- (_ < _)%Z => lia
  | |- (_ ≤ _)%Z => lia
  | |- (_ > _)%Z => lia
  | |- (_ ≥ _)%Z => lia
  | |- (_ ≤ 0)%nat => apply Nat.le_refl (* triggers if LHS is an evar, in which case only 0 is a good instantiation *)
  | |- (¬ @eq nat _ _) => lia
  | |- (¬ @eq (ofe_car natO) _ _) => lia
  | |- (¬ @eq positive _ _) => lia
  | |- (¬ @eq Z _ _) => lia
  | |- (¬ @eq (ofe_car ZO) _ _) => lia
  | |- (¬ @eq (ofe_car boolO) _ _) => progress (change (ofe_car boolO) with bool in * ); solve [trySolvePure]
  | |- (¬ @eq bool ?b1 ?b2) =>
      first 
      [is_constructor b2; assert_fails (has_evar b1);
        match b2 with
        | true => apply not_true_iff_false
        | false => apply not_false_iff_true
        end; pure_solver.trySolvePure
      |assert_fails (is_constructor b2); (* prevents loops for true ≠ true *)
       is_constructor b1; apply not_eq_sym; pure_solver.trySolvePure]
  | |- ¬ (_ < _)%positive => lia
  | |- ¬ (_ ≤ _)%positive => lia
  | |- ¬ (_ > _)%positive => lia
  | |- ¬ (_ ≥ _)%positive => lia
  | |- ¬ (_ < _)%nat => lia
  | |- ¬ (_ ≤ _)%nat => lia
  | |- ¬ (_ > _)%nat => lia
  | |- ¬ (_ ≥ _)%nat => lia
  | |- ¬ (_ < _)%Z => lia
  | |- ¬ (_ ≤ _)%Z => lia
  | |- ¬ (_ > _)%Z => lia
  | |- ¬ (_ ≥ _)%Z => lia
  | |- ¬ False => exact id
  | |- ¬ (?l + ?r ≤ ?l)%Qp => apply Qp.not_add_le_l
  | |- ¬ (?l + ?r ≤ ?r)%Qp => apply Qp.not_add_le_r
  | |- ¬¬ _ => apply dneg_involutive; solve [trySolvePure]

  | |- (✓ (● _)) => apply auth_auth_valid; trySolvePure
  | |- (✓ (●F _)) => apply frac_auth_auth_valid; trySolvePure
  | |- (✓ (◯ _)) => apply auth_frag_valid; trySolvePure

  | |- _ => progress eauto with nocore solve_pure_add
(*  | |- _ => fast_done *)
  | |- ?φ => assert_fails (has_evar φ); done
    (* done is currently at least responsible for solving ✓ _ goals. if those are suitable simplified,
       we might get away with fast_done.
       we check for evars so that for instance (0 ≠ ?y) does not unify y with 1 *)
  | |- ∀ _, _ => solve [intros; pure_solver.trySolvePure]
  | |- ∃ _, _ => solve [eexists; pure_solver.trySolvePure]
  | |- _ ∧ _ => solve [split; pure_solver.trySolvePure]
  | |- _ ∨ _ => solve [left; pure_solver.trySolvePure | right; pure_solver.trySolvePure] (*
  | |- _ => eauto*)
  end.

Global Hint Extern 4 (SolveSepSideCondition ?φ) => unfold SolveSepSideCondition; trySolvePure : typeclass_instances.




