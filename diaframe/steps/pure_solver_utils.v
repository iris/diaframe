From diaframe.steps Require Import pure_solver.
From diaframe Require Import utils_ltac2 solve_defs.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode environments coq_tactics.


(* This file provides utilities for using the pure solver, especially during
  stepping with the automation. We want to get some additional information
  out of the pure solver, in particular when a goal is _not_ solved. *)


(* Miscellaneous utilities *)

(* We need to do some juggling with evars, which the next few tactics are for *)
Ltac hide_evars_adv next_tac name_hook := 
  match goal with
  | |- context [?arg] =>
    is_evar arg;
    let evar_name := fresh "x" in
    set (evar_name := arg);
    try (hide_evars_adv next_tac name_hook); 
    (* it is important that failure of next_tac will not cause evars to be unfolded *)
    name_hook evar_name
  | |- _ => next_tac
  end.

Ltac unhider evar_name :=
  unfold evar_name; clear evar_name.

Ltac noop_tac evar_name := idtac.

Ltac hide_evars_then next_tac :=
  hide_evars_adv next_tac unhider.

Tactic Notation "hide_evars" "then" tactic(next) "then" "unhide" :=
  hide_evars_then next.

Tactic Notation "hide_evars" := hide_evars_adv idtac noop_tac.


Lemma tac_pure_intro_direct `(Δ : envs PROP) (φ : Prop) :
  φ → envs_entails Δ ⌜φ⌝.
Proof. intros. eapply tac_pure_intro; [tc_solve| split| exact H]. Qed.

Ltac maybeIPureIntro :=
  lazymatch goal with
  | |- envs_entails _ _ => 
      refine (tac_pure_intro_direct _ _ _)
  | |- _ => idtac
  end.


(* 'Return values' for the pure solver *)
Ltac2 Type pure_solver_result := [
  | GoalSolved
  | GoalProvablyFalse
  | GoalUnsolvedNoEvars
  | GoalUnsolvedEvars
].


(* Now, we define the tactic that tries to solve pure goals during automation steps. *)
Ltac2 try_solve_pure (_ : unit) : pure_solver_result :=
  lazy_match! goal with
  | [|- ?eφ ] =>
    let φ := 
      lazy_match! eφ with
      | envs_entails _ ⌜?φ⌝ => φ
      | _ => eφ
      end in
    let φr :=
      lazy_match! φ with
      | BehindModal _ ?φr => φr
      | _ => φ
      end in
    plus_once (fun _ =>
      lazy_match! φr with
      | @eq _ ?a ?b => 
        if Bool.or (Constr.is_evar a) (Constr.is_evar b) then
          Control.zero Assertion_failure
        else ()
      | _ => ()
      end; 
      (assert (¬ $φr) as _ by (ltac1:(hide_evars then (idtac; trySolvePure) then unhide)));
      (* if idtac is not present, trySolvePure will run immediately...? *)
      GoalProvablyFalse
    ) (fun _ =>
      let unsolved_result := 
        if Constr.has_evar φr then
          GoalUnsolvedEvars
        else
          GoalUnsolvedNoEvars
      in
      plus_once (fun _ =>
        solve [ltac1:(maybeIPureIntro; trySolvePure)];
        GoalSolved
      ) (fun _ =>
        (* remove BehindModal in case we show the goal to the user *)
        ltac1:(φ φr |- change φ with φr) (Ltac1.of_constr φ) (Ltac1.of_constr φr);
        unsolved_result
      )
    )
  end.


(* We often want to rollback/do something else depending on 
  the result of pure solving. This 'try_solve_pure_on' takes two arguments:
  - a closure : (unit -> unit) -> unit.
    This closure takes the pure_solver : unit -> unit as an input,
      and may call it at some generated subgoals.
  - handle_and_get_rollback : pure_solver_result -> bool
    This will be called with the result of the pure solver. This tactic
    may itself affect the proof state (i.e., by calling shelve)
    If 'true' is returned, everything will be rolled back *)
Ltac2 try_solve_pure_on 
    (solve_pure_on : (unit -> unit) -> unit)
    (handle_and_get_rollback : pure_solver_result -> bool)
  : unit :=
  plus_once (fun _ =>
    solve_pure_on (fun _ =>
      let result := try_solve_pure () in
      if handle_and_get_rollback result then
        Control.zero Assertion_failure
      else
        ()
    )
  ) (fun _ =>
    ()
  ).

(* iStep/iSmash have different behavior depending on [pure_solver_result] 
  to execute that on the steps side, we must be given a [c : pure_solver_result -> unit] in Ltac2.
  We want to keep some things Ltac1 for now, so we provide two behaviors here: 
    - shelve unsolved goals with no evars, otherwise fail
    - fail always on unsolved goals
*)

Ltac2 handle__shelve_unsolved_noevars : pure_solver_result -> bool :=
  (fun r =>
    match r with
    | GoalSolved => false
    | GoalUnsolvedNoEvars => Control.shelve (); false
    | GoalUnsolvedEvars => true
    | GoalProvablyFalse => true
    end
  ).

Ltac2 try_solve_pure_on__silent_shelve_unsolved_noevars 
    (solve_pure_on : (unit -> unit) -> unit)
  : unit :=
  try_solve_pure_on solve_pure_on handle__shelve_unsolved_noevars.


Ltac2 handle__rollback_unsolved : pure_solver_result -> bool :=
  (fun r =>
    match r with
    | GoalSolved => false
    | GoalUnsolvedNoEvars => true
    | GoalUnsolvedEvars => true
    | GoalProvablyFalse => true
    end
  ).

Ltac2 try_solve_pure_on__silent_fail
    (solve_pure_on : (unit -> unit) -> unit)
  : unit :=
  try_solve_pure_on solve_pure_on handle__rollback_unsolved.

(* And now we provide these for Ltac1 *)

Ltac try_solve_pure_on__silent_shelve_unsolved_noevars solve_on :=
  let f := ltac2:(solve_pure_on |-
    try_solve_pure_on__silent_shelve_unsolved_noevars (fun c =>
      Ltac1.apply solve_pure_on [Ltac1.lambda (fun _ => c (); ltac1val:(idtac))] Ltac1.run
    )
  ) in
  progress (f solve_on).

Ltac try_solve_pure_on__silent_fail solve_on :=
  let f := ltac2:(solve_pure_on |-
    try_solve_pure_on__silent_fail (fun c =>
      Ltac1.apply solve_pure_on [Ltac1.lambda (fun _ => c (); ltac1val:(idtac))] Ltac1.run
    )
  ) in
  progress (f solve_on).

Goal True ∧ True.
try_solve_pure_on__silent_shelve_unsolved_noevars ltac:(fun c => split; [c () | ]).
Abort.
Goal True ∧ True.
try_solve_pure_on__silent_fail ltac:(fun c => split; [c () | ]).
Abort.

Goal False ∧ True.
Fail try_solve_pure_on__silent_shelve_unsolved_noevars ltac:(fun c => split; [c () | ]).
Fail try_solve_pure_on__silent_fail ltac:(fun c => split; [c () | ]).
Abort.

Goal ∃ n, n < 0 ∧ True.
Fail try_solve_pure_on__silent_shelve_unsolved_noevars ltac:(fun c => eexists; split; [c () | ]).
Fail try_solve_pure_on__silent_fail ltac:(fun c => split; [c () | ]).
Abort.

Goal ∀ P, (P ∨ ¬ P) ∧ True.
Fail try_solve_pure_on__silent_fail ltac:(fun c => split; [c () | ]).
try_solve_pure_on__silent_shelve_unsolved_noevars ltac:(fun c => intros; split; [c () | ]). Unshelve.
Abort.


Ltac repeat_eexists :=
  repeat (
    lazymatch goal with
    | |- ex _ => eexists; [shelve..| ]
      (* if existential variable is not mentioned, it is unshelved.
          but the pure solver does not like that, so we shelve it *)
    end
  ).













