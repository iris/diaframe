From diaframe Require Import env_utils util_classes tele_utils utils_ltac2.
From iris.bi Require Import bi.
From iris.proofmode Require Import proofmode environments.


Section defs.
  Context {PROP : bi}.

  Fixpoint right_big_sep (Ps : list PROP) : PROP :=
    match Ps with
    | [] => emp
    | [P] => P
    | P :: Ps => P ∗ right_big_sep Ps
    end.

  Fixpoint my_map {A B : Type} (f : A → B) (xs : list A) : list B :=
    match xs with
    | [] => []
    | x :: xs => f x :: my_map f xs
    end.

  Lemma right_big_sep_cons P (Ps : list PROP) :
    right_big_sep (P :: Ps) ⊣⊢ P ∗ right_big_sep Ps.
  Proof. destruct Ps => //=. by rewrite right_id. Qed.

  Lemma right_big_sep_equiv_big_sepL {A : Type} (xs : list A) (f : A → PROP) :
    right_big_sep (my_map f xs) ⊣⊢ [∗ list] x ∈ xs, f x.
  Proof.
    induction xs; first done.
    cbn [ map ]. by rewrite right_big_sep_cons IHxs.
  Qed.

  Definition my_id {A : Type} : A → A := fun a => a.
  Global Arguments my_id {_} _ /.
End defs.


(* We declare a reduction for reducing envs_add_fresh and envs_option_delete, mainly.
Also includes right_big_sep which is used inside MergableEnv *)
Declare Reduction red_diaframe_envs := cbv [
  (* base *)
  base.beq base.ident_beq base.positive_beq base.string_beq
  base.ascii_beq base.pm_option_bind
  (* env stuff *)
  envs_delete env_delete
  envs_lookup env_lookup eqb
  (* envs_add_fresh internals *)
  envs_incr_counter env_intuitionistic env_spatial env_counter base.Pos_succ
  (* envs_app internals, used in envs_add_fresh *)
  envs_app env_app
  (* right_big_sep auxiliaries *)
  my_map
  (* envs_add_fresh, envs_option_delete, right_big_sep *)
  envs_add_fresh envs_option_delete right_big_sep
].

Ltac simpl_diaframe_envs :=
  (* Use [change_no_check] instead of [change] to avoid performing the
  conversion check twice. *)
  lazymatch goal with |- ?u => let v := eval red_diaframe_envs in u in change_no_check v end.


Declare Reduction red_diaframe_tele := cbv [
  (* native tele stuff *)
  tele_app tele_bind bi_texist tele_fold tforall TCTForall
  (* diaframe additions *)
  tele_appd_dep tele_dep_bind tele_arg_rect
  (* utilities *)
  tele_curry tele_fun_compose' TeleConcatType
  tele_pair_arg TelePairType TeleConcatArg as_dependent_arg as_dependent_fun
].

Declare Reduction red_diaframe_tele_cbn := cbn [
  (* native tele stuff *)
  tele_app tele_bind bi_texist tele_fold tforall TCTForall
  (* diaframe additions *)
  tele_appd_dep tele_dep_bind tele_arg_rect
  (* utilities *)
  tele_curry tele_fun_compose' TeleConcatType
  tele_pair_arg TelePairType TeleConcatArg as_dependent_arg as_dependent_fun
].

Ltac simpl_diaframe_tele :=
  lazymatch goal with
  | |- envs_entails ?Δ ?G =>
    let G' := eval red_diaframe_tele in G in
    change_no_check (envs_entails Δ G')
  end.

Ltac simpl_diaframe_tele_tcforall :=
  lazymatch goal with
  | |- ?G =>
    lazymatch G with
    | TCTForall _ =>
      let G' := eval red_diaframe_tele in G in
      change_no_check G'
    end
  end.


Declare Reduction red_diaframe_pure_tele := cbv [
  (* just native tele stuff *)
  tele_app tele_bind tele_fold texist
  (* along with my_id *)
  my_id
].
Ltac simpl_diaframe_pure_tele :=
  lazymatch goal with
  | |- ?G ?Δ =>
    let G' := eval red_diaframe_pure_tele in G in
    let GΔ := eval cbn beta in (G' Δ) in
    change_no_check (GΔ)
  end.

Declare Reduction red_diaframe_var_intro := cbn [
  (* just native tele stuff *)
  tele_app tele_bind tele_fold texist tforall
  (* along with my_id *)
  my_id
].
Ltac simpl_diaframe_var_intro :=
  lazymatch goal with
  | |- ?G ?Δ =>
    let G' := eval red_diaframe_var_intro in G in
    let GΔ := eval cbn beta in (G' Δ) in
    change_no_check (GΔ)
  end.


Ltac2 to_reference (c : constr) : Std.reference :=
  match Constr.Unsafe.kind c with
  | Constr.Unsafe.Constant c' _ => Std.ConstRef c'
  | _ =>
    printf "%t" c;
    Control.throw Assertion_failure
  end.

Ltac2 mutable reduce_post_hint (_ : unit) : Std.reference list :=
  List.map to_reference
  [
  (* base *)
  '@base.beq; '@base.ident_beq; '@base.positive_beq; 'base.string_beq;
  '@base.ascii_beq; '@base.pm_option_bind;
  (* env stuff *)
  '@envs_delete; '@env_delete;
  '@envs_lookup; '@env_lookup; '@eqb;
  (* envs_add_fresh internals *)
  '@envs_incr_counter; '@env_intuitionistic; '@env_spatial; '@env_counter; '@base.Pos_succ;
  (* envs_app internals, used in envs_add_fresh *)
  '@envs_app; '@env_app;
  (* right_big_sep auxiliaries *)
  '@my_map;
  (* envs_add_fresh, envs_option_delete, right_big_sep *)
  '@envs_add_fresh; '@envs_option_delete; '@right_big_sep;
  (* native tele stuff *)
  '@tele_app; '@tele_bind; '@bi_texist; '@tele_fold;
  (* diaframe additions *)
  '@tele_appd_dep; '@tele_dep_bind; '@tele_arg_rect;
  (* utilities *)
  '@tele_curry; '@tele_fun_compose'; '@TeleConcatType;
  '@tele_pair_arg; '@TelePairType; '@TeleConcatArg; '@as_dependent_arg; '@as_dependent_fun
].

(*
Ltac2 Set reduce_post_hint as old := fun () =>
  List.append (old ()) [to_reference '@compose].

To get this working for HeapLang we need at least


Ltac2 Set reduce_post_hint as old := fun () =>
  List.append (old ()) (List.map to_reference 
   ['@compose;
    '@flip;
    '@weakestpre.wp_execute_op;
    '@fill;
    '@foldl;
    '@ectxi_language.fill_item;
    '@weakestpre.template_I;
    '@bi_tforall;
    '@tele_fold;
    '@fill_item;
    '@heap_ectxi_lang;
    '@subst';
    '@subst; 
    '@id;
    '@decide;
    '@and_dec;
    '@not_dec;
    '@decide_rel;
    '@binder_dec_eq;
    '@binder_rec;
    '@binder_rect;
    '@sumbool_rec;
    '@sumbool_rect;
    '@string_eq_dec;
    '@string_rec;
    '@string_rect;
    '@ascii_eq_dec;
    '@Ascii.ascii_dec;
    '@Ascii.ascii_rec;
    '@Ascii.ascii_rect;
    '@bool_dec;
    '@bool_rec;
    '@bool_rect;
    '@bi_laterN;
    '@defs.template_M;
    '@fst]
).

*)

Local Ltac2 Notation "red_flags" s(strategy) := s.

Ltac simpl_diaframe_post_hint :=
  ltac2:(
    let the_clause := { Std.on_hyps := None; Std.on_concl := Std.AllOccurrences } in
    let the_flags := { (red_flags beta match fix zeta) with
      Std.rDelta := false;
      Std.rConst := reduce_post_hint ()
    } in
    Std.lazy the_flags the_clause
  ).

Ltac simpl_diaframe_post_hint ::=
  lazymatch goal with
  | |- ?u =>
    let v := eval red_diaframe_envs in u in
    lazymatch v with
    | envs_entails ?Δ ?G =>
      let G' := eval simpl in G in
      try progress change_no_check (envs_entails Δ G')
    end
  end.

