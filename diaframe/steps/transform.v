From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import automation_state.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file contains the part of Diaframe's proof search strategy that deals with introducing
   non-monadic modalities, like ▷ and □, but can also deal with, for example, Löb induction
   To see how to use these, see for example lib.intuitionistically and steps.solve_instances *)

Lemma transform_transform_hyp {PROP : bi} (R P R' : PROP) p :
  TransformHyp p P R R' →
  Transform R' ⊢ □?p P -∗ Transform R.
Proof.
  unseal_diaframe => /= HPR.
  by apply HPR.
Qed.

Section coq_tactics.
  Context {PROP : bi}.
  Implicit Type R : PROP.

  Lemma tac_solve_transform_ctx Δ R G :
    TransformCtx Δ R G →
    envs_entails Δ G →
    envs_entails Δ $ Transform R.
  Proof. unseal_diaframe. eauto. Qed.

  Lemma tac_solve_transform_hyp Δ R p P R' i :
    FindInContext Δ (λ p' P', TransformHyp p' P' R R') i p P →
    envs_entails (envs_delete true i p Δ) $ Transform R' →
    envs_entails Δ $ Transform R.
  Proof.
    rewrite envs_entails_unseal => [[HiP HPQ]] HΔ.
    rewrite (envs_lookup_sound' _ true) // HΔ.
    apply wand_elim_r'.
    eapply transform_transform_hyp => //.
  Qed.

End coq_tactics.

Ltac2 transformStep state gid :=
  first
  [ltac1:(notypeclasses refine (tac_solve_transform_hyp _ _ _ _ _ _ _ _ )) >
    [tc_solve () | ]; handle_prop_deletion_on_goal state gid; simpl; cbn
    (* not too happy with cbn here, but simpl is not smart enough to figure out how to reduce some telescope hacking.
       Tweaking Arguments is not enough to fix that quickly *)
  |ltac1:(notypeclasses refine (tac_solve_transform_ctx _ _ _ _ _); [ tc_solve.. | simpl; cbn])].





