From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import env_utils util_classes solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import disj_chooser_tacs pure_solver_utils automation_state.
From iris.bi Require Import bi telescopes.

Import bi.

Section solve_one_foc_rules.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma solve_one_foc_fromsep {TT : tele} M Q L1 L2 D TT1 L1' TT2 h:
    (TC∀.. (tt : TT), FromSepCareful (tele_app Q tt) (tele_app L1 tt) (tele_app L2 tt)) →
    ExtractDep L1 TT1 L1' TT2 h →
    ModalityMono M →
    SolveSepFoc (TT := TT1 ) M L1' 
      (tele_bind (λ tt1, ∃..(tt2 : tele_app TT2 tt1), tele_app L2 (tele_app (tele_appd_dep h tt1) tt2))) D ⊢ SolveOneFoc (TT := TT) M Q D.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromSepCareful /ExtractDep !tforall_forall => H HL1 HM.
    apply HM => {HM} /=.
    apply or_mono; last done.
    apply bi_texist_elim => tt1.
    rewrite tele_app_bind.
    apply bi.wand_elim_r', bi_texist_elim => tt2.
    apply bi.wand_intro_l.
    revert HL1 => /(dep_eval tt1) /(dep_eval_tele tt2) ->.
    rewrite bi_texist_exist -(exist_intro) -H //.
  Qed.

  Lemma solve_one_foc_fromexist {TT1 : tele} {TT2} M P P' D :
    (TC∀.. (tt : TT1), FromTExist (tele_app P tt) (tele_app TT2 tt) (tele_appd_dep P' tt)) →
    ModalityMono M →
    SolveOneFoc (TT := TeleConcatType TT1 TT2) M (tele_curry P') D 
    ⊢ SolveOneFoc (TT := TT1) M P D.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromTExist => /tforall_forall HPP' HM.
    apply HM, or_mono; last done.
    apply bi_texist_elim => tt.
    rewrite (tele_arg_concat_inv_eq tt).
    destruct (tele_arg_concat_inv_strong_hd tt) as [tt1 tt2].
    rewrite bi_texist_exist -(exist_intro tt1).
    rewrite -HPP' !bi_texist_exist.
    rewrite -(exist_intro tt2).
    rewrite tele_uncurry_curry_compose.
    by rewrite -tele_curry_uncurry_relation.
  Qed.

  Lemma solve_one_foc_fromor {TT : tele} M Q L1 L2 L1' D b :
    (TC∀.. (tt : TT), FromOrCareful (tele_app Q tt) (tele_app L1 tt) (tele_app L2 tt)) →
    (TC∀.. (tt : TT), NormalizeProp (tele_app L1 tt) (tele_app L1' tt) b ) → 
      (* this tries to make some branches false before investigating deeper, improves iStep behaviour *)
    ModalityMono M → (* TODO: ExtractDep ? *)
    SolveOneFoc (TT := TT) M L1' ((∃.. (tt: TT), tele_app L2 tt) ∨ D) ⊢ SolveOneFoc (TT := TT) M Q D.
  Proof.
    unseal_diaframe; rewrite /TCTForall /FromOrCareful !tforall_forall => H HL HM.
    apply HM => {HM}.
    rewrite assoc.
    apply or_mono; last done.
    apply or_elim; apply bi_texist_mono => tt; rewrite -H HL; eauto.
  Qed.

  Lemma solve_one_foc_nested_solve_one {TT : tele} M L D :
    SolveOneFoc (TT := TT) M L D ⊢ SolveOneFoc (TT := [tele]) M (SolveOne (TT := TT) id L) D.
  Proof. by unseal_diaframe. Qed.

  Lemma solve_one_foc_nested_solve_sep {TT : tele} M L Q D :
    SolveSepFoc (TT := TT) M L Q D ⊢ SolveOneFoc (TT := [tele]) M (SolveSep (TT := TT) id L Q) D.
  Proof. by unseal_diaframe. Qed.

  Lemma solve_one_foc_focus {TT : tele} M Q D G :
    AsSolveGoalInternal (M $ ∃.. (tt : TT), tele_app Q tt)%I G →
    ModalityMono M →
    G ⊢ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite /AsSolveGoalInternal => -> HM.
    unseal_diaframe. apply HM.
    apply or_intro_l.
  Qed.

  Lemma solve_one_foc_abd_foc_disj_ex {TT} p P M Q R D :
    AbductModInput [tele] p P (FocDisjEx Q D) M R →
    □?p P ∗ R ⊢ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite /AbductModInput /Abduct /= => ->.
    unseal_diaframe.
    by rewrite FocDisjEx_eq /FocDisjEx_def.
  Qed.

  Lemma solve_one_foc_unfocus {TT : tele} M Q D G :
    AsSolveGoalInternal (M D) G →
    ModalityMono M →
    G ⊢ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite /AsSolveGoalInternal => -> HM.
    unseal_diaframe. apply HM.
    apply or_intro_r.
  Qed.

  Lemma solve_one_foc_not_introducable {TT : tele} M M1 M2 Q D :
    NotIntroducable M →
    SplitLeftModality3 M M1 M2 →
    SolveSep (TT := [tele]) M1 (χ) (M2 ((∃.. tt, tele_app Q tt) ∨ D)) ⊢ SolveOneFoc (TT := TT) M Q D.
  Proof.
    unseal_diaframe => /= => _ [HMs HM1 HM2].
    rewrite -HMs. apply HM1. by rewrite empty_goal_eq left_id.
  Qed.
End solve_one_foc_rules.

Section coq_tactics.
  Context {PROP : bi}.
  Implicit Types M : PROP → PROP.

  Lemma tac_solve_one_foc_fromsep {TT : tele} Δ M Q L1 L2 D TT1 L1' TT2 h:
    (TC∀.. (tt : TT), FromSepCareful (tele_app Q tt) (tele_app L1 tt) (tele_app L2 tt)) →
    ExtractDep L1 TT1 L1' TT2 h →
    ModalityMono M →
    envs_entails Δ $ SolveSepFoc (TT := TT1 ) M L1' 
      (tele_bind (λ tt1, ∃..(tt2 : tele_app TT2 tt1), tele_app L2 (tele_app (tele_appd_dep h tt1) tt2))%I) D →
    envs_entails Δ $ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite envs_entails_unseal => HQ HL HM ->.
    by eapply solve_one_foc_fromsep.
  Qed.

  Lemma tac_solve_one_foc_fromexist {TT1 : tele} {TT2} Δ M P P' D :
    (TC∀.. (tt : TT1), FromTExist (tele_app P tt) (tele_app TT2 tt) (tele_appd_dep P' tt)) →
    ModalityMono M →
    envs_entails Δ $ SolveOneFoc (TT := TeleConcatType TT1 TT2) M (tele_curry P') D →
    envs_entails Δ $ SolveOneFoc (TT := TT1) M P D.
  Proof.
    rewrite envs_entails_unseal => HP HM ->.
    by apply solve_one_foc_fromexist.
  Qed.

  Lemma tac_solve_one_foc_pure (Δ : envs PROP) {TT : tele} b M P D φ :
    (∀.. tt : TT, FromPure b (tele_app P tt) (tele_app φ tt)) →
    IntroducableModality M →
    (TCIf (TCEq b true) (TCForall Affine (env_spatial Δ)) (TCEq b false)) →
    (∃.. tt, tele_app φ tt) →
    envs_entails Δ $ SolveOneFoc (TT := TT) M P D.
  Proof.
    unseal_diaframe; rewrite envs_entails_unseal texist_exist => /tforall_forall HPφ <- + [tt Hφ] /=.
    rewrite bi_texist_exist -(exist_intro tt) -or_intro_l -HPφ pure_True //.
    case => [-> HΔ | ->] /=; eauto.
  Qed.

  Lemma tac_solve_one_foc_fromor {TT : tele} Δ M Q L1 L2 D L1' b :
    (TC∀.. (tt : TT), FromOrCareful (tele_app Q tt) (tele_app L1 tt) (tele_app L2 tt)) →
    (TC∀.. (tt : TT), NormalizeProp (tele_app L1 tt) (tele_app L1' tt) b ) → 
    ModalityMono M →
    envs_entails Δ $ SolveOneFoc (TT := TT) M L1' ((∃.. (tt: TT), tele_app L2 tt) ∨ D) →
    envs_entails Δ $ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite envs_entails_unseal => HQ HL HM ->.
    by eapply solve_one_foc_fromor.
  Qed.

  Lemma tac_solve_one_foc_nested_solve_one {TT : tele} Δ M L D :
    envs_entails Δ $ SolveOneFoc (TT := TT) M L D →
    envs_entails Δ $ SolveOneFoc (TT := [tele]) M (SolveOne (TT := TT) id L) D.
  Proof. rewrite envs_entails_unseal => ->. apply solve_one_foc_nested_solve_one. Qed.

  Lemma tac_solve_one_foc_nested_solve_sep {TT : tele} Δ M L Q D :
    envs_entails Δ $ SolveSepFoc (TT := TT) M L Q D →
    envs_entails Δ $ SolveOneFoc (TT := [tele]) M (SolveSep (TT := TT) id L Q) D.
  Proof. rewrite envs_entails_unseal => ->. apply solve_one_foc_nested_solve_sep. Qed.

  Lemma tac_solve_one_foc_focus {TT : tele} Δ M Q D G :
    AsSolveGoalInternal (M $ ∃.. (tt : TT), tele_app Q tt)%I G →
    ModalityMono M →
    envs_entails Δ G →
    envs_entails Δ $ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite envs_entails_unseal => HQG HM ->.
    apply: solve_one_foc_focus.
  Qed.

  Lemma tac_solve_one_foc_focus_teleO Δ M Q D G :
    AsSolveGoalInternal (M Q)%I G →
    ModalityMono M →
    envs_entails Δ G →
    envs_entails Δ $ SolveOneFoc (TT := [tele]) M Q D.
  Proof.
    rewrite envs_entails_unseal => HQG HM ->.
    apply: solve_one_foc_focus.
  Qed.

  Lemma tac_solve_one_foc_abduct_foc_disj_ex Δ i {TT} p P M Q R D :
    FindInExtendedContext Δ (λ p P, AbductModInput [tele] p P (FocDisjEx Q D) M R) i p P →
    envs_entails (envs_option_delete false i p Δ) R →
    envs_entails Δ $ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite envs_entails_unseal => HΔiP.
    rewrite (findinextcontext_spec false) => ->.
    apply ficext_satisfies in HΔiP as HPQ.
    by rewrite <-solve_one_foc_abd_foc_disj_ex.
  Qed.

  Lemma tac_solve_one_foc_unfocus {TT : tele} Δ M Q D G :
    AsSolveGoalInternal (M D) G →
    ModalityMono M →
    envs_entails Δ G →
    envs_entails Δ $ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite envs_entails_unseal => HG HM ->.
    by apply solve_one_foc_unfocus.
  Qed.

  Lemma tac_solve_one_foc_not_introducable {TT : tele} Δ M M1 M2 Q D :
    NotIntroducable M →
    SplitLeftModality3 M M1 M2 →
    envs_entails Δ (SolveSep (TT := [tele]) M1 (χ)%I (M2 ((∃.. tt, tele_app Q tt) ∨ D)))%I →
    envs_entails Δ $ SolveOneFoc (TT := TT) M Q D.
  Proof.
    rewrite envs_entails_unseal => + + ->.
    apply solve_one_foc_not_introducable.
  Qed.
End coq_tactics.


Ltac2 apply_unfocus_rule (_ : unit) :=
  ltac1:(
    notypeclasses refine (tac_solve_one_foc_unfocus _ _ _ _ _ _ _ _);
      [tc_solve..| simpl]
  ).

(* On goal (G ∨ D), if the focused goal G satisfies [CommitLeftDisjunct] 
  and the telescope is trivial (i.e. there is no existential quantification),
  we want to skip [Abduct] search and directly focus: *)
Ltac2 left_side_easier 
    (chooser_tac : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit)
    (state : automation_state.t) (gid : goal_id) :=
  (* Before we focus, ask the chooser tactic if that is okay, or if we should halt proof search *)
  chooser_tac state gid PickEvent.LeftEasier (fun side =>
    match side with
    | PickSide.Left =>
      ltac1:(notypeclasses refine (tac_solve_one_foc_focus_teleO _ _ _ _ _ _ _ _)) > 
        [tc_solve () ..| simpl]
    | PickSide.Right => apply_unfocus_rule ()
    | PickSide.Stop => Control.zero Assertion_failure
    end
  ).

(* Otherwise, we should consider searching for [Abduct]. *)
Ltac2 maybe_abduct_search 
    (chooser_tac : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit)
    (state : automation_state.t) (gid : goal_id) :=
  (* maybe_abduct_search is used when the goal has shape G ∨ D, possibly behind existential quantifiers and a modality M,
    and G is not a connective *)
  let cont := plus_once_list
    [ (* The surrounding modality M is not introducable. This indicates the need to close an invariant:
        set the dummy resource χ as the new goal, to start search for a closing resource *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_not_introducable _ _ _ _ _ _ _ _ _)) >
        [tc_solve ().. | simpl]; fun _ => ()
    | (* Find a connection to G *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_abduct_foc_disj_ex _ _ _ _ _ _ _ _ _ _)) >
      [tc_solve () | ];
        handle_prop_deletion_on_goal state gid;
        simpl; fun _ => ()
    | (* We could not find a hint/connection and the modality was introducable. We need to ask the chooser_tac for instructions *)
     (* We only activate NoHintOne if the left hand side is not a modality itself! *)
      lazy_match! goal with
      | [|- envs_entails _ (SolveOneFoc (TT := [tele]) _ ?l _)] =>
        (* there is no existential quantification.. *)
        let cont := plus_once (fun _ =>
          (* check to see if G itself contains a modality, i.e. the goal has shape (M ((M' G) ∨ D)) *)
          assert_succeeds (fun _ => assert (∃ M P', Collect1Modal $l M P') by (ltac1:(do 2 eexists; tc_solve)));
          fun _ =>
            (* if so, signal that to the chooser_tac *)
            chooser_tac state gid PickEvent.LeftModOne (fun side =>
              match side with
              | PickSide.Left => 
                ltac1:(notypeclasses refine (tac_solve_one_foc_focus _ _ _ _ _ _ _ _)) >
                  [tc_solve ()..| simpl]
              | PickSide.Right => apply_unfocus_rule ()
              | PickSide.Stop => fail
              end
            )
        ) (fun _ _ =>
          (* Otherwise, signal that we could not find a connection to the chooser_tac *)
          chooser_tac state gid PickEvent.NoHintOne (fun side =>
            match side with
            | PickSide.Left => apply_unfocus_rule ()
            | _ => fail
            end
          )
        ) in
        cont
      | [|- _] => 
        (* nonempty telescope: signal that we could not find a connection to the chooser_tac *)
        fun _ => chooser_tac state gid PickEvent.NoHintOne (fun side =>
          match side with
          | PickSide.Left => apply_unfocus_rule ()
          | _ => fail
          end
        )
      end
    ] in
  cont ().


Ltac2 solveOneFocStep (chooser_tac : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit)
    (state : automation_state.t) (gid : goal_id) : goal_id list :=
  (* solveOneFocStep is used when the goal has shape G ∨ D, possibly behind existential quantifiers and a modality M. *)
  let cont := plus_once_list 
    [ (* G has shape (L ∗ G') *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_fromsep _ _ _ _ _ _ _ _ _ _ _ _ _ _)) >
        [tc_solve ()..| simpl]; fun _ => [gid]
    | (* G has shape (G1 ∨ G2) *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_fromor _ _ _ _ _ _ _ _ _ _ _ _)) >
        [tc_solve ()..| simpl]; fun _ => [gid]
    | (* G has shape (∃ G) *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_fromexist _ _ _ _ _ _ _ _)) >
        [tc_solve ()..| simpl]; fun _ => [gid]
    | (* G has shape ⌜G⌝, a pure proposition *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_pure _ _ _ _ _ _ _ _ _ _)) >
        [tc_solve ()..| simpl ]; 
      (* below failure _should_ cause consideration of the next option! *)
        progress (try_solve_pure_on 
          (fun c => ltac1:(repeat_eexists); c ())
          handle__rollback_unsolved
        ); fun _ => []
        (* above _must_ fail when it does not solve the goal, to cause backtracking to other options *)
    | (* G has shape SolveOne G'. Causes the inner G' to be moved to the outer SolveOneFoc 
        (corner case, TODO: document when this occurs) *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_nested_solve_one _ _ _ _ _)); fun _ => [gid]
    | (* G has shape SolveSep L G'. Moves this inner construct upward, to become a SolveSepFoc.
        (corner case, TODO: document when this occurs) *)
      ltac1:(notypeclasses refine (tac_solve_one_foc_nested_solve_sep _ _ _ _ _ _)); fun _ => [gid]
    | (* G has none of the above shapes. We inspect our goal a bit more to determine the appropriate rule *)
      lazy_match! goal with
      | [|- envs_entails _ (SolveOneFoc (TT := ?the_tele) _ ?l _)] => 
        (* if TT = [tele] (no existential quantifiers) and 
          [CommitLeftDisjunct L] (meaning the left-goal G looks 'easier' -- for example, by being a wand H -∗ G')
          then we short-circuit to the [left_side_easier] tactic.
          Otherwise, we call [maybe_abduct_search], which possibly finds an (abduction) connection to L *)
        lazy_match! the_tele with
        | [tele] =>
          plus_once (fun _ =>
            assert_succeeds (fun _ => assert (CommitLeftDisjunct $l) by tc_solve ());
            fun _ => left_side_easier chooser_tac state gid; [gid]
          ) (fun _ =>
            fun _ => maybe_abduct_search chooser_tac state gid; [gid]
          )
        | _ =>
          fun _ => maybe_abduct_search chooser_tac state gid; [gid]
        end
      end
    ] in
  cont ().
