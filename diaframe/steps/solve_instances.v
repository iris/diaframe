From iris.proofmode Require Import base coq_tactics reduction tactics.
From diaframe Require Import util_classes util_instances solve_defs tele_utils.
From iris.bi Require Import bi telescopes.

Export solve_defs.

(* This file contains the minimum instances necessary for functionality of the automation.
   Concretely, it provides some instances responsible for getting appropriate
   SolveOne/IntroduceHyp/IntroduceVars/Transform goals from a regular Iris goal. *)

(* TODO: remove this, superfluous, since SolveOne will do it one step later.
   Its current only use is to present a readable goal if iStep fails while not making any progress *)
Class FromTExistAlways {PROP : bi} (P : PROP) (TT : tele) (P' : TT -t> PROP) :=
  from_texist_always : (∃.. tt, tele_app P' tt) ⊢ P.


Section as_solve_goal_instances.
  Context {PROP : bi}.
  Implicit Types (M : PROP → PROP).

  Global Instance fromtexist_always_fromtexist (P : PROP) TT P' :
    FromTExist P TT P' →
    FromTExistAlways P TT P' | 5.
  Proof. done. Qed.

  Global Instance fromtexists_refl (P : PROP) :
    FromTExistAlways P [tele] P | 10.
  Proof. by rewrite /FromTExistAlways /=. Qed.

  (* By default, we only see connectives as solve goals when we have native support for them in the hint search.
    So those are: wand, forall, later, and the default ∗, possibly behind a modality *)


  Global Instance fromtexist_into_solve_goal M P TT P' :
    ModalityMono M →
    FromTExistAlways P TT P' →
    AsSolveGoal M P (SolveOne M P') | 99.
  Proof.
    unseal_diaframe; rewrite /AsSolveGoal /FromTExistAlways => HM HP'.
    by apply HM.
  Qed.

  (* Wand has preference over forall, since FromForall will trigger on goals of the form (⌜φ⌝ -∗ R) -> (∀ (_ : φ), R) *) 

  Global Instance mod_wand_as_solve_goal M P H G :
    IntroducableModality M →
    FromWand P H G →
    AsSolveGoal M P (IntroduceHyp H G) | 38.
  Proof. 
    unseal_diaframe; rewrite /AsSolveGoal /FromWand /= /ModWeaker /ModalityMono.
    move => <- /= -> //.
  Qed.

  (* There is also [class_instances.from_forall_pure_not], which does not have a wand counterpart in Iris (TODO: PR?).
     We add it, so it is introduced as a wand, and the simplification machinery for pure stuff kicks in. *)
  Global Instance from_wand_pure_not (φ : Prop) `{!BiPureForall PROP} : 
    FromWand (PROP := PROP) ⌜¬φ⌝ ⌜φ⌝ False%I.
  Proof. rewrite /FromWand. iIntros (Hnφ Hφ). contradiction. Qed.

  Global Instance mod_later_wand_as_solve_goal M P P' n H G :
    IntroducableModality M →
    FromLaterNMax P n P' →
    FromWand P' H G →
    AsSolveGoal M P (Transform $ (bi_laterN n) $ IntroduceHyp H G) | 39. (* FromWand does not look under laters, FromForall does... *)
  Proof. 
    unseal_diaframe; rewrite /AsSolveGoal /FromLaterNMax /FromWand /= /ModWeaker.
    move => <- /= <- <- //.
  Qed.

  Global Instance mod_forall_as_solve_goal M P A (Φ : A → PROP) ident :
    IntroducableModality M → 
    FromForall P Φ ident →
    AsSolveGoal M P (IntroduceVars (TT := [tele_pair A]) Φ) | 40.
  Proof. 
    unseal_diaframe; rewrite /AsSolveGoal /FromForall /= /ModWeaker.
    move => /= <- -> //.
  Qed.

  Global Instance mod_later_forall_as_solve_goal M P P' n A (Φ : A → PROP) ident :
    IntroducableModality M →
    FromLaterNMax P n P' →
    FromForall P' Φ ident →
    AsSolveGoal M P (Transform $ (bi_laterN n) $ IntroduceVars (TT := [tele_pair A]) Φ) | 50.
  Proof. 
    unseal_diaframe; rewrite /AsSolveGoal /FromLaterNMax /FromForall /= /ModWeaker.
    move => <- /= <- <- //.
  Qed.

  Global Instance and_as_solve_goal M P Q PQ :
    IntroducableModality M →
    FromAndCareful PQ P Q →
    AsSolveGoal M (PQ) (SolveAnd P Q) | 30.
  Proof. unseal_diaframe; rewrite /AsSolveGoal => <- //. Qed.

  Global Instance later_as_solve_goal (P : PROP) :
    AsSolveGoal id (▷ P)%I (Transform $ (bi_later) P).
  Proof. unseal_diaframe. rewrite /AsSolveGoal //. Qed.

  Global Instance laterN_as_solve_goal (P : PROP) n :
    AsSolveGoal id (▷^n P)%I (Transform $ (bi_laterN n) P).
  Proof. unseal_diaframe. rewrite /AsSolveGoal //. Qed. (*

  Global Instance or_as_solve_goal M (P Q : PROP) :
    AsSolveGoal M (P ∨ Q)%I (SolveOneFoc (TT := [tele]) M P Q).
  Proof. unseal_diaframe => /=. rewrite /AsSolveGoal //. Qed. *)

  (* Only AsSolveGoalInternal instance: *)
  Global Instance as_solve_goal_internal_instance P M P' P'' :
    TCNoBackTrack (CollectModal P M P') →
    AsSolveGoal M P' P'' →
    AsSolveGoalInternal P P''.
  Proof.
    rewrite /AsSolveGoal /AsSolveGoalInternal /CollectModal => [[<-]] <- //.
  Qed.
End as_solve_goal_instances.
(* AsSolveGoal instance for goals that are already solve goals can be found below AbsorbModal instances *)


Section later_transform.
  Context {PROP : bi}.
  Implicit Types P : PROP.
  (* Since ▷ is part of the grammar and of the recursive hint rules, 
    we always include the ▷ transform rules *)

  Global Instance introducable_in_if_always M Δ P :
    IntroducableModality M → TRANSFORM Δ, M P =[ctx]=> P | 10.
  Proof.
    rewrite /TransformCtx => HM.
    rewrite envs_entails_unseal => -> //.
    apply HM.
  Qed.

  Global Instance transform_later_hyp p P P' R : 
    IntoLaterN false 1 P P' →
    TRANSFORM □⟨p⟩ P, ▷ R =[hyp]=> (▷ IntroduceHyp (□?p P') R).
  Proof.
    rewrite /TransformHyp; unseal_diaframe => ->. destruct p; simpl;
    iIntros "HQR HQ !>";
    by iApply "HQR".
  Qed.

  Global Instance transform_laterN_hyp p n P P' R : 
    IntoLaterN false n P P' →
    TRANSFORM □⟨p⟩ P, ▷^n R =[hyp]=> (▷^n IntroduceHyp (□?p P') R).
  Proof.
    rewrite /TransformHyp; unseal_diaframe => ->. destruct p; simpl;
    iIntros "HQR HQ !>";
    by iApply "HQR".
  Qed.
End later_transform.


Section collect_modality_instances.
  Context {PROP : bi}.
  Implicit Type M : PROP → PROP.
  (* These instances ensure CollectModal looks deep enough behind nested modalities *)

  Global Instance collect_modal_base P M P' :
    PrependModality P M P' →
    CollectModal P M P' | 90.
  Proof. by rewrite /PrependModality /CollectModal => <-. Qed.

  Global Instance collect_modal_rec Pin P' P'' Pout M1 M2 M :
    Collect1Modal Pin M1 P' →
    CollectModal P' M2 P'' →
    CombinedModality M1 M2 M →
    ModalityMono M1 →
    TCEq P'' Pout →
    CollectModal Pin M Pout | 50.
  Proof.
    rewrite /Collect1Modal /CollectModal /CombinedModality => <- + ->.
    move => HM2 HM1 <-. by apply HM1.
  Qed.
End collect_modality_instances.


Section absorb_modal_instances.
  Context {PROP : bi}.
  Implicit Types P : PROP.

  Global Instance absorb_modal_id P :
    AbsorbModal P id P | 20.
  Proof. rewrite /AbsorbModal //. Qed.
  (* we need high priority instances on SolveOne, SolveSep, SolveSepFoc goals *)

  Global Instance absorb_modal_default P M P' :
    TCEq (M P) P' →
    AbsorbModal P M P' | 99.
  Proof. rewrite /AbsorbModal => -> //. Qed.

  Lemma absorb_modal_default_solve_one P M P' :
    TCEq (SolveOne (TT := [tele]) M P) P' →
    AbsorbModal P M P'.
  Proof. unseal_diaframe => /=. tc_solve. Qed.

  Global Instance absorb_modal_collect1 P M P' M1 M2 Pout :
    Collect1Modal P M1 P' →
    CombinedModality M2 M1 M →
    ModalityMono M2 →
    TCEq Pout (M P') →
    AbsorbModal P M2 Pout | 25.
  Proof.
    rewrite /Collect1Modal /CombinedModality /AbsorbModal => HP HM HM2 ->.
    rewrite HM. by apply HM2.
  Qed.

  Lemma absorb_modal_introducable M P :
    IntroducableModality M →
    AbsorbModal P M P.
    (* introducevars occurs on two applications of solve_sep_biabd *)
  Proof. rewrite /AbsorbModal /ModWeaker => <- //. Qed.

  Global Instance absorb_modal_wand_introducable P Q M :
    IntroducableModality M →
    AbsorbModal (P -∗ Q) M (P -∗ Q)%I | 20.
  Proof. apply absorb_modal_introducable. Qed.
  (* this one above is actually crucial for opening invariants with Abduct.
     The IntroducableModality forces early unification of the mask, so that
     the mask E is unified before the proof obligation ↑ N ⊆ E triggers *)

  Global Instance absorb_modal_forall_introducable `(P : A → PROP) M :
    IntroducableModality M →
    AbsorbModal (∀ x, P x) M (∀ x, P x)%I | 20.
  Proof. apply absorb_modal_introducable. Qed.


  Global Instance absorb_modal_solve_and M P Q :
    IntroducableModality M →
    AbsorbModal (SolveAnd P Q) M (SolveAnd P Q) | 20.
  Proof. apply absorb_modal_introducable. Qed.

  Lemma absorb_modal_solve_sep {TT : tele} (P Q : TT -t> PROP) M1 M2 M :
    CombinedModality M2 M1 M →
    ModalityMono M2 →
    AbsorbModal (SolveSep (TT := TT) M1 P Q) M2 (SolveSep (TT := TT) M P Q).
  Proof.
    intros. unseal_diaframe.
    eapply (absorb_modal_collect1 _ _ _ M1); try tc_solve.
    rewrite /Collect1Modal //.
  Qed.

  Lemma absorb_modal_solve_one {TT : tele} (P : TT -t> PROP) M1 M2 M :
    CombinedModality M2 M1 M →
    ModalityMono M2 →
    AbsorbModal (SolveOne (TT := TT) M1 P) M2 (SolveOne (TT := TT) M P).
  Proof.
    intros. unseal_diaframe.
    eapply (absorb_modal_collect1 _ _ _ M1); try tc_solve.
    rewrite /Collect1Modal //.
  Qed.
End absorb_modal_instances.

Global Hint Extern 4 (AbsorbModal (@IntroduceVars _ _ _) _ _) =>
  (notypeclasses refine (absorb_modal_introducable _ _ _); tc_solve) ||
  (notypeclasses refine (absorb_modal_default_solve_one _ _ _ _); tc_solve) : typeclass_instances.

(* introduceHyp occurs after a branched has been dropped from solvesepfoc *)
Global Hint Extern 4 (AbsorbModal (IntroduceHyp _ _) _ _) =>
  (notypeclasses refine (absorb_modal_introducable _ _ _); tc_solve) ||
  (notypeclasses refine (absorb_modal_default_solve_one _ _ _ _); tc_solve) : typeclass_instances.

Global Hint Extern 4 (AbsorbModal (SolveSep _ _ _) _ _) => 
  notypeclasses refine (absorb_modal_solve_sep _ _ _ _ _ _ _) : typeclass_instances.

Global Hint Extern 4 (AbsorbModal (SolveOne _ _) _ _) => 
  notypeclasses refine (absorb_modal_solve_one _ _ _ _ _ _) : typeclass_instances.


Section absorb_exist_instances.
  Context {PROP : bi}.

  Global Instance absorb_exist_default_teleO (P : TeleO -t> PROP) :
    AbsorbExists P P | 99.
  Proof. rewrite /AbsorbExists //=. Qed.

  Global Instance absorb_exist_default `(P : TT -t> PROP) :
    AbsorbExists P (∃.. tt, tele_app P tt) | 99.
  Proof. rewrite /AbsorbExists. reflexivity. Qed.

  (* TODO: Do we need these classes in more places? *)
  Inductive TCEqRefine {A : Type} (x : A) : A → Prop :=  TCEqRefine_refl : TCEqRefine x x.
  Existing Class TCEqRefine.

  Class UnbindSolveSep (I O : PROP) :=
    unbind_solve_sep : I = O.

  Lemma absorb_exist_solvesep (TTi TT : tele) `(P : TT -t> PROP) (L Q : TT -t> TTi -t> PROP) R :
    (TC∀.. tt : TT, 
      TCEqRefine (SolveSep (TT := TTi) id (tele_app L tt) (tele_app Q tt)) (tele_app P tt)
    ) →
    UnbindSolveSep (SolveSep (TT := TelePairType TT TTi) id (as_pair_fun L) (as_pair_fun Q)) R →
    AbsorbExists P R.
  Proof.
    rewrite /AbsorbExists. unseal_diaframe => /=.
    rewrite /TCTForall !tforall_forall => HPQ <-.
    apply bi_texist_elim => tt.
    rewrite (tele_arg_concat_inv_eq tt). destruct (tele_arg_concat_inv_strong_hd tt) as [tt1 tt2]. clear tt.
    rewrite -(dependent_arg_eq _ _ tt1 tt2).
    rewrite (as_pair_fun_on_pair_arg L).
    rewrite (as_pair_fun_on_pair_arg Q).
    generalize (as_non_dependent_arg TT TTi tt1 tt2) => tt2'. clear tt2.
    rewrite bi_texist_exist -bi.exist_intro. rewrite -HPQ.
    by rewrite bi_texist_exist -bi.exist_intro.
  Qed.
  Global Existing Instance absorb_exist_solvesep.
End absorb_exist_instances.

Global Hint Extern 4 (TCEqRefine _ _) => refine (TCEqRefine_refl _) : typeclass_instances.
Global Hint Extern 4 (UnbindSolveSep ?I ?O) =>
    unseal_solvesep; unseal_solveone;
    cbn [as_pair_fun tele_curry tele_dep_bind as_dependent_fun tele_app tele_fold as_dependent_fun tele_arg_rect];
    reflexivity : typeclass_instances.

Section commit_left_disjunct_instances.
  Context {PROP : bi}.
  Implicit Types P Q : PROP.

  Global Instance commit_left_and P Q : CommitLeftDisjunct (P ∧ Q) := ltac:(by constructor).
  Global Instance commit_left_solve_and P Q : CommitLeftDisjunct (SolveAnd P Q) := ltac:(by constructor).
  Global Instance commit_left_wand P Q : CommitLeftDisjunct (P -∗ Q) := ltac:(by constructor).
  Global Instance commit_left_intro_hyp P Q : CommitLeftDisjunct (IntroduceHyp P Q) := ltac:(by constructor).
  Global Instance commit_left_forall `(Q : A → PROP) : CommitLeftDisjunct (bi_forall Q) := ltac:(by constructor).
  Global Instance commit_left_intro_vars `(Q : TT -t> PROP) : CommitLeftDisjunct (IntroduceVars Q) := ltac:(by constructor).
  Global Instance commit_left_if_later P : CommitLeftDisjunct P → CommitLeftDisjunct (▷ P) := λ _, ltac:(by constructor).
End commit_left_disjunct_instances.


Section as_solve_goal_continue_instances.
  Context {PROP : bi}.
  Implicit Types (M : PROP → PROP).

  Lemma as_solve_goal_from_absorb_modal P M Q :
    AbsorbModal P M Q →
    AsSolveGoal M P Q.
  Proof. done. Qed.

  Global Instance solve_sep_as_solve_goal_from_modal {TT} M P R M' Q :
    AbsorbModal (SolveSep (TT := TT) M P R) M' Q →
    AsSolveGoal M' (SolveSep (TT := TT) M P R) Q | 20.
  Proof. apply as_solve_goal_from_absorb_modal. Qed.

  Global Instance solve_one_as_solve_goal {TT} M P M' Q :
    AbsorbModal (SolveOne (TT := TT) M P) M' Q →
    AsSolveGoal M' (SolveOne (TT := TT) M P) Q | 20.
  Proof. apply as_solve_goal_from_absorb_modal. Qed.

  Global Instance solve_and_as_solve_goal M P1 P2 Q :
    AbsorbModal (SolveAnd P1 P2) M Q →
    AsSolveGoal M (SolveAnd P1 P2) Q | 20.
  Proof. apply as_solve_goal_from_absorb_modal. Qed.

  Global Instance introduce_empty_vars_as_solve_goal (G : PROP) M Q :
    AbsorbModal (IntroduceVars (TT := [tele]) G) M Q →
    AsSolveGoal M (IntroduceVars (TT := [tele]) G) Q | 20.
  Proof. apply as_solve_goal_from_absorb_modal. Qed.


  (* These should never be found with a modality differing from id? *)
  Global Instance solve_sep_foc_as_solve_goal {TT} M P R G :
    AsSolveGoal id (SolveSepFoc (TT := TT) M P R G) (SolveSepFoc (TT := TT) M P R G) | 20.
  Proof. rewrite /AsSolveGoal //. Qed.

  Global Instance solve_one_foc_as_solve_goal {TT} M P G :
    AsSolveGoal id (SolveOneFoc (TT := TT) M P G) (SolveOneFoc (TT := TT) M P G) | 20.
  Proof. rewrite /AsSolveGoal //. Qed.
End as_solve_goal_continue_instances.


Section prepare_for_retry.
  Context {PROP : bi}.

  Global Instance prepare_easy_for_retry {TT : tele} (Q : TT -t> PROP) :
    (TC∀.. ttr, IsEasyGoal (tele_app Q ttr)) →
    PrepareForRetry Q (tele_map (flip bi_sep (emp ∗ (emp -∗ emp))%I) Q) | 20. 
    (* emp will get normalized, (emp -∗ emp) won't. if no normalization occurs, emp will take a leftover modality,
       emp -∗ emp won't since a wand *)
  Proof. rewrite /PrepareForRetry => _ tt. by rewrite tele_map_app /= !left_id right_id. Qed.

  Global Instance prepare_foc_disj_ex_for_retry {TT : tele} (Q Q' : TT -t> PROP) D :
    PrepareForRetry (TT := TT) Q Q' →
    PrepareForRetry (TT := [tele]) (FocDisjEx (TT := TT) Q D) ((∃.. tt, tele_app Q' tt) ∨ D)%I | 10.
  Proof.
    rewrite /PrepareForRetry /= => HQ. apply tforall_forall => /=.
    rewrite FocDisjEx_eq /FocDisjEx_def. apply bi.or_mono; last done.
    apply bi_texist_mono => tt'. apply HQ.
  Qed.

  Global Instance prepare_retry_default {TT : tele} (Q : TT -t> PROP) :
    PrepareForRetry Q Q | 99.
  Proof. by rewrite /PrepareForRetry. Qed.

  Global Instance easy_goal_from_foc_disj_ex_easy {TT : tele} (Q : TT -t> PROP) D :
    IsEasyGoal (FocDisjEx Q D) →
    (TC∀.. tt, IsEasyGoal (tele_app Q tt)).
  Proof. move => _. apply tforall_forall => tt. exact I. Qed.
End prepare_for_retry.


Section drop_modal_instances.
  Context {PROP : bi}.
  Implicit Types R : PROP.

  (* this causes better inspection on things like □ ⋄ ⌜φ⌝ *)
  Global Instance drop_modal_persistently R:
    DropModal (bi_intuitionistically)%I R (λ P, TCAnd (Persistent P) (Affine P)).
  Proof.
    rewrite /DropModal => P HP.
    iIntros "HPR #HP".
    iApply "HPR". iApply "HP".
  Qed.

  Global Instance drop_modal_affine R :
    DropModal (bi_affinely) R (λ P, Affine P).
  Proof.
    rewrite /DropModal => P HP.
    iIntros "HPR HP".
    by iApply "HPR".
  Qed.

  Global Instance IntoModal_affinely p R : 
    IntoModal p (<affine> R) bi_affinely R | 30.
  Proof. rewrite /IntoModal bi.intuitionistically_if_elim //. Qed.
End drop_modal_instances.


Section misc.
  Context {PROP : bi}.
  (* TODO: where to put this? *)

  Global Instance exclusive_prop_merge_false Q : 
    ExclusiveProp Q →
    MergableConsume (PROP := PROP) Q true (λ p Pin Pout,
      TCAnd (TCEq Pin Q) $
            TCEq Pout False%I).
  Proof.
    rewrite /ExclusiveProp /MergableConsume => HQ p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim HQ //.
  Qed.
End misc.

