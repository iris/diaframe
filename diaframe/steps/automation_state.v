From diaframe Require Import utils_ltac2.
From diaframe.steps Require Import ltac2_store ipm_hyp_utils.
From diaframe.hint_search Require Import reify_prop.
From iris.proofmode Require Import environments.


Ltac2 Type goal_id := { goal_id_internal : int }.
Ltac2 new_goal_id (_ : unit) := { goal_id_internal := 1 }.

Ltac2 split_goal_ids (g : goal_id) : goal_id * goal_id :=
  let g_id := g.(goal_id_internal) in
  (
    {goal_id_internal := Int.mul g_id 2}
  ,
    {goal_id_internal := Int.add (Int.mul g_id 2) 1}
  ).

Ltac2 goal_id_eq : goal_id -> goal_id -> bool :=
  fun g1 g2 => Int.equal (g1.(goal_id_internal)) (g2.(goal_id_internal)).

Module automation_state.
  Ltac2 Type t := {
    goal_hyp_prop_store : (goal_id, (ipm_ident, bi_prop) store.t) store.t;
    goal_step_store : (goal_id, int) store.t;
  }.

  Ltac2 new_int (_ : unit) : t :=
    { goal_hyp_prop_store := store.new goal_id_eq;
      goal_step_store := store.new goal_id_eq }.

  Ltac2 new (_ : unit) : t * goal_id :=
    let state := new_int () in
    let gid := new_goal_id () in
    store.set (state.(goal_hyp_prop_store)) gid (store.new ipm_ident_equal);
    store.set (state.(goal_step_store)) gid -1;
    (state, gid).

  Ltac2 split_goal (state : t) (g : goal_id) : goal_id * goal_id :=
    let old_hyp_prop_store := Option.get (store.get (state.(goal_hyp_prop_store)) g) in
    let old_step_store := Option.get (store.get (state.(goal_step_store)) g) in
    store.remove (state.(goal_hyp_prop_store)) g;
    store.remove (state.(goal_step_store)) g;
    let (g1, g2) := split_goal_ids g in
    store.set (state.(goal_hyp_prop_store)) g1 old_hyp_prop_store;
    store.set (state.(goal_hyp_prop_store)) g2 (store.copy old_hyp_prop_store);
    store.set (state.(goal_step_store)) g1 old_step_store;
    store.set (state.(goal_step_store)) g2 old_step_store;
    g1, g2.

  Ltac2 get_rp_store_for (state : t) (g : goal_id) : (ipm_ident, bi_prop) store.t :=
    Option.get (store.get (state.(goal_hyp_prop_store)) g).

  Ltac2 set_rp_store_for (state : t) (g : goal_id) (store : (ipm_ident, bi_prop) store.t) : unit :=
    store.set (state.(goal_hyp_prop_store)) g store.

  Ltac2 get_steps_for (state : t) (g : goal_id) : int :=
    Option.get (store.get (state.(goal_step_store)) g).

  Ltac2 set_steps_for (state : t) (g : goal_id) (steps : int) : unit :=
    store.set (state.(goal_step_store)) g steps.
End automation_state.


Ltac2 dispatch_and_join (gids : goal_id list) (tac : goal_id -> 'a list) : 'a list :=
  let gid_store : (goal_id, 'a list) store.t := store.new goal_id_eq in
  Control.dispatch (List.map (fun gid' _ =>
    let result := tac gid' in
    store.set gid_store gid' result
  ) gids);
  let to_join : 'a list list := List.map (fun gid' => Option.get (store.get gid_store gid')) gids in
  List.flatten to_join.

Ltac2 handle_prop_deletion_on_goal (state : automation_state.t) (gid : goal_id) :=
  let (mi, rp, pers) :=
    lazy_match! goal with
    | [ |- envs_entails (envs_delete ?rp ?i ?pers _) _] =>
      (Some i, rp, pers)
    | [ |- envs_entails (envs_option_delete ?rp ?mi ?pers _) _] =>
      lazy_match! mi with
      | Some ?i => (Some i, rp, pers)
      | None => (None, rp, pers)
      end
    end 
  in
  match mi with
  | Some i =>
    let actual_delete :=
      lazy_match! pers with
      | false => true
      | true =>
        lazy_match! rp with
        | false => false
        | true => true
        end
      end
    in
    if actual_delete then
      let store := Option.get (store.get (state.(automation_state.goal_hyp_prop_store)) gid) in
      store.remove store (ipm_ident_of_constr i)
    else
      ()
  | None => ()
  end.








