From iris.proofmode Require Import base coq_tactics reduction tactics string_ident.
From diaframe Require Import util_classes util_instances solve_defs tele_utils utils_ltac2.
From diaframe.steps Require Import small_steps solve_instances ipm_hyp_renamer pure_solver_utils automation_state ltac2_store diaframe_options.
From diaframe.steps Require Export disj_chooser_tacs renaming_tacs goal_inspector.
From iris.bi Require Import bi telescopes.

Import bi.

(* This file bundles all files in the steps/ folder, to create the iStep tactic (and friends).
*)

Export solve_defs.

Lemma from_as_solve_goal {PROP : bi} Δ (P P' : PROP) :
  AsSolveGoalInternal P P' →
  envs_entails Δ P' →
  envs_entails Δ P.
Proof. rewrite /AsSolveGoalInternal => <- //. Qed.

Lemma as_emp_valid_weak {PROP : bi} (φ : Prop) (P : PROP) `{!AsEmpValidWeak φ P} :
  (⊢ P) → φ.
Proof. by apply as_emp_valid_weak. Qed.

Ltac iStartProof2 :=
  first 
  [ iStartProof 
  | (notypeclasses refine (as_emp_valid_weak _ _ _); [tc_solve || fail "iStartProof2: not a BI assertion" | iStartProof; simpl]) ].

Ltac asSolveGoalStep :=
  notypeclasses refine (from_as_solve_goal _ _ _ _ _ );
    [tc_solve
    |simpl].


Ltac2 Type iStep_option_set := {
  disj_chooser : automation_state.t -> goal_id -> PickEvent.t -> (PickSide.t -> unit) -> unit;
  pure_handler : pure_solver_result -> bool;
  rename_instrs : rename_instructions list option;
  error_verbosity : rename_error_verbosity;
  (* tactic to call after every iStep. Provided arguments:
     - goal_id before calling
     - list of goal_ids after running (= list of open goals passed to tactic!)
     - a closure which will disable further processing of goal_id
     Return value should be another list of goal_ids, after running the 'inspector'*)
  goal_inspector : goal_id -> goal_id list -> (goal_id -> unit) -> goal_id list;
}.

Ltac2 iStep_base_config_steps (opts : iStep_option_set) state gid' : goal_id list :=
  ltac1:(asSolveGoalStep); Control.progress (fun _ =>
    solveStepsWithOptTacs (opts.(disj_chooser)) (opts.(pure_handler)) state gid'
  ).


(** Runs a single [iStep] automation *)
Ltac2 iStep_ (opts : iStep_option_set) :=
  Control.enter (fun _ =>
    ltac1:(iStartProof2); 
    let (state, gid) := automation_state.new () in
    let _ := iStep_base_config_steps opts state gid in
    ()
  ).



Ltac2 register_stopped (tac : goal_id -> goal_id list) : 
  (goal_id -> goal_id list) * (goal_id -> unit) * (goal_id list -> bool) :=
  (* The first projection will behave as the provided tactic, _unless_ 
      the tactic already failed on that goal. In that case, it will just
      return [goal_id].
     The second projection is a 'disabler', with which one can manually disable the provided goal_id
     The third projection will return whether tac has failed on all provided goal_ids.

    We used to shelve goals that could not make progress. This is easier to implement,
    but it disturbs the natural goal order *)
  let stopped_gid_store := store.new goal_id_eq in
  (
    (fun gid =>
      match store.get stopped_gid_store gid with
      | Some _ => [gid]
      | None =>
        plus_once (fun _ => tac gid)
                  (fun _ => store.set stopped_gid_store gid (); [gid])
      end)
    ,
    (fun gid =>
      store.set stopped_gid_store gid ())
    ,
    (fun gids =>
      Int.equal (List.length gids) (store.size stopped_gid_store))
  ).


Ltac2 with_inspection_and_stopping (tac : goal_id -> goal_id list) (opts : iStep_option_set) : 
  (goal_id -> goal_id list) * (goal_id list -> bool) :=
  let (stopping_tac, gid_disabler, stop_condition) := register_stopped tac in
  let inspected_tac := (fun gid' =>
    let gids := stopping_tac gid' in
    opts.(goal_inspector) gid' gids gid_disabler
  ) in
  (inspected_tac, stop_condition).


(** Runs a given amount of [iStep] automation *)
Ltac2 iStep_num (opts : iStep_option_set) (n : int) :=
  Control.enter (fun _ =>
    ltac1:(iStartProof2); 
    let (state, start_gid) := automation_state.new () in
    let (inspected_stopping_tac, stop_condition) := 
      with_inspection_and_stopping (iStep_base_config_steps opts state) opts in
    let rec go m gids :=
      if Bool.or (Int.equal m 0) (stop_condition gids) then
        ()
      else
        match gids with
        | [] => ()
        | _ =>
          let gids' := dispatch_and_join gids inspected_stopping_tac in
          go (Int.sub m 1) gids'
        end
    in
    go n [start_gid]
  ).


Ltac2 unshelve (c : unit -> unit) :=
  ltac1:(cont |- unshelve (cont ())) 
  (Ltac1.lambda (fun _ => c (); ltac1val:(idtac))).

Ltac2 rec repeat_with_gids 
    (tac : goal_id -> goal_id list)
    (gids : goal_id list)
    (stop_condition : goal_id list -> bool) :=
  let gids' := dispatch_and_join gids tac in
  if stop_condition gids' then
    ()
  else
    repeat_with_gids tac gids' stop_condition
  .

Ltac2 repeat_and_initialize (tac : automation_state.t -> goal_id -> goal_id list) (opts : iStep_option_set) :=
  (* assumes it is focused *)
  let (state, gid) := automation_state.new () in
  let (inspected_stopping_tac, stop_condition) :=
    with_inspection_and_stopping (tac state) opts in
  repeat_with_gids inspected_stopping_tac [gid] stop_condition.

(** Runs the [iStep] automation until it can no longer continue *)
Ltac2 iSteps_ (opts : iStep_option_set) := 
  Control.enter (fun _ =>
    ltac1:(iStartProof2);
    unshelve (fun _ => 
      Control.progress (fun _ => repeat_and_initialize (iStep_base_config_steps opts) opts)
    ); 
    Control.shelve_unifiable ()
  ).


Ltac2 compose_inspectors 
    (f1 : goal_id -> goal_id list -> (goal_id -> unit) -> goal_id list)
    (f2 : goal_id -> goal_id list -> (goal_id -> unit) -> goal_id list) :
          goal_id -> goal_id list -> (goal_id -> unit) -> goal_id list :=
  fun gid gids dis =>
    let gids' := f1 gid gids dis in
    f2 gid gids' dis.


Ltac2 iStep_default_option_set (_ : unit) := {
  disj_chooser := chooser_always_left;
  pure_handler := handle__shelve_unsolved_noevars;
  rename_instrs := None;
  error_verbosity := diaframe_default_rename_error_verbosity;
  goal_inspector := (fun _ gids _ => gids);
}.

Ltac2 set_or_err (cell : 'a option ref) (val : 'a) (m : message) :=
  match cell.(contents) with
  | None =>
    cell.(contents) := Some val
  | _ =>
    Control.throw (Tactic_failure (Some m))
  end.

Ltac2 set_or_append (cell : 'a list option ref) (val : 'a) :=
  match cell.(contents) with
  | None =>
    cell.(contents) := Some [val]
  | Some vs =>
    cell.(contents) := Some (List.append vs [val])
  end.

Ltac2 get_or_default (cell : 'a option ref) (default : 'a) :=
  match cell.(contents) with
  | None => default
  | Some v => v
  end.

(** "--help" meta tactic to display options *)
Ltac2 Type diaframe_option ::= [ DiaframeHelp ].

Tactic Notation "--help" :=
  ltac2:(throw_option (DiaframeHelp)).

Ltac2 help_message (_ : unit) : string := 
"Syntax:

iStep <list of options, separated by '/'>

For example:

iStep as (v) 'Hv' / as (w) 'Hw' / --safe

Specifying multiple 'as' clauses is for when the tactic spawns multiple subgoals.


List of available options to Diaframe's tactics:

--help
   displays this message
as (simple_intropattern_list) 'ipm_intro_patterns'
   reintroduces new variables and hypotheses according to the provided names, in a single branch
as_anon
   marking a spawned branch as not to be renamed
--verbose
   makes sure Diaframe displays any possible renaming error (default behavior)
--silent
   makes sure Diaframe does not display any possible renaming error
--print-goals
   prints the IPM goal as it was after each successive iStep
--until goal-matches (_your_goal_pattern_)%I
   stops using iStep once your goal can be unified with the provided pattern
--until program-matches (_your_expression_)%E
   stops using iStep once your goal can be unified with (WP _your_expression_ {{ _ }}). Currently only included with the HeapLang automation.
--until ltac:(idtac; your_tactic)
   stops using iStep once your_tactic succeeds. The preceding idtac is necessary to 'thunk' your tactic.
--safe
   makes Diaframe slightly more careful before committing to sides of a disjunction
--safest
   makes Diaframe even more careful about disjunctions, no longer dropping unlikely sides
".


Ltac2 parse_iStep_options (option_tac1s : Ltac1.t list) : iStep_option_set :=
  let default_opts := iStep_default_option_set () in
  let the_chooser := { contents := None } in
  let the_instrs := { contents := None } in
  let verbosity := { contents := None } in
  let inspector := { contents := default_opts.(goal_inspector) } in
  List.iter (fun option_tac1 =>
    let err := catch_ltac1 option_tac1 (Some (fprintf "This is not a valid Diaframe option.")) in
    match err with
    | Diaframe_option_exn _ o =>
      match o with
      | DisjChooser c =>
        set_or_err the_chooser c (fprintf "Disjunction behavior already set: please use exactly one of the --safe or --safest options.")
      | OptRenaming i =>
        set_or_append the_instrs i
      | OptRenameErrorVerbosity v =>
        set_or_err verbosity v (fprintf "Rename error verbosity already set: please use exactly one of the --verbose or --silent options.")
      | GoalInspector i =>
        inspector.(contents) := compose_inspectors (inspector.(contents)) i
      | DiaframeHelp =>
        Message.print (Message.of_string (help_message ()));
        Control.throw Assertion_failure
      | _ =>
        Control.throw (Invalid_argument (Some (fprintf "Unknown option %a" (fun () () => Message.of_exn err) ())))
      end
    | _ => 
      Control.throw (Invalid_argument (Some (fprintf "This is not a valid Diaframe option %a" (fun () () => Message.of_exn err) ())))
    end
  ) option_tac1s;
  {
    disj_chooser := get_or_default the_chooser (default_opts.(disj_chooser));
    pure_handler := default_opts.(pure_handler);
    rename_instrs := the_instrs.(contents);
    error_verbosity := get_or_default verbosity (default_opts.(error_verbosity));
    goal_inspector := inspector.(contents);
  }.


Ltac2 rename_with_opts (closure : unit -> unit) (opts : iStep_option_set) :=
  match opts.(rename_instrs) with
  | None => closure ()
  | Some instrs =>
    Control.enter (fun _ =>
      rename_both_after_closure
        (opts.(error_verbosity))
        instrs
        closure
    )
  end.



Tactic Notation "iStep" tactic1_list_sep(options, "/") := 
  let f := ltac2:(tac1_opts |-
    let opts := parse_iStep_options (Option.get (Ltac1.to_list tac1_opts)) in
    rename_with_opts (fun () => iStep_ opts) opts
  ) in
  f options.


(* JOY *)
Ltac2 from_ltac1_int (num : Ltac1.t) : int :=
  let resultc := { contents := 0 } in
  let incr _ := resultc.(contents) := Int.add (resultc.(contents)) 1 in
  let tac1_incr := Ltac1.lambda (fun _ => incr (); ltac1val:(idtac)) in
  let tac1_go := ltac1:(n tac |- do n (tac ())) in
  tac1_go num tac1_incr;
  resultc.(contents).
(* we better hope no-one calls iStep 90000.
  FIXME: Once coq#18121 is fixed, there should be a Ltac1.to_int function *)

Tactic Notation "iStep" integer(n) tactic1_list_sep(options, "/") :=
  let f := ltac2:(tac1int tac1_opts |-
    let opts := parse_iStep_options (Option.get (Ltac1.to_list tac1_opts)) in
    let amnt := (from_ltac1_int tac1int) in
    rename_with_opts (fun () => iStep_num opts amnt) opts
  ) in
  f n options.


Tactic Notation "iSteps" tactic1_list_sep(options, "/") :=
  let f := ltac2:(tac1_opts |-
    let opts := parse_iStep_options (Option.get (Ltac1.to_list tac1_opts)) in
    rename_with_opts (fun () => iSteps_ opts) opts
  ) in
  f options.


Ltac iStepDebug :=
  iStartProof2; asSolveGoalStep.



(* constructing iSmash takes a bit more effort *)
Ltac2 smashStep state gid : goal_id list :=
  (* assumes a single goal to be focused *)
  orelse (fun _ =>
    Control.progress (fun _ =>
      solveStepWithOptTacs chooser_smash handle__rollback_unsolved state gid
    )
  ) (fun _ =>
    progress ltac1:(asSolveGoalStep); [gid]
  ).

(* Note that an individual smashStep may have backtracking (with (+)). Ensuring this behaves well,
  also in a multigoal setting, is somewhat tricky. See also the tests in tests/other/and_parallelism.v *)
Ltac2 smashSteps (_ : unit) : unit :=
  Control.enter (fun _ =>
    let (state, new_gid) := automation_state.new () in
    let rec go (gids : goal_id list) :=
      Control.dispatch (List.map (fun gid _ =>
        let gids' := smashStep state gid in
        Control.once (fun _ => 
           (* cannot use solve, it enters all goals *)
            unshelve (fun _ => go gids'); 
            apply inhabitant;
            Control.enter (fun () => Control.zero (Tactic_failure None))
        )
      ) gids)
    in
    go [new_gid]
  ).

Ltac iSmash := once (iStartProof2); ltac2:(smashSteps ()).


(* Used for the iDecompose tactic, which reruns a hypothesis through Diaframe's introduction steps *)
Lemma tac_ireintro {PROP : bi} i (Δ : envs PROP) p P G :
  envs_lookup i Δ = Some (p, P) →
  envs_entails (envs_delete true i p Δ) (IntroduceHyp (□?p P) G) →
  envs_entails Δ G.
Proof.
  unseal_diaframe => HΔi.
  rewrite envs_entails_unseal => HPG.
  rewrite envs_lookup_sound // HPG.
  apply bi.wand_elim_r.
Qed.

Ltac iDecompose_base hyp_name :=
  ((eapply (tac_ireintro (INamed hyp_name)); [reflexivity | simpl] ) || 
    fail 1 "Could not decompose" hyp_name ", is it present in your context?"
  );
  repeat (introStep).


Tactic Notation "iDecompose" constr(hyp_name) tactic1_list_sep(options, "/") :=
  let f := ltac2:(hyp1 tac1_opts |-
    let opts := parse_iStep_options (Option.get (Ltac1.to_list tac1_opts)) in
    rename_with_opts (fun _ => ltac1:(hyp |- iDecompose_base hyp) hyp1) opts
  ) in
  f hyp_name options.

















