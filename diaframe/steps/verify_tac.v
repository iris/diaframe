From diaframe Require Import util_classes.
From diaframe.steps Require Import tactics.
From iris.base_logic.lib Require Import iprop own.
From iris.proofmode Require Import proofmode.

From Coq.Program Require Import Tactics.


(* This file uses the iStep tactic to build the tactic 'verify_tac' and 'program_verify_tac'.
   These are used when we define SPECs using Program Instance. The main work we still have to do here is 
   possibly performing Löb induction - and if so, to generalize this over as many variables as possible.
*)

Ltac inside_step :=
  lazymatch goal with
  | |- subG _ _ → _ => solve_inG
  | |- IntoWand2 _ _ _ _ => rewrite /IntoWand2 /=; iStartProof
  | |- forall n : nat, respectful (dist n) (dist n) ?P ?P => solve_proper
  | |- forall n : nat, respectful (dist_later n) (dist n) ?P ?P => solve_contractive
  | |- (∀ a, _) => 
    intros a; 
    inside_step
    (* sometimes Tactics.reverse reverts let bindings, reintroduce those and continue *)
  | |- (let _ := _ in _) => intros a; inside_step
  | |- ?P ⊢ <pers> ?P => fold (Persistent P); tc_solve
  | |- ▷ ?P ⊢ ◇ ?P => fold (Timeless P); tc_solve
  | |- ?P => simpl; (tc_solve || iStartProof || iStartProof2 => /= || pure_solver.trySolvePure)
  end.

Ltac finish_remaining2 rem_goal_tac :=
  match goal with
  | |- environments.envs_entails _ ?G => try solve [rem_goal_tac; try apply inhabitant]
  | |- _ => try apply inhabitant
  end.

Ltac verify_by prefix_tac rem_goal_tac :=
  try (unshelve (prefix_tac; inside_step; finish_remaining2 ltac:(rem_goal_tac)); try apply inhabitant).

Ltac verify_tac := verify_by ltac:(reverse) ltac:(iSteps).
Ltac smash_verify_tac := verify_by ltac:(reverse) ltac:(iSmash).

(* when starting a program, hypotheses are more properly reversed than what reverse itself manages. So skip reversing *)
Ltac program_verify := verify_by ltac:(idtac) ltac:(iSteps).
Ltac program_smash_verify := verify_by ltac:(idtac) ltac:(iSmash).

(* we would like to do Instance blabla : SPEC ... := verify. 
  However, it seems this causes an unacceptable slowdown 
  (see https://coq.discourse.group/t/instances-created-with-ltac-are-much-slower-than-definitions/1458 )
  Alternatively, we can locally set Obligation Tactic := verify_tac/smash_verify_tac *)

(* we could also use 'Proof verify.' But this syntax might be deprecated one day
  ( https://github.com/Zimmi48/ceps/blob/unify-theorem-definition-fixpoint/text/042-unify-definition-fixpoint-syntax.md )
  and is unusable by users of the Proof General IDE
  ( https://github.com/ProofGeneral/PG/issues/498 ) *)
Notation verify := ltac:(verify_tac) (only parsing).
Notation smash_verify := ltac:(smash_verify_tac) (only parsing).




