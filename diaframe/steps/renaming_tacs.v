From diaframe Require Import utils_ltac2 steps.ipm_hyp_renamer steps.diaframe_options steps.ipm_hyp_utils.
From iris.proofmode Require Import proofmode environments string_ident intro_patterns.



(** Option for renaming Coq variables and IPM hypotheses in a single branch.*)
Ltac2 Type rename_instructions := [
  | InstrNoRenaming
  | InstrRenaming (Ltac1.t list, constr list) 
       (* list of intropatterns, list of IPM intro patterns *)
].
Ltac2 Type diaframe_option ::= [ OptRenaming (rename_instructions) ].


Tactic Notation "as_anon" :=
  ltac2:(throw_option (OptRenaming InstrNoRenaming)).

Tactic Notation "as" "(" simple_intropattern_list(pure_pats) ")" constr(ipm_pats) :=
  let f := ltac2:(pure_pats_raw ipm_pats_raw |-
    let pure_pats := Option.get (Ltac1.to_list pure_pats_raw) in
    let ipm_pats := parse_intro_pats (Option.get (Ltac1.to_constr ipm_pats_raw)) in

    throw_option (OptRenaming (InstrRenaming pure_pats ipm_pats))
  ) in
  f pure_pats ipm_pats.

Tactic Notation "as" "(" simple_intropattern_list(pure_pats) ")" :=
  let f := ltac2:(pure_pats_raw |-
    let pure_pats := Option.get (Ltac1.to_list pure_pats_raw) in
    let ipm_pats := [] in

    throw_option (OptRenaming (InstrRenaming pure_pats ipm_pats))
  ) in
  f pure_pats.

Tactic Notation "as" constr(ipm_pats) :=
  let f := ltac2:(ipm_pats_raw |-
    let pure_pats := [] in
    let ipm_pats := parse_intro_pats (Option.get (Ltac1.to_constr ipm_pats_raw)) in

    throw_option (OptRenaming (InstrRenaming pure_pats ipm_pats))
  ) in
  f ipm_pats.

(*
Goal True.
Fail as (x y z [c d]).
Fail as (x y z [c|d]) "Ha Hb".
Fail as "Ha Hb".
Fail as_anon.
Abort.
*)

Ltac2 rename_both_in_single_branch
  (register_closure : (unit -> unit) -> unit)
  (initial_coq_names : ident list)
  (initial_ipm_names : ipm_ident list)
  (instr : rename_instructions) :=
  match instr with
  | InstrNoRenaming => ()
  | InstrRenaming coq_intro_pats ipm_intro_pats =>
    reintroduce_with_pats 
      register_closure
      initial_coq_names
      coq_intro_pats;
    (* in the case of disjunctive patterns: *)
    Control.enter (fun _ =>
      ipm_hyp_renamer
        register_closure
        initial_ipm_names
        ipm_intro_pats
        ()
    )
  end.



Ltac2 Type diaframe_option ::= [ OptRenameErrorVerbosity (rename_error_verbosity) ].


Ltac2 mutable diaframe_default_rename_error_verbosity : rename_error_verbosity
  := VerboseRenameErrors.

(* You can use
Ltac2 Set diaframe_default_rename_error_verbosity := RenamingErrorSilent.
to set turn off printing bad renaming by default *)

Tactic Notation "--silent" := 
  ltac2:(throw_option (OptRenameErrorVerbosity SilenceRenameErrors)).

Tactic Notation "--verbose" := 
  ltac2:(throw_option (OptRenameErrorVerbosity VerboseRenameErrors)).


Ltac2 rename_both_after_closure
  (b : rename_error_verbosity)
  (rename_instrs_raw : rename_instructions list)
  (closure : unit -> unit) :=
  let probs := no_problems () in

  let (coq_idents_getter, cleanup) := find_fresh_idents_between () in
  let initial_ipm_names : ipm_ident list := 
    List.map (fun pr => let (i, _ ) := pr in i) (get_ipm_hyp_names ()) in

  closure ();

  shelve_record_current_variables_goals ();
  initialize_subgoal_problems probs;

  let default_instr := InstrRenaming [] [] in

  let rename_instrs := to_list_of_size rename_instrs_raw (count_goals ()) default_instr
      (fun i => add_global_problem probs (fun _ => 
        printf "Too little branches of naming instructions supplied! Missing %i branches" i 
      ))
      (fun i => add_global_problem probs (fun _ =>
        printf "Too many branches of naming supplied! Dropping %i branches" i
      ))
  in

  Control.dispatch (List.mapi
    (fun i rename_instr () =>
      rename_both_in_single_branch
        (add_subgoal_problem probs i)
        (coq_idents_getter ())
        initial_ipm_names
        rename_instr
    )
    rename_instrs
  );
  cleanup ();
  match b with
  | SilenceRenameErrors => ()
  | VerboseRenameErrors =>
    print_problems probs
  end.

