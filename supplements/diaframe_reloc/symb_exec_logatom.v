From diaframe.symb_exec Require Import defs weakestpre.
From diaframe.reloc Require Import proof_automation atomic_post_update symb_exec.
From diaframe.heap_lang Require Import proof_automation atomic_specs.
From iris.bi.lib Require Import laterable.

Section atomic_compat.
  Context `{!relocG Σ}.

  (* So the ReLoC's refinement definition supports applying Iris's logically atomic triples,
     given the following:
      - the triple has an empty implementation mask
      - the triple has no private postcondition.
     If either of these fails to hold, we cannot express the resulting goal in terms of 
     the original definition. *)
  Lemma atomic_templateM_satisfies_wp_template_condition P1 (TT1 TT2 : tele) P2 Q1 Q2 e A Ef :
    (∀.. (tt1 : TT1) (tt2 : TT2), TCEq (tele_app (tele_app Q1 tt1) tt2) True%I) → 
    defs.SatisfiesTemplateCondition left_template_condition
        [tele_arg3 e; ⊤; A] (atomic_templateM ∅ P1 TT1 TT2 P2 Q1 Q2) 
        [tele_arg3 e; Ef; A] (atomic_post_templateM ∅ P1 Ef TT1 TT2 P2 Q1 Q2).
  Proof.
    do 3 split => //.
    - move => Ψ.
      iIntros ">[$ H] !> !>". rewrite atomic_post_update_equiv.
      iApply atomic_post_update_mask_change.
      iApply (atomic_post_update_mono with "H").
      iIntros "!>". iSplit; eauto.
      iIntros (a). iExists id. iSplit; eauto.
      iIntros (b) "HQ". simpl. revert H => /(dep_eval_tele a) /(dep_eval_tele b) ->.
      iIntros "!>".
      rewrite difference_empty_L.
      iMod ("HQ" with "[]") as "$"; eauto.  
    - split => //.
      apply atomic_post_templateM_is_mono.
      apply defs.absorbing_to_conditionally_absorbing, atomic_post_templateM_is_absorbing.
      tc_solve.
  Qed.
  Global Existing Instance atomic_templateM_satisfies_wp_template_condition.
End atomic_compat.

  (* That does not mean it is impossible to apply these: it can work, with an alternative 
     definition of refinement (with more parameters). The non-empty implementation mask problem
     could maybe also be fixed by defining logatom triples not with masks
      |={⊤ ∖ Ei, ∅}=>    but with masks    |={⊤, Ei}=>    .
     In any case, support for these situations requires changes outside of Diaframe.

     The approach we take, is to define the alternative definition of refinement here, and
      only apply it when the usual case is insufficient. This probably has the best user
      interface, since you only need to worry about a different refinement judgment in cases
      where it is necessary.

      The definition below in principle supports private post-conditions, but in practice does not.
      This is because that would require changes to the symb_exec setup: the execution arguments 
      would need to be allowed to depend on the quantified arguments, and this is currently not the case. *)
Section alt_def.
  Context `{!relocG Σ}.
  Definition refinesm_def (E1 E2 : coPset) (e : expr) (e'k : rhs_t) (Q : iProp Σ) (A : lrel Σ) : iProp Σ :=
    ∀ j : ref_id, match e'k with | inl e' => refines_right j e' | inr k => ⌜j = k⌝ end 
        ={E1, E2}=∗ Q -∗ WP e {{ v, ∃ v' : val, refines_right j v' ∗ A v v' } }.
  Definition refinesm_aux : seal (@refinesm_def). Proof. by eexists. Qed.
  Definition refinesm := refinesm_aux.(unseal).
  Lemma refinesm_eq : @refinesm = @refinesm_def.
  Proof. by rewrite -refinesm_aux.(seal_eq). Qed.

  Lemma refines_refinesm_equiv (E : coPset) (e : expr) (e'k : rhs_t) (A : lrel Σ) :
    refines E e e'k A ⊣⊢ refinesm E ⊤ e e'k emp A.
  Proof.
    rewrite refines_eq /refines_def refinesm_eq /refinesm_def.
    apply: (anti_symm _);
    apply bi.forall_mono => j; 
    apply bi.wand_mono => //;
    apply fupd_mono; by rewrite left_id.
  Qed.
End alt_def.

Global Notation "'REL2' e1 '<<' e2 '@' E1 ',' E2 ',' Q ':' A" :=
  (refinesm E1 E2 e1%E e2%E Q (A)%lrel)
  (at level 100, E1, E2, Q at next level, e1, e2 at next level,
   A at level 200,
   format "'[hv' 'REL2'  e1  '/' '<<'  '/  ' e2  '@'  E1 ,  E2 ,  Q  :  A ']'").

Section refinesm_exec.
  Context `{!relocG Σ}.
  Instance right_execute' : ExecuteOp (iPropI Σ) expr [tele_pair expr ; coPset ; coPset ; iProp Σ ; lrel Σ] :=
    λ e, (λᵗ tl E1 E2 Q A, REL2 tl << e @ E1, E2, Q : A).

  Global Arguments right_execute' e !R /.

  Global Instance as_right_execute' tl (tr : expr) E1 E2 Q A : AsExecutionOf (REL2 tl << tr @ E1, E2, Q : A) right_execute' tr [tele_arg3 tl; E1; E2; Q; A].
  Proof. done. Qed.

  Instance right_template_condition' : TemplateCondition (iPropI Σ) [tele_pair expr ; coPset ; coPset ; iProp Σ ; lrel Σ] 
    := (λ A R M R' M', R = R' ∧ M = M').

  Global Arguments right_template_condition' A R M /.

  Global Instance all_satisfies_right_template_condition' R `(M : (A → iPropI Σ) → iPropI Σ) : SatisfiesTemplateCondition right_template_condition' R M R M.
  Proof. by rewrite /SatisfiesTemplateCondition /=. Qed.

  Global Instance right_execute_reduction_compat' : 
    ExecuteReductionCompatibility right_execute' (λᵗ _ E1 _ _ _, E1) tp_reduction_condition context_as_item_condition right_template_condition'.
  Proof.
    move => K e A e' M /= HK R R' M' [<- <-]. destruct HK as [K K' HK]. rewrite -HK.
    drop_telescope R as tl E1 E2 Q Φ => /=.
    iIntros "[Hred HM]" => /=. rewrite refinesm_eq /refinesm_def.
    iIntros (j) "[#H3 H4]".
    rewrite -fill_app.
    iMod ("Hred" with "[$H3 $H4] HM") as (a) "[HP Hj]"; simpl. rewrite -HK.
    rewrite refinesm_eq /refinesm_def.
    iApply "HP" => //. iFrame "#".
    by rewrite fill_app.
  Qed.

  Instance left_execute' : ExecuteOp (iPropI Σ) expr [tele_pair rhs_t; coPset; coPset; iProp Σ; lrel Σ] :=
    λ e, λᵗ tr E1 E2 Q A, REL2 e << tr @ E1, E2, Q : A.

  Global Arguments left_execute' e !R /.

  Global Instance as_left_execute' tl tr E1 E2 Q A : AsExecutionOf (REL2 tl << tr @ E1, E2, Q : A) left_execute' tl [tele_arg3 tr; E1; E2; Q; A].
  Proof. done. Qed.

  Global Instance rel_as_left_execute tl tr E A : AsExecutionOf (REL tl << tr @ E : A) left_execute' tl [tele_arg3 tr; E; ⊤; emp%I; A].
  Proof. rewrite /AsExecutionOf /=. apply refines_refinesm_equiv. Qed.

  Instance left_template_condition' : TemplateCondition (iPropI Σ) [tele_pair rhs_t; coPset; coPset; iProp Σ; lrel Σ]
    := (λ Φ, λᵗ tr E1 E2 Q A, λ M, λᵗ tr' E1' E2' Q' A', λ M', 
        (A' = A ∧ tr' = tr ∧ Q = Q' ∧ (∀ Ψ, M' (fupd E1' E2' ∘ Ψ) ⊢ |={E1, E2}=> M Ψ) ∧ 
        (template_mono M' ∧ template_conditionally_absorbing M' Laterable ∧ template_strong_mono M))
    ).

  Global Arguments left_template_condition' A !R M !R' M' /.

  Definition template_subtracting {PROP : bi} `(M : ((A → PROP) → PROP)) : Prop :=
    ∀ Q Φ, M (λ a, Q -∗ Φ a)%I ⊢ Q -∗ M Φ.

  Lemma modality_strong_subtracting {PROP : bi} `(M : ((A → PROP) → PROP)) : template_strong_mono M → template_subtracting M.
  Proof.
    move => HM Q Φ.
    rewrite /template_strong_mono in HM.
    apply bi.wand_intro_l.
    apply bi.wand_elim_l'.
    rewrite -HM.
    apply bi.forall_intro => a.
    apply bi.wand_intro_l. rewrite bi.wand_elim_l //.
  Qed.

  Global Instance left_execute_reduction_compat' : 
    ExecuteReductionCompatibility left_execute' (λ _, [tele_arg3 ⊤ ; NotStuck]) wp_red_cond context_as_item_condition left_template_condition'.
  Proof.
    move => K e A e' M /= HK R R' M'. destruct HK as [K K' HK]. rewrite -HK.
    drop_telescope R as tr E1 E2 Q Φ => /=.
    drop_telescope R' as tr' E1' E2' Q' Φ' => /=.
    move => [-> [-> [-> [HMΨ [HM1 [HM2 HM3]]]]]].
    rewrite refinesm_eq /refinesm_def.
    iIntros "[Hred HM1]" (j) "Hj".
    rewrite /flip /compose /=.
    rewrite -wp_bind /=.
    iAssert (|={E1,E2}=> Q' -∗ M _)%I with "[-Hred]" as ">H"; last first. (* iEnough *)
    { iIntros "!> HQ". iApply "Hred". by iApply "H". }
    rewrite -modality_strong_subtracting //.
    rewrite -HMΨ. iStopProof. erewrite HM2; last (destruct tr; tc_solve).
    apply HM1 => a /=.
    iIntros "[HK Hj]". rewrite refinesm_eq /refinesm_def.
    iMod ("HK" with "Hj"). rewrite -HK.
    by rewrite wp_bind_inv.
  Qed.

  (* we intentionally do not define more of these (although maybe pure execution would be nice?), to only allow this execution for the relevant specs *)
  Lemma atomic_templateM_satisfies_wp_template_condition' P1 (TT1 TT2 : tele) P2 Q1 Q2 e A Ef Ei Q :
    (∀.. (tt1 : TT1) (tt2 : TT2), TCEq (tele_app (tele_app Q1 tt1) tt2) True%I) → 
    defs.SatisfiesTemplateCondition left_template_condition'
        [tele_arg3 e; ⊤; ⊤; Q; A] (atomic_templateM Ei P1 TT1 TT2 P2 Q1 Q2) 
        [tele_arg3 e; Ef; ⊤ ∖ Ei; Q; A] (atomic_post_templateM Ei P1 Ef TT1 TT2 P2 Q1 Q2).
  Proof.
    do 4 split => //.
    - move => Ψ.
      iIntros ">[$ H] !> !>". rewrite atomic_post_update_equiv.
      iApply atomic_post_update_mask_change.
      iApply (atomic_post_update_mono with "H").
      iIntros "!>". iSplit; eauto.
      iIntros (a). iExists id. iSplit; eauto.
      iIntros (b) "HQ". simpl. revert H => /(dep_eval_tele a) /(dep_eval_tele b) ->.
      iIntros "!>".
      by iMod ("HQ" with "[]") as "$"; eauto.  
    - split => //.
      apply atomic_post_templateM_is_mono. split.
      apply defs.absorbing_to_conditionally_absorbing, atomic_post_templateM_is_absorbing.
      tc_solve.
      apply absorbing_mono_gives_stronger. apply atomic_templateM_is_mono. 
      apply atomic_templateM_is_absorbing. tc_solve.
  Qed.
  Global Existing Instance atomic_templateM_satisfies_wp_template_condition'.

  (* register the abduction hints. *)
  Global Instance left_execution_abduct' H P e R K e_in' T e_out' MT MT' R' :
    AsExecutionOf P left_execute' e R →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep wp_red_cond T H [tele_arg ⊤ ; NotStuck] e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition left_template_condition' R MT R' MT' →
    HINT1 H ✱ [MT' $ flip left_execute' R' ∘ K ∘ e_out'] ⊫ [id]; P | 60.
  Proof. intros. eapply execution_abduct_lem => //. tc_solve. cbn => //. Qed.

  Global Instance right_execution_abduct_if_left_val H P e R K e_in' T e_out' MT MT' R' vl :
    AsExecutionOf P right_execute' e R →
    IntoVal ((λᵗ tl _ _ _ _, tl) R) vl →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep tp_reduction_condition T H ((λᵗ _ E1 _ _ _, E1) R) e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition right_template_condition' R MT R' MT' →
    HINT1 H ✱ [MT' $ flip right_execute' R' ∘ K ∘ e_out'] ⊫ [id]; P.
  Proof. intros. eapply execution_abduct_lem => //. tc_solve. Qed.

  Global Instance refinesm_values_abduct E1 E2 Q e1 (e2 : expr) v1 v2 (A : lrel Σ) :
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    HINT1 ε₀ ✱ [|={E1, E2}=> Q -∗ |={⊤}=> A v1 v2] ⊫ [id]; REL2 e1 << e2 @ E1, E2, Q : A | 10.
  Proof.
    move => <- <-. iStep as "HA".
    rewrite refinesm_eq /refinesm_def.
    iIntros (j) "Hj". iMod "HA". iSteps.
  Qed.
End refinesm_exec.












