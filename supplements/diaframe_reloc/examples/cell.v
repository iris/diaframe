From diaframe.reloc Require Import proof_automation.
From reloc.examples Require Import cell.


Section cell_refinement.
  Context `{!relocG Σ, !lockG Σ}.

  Lemma cell2_cell1_refinement' :
    ⊢ REL cell2 << cell1 : ∀ α, ∃ β, (α → β) * (β → α) * (β → α → ()).
  Proof.
    iSteps. iExists (cellR x). iSplitR; [ | iSplitR].
    - iSteps. exact nil.
    - iStep 25; do 6 iStepR; iSteps. (* TODO: make this iSmash? *)
    - iStep 40; do 6 iStepR; iSteps.
  Qed.
End cell_refinement.