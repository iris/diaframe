From reloc Require Import examples.symbol lib.lock lib.list.
From diaframe.reloc Require Import proof_automation backtracking. (* backtracking not used *)
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import auth numbers.

Section symbol_refinement.
  Context `{!relocG Σ, !msizeG Σ, !lockG Σ}.

  Definition table_inv' γ (size1 size2 tbl1 tbl2 : loc) : iProp Σ :=
    (∃ (n : nat) (ls : val), size1 ↦{#1/2} #n ∗ size2 ↦ₛ{1/2} #n
                           ∗ own γ (● (MaxNat n))
                           ∗ tbl1 ↦{#1/2} ls ∗ tbl2 ↦ₛ{1/2} ls
                           ∗ lrel_list lrel_int ls ls)%I.

  Global Instance lrel_list_both_nil A :
    HINT ε₀ ✱ [- ; emp] ⊫ [id]; lrel_list A NILV NILV ✱ [emp].
  Proof. iStep. rewrite -lrel_list_nil. iSteps. Qed.

  Global Instance lrel_list_both_cons A v1 v2 ls1 ls2 :
    HINT ε₁ ✱ [- ; ▷ A v1 v2 ∗ ▷ lrel_list A ls1 ls2] ⊫ [id]; 
         lrel_list A (CONSV v1 ls1) (CONSV v2 ls2) ✱ [emp].
  Proof. iStep. rewrite right_id. iApply lrel_list_cons; iSteps. Qed.

  Hint Extern 4 (_ ≤ _)%nat => apply Nat.le_refl : solve_pure_add.

  Lemma refinement1' :
    ⊢ REL symbol1 << symbol2 : () → lrel_symbol.
  Proof.
    iSteps as (size1 tbl1 lk1 size2 tbl2 lk2)
            "Hsize1 Htbl1 Hlk1 Hsize2 Htbl2 Hlk2".
    iAssert (|={⊤}=> ∃ γ γl, 
      inv sizeN (table_inv' γ size1 size2 tbl1 tbl2)
      ∗ is_lock (relocN.@"lock1") γl #lk1 (lok_inv size1 size2 tbl1 tbl2 lk2) )%I 
      with "[-]"
      as ">HN"; first by iSteps. iDecompose "HN" as (γ γl) "Htable Hlock".
    iExists (tableR γ). iSplitR; [ | iSplitR]; iSteps --safe.
    - iRight. (* iSmash works here, but takes 4x as long. manually run store on RHS.*) do 43 iStepR. 
      iRestore. iSteps --safe. iRight. (* after store CONS: run all on RHS *) repeat iStepR.
    - iRight. (* after load, do load on RHS. *) repeat iStepR. rewrite bool_decide_true; last lia.
          do 8 iStepR. iRestore.
      iSteps --safe. iRight. (* after 2nd load, do 2nd load on RHS. *) repeat iStepR. iRestore.
      repeat iApply refines_app; [iApply nth_int_typed |iSteps..].
  Qed.

  Lemma refinement2' :
    ⊢ REL symbol2 << symbol1 : () → lrel_symbol.
  Proof.
    iSteps as (size1 tbl1 lk1 size2 tbl2 lk2)
            "Hsize1 Htbl1 Hlk1 Hsize2 Htbl2 Hlk2".
    iAssert (|={⊤}=> ∃ γ γl, 
      inv sizeN (table_inv' γ size1 size2 tbl1 tbl2)
      ∗ is_lock (relocN.@"lock1") γl #lk1 (lok_inv size1 size2 tbl1 tbl2 lk2) )%I 
      with "[-]"
      as ">HN"; first by iSteps. iDecompose "HN" as (γ γl) "Htable Hlock".
    iExists (tableR γ). iSplitR; [ | iSplitR]; iSteps --safe.
    - iRight. (* iSmash works here, but takes 4x as long. manually run store on RHS.*) do 43 iStepR.
      iRestore. iSteps --safe. iRight. (* after store CONS: run all on RHS *) repeat iStepR.
    - rewrite bool_decide_true; last lia. iStep.
      iSteps --safe. iRight. (* this load: do load on RHS. *) do 11 iStepR. iRestore.
      iSteps --safe. iRight. (* 2nd load: do 2nd load on RHS. *) repeat iStepR. iRestore.
      repeat iApply refines_app; [iApply nth_int_typed |iSteps..].
  Qed.
End symbol_refinement.