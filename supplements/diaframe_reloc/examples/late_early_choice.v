From diaframe.reloc Require Import tp_specs proof_automation.
From reloc Require Import examples.lateearlychoice.


Section lateearlychoice_refinement.
  Context `{!relocG Σ}.

  Global Instance rand_spec_l :
    SPEC {{ True } } rand #() {{ (b : bool), RET #b; True } }.
  Proof.
    iStep 12 as (l) "Hl".
    iAssert (|={⊤}=> inv nroot (∃ (b : bool), l ↦ #b))%I with "[-]" as ">#Hi"; first iSteps.
    iIntros "!>".
    wp_apply wp_fork; iSteps.
  Qed.

  Global Instance rand_spec_r E (b : bool) :
    TPSPEC ⟨E⟩ {{ True } } rand #() {{ RET #b; True } }. (* the expression becomes an evar! *)
  Proof.
    iStep 34 as (n K HE l j) "Hl Hj".
    destruct b; last iSteps.
    rewrite tpwp_eq /tpwp_def. iIntros "!> #Hs".
    iApply (from_tpwp with "[-] Hs").
    iRevert "Hj". iApply (from_tpwp with "[-] Hs").
    iSteps.
  Qed.
  Global Opaque rand.

  (* We need to loosen our usual restrictions that prevent symbolic execution of evar expressions *)
  Global Hint Mode defs.ReshapeExprAnd + ! - - ! : typeclass_instances.
  Global Hint Mode defs.SatisfiesContextCondition + + ! : typeclass_instances.

  Lemma late'_early_choice_autom : ⊢ REL lateChoice' << earlyChoice : ref lrel_int → lrel_bool.
  Proof.
    iSteps as (l1 l2 pvs p z) "HI Hp Hcl Hl2 Hl1".
    pose right_execution_abduct.
    iSteps as (b pvs') "Hp". iPureIntro. repeat f_equal.
    match goal with | |- ?b' = b => unify b' (extract_bool ((#(), #b) :: pvs')); done end.
  Qed.

  Lemma early_late_choice_autom : ⊢ REL earlyChoice << lateChoice : ref lrel_int → lrel_bool.
  Proof. iSteps. Qed.

  Lemma late_late'_choice_autom : ⊢ REL lateChoice << lateChoice' : ref lrel_int → lrel_bool.
  Proof. iSteps. do 29 iStepR. iRestore. iSteps. Qed.
End lateearlychoice_refinement.