From diaframe.reloc Require Import proof_automation symb_exec_logatom backtracking reloc_auto_lob.
From diaframe.heap_lang Require Import atomic_specs wp_auto_lob.
From reloc.lib Require Import lock.

From diaframe.lib Require Import own_hints.
From iris.algebra Require Import auth excl numbers.


Definition inc : val := 
  rec: "f" "l" :=
    let: "n" := ! "l" in
    if: CAS "l" "n" ("n" + #1) then
      "n"
    else
      "f" "l".

Section inc_spec.
Context `{!heapGS Σ}.

Instance inc_spec (l : loc) :
  SPEC (z : Z), << l ↦ #z >> inc #l << RET #z; l ↦ #(z + 1) >>.
Proof. iSteps. Qed.

End inc_spec.

Definition fg_incrementer : val :=
  λ: <>, 
    let: "l" := ref #1 in
    (rec: "f" <> :=
      let: "n" := ! "l" in
      if: CAS "l" "n" ("n" + #1) then
        "n"
      else
        "f" #()).

Definition cg_incrementer : val :=
  λ: <>,
    let: "l" := ref #1 in
    let: "lk" := newlock #() in
    (λ: <>,
      acquire "lk";;
      let: "n" := ! "l" in 
      "l" <- "n" + #1 ;;
      release "lk";;
      "n").

Section refinement.
Context `{!relocG Σ}.

Lemma fg_cg_incrementer_refinement :
  ⊢ REL fg_incrementer << cg_incrementer : () → () → lrel_int.
Proof.
  iSteps as (l1 l2 lk) "Hl1 Hl2 Hlk".
  iAssert (|={⊤}=> inv (nroot.@"incr") 
      (∃ (n : nat), l1 ↦ #n ∗ l2 ↦ₛ #n ∗ is_locked_r lk false))%I 
      with "[-]" as ">#Hinv"; iSteps.
Qed.

End refinement.

(* above establishes logical refinment, contextual refinement follows: *)
Lemma fg_cg_incrementer_ctx_refinement :
  ∅ ⊨ fg_incrementer ≤ctx≤ cg_incrementer : () → () → TNat.
Proof.
  pose (Σ := #[relocΣ]).
  eapply (refines_sound Σ).
  iIntros (? Δ).
  iApply fg_cg_incrementer_refinement.
Qed.


From iris.program_logic Require Import atomic.
From iris.heap_lang Require Import proofmode.

Definition incr_stateR := authR $ optionUR $ exclR natO.
Class incrStateG Σ := IncrStateG {
  #[local] incrState_inG :: inG Σ incr_stateR
}.
Definition incr_stateΣ := #[ GFunctor incr_stateR].

Global Instance subG_incrStateΣ {Σ} : subG incr_stateΣ Σ → incrStateG Σ.
Proof. solve_inG. Qed.

Section logatom_spec_incrementer.
Context `{!heapGS Σ, !incrStateG Σ}.

Let Nincr := nroot.@"fg_incr".

Instance fg_incrementer_spec :
  SPEC {{ True }} fg_incrementer #() {{ (v : val) γ, RET v;
    own γ (● Excl' 1) ∗ 
    □ <<{ ∀∀ n : nat, own γ (● Excl' n) }>>
          v #() @ ↑Nincr
      <<{ own γ (● Excl' (S n)) | RET #n }>>
    }}.
Proof.
  iSteps as (l γ) "Hl1 H◯".
  iAssert (|={⊤}=> inv Nincr (∃ (n : nat), l ↦ #n ∗ own γ (◯ Excl' n)))%I with "[-]" as ">#Hinv".
  { iSteps. }
  iSteps as (_ l' Φ γ' Hlöb n _) "HI IH HAU H◯".
  iMod "HAU". iDecompose "HAU" as (n') "H● H◯ Hcl".
  iAssert (|={∅, ⊤ ∖ ↑Nincr}=> emp -∗ own γ' (◯ Excl' (S n')) ∗ Φ #n')%I with "[-]" as ">Hcl".
  { iPoseProof (diaframe_close_right with "Hcl") as "Hcl". iSteps. }
  replace (Z.to_nat (n' + 1)) with (S n') by lia.
  iSteps.
Qed.
End logatom_spec_incrementer.

