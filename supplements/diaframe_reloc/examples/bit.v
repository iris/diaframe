From diaframe.reloc Require Import proof_automation.
From reloc Require Import examples.bit.


Section bit_refinement.
  Context `{!relocG Σ}.

  Lemma bit_refinement_autom Δ : ⊢ REL bit_bool << bit_nat : interp bitτ Δ.
  Proof.
    iApply (refines_pack R).
    unfold bit_nat, bit_bool.
    iSteps; iSplitR; iSteps.
  Qed.

End bit_refinement.