From reloc Require Import examples.various lib.lock lib.counter.
From diaframe.reloc Require Import proof_automation counter backtracking.
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import cmra numbers agree excl.

Section proofs.
  Context `{!relocG Σ}.

  Instance update_shot γ `{oneshotG Σ} :
    HINT pending γ ✱ [- ; emp] ⊫ [bupd]; shot γ ✱ [shot γ].
  Proof. iStep as "Hγ". iMod (shoot with "Hγ") as "#H". iSteps. Qed.

  Lemma refinement2' `{oneshotG Σ} :
    ⊢ REL
      let: "x" := ref #0 in
      (λ: "f", "x" <- #1;; "f" #();; !"x")
    <<
      (let: "x" := ref #1 in
       λ: "f", "f" #();; !"x")
    : (() → ()) → lrel_int.
  Proof.
    iSteps as (l1 l2) "Hl1 Hl2".
    iAssert (|={⊤}=> ∃ γ, inv shootN (∃ (n : nat), 
          l1 ↦ #n ∗ (⌜n = 0⌝ ∗ pending γ  ∨ ⌜n = 1⌝ ∗ shot γ) ∗ 
          l2 ↦ₛ #1))%I
         with "[-]"
         as ">[%γ #Hinv]"; first by iSteps.
    iSmash.
  Qed.

  Lemma refinement25' `{oneshotG Σ} :
    ⊢ REL
      (λ: "f", "f" #();; #1)
    <<
      (let: "x" := ref #0 in
       (λ: "f", "x" <- #1;; "f" #();; !"x"))
    : (() → ()) → lrel_int.
  Proof.
    iSteps as (l2) "Hl2".
    iAssert (|={⊤}=> ∃ γ, inv shootN (∃ (n : nat), 
          l2 ↦ₛ #n ∗ (⌜n = 0⌝ ∗ pending γ  ∨ ⌜n = 1⌝ ∗ shot γ)))%I
         with "[-]"
         as ">[%γ #Hinv]"; first by iSteps.
    iSteps; repeat iStepR.
  Qed.

  Definition i3' (x x' b b' : loc) : iProp Σ :=
    (∃ (n:nat), x ↦{#1/2} #n ∗ x' ↦ₛ{1/2} #n ∗
     ∃ (bv:bool), b ↦ #bv ∗ b' ↦ₛ #bv ∗
    ((⌜bv = true⌝ ∗ x ↦{#1/2} #n ∗ x' ↦ₛ{1/2} #n)
    ∨ (⌜bv = false⌝)))%I.

  Lemma refinement3' :
    ⊢ REL
      let: "b" := ref #true in
      let: "x" := ref #0 in
      (λ: "f", if: CAS "b" #true #false
               then "f" #();; "x" <- !"x" + #1 ;; "b" <- #true
               else #(),
       λ: <>, !"x")
    <<
      (let: "b" := ref #true in
      let: "x" := ref #0 in
      (λ: "f", if: CAS "b" #true #false
               then let: "n" := !"x" in
                    "f" #();; "x" <- "n" + #1 ;; "b" <- #true
               else #(),
       λ: <>, !"x"))
    : ((() → ()) → ()) * (() → lrel_int).
  Proof.
    iSteps as (l1f l1c l2f l2c) "Hl1f Hl1c Hl2f Hl2c".
    iMod (inv_alloc i3n _ (i3' l1c l2c l1f l2f) with "[-]") as "#Hinv"; first iSteps.
    iSplitR; iSmash.
  Qed.

  Lemma refinement4' `{!lockG Σ}:
    ⊢ REL
      (let: "x" := ref #1 in
       let: "l" := newlock #() in
       λ: "f", acquire "l";;
               "x" <- #0;; "f" #();;
               "x" <- #1;; "f" #();;
               let: "v" := !"x" in
               release "l";; "v")
    <<
      (let: "x" := ref #0 in
       λ: "f", "f" #();; "x" <- #1;; "f" #();; !"x")
    : (() → ()) → lrel_int.
  Proof.
    iSteps as (l1 lk l2) "Hl1 Hlk Hl2".
    pose (N:=relocN.@"lock").
    iAssert (|={⊤}=> ∃ γ, is_lock N γ #lk (∃ (n m : Z), l1 ↦ #n ∗ l2 ↦ₛ #m))%I 
      with "[-]" as ">[%γ #HN]"; first iSteps.
    iSteps. repeat iStepR.
  Qed.

  Lemma refinement5' :
    ⊢ REL
      (λ: "f", let: "x" := ref #0 in
               let: "y" := ref #0 in
               "f" #();;
               "x" <- !"y";;
               "y" <- #1;;
               !"x")
    <<
      (λ: "f", let: "x" := ref #0 in
               let: "y" := ref #0 in
               "f" #();;
               "x" <- !"y";;
               "y" <- #2;;
               !"x")
    : (() → ()) → lrel_int.
  Proof. repeat iStepR. Qed.

  Definition i6' `{oneshotG Σ} `{inG Σ (exclR unitR)} (c1 c2 : loc) γ γ' :=
    (∃ (n n' : nat), 
      c1 ↦ #n' ∗ c2 ↦ₛ #n ∗
     ((⌜n' = n⌝ ∗ pending γ)
   ∨ (⌜n' = (n + 1)%nat⌝ ∗ shot γ ∗ own γ' (Excl ()))))%I.

  Lemma refinement6' `{oneshotG Σ} `{inG Σ (exclR unitR)} :
    ⊢ REL
      (λ: "f" "g" "f'",
       let: "pg" := p "g" in
       let: "g'" := Fst "pg" in
       let: "g''" := Snd "pg" in
       "f" "g'";; "g'" #();; "f'" "g'";; "g''" #())
    <<
      (λ: "f" "g" "f'",
       let: "pg" := p "g" in
       let: "g'" := Fst "pg" in
       let: "g''" := Snd "pg" in
       "f" "g'";; "g" #();; "f'" "g'";; "g''" #() + #1)
    : ((() → ()) → ()) → (() → ()) → ((() → ()) → ()) → lrel_int.
  Proof.
    pose (HR := right_execution_abduct).
    iSteps as (f1 f2 f3 f4 f5 f6 l2 l1) "Hf1f2 Hf3f4 Hf5f6 Hl2 Hl1". clear HR.
    iAssert (|={⊤}=> ∃ γ γ', own γ' (Excl ()) ∗ inv shootN (i6' l1 l2 γ γ'))%I 
        with "[-]" as ">[%γ [%γ' [He #Hinv]]]"; first iSteps.
    iSplitR "He"; [iSmash | iSteps].
    repeat iStepR; iSplitR; iSmash.
  Qed.
End proofs.