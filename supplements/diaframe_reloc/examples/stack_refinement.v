(* ReLoC -- Relational logic for fine-grained concurrency *)
(** Refinement proof for the Treiber stack *)
From diaframe.reloc Require Import proof_automation reloc_auto_lob backtracking.
From reloc.examples.stack Require Import CG_stack FG_stack.

Definition stackN := nroot.@"stack".

Section proof.
  Context `{relocG Σ}.

  Fixpoint stack_contents (v1 : loc) (ls : list val) :=
    match ls with
    | [] => v1 ↦□ NONEV
    | h::tl => ∃ (z1 : loc), v1 ↦□ SOMEV (h, #z1) ∗ stack_contents z1 tl
    end%I.

  Definition stack_link (A : lrel Σ) (v1 : loc) (v2 : val) :=
    (∃ (ls1 : list val) (ls2 : list val),
        stack_contents v1 ls1 ∗ is_stack v2 ls2 ∗
        [∗ list] v1;v2 ∈ ls1;ls2, A v1 v2)%I.

  Global Instance stack_contents_persistent v1 ls :
    Persistent (stack_contents v1 ls).
  Proof. revert v1. induction ls; apply _. Qed.

  Lemma stack_contents_agree istk ls ls' :
    stack_contents istk ls -∗ stack_contents istk ls' -∗ ⌜ls = ls'⌝.
  Proof.
    revert istk ls'. induction ls as [|h ls]; intros istk ls'; simpl.
    - iIntros "#Histk". destruct ls' as [|h' ls']; iSteps.
    - destruct ls' as [|h' ls']; first iSteps.
      iStep 2 as (l) "Hls l↦ Hls'".
      iDestruct (IHls with "Hls Hls'") as %->; iSteps.
  Qed.

  Definition sinv (A : lrel Σ) stk stk' : iProp Σ :=
    (∃ (istk : loc), stk  ↦ #istk ∗ stack_link A istk stk')%I.

  Global Instance mapsto_to_stack_content hd v (tl : loc) q :
    HINT hd ↦{q} CONSV v #tl ✱ [xs_tl; stack_contents tl xs_tl]
            ⊫ [bupd] xs;
         stack_contents hd xs ✱ [⌜xs = v :: xs_tl⌝ ∗ hd ↦□ CONSV v #tl].
  Proof.
    iSteps as (vs) "Htl Hd".
    iExists (v :: vs). iSteps.
  Qed.

  Global Instance mapsto_to_stack_content_empty hd q :
    HINT hd ↦{q} NILV ✱ [- ; emp] ⊫ [bupd]; stack_contents hd [] ✱ [hd ↦□ NILV].
  Proof. iSteps. Qed.

  Global Instance val_of_list_suc l x xs :
    HINT l ↦ₛ CONSV x (val_of_list xs) ✱ [- ; emp] ⊫ [id]; l ↦ₛ val_of_list (x :: xs) ✱ [emp].
  Proof. tc_solve. Qed.

  Global Instance val_of_list_empty l :
    HINT l ↦ₛ NILV ✱ [- ; emp] ⊫ [id]; l ↦ₛ val_of_list [] ✱ [emp].
  Proof. tc_solve. Qed.

  Global Instance stack_contents_merge_cons hd (nx : loc) v vs' :
    MergableConsume (stack_contents hd vs') false (λ p Pin Pout,
      TCAnd (TCEq Pin (hd ↦□ CONSV v #nx)%I) $
             TCEq Pout (∃ vs, ⌜vs' = v :: vs⌝ ∗ stack_contents nx vs)%I).
  Proof.
    rewrite /MergableConsume /= => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    destruct vs' as [|v' vs]; iSteps.
  Qed.

  Global Instance stack_contents_merge_eq hd vs vs' :
    MergableConsume (stack_contents hd vs') true (λ p Pin Pout,
      TCAnd (TCEq Pin (stack_contents hd vs)%I) $
             TCEq Pout (⌜vs = vs'⌝ ∗ stack_contents hd vs)%I).
  Proof.
    rewrite /MergableConsume /= => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    iStep as "Hvs' Hvs".
    iDestruct (stack_contents_agree with "Hvs' Hvs") as %<-.
    iSteps.
  Qed.

  Global Instance big_sepL_mergable_nil_r `(Φ : nat → A → B → iProp Σ) l :
    MergableConsume (big_sepL2 Φ l []) true (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₀)%I)
            (TCEq Pout ⌜l = []⌝%I)) | 20.
  Proof.
    rewrite /MergableConsume /= empty_hyp_first_eq => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim right_id; iStep as "Hl".
    iDestruct (big_sepL2_length with "Hl") as %Hfoo.
    by assert (l = []) as -> by (apply length_zero_iff_nil; done).
  Qed.

  Global Instance big_sepL_mergable_nil_l `(Φ : nat → A → B → iProp Σ) l :
    MergableConsume (big_sepL2 Φ [] l) true (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₀)%I)
            (TCEq Pout ⌜l = []⌝%I))| 20.
  Proof.
    rewrite /MergableConsume /= empty_hyp_first_eq => p Pin Pout [-> ->].
    rewrite big_sepL2_flip. iSteps.
  Qed.

  Global Instance big_sepL_mergable_cons_r `(Φ : nat → A → B → iProp Σ) ls r rs :
    MergableConsume (big_sepL2 Φ ls (r :: rs)) true (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₀)%I)
            (TCEq Pout (∃ l ls', ⌜ls = l :: ls'⌝ ∗ Φ 0 l r ∗ [∗ list] k↦x1;x2 ∈ ls';rs, Φ (S k) x1 x2)%I)) | 30.
  Proof.
    rewrite /MergableConsume /= empty_hyp_first_eq => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim right_id; iStep as "Hls".
    iDestruct (big_sepL2_length with "Hls") as %Hfoo; iRevert "Hls".
    destruct ls; iSteps.
  Qed.

  Global Instance big_sepL_mergable_cons_l `(Φ : nat → A → B → iProp Σ) l ls rs :
    MergableConsume (big_sepL2 Φ (l :: ls) rs) true (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₀)%I)
            (TCEq Pout (∃ r rs', ⌜rs = r :: rs'⌝ ∗ Φ 0 l r ∗ [∗ list] k↦x1;x2 ∈ ls;rs', Φ (S k) x1 x2)%I)) | 30.
  Proof.
    rewrite /MergableConsume /= empty_hyp_first_eq => p Pin Pout [-> ->].
    rewrite big_sepL2_flip. iSteps. rewrite big_sepL2_flip. iSteps.
  Qed.

  Definition stackInt A : lrel Σ := LRel (λ v1 v2,
    ∃ (stk : loc), ⌜v1 = #stk⌝
      ∗ inv (stackN .@ (stk,v2)) (sinv A stk v2))%I.

  Lemma stack_refinement :
    ⊢ REL FG_stack << CG_stack : ∀ A, ∃ B, (() → B) * (B → () + A) * (B → A → ()).
  Proof.
    iSteps as (A). iExists (stackInt A). iSplitR; [ iSteps | iSplitR].
    - iSteps --safe / as (_ _ l1 Hlöb ls1 vs1 vs2 l2 lk) "IH HI Hvs1 HA Hcl Hlk Hl2 Hl1".
      destruct vs1 as [|h1 vs1']; iDecompose "HA"; iDecompose "Hvs1"; [ iSmash | iSteps ].
    - iSteps.
  Qed.

End proof.

Open Scope nat.
Theorem stack_ctx_refinement :
  ∅ ⊨ FG_stack ≤ctx≤ CG_stack :
      ∀: ∃: (() → #0) * (#0 → () + #1) * (#0 → #1 → ()).
Proof.
  eapply (refines_sound relocΣ).
  iIntros (? ?).
  iApply stack_refinement.
Qed.
