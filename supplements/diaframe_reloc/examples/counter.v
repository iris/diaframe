From reloc.lib Require Import lock counter.

From diaframe.reloc Require Import proof_automation symb_exec_logatom tp_specs.
From diaframe.heap_lang Require Import atomic_specs wp_auto_lob.


Section fg_spec.
  Context `{!heapGS Σ}.

  Global Instance fg_increment_spec (l : loc) :
    SPEC (z : Z), << l ↦ #z >> FG_increment #l << RET #z; l ↦ #(z + 1) >>.
  Proof. iSteps. Qed.
End fg_spec.


From diaframe.reloc Require Import reloc_auto_lob.


Section counter_refinement.
  Context `{!relocG Σ}.

  Global Instance fg_increment_tp_spec (l : loc) E1 E2 :
    TPSPEC ⟨E1, E2⟩ (z : Z), {{ l ↦ₛ #z } } FG_increment #l {{ RET #z; l ↦ₛ #(z + 1) } }.
  Proof. rewrite /FG_increment. iSteps. Qed.

  Lemma FG_CG_counter_ref :
    ⊢ REL FG_counter << CG_counter : () → (() → lrel_int) * (() → lrel_int).
  Proof.
    iSteps.
    iAssert (|={⊤}=> inv counterN (counter_inv x0 x x1))%I with"[-]" as ">#HN"; first iSteps.
    iSplitR; iSteps.
  Qed.
End counter_refinement.