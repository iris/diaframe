From diaframe.reloc Require Import proof_automation.

Section tests.
  Context `{!relocG Σ}.

  Definition EqI : lrel Σ := LRel (λ v1 v2, ⌜v1 = v2⌝)%I.

  Lemma test3 l r :
    ▷ l ↦ #3 -∗
    r ↦ₛ #4 -∗
    REL (!#l;;!#l+#1) << (!#r;;#0+!#r) : EqI.
  Proof. iSteps. Qed.

  Lemma refines_pure_r E K' e e' t A n
      (Hspec : nclose specN ⊆ E) ϕ :
      PureExec ϕ n e e' →
      ϕ →
      (REL t << inl $ fill K' e' @ E : A) ⊢ REL t << fill K' e @ E : A.
  Proof.
    rewrite refines_eq /refines_def => Hpure Hϕ /=.
    iIntros "Hlog". iIntros (j) "[#Hs Hj]".
    iApply (tp_specs.from_tpwp with "[Hlog]") => //.
    rewrite -fill_app. 
    iSteps.
    rewrite fill_app tp_specs.tpwp_eq /=.
    iStep.
    iStep.
    iApply ("Hlog"). iSteps.
  Qed.

End tests.