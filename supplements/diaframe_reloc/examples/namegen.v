From reloc Require Import examples.namegen.
From reloc.prelude Require Import bijections.
From diaframe.reloc Require Import proof_automation examples.counter.

Section proofs.
  Context `{!relocG Σ, !pBijPreG loc nat Σ}.

  Lemma nameGen_ref1_autom :
    ⊢ REL nameGen1 << nameGen2 : ∃ α, (() → α) * (α → α → lrel_bool).
  Proof.
    unfold nameGen1, nameGen2. iSteps as (l2) "Hl2".
    iMod alloc_empty_bij as (γ) "HB".
    iExists (ngR γ).
    iAssert (|={⊤}=> inv (relocN.@"ng") (ng_Inv γ l2))%I with "[-]" as ">#Hinv".
    { iSteps. rewrite big_sepS_empty. iSteps. }
    iSplitR.
    - iStep 6. iIntros "!>". rel_alloc_l_atomic. 
      (* right execution does not support opening invariants currently *)
      iInv "Hinv" as (n L) "(HB & Hc & HL)" "Hcl".
      iStep 26 as (l1) "HB HL Hl1 Hl2".
      iAssert (⌜(∃ y, (l1, y) ∈ L) → False⌝)%I with "[HL Hl1]" as %Hl'.
      { iIntros ([y Hy]). rewrite (big_sepS_elem_of _ L (l1 ,y) Hy). iDecompose "HL". }
      iAssert (⌜(∃ x, (x, Z.to_nat (n + 1)) ∈ L) → False⌝)%I with "[HL]" as %Hc.
      { iIntros ([x' Hy]).
        rewrite (big_sepS_elem_of _ L (x',_) Hy). iDecompose "HL". }
      iMod (bij_alloc_alt _ _ γ _ l1 (Z.to_nat (n + 1)) with "HB") as "[HB #Hl'n]"; auto.
      iStep. rewrite big_sepS_insert. iStep. rewrite big_sepS_mono; last case; iSteps.
      contradict Hc. eauto.
    - iStep 35 as (l1 n1 l3 n3) "Hbl1 Hbl3".
      iInv "Hinv" as (n L) "(>HB & Hc & HL)" "Hcl".
      iDestruct (bij_agree with "HB Hbl1 Hbl3") as %Hag.
      iSteps. iPureIntro. repeat f_equal.
      destruct (decide (l1 = l3)) as [->|Hneq].
      + rewrite !bool_decide_true //. repeat f_equal. by apply Hag.
      + rewrite !bool_decide_false // => [= Heq]; subst; apply Hneq; [eauto | apply Hag; lia].
  Qed.
End proofs.