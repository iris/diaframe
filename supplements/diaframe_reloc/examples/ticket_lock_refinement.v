(* ReLoC -- Relational logic for fine-grained concurrency *)
(** Ticket lock refines a simple spin lock *)

From diaframe.reloc Require Import proof_automation counter reloc_auto_lob backtracking.
From diaframe.lib Require Import own_hints ticket_cmra.
From iris.algebra Require Export auth gset.

From reloc.lib Require Import lock counter.
From iris.heap_lang.lib Require Import ticket_lock.


(* A different `acquire` function to showcase the atomic rule for FG_increment *)
Definition acquire : val := λ: "lk",
  let: "n" := FG_increment (Snd "lk") in
  ticket_lock.wait_loop "n" "lk".
(* A different `release` function to showcase the rule for wkincr *)
Definition release : val := λ: "lk", wkincr (Fst "lk").

Definition newlock : val := ticket_lock.newlock.

Definition lrel_lock `{relocG Σ} : lrel Σ :=
  ∃ A, (() → A) * (A → ()) * (A → ()).

Class tlockG Σ :=
  #[local] tlock_G :: inG Σ ticketR.
Definition tlockΣ : gFunctors := 
  #[ GFunctor ticketR ].
Global Instance subG_tlockΣ {Σ} : subG tlockΣ Σ → tlockG Σ.
Proof. solve_inG. Qed.

Section refinement.
  Context `{!relocG Σ, !tlockG Σ}. (*, !lockPoolG Σ}.*)

  (** * Basic abstractions around the concrete RA *)

  (** ticket with the id `n` *)
  Definition ticket (γ : gname) (n : nat) := own γ (CoPset $ ticket n).
  (** total number of issued tickets is `n` *)
  Definition issuedTickets (γ : gname) (n : nat) := own γ (CoPset $ tickets_geq $ n).

  (** * Invariants and abstracts for them *)
  Definition lockInv (lo ln : loc) (γ : gname) (l' : val) : iProp Σ :=
    (∃ (o n : nat) (b : bool), lo ↦ #o ∗ ln ↦ #n
   ∗ issuedTickets γ n ∗ is_locked_r l' b
   ∗ (⌜b = true⌝ ∗ ticket γ o ∨ ⌜b = false⌝))%I.

  Definition N := relocN.@"locked".

  Definition lockInt : lrel Σ := LRel (λ v1 v2,
    ∃ (lo ln : loc) (γ : gname),
     ⌜v1 = (#lo, #ln)%V⌝ ∗ inv N (lockInv lo ln γ v2))%I.

  (* Allocating a new lock *)
  Lemma newlock_refinement :
    ⊢ REL newlock << reloc.lib.lock.newlock : () → lockInt.
  Proof. iSteps. Qed.

  (* Acquiring a lock *)
  Lemma acquire_refinement :
    ⊢ REL acquire << reloc.lib.lock.acquire : lockInt → ().
  Proof.   (* restore after fg_increment, case_decide for wait_equal *)
    iSteps --safe; iStep; iSteps --safe; case_decide; simplify_eq/=; iSmash.
    Unshelve. all: apply inhabitant. (* probably a better if: bool_decide hint would smooth this out *)
  Qed.

  Lemma release_refinement :
    ⊢ REL release << reloc.lib.lock.release : lockInt → ().
  Proof. iSteps. all: apply inhabitant. Qed.

  Lemma ticket_lock_refinement :
    ⊢ REL (newlock, acquire, release)
        <<
        (reloc.lib.lock.newlock, reloc.lib.lock.acquire, reloc.lib.lock.release)
    : lrel_lock.
  Proof.
    iApply (refines_pack lockInt). repeat iApply refines_pair.
    - by iApply newlock_refinement.
    - by iApply acquire_refinement.
    - by iApply release_refinement.
  Qed.

End refinement.

Open Scope nat.
Definition lockT : type :=
  ∃: (() → #0) * (#0 → ()) * (#0 → ()).

Lemma ticket_lock_ctx_refinement :
  ∅ ⊨   (newlock, acquire, release)
  ≤ctx≤ (reloc.lib.lock.newlock, reloc.lib.lock.acquire, reloc.lib.lock.release)
  : lockT.
Proof.
  pose (Σ := #[relocΣ;tlockΣ]).
  eapply (refines_sound Σ).
  iIntros (? Δ). iApply ticket_lock_refinement.
Qed.
