From diaframe.reloc Require Import proof_automation reloc_auto_lob backtracking.
From reloc Require Import examples.coinflip examples.lateearlychoice.

From diaframe.reloc.examples Require Import late_early_choice.


Section coinflip_refinement.
  Context `{!relocG Σ, !lock.lockG Σ}.

(** Lazy coin (with prophecies) refines eager coin *)
  Definition I' (cl ce : loc) (p : proph_id) : iProp Σ :=
    (∃ vs : list (val*val), proph p vs ∗ ∃ v (b : bool), cl ↦ v ∗ ce ↦ₛ #b ∗
      (⌜v = NONEV⌝ ∗ ⌜b = extract_bool vs⌝
       ∨ ⌜v = SOMEV #b⌝)).

  Transparent lock.is_locked_r. (* TODO: bug, needed because newcoin and newlock have the same implementation *)

  Lemma coin_lazy'_eager_refinement_autom :
    ⊢ REL coin2' << coin1 : (() → lrel_bool) * (() → ()).
  Proof.
    unfold coin2', coin1. iSteps as (c1 pvs p lk c2) "Hc1 Hp Hlk Hc2".
    iAssert (|={⊤}=> ∃ γ, lock.is_lock coinN γ #lk (I' c1 c2 p))%I with "[-]" as ">HI";
    [ iSteps | iDecompose "HI" as (γ) "Hl"].
    iSplitR; iStep 20; unfold read, flip; repeat iStepR.
    (* execute left until lock is acquired, then execute right *)
  Qed.

  Opaque lock.is_locked_r.

  Lemma coin_eager_lazy'_refinement_autom :
    ⊢ REL coin1 << coin2' : (() → lrel_bool) * (() → ()).
  Proof.              (* this stuff   vvvv   is needed because of the bug described above *)
    unfold coin2', coin1. iStartProof2. rel_rec_l. iSteps as (c1 c2 p lk) "Hc1 Hc2 Hlk".
    iAssert (|={⊤}=> inv coinN (
        ∃ (b : bool), lock.is_locked_r lk false ∗ c1 ↦ #b ∗ ∃ v, c2 ↦ₛ v ∗ (⌜v = NONEV⌝ ∨ ⌜v = SOMEV #b⌝)))%I with "[-]" as ">#HI";
    [ iSteps | ].
    iSplitR; iSteps.
  Qed.

  Lemma coin_lazy'_lazy_refinement_autom :
    ⊢ REL coin2' << coin2 : (() → lrel_bool) * (() → ()).
  Proof.
    unfold coin2', coin2. iSteps as (c1 pvs p lk c2) "Hc1 Hp Hlk Hc2".
    iAssert (|={⊤}=> ∃ γ, lock.is_lock coinN γ #lk (
        ∃ vs, proph p vs ∗ ∃ v, c1 ↦ v ∗ c2 ↦ₛ v ∗ (⌜v = NONEV⌝ ∨ ∃ (b : bool), ⌜v = SOMEV #b⌝)))%I with "[-]" as ">HI";
    [ iSteps | iDecompose "HI" as (γ) "HI"].
    iSplitR; iStep 20; unfold read_lazy, flip_lazy;
    pose (right_execution_abduct);
    [iSteps as (b pvs') | iSteps..].
    iSplitR. 
    { iPureIntro. repeat f_equal.
      match goal with | |- ?b' = b => unify b' (extract_bool ((#(), #b) :: pvs')); done end. }
    iSteps.
  Qed.

  Lemma coin_lazy_lazy'_refinement_autom :
    ⊢ REL coin2 << coin2' : (() → lrel_bool) * (() → ()).
  Proof.
    unfold coin2', coin2. iSteps as (c1 c2 p lk) "Hc1 Hc2 Hlk".
    iAssert (|={⊤}=> inv coinN (
        lock.is_locked_r lk false ∗ ∃ v, c1 ↦ v ∗ c2 ↦ₛ v ∗ (⌜v = NONEV⌝ ∨ ∃ (b : bool), ⌜v = SOMEV #b⌝)))%I with "[-]" as ">#HI";
    [ iSteps | ].
    iSplitR; iSteps. (* this iSteps eagerly chooses restoring invariants *)
  Qed.
End coinflip_refinement.