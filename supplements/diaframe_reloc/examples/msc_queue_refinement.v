From reloc Require Import reloc lib.lock lib.list.

(* The course-grained queue is implemented as a linked list guarded by a
   lock. Partially taken from Vindum et al., CPP '21, https://dl.acm.org/doi/10.1145/3437992.3439930 *)

Definition CG_dequeue_go : val := λ: "head",
  match: !"head" with
    NONE => NONE
  | SOME "p" => "head" <- (Snd "p");; SOME (Fst "p")
  end.

Definition CG_dequeue : val := λ: "lock" "head" <>,
  acquire "lock" ;;
  let: "v" := CG_dequeue_go "head" in
  release "lock" ;;
  "v".

Definition CG_enqueue_go : val := rec: "enqueue" "x" "head" :=
  match: "head" with
    NONE => CONS "x" NIL
  | SOME "p" => CONS (Fst "p") ("enqueue" "x" (Snd "p"))
  end.

Definition CG_enqueue : val := λ: "lock" "head" "x",
  acquire "lock" ;;
  "head" <- CG_enqueue_go "x" (! "head") ;;
  release "lock".

Definition CG_queue : val := Λ:
  let: "head" := ref NIL in
  let: "lock" := newlock #() in
  ((λ: "x", CG_dequeue "lock" "head" "x"), (λ: "x", CG_enqueue "lock" "head" "x")).

From diaframe.reloc Require Import proof_automation tp_specs symb_exec_logatom.
From diaframe.heap_lang.examples.logatom Require Import msc_queue_faithful.
From diaframe.heap_lang Require Import wp_later_credits.


Definition MS_queue_original : val :=
  λ: <>, let: "queue" := new_queue #() in
    ((λ: <>, dequeue "queue"), λ: "v", enqueue "queue" "v").


Section refinement.
  Context `{!relocG Σ, !queueAltG Σ}.
  Set Default Proof Using "Type*".

  Fixpoint is_pair_list (xs : list val) : val :=
    match xs with
    | nil => NILV
    | x :: xs' => (CONSV x (is_pair_list xs'))
    end.

  Global Instance cg_dequeue_go_spec (l : loc) E1 E2 :
    TPSPEC ⟨E1, E2⟩ (vs : list val), {{ l ↦ₛ (is_pair_list vs) } } 
      CG_dequeue_go #l 
    {{ (v : val), RET v; 
        ⌜v = NONEV⌝ ∗ ⌜vs = []⌝ ∗ l ↦ₛ (is_pair_list vs) 
      ∨ ∃ v' vs', ⌜v = SOMEV v'⌝ ∗ ⌜vs = v'::vs'⌝ ∗ l ↦ₛ (is_pair_list vs') } }.
  Proof. iStep 5 as (j K HE vs) "Hl". destruct vs; iSmash. Qed.

  Global Instance cg_enqueue_go_spec E1 E2 (v : val) (vs : list val) :
    TPSPEC ⟨E1, E2⟩ {{ True } } 
      CG_enqueue_go v (is_pair_list vs) 
    {{ RET (is_pair_list (vs ++ [v])); True } }.
  Proof. (* this proof is kind of awkward: we cant do Löb induction, have no access to a twp_lam-like tactic, nor a bind lemma *)
    iStep 4 as (j K HE).
    iInduction vs as [|v' vs'] "IH" forall (K).
    - unfold CG_enqueue_go. iSteps.
    - unfold CG_enqueue_go.
      iStep 28. (* stop just before recursive call. manually add in binding context *)
      iSpecialize ("IH" $! ([PairRCtx (Fst (v', is_pair_list vs')%V); InjRCtx] ++ K)).
      simpl. (* now bind and apply the IH *) rewrite tpwp_eq /tpwp_def. iStep 2 as "Hs Hj".
      iMod ("IH" with "Hs Hj") as "HN". iDecompose "HN" as "Hj". (* now return to TPWP, where automation can finish *)
      iRevert "Hs Hj". rewrite bi.intuitionistically_elim.
      iApply from_tpwp. iSteps.
  Qed.

  Instance is_queue_timeless γ1 γ2 vs : Timeless (is_queue γ1 γ2 vs).
  Proof. tc_solve. Qed.

  Instance is_queue_inv_pers l γ1 γ2 γ3 γ4 : Persistent (is_queue_inv l γ1 γ2 γ3 γ4).
  Proof. tc_solve. Qed.

  Opaque new_queue dequeue enqueue is_queue is_queue_inv is_pair_list.

  Definition queue_sync_inv γm1 γm2 γc lc A : iProp Σ :=
     ∃ (vs1 vs2 : list val), is_queue γm1 γm2 vs1 ∗ is_locked_r γc false ∗ lc ↦ₛ (is_pair_list vs2) ∗ 
             [∗ list] xᵢ ; xₛ ∈ vs1 ; vs2, A xᵢ xₛ.

  Let N := nroot.@"queue_refinement".

  Lemma queue_refinement :
    ⊢ REL MS_queue_original << CG_queue : ∀ A, (() → () + A) * (A → ()).
  Proof.
    iSteps as (A ql1 γ1 γm1 γm2 γ4 ql2 lk) "Hql1I Hq1 H£ Hql2 Hlk".
    iAssert (|={⊤}=> inv N (queue_sync_inv γm1 γm2 lk ql2 A))%I with "[-]" as ">#HN"; first iSteps.
    { iExists []. iSteps. }
    iSplitR; last first.
    { iSteps. rewrite big_sepL2_snoc. iSteps. }
    iSteps as (h1 tl1 h2 tl2) "HAh HAtl _ H£".
    by iMod (lc_fupd_elim_later with "H£ HAh") as "#$".
  Qed.
End refinement.










