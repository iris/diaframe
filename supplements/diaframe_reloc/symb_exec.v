From iris.bi Require Import bi telescopes lib.laterable.
From reloc.logic.proofmode Require Import spec_tactics tactics.
From reloc.logic Require Import model rules derived.
From diaframe.symb_exec Require Import defs weakestpre.
From diaframe Require Import util_classes tele_utils util_instances.

From diaframe.reloc Require Export tp_specs.

Import bi.

Section reloc_executor.

  Context `{!relocG Σ}.

  Instance right_execute : ExecuteOp (iPropI Σ) expr [tele_pair expr ; coPset ; lrel Σ] :=
    λ e, (λᵗ tl E A, REL tl << e @ E : A).

  Global Arguments right_execute e !R /.

  Global Instance as_right_execute tl (tr : expr) E A : AsExecutionOf (REL tl << tr @ E : A) right_execute tr [tele_arg3 tl; E; A].
  Proof. done. Qed.

  Global Instance tp_reduction_condition_well_behaved_equiv A : 
    Proper ((=) ==> (=) ==>
      (pointwise_relation _ (=)) ==>
      ((pointwise_relation _ (⊣⊢)) ==> (⊣⊢)) ==> (⊣⊢)) (tp_reduction_condition A).
  Proof.
    move => E1 E -> {E1} e1 e -> {e1} e' e'' He' M1 M2 HM /=.
    apply (anti_symm _).
    all: apply forall_mono=> j.
    all: apply forall_mono=> K.
    all: apply wand_mono => //.
    all: apply forall_mono=> P.
    all: rewrite HM => // {HM}.
    all: apply wand_mono => //.
    all: apply fupd_mono.
    all: apply exist_mono => a.
    all: by rewrite He'.
  Qed.

  Global Instance tp_reduction_condition_well_behaved_ent A : 
    Proper ((=) ==> (=) ==>
      (pointwise_relation _ (=)) ==>
      ((pointwise_relation _ (flip (⊢))) ==> (flip (⊢))) ==> (⊢)) (tp_reduction_condition A).
  Proof.
    move => E1 E -> {E1} e1 e -> {e1} e' e'' He' M1 M2 HM /=.
    apply forall_mono=> j.
    apply forall_mono=> K.
    apply wand_mono => //.
    apply forall_mono=> P.
    rewrite HM => // {HM}.
    apply wand_mono => //.
    apply fupd_mono.
    apply exist_mono => a.
    by rewrite He'.
  Qed.

  Global Instance tp_reduction_condition_well_behaved_tne A : 
    Proper ((=) ==> (=) ==>
      (pointwise_relation _ (=)) ==>
      ((pointwise_relation _ (⊢)) ==> (⊢)) ==> (flip (⊢))) (tp_reduction_condition A).
  Proof.
    move => E1 E -> {E1} e1 e -> {e1} e' e'' He' M1 M2 HM /=.
    apply forall_mono=> j.
    apply forall_mono=> K.
    apply wand_mono => //.
    apply forall_mono=> P.
    rewrite HM => // {HM}.
    apply wand_mono => //.
    apply fupd_mono.
    apply exist_mono => a.
    by rewrite He'.
  Qed.

  Inductive context_as_item : (expr → expr) → Prop :=
    is_item K Kits : (∀ e, fill Kits e = K e) → context_as_item K.

  Instance context_as_item_condition : ContextCondition expr := λ K, context_as_item K.

  Global Arguments context_as_item_condition K /.

  Global Instance items_valid_context K : SatisfiesContextCondition context_as_item_condition (fill K).
  Proof. by econstructor. Qed.

  Global Instance items_compose_valid_context K1 K2 : SatisfiesContextCondition context_as_item_condition (fill K1 ∘ fill K2).
  Proof. apply (is_item _ (K2 ++ K1)) => e. rewrite /fill /=. rewrite foldl_app //.  Qed.

  Instance right_template_condition : TemplateCondition (iPropI Σ) [tele_pair expr ; coPset ; lrel Σ] 
    := (λ A R M R' M', R = R' ∧ M = M').

  Global Arguments right_template_condition A R M /.

  Global Instance all_satisfies_right_template_condition R `(M : (A → iPropI Σ) → iPropI Σ) : SatisfiesTemplateCondition right_template_condition R M R M.
  Proof. by rewrite /SatisfiesTemplateCondition /=. Qed.

  Global Instance right_execute_reduction_compat : 
    ExecuteReductionCompatibility right_execute (λᵗ _ E _, E) tp_reduction_condition context_as_item_condition right_template_condition.
  Proof.
    move => K e A e' M /= HK R R' M' [<- <-]. destruct HK as [K K' HK]. rewrite -HK.
    drop_telescope R as tl E Φ => /=.
    iIntros "[Hred HM]" => /=.
    rewrite refines_eq /refines_def /=.
    iIntros (j) "[#H3 H4]".
    rewrite -fill_app.
    iMod ("Hred" with "[$H3 $H4] HM") as (a) "[HP Hj]"; simpl. rewrite -HK.
    rewrite refines_eq /refines_def.
    iApply "HP" => //. iFrame "#".
    by rewrite fill_app.
  Qed.

  Instance tp_execute : ExecuteOp (iPropI Σ) expr [tele_pair nat; coPset; coPset; iPropI Σ] :=
    λ e, (λᵗ j E1 E2 Φ, tpwp j e E1 E2 Φ)%I.

  Global Arguments tp_execute e !R /.

  Global Instance tpwp_as_tp_execute e j E1 E2 Φ : AsExecutionOf (tpwp j e E1 E2 Φ)%I tp_execute e [tele_arg3 j; E1; E2; Φ].
  Proof. done. Qed.

  Lemma as_tp_execute e j E1 E2 Φ : AsExecutionOf (spec_ctx -∗ j ⤇ e ={E1,E2}=∗ Φ)%I tp_execute e [tele_arg3 j; E1; E2; Φ].
  Proof. rewrite /tp_execute tpwp_eq /=. done. Qed.

  Instance tp_template_condition : TemplateCondition (iPropI Σ) [tele_pair nat; coPset; coPset; iPropI Σ]
    := (λ A R M R' M', R = R' ∧ M = M').

  Global Arguments tp_template_condition A R M /.

  Global Instance all_satisfies_tp_template_condition R `(M : (A → iPropI Σ) → iPropI Σ) : SatisfiesTemplateCondition tp_template_condition R M R M.
  Proof. by rewrite /SatisfiesTemplateCondition /=. Qed.

  Global Instance tp_execute_reduction_compat : 
    ExecuteReductionCompatibility tp_execute (λᵗ _ E1 _ _, E1) tp_reduction_condition context_as_item_condition tp_template_condition.
  Proof.
    move => K e A e' M /= HK R R' M' [<- <-]. destruct HK as [K K' HK]. rewrite -HK.
    drop_telescope R as i E1 E2 Φ => /=. rewrite tpwp_eq /=.
    iIntros "[Hred HM] #Hs Hi" => /=.
    iMod ("Hred" with "[$Hs $Hi] HM") as (a) "[HP Hi]"; simpl. rewrite -HK.
    rewrite tpwp_eq /=.
    by iApply "HP".
  Qed.

  Instance left_execute : ExecuteOp (iPropI Σ) expr [tele_pair rhs_t; coPset; lrel Σ] :=
    λ e, λᵗ tr E A, REL e << tr @ E : A.

  Global Arguments left_execute e !R /.

  Global Instance as_left_execute tl tr E A : AsExecutionOf (REL tl << tr @ E : A) left_execute tl [tele_arg3 tr; E; A].
  Proof. done. Qed.

  Instance left_template_condition : TemplateCondition (iPropI Σ) [tele_pair rhs_t; coPset; lrel Σ]
    := (λ Φ, λᵗ tr E A, λ M, λᵗ tr' E' A', λ M', 
        (A' = A ∧ tr' = tr ∧ (∀ Ψ, M' (fupd E' ⊤ ∘ Ψ) ⊢ |={E, ⊤}=> M Ψ) ∧ (template_mono M' ∧ template_conditionally_absorbing M' Laterable))
    ).

  Global Arguments left_template_condition A !R M !R' M' /.

  Lemma templateM_satisfies_left_template_condition tr A n M1 M2' M2 TT1 TT2 Ps Qs :
    ModalityStrongMono M1 → 
    ModalityCompat3 M2 M2' (fupd ⊤ ⊤) →
    (* when M1 = |={⊤,E}=>, then M2 = |={E,⊤}=>. this condition then states that |={E,⊤}=> = M2 ∘ |={⊤, ⊤}=> *) 
    ModalityStrongMono M2' →
    SatisfiesTemplateCondition left_template_condition [tele_arg3 tr; ⊤; A] (template_M n M1 M2 TT1 TT2 Ps Qs) 
                                                       [tele_arg3 tr; ⊤; A] (template_M n M1 M2' TT1 TT2 Ps Qs).
    (* a lemma because usually in a refinement proof, we don't need to, and thus don't want to, close the invariant immediately *)
  Proof.
    rewrite /SatisfiesTemplateCondition /= => HM1 HM2. do 3 (split => //); last split.
    - move => Ψ. rewrite -fupd_intro. apply HM1, bi_texist_mono => tt1.
      rewrite /ModalityCompat3 in HM2.
      iIntros "[$ H] !>"; iStopProof. apply bi_tforall_mono => tt2.
      apply wand_mono => //.
    - apply template_M_is_mono; tc_solve.
    - apply absorbing_to_conditionally_absorbing, template_M_is_absorbing; tc_solve.
  Qed.

  Global Instance templateM_satisfies_left_template_condition_keep_inv_open tr A n TT1 TT2 Ps Qs E :
    SatisfiesTemplateCondition left_template_condition [tele_arg3 tr; ⊤; A] (template_M n (fupd ⊤ E) (fupd E ⊤) TT1 TT2 Ps Qs) 
                                                       [tele_arg3 tr; E; A] (template_M n (fupd ⊤ E) id TT1 TT2 Ps Qs) | 50.
  Proof.
    rewrite /SatisfiesTemplateCondition /=. do 3 (split => //); last split.
    - iIntros (Ψ) "H !>".
      iMod "H" as (tt) "[HPS HΨ]".
      iExists tt.
      iFrame "HPS".
      by iIntros "!> !>".
    - apply template_M_is_mono; tc_solve.
    - apply absorbing_to_conditionally_absorbing, template_M_is_absorbing; tc_solve.
  Qed.

  Lemma templateM_satisfies_left_template_condition_pure_exec_under_inv tr A n M1 TT Ps E mQs :
    IntroducableModality M1 → 
    (∀.. tt1 tt2, TCEq (tele_app (tele_app mQs tt1) tt2) emp%I) →
    SatisfiesTemplateCondition left_template_condition [tele_arg3 tr; E; A] (template_M n M1 id TT [tele] Ps mQs) 
                                                       [tele_arg3 tr; E; A] (template_M 0 (fupd E E) id TT [tele] Ps mQs).
  Proof.
    rewrite /SatisfiesTemplateCondition /= => HM1 /tforall_forall HQs. do 3 (split => //); last split.
    - iIntros (Ψ) ">H".
      iDestruct "H" as (tt) "[HPs HΨ]".
      rewrite -HM1 /= {HM1}.
      iExists tt.
      iFrame "HPs".
      rewrite HQs => /=.
      rewrite left_id /=.
      by iMod "HΨ" as "$".
    - apply template_M_is_mono; tc_solve.
    - apply absorbing_to_conditionally_absorbing, template_M_is_absorbing; tc_solve.
  Qed.

  (* magically, Lemma causes no problem, Global Instance causes a bugged tactic *)
  Global Instance templateM_satisfies_left_template_condition_pure_exec_under_inv_inst tr A n M1 TT Ps E mQs :
    IntroducableModality M1 → 
    (∀.. tt1 tt2, TCEq (tele_app (tele_app mQs tt1) tt2) emp%I) →
    SatisfiesTemplateCondition left_template_condition [tele_arg3 tr; E; A] (template_M n M1 id TT [tele] Ps mQs) 
                                                       [tele_arg3 tr; E; A] (template_M 0 (fupd E E) id TT [tele] Ps mQs).
  Proof. eapply templateM_satisfies_left_template_condition_pure_exec_under_inv. Qed.

  Global Instance later_forall_satisfies_template_condition n (TT : tele) tr A  :
    SatisfiesTemplateCondition left_template_condition [tele_arg3 tr; ⊤; A] (template_I n (fupd ⊤ ⊤) (TT := TT))%I 
                                                       [tele_arg3 tr; ⊤; A] (template_I n (fupd ⊤ ⊤))%I | 20.
  Proof.
    rewrite /defs.SatisfiesTemplateCondition.
    repeat (split; first done).
    split. 
    - iIntros (Ψ). rewrite -fupd_intro.
      apply bi.laterN_mono, bi.wand_mono, bi_tforall_mono; first done.
      move => tt. by rewrite fupd_trans.
    - split; first apply: template_I_mono.
      apply absorbing_to_conditionally_absorbing, strong_mono_gives_absorbing, template_I_strong_mono.
      iIntros (P Q) "HPQ >HP". by iApply "HPQ".
  Qed.

  Global Instance later_forall_satisfies_template_condition_in_inv n tr A E M :
    IntroducableModality M →
    SatisfiesTemplateCondition left_template_condition [tele_arg3 tr; E; A] (template_I n M (TT := [tele]))%I 
                                                       [tele_arg3 tr; E; A] (template_I 0 (fupd E E))%I  | 30.
  Proof.
    rewrite /SatisfiesTemplateCondition => HM.
    repeat (split; first done).
    split. 
    - iIntros (Ψ). rewrite /= left_id -HM. by iIntros ">>$".
    - split; first apply: template_I_mono.
      apply absorbing_to_conditionally_absorbing, strong_mono_gives_absorbing, template_I_strong_mono.
      iIntros (P Q) "HPQ >HP". by iApply "HPQ".
  Qed.

  Global Instance left_execute_reduction_compat : 
    ExecuteReductionCompatibility left_execute (λ _, [tele_arg3 ⊤ ; NotStuck]) wp_red_cond context_as_item_condition left_template_condition.
  Proof.
    move => K e A e' M /= HK R R' M'. destruct HK as [K K' HK]. rewrite -HK.
    drop_telescope R as tr E Φ => /=.
    drop_telescope R' as tr' E' Φ' => /=.
    move => [-> [-> [HMΨ [HM1 HM2]]]].
    rewrite refines_eq /refines_def /=.
    iIntros "[Hred HM1]" (j) "Hj".
    rewrite /flip /compose /=.
    rewrite -wp_bind /=.
    iApply "Hred". iStopProof.
    erewrite HM2; last (destruct tr; tc_solve).
    rewrite -HMΨ.
    apply HM1 => a /=.
    iIntros "[HK Hj]".
    rewrite refines_eq /refines_def /=.
    iMod ("HK" with "Hj"). rewrite -HK.
    by iApply wp_bind_inv.
  Qed.
End reloc_executor.























