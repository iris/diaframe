From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import do_lob.

From reloc Require Import reloc.
From diaframe.reloc Require Import proof_automation.

Section hints.
  Context `{!relocG Σ}.

  Set Universe Polymorphism.
  Unset Universe Minimization ToSet. (* hilarious *)

  Lemma rec_abduct e Φ TT args shape e' Φ' er :
    AsRecFunOn e TT args shape e' →
    AsFunOfOnly Φ args Φ' →
    HINT1 ε₁ ✱ 
        [do_löb TT args (tele_map bi_pure shape) (tele_bind (λ tt, REL (tele_app e' tt) << er : (tele_app Φ' tt) ))]
        ⊫ [id]; REL e << er : Φ.
  Proof.
    case => -> Hshape <-.
    rewrite /Abduct.
    iIntros "[_ H]".
    rewrite do_löb_eq /do_löb_def tele_app_bind.
    iSimpl. iApply "H". rewrite tele_map_app. done.
  Qed.

  Global Instance rec_abduct_v2 e Φ TT args shape e' Φ' er (TT2 : tele) (tt2 : TT2) er' :
    AsRecFunOn e TT args shape e' →
    TCIf (TCEq TT TeleO) False TCTrue →
    AsFunOfOnly Φ args Φ' →
    AsFunOfAmongOthers er args tt2 er' →
    HINT1 ε₁ ✱ 
        [do_löb (TelePairType TT TT2) (tele_pair_arg args tt2) 
          (tele_curry $ tele_dep_bind (λ tt1, tele_bind (λ tt2, ⌜tele_app shape tt1⌝)))
          (tele_curry $ tele_dep_bind (λ tt1, as_dependent_fun _ _ (tele_bind (λ tt2,
                REL (tele_app e' tt1) << tele_app (tele_app er' tt2) tt1 : (tele_app Φ' tt1))) tt1))]
        ⊫ [id]; REL e << er : Φ.
  Proof.
    case => -> Hshape _ <-.
    rewrite /AsFunOfAmongOthers /Abduct => Her.
    iIntros "[_ H]".
    rewrite do_löb_eq /do_löb_def.
    rewrite /tele_pair_arg.
    rewrite !tele_curry_uncurry_relation.
    rewrite -!tele_uncurry_curry_compose.
    rewrite !tele_dep_appd_bind.
    rewrite tele_app_bind.
    rewrite -!dependent_fun_eq.
    rewrite tele_app_bind.
    rewrite Her.
    iSimpl. iApply "H". done.
  Qed.

  Set Universe Minimization ToSet.
  Unset Universe Polymorphism.

  Global Instance post_lob_rel el er A vr args f x eb fa :
    AsRecApply el vr args →
    ((∀ f' x' v', AsRecV (RecV f' x' v') f' x' v') → AsRecV vr f x eb) →
    TCEq (last args) (Some fa) →
    HINT1 ε₁ ✱
      [▷ (⌜has_löbbed vr⌝ -∗ REL 
          (fold_right (λ (ar : val) r, App r ar) (subst' x fa (subst' f vr eb)) (reverse (tl (reverse args))))
          << er : A)]
      ⊫ [id]; post_löb (REL el << er : A).
  Proof.
    rewrite /AsRecApply => Hvr1 Hvr2 /TCEq_eq /last_Some [l' Hl]. subst.
    iStep as "Hrel". iSpecialize ("Hrel" with "[%//]").
    rewrite post_löb_eq /post_löb_def. simpl.
    iApply (refines_pure_l 1 []); last first.
    { iIntros "!>". simpl. iExact "Hrel". }
    - exact I.
    - rewrite reverse_snoc. simpl. rewrite reverse_involutive.
      induction l'. tc_solve.
      eapply (pure_exec_ctx (fill_item (AppLCtx a))). apply IHl'.
  Qed.
End hints.




