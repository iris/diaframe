From diaframe.heap_lang Require Export proof_automation wp_auto_lob.
From diaframe.lib Require Import own_hints.
From diaframe.symb_exec Require Import defs weakestpre.
From iris.heap_lang Require Import proofmode.

From diaframe.reloc Require Import symb_exec.


From stdpp Require Import sets.
From reloc Require Import reloc lib.lock lib.counter examples.stack.CG_stack.
From reloc Require Export logic.model reloc. (* not importing spec_tactics tactics: no need *)


Section main.
  Context `{!relocG Σ}.

  Global Instance refines_values_abduct E e1 (e2 : expr) v1 v2 (A : lrel Σ) :
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    HINT1 ε₀ ✱ [|={E, ⊤}=> A v1 v2] ⊫ [id]; REL e1 << e2 @ E : A | 10.
  Proof.
    move => <- <-. iStep.
    iApply refines_ret => //.
  Qed.

  Global Instance refines_prepend_modal E el er A :
    PrependModality (REL el << er @ E : A) (fupd E E) (REL el << er @ E : A).
  Proof.
    rewrite /PrependModality.
    apply (anti_symm _).
    - iApply fupd_refines.
    - apply fupd_intro.
  Qed.

  Lemma restore_mask E el er A : (|={E,⊤}=> REL el << er : A) ⊢ REL el << er @ E : A.
  Proof. iApply fupd_refines. Qed.

  Global Instance restore_abduct e1 e2 E A :
    HINT1 REL e1 << e2 : A ✱ [fupd E ⊤ emp] ⊫ [id]; REL e1 << e2 @ E : A | 10.
  Proof.
    iStep as "HRel He".
    iApply restore_mask. by iMod "He".
  Qed.

  (* left execution and tp execution should always be performed *)

  Global Instance left_execution_abduct H P e R K e_in' T e_out' MT MT' R' :
    AsExecutionOf P left_execute e R →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep wp_red_cond T H [tele_arg ⊤ ; NotStuck] e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition left_template_condition R MT R' MT' →
    HINT1 H ✱ [MT' $ flip left_execute R' ∘ K ∘ e_out'] ⊫ [id]; P.
  Proof. intros. eapply execution_abduct_lem => //. tc_solve. cbn => //. Qed.

  Global Instance tp_execution_abduct H P e R K e_in' T e_out' MT MT' R' :
    AsExecutionOf P tp_execute e R →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep tp_reduction_condition T H ((λᵗ _ E1 _ _ , E1) R) e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition tp_template_condition R MT R' MT' →
    HINT1 H ✱ [MT' $ flip tp_execute R' ∘ K ∘ e_out'] ⊫ [id]; P.
  Proof. intros. eapply execution_abduct_lem => //. tc_solve. Qed.

  (* right execution should not always be performed *)

  Lemma right_execution_abduct H P e R K e_in' T e_out' MT MT' R' :
    AsExecutionOf P right_execute e R →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep tp_reduction_condition T H ((λᵗ _ E1 _, E1) R) e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition right_template_condition R MT R' MT' →
    HINT1 H ✱ [MT' $ flip right_execute R' ∘ K ∘ e_out'] ⊫ [id]; P.
  Proof. intros. eapply execution_abduct_lem => //. tc_solve. Qed.

  (* but it should always be performed when the left hand side is a value *)

  Global Instance right_execution_abduct_if_left_val H P e R K e_in' T e_out' MT MT' R' vl :
    AsExecutionOf P right_execute e R →
    IntoVal ((λᵗ tl _ _, tl) R) vl →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep tp_reduction_condition T H ((λᵗ _ E1 _, E1) R) e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition right_template_condition R MT R' MT' →
    HINT1 H ✱ [MT' $ flip right_execute R' ∘ K ∘ e_out'] ⊫ [id]; P.
  Proof. intros; exact: right_execution_abduct. Qed.

  Global Instance tpwp_abduct_val j e E1 E2 Φ v:
    IntoVal e v →
    HINT1 ε₀ ✱ [tpwp_def j e E1 E2 Φ] ⊫ [id]; tpwp j e E1 E2 Φ.
  Proof. move => <-. iStep as "HΦ". rewrite tpwp_eq. iStep. by iApply "HΦ". Qed.

  Global Instance restore_if_right_val e1 e2 v A E :
    TCIf (TCEq E ⊤) False TCTrue →
    IntoVal e2 v →
    HINT1 ε₁ ✱ [|={E,⊤}=> REL e1 << v : A] ⊫ [id]; REL e1 << (inl e2) @ E : A.
  Proof. move => _ <-. iStep. by iApply restore_mask. Qed.

  Global Instance tpwp_abduct_K_val j e E1 E2 Φ v K:
    IntoVal e v →
    HINT1 ε₀ ✱ [tpwp_def j (fill K e) E1 E2 Φ] ⊫ [id]; tpwp j (fill K e) E1 E2 Φ.
  Proof. move => <-. iStep as "HΦ". rewrite tpwp_eq. iStep. by iApply "HΦ". Qed.

  Global Opaque spec_ctx.

  Global Instance refinement_bind el_h er_h el_g er_g Kl Kr Ah Ag E :
    ReshapeExprAnd expr el_g Kl el_h (SatisfiesContextCondition context_as_item_condition Kl) →
    ReshapeExprAnd expr er_g Kr er_h (SatisfiesContextCondition context_as_item_condition Kr) →
    HINT1 (REL el_h << er_h : Ah) ✱ [|={E, ⊤}=> ∀ v1 v2, Ah v1 v2 -∗ REL Kl v1 << Kr v2 : Ag] ⊫ [id]; REL el_g << er_g @ E : Ag | 20.
  Proof.
    rewrite /SatisfiesContextCondition.
    move => [-> H]. inversion_clear H as [? Kl' HKl].
    move => [-> H]. inversion_clear H as [? Kr' HKr].
    iStep as "He HK". rewrite -HKl -HKr. iMod "HK".
    iApply (refines_bind with "He").
    iSteps. rewrite HKl HKr. iSteps.
  Qed.

  Global Instance resolve_proph_l_spec pre el er K e (p : proph_id) (v : val) A n M1 M2 TT1 TT2 L e' U v' E :
    ReshapeExprAnd expr el K (Resolve e #p v) (TCAnd (LanguageCtx K) $ 
                                                 TCAnd (Atomic StronglyAtomic e) $
                                                 TCAnd (Atomic WeaklyAtomic e) $ (SolveSepSideCondition (to_val e = None))) →
    ReductionStep' wp_red_cond pre n M1 M2 TT1 TT2 L U e e' [tele_arg3 E; NotStuck] → (* does not work for pure since that is a ReductionTemplateStep *)
    IntroducableModality M1 → IntroducableModality M2 →
    (TC∀.. ttl, TC∀.. ttr, IntoVal (tele_app (tele_app e' ttl) ttr) (tele_app (tele_app v' ttl) ttr)) →
    HINT1 pre ✱ [|={⊤, E}=> ∃ (pvs : list $ prod val val), proph p pvs ∗ ∃.. ttl, tele_app L ttl ∗ 
      ▷^n (∀ pvs', ∀.. ttr, ⌜pvs = (pair (tele_app (tele_app v' ttl) ttr) v)::pvs'⌝ ∗ proph p pvs' ∗ tele_app (tele_app U ttl) ttr ={E}=∗
              REL (K $ tele_app (tele_app e' ttl) ttr) << er @ E : A) ]
          ⊫ [id]; REL el << er : A | 45.
  Proof.
    case => -> [HK [He1 [He2 He3]]]. (*TODO: improve proof? use native proph stuff *)
    rewrite /ReductionStep' /ReductionTemplateStep /Abduct /TCTForall /IntoVal /= => -> HM1 HM2 He'.
    iStep as "He Hrel". rewrite refines_eq /refines_def.
    iStep 2. iIntros "!>". iApply wp_bind. iMod "Hrel". iDecompose "Hrel" as (pvs) "Hproph HK".
    iApply (wp_resolve with "Hproph"); first apply He3.
    iApply "He". iApply HM1 => /=. iDestruct "HK" as (ttl) "[Hl HΦ]".
    iExists _. iFrame "Hl". iIntros "!>" (tt2) "HU". iApply HM2 => /=.
    revert He' => /(dep_eval_tele ttl) /(dep_eval_tele tt2) He'. rewrite -He'.
    iStep 4 as (pvs) "HK Hproph".
    iSpecialize ("HK" $! pvs tt2 with "[Hproph HU]"). iSteps.
    iMod ("HK" with "[$]") as ">H". by rewrite -He'.
  Qed.

  Global Instance resolve_proph_l_spec_skip el er K (p : proph_id) (v : val) A E :
    ReshapeExprAnd expr el K (Resolve Skip #p v) (LanguageCtx K) →
    HINT1 ε₀ ✱ [|={⊤, E}=> ∃ (pvs : list $ prod val val), proph p pvs ∗ 
      ▷ (∀ pvs', ⌜pvs = (pair (#()) v)::pvs'⌝ ∗ proph p pvs' ={E}=∗
              REL (K $ #()) << er @ E : A) ]
          ⊫ [id]; REL el << er : A | 45.
  Proof.
    case => -> HK.
    rewrite /ReductionStep' /ReductionTemplateStep /Abduct /=.
    iStep as "Hps". rewrite refines_eq /refines_def.
    iStep 2 as (j) "He".
    iIntros "!>". iApply wp_bind. iMod "Hps". iDecompose "Hps" as (ps) "Hps HΦ".
    iApply (wp_resolve with "Hps"); first done. iStep 6 as (ps) "HΦ Hps".
    iSpecialize ("HΦ" $! ps with "[Hps]"); [iSteps|].
    iMod ("HΦ" with "He") as ">H". iSteps.
  Qed.

  Global Instance mergable_consume_mapsto_own q1 q2 q l v1 v2 :
    MergableConsume (l ↦ₛ{q1} v1)%I true (λ p Pin Pout, 
        TCAnd (TCEq Pin (l ↦ₛ{q2} v2)) $ 
        TCAnd (proofmode_classes.IsOp (A := frac.fracR) q q1 q2) $
              (TCEq Pout (l ↦ₛ{q} v1 ∗ ⌜v1 = v2⌝)))%I | 30. (* this does not include q ≤ 1! *)
  Proof.
    rewrite /MergableConsume => p Pin Pout [-> [+ ->]].
    rewrite bi.intuitionistically_if_elim => Hq.
    iStep as "Hl1 Hl2".
    iDestruct (pointstoS_agree with "Hl1 Hl2") as "#->".
    rewrite Hq. by iFrame.
  Qed.

  Global Instance mapsto_val_may_need_more (l : loc) (v1 v2 : val) (q1 q2 : Qp) mq q :
    FracSub q2 q1 mq →
    TCEq mq (Some q) →
    HINT l ↦ₛ{q1} v1 ✱ [v'; ⌜v1 = v2⌝ ∗ l ↦ₛ{q} v'] ⊫ [id]; l ↦ₛ{q2} v2 ✱ [⌜v1 = v2⌝ ∗ ⌜v' = v1⌝] | 55.
  Proof.
    rewrite /FracSub => <- -> v' /=.
    iSteps.
  Qed.

  Global Instance mapsto_val_have_enough (l : loc) (v1 v2 : val) (q1 q2 : Qp) mq :
    FracSub q1 q2 mq →
    HINT l ↦ₛ{q1} v1 ✱ [- ; ⌜v1 = v2⌝] ⊫ [id]; l ↦ₛ{q2}v2 ✱ [⌜v1 = v2⌝ ∗ match mq with | Some q => l ↦ₛ{q} v1 | _ => True end] | 54.
  Proof.
    rewrite /= /FracSub => <-.
    destruct mq; iSteps as "Hl".
    iDestruct "Hl" as "[H1 H1']".
    iSteps.
  Qed.


  Section lock_n_stack_specs.
    (* spin lock specs as tp executions, so that automation can apply them *)    
    Global Instance pure_tp_spec_rec E1 φ n e e' :
      ((∀ f x e, SolveSepSideCondition (is_recursive_fun (RecV f x e) = false) → 
                  AsRecV (RecV f x e) f x e) → 
        PureExec φ n e e') →
      SolveSepSideCondition φ →
      TPSPEC [ε₁] ⟨E1⟩ {{ True } } e {{ RET e'; emp } }.
    Proof.
      rewrite /SolveSepSideCondition => Hφe Hφ. 
      rewrite empty_hyp_last_eq -{1}empty_hyp_first_eq.
      eapply pure_tp_spec.
      enough (PureExec φ n e e').
      { move => _. by apply H. }
      apply Hφe. intros. reflexivity.
    Qed.

    Global Instance tpspec_goal pre e E1 E2 TT1 TT2 P e' Q :
      AsEmpValidWeak
        (ReductionTPStep tp_reduction_condition pre e E1 E2 TT1 TT2 P e' Q E1)
        ((∀ j K, ⌜nclose specN ⊆ E2⌝ -∗ ∀.. a : TT1, 
        pre ∗ tele_app P a -∗ TPWP (fill K e) @ j; E2 , E2 {{ ∃ (e'' : expr), j ⤇  fill K e'' ∗
          ∃.. (b : TT2), ⌜e'' = tele_app (tele_app e' a) b⌝ ∗ tele_app (tele_app Q a) b }})) | 10.
    Proof.
      move => HjWP.
      apply: tp_condition_execution_step_general => //= j K HE.
      iIntros "[#Hs Hj]" (a) "HP".
      iRevert "Hs Hj". rewrite bi.intuitionistically_elim.
      iApply from_tpwp.
      iPoseProof (HjWP $! j K with "[%//] HP") as "H".
      rewrite tpwp_eq /tpwp_def. iSteps as "Hs HQ". 
      iMod ("H" with "Hs HQ") as (e'') "[Hj [%b [-> HQ]]]". (* telescope business, no touching iSteps*)
      iExists b.
      iSteps.
    Qed.

    Global Instance newlock_step_tp E1 :
      TPSPEC ⟨E1⟩ {{ True } } reloc.lib.lock.newlock #() {{ (v : val), RET v; is_locked_r v false } }.
    Proof. iSteps. Qed.

    Global Instance acquirelock_step_tp (v : val) E1 E2 :
      TPSPEC ⟨E1, E2⟩ {{ is_locked_r v false } } acquire v {{ RET #(); is_locked_r v true } }.
    Proof. rewrite /acquire. iSteps. Qed.

    Global Instance releaselock_step_tp (v : val) E1 E2 b:
      TPSPEC ⟨E1, E2⟩ {{ is_locked_r v b } } release v {{ RET #(); is_locked_r v false } }.
    Proof. iSteps. Qed.

    Global Instance is_locked_r_timeless v b : Timeless (is_locked_r v b). Proof. tc_solve. Qed.

    Global Opaque is_locked_r.

    (* CG stack steps *)
    Global Instance CG_push_step_tp (rs : val) (v : val) E1 E2 :
      TPSPEC ⟨E1, E2⟩ tl, {{ is_stack rs tl } } CG_locked_push rs v {{ RET #(); is_stack rs (v :: tl) } }.
    Proof. iSteps. Qed.

    Global Instance CG_pop_step_tp (rs : val) E1 E2 :
      TPSPEC ⟨E1, E2⟩ vs, {{ is_stack rs vs } } 
            CG_locked_pop rs 
        {{ (v' : val), RET v'; 
          ⌜v' = NONEV⌝ ∗ ⌜vs = []⌝ ∗ is_stack rs [] ∨ 
          ∃ v tl, ⌜vs = v :: tl⌝ ∗ ⌜v' = SOMEV v⌝ ∗ is_stack rs tl } }.
    Proof.
      iSteps as (l v tid K He vs) "Hlk Hvs".
      destruct vs as [|v' vs]; iSteps.
    Qed.

    Obligation Tactic := program_verify.

    Global Program Instance newlock_spec `{!lockG Σ} :
      SPEC {{ True } } reloc.lib.lock.newlock #() {{ (l : loc), RET #l; ∀ R N, R ={⊤}=∗ ∃ γ, is_lock N γ #l R } }.

    Global Program Instance acquire_spec `{!lockG Σ} (lk : val) R N :
      SPEC γ, {{ is_lock N γ lk R } } reloc.lib.lock.acquire lk {{ RET #(); R ∗ lock.locked γ } }.

    Global Program Instance release_spec `{!lockG Σ} (lk : val) R N :
      SPEC γ, {{ is_lock N γ lk R ∗ R ∗ lock.locked γ } } reloc.lib.lock.release lk {{ RET #(); True } }.

    Global Opaque is_lock reloc.lib.lock.newlock reloc.lib.lock.acquire reloc.lib.lock.release.
  End lock_n_stack_specs.

End main.

Ltac iRestore := iApply restore_mask; iStep.

Ltac iStepR :=
  let H := fresh "H" in
  pose (H := right_execution_abduct); iStep; clear H.











