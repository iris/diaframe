From iris.bi Require Import bi telescopes lib.laterable.
From reloc.logic.proofmode Require Import spec_tactics tactics.
From reloc.logic Require Import model rules derived.
From diaframe.symb_exec Require Import defs.
From diaframe Require Import proofmode_base.


Section tpwp.
  Context `{!relocG Σ}.

  Definition tpwp_def j e E1 E2 Φ : iProp Σ := spec_ctx -∗ j ⤇ e ={E1, E2}=∗ Φ.
  Global Arguments tpwp_def j e E1 E2 Φ /.
  Definition tpwp_aux : seal (@tpwp_def). Proof. by eexists. Qed.
  Definition tpwp := tpwp_aux.(unseal).
  Global Opaque tpwp.
  Global Arguments tpwp : simpl never.
  Lemma tpwp_eq : tpwp = tpwp_def.
  Proof. by rewrite -tpwp_aux.(seal_eq). Qed.

  Lemma from_tpwp j e E1 E2 Φ : (|={E1}=> tpwp j e E1 E2 Φ) ⊢ spec_ctx -∗ j ⤇ e ={E1, E2}=∗ Φ.
  Proof. rewrite tpwp_eq /=. iIntros "H #HS Hj". iMod "H". by iApply "H". Qed.

  Lemma refines_spec_ctx tl (tr : expr) E A :
    (spec_ctx -∗ (REL tl << tr @ E : A)) ⊢ (REL tl << tr @ E : A).
  Proof.
    iIntros "Hrel".
    rewrite {2}refines_eq /refines_def {1}/refines_right /=.
    iIntros (j') "[#Hs Htr]".
    iCombine "Hs Htr" as "Htr".
    iRevert "Htr".
    fold (refines_right j' tr).
    change (refines_right j' tr) with
      match inl tr : rhs_t with
      | inl e => refines_right j' e
      | inr k => ⌜j' = k⌝%I
      end.
    set (tr' := inl tr).
    iRevert (j').
    fold (refines_def E tl tr' A). rewrite /tr'.
    rewrite -refines_eq.
    by iApply "Hrel".
  Qed.

  Lemma from_tpwp_pre_rel j e tl (tr : expr) E A :
    (|={E}=> tpwp j e E E (REL tl << tr @ E : A)) ⊢ j ⤇ e -∗ REL tl << tr @ E : A%I.
  Proof. 
    rewrite tpwp_eq /=.
    iIntros "HjR Hj".
    iApply refines_spec_ctx.
    iIntros "#Hs".
    iApply fupd_refines.
    iMod "HjR".
    by iApply "HjR".
  Qed.

End tpwp.

Notation "'TPWP' e @ j ; E1 , E2 {{ Φ }}" := (tpwp j e E1 E2 Φ)
  (at level 20, e, Φ at level 200) : bi_scope.



Section tp_templates.
  Context `{!BiFUpd PROP}.

  Definition tp_template (E1 E2 : coPset) (TT1 TT2 : tele) P Q (Φ : (qprod TT1 TT2 → PROP)) : PROP
    := (|={E1, E2}=> ∃.. (tt1 : TT1), tele_app P tt1 ∗ |={E2}=> ⌜nclose specN ⊆ E2⌝ ∗ (∀.. (tt2 : TT2), 
          tele_app (tele_app Q tt1) tt2 -∗ |={E2, E1}=> (Φ $ QPair tt1 tt2)))%I.

  (* We want to specify !TT1 !TT2 but: https://github.com/coq/coq/issues/7674 means cbn will not unfold it.
    worse, simpl will, but exposes fupd internals, which is BAD! *)
  Global Arguments tp_template E1 E2 TT1 TT2 P Q Φ /.
  Global Arguments fupd {_ _} _ : simpl never.

  Class ReductionTPStep `(condition : ReductionCondition PROP E W ) (pre : PROP) e E1 E2
      TT1 TT2 (Ps : TT1 -t> PROP) (e' : TT1 -t> TT2 -t> E) (Ps' : TT1 -t> TT2 -t> PROP) w :=
    #[global] as_tp_template_step :: ReductionTemplateStep condition (qprod TT1 TT2)%type pre w e 
      (λ pr, tele_app (tele_app e' $ qfst pr) $ qsnd pr)
                        (tp_template E1 E2 TT1 TT2 Ps Ps').
End tp_templates.

Section tp_red_cond.
  Context `{!relocG Σ}.

  Instance tp_reduction_condition : ReductionCondition (iPropI Σ) expr coPset :=
    (λ A E e e' M, ∀ j K, spec_ctx ∗ j ⤇ fill K e -∗ ∀ P, M P ={E}=∗ ∃ a, P a ∗ j ⤇ fill K (e' a))%I.
  Global Arguments tp_reduction_condition A E e e' M /.

  Proposition tp_condition_execution_step_general {A B : tele} e1 E1 E2 P e2 Q pre:
    (∀ j K', nclose specN ⊆ E2 → (spec_ctx ∗ j ⤇ fill K' e1 ⊢ 
      ∀.. (a : A), pre ∗ tele_app P a -∗ (|={E2}=> ∃.. (b : B), spec_ctx ∗ j ⤇ fill K' (tele_app (tele_app e2 a) b) ∗ (tele_app (tele_app Q a) b)))) →
    ReductionTPStep tp_reduction_condition pre e1 E1 E2 A B P e2 Q E1.
  Proof.
    rewrite /ReductionTPStep /ReductionTemplateStep => Hjk.
    iIntros "Hpre" (j K) "[#Hs Hj]".
    iIntros (M) "HM".
    iCombine "Hs Hj" as "Hjs".
    iMod "HM" as (a) "[HPa >[%HE Hr]]".
    rewrite Hjk //.
    iMod ("Hjs" with "[$Hpre $HPa]") as (b) "(_ & Hj & HQ)".
    iMod ("Hr" with "HQ").
    iExists (QPair a b). eauto with iFrame.
  Qed.

End tp_red_cond.

Notation "'TPSPEC' [ pre ] ⟨ E1 , E2 ⟩ x1 .. xn , {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E2
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, e') .. )) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, Qs%I) .. )) .. )
    E1 )
  (at level 20, E1, E2 at level 9, x1 closed binder, xn closed binder, y1 closed binder, yn closed binder, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 , E2 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' [ pre ] ⟨ E1 , E2 ⟩ x1 .. xn , {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E2
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    TeleO
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, e') .. )
    (λ x1, .. (λ xn, Qs%I) .. )
    E1 )
  (at level 20, E1, E2 at level 9, x1 closed binder, xn closed binder, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 , E2 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 , E2 ⟩ x1 .. xn , {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E2
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, e') .. )) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, Qs%I) .. )) .. )
    E1 )
  (at level 20, E1, E2 at level 9, x1 closed binder, xn closed binder, y1 closed binder, yn closed binder, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 , E2 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 , E2 ⟩ x1 .. xn , {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E2
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    TeleO
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, e') .. )
    (λ x1, .. (λ xn, Qs%I) .. )
    E1 )
  (at level 20, E1, E2 at level 9, x1 closed binder, xn closed binder, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 , E2 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' [ pre ] ⟨ E1 , E2 ⟩ {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E2
    TeleO
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    Ps%I
    (λ y1, .. (λ yn, e') .. )
    (λ y1, .. (λ yn, Qs%I) .. )
    E1 )
  (at level 20, E1, E2 at level 9, y1 closed binder, yn closed binder, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 , E2 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' [ pre ] ⟨ E1 , E2 ⟩ {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E2
    TeleO
    TeleO
    Ps%I
    e'
    Qs%I
    E1 )
  (at level 20, E1, E2 at level 9, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 , E2 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 , E2 ⟩ {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E2
    TeleO
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    Ps%I
    (λ y1, .. (λ yn, e') .. )
    (λ y1, .. (λ yn, Qs%I) .. )
    E1 )
  (at level 20, E1, E2 at level 9, y1 closed binder, yn closed binder, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 , E2 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 , E2 ⟩ {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E2
    TeleO
    TeleO
    Ps%I
    e'
    Qs%I
    E1 )
  (at level 20, E1, E2 at level 9, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 , E2 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' [ pre ] ⟨ E1 ⟩ x1 .. xn , {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E1
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, e') .. )) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, Qs%I) .. )) .. )
    E1 )
  (at level 20, E1 at level 9, x1 closed binder, xn closed binder, y1 closed binder, yn closed binder, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' [ pre ] ⟨ E1 ⟩ x1 .. xn , {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E1
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    TeleO
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, e') .. )
    (λ x1, .. (λ xn, Qs%I) .. )
    E1 )
  (at level 20, E1 at level 9, x1 closed binder, xn closed binder, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 ⟩ x1 .. xn , {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E1
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, e') .. )) .. )
    (λ x1, .. (λ xn, (λ y1, .. (λ yn, Qs%I) .. )) .. )
    E1 )
  (at level 20, E1 at level 9, x1 closed binder, xn closed binder, y1 closed binder, yn closed binder, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 ⟩ x1 .. xn , {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E1
    (TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
    TeleO
    (λ x1, .. (λ xn, Ps%I) .. )
    (λ x1, .. (λ xn, e') .. )
    (λ x1, .. (λ xn, Qs%I) .. )
    E1 )
  (at level 20, E1 at level 9, x1 closed binder, xn closed binder, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 ⟩  x1 .. xn ,  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' [ pre ] ⟨ E1 ⟩ {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E1
    TeleO
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    Ps%I
    (λ y1, .. (λ yn, e') .. )
    (λ y1, .. (λ yn, Qs%I) .. )
    E1 )
  (at level 20, E1 at level 9, y1 closed binder, yn closed binder, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' [ pre ] ⟨ E1 ⟩ {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    pre%I
    e
    E1
    E1
    TeleO
    TeleO
    Ps%I
    e'
    Qs%I
    E1 )
  (at level 20, E1 at level 9, e, Ps, pre, e', Qs at level 200, format
  "'[hv' TPSPEC  [ pre ]  ⟨ E1 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 ⟩ {{ Ps } } e {{ y1 .. yn , 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E1
    TeleO
    (TeleS (λ y1, .. (TeleS (λ yn, TeleO)) .. ))
    Ps%I
    (λ y1, .. (λ yn, e') .. )
    (λ y1, .. (λ yn, Qs%I) .. )
    E1 )
  (at level 20, E1 at level 9, y1 closed binder, yn closed binder, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  y1 .. yn ,  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Notation "'TPSPEC' ⟨ E1 ⟩ {{ Ps } } e {{ 'RET' e' ; Qs } }" :=
  (ReductionTPStep
    tp_reduction_condition
    empty_hyp_first%I
    e
    E1
    E1
    TeleO
    TeleO
    Ps%I
    e'
    Qs%I
    E1 )
  (at level 20, E1 at level 9, e, Ps, e', Qs at level 200, format
  "'[hv' TPSPEC  ⟨ E1 ⟩  '/ ' {{  Ps  } } '/  '  e  '/ ' {{ '[hv'  'RET'  e' ; '/  '  Qs  ']' } } ']'"
).

Global Opaque spec_ctx.

Section tp_specs.
  Context `{!relocG Σ}.

  Open Scope expr_scope.

  Global Instance load_tp_spec (l : loc) E1 E2 :
    TPSPEC ⟨E1, E2⟩ v q, {{ l ↦ₛ{q} v } } ! #l {{ RET v; l ↦ₛ{q} v } }.
  Proof.
    apply tp_condition_execution_step_general => /= j K HE.
    iStep 4.
    rewrite -step_load //.
    iSteps.
  Qed.

  Global Instance store_tp_spec (l : loc) E1 E2 e v':
    IntoVal e v' →
    TPSPEC ⟨E1, E2⟩ v, {{ l ↦ₛ v } } #l <- e {{ RET #(); l ↦ₛ v' } }.
  Proof.
    move => <-.
    apply tp_condition_execution_step_general => /= j K HE.
    iStep 3.
    rewrite -step_store //.
    iSteps.
  Qed.

  Global Instance alloc_tp_spec E1 e v :
    IntoVal e v →
    TPSPEC ⟨E1⟩ {{ True } } ref e {{ (l : loc), RET #l; l ↦ₛ v } }.
  Proof.
    move => <-.
    apply tp_condition_execution_step_general => /= j K HE.
    iStep 2.
    rewrite -step_alloc //.
    iSteps.
  Qed.

  Global Instance cmpxchg_tp_spec E1 E2 (l : loc) e1 e2 v1 v2 :
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    TPSPEC ⟨E1, E2⟩ v, {{ l ↦ₛ v ∗ ⌜vals_compare_safe v v1⌝ } } 
        CmpXchg #l e1 e2 
      {{ (b : bool), RET (v, #b)%V; ⌜b = true⌝ ∗ ⌜v = v1⌝ ∗ l ↦ₛ v2 ∨ ⌜b = false⌝ ∗ ⌜v ≠ v1⌝ ∗ l ↦ₛ v } }.
  Proof.
    move => <-<-.
    apply: tp_condition_execution_step_general => //= j K HE.
    iStep 3 as (v' Hv) "Hs Hj Hl".
    destruct (decide (v1 = v')) as [-> | Hneq].
    - iExists true. rewrite -bi.or_intro_l !bi.pure_True // !left_id.
      rewrite -step_cmpxchg_suc //.
      iSteps.
    - iExists false. rewrite -bi.or_intro_r !bi.pure_True // !left_id.
      rewrite -step_cmpxchg_fail //.
      iSteps.
  Qed.

  Global Instance fork_tp_spec E1 e :
    TPSPEC ⟨E1⟩ {{ True } } Fork e {{ (i : nat), RET #(); i ⤇ e } }.
  Proof.
    apply: tp_condition_execution_step_general => //= j K HE.
    iStep 3 as "Hs Hj".
    iCombine "Hs Hj" as "H". rewrite step_fork //.
    iMod "H". iDecompose "H". iSteps.
  Qed.

  Global Instance pure_tp_spec E1 φ n e e' :
    PureExec φ n e e' →
    TPSPEC ⟨E1⟩ {{ ⌜φ⌝ } } e {{ RET e'; emp } }.
  Proof.
    move => Hφe.
    apply: tp_condition_execution_step_general => //= j K HE.
    iStep 2.
    rewrite right_id. iApply step_pure; last iFrame; done.
  Qed.

  Global Instance newproph_tp_spec E1 :
    TPSPEC ⟨E1⟩ {{ True } } NewProph {{ (p : proph_id), RET #p; True } }.
  Proof.
    apply: tp_condition_execution_step_general => //= j K HE.
    iStep 2 as "Hs Hj".
    iCombine "Hs Hj" as "H". iMod (step_newproph with "H") as "H" => //.
    iDecompose "H". iSteps.
  Qed.

  Global Instance resolveproph_tp_spec E1 (p : proph_id) w :
    TPSPEC ⟨E1⟩ {{ True } } ResolveProph #p (of_val w) {{ RET #(); True } }.
  Proof.
    apply: tp_condition_execution_step_general => //= j K HE.
    iStep 2 as "Hs Hj".
    iCombine "Hs Hj" as "H". iMod (step_resolveproph with "H") as "H" => //.
    iSteps.
  Qed.

  Global Instance faa_tp_spec (l : loc) E1 E2 (z : Z) :
    TPSPEC ⟨E1, E2⟩ (z' : Z), {{ l ↦ₛ #z' } } FAA #l #z {{ RET #z'; l ↦ₛ #(z' + z) } }.
  Proof.
    apply tp_condition_execution_step_general => /= j K HE.
    iStep 3.
    erewrite <-step_faa.
    iSteps. exact NONE. done. done.
  Qed.

  Global Instance xchg_tp_spec (l : loc) E1 E2 (nv : val) :
    TPSPEC ⟨E1, E2⟩ (ov : val), {{ l ↦ₛ ov } } Xchg #l nv {{ RET ov; l ↦ₛ nv } }.
  Proof.
    apply tp_condition_execution_step_general => /= j K HE.
    iStep 3.
    erewrite <-step_xchg.
    iSteps. done. done.
  Qed.
End tp_specs.















