From diaframe.reloc Require Import proof_automation symb_exec.
From diaframe.symb_exec Require Import defs.


Section extra_hint.
  Context `{!relocG Σ}.

  Global Instance restore_or_right_execute e el E A K e_in' T e_out' MT MT' R' :
    TCIf (TCEq E ⊤) False TCTrue →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep tp_reduction_condition T (ε₀)%I E e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition right_template_condition [tele_arg3 el; E; A] MT R' MT' →
    HINT1 ε₁ ✱ [(|={E,⊤}=> REL el << e : A) ∨ (MT' $ flip right_execute R' ∘ K ∘ e_out')] ⊫ [id]; REL el << e @ E : A | 999.
  Proof.
    intros. rewrite /Abduct. iIntros "[? [?|?]]"; iStopProof.
    - iStep. by iApply restore_mask.
    - eapply execution_abduct_lem => //. tc_solve. tc_solve. rewrite empty_hyp_last_eq -empty_hyp_first_eq. tc_solve.
  Qed.
End extra_hint.