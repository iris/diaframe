From iris.bi Require Export bi telescopes lib.atomic.
From iris.proofmode Require Import tactics notation reduction.
From iris.program_logic Require Import weakestpre lifting atomic.
From diaframe Require Import proofmode_base.
From diaframe.lib Require Import greatest_fixpoint atomic.

Section atomic_post_acc.
  Context `{BiFUpd PROP}.

  (* atomic accessor where the commiting mask is a parameter. *)
  Definition atomic_post_acc {TA TB : tele} Eo Ei Ef (α : TA → PROP) P (β Φ : TA → TB → PROP) : PROP :=
    |={Eo, Ei}=> ∃ x, α x ∗ (α x ={Ei, Eo}=∗ P) ∧ (∀.. y, β x y ={Ei, Ef}=∗ Φ x y).

  Lemma atomic_post_acc_equiv {TA TB : tele} Eo Ei (α : TA → PROP) P (β Φ : TA → TB → PROP) :
    atomic_acc Eo Ei α P β Φ ⊣⊢ atomic_post_acc Eo Ei Eo α P β Φ.
  Proof.
    rewrite /atomic_acc /atomic_post_acc.
    apply (anti_symm _).
    - iIntros ">[%x [Hα Hr]]".
      iExists _. by iFrame.
    - iIntros ">[%x [Hα Hr]]".
      iExists _. by iFrame.
  Qed.

  Context {TA TB : tele}.
  Context (Eo Ei Ef : coPset) (α : TA → PROP) (β Φ : TA → TB → PROP).

  Definition atomic_post_update_pre (Ψ : () → PROP) (_ : ()) : PROP :=
    atomic_post_acc Eo Ei Ef α (Ψ ()) β Φ.

  Instance atomic_post_update_pre_mono : fixpoint.BiMonoPred atomic_post_update_pre.
  Proof.
    constructor.
    - iIntros (P1 P2 ??) "#HP12". iIntros ([]) "AU".
      rewrite /atomic_post_update_pre /atomic_post_acc.
      iMod "AU" as "[%x [H1 H2]]".
      iExists _. iFrame. iIntros "!>".
      iSplit. 
      * iIntros "Ha". iApply "HP12". by iApply "H2".
      * iIntros (y) "Hb". by iApply "H2".
    - intros ??. solve_proper.
  Qed.

  Definition atomic_post_update_def :=
    fixpoint.bi_greatest_fixpoint atomic_post_update_pre ().
End atomic_post_acc.

(** Seal it *)
Definition atomic_post_update_aux : seal (@atomic_post_update_def). Proof. by eexists. Qed.
Definition atomic_post_update := atomic_post_update_aux.(unseal).
Global Arguments atomic_post_update {PROP _ TA TB}.
Global Arguments atomic_post_acc {PROP _ TA TB} Eo Ei Ef _ _ _ _ : simpl never.
Definition atomic_post_update_eq : @atomic_post_update = _ := atomic_post_update_aux.(seal_eq).


From diaframe.symb_exec Require Import defs weakestpre_logatom.

Section lemmas.
  Context `{BiFUpd PROP}.

  Local Existing Instance atomic_post_update_pre_mono.

  Lemma atomic_post_update_equiv {TA TB : tele} Eo Ei (α : TA → PROP) (β Φ : TA → TB → PROP) :
    atomic_update Eo Ei α β Φ ⊣⊢ atomic_post_update Eo Ei Eo α β Φ.
  Proof.
    rewrite atomic.atomic_update_unseal /atomic.atomic_update_def.
    rewrite atomic_post_update_eq /atomic_post_update_def.
    apply (anti_symm _).
    - iApply (fixpoint.greatest_fixpoint_strong_mono with "[]").
      { apply atomic_update_pre_mono. }
      iIntros "!>" (F' []).
      rewrite /atomic_post_update_pre /atomic_update_pre.
      rewrite /atomic_acc /atomic_post_acc.
      iMod 1 as "[%x [Hα Hx]]".
      iExists _. by iFrame.
    - iApply (fixpoint.greatest_fixpoint_strong_mono with "[]").
      { apply atomic_update_pre_mono. }
      iIntros "!>" (F' []).
      rewrite /atomic_post_update_pre /atomic_update_pre.
      rewrite /atomic_acc /atomic_post_acc.
      iMod 1 as "[%x [Hα Hx]]".
      iExists _. by iFrame.
  Qed.

  Definition atomic_post_templateM (M : coPset) (P1 : PROP) Ef TT1 TT2 (P2 : TT1 -t> PROP) (Q1 Q2 : TT1 -t> TT2 -t> PROP) : (qprod TT1 TT2 → PROP) → PROP
    := (λ Ψ, |={⊤}=> P1 ∗ atomic_post_update (⊤∖ M) ∅ Ef (tele_app P2) (tele_app (tele_map tele_app Q2)) (λ a b, tele_app (tele_app Q1 a) b -∗ Ψ (QPair a b)))%I.

  Global Arguments atomic_post_templateM M P1 Ef TT1 TT2 P2 Q1 Q2 Ψ /.

  Lemma atomic_post_templateM_equiv (M : coPset) (P1 : PROP) TT1 TT2 (P2 : TT1 -t> PROP) (Q1 Q2 : TT1 -t> TT2 -t> PROP) Ψ :
    atomic_post_templateM M P1 (⊤ ∖ M) TT1 TT2 P2 Q1 Q2 Ψ ⊣⊢ atomic_templateM M P1 TT1 TT2 P2 Q1 Q2 Ψ.
  Proof.
    rewrite /atomic_post_templateM /atomic_templateM.
    rewrite atomic_post_update_equiv //.
  Qed.

  Lemma atomic_post_update_mono {TA TB TB' : tele} Eo Ei Ef1 Ef2 (α α' : TA → PROP) (β Φ : TA → TB → PROP) (β' Ψ : TA → TB' → PROP) :
    atomic_post_update Eo Ei Ef1 α β Φ -∗ (□ ((∀.. a, (|={Ei}=> α a) ∗-∗ |={Ei}=> α' a) ∧ (∀.. a, 
        ∃ (f: TB' → TB), (∀.. b', β' a b' ={Ei}=∗ β a (f b')) ∧ (∀.. b', Φ a (f b') ={Ef1, Ef2}=∗ Ψ a b')))) -∗ 
    atomic_post_update Eo Ei Ef2 α' β' Ψ.
  Proof.
    rewrite atomic_post_update_eq /atomic_post_update_def.
    iIntros "HΦ #(Ha & Hf)".
    iApply (fixpoint.greatest_fixpoint_strong_mono with "[] HΦ").
    iIntros "!>" (F' []).
    rewrite /atomic_post_update_pre /atomic_post_acc.
    iMod 1 as "[%x [Hα Hx]]".
    iExists _.
    iAssert (|={Ei}=> α x)%I with "[Hα]" as "Hα"; first eauto.
    iDestruct ("Ha" with "Hα") as ">$".
    iIntros "!>". iSplit.
    - iIntros "Hα". 
      iDestruct "Hx" as "[Hx _]".
      iApply fupd_trans.
      iApply "Hx".
      iApply "Ha". by iFrame.
    - iIntros (y) "Hβ".
      iDestruct "Hx" as "[_ Hx]".
      iDestruct ("Hf" $! x) as "[%f [HB H]]".
      iApply fupd_trans.
      iApply "H".
      iApply fupd_trans.
      iApply "Hx".
      iApply "HB".
      iApply "Hβ".
  Qed.

  Lemma atomic_post_update_mask_change {TA TB : tele} Eo Ei Ef1 Ef2 (α : TA → PROP) (β Φ : TA → TB → PROP) :
    atomic_post_update Eo Ei Ef1 α β (λ a b, fupd Ef1 Ef2 $ Φ a b) -∗
    atomic_post_update Eo Ei Ef2 α β Φ.
  Proof.
    iIntros "H".
    iApply (atomic_post_update_mono with "H").
    iIntros "!>". iSplit; eauto.
    iIntros (a). iExists id. iSplit; eauto.
  Qed.

  Lemma atomic_post_templateM_is_mono M P1 Ef TT1 TT2 P2 Q1 Q2 :
    template_mono (atomic_post_templateM M P1 Ef TT1 TT2 P2 Q1 Q2).
  Proof.
    move => S T HST /=.
    apply fupd_mono, bi.sep_mono_r.
    iIntros "H".
    iApply (atomic_post_update_mono with "H").
    iIntros "!>"; iSplit; first eauto.
    iIntros (a). iExists id.
    iSplit; first eauto.
    iIntros (b) => /=.
    rewrite -fupd_intro. iStopProof.
    apply bi.wand_intro_l.
    rewrite right_id.
    apply bi.wand_mono; first done.
    apply HST.
  Qed.

  Lemma atomic_post_templateM_is_absorbing M P1 Ef TT1 TT2 P2 Q1 Q2 :
    Affine (PROP := PROP) True%I →
    template_absorbing (atomic_post_templateM M P1 Ef TT1 TT2 P2 Q1 Q2).
  Proof.
    move => Htrue P Q /=.
    iIntros "[>[$ H] HQ] !>".
    rewrite {2}atomic_post_update_eq /atomic_post_update_def.
    iApply (fixpoint.greatest_fixpoint_coiter _ (λ _, _) with "[] [H HQ]"); last iAccu.
    iIntros "!>" ([]) "[HAU HQ]".
    rewrite /atomic_post_update_pre /atomic_post_acc.
    rewrite atomic_post_update_eq {1}/atomic_post_update_def fixpoint.greatest_fixpoint_unfold /atomic_post_update_pre /atomic_post_acc.
    iMod "HAU" as "[%x [HP Hr]]".
    iExists _. by iFrame.
  Qed.
End lemmas.


Section automation.
  Context `{BiFUpd PROP}.

  Lemma atomic_post_update_to_gfp {TA TB : tele} Eo Ei Ef (α : TA → PROP) (β Φ : TA → TB → PROP) :
    run_greatest_fixpoint (λ Ψ, atomic_post_acc Eo Ei Ef α Ψ β Φ) ⊢ atomic_post_update Eo Ei Ef α β Φ.
  Proof.
    rewrite run_greatest_fixpoint_eq /run_greatest_fixpoint_def
            atomic_post_update_eq /atomic_post_update_def //.
  Qed.

  (* this instance is necessary to prevent looping TC search *)
  Global Instance au_later_except_0 {TA TB : tele} Eo Ei Ef (α : TA → PROP) (β Φ : TA → TB → PROP) :
    LaterToExcept0 (atomic_post_update Eo Ei Ef α β Φ) (▷ atomic_post_update Eo Ei Ef α β Φ)%I | 10.
  Proof. rewrite /LaterToExcept0 //. eauto. Qed.

  (* an atomic post accessor in a shape more accessible to the automation *)
  (* We need to use option2 to get around the universe constraints: option < tele *)
  Definition atomic_post_acc' {TA TB : tele} Eo Ei Ef (α : TA → PROP) P (β Φ : TA → TB → PROP) : PROP :=
    |={Eo, Ei}=> ∃ x, α x ∗
      (∀ my : option2 (tele_arg TB), 
          ((α x ∧ ⌜my = None2⌝) ∨ (∃.. y, β x y ∧ ⌜my = Some2 y⌝)) -∗
            match my with
            | None2 => |={Ei, Eo}=> P
            | Some2 y => |={Ei, Ef}=> Φ x y
            end).

  Lemma atomic_accs_equiv {TA TB : tele} Eo Ei Ef (α : TA → PROP) P (β Φ : TA → TB → PROP) :
    atomic_post_acc Eo Ei Ef α P β Φ ⊣⊢ atomic_post_acc' Eo Ei Ef α P β Φ.
  Proof.
    (* cannot use automation here since operating on abstract telescopes *)
    apply (anti_symm _); rewrite /atomic_post_acc /atomic_post_acc'. 
    - iMod 1 as "[%x [Hα Hx]]".
      iExists _. iFrame.
      iIntros "!>" ([y|]) "[[Hα %]|[%y' [Hβ %]]]"; try simplify_eq.
      * iDestruct "Hx" as "[_ Hx]".
        by iApply "Hx".
      * iDestruct "Hx" as "[Hx _]".
        by iApply "Hx".
    - iMod 1 as "[%x [Hα Hx]]".
      iExists _. iFrame.
      iIntros "!>"; iSplit.
      * iIntros "Hα".
        iSpecialize ("Hx" $! None2).
        iApply "Hx". iLeft. by iFrame.
      * iIntros (y) "Hβ".
        iSpecialize ("Hx" $! (Some2 y)).
        iApply "Hx". iRight. iExists _. by iFrame.
  Qed.

  (* when we need to prove an atomic update, we first run the greatest laterable fixpoint *)
  Global Instance abduct_atomic_post_update {TA TB : tele} Eo Ei Ef (α : TA → PROP) (β Φ : TA → TB → PROP) :
    HINT1 ε₁ ✱ [run_greatest_fixpoint (λ Ψ, atomic_post_acc' Eo Ei Ef α Ψ β Φ)] 
      ⊫ [id]; atomic_post_update Eo Ei Ef α β Φ.
  Proof.
    rewrite /Abduct /= empty_hyp_last_eq left_id. rewrite <-atomic_post_update_to_gfp.
    rewrite run_greatest_fixpoint_eq /run_greatest_fixpoint_def.
    rewrite {1}greatest_fixpoint_proper //.
    move => R [] /=.
    generalize (R ()) => R' {R}.
    rewrite atomic_accs_equiv //.
  Qed.

  Class SimplifyMask (E E' : coPset) :=
    mask_simplify : E = E'.

  Global Instance simplify_sub_emptyset E : SimplifyMask (E ∖ ∅) E | 10.
  Proof. rewrite /SimplifyMask difference_empty_L //. Qed.

  Global Instance simplify_mask_fallback E : SimplifyMask E E | 20.
  Proof. done. Qed.

  (* after running the fixpoint and introducing make_laterable, we proceed as follows: *)
  Global Instance atomic_post_acc_abd {TA TB : tele} Eo Eo' Ei' Ei Ef (α : TA → PROP) P (β Φ : TA → TB → PROP) :
    SimplifyMask Eo Eo' →
    HINT1 ε₀ ✱ [
        |={Eo', Ei'}=> ∃.. x, α x ∗ (* A neat trick is that we need Ei ⊆ Ei', but we can actually defer that to below! *)
          ((α x ={Ei', Eo'}=∗ ⌜Ei ⊆ Ei'⌝ ∧ (emp ={Eo'}=∗ P)) ∧
           (∀.. y : TB, β x y ={Ei', Ef}=∗ Φ x y)) (*
        (∀ (b : bool), ⌜b = true⌝ ∨ ⌜b = false⌝ -∗ ∀.. (my : if b then TB else [tele]),
          α x ∧ ⌜b = false⌝ ∨ (∃.. y : TB, β x y ∧ ⌜b = true⌝ ∧ ⌜match b, my with | true, y' => tele_app (tele_app beq y') y | false, _ => False end⌝) 
              -∗ match b, my with
                 | false, _ => |={Ei', Eo'}=> ⌜Ei ⊆ Ei'⌝ ∧ P
                 | true, my => |={Ei', Ef}=> Φ x my
                 end) *)
      ] ⊫ [id]; atomic_post_acc' Eo Ei Ef α P β Φ.
  Proof.
    rewrite /Abduct /atomic_post_acc' /= empty_hyp_first_eq left_id => ->.
    iIntros ">[%x (Hα & Hy)]".
    destruct (decide (Ei ⊆ Ei')); last first.
    { iDestruct "Hy" as "[Hy _]". 
      iMod ("Hy" with "Hα") as "[% H]". contradiction. }
    iMod (fupd_mask_subseteq) as "Hcl"; last iIntros "!>". exact s.
    iExists _. iFrame.
    iIntros (my) "[[Hα ->] | [%y [Hβ ->]]]"; iMod "Hcl" as "_".
    * iDestruct "Hy" as "[Hy _]". iMod ("Hy" with "Hα") as "[% HP]". by iApply "HP".
    * iDestruct "Hy" as "[_ Hy]". iMod ("Hy" with "Hβ") as "HΦ". by iIntros "!>".
  Qed.

End automation.

Global Opaque atomic_post_acc'.

Unset Universe Polymorphism.

