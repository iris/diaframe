From diaframe.heap_lang Require Export proof_automation.
From diaframe.lib Require Import own_hints.
From iris.heap_lang Require Import proofmode. 

From actris Require Export proto.

Set Universe Polymorphism.

Class MsgTele2 {Σ V} {TT : tele} (m : iMsg Σ V)
  (tv : TT -t> V) (tP : TT -t> iProp Σ) (tp : TT -t> iProto Σ V) :=
msg_tele : m ≡ (∃.. x, MSG tele_app tv x {{ tele_app tP x }}; tele_app tp x)%msg.
Global Hint Mode MsgTele ! ! - ! - - - : typeclass_instances.

Section msgtele2.
  Context `{!protoG Σ V}.
  Implicit Types v : V.
  Implicit Types p pl pr : iProto Σ V.
  Implicit Types m : iMsg Σ V.

  Global Instance msg_tele_base (v:V) (P : iProp Σ) (p : iProto Σ V) :
    MsgTele2 (TT:=TeleO) (MSG v {{ P }}; p) v P p.
  Proof. done. Qed.
  Global Instance msg_tele_exist {A} {TT : A → tele} (m : A → iMsg Σ V) tv tP tp :
  (∀ x, MsgTele2 (TT:=TT x) (m x) (tv x) (tP x) (tp x)) →
  MsgTele2 (TT:=TeleS TT) (∃ x, m x) tv tP tp.
  Proof. intros Hm. rewrite /MsgTele2 /=. f_equiv=> x. apply Hm. Qed.
End msgtele2.

Unset Universe Polymorphism.

From diaframe.symb_exec Require Import defs weakestpre.

From actris Require Export channel proofmode.
From actris.utils Require Import llist.
Import bi.

Section llist_instances.
  Context `{heapGS Σ} {A} (I : A → val → iProp Σ).
  Implicit Types xs : list A.
  Implicit Types l : loc.

  Opaque llist lisnil lpop lprep lsnoc llength.

  Global Instance lisnil_step xs (l : loc) :
    SPEC {{ llist I l xs }}
      lisnil #l
    {{ (b : bool), RET #b; ((⌜b = true⌝ ∧ ⌜xs = []⌝) ∨ (⌜b = false⌝ ∧ ∃ x xs', ⌜xs = x :: xs'⌝)) ∗ llist I l xs }} | 50.
  Proof.
    iSteps as "Hl".
    wp_apply (lisnil_spec with "Hl").
    destruct xs; iSteps.
  Qed.

  Global Instance lpop_step x xs l :
    SPEC {{ llist I l (x :: xs) }}
      lpop #l
    {{ (v : val), RET v; llist I l xs ∗ I x v }} | 50.
  Proof. 
    iSteps as "Hl".
    wp_apply (lpop_spec with "[Hl]"); iSteps.
  Qed.

  Global Instance lprep_step xs1 xs2 l1 l2 :
    SPEC {{ llist I l1 xs1 ∗ llist I l2 xs2 }}
      lprep #l1 #l2
    {{ (v : val), RET #(); llist I l1 (xs2 ++ xs1) ∗ l2 ↦ v }} | 50.
  Proof.
    iSteps as "Hl1 Hl2".
    wp_apply (lprep_spec with "[Hl1 Hl2]"); iSteps.
  Qed.

  Global Instance lsnoc_step x xs l (v : val) : 
    SPEC {{ llist I l xs ∗ I x v }}
      lsnoc #l v
    {{ RET #(); llist I l (xs ++ [x]) }}.
  Proof.
    iSteps as "Hl Hx".
    wp_apply (lsnoc_spec with "[$Hl Hx]"); iSteps.
  Qed.

  Global Instance llength_step xs l : 
    SPEC {{ llist I l xs }}
      llength #l
    {{ RET #(length xs); llist I l xs }}.
  Proof.
    iSteps as "Hl".
    wp_apply (llength_spec with "Hl"). iSteps.
  Qed.
End llist_instances.

Global Opaque llist.

Section instances.
  Context `{!heapGS Σ, !chanG Σ}.
  Implicit Types p : iProto Σ.
  Implicit Types TT : tele.

  Lemma recv_spec_later {TT} c (v : TT → val) (P : TT → iProp Σ) (p : TT → iProto Σ) :
    {{{ ▷ c ↣ <?.. x> MSG v x {{ ▷ P x }}; p x }}}
      recv c
    {{{ x, RET v x; c ↣ p x ∗ P x }}}.
  Proof.
    iIntros (Φ) "Hc HΦ". iLöb as "IH". wp_lam.
    wp_apply (try_recv_spec (TT := TT) with "Hc").
    iIntros (w)"[[-> H]|H]".
    { iStep 8. iFrame "HΦ". iSteps. }
    iDestruct "H" as (x ->) "[Hc HP]". rewrite {2}bi.forall_elim. iSteps.
  Qed.

  (* if we want to turn this into an existential requirement, TT actually depends on the first argument.
    ReductionStep' currently does not support this. *)

  (* cant use spec notation here since it quantifies a telescope *)

  Global Instance recv_step_wp {TT} (c : val) (v : TT -t> val) (P : TT -t> iProp Σ) (p : TT -t> iProto Σ) :
    ReductionStep' wp_red_cond (ε₀)%I 1 (fupd ⊤ ⊤) (fupd ⊤ ⊤) [tele] TT
      (* all these nasty extra tele_bind/tele_maps are necessary for unification *)
      (tele_bind (λ _, c ↣ <?.. x> MSG (tele_app v) x {{ ▷ (tele_app P x) }}; tele_app p x)) 
      (tele_bind (λ _, tele_bind (λ t, c ↣ tele_app p t ∗ tele_app P t)))%I 
      (recv c) (tele_map (TT := [tele]) (tele_map of_val) v) [tele_arg ⊤; NotStuck].
  Proof.
    iSteps as (Φ) "Hc HΦ".
    iApply wp_fupd.
    iApply (recv_spec_later with "[$Hc]").
    iIntros "!>" (x').
    iSpecialize ("HΦ" $! x').
    rewrite !tele_map_app tele_app_bind /= wp_value_fupd.
    iSteps.
  Qed.

  Global Instance protonormalize_recv_biabd2 {TT} (c : val) p m tv tP tP' tp : 
    ProtoNormalize false p [] (<?> m) →
    MsgTele2 m tv tP tp →
    (∀.. x, MaybeIntoLaterN false 1 (tele_app tP x) (tele_app tP' x)) →
    HINT c ↣ p ✱ [- ; emp] ⊫ [id]; 
         c ↣ <?.. x> MSG (tele_app tv) x {{ ▷ (tele_app tP' x) }}; tele_app (TT:= TT) tp x ✱ [emp].
  Proof.
    rewrite /ProtoNormalize /MsgTele2 /MaybeIntoLaterN /= => Hpm Hm HtP.
    iIntros "[Hc _]" => /=. rewrite right_id in Hpm. rewrite right_id.
    iApply (iProto_pointsto_le with "Hc"). iIntros "!>".
    iApply iProto_le_trans; [iApply Hpm|rewrite Hm].
    iApply iProto_le_texist_elim_l; iIntros (x).
    iApply iProto_le_trans; [|iApply (iProto_le_texist_intro_r _ x)]; simpl.
    iIntros "H". by iDestruct (HtP with "H") as "$".
  Qed.

  Opaque spin_lock.is_lock iProto_ctx iProto_own spin_lock.release.

  Lemma send_spec_later c v p :
    {{{ ▷ c ↣ <!> MSG v; p }}}
      send c v
    {{{ RET #(); c ↣ p }}}.
  Proof.
    rewrite iProto_pointsto_eq. iIntros (Φ) "Hc HΦ".
    wp_lam.
    iDestruct "Hc" as (γl γr γlk l r lk ->) "[#Hlk H]".
    iSteps. iIntros "!>".
    (* FIXME: are these lock specs not registered? *)
    wp_apply (lock.acquire_spec). done.
    iStep 5 as (vsl vsr) "Hvsl Hvsr Hlkd Heq1 Heq2 Hctx". iIntros "!>".
    wp_bind (lsnoc _ _).
    iApply (wp_step_fupdN_lb with "Hvsr [Hctx H]"); [done| |].
    { iApply fupd_mask_intro; [set_solver|]. simpl.
      iIntros "Hclose !>!>".
      iMod (iProto_send with "Hctx H []") as "[Hctx H]".
      { rewrite iMsg_base_eq /=; auto. }
      iModIntro.
      iApply step_fupdN_intro; [done|].
      iIntros "!>". iMod "Hclose".
      iCombine ("Hctx H") as "H".
      iExact "H". }
    iApply (wp_lb_update with "Hvsl").
    iSteps as "Hvsr' Heq3 Hproto Hown".
    replace (S (length vsl)) with (length (vsl ++ [v])); last first.
    { rewrite length_app /=. lia. }
    iApply (lock.release_spec with "[Hlkd Heq2 Heq3 Hproto]");
    iSteps.
  Qed.

  Global Opaque send.

  Global Instance send_step (c v : val) p : 
    SPEC {{ ▷ c ↣ <!> MSG v; p }}
      send c v
    {{ RET#(); c ↣ p }}.
  Proof.
    iSteps as "Hc". wp_apply (send_spec_later with "Hc"); iSteps.
  Qed.

  Proposition send_proto_coincide {TT : tele} c p1 p2 v m tv tP tp:
    ProtoNormalize false p1 [] (<!> m) →
    MsgTele m tv tP tp →
    c ↣ p1 ∗ (∃.. x : TT, tele_app tP x ∗ ⌜v = tele_app tv x⌝ ∗ p2 ≡ tele_app tp x) ⊢ c ↣ <!> MSG v; p2.
  Proof.
    rewrite /ProtoNormalize /MsgTele bi_texist_exist /= right_id => Hp1 Hm.
    iIntros "[Hc Hm]". iApply (iProto_pointsto_le with "Hc"); iIntros "!>".
    iDestruct "Hm" as (x) "(Htp & -> & Hp2)".
    iApply iProto_le_trans; [iApply Hp1|]. rewrite Hm.
    iApply iProto_le_trans; [iApply iProto_le_texist_intro_l|]. iFrame. by iRewrite "Hp2".
  Qed.

  Global Instance biabd_proto_send {TT : tele} c p1 p2 v m tv tP tp :
    ProtoNormalize false p1 [] (<!> m) →
    MsgTele (TT := TT) m tv tP tp →
    BiAbd false (TTl := TT) (TTr := [tele]) (c ↣ p1) (c ↣ <!> MSG v; p2) id 
      (tele_bind (λ x, ⌜v = tele_app tv x⌝ ∗ tele_app tP x ∗ p2 ≡ tele_app tp x)%I) (tele_bind (λ _, emp))%I.
  Proof.
    rewrite /BiAbd /= => Hp1 Hm.
    apply tforall_forall => ttl.
    rewrite !tele_app_bind right_id.
    erewrite <-send_proto_coincide; [ | exact Hp1 | exact Hm].
    apply bi.sep_mono_r.
    rewrite bi_texist_exist -(bi.exist_intro ttl).
    iIntros "(-> & HP & Hp)".
    iRewrite "Hp".
    iSteps.
  Qed.

End instances.
