From diaframe.actris Require Export proof_automation.
From diaframe.lib Require Import own_hints.
From iris.heap_lang Require Import proofmode.

From actris.utils Require Import llist contribution.
Import bi.


From actris.examples Require Import par_map.
From actris.utils Require Import llist contribution.
From iris.heap_lang Require Import lib.spin_lock.
From iris.algebra Require Import gmultiset.

Section contribution_biabd.
  Context `{contributionG Σ A}.

  Global Instance deallocate_client γ i (x : A) :
    HINT server γ (S i) x ✱ [- ; client γ (ε : A)] ⊫ [bupd]; server γ i x ✱ [emp].
  Proof. 
    iStep as "Hs Hc".
    by iMod (@dealloc_client with "Hs Hc") as "$".
  Qed.

  Global Instance update_client_biabd γ i (x x' y y' : A) φ :
    FindLocalUpdate x x' y y' φ →
    HINT server γ i x ✱ [- ; client γ y ∗ ⌜φ⌝] ⊫ [bupd]; server γ i x' ✱ [client γ y'] | 60.
  (* should have precedence lower than assumption *)
  Proof.
    rewrite /FindLocalUpdate => Hφ. iStep as (Hφwit) "Hs Hc".
    iMod (@update_client with "Hs Hc") as "[$ $]"; eauto.
  Qed.

  Global Instance server_agree γ n (x y : A) P j :
    MergablePersist (server γ n x) (λ p Pin Pout,
      TCAnd (TCEq Pin (client γ y)) $
      TCAnd (IsIncludedMerge y x P) $
      TCIf (TCEq n (S j)) (TCEq Pout P) $ 
      (* need to take into account that rewrites from mergable persist might revert the key hypothesis! *)
           TCAnd (TCEq j 0%nat) 
          (TCEq Pout (P ∧ ∃ i, ⌜n = S i⌝ ∧ ⌜(n = 1 → x ≡ y)⌝)%I)).
  Proof.
    rewrite /MergablePersist => p Pin Pout.
    case => [-> [HP HPout]] /=.
    rewrite bi.intuitionistically_if_elim.
    iIntros "[Hs Hc]".
    iDestruct (@server_agree with "Hs Hc") as %[? ?]; destruct n as [|n] =>//=.
    iDestruct (server_valid with "Hs") as %?.
    iAssert (□ P)%I as "#HP".
    { rewrite /IsIncludedMerge in HP.
      iApply (HP); by iPureIntro. }
    destruct n as [|n].
    - iDestruct (@server_1_agree with "Hs Hc") as %?.
      iIntros "!>".
      case: HPout => [_ -> | [_ ->] ];
      iSteps.
    - iIntros "!>". 
      case: HPout => [_ -> | [_ ->] ];
      iSteps.
  Qed.

  Global Instance biabd_client_equiv γ (x y : A) :
    HINT client γ x ✱ [ - ; x ≡ y] ⊫ [id]; client γ y ✱ [emp]. (* this is probably a bad instance *)
  Proof.
    iStep as (Hxy) "Hc".
    rewrite Hxy. iSteps.
  Qed.

End contribution_biabd.

Local Existing Instance spin_lock.

Section actris_test.
  Context `{Countable A} {B : Type}.
  Context `{!heapGS Σ, !chanG Σ, !mapG Σ A}.
  Context (IA : A → val → iProp Σ) (IB : B → val → iProp Σ) (map : A → list B).
  Local Open Scope nat_scope.
  Implicit Types n : nat.

  Typeclasses Transparent iProto_choice.

  Definition par_map_protocol_aux (rec : nat -d> gmultiset A -d> iProto Σ) :
      nat -d> gmultiset A -d> iProto Σ := λ i X,
    let rec : nat → gmultiset A → iProto Σ := rec in
    (if i is 0 then END else
     ((<! x v> MSG v {{ IA x v }}; rec i (X ⊎ {[+ x +]}))
        <+>
      rec (pred i) X
     ) <{⌜ i ≠ 1 ∨ X = ∅ ⌝}&>
         <? x (l : loc)> MSG #l {{ llist IB l (map x) ∗ ⌜ x ∈ X ⌝ }};
         rec i (X ∖ {[+ x +]}))%proto.
  Instance par_map_protocol_aux_contractive : Contractive par_map_protocol_aux.
  Proof. solve_proper_prepare. f_equiv. solve_proto_contractive. Qed.
  Definition par_map_protocol := fixpoint par_map_protocol_aux.
  Global Instance par_map_protocol_unfold n X :
    ProtoUnfold (par_map_protocol n X) (par_map_protocol_aux par_map_protocol n X).
  Proof. apply proto_unfold_eq, (fixpoint_unfold par_map_protocol_aux). Qed.

  Definition map_worker_lock_inv2 (γ : gname) (c : val) : iProp Σ :=
    (∃ i X, c ↣ iProto_dual (par_map_protocol i X) ∗ server γ i X)%I.

  Arguments disj_union : simpl never.

  Instance texan_into_wand p P (e : expr) T Q E s (φv : T → val) :
    AtomIntoWand p ({{{ P }}} e @ s; E {{{ (a : T), RET φv a; Q a }}})
      P (WP e @ s ; E {{ v, ∃ a, ⌜v = φv a⌝ ∗ Q a }})%I.
    (* this instance removes the Φ redirection, at the cost of 1 later *)
  Proof.
    rewrite /AtomIntoWand /IntoWand2 bi.intuitionistically_if_elim.
    iStep 4. iExists (λ v, ∃ t, ⌜v = φv t⌝ ∗ Q t)%I. iSplitR; iSteps.
  Qed.

  Opaque is_lock.

  Global Instance par_map_worker_spec γl (γ : gname) (vmap lk c : val) : 
    SPEC {{ map_spec IA IB map vmap ∗ is_lock γl lk (map_worker_lock_inv2 γ c) ∗ client γ (∅ : gmultiset A) }}
      par_map_worker vmap lk c
    {{ RET #(); True }}.
  Proof.
    iSteps as "Hmap Hlk Hclient". iLöb as "IH".
    wp_lam.
    iSteps as (ms _ n _ Hnms) "??" /
           as (ms _ n _ Hnms [|]) "????".
    { iPureIntro. destruct n; eauto. right. fold_leibniz. by apply Hnms. }
    - iSteps; iPureIntro; multiset_solver.
    - iSteps.
  Qed.

  Instance map_spec_persistent {C D} 
      (IC : C → val → iPropI Σ) (ID : D → val → iPropI Σ) m vm : Persistent (map_spec IC ID m vm) := _.

  Opaque map_spec.

  Global Instance par_map_workers_spec2 γl γ n (vmap lk c : val) :
    SPEC {{ map_spec IA IB map vmap ∗ is_lock γl lk (map_worker_lock_inv2 γ c) ∗
        [∗] replicate n (client γ (∅:gmultiset A)) }}
      par_map_workers #n vmap lk c
    {{ RET #(); True }}.
  Proof.
    iSteps.
    iInduction n as [|n] "IH"; wp_lam; iSteps.
    rewrite Nat2Z.inj_succ Z.sub_1_r Z.pred_succ.
    iSteps.
  Qed.

  Opaque newlock.

  Global Instance par_map_service_spec2 n (vmap c : val) :
    SPEC {{ map_spec IA IB map vmap ∗ c ↣ iProto_dual (par_map_protocol n ∅) }}
      par_map_service #n vmap c
    {{ RET #(); True }}.
  Proof.
    iSteps as "Hm Hc".
    iMod (contribution_init_pow (A:=gmultisetUR A) n) as (γ) "[Hs Hγs]". iIntros "!>".
    wp_apply (newlock_spec (map_worker_lock_inv2 γ c) with "[Hc Hs]");
    iSteps.
  Qed.

  Instance simplify_Znat_neq_0 (n : nat) : SimplifyPureHypSafe (Z.of_nat n ≠ 0) (∃ m, n = S m).
  Proof. split. destruct n => Hn; eauto. by contradict Hn. case => m -> //. Qed.

  Instance simplify_Znat_eq_0 (n : nat) : SimplifyPureHypSafe (Z.of_nat n = 0) (n = 0).
  Proof. split; lia. Qed.

  Global Instance par_map_client_loop_spec2 n (c : val) (l k : loc) xs X ys :
    SPEC {{ llist IA l xs ∗ llist IB k ys ∗ c ↣ par_map_protocol n X ∗ ⌜n = 0 → X = ∅ ∧ xs = []⌝ }}
      par_map_client_loop #n c #l #k
    {{ ys', RET #(); llist IA l [] ∗ llist IB k (ys' ++ ys) ∗ ⌜ys' ≡ₚ (xs ++ elements X) ≫= map⌝ }}.
  Proof.
    iStartProof2 => /=. iLöb as "IH" forall (n l xs X ys).
    iStep as (HnX) "???". wp_lam. iSteps as "Hc" / as (n b) "HSnX Hc".
    { destruct HnX as [-> ->]; try lia.
      iSteps.
      iExists []; simpl.
      iSteps. }
    destruct b; iStep 2.
    - iDestruct "HSnX" as %Hnx.
      iSteps as (a las _ lbs Hlbs) / as "Hl_nil Hc".
      + iPureIntro. rewrite Hlbs.
        rewrite gmultiset_elements_disj_union gmultiset_elements_singleton.
        rewrite assoc_L -(comm _ [a]) //.
      + (* IH is not applicable since types don't match up *)
        rewrite Nat2Z.inj_succ Z.sub_1_r Z.pred_succ.
        iSteps. iPureIntro.
        destruct Hnx; naive_solver.
    - iSteps as (a l' Ha v lbs Hlbs) "Hl HIB".
      rewrite assoc_L.
      iSteps. iPureIntro.
      rewrite (gmultiset_disj_union_difference {[+ a +]} X) -?gmultiset_elem_of_singleton_subseteq //; last multiset_solver.
      rewrite (comm_L disj_union) gmultiset_elements_disj_union.
      by rewrite gmultiset_elements_singleton assoc_L bind_app /= Hlbs right_id_L.
  Qed.

  Opaque par_map_service.

  Global Instance llist_empty_biabd `(IC : C → val → iProp Σ) l xs : 
    HINT l ↦ NONEV ✱ [- ; ⌜xs = []⌝] ⊫ [id]; llist IC l xs ✱ [⌜xs = []⌝].
  Proof. iSteps. Qed.

  Opaque fork_chan.

  Global Instance par_map_client_spec2 n (vmap : val) (l : loc) xs :
    SPEC {{ map_spec IA IB map vmap ∗ llist IA l xs ∗ ⌜0 < n⌝ }}
      par_map_client #n vmap #l
    {{ ys, RET #(); llist IB l ys ∗ ⌜ys ≡ₚ xs ≫= map⌝ }}.
  Proof.
    iSteps as (Hn) "Hmap HIA". iIntros "!>".
    wp_apply (fork_chan_spec (par_map_protocol n ∅)).
    { iSteps. } Opaque elements. (* prevents cbn from unfolding elements *)
    iSteps as_anon / as (_ l' lbs Hlbs) "HIA HIB". { iPureIntro. lia. } iIntros "!>".
    wp_apply (lapp_spec IB l l' [] with "[HIA HIB]"); iSteps. iPureIntro.
    rewrite right_id Hlbs gmultiset_elements_empty right_id //.
  Qed.

End actris_test.




















