From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From iris.heap_lang Require Import proofmode notation atomic_heap.
From iris_examples.logatom.elimination_stack Require Import stack.
From iris.algebra Require Import excl auth list frac agree.
From diaframe.lib Require Import own_hints.

Definition stackR := (prodR fracR $ agreeR $ listO valO). (* used to be authUR $ optionUR $ exclR $ listO valO *)
Class stack2G Σ := StackG {
  #[local] stack_tokG :: inG Σ $ optionR $ exclR unitO;
  #[local] stack_stateG :: inG Σ stackR;
 }.
Definition stack2Σ : gFunctors :=
  #[GFunctor (optionUR $ exclR unitO); GFunctor stackR].

Section stack.
  Context {aheap: atomic_heap} `{!heapGS Σ, !stack2G Σ, !atomic_heapGS Σ}  (N : namespace).
  Notation iProp := (iProp Σ).

  Let offerN := N .@ "offer".
  Let stackN := N .@ "stack".

  Import atomic_heap.notation.

  Definition stack_content (γs : gname) (l : list val) : iProp :=
    (own γs ((1/2)%Qp, to_agree l)). (* (own γs (◯ Excl' l))%I. *)
  Global Instance stack_content_timeless γs l : Timeless (stack_content γs l) := _.

  Fixpoint list_inv (l : list val) (rep : val) : iProp :=
    match l with
    | nil => ⌜rep = NONEV⌝
    | v::l => ∃ (ℓ : loc) (rep' : val), ⌜rep = SOMEV #ℓ⌝ ∗
                              ℓ ↦□ (v, rep') ∗ list_inv l rep'
    end%I.

  Global Instance list_inv_pers l rep : Persistent (list_inv l rep).
  Proof. revert rep. induction l; tc_solve. Qed.

  Global Instance list_inv_timeless l rep : Timeless (list_inv l rep).
  Proof. revert rep. induction l; tc_solve. Qed.

  Definition offer_inv (st_loc : loc) (v : val) (γo γs : gname) (Q : iProp) : iProp :=
    ∃ st : Z, st_loc ↦ #st ∗
      ((⌜st = 0⌝ ∗ own γo (None) ∗ AU <{ ∃∃ l, stack_content γs l }> @ ⊤∖↑N, ∅
                                      <{ stack_content γs (v::l), COMM Q }>) ∨ 
       (⌜st = 1⌝ ∗ (Q ∨ own γo (Excl' ()))) ∨ 
       (⌜st = 2⌝ ∗ own γo (Excl' ()))).

  Definition is_offer (γs : gname) (offer_rep : val) : iProp :=
    ⌜offer_rep = NONEV⌝ ∨ ∃ (v : val) (st_loc : loc),
      ⌜offer_rep = SOMEV (v, #st_loc)⌝ ∗
      ∃ γo Q, inv offerN (offer_inv st_loc v γo γs Q).

  Arguments is_offer γs !offer_rep /.

  Local Instance is_offer_persistent γs offer_rep : Persistent (is_offer γs offer_rep).
  Proof. apply _. Qed.

  Definition stack_inv1 (γs : gname) (head : loc) : iProp :=
    (∃ stack_rep l, 
       head ↦ stack_rep ∗ list_inv l stack_rep ∗ own γs ((1/2)%Qp, to_agree l) ∗ emp%I)%I. 
  (* (∗ emp) allows us some leeway to open additional invariants/atomic updates to get own γs *)

  Definition stack_inv2 (γs : gname) (offer : loc) : iProp :=
    (∃ offer_rep, 
       offer ↦ offer_rep ∗ is_offer γs offer_rep)%I.

  Definition is_stack (γs : gname) (s : val) : iProp :=
    (∃ head offer : loc, ⌜s = (#head, #offer)%V⌝ ∗ inv stackN (stack_inv1 γs head)
                                                 ∗ inv stackN (stack_inv2 γs offer))%I.
  Global Instance is_stack_persistent γs s : Persistent (is_stack γs s) := _.

  Lemma is_list_agree xs ys v : list_inv xs v ∗ list_inv ys v ⊢ ⌜xs = ys⌝.
  Proof.
    revert ys v.
    induction xs.
    { move => [| y ys]; iSteps. }
    move => [| y ys]; iSteps.
    iAssert (⌜xs = ys⌝)%I as "->"; last done.
    iApply (IHxs ys x1). iSteps.
  Qed.

  Global Instance is_list_merge xs v ys :
    MergableConsume (list_inv xs v) true (λ p Pin Pout,
      TCAnd (TCEq p true) $
      TCAnd (TCEq Pin (list_inv ys v)) $
             TCEq Pout (list_inv xs v ∗ ⌜xs = ys⌝)%I).
  Proof.
    rewrite /MergableConsume /= => p Pin Pout [-> [-> ->]] /=.
    iStep 2. iApply (is_list_agree _ _ (v)). iSteps.
  Qed.

  Global Instance biabd_islist_none xs :
    HINT ε₀ ✱ [- ; ⌜xs = []⌝] ⊫ [id]; list_inv xs NONEV ✱ [⌜xs = []⌝].
  Proof. iSteps. Qed.

  Lemma list_inv_unboxed xs v : list_inv xs v ⊢ ⌜val_is_unboxed v⌝%I.
  Proof. induction xs; iSteps. Qed.

  Global Instance biabd_islist_keep_unboxed xs v :
    HINT list_inv xs v ✱ [- ; emp] ⊫ [id]; list_inv xs v ✱ [⌜val_is_unboxed v⌝] | 40.
  Proof.
    iStep.
    iAssert ⌜val_is_unboxed v⌝%I as "%". { by iApply list_inv_unboxed. }
    iSteps.
  Qed.

  Obligation Tactic := program_smash_verify.

  Global Program Instance biabd_islist_some (l : loc) xs :
    HINT ε₁ ✱ [x xs' t; l ↦□ (x, t) ∗ list_inv xs' t ∗ ⌜xs = x :: xs'⌝] ⊫ [id]; 
         list_inv xs (SOMEV #l) ✱ [⌜xs = x :: xs'⌝].

  Obligation Tactic := program_smash_verify.

  (** Proofs. *)
  Global Program Instance new_stack_spec :
    SPEC {{ True }} new_stack #() {{ (γs : gname) (s : val), RET s; is_stack γs s ∗ stack_content γs [] }}.

  Global Instance frac_agree_upd γs qin qout mqin mqout l1 l2 :
    TCIf (SolveSepSideCondition (l1 = l2)) False TCTrue →
    (* makes sure we find a better instance if they are unifiable *)
    FracSub 1 qin mqin →
    FracSub 1 qout mqout →
    HINT own γs (qin, to_agree l1) ✱ [l3; match mqin with | Some q' => own γs (q', to_agree l3) | None => ⌜l3 = l1⌝ end] 
          ⊫ [bupd]; own γs (qout, to_agree l2) ✱ [match mqout with | Some q' => own γs (q', to_agree l2) | None => emp end ∗ ⌜l3 = l1⌝] | 55.
  Proof.
    move => _ Hq1 Hq2. iStep as (vs).
    enough (own γs (1%Qp, to_agree l1) ⊢ |==> own γs (1%Qp, to_agree l2)) as Hγsu.
    destruct mqin; destruct mqout; iStep; rewrite Hq1;
    try (rewrite assoc -own_op -pair_op frac_op (comm Qp.add) agree_idemp); rewrite Hq2 Hγsu; iSteps.
    by apply own_update, cmra_update_exclusive.
  Qed.

  Global Instance co_alloc_none : CoAllocate (None) (Some $ Excl' ()).
  Proof.
    split => //=.
    case => [[[]|]|] => // _ _ n.
    exists (Excl' ()) => //.
  Qed.

  Global Instance push_spec γs (s v : val) :
    SPEC ⟨↑ N⟩ l, << is_stack γs s ¦ stack_content γs l >>
      push s v
    << RET #(); stack_content γs (v :: l) >>.
  Proof.
    iSteps --safest.
    1,3,5,7,9: iSteps. (* the ▷ (True -∗ x6 #()) pattern is troublesome *)
    all: iLeft; unseal_diaframe => /=; iFrame; iSteps.
  Qed.

  Global Instance is_list_decompose xs (l : loc) :
    MergableConsume (list_inv xs (SOMEV #l)) true (λ p Pin Pout,
      TCAnd (TCEq p false) $
      TCAnd (TCEq Pin empty_hyp_first) $
             TCEq Pout (∃ x xs' v, ⌜xs = x :: xs'⌝ ∗ l ↦□ (x, v) ∗ list_inv xs' v)%I).
  Proof.
    rewrite /MergableConsume /= => p Pin Pout [-> [-> ->]] /=.
    destruct xs; iSteps.
  Qed.

  Global Instance pop_spec γs (s : val) :
    SPEC ⟨↑ N⟩ l, << is_stack γs s ¦ stack_content γs l >>
      pop s
    << (v : val), RET v; 
        ⌜l = []⌝ ∗ stack_content γs [] ∗ ⌜v = NONEV⌝ ∨ 
        ∃ w l', ⌜l = w :: l'⌝ ∗ stack_content γs l' ∗ ⌜v = SOMEV w⌝ >>.
  Proof.
    iStep 16 as (_ _ _ head offer Φ γs' Hlöb) "HIoff HIhd IH HAU".
    iStep 9 as_anon / as (vl vs) "Hvs Hcl Hγs Hhd"; [iSteps| ].
    (* run until the load of the head.  if stack is empty, linearize now *)
    destruct vs; iDecompose "Hvs".
    - iMod "HAU" as "Hl". iDecompose "Hl" as (_) "Hγs HAUcl".
      iPoseProof (diaframe_close_right_quant with "HAUcl") as "HAUcl".
      iSteps.
    - (* run until the CAS is run to pop from the head. Succesful case is easy *)
      iStep 39; [ iSteps.. | ].
      (* load the offer location and check if an offer is available *)
      iStep 24; first iSteps. (* no offer, then recurse is easy *)
      iIntros "!>". wp_pure credit: "H£". (* get one credit to strip a later from the possibly contained AU *)
      (* try to accept the offer *)
      iSteps --until goal-matches (|={⊤ ∖ ∅ ∖ _,⊤ ∖ ∅}=> emp -∗ _)%I /
          as_anon / as (_) "HOcl HAU2 Hoff" /
          as_anon / as_anon / as_anon / as_anon / as_anon / as_anon.
        (* four cases, each with 2 goals for the AU abort side conditions *)
      (* succeeding CAS is interesting (offer was 0). All other cases are easy (offer was 1, done or accepted, or offer was 2, retracted) *)
      1,3,4,5,6,7,8: iSteps.
      (* to get the post condition: peek at the stack, whatever it is. then quickly push and pull the pending AU's *)
      iMod (inv_acc with "HIhd") as "HN"; first solve_ndisj.
      iMod (lc_fupd_elim_later with "H£ HAU2") as ">Hl". (* strip a later from the obtained AU *) 
      iDecompose "Hl" as (ls) "Hγs HAUcl". iDecompose "HN".
      iPoseProof (diaframe_close_right with "HAUcl") as "HAUcl". (* this enforces logically pushing to the stack *)
      (* and now diaframe figures out automatically that the pop should be done, to finish the proof *)
      iSteps.
  Qed.
End stack.


































