From iris.algebra Require Import auth gset.
From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import own_hints.
From iris.heap_lang Require Import proofmode.

From iris.base_logic Require Import lib.saved_prop.
From iris_examples.barrier Require Export barrier.


Set Default Proof Using "Type".

(** The CMRAs/functors we need. *)
Class barrierG Σ := BarrierG {
  #[local] barrier_inG :: inG Σ (authR (gset_disjUR gname));
  #[local] barrier_savedPropG :: savedPropG Σ;
}.
Definition barrierΣ : gFunctors :=
  #[ GFunctor (authRF (gset_disjUR gname)); savedPropΣ ].

Global Instance subG_barrierΣ {Σ} : subG barrierΣ Σ → barrierG Σ.
Proof. solve_inG. Qed.


Section barrier_biabds.
  Context `{!invGS Σ, !barrierG Σ}.

  Global Instance gset_dealloc_biabd2 (X : gset gname) γ :
    HINT own γ (● GSet X) ✱ [b Y; ⌜b = true⌝ ∗ own γ (◯ GSet Y) ∨ ⌜b = false⌝ ∗ ⌜Y = ∅⌝]
            ⊫ [bupd] gs;
         own γ (● GSet gs) ✱ [if b then ⌜gs = X ∖ Y⌝ else ⌜gs = X⌝].
  Proof.
    iStep 3 as (Y HY) "H● H◯" / as "H●".
    - rewrite -(bi.exist_intro (X ∖ Y)).
      iSteps.
    - iExists X. iSteps.
  Qed.

  Global Instance saved_prop_alloc P :
    HINT ε₁ ✱ [- ; emp] ⊫ [bupd] γ; saved_prop_own γ DfracDiscarded P ✱ [saved_prop_own γ DfracDiscarded P].
  Proof.
    iStep.
    iMod (saved_prop_alloc P%I) as (γsp) "#Hsp"; first done.
    iSteps.
  Qed.

  (* not done by local_updates since this does not satisfy coallocation premises *)
  Global Instance own_auth_alloc X :
    HINT ε₁ ✱ [- ; emp] ⊫ [bupd] γ; own γ (◯ GSet X) ✱ [own γ (● GSet X)].
  Proof.
    iStep.
    iMod (own_alloc (● (GSet X) ⋅ ◯ (GSet X))) as (γ) "[H1 H2]".
    { by apply auth_both_valid_discrete. }
    iSteps.
  Qed.

  Global Instance merged_hyp_saved_prop γ P Q :
    MergablePersist (saved_prop_own γ DfracDiscarded P) (λ p Pin Pout,
      TCAnd (TCEq Pin (saved_prop_own γ DfracDiscarded Q)) (TCEq Pout (▷ (P ≡ Q))%I)).
  Proof.
    rewrite /MergablePersist => p P2 P3 [-> ->] /=. 
    rewrite bi.intuitionistically_if_elim.
    iStep. iIntros "!>". iApply saved_prop_agree; iSteps.
  Qed.

End barrier_biabds.

Section big_opS_biabds.
  Context {PROP : bi}.

  Global Instance big_sepS_delete_biabd `{EqDecision A, Countable A} (Φ : A → PROP) (X : gset A) (x : A) :
    SolveSepSideCondition (x ∈ X) →
    HINT [∗ set] y ∈ X, Φ y ✱ [- ; emp] ⊫ [id]; [∗ set] y ∈ X ∖ {[x]}, Φ y ✱ [Φ x].
  Proof.
    move => Hx. iStep.
    rewrite big_sepS_delete //. iSteps.
  Qed.

  Global Instance internal_eq_biabd `{BiInternalEq PROP} p (P Q : PROP) :
    TCOr (TCEq p true) (TCAnd (TCEq p false) $ TCOr (Affine (PROP := PROP) (P ≡ Q)%I) (Absorbing P)) →
    HINT □⟨p⟩ P ≡ Q ✱ [- ; P] ⊫ [id]; Q ✱ [emp].
  Proof.
    case => [-> | [_ HPQ]].
    - iStep as "H≡ HP". iRewrite -"H≡". iSteps.
    - case: HPQ => HPQ; iStartProof => /=;
      rewrite bi.intuitionistically_if_elim; iStep as "H≡ HP";
      iRewrite -"H≡"; iSteps.
  Qed.

End big_opS_biabds.

Global Hint Extern 4 (_ ∈ _) => set_solver : solve_pure_add.

(** Now we come to the Iris part of the proof. *)
Section proof.
  Context `{!heapGS Σ, !barrierG Σ} (N : namespace).

  Definition barrier_inv (l : loc) (γ : gname) (P : iProp Σ) : iProp Σ :=
    (∃ (b : bool) (γsps : gset gname),
      l ↦ #b ∗
      own γ (● (GSet γsps)) ∗
      ((⌜b = true⌝ ∗ [∗ set] γsp ∈ γsps, ∃ R, saved_prop_own γsp DfracDiscarded R ∗ ▷ R) ∨
       (⌜b = false⌝ ∗ (P -∗ [∗ set] γsp ∈ γsps, ∃ R, saved_prop_own γsp DfracDiscarded R ∗ ▷ R)))).

  Definition recv (l : loc) (R : iProp Σ) : iProp Σ :=
    (∃ γ P R' γsp,
      ▷ (R' -∗ R) ∗
      saved_prop_own γsp DfracDiscarded R' ∗
      own γ (◯ GSet {[ γsp ]}) ∗
      inv N (barrier_inv l γ P))%I.

  Definition send (l : loc) (P : iProp Σ) : iProp Σ :=
    (∃ γ, inv N (barrier_inv l γ P))%I.

  Global Instance wand_atom p (A B : iProp Σ) : AtomAndConnective p (A -∗ B).
  Proof. done. Qed.

  Ltac connective_as_atom_shortcut ::=
    biabd_disj_step_atom_shortcut.

  (** Actual proofs *)
  Lemma newbarrier_spec (P : iProp Σ) :
    {{{ True }}} newbarrier #() {{{ l, RET #l; recv l P ∗ send l P }}}.
  Proof.
    iSteps as (Φ l) "Hl".
    iExists P.
    erewrite <-bi.entails_wand => //.
    iStep as (γ γ') "HγP".
    iExists P.
    unseal_diaframe => /=.
    iSplitR.
    { iSteps. rewrite difference_empty_L big_opS_singleton.
      iSteps. }
    iSteps.
  Qed.

  Lemma signal_spec l P :
    {{{ send l P ∗ P }}} signal #l {{{ RET #(); True }}}.
  Proof. iSteps; rewrite difference_empty_L; iSteps. Qed.

  Lemma wait_spec l P:
    {{{ recv l P }}} wait #l {{{ RET #(); P }}}.
  Proof.
    iSteps --safest / as_anon /
      as (_ _ _ _ _ l' Φ P' R' γ Q' γ' Hlöb γsp Hγsp) "??? HPΦ ? Hγsp"; 
    first iSteps.
    iRight. iStep. iSplitL "Hγsp"; [ by iFrame | ].
    iStep 4.
    do 4 iExists _. iFrame. iSteps.
  Qed.

  Lemma recv_split E l P1 P2 :
    ↑N ⊆ E → recv l (P1 ∗ P2) ={E}=∗ recv l P1 ∗ recv l P2.
  Proof. 
    (* this goal is really not well-suited to Diaframe's automation: 
        - the internals of recv do not appear in the invariant
        - once opened, the required updates are really quite subtle (and i think set in general is not really nice for automation)
        - closing of the invariant requires introducing ▷ to get the required saved_prop agreement, which means
          we forego being able to update. *)
  Abort.
End proof.
