From diaframe.heap_lang Require Import proof_automation wp_auto_lob.

From iris_examples.concurrent_stacks Require Import specs concurrent_stack1.
From iris.heap_lang Require Import proofmode.
Set Default Proof Using "Type".

(** Stack 1: No helping, bag spec. *)

Section stacks.
  Context `{!heapGS Σ} (N : namespace).
  Implicit Types l : loc.

  Definition is_list_pre (P : val → iProp Σ) (F : val -d> iPropO Σ) :
     val -d> iPropO Σ := (λ v, 
      ⌜v = NONEV⌝ ∨ (
        ∃ (l : loc), ⌜v = SOMEV #l⌝ ∗ ∃ (h : val) (t : val), l ↦□ (h, t)%V ∗ P h ∗ ▷ F t))%I.

  Local Instance is_list_contr (P : val → iProp Σ) : Contractive (is_list_pre P).
  Proof. rewrite /is_list_pre; repeat (intro); repeat (f_contractive || f_equiv). Qed.

  Definition is_list_def (P : val -> iProp Σ) := fixpoint (is_list_pre P).
  Definition is_list_aux P : seal (@is_list_def P). by eexists. Qed.
  Definition is_list P := unseal (is_list_aux P).
  Definition is_list_eq P : @is_list P = @is_list_def P := seal_eq (is_list_aux P).

  Lemma is_list_unfold (P : val → iProp Σ) v :
    is_list P v ⊣⊢ is_list_pre P (is_list P) v.
  Proof.
    rewrite is_list_eq. apply (fixpoint_unfold (is_list_pre P)).
  Qed.

  Lemma is_list_dup (P : val → iProp Σ) v :
    is_list P v ⊢ is_list P v ∗ (⌜v = NONEV⌝ ∨ ∃ l, ⌜v = SOMEV (LitV (LitLoc l))⌝ ∗ ∃ h t, l ↦□ (h, t)).
  Proof. rewrite is_list_unfold. iSmash. Qed.

  Definition stack_inv P l :=
    (∃ vl, l ↦ vl ∗ is_list P vl ∗ ⌜val_is_unboxed vl⌝)%I.

  Definition is_stack (P : val → iProp Σ) v : iProp Σ :=
    ∃ l, ⌜v = #l⌝ ∗ inv N (stack_inv P l)%I.

  Global Instance biabd_islist_none P :
    HINT ε₀ ✱ [- ; emp] ⊫ [id]; is_list P NONEV ✱ [emp].
  Proof. iStep. rewrite is_list_unfold /=. iSteps. Qed.

  Global Instance biabd_islist_some P (l : loc) :
    HINT ε₁ ✱ [h t; l ↦□ (h, t)%V ∗ P h ∗ ▷ is_list P t] ⊫ [id]; 
         is_list P (SOMEV #l) ✱ [emp].
  Proof.
    do 3 iStep.
    rewrite (is_list_unfold _ (InjRV _)) /=.
    iSteps.
  Qed.

  (* we don't want to make is_list into_or, because that will recursively unfold all possible lengths of list *)
  (* this solution costs verification time, since it does some unnecessary case splits. See [is_list_skel] in
     diaframe_heap_lang_examples/comparison/bag_stack.v for another solution *)
  Instance is_list_remember_part P v :
    MergablePersist (is_list P v) (λ p Pin Pout,
        TCAnd (TCEq Pin (ε₀)%I)
              (TCEq Pout (⌜v = NONEV⌝ ∨ ∃ l, ⌜SOMEV (LitV (LitLoc l)) = v⌝ ∗ ⌜val_is_unboxed v⌝ ∗ ∃ h t, l ↦□ (h, t))))%I | 30.
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->] /=. 
    rewrite bi.intuitionistically_if_elim is_list_dup.
    iSteps; iIntros "!>"; iSteps.
  Qed.

  Global Instance biabd_islist_pop P l h v E :
    HINT l ↦□ (h, v)%V ✱ [- ; is_list P (SOMEV #l)] ⊫ [fupd E E];
         ▷ is_list P v ✱ [⌜val_is_unboxed v⌝ ∗ P h] | 30.
  Proof.
    iStep as "Hl HPls".
    rewrite (is_list_unfold _ (InjRV _)) /=. iDecompose "HPls" as (l') "Hl' HPh Hls".
    rewrite is_list_unfold. iDecompose "Hls"; iSteps.
  Qed.

  Context (P : val → iProp Σ).

  Theorem new_stack_spec :
    {{{ True }}} new_stack #() {{{ s, RET s; is_stack P s }}}.
  Proof. iSteps. Qed.

  Theorem push_spec (s v : val) :
    SPEC {{ is_stack P s ∗ P v }} push s v {{ RET #(); True }}.
  Proof. iSteps. Qed.
    (* Regular Texan triples give some trouble here because of the ∀ Φ, ▷(post -∗ Φ) thing *)

  Theorem pop_spec (s : val) :
    SPEC {{ is_stack P s }} pop s {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ v, ⌜ov = SOMEV v⌝ ∗ P v }}.
  Proof. iSteps. Qed.
End stacks.

