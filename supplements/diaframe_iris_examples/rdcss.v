From iris.algebra Require Import excl auth agree frac list cmra csum.
From iris.base_logic.lib Require Export invariants.
From iris.program_logic Require Export atomic.
From iris.proofmode Require Import proofmode.
From iris.heap_lang Require Import proofmode notation.
From iris_examples.logatom.rdcss Require Import spec.
From iris.prelude Require Import options.

From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob loc_map inv_loc wp_later_credits.
From diaframe.lib Require Import own_hints later_credits.


(** We consider here an implementation of the RDCSS (Restricted Double-Compare
    Single-Swap) data structure of Harris et al., as described in "A Practical
    Multi-Word Compare-and-Swap Operation" (DISC 2002).

    Our goal is to prove logical atomicity for the operations of RDCSS, and to
    do so we will need to use prophecy variables! This Coq file is part of the
    artifact accompanying the paper "The Future is Ours: Prophecy Variables in
    Separation Logic" (POPL 2020). Proving logical atomicity for RDCSS appears
    as a major case study in the paper, so it makes sense to read the relevant
    sections first (§4 to §6) to better understand the Coq proof. The paper is
    available online at: <https://plv.mpi-sws.org/prophecies/>. *)

(** * Implementation of the RDCSS operations *)

(** The RDCSS data structure manipulates two kinds of locations:
    - N-locations (a.k.a. RDCSS locations) denoted [l_n] identify a particular
      instance of RDCSS. (Harris et al. refer to them as locations in the data
      section.) They are the locations that may be updated by a call to RDCSS.
    - M-locations denoted [l_m] are not tied to a particular RDCSS instance.
      (Harris et al. refer to them as locations in the control section.)  They
      are never directly modified by the RDCSS operation, but they are subject
      to arbitrary interference by other heap operations.

    An N-location can contain values of two forms.
    - A value of the form [injL n] indicates that there is currently no active
      RDCSS operation on the N-location, and that the location has the logical
      value [n]. In that case, the N-location is in a "quiescent" state.
    - A value of the form [injR descr] indicates that the RDCSS operation that
      is identified by descriptor [descr] is ongoing. In that case, descriptor
      [descr] must point to a tuple [(l_m, m1, n1, n2, p)] for some M-location
      [l_m], integers [m1], [n1], [n2] and prophecy [p]. An N-location holding
      a value of the form [injR descr] is in an "updating" state. *)

(** As mentioned in the paper, there are minor differences between our version
    of RDCSS and the original one (by Harris et al.):
    - In the original version, the RDCSS operation takes as input a descriptor
      for the operation,  whereas in our version the RDCSS operation allocates
      the descriptor itself.
    - In the original version, values (inactive state) and descriptors (active
      state) are distinguished by looking at their least significant bits. Our
      version rather relies on injections to avoid bit-level manipulations. *)

(** The [new_rdcss] operation creates a new RDCSS location.  It corresponds to
    the following pseudo-code:
<<
  new_rdcss(n) := ref (injL n)
>>
*)
Definition new_rdcss : val := λ: "n", ref (InjL "n").

(** The [complete] function is used internally by the RDCSS operations. It can
    be expressed using the following pseudo-code:
<<
  complete(l_descr, l_n) :=
    let (l_m, m1, n1, n2, p) := !l_descr;
    (* data = (l_m, m1, n1, n2, p) *)
    let tid_ghost = NewProph;
    let n_new = (if !l_m = m1 then n1 else n2);
    Resolve (CmpXchg l_n (InjR l_descr) (ref (InjL n_new))) p tid_ghost;
    #().
>>

    In this function, we rely on a prophecy variable to emulate a ghost thread
    identifier. In particular, the corresponding prophecy variable [tid_ghost]
    is never resolved.  Here, the main reason for using a prophecy variable is
    that we can use erasure to argue that it has no effect on the code. *)
Definition complete : val :=
  λ: "l_descr" "l_n",
    let: "data" := !"l_descr" in (* data = (l_m, m1, n1, n2, p) *)
    let: "l_m" := Fst (Fst (Fst (Fst ("data")))) in
    let: "m1"  := Snd (Fst (Fst (Fst ("data")))) in
    let: "n1"  := Snd (Fst (Fst ("data"))) in
    let: "n2"  := Snd (Fst ("data")) in
    let: "p"   := Snd ("data") in
    (* Create a thread identifier using NewProph. *)
    let: "tid_ghost" := NewProph in
    let: "n_new" := (if: !"l_m" = "m1" then "n2" else "n1") in
    Resolve (CmpXchg "l_n" (InjR "l_descr") (InjL "n_new")) "p" "tid_ghost";;
    #().

(** The [get] operation reads the value stored in an RDCSS location previously
    created using [new_rdcss]. In pseudo-code, it corresponds to:
<<
  rec get(l_n) :=
    match !l_n with
    | injL n       => n
    | injR l_descr => complete(l_descr, l_n);
                      get(l_n)
    end
>>
*)
Definition get : val :=
  rec: "get" "l_n" :=
    match: !"l_n" with
      InjL "n"    => "n"
    | InjR "l_descr" =>
        complete "l_descr" "l_n" ;;
        "get" "l_n"
    end.

(** Finally, the [rdcss] operation corresponds to the following pseudo-code:
<<
  rdcss(l_m, l_n, m1, n1, n2) :=
    let p := NewProph;
    let l_descr := ref (l_m, m1, n1, n2, p);
    rec rdcss_inner() =
      let (r, b) := CmpXchg(l_n, InjL n1, InjR l_descr) in
      match r with
      | InjL n             =>
          if b then
            complete(l_descr, l_n); n1
          else
            n
      | InjR l_descr_other =>
           complete(l_descr_other, l_n);
           rdcss_inner()
      end;
    rdcss_inner()
>>
*)
Definition rdcss: val :=
  λ: "l_m" "l_n" "m1" "n1" "n2",
    (* Allocate the descriptor for the operation. *)
    let: "p" := NewProph in
    let: "l_descr" := ref ("l_m", "m1", "n1", "n2", "p") in
    (* Attempt to establish the descriptor to make the operation "active". *)
    ( rec: "rdcss_inner" "_" :=
        let: "r" := CmpXchg "l_n" (InjL "n1") (InjR "l_descr") in
        match: Fst "r" with
          InjL "n" =>
            (* non-descriptor value read, check if CmpXchg was successful *)
            if: Snd "r" then
              (* CmpXchg was successful, finish operation *)
              complete "l_descr" "l_n" ;; "n1"
            else
              (* CmpXchg failed, hence we could linearize at the CmpXchg *)
              "n"
        | InjR "l_descr_other" =>
            (* A descriptor from a concurrent operation was read, try to help
               and then restart. *)
            complete "l_descr_other" "l_n";;
            "rdcss_inner" #()
        end
    ) #().

(** ** Proof setup *)

Definition valUR      := authR $ optionUR $ exclR valO.
Definition tokenUR    := optionR $ exclR unitO.
Definition one_shotUR := csumR (exclR unitO) (agreeR unitO).

Class rdcssG Σ := RDCSSG {
                     #[local] rdcss_valG      :: inG Σ valUR;
                     #[local] rdcss_tokenG    :: inG Σ tokenUR;
                     #[local] rdcss_one_shotG :: inG Σ one_shotUR;
                     #[local] rdcss_loc_mapG1  :: loc_mapG Σ gname;
                     #[local] rdcss_loc_mapG2  :: loc_mapG Σ (proph_id * gname * gname * gname);
                   }.

Definition rdcssΣ : gFunctors :=
  #[GFunctor valUR; GFunctor tokenUR; GFunctor one_shotUR; loc_mapΣ gname; loc_mapΣ (proph_id * gname * gname * gname)].

Global Instance subG_rdcssΣ {Σ} : subG rdcssΣ Σ → rdcssG Σ.
Proof. solve_inG. Qed.

Section rdcss.
  Context {Σ} `{!heapGS Σ, !rdcssG Σ}.
  Context (N : namespace).

  Implicit Types γ_n γ_a γ_t γ_s γ_m : gname.
  Implicit Types l_n l_m l_descr : loc.
  Implicit Types p : proph_id.

  Local Definition descrN := N .@ "descr".
  Local Definition rdcssN := N .@ "rdcss".

  (** Logical value for the N-location. *)

  Definition rdcss_state_auth γm (l_n : loc) (n : val) :=
    (∃ (γ_n : gname), own γ_n (● Excl' n) ∗ loc_map_elem γm l_n DfracDiscarded γ_n)%I.

  Definition rdcss_state γm (l_n : loc) (n : val) :=
    (∃ (γ_n : gname), loc_map_elem γm l_n DfracDiscarded γ_n ∗ own γ_n (◯ Excl' n))%I.

  (** Definition of the invariant *)

  (** Extract the [tid] of the winner (i.e., the first thread that preforms a
      CAS) from the prophecy. *)
  Definition proph_extract_winner (pvs : list (val * val)) : proph_id :=
    match pvs with
    | (_, LitV (LitProphecy tid)) :: _  => tid
    | _                                 => xH
    end.

  Definition own_token γ := (own γ (Excl' ()))%I.

  Definition pending_state γm P (n1 : val) l_n γ_a :=
    (P ∗ rdcss_state_auth γm l_n n1 ∗ own_token γ_a)%I.

  (* After the prophecy said we are going to win the race, we commit and run the AU,
     switching from [pending] to [accepted]. *)
  Definition accepted_state Q (proph_winner : proph_id) :=
    ((∃ vs, proph proph_winner vs) ∗ Q)%I.

  (* The same thread then wins the CmpXchg and moves from [accepted] to [done].
     Then, the [γ_t] token guards the transition to take out [Q].
     Remember that the thread winning the CmpXchg might be just helping.  The token
     is owned by the thread whose request this is.
     In this state, [tid_ghost_winner] serves as a token to make sure that
     only the CmpXchg winner can transition to here, and owning half of [l_descr] serves as a
     "location" token to ensure there is no ABA going on. Notice how [rdcss_inv]
     owns *more than* half of its [l_descr] in the Updating state,
     which means we know that the [l_descr] there and here cannot be the same. *)
  Definition done_state Qn l_descr (tid_ghost_winner : proph_id) γ_t γ_a :=
    ((Qn ∨ own_token γ_t) ∗ (∃ vs, proph tid_ghost_winner vs) ∗
     (∃ v, l_descr ↦{# 1/2} v) ∗ own_token γ_a)%I.

  (* Invariant expressing the descriptor protocol.
     - We always need the [proph] in here so that failing threads coming late can
       always resolve their stuff.
     - We need a way for anyone who has observed the [done] state to
       prove that we will always remain [done]; that's what the one-shot token [γ_s] is for.
     - [γ_a] is a token which is owned by the invariant in [pending] and [done] but not in [accepted].
       This permits the CmpXchg winner to gain ownership of the token when moving to [accepted] and
       hence ensures that no other thread can move from [accepted] to [done].
       Side remark: One could get rid of this token if one supported fractional ownership of
                    prophecy resources by only keeping half permission to the prophecy resource
                    in the invariant in [accepted] while the other half would be kept by the CmpXchg winner.
   *)
  Definition descr_inv γm P Q p n l_n l_descr (tid_ghost_winner : proph_id) γ_t γ_s γ_a: iProp Σ :=
    (∃ vs, proph p vs ∗
      ((l_n ↦{# 1/2} InjRV #l_descr ∗ own γ_s (Cinl $ Excl ()) ∗
          ⌜tid_ghost_winner = proph_extract_winner vs⌝ ∗ ( pending_state γm P n l_n γ_a
        ∨ accepted_state (Q n) tid_ghost_winner ) ∗ own γ_t None)
       ∨ own γ_s (Cinr $ to_agree ()) ∗ done_state (Q n) l_descr tid_ghost_winner γ_t γ_a))%I.

  Definition rdcss_au γm Q l_n l_m m1 n1 n2 :=
    (AU <{ ∃∃ (m n : val), (l_m ↦_(λ _, True) m) ∗ rdcss_state γm l_n n }>
         @ ⊤∖(↑N ∪ ↑inv_heapN), ∅
        <{ (l_m ↦_(λ _, True) m) ∗ (rdcss_state γm l_n (if (decide ((m = m1) ∧ (n = n1))) then n2 else n)),
           COMM Q n }>)%I.

  Definition rdcss_inv γm γmd l_n : iProp Σ :=
    (∃ (v : val),
       l_n ↦{# 1/2} v ∗
        ((∃ n, ⌜v = InjLV n⌝ ∗ l_n ↦{# 1/2} (InjLV n) ∗ rdcss_state_auth γm l_n n
              ) ∨ 
         (∃ (l_descr l_m : loc) (m1 n1 n2 : val) (p : proph_id), ⌜v = InjRV #l_descr⌝ ∗
            l_descr ↦{# 1/2} (#l_m, m1, n1, n2, #p)%V ∗
            l_descr ↦□ (#l_m, m1, n1, n2, #p)%V ∗ ⌜val_is_unboxed m1⌝ ∗
            l_m ↦_(λ _, True) □ ∗ 
            ∃ Q tid_ghost_winner γ_t γ_s γ_a,
            inv descrN (descr_inv γm (rdcss_au γm Q l_n l_m m1 n1 n2) Q p n1 l_n l_descr tid_ghost_winner γ_t γ_s γ_a) ∗
            loc_map_elem γmd l_descr DfracDiscarded (tid_ghost_winner, γ_t, γ_s, γ_a)))).

  Definition is_rdcss γm γmd (l_n : loc) :=
    (inv rdcssN (rdcss_inv γm γmd l_n) ∧ inv_heap_inv ∧ ⌜N ## inv_heapN⌝ ∧ global_reg γmd ∧ ⌜N ## N_map⌝)%I.

  Global Instance is_rdcss_persistent γm γmd l_n : Persistent (is_rdcss γm γmd l_n) := _.
  (* no longer holds since loc_map_elems are not Timeless
  Global Instance rdcss_state_timeless γm l_n n : Timeless (rdcss_state γm l_n n) := _. *)

  (** A few more helper lemmas that will come up later *)

  (** Once a [descr] protocol is [done] (as reflected by the [γ_s] token here),
      we can at any later point in time extract the [Q].
  Lemma state_done_extract_Q P Q p n l_n l_descr tid_ghost γ_t γ_s γ_a :
    inv descrN (descr_inv P Q p n l_n l_descr tid_ghost γ_t γ_s γ_a) -∗
    own γ_s (Cinr (to_agree ())) -∗
    □(own_token γ_t ={⊤}=∗ ▷ (Q n)).
  Proof.
    iSteps.
    iInv descrN as (vs) "HN". iReIntro "HN". iSmash.
  Qed. *)

  Instance cinl_update γ :
    HINT own γ (Cinl (Excl ())) ✱ [- ; emp] ⊫ [bupd]; own γ (Cinr (to_agree ())) ✱ [own γ (Cinr (to_agree ()))].
  Proof.
    iStep as "HE".
    iMod (own_update with "HE") as "Hs".
    { apply (cmra_update_exclusive (Cinr (to_agree ()))). done. } iStopProof.
    iSteps.
  Qed.

  Global Instance cmpxchg_step_wp_stronger2 (l : loc) (v1 v2 : val) E1 E2 s :
    SPEC ⟨E1, E2⟩ (v : val) (dq dq' : dfrac) (mv : option val), 
      {{ ▷ l ↦{dq} v ∗ ⌜vals_compare_safe v v1⌝ ∗ 
          match dq with
          | DfracDiscarded => ⌜v ≠ v1⌝ ∗ ⌜dq = dq'⌝ ∗ ⌜mv = None⌝
          | DfracBoth _ => ⌜v ≠ v1⌝ ∗ ⌜dq = dq'⌝ ∗ ⌜mv = None⌝
          | DfracOwn q => ∃ oq, ⌜FracSub 1 q oq⌝ ∗
            match oq with
            | None => ⌜dq = dq'⌝ ∗ ⌜mv = None⌝
            | Some q' => ⌜v ≠ v1⌝ ∗ ⌜dq = dq'⌝ ∗ ⌜mv = None⌝ ∨ ⌜dq' = DfracOwn 1%Qp⌝ ∗ ∃ v', ▷ l ↦{#q'} v' ∗ ⌜mv = Some v'⌝
            end
          end
       }} 
        CmpXchg #l v1 v2 @ s 
      {{ (b : bool), RET (v, #b)%V; match mv with | None => True | Some v' => ⌜v = v'⌝ end ∗
          (⌜b = true⌝ ∗ ⌜v = v1⌝ ∗ l ↦{dq'} v2   ∨
          ⌜b = false⌝ ∗ ⌜v ≠ v1⌝ ∗ l ↦{dq'} v) }} | 0.
  Proof.
    iStep 4 as (v dq dq' mv).
    destruct dq.
    - iStep as (Hv mq Hmq) "Hl Hrem". destruct mq; iDecompose "Hrem".
      * iSteps.
      * rewrite Hmq. iSteps.
      * rewrite Hmq. iSteps.
    - iSteps.
    - iSteps.
  Qed.

  Hint Extern 4 (FracSub _ _ _) => tc_solve : solve_pure_add.

  (** ** Proof of [complete] *)

  Instance simplify_dneg_dec (φ : Prop) : Decision φ → SimplifyPureHypSafe (¬¬ φ) φ.
  Proof.
    rewrite /SimplifyPureHypSafe => //. 
    case => // Hφ. split => //.
    apply pure_solver.dneg_involutive.
  Qed.

  Instance complete_spec_both γm γmd l_n l_descr n (p tid_ghost : proph_id) : 
    SPEC P Q n1 tid_ghost_inv γ_t γ_s γ_a, {{ inv rdcssN (rdcss_inv γm γmd l_n) ∗
      inv descrN (descr_inv γm P Q p n1 l_n l_descr tid_ghost_inv γ_t γ_s γ_a) ∗
      (⌜tid_ghost_inv ≠ tid_ghost⌝ ∨ own_token γ_a ∗ rdcss_state_auth γm l_n n)
    }} Resolve (CmpXchg #l_n (InjRV #l_descr) (InjLV n)) #p #tid_ghost 
    {{ (v : val), RET v; own γ_s (Cinr (to_agree ())) }}.
  Proof.
    iSteps. (* boom *)
  Qed.

  (** ** Proof of [complete] *)
  (* The postcondition basically says that *if* you were the thread to own
     this request, then you get [Q].  But we also try to complete other
     thread's requests, which is why we cannot ask for the token
     as a precondition. *)
  Instance complete_spec (* l_n l_m l_descr (m1 n1 n2 : val) p γ_t γ_s γ_a tid_ghost_inv Q q *) l_n l_descr :
    SPEC dq l_m m1 n1 n2 p γm γmd Q tid_ghost_inv γ_t γ_s γ_a, {{ 
        inv rdcssN (rdcss_inv γm γmd l_n) ∗ global_reg γmd ∗
        l_descr ↦{dq} (#l_m, m1, n1, n2, #p) ∗ ⌜val_is_unboxed m1⌝ ∗
        l_m ↦_(λ _, True) □ ∗
        inv_heap_inv ∗ ⌜N ## inv_heapN⌝ ∗
        loc_map_elem γmd l_descr DfracDiscarded (tid_ghost_inv, γ_t, γ_s, γ_a) ∗
        inv descrN (descr_inv γm (rdcss_au γm Q l_n l_m m1 n1 n2) Q p n1 l_n l_descr tid_ghost_inv γ_t γ_s γ_a)
     }}
      complete #l_descr #l_n
    {{ RET #(); own γ_s (Cinr (to_agree ())) }}.
  Proof.
    iStep 116 as  (* proof is cleanest if we do case distinction here. *)
          (dq l_m m1 n1 n2 p γm γmd Q tid_ghost_inv γ_t γ_s γ_a Hl_m HN_disj pvs p')
          "HrdcssI Hγm Hl_mI H□h Hγm_el HdescrI Hl_descr Hp H£".
    (* for automatic case distinction, we need to look under disjunctions ánd have automatic later-credit elimination
        ánd the awkward open inv_heap_inv business needs to be able to be performed lazily (after opening AU) *)
    destruct (decide (tid_ghost_inv = p')) as [-> | Hnl]; last iSteps.
    iStep. iIntros "!>". (* we dont want to use l ↦_I □, instead use the AU! *) 
    iMod (inv_acc with "HdescrI") as "[HN2 Hcl2]"; first done.
    iDecompose "HN2" as (pvs' Hp_not_winner _ γ') "HdescrI Hγm_el1 Hγm_el2 Hp Hl_n Hγ_s Hdescr_cl Hwinner HAU H● Hγ_a".
    iMod (inv_acc with "HrdcssI") as "[HN Hcl]";first solve_ndisj. 
    iDecompose "HN" as (l _ _ _ _ Hm1 Q') "Hγm_el1 HdescrI Hl HC1 HC2 HC3 Hl_n Hdescr_cl Hl1 Hl2". iClear "HC1 HC2 HC3". clear Q'.
    iMod (lc_fupd_elim_later with "[H£] HAU") as "AU". { iSteps. } (* cant do strong access once the AU is open, since that makes the mask ∅ *)
    iMod (inv_pointsto_own_acc_strong with "H□h") as "Hl0I"; first solve_ndisj. 
    iStep 2 as (v) "Hγm_el3 H◯ HAUcl Hl0I Hl_m".
    iDecompose "HAUcl" as "HAUcl". (* close AU first. closing descr first would be ideal, but we cannot find hints for AU post (x5) currently *)
    iStep. (* ◯ is leading, not ●, so do this manually *)
    iAssert (|==> own γ' (● Excl' (if decide (v = m1 ∧ n1 = n1) then n2 else n1)) ∗ 
                  own γ' (◯ Excl' (if decide (v = m1 ∧ n1 = n1) then n2 else n1)))%I with "[H● H◯]" as ">[H● H◯]"; first iSteps.
    iStep 17.
    - rewrite decide_True //. iSteps.
    - rewrite decide_False; last (case; eauto). iSteps.
  Qed.

  Instance strip_useless_forall (A : Type) {PROP : bi} (P : PROP) :
    HINT ε₁ ✱ [-; P] ⊫ [id]; (∀ _ : A, P) ✱ [True] | 99.
  Proof.
    iStep. iFrame. iSplitL; iSteps.
  Qed.

  (** ** Proof of [rdcss] *)
  Lemma rdcss_spec γm γmd (l_n l_m : loc) (m1 n1 n2 : val) :
    val_is_unboxed m1 →
    val_is_unboxed (InjLV n1) →
    is_rdcss γm γmd l_n -∗
    <<{ ∀∀ (m n: val), l_m ↦_(λ _, True) m ∗ rdcss_state γm l_n n }>>
        rdcss #l_m #l_n m1 n1 n2 @ ↑N ∪ ↑inv_heapN
    <<{ l_m ↦_(λ _, True) m ∗ rdcss_state γm l_n (if decide (m = m1 ∧ n = n1) then n2 else n) | RET n }>>.
  Proof. (* TODO: fix naming in this proof *)
    move => Hm1_unbox Hn1_unbox.
    iSteps --safest /
        as (HN1 HN2 _ _ _ _ n l_n' l_m' Φ ??? p pvs ? γm' ?????) "???????? HAU ????" /
        as_anon;
      last iSteps.
    solveStep. iSteps --safest.
    - solveStep. solveStep. solveStep. solveStep.
      iMod "HAU"; iDecompose "HAU" as (m') "??? HAUcl".
      iPoseProof (diaframe_close_right with "HAUcl") as "HAUcl".
      iSteps. rewrite decide_False; last (case; eauto). iSteps.
    - solveStep. iSteps --safest. solveStep. iStep --safest / as (m'') "??? H◯ HAUcl H↦I". solveStep. iStep --safest.
      iDestruct ("HAUcl" $! true with "[H◯ H↦I]") as "(_ & _ & HAU)"; first iSteps.
      iSteps. (* TODO: add abduction support for later credits *)
      iAssert (|={⊤}=> Φ n ∗ emp)%I with "[-]" as ">[$ _]"; iSteps.
      (* TODO: 'failed' application of a later credit hint can cause one to receive gathered_later hyps. fix that, unfold them on intro *)
  Qed.

  (** ** Proof of [new_rdcss] *)
  Instance new_rdcss_spec (n : val) :
    SPEC {{ inv_heap_inv ∗ ⌜N ## inv_heapN⌝ ∗ ⌜N ## N_map⌝ }}
      new_rdcss n
    {{ l_n, RET #l_n ; ∀ γm γmd, global_reg (V := gname) γm ∗ global_reg γmd ={⊤}=∗ is_rdcss γm γmd l_n ∗ rdcss_state γm l_n n }}.
  Proof.
    iStep 18. iExists _. by iFrame "∗#".
    (* conflicts with a [loc_map_elem] inside [rdcss_inv] *)
  Qed.

  (** ** Proof of [get] *)
  Lemma get_spec γm γmd l_n :
    is_rdcss γm γmd l_n -∗
    <<{ ∀∀ (n : val), rdcss_state γm l_n n }>>
        get #l_n @↑N
    <<{ rdcss_state γm l_n n | RET n }>>.
  Proof.
    iStep 15 as (HN_disj1 HN_disj2 _ l_n' Φ γm' γmd' Hlöb) "H□h HrdcssI Hγmd IH HAU".
    iStep 2; last iSteps.
    iMod "HAU"; iDecompose "HAU" as "Hγm_el H◯ HAUcl". iPoseProof (diaframe_close_right with "HAUcl") as "HAUcl".
    iSteps.
  Qed.
End rdcss.
