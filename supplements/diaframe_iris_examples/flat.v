(* Flat Combiner *)
From diaframe.heap_lang Require Import proof_automation.

From iris_examples.concurrent_stacks Require Import specs concurrent_stack1.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import auth frac agree excl agree gset gmap.
From iris.base_logic Require Import saved_prop.
From iris.heap_lang.lib Require Import spin_lock.

From iris_examples.logatom.flat_combiner Require Import misc peritem sync flat.

From diaframe.lib Require Import own_hints.

Local Existing Instance spin_lock.

Definition reqR := prodR fracR (agreeR valO). (* request x should be kept same *)
Definition toks : Type := gname * gname * gname * gname * gname. (* a bunch of tokens to do state transition *)
Class flatG Σ := FlatG {
  #[local] req_G :: inG Σ reqR;
  #[local] excl_G :: inG Σ (exclR unitO);
  #[local] sp_G :: savedPredG Σ val
}.

Definition flatΣ : gFunctors :=
  #[ GFunctor (constRF reqR);
     GFunctor (exclR unitO);
     savedPredΣ val ].

Global Instance subG_flatΣ {Σ} : subG flatΣ Σ → flatG Σ.
Proof. solve_inG. Qed.

Section additional_solve_sep.
  Context {PROP : bi}.

  Global Instance abduct_pairtype_exists {A B} {TTf : A * B → tele} (P : TeleS TTf -t> PROP) :
    BiAbd (TTl := TeleS (λ a, TeleS (λ b, TTf (a, b)))) (TTr := TeleS TTf) false (ε₁)%I P id (λ a b, P (a, b)) 
      (λ a b, tele_bind (λ ttl, tele_bind (λ ttpr : TeleS TTf, ⌜tele_app ((tele_app (tele_eq (TeleS TTf)) ttpr) (a, b)) ttl⌝)%I)) | 80.
  Proof.
    rewrite /BiAbd /= => a b.
    apply tforall_forall => ttl.
    rewrite empty_hyp_last_eq left_id -(bi.exist_intro (a, b)).
    match goal with
    | |- _ ⊢ tele_fold _ _ (tele_bind ?Ψ) =>
      fold (bi_texist Ψ)
    end.
    rewrite bi_texist_exist -(bi.exist_intro ttl).
    rewrite !tele_app_bind /=.
    rewrite tele_map_app tele_app_bind.
    iIntros "$"; iPureIntro.
    exists eq_refl => /=.
    apply tele_eq_app_both.
  Qed.

End additional_solve_sep.

Section proof.
  Context `{!heapGS Σ, !lockG Σ, !flatG Σ} (N: namespace).

  Definition init_s (ts: toks) :=
    let '(_, γ1, γ3, _, _) := ts in (own γ1 (Excl ()) ∗ own γ3 (Excl ()))%I.

  Definition installed_s R (ts: toks) (f x: val) :=
    let '(γx, γ1, _, γ4, γq) := ts in
    (∃ (P: val → iProp Σ) Q,
       own γx ((1/2)%Qp, to_agree x) ∗ ({{{ R ∗ P x }}} f x {{{ v, RET v; R ∗ Q x v }}}) ∗ P x ∗
       saved_pred_own γq DfracDiscarded (Q x) ∗ own γ1 (Excl ()) ∗ own γ4 (Excl ()))%I.

  Definition received_s (ts: toks) (x: val) γr :=
    let '(γx, _, _, γ4, _) := ts in
    (own γx ((1/2/2)%Qp, to_agree x) ∗ own γr (Excl ()) ∗ own γ4 (Excl ()))%I.

  Definition finished_s (ts: toks) (x y: val) :=
    let '(γx, γ1, _, γ4, γq) := ts in
    (∃ Q: val → val → iProp Σ,
       own γx ((1/2)%Qp, to_agree x) ∗ saved_pred_own γq DfracDiscarded (Q x) ∗
       Q x y ∗ own γ1 (Excl ()) ∗ own γ4 (Excl ()))%I.
  
  Definition p_inv R (γm γr: gname) (ts: toks) (p : loc) : iProp Σ :=
      ∃ v, p ↦ v ∗
      ((∃ y: val, ⌜v = InjRV y⌝ ∗ (init_s ts ∨
                                  ∃ x, finished_s ts x y)) ∨
      (∃ f x: val, ⌜v = InjLV (f, x)⌝ ∗ (installed_s R ts f x ∨
                                         received_s ts x γr))).

  (* init and finished have the same physical state: p ↦ InjRV y *)
    (* init_s := own ts.2 Excl () ∗ own ts.3 Excl () *)
    (* finished_s := Payload ∗ own ts.2 Excl() ∗ own ts.4 Excl () *)
  (* installed and received have the same physical state: p ↦ InjLV (f, x) *)
    (* own γr Excl () -∗ installed_s case *)
    (* own ts.2 Excl () -∗ received_s case *)
    (* installed_s := Payload ∗ own ts.2 Excl () ∗ own ts.4 Excl () *)
    (* received_s := Payload? ∗ own ts.4 Excl () * own γr Excl () *)

  Definition p_inv' R γm γr : val → iProp Σ :=
    (λ v: val, ∃ ts (p: loc), ⌜v = #p⌝ ∗ inv N (p_inv R γm γr ts p))%I.

  Definition srv_bag R γm γr s := (∃ xs, is_bag_R N (p_inv' R γm γr) xs s)%I.

  Definition installed_recp (ts: toks) (x: val) (Q: val → iProp Σ) :=
    let '(γx, _, γ3, _, γq) := ts in
    (own γ3 (Excl ()) ∗ own γx ((1/2)%Qp, to_agree x) ∗ saved_pred_own γq DfracDiscarded Q)%I.

  Global Instance push_spec_step_wp (l : loc) (x : val) R :
    SPEC {{ bag_inv N R l ∗ R x }}
      push #l x
    {{ RET #(); inv N (R x) }}.
  Proof.
    iSteps as "HI HR".
    wp_apply (push_spec _ R with "[HR]"); iSteps.
  Qed.

  Global Instance abduct_saved_pred_alloc Q :
    HINT ε₁ ✱ [- ; emp] ⊫ [bupd] γ; saved_pred_own γ DfracDiscarded Q ✱ [saved_pred_own γ DfracDiscarded Q].
  Proof.
    iStep.
    iMod (saved_pred_alloc Q) as (γsp) "#HQ"; first done.
    iSteps.
  Qed.

  Global Instance pers_to_intuit (P : iProp Σ) :
    HINT (<pers> P) ✱ [- ; emp] ⊫ [id]; □ P ✱ [□ P].
  Proof. iSteps. Qed.

  Instance texan_into_wand p P (e : expr) T Q E s (φv : T → val) :
    AtomIntoWand p ({{{ P }}} e @ s; E {{{ (a : T), RET φv a; Q a }}})
      P (WP e @ s ; E {{ v, ∃ a, ⌜v = φv a⌝ ∗ Q a }})%I.
    (* this instance removes the Φ redirection, at the cost of 1 later *)
  Proof.
    rewrite /AtomIntoWand /IntoWand2 bi.intuitionistically_if_elim.
    iStep 4 as "HΦ". iExists (λ v, ∃ t, ⌜v = φv t⌝ ∗ Q t)%I. iSplitR; iSteps.
  Qed.

  Global Instance HT_into_persistent p P (e : expr) T Q E s (φv : T → val) :
    IntoPersistent p ({{{ P }}} e @ s; E {{{ (a : T), RET φv a; Q a }}}) ({{{ P }}} e @ s; E {{{ (a : T), RET φv a; Q a }}}) | 0.
  Proof. destruct p; tc_solve. Qed.

  Lemma to_agree_valid {O : ofe} (o : O) : ✓ to_agree o.
  Proof.
    rewrite /valid /cmra_valid /= /agree_valid_instance /= => n.
    rewrite /validN /agree_validN_instance //=.
  Qed.

  Hint Immediate to_agree_valid : solve_pure_add.

  Global Instance install_spec3 R P Q (f x: val) (γm γr: gname) (s: loc):
    SPEC {{ inv N (srv_bag R γm γr s) ∗ ({{{ R ∗ P }}} f x {{{ v, RET v; R ∗ Q v }}}) ∗ P }}
      install f x #s
    {{ (p : loc) ts, RET #p; inv N (p_inv R γm γr ts p) ∗ installed_recp ts x Q }}.
  Proof.
    iSteps. (* automation now nicely stops at this point :) *)
    iMod (own_alloc (Excl ())) as (γ) "?"; first done. iExists γ.
    iSteps.
  Qed.

  Global Instance doOp_f_spec R γm γr (p: loc) ts :
    SPEC {{ inv N (p_inv R γm γr ts p) ∗ own γr (Excl ()) ∗ R }} 
      doOp #p 
    {{ RET #(); own γr (Excl ()) ∗ R }}.
  Proof. destruct ts as [[[[γ1 γ2] γ3] γ4]γ5] => /=; iSmash. Qed.

  Global Instance loop_iter_doOp_spec R (γm γr: gname) xs (hd : loc) :
    SPEC {{ is_list_R N (p_inv' R γm γr) hd xs ∗ own γr (Excl ()) ∗ R }} 
      treiber.iter #hd doOp
    {{ RET #(); own γr (Excl ()) ∗ R }}.
  Proof.
    iStep as "HL HE HR".
    iInduction xs as [|x xs' IHxs] "IH" forall (hd).
    - wp_lam. iSteps.
    - wp_lam. iDecompose "HL" as (tl) "Hhd HI HL".
      (* we need to extract an invariant out of an invariant here.. *)
      iApply fupd_wp. iInv N as "H" "Hclose". iDecompose "H" as (l' ts) "HI Hhd Hl' Hcl".
      iMod ("Hcl" with "[]") as "_"; first iSteps. 
      iSteps.
  Qed.

  Opaque spin_lock.is_lock spin_lock.try_acquire newlock.

  Global Instance try_srv_spec R (s: loc) (lk: val) (γr γm γlk: gname) :
    SPEC {{ inv N (srv_bag R γm γr s) ∗ is_lock γlk lk (own γr (Excl ()) ∗ R) }}
      try_srv lk #s
    {{ RET #(); True }}.
  Proof.
    iSteps. iIntros "!>".
    wp_apply spin_lock.try_acquire_spec; first iSteps.
    iStep as ([|]); iSteps.
  Qed.

  Definition own_γ3 (ts: toks) := let '(_, _, γ3, _, _) := ts in own γ3 (Excl ()).
  Definition finished_recp (ts: toks) (x y: val) :=
    let '(γx, _, _, _, γq) := ts in
    (∃ Q, own γx ((1 / 2)%Qp, to_agree x) ∗ saved_pred_own γq DfracDiscarded (Q x) ∗ Q x y)%I.

  Global Instance loop_spec R (p s: loc) (lk: val) (γr γm γlk: gname) (ts: toks):
    SPEC {{ inv N (srv_bag R γm γr s) ∗ inv N (p_inv R γm γr ts p) ∗ is_lock γlk lk (own γr (Excl ()) ∗ R) ∗ own_γ3 ts }}
      loop #p #s lk
    {{ x (y : val), RET y; finished_recp ts x y }}.
  Proof.
    iSteps. iLöb as "IH".
    wp_lam.
    destruct ts as [[[[γx γ1] γ3] γ4] γq].
    iSteps.
  Qed.

  Global Instance saved_pred_own_mergable γ P1 P2 :
    MergablePersist (saved_pred_own γ DfracDiscarded P1) (λ p Pin Pout,
      TCAnd (TCEq Pin (saved_pred_own γ DfracDiscarded P2)) $
             TCEq Pout (▷ (∀ a, P1 a ≡ P2 a))%I).
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->] /=.
    rewrite bi.intuitionistically_if_elim. iStep as "H1 H2".
    iIntros "!>" (a).
    by iDestruct (saved_pred_agree with "H1 H2") as "#Heq".
  Qed.

  Opaque new_stack install.

  Lemma mk_flat_spec (γm: gname): mk_syncer_spec mk_flat.
  Proof.
    iSteps as (R Φ) "HR HΦ".
    iMod (own_alloc (Excl ())) as (γr) "Ho2"; first done. iIntros "!>".
    wp_apply (newlock_spec (own γr (Excl ()) ∗ R)%I with "[Ho2 HR]"); first iSteps.
    iSteps as (lk γ) "Hlk". iIntros "!>".
    wp_apply (new_bag_spec N (p_inv' R γm γr)); first done.
    iSteps as (lb f Q Ψ v') "Hbag".
    iIntros "#Hf".
    iSteps as (ζ l' ts) "HI HΨζ Hrecp".
    destruct ts as [[[[γx γ1] γ3] γ4] γq]. iDecompose "Hrecp".
    iSteps as (v'' ℛ _) "HR HRΨ Hγx HRv''".
    iSpecialize ("HRΨ" $! v'').
    iRewrite -"HRΨ".
    iSteps.
  Qed.

End proof.