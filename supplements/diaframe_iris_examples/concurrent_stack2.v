From iris_examples.concurrent_stacks Require Import specs concurrent_stack2.
From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import excl.

From diaframe.lib Require Import own_hints.

Set Default Proof Using "Type".

(** Stack 2: Helping, bag spec. *)

Local Obligation Tactic := program_verify.

Definition channelR := exclR unitR.
Class channelG Σ := { #[local] channel_inG :: inG Σ channelR }.
Definition channelΣ : gFunctors := #[GFunctor channelR].
Global Program Instance subG_channelΣ {Σ} : subG channelΣ Σ → channelG Σ.

(* ^can probably be fixed by rewriting invariants *)

Section side_channel.
  Context `{!heapGS Σ, !channelG Σ} (N : namespace).
  Implicit Types l : loc.

  Definition revoke_tok γ := own γ (Excl ()).

  Definition stages γ (P : iProp Σ) l :=
    (∃ (z : Z), l ↦ #z ∗ (⌜z = 0⌝ ∗ P
     ∨ ⌜z = 1⌝
     ∨ ⌜z = 2⌝ ∗ revoke_tok γ))%I.

  (* this one is not a spec since user will need to specify P *)
  Global Program Instance mk_offer_spec (v : val) :
    SPEC {{ True }} 
      mk_offer v 
    {{ (o : loc), RET (v, #o)%V; ∀ P E, P ={E}=∗ ∃ γ, revoke_tok γ ∗ inv N (stages γ P o) }}.

  (* A partial specification for revoke that will be useful later *)
  Global Program Instance revoke_spec γ P v (o : loc) :
    SPEC {{ inv N (stages γ P o) ∗ revoke_tok γ }}
      revoke_offer (v, #o)%V
    {{ (v' : val), RET v'; (⌜v' = InjRV v⌝ ∗ P) ∨ ⌜v' = InjLV #()⌝ }}.

  (* A partial specification for take that will be useful later *)
  Global Program Instance take_spec γ P (o : loc) v :
    SPEC {{ inv N (stages γ P o) }}
      take_offer (v, #o)%V
    {{ (v' : val), RET v'; (⌜v' = InjRV v⌝ ∗ P) ∨ ⌜v' = InjLV #()⌝ }}.

  Global Opaque mk_offer.
End side_channel.

Section mailbox.
  Context `{!heapGS Σ, channelG Σ} (N : namespace).
  Implicit Types l : loc.

  Definition Noffer := N .@ "offer".

  Definition mailbox_inv (P : val → iProp Σ) (l : loc) : iProp Σ :=
    ∃ (v : val), l ↦ v ∗
    (⌜v = NONEV⌝ ∨ (∃ v' (o : loc) γ, ⌜v = SOMEV (v', #o)⌝ ∗ inv Noffer (stages γ (P v') o)))%I.

  Definition is_mailbox (P : val → iProp Σ) (v : val) : iProp Σ :=
    (∃ l, ⌜v = #l⌝ ∗ inv N (mailbox_inv P l))%I.

  Global Program Instance mk_mailbox_works :
    SPEC {{ True }} 
      mk_mailbox #() 
    {{ (v : val), RET v; ∀ P E, |={E}=> is_mailbox P v }}.

  Global Program Instance get_spec (P : val → iProp Σ) (mailbox : val) :
    SPEC {{ is_mailbox P mailbox }}
      concurrent_stack2.get mailbox
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ v, ⌜ov = SOMEV v⌝ ∗ P v}}.

  Global Program Instance put_spec (P : val → iProp Σ) (mailbox v : val) :
    SPEC {{ is_mailbox P mailbox ∗ P v }}
      put mailbox v
    {{ (o : val), RET o; (⌜o = SOMEV v⌝ ∗ P v) ∨ ⌜o = NONEV⌝ }}.

  Global Program Instance mailbox_persistent P m : Persistent (is_mailbox P m).

  Global Opaque mk_mailbox is_mailbox.
End mailbox.

Section stack_works.
  Context `{!heapGS Σ, channelG Σ} (N : namespace).
  Implicit Types l : loc.

  Definition Nmailbox := N .@ "mailbox".

  Definition is_list_pre (P : val → iProp Σ) (F : val -d> iPropO Σ) :
     val -d> iPropO Σ := (λ v, 
      ⌜v = NONEV⌝ ∨ (
        ∃ (l : loc), ⌜v = SOMEV #l⌝ ∗ ∃ (h : val) (t : val), l ↦□ (h, t)%V ∗ P h ∗ ▷ F t))%I.

  Local Instance is_list_contr (P : val → iProp Σ) : Contractive (is_list_pre P).
  Proof. rewrite /is_list_pre; repeat (intro); repeat (f_contractive || f_equiv). Qed.

  Definition is_list_def (P : val -> iProp Σ) := fixpoint (is_list_pre P).
  Definition is_list_aux P : seal (@is_list_def P). by eexists. Qed.
  Definition is_list P := unseal (is_list_aux P).
  Definition is_list_eq P : @is_list P = @is_list_def P := seal_eq (is_list_aux P).

  Lemma is_list_unfold (P : val → iProp Σ) v :
    is_list P v ⊣⊢ is_list_pre P (is_list P) v.
  Proof.
    rewrite is_list_eq. apply (fixpoint_unfold (is_list_pre P)).
  Qed.

  Lemma is_list_dup (P : val → iProp Σ) v :
    is_list P v ⊢ is_list P v ∗ (⌜v = NONEV⌝ ∨ ∃ l, ⌜v = SOMEV (LitV (LitLoc l))⌝ ∗ ∃ h t, l ↦□ (h, t)).
  Proof. rewrite is_list_unfold. iSteps. Qed.

  Definition stack_inv P l := (∃ vl, l ↦ vl ∗ is_list P vl ∗ ⌜val_is_unboxed vl⌝)%I.
  Definition is_stack P v :=
    (∃ mailbox l, ⌜v = (mailbox, #l)%V⌝ ∗ is_mailbox Nmailbox P mailbox ∗ inv N (stack_inv P l))%I.

  Global Instance biabd_islist_none P :
    HINT ε₀ ✱ [- ; emp] ⊫ [id]; is_list P NONEV ✱ [emp].
  Proof. iStep. rewrite is_list_unfold. iSteps. Qed.

  Global Instance biabd_islist_some P l :
    HINT ε₁ ✱ [h t; l ↦□ (h, t)%V ∗ P h ∗ ▷ is_list P t] ⊫ [id];
         is_list P (SOMEV #l) ✱ [emp].
  Proof.
    iSteps. rewrite (is_list_unfold _ (SOMEV _)) /=. 
    iSteps.
  Qed.

  Instance is_list_remember_part P v :
    MergablePersist (is_list P v) (λ p Pin Pout,
        TCAnd (TCEq Pin (ε₀)%I)
              (TCEq Pout (⌜v = NONEV⌝ ∨ ∃ l, ⌜SOMEV (LitV (LitLoc l)) = v⌝ ∗ ⌜val_is_unboxed v⌝ ∗ ∃ h t, l ↦□ (h, t))))%I | 30.
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->] /=. 
    rewrite bi.intuitionistically_if_elim is_list_dup.
    iSteps; iIntros "!>"; iSteps.
  Qed.

  Global Instance biabd_islist_pop P l h v E :
    HINT l ↦□ (h, v)%V ✱ [- ; is_list P (SOMEV #l)] ⊫ [fupd E E];
         ▷ is_list P v ✱ [⌜val_is_unboxed v⌝ ∗ P h] | 30.
  Proof.
    iStep as "Hl HPls".
    rewrite (is_list_unfold _ (InjRV _)) /=. iDecompose "HPls" as (l') "Hl HP Hls".
    rewrite is_list_unfold; iRevert "Hls". iSteps.
  Qed.

  Context (P : val → iProp Σ).

  Global Program Instance new_stack_spec :
    SPEC {{ True }} 
      new_stack #() 
    {{ (v : val), RET v; is_stack P v }}.

  Global Program Instance push_spec (s v : val) :
    SPEC {{ is_stack P s ∗ P v }} push s v {{ RET #(); True }}.

  Global Program Instance pop_spec (s : val) :
    SPEC {{ is_stack P s }}
      pop s
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ v, ⌜ov = SOMEV v⌝ ∗ P v }}.
End stack_works.

