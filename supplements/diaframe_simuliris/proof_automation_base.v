From diaframe Require Export proofmode_base.
From diaframe.symb_exec Require Import defs.
From diaframe.lib Require Import intuitionistically.

From simuliris.simulation Require Import lifting.
From simuliris.simulang Require Import primitive_laws.

Set Universe Polymorphism.

Proposition to_tforall {TT : tele} (Ψ : TT → Prop) :
  tforall Ψ → (∀ x, Ψ x).
Proof. apply tforall_forall. Qed.

Unset Universe Polymorphism.

Ltac drop_telescope_tac tele_name intro_pat :=
  revert tele_name; refine (to_tforall _ _); intros intro_pat.

Tactic Notation "drop_telescope" constr(R) "as" simple_intropattern_list(intro_pat) := 
  drop_telescope_tac R intro_pat.

Inductive context_as_item : (expr → expr) → Prop :=
  is_item K Kits : (∀ e, fill Kits e = K e) → context_as_item K.

Global Instance context_as_item_condition : ContextCondition expr := λ K, context_as_item K.

Global Arguments context_as_item_condition K /.

Global Instance items_valid_context K : SatisfiesContextCondition context_as_item_condition (fill K).
Proof. by econstructor. Qed.

Global Instance mono_template_condition {PROP : bi} {TT : tele} : 
  TemplateCondition PROP TT
  := (λ A R M R' M', template_mono M ∧ R = R' ∧ M = M').

Global Instance templateM_satisfies_wp_template_condition (TT : tele) (R : TT) n (PROP : bi) (M1 : PROP → PROP) M2 TT1 TT2 Ps Qs :
  ModalityMono M1 → 
  ModalityMono M2 → 
  SatisfiesTemplateCondition mono_template_condition R (template_M n M1 M2 TT1 TT2 Ps Qs) R (template_M n M1 M2 TT1 TT2 Ps Qs).
Proof.
  rewrite /SatisfiesTemplateCondition /= => HM1 HM2.
  split => //.
  by apply template_M_is_mono.
Qed.

Section target_executor.
  Context `{!sheapGS Σ, sheapInv Σ}.

  Instance target_execute : ExecuteOp (iPropI Σ) expr [tele_pair (expr → iProp Σ)] :=
    λ e, (λᵗ Φ, target_red e Φ)%I.

  Global Arguments target_execute e !R /.

  Instance target_red_cond : ReductionCondition (iPropI Σ) (expr) [tele] :=
    (λ A, λ _ e e' M, 
      (*⌜(∀ a, is_Some (to_val (e' a))) ∨ ∃ e'', (∀ a, e' a = e'') ∧ ∃ n φ, PureExec φ n e e'' ∧ φ⌝ ∧*)
      ∀ Φ, (M (λ a, target_red (e' a) Φ) -∗ target_red e Φ))%I.

  Global Arguments target_red_cond _ _ _ /.

  Global Instance target_execute_reduction_compat : 
    ExecuteReductionCompatibility target_execute (λᵗ Φ, TargO) target_red_cond context_as_item_condition 
        (mono_template_condition (TT := [tele_pair (expr → iProp Σ)])). (* not sure why this implicit cant be inferred *)
  Proof.
    move => K e A e' M /= HK R R' M' [HM [<- <-]].
    drop_telescope R as Φ => /= {R'}.
    iStep as "HΦ HM".
    destruct HK as [K Kits HKits]. rewrite -HKits.
    iApply target_red_bind.
    iApply "HΦ".
    iStopProof. apply HM => a. iSteps.
    iApply target_red_base. rewrite HKits //.
  Qed.

  Global Instance as_target_execute (e : expr) (Φ : expr → iProp Σ) : AsExecutionOf (target_red e Φ) target_execute e [tele_arg3 Φ].
  Proof. done. Qed.
End target_executor.

Section source_executor.
  Context `{!sheapGS Σ, sheapInv Σ}.

  Instance source_execute : ExecuteOp (iPropI Σ) expr [tele_pair thread_id; (expr → iProp Σ)] :=
    λ e, (λᵗ π Φ, source_red e π Φ)%I.

  Global Arguments source_execute e !R /.

  Instance source_red_cond : ReductionCondition (iPropI Σ) (expr) [tele] :=
    (λ A, λ _ e e' M, 
      (*⌜(∀ a, is_Some (to_val (e' a))) ∨ ∃ e'', (∀ a, e' a = e'') ∧ ∃ n φ, PureExec φ n e e'' ∧ φ⌝ ∧*)
      ∀ π Φ, (M (λ a, source_red (e' a) π Φ) -∗ source_red e π Φ))%I.

  Global Arguments source_red_cond _ _ _ /.

  Global Instance source_execute_reduction_compat : 
    ExecuteReductionCompatibility source_execute (λᵗ π Φ, TargO) source_red_cond context_as_item_condition 
        (mono_template_condition (TT := [tele_pair thread_id; (expr → iProp Σ)])).
  Proof.
    move => K e A e' M /= HK R R' M' [HM [<- <-]].
    drop_telescope R as π => /= {R'}.
    iStep 2 as (Φ) "HΦ HM".
    destruct HK as [K Kits HKits]. rewrite -HKits.
    iApply source_red_bind.
    iApply "HΦ".
    iStopProof. apply HM => a. iSteps.
    iApply source_red_base. rewrite HKits //.
  Qed.

  Global Instance as_source_execute e π Φ : AsExecutionOf (source_red e π Φ) source_execute e [tele_arg3 π; Φ].
  Proof. done. Qed.
End source_executor.

Section executor.
  Context `{!sheapGS Σ, sheapInv Σ}.

  Instance left_execute : ExecuteOp (iPropI Σ) expr [tele_pair expr ; thread_id; (expr → expr → iProp Σ)] :=
    λ el, (λᵗ er π Φ, el ⪯{π} er [{ Φ }])%I.

  Global Arguments left_execute e !R /.

  Global Instance left_execute_reduction_compat : 
    ExecuteReductionCompatibility left_execute (λᵗ el π Φ, TargO) target_red_cond context_as_item_condition 
      (mono_template_condition (TT := [tele_pair expr; thread_id; (expr → expr → iProp Σ)])).
  Proof.
    move => K e A e' M /= HK R R' M' [HM [<- <-]].
    drop_telescope R as er π Φ => /=.
    iStep as "HΦ HM".
    iApply target_red_sim_expr.
    destruct HK as [K Kits HKits]. rewrite -HKits.
    iApply target_red_bind. rewrite /target_red_cond /=.
    iApply "HΦ".
    iStopProof. apply HM => a. iSteps.
    iApply target_red_base.
    iApply target_red_base. iIntros "!> !>".
    rewrite HKits.
    iApply sim_expr_mono; last done. eauto.
  Qed.

  Instance right_execute : ExecuteOp (iPropI Σ) expr [tele_pair expr ; thread_id; (expr → expr → iProp Σ)] :=
    λ er, (λᵗ el π Φ, el ⪯{π} er [{ Φ }])%I.

  Global Arguments right_execute e !R /.

  Global Instance right_execute_reduction_compat : 
    ExecuteReductionCompatibility right_execute (λᵗ el π Φ, TargO) source_red_cond context_as_item_condition 
      (mono_template_condition (TT := [tele_pair expr; thread_id; (expr → expr → iProp Σ)])).
  Proof.
    move => K e A e' M /= HK R R' M' [HM [<- <-]].
    drop_telescope R as er π Φ => /=.
    iStep as "HΦ HM".
    iApply source_red_sim_expr.
    destruct HK as [K Kits HKits]. rewrite -HKits.
    iApply source_red_bind.
    iApply "HΦ".
    iStopProof. apply HM => a. iSteps.
    iApply source_red_base.
    iApply source_red_base. iIntros "!> !>".
    rewrite HKits.
    iApply sim_expr_mono; last done. eauto.
  Qed.

  Global Instance as_right_execute el er π Φ : AsExecutionOf (el ⪯{π} er [{ Φ }]) right_execute er [tele_arg3 el; π; Φ] | 10.
  Proof. done. Qed.

  Global Instance as_left_execute el er π Φ : AsExecutionOf (el ⪯{π} er [{ Φ }]) left_execute el [tele_arg3 er; π; Φ] | 20.
  Proof. done. Qed.

  Global Instance prepend_strong_update el er π Φ : 
    PrependModality (el ⪯{π} er [{ Φ }])%I (update_si_strong er π) (el ⪯{π} er [{ Φ }])%I.
  Proof.
    rewrite /PrependModality. apply: anti_symm.
    - iIntros "H". by iApply sim_update_si_strong.
    - iSteps.
  Qed.
End executor.


Section modalities.
  Context {PROP : bi} `{!BiBUpd PROP, !BiAffine PROP, !BiPureForall PROP}.
  Context {Λ : language}.
  Context {s : simulirisGS PROP Λ}.

  Global Instance update_si_ec : ModalityStrongMono update_si.
  Proof.
    split.
    - move => P Q HPQ. rewrite /update_si.
      iStep 7 as (el sl er sr πs) "HP Hs".
      iSpecialize ("HP" with "Hs"). rewrite -HPQ.
      iSteps.
    - move => P Q. rewrite /update_si.
      iStep 7 as (el sl er sr πs) "HP HQ Hs".
      iSpecialize ("HP" with "Hs"). 
      iSteps.
  Qed.

  Lemma bupd_stronger_update_si (P : PROP) : bupd P ⊢ update_si P.
  Proof. rewrite /update_si. iSteps. Qed.

  Lemma update_si_stronger_update_si_strong π e (P : PROP) : update_si P ⊢ update_si_strong π e P.
  Proof. 
    iStep 9 as (el sl er sr πs K Hπ Hsafe) "HP Hs".
    iDestruct ("HP" with "Hs") as ">[Hs HP]".
    iSteps.
  Qed.

  Global Instance update_si_split : ModalityCompat3 update_si update_si update_si.
  Proof.
    move => P.
    rewrite /update_si.
    iStep 7 as (el sl er sr πs) "HP Hs".
    iDestruct ("HP" with "Hs") as ">[Hs HP]".
    iDestruct ("HP" with "Hs") as ">[Hs HP]".
    iSteps.
  Qed.

  Global Instance split_update_si : SplitLeftModality3 update_si update_si update_si.
  Proof. split; tc_solve. Qed.

  Lemma split_left M M1 M2 (P : PROP) : SplitLeftModality3 M M1 M2 → M1 $ M2 P ⊢ M P.
  Proof. case => HM _ _ . apply HM. Qed.

  Global Instance update_si_split_bupd : ModalityCompat3 update_si update_si bupd.
  Proof.
    move => P. rewrite -(split_left update_si _ _ P).
    apply util_classes.modality_mono.
    apply bupd_stronger_update_si.
  Qed.

  Global Instance update_si_split_id : ModalityCompat3 update_si update_si id.
  Proof. move => P //. Qed.

  Global Instance update_si_strong_ec π e : ModalityStrongMono (update_si_strong π e).
  Proof.
    split.
    - move => P Q HPQ. rewrite /update_si_strong.
      iStep 8 as (el sl er sr πs K) "HP Hs".
      iSpecialize ("HP" with "Hs"). rewrite -HPQ.
      iSteps.
    - move => P Q. rewrite /update_si_strong.
      iStep 8 as (el sl er sr πs K) "HP HQ Hs".
      iSpecialize ("HP" with "Hs").
      iSteps.
  Qed.

  Global Instance update_strong_si_split π e : ModalityCompat3 (update_si_strong π e) (update_si_strong π e) (update_si_strong π e).
  Proof.
    move => P.
    iStep 9 as (el sl er sr πs K Hπ Hsafe) "HP Hs".
    iDestruct ("HP" with "Hs [%//]") as ">[Hs HP]".
    iDestruct ("HP" with "Hs [%//]") as ">[Hs HP]".
    iSteps.
  Qed.

  Global Instance split_update_si_strong π e : SplitLeftModality3 (update_si_strong π e) (update_si_strong π e) (update_si_strong π e).
  Proof. split; tc_solve. Qed.

  Global Instance update_strong_si_split_weak π e : ModalityCompat3 (update_si_strong π e) (update_si_strong π e) update_si.
  Proof.
    move => P. rewrite -(split_left (update_si_strong _ _) _ _ P).
    apply util_classes.modality_mono.
    apply update_si_stronger_update_si_strong.
  Qed.

  Global Instance update_si_strong_split_bupd π e : ModalityCompat3 (update_si_strong π e) (update_si_strong π e) bupd.
  Proof.
    move => P. rewrite -(split_left (update_si_strong _ _) _ _ P).
    apply util_classes.modality_mono.
    rewrite -update_si_stronger_update_si_strong -bupd_stronger_update_si //.
  Qed.

  Global Instance update_si_strong_split_id π e : ModalityCompat3 (update_si_strong π e) (update_si_strong π e) id.
  Proof. move => P //. Qed.


  Global Instance update_si_introducable : IntroducableModality (update_si).
  Proof. move => P /=. rewrite -bupd_stronger_update_si. iSteps. Qed.

  Global Instance update_si_strong_introducable π e : IntroducableModality (update_si_strong π e).
  Proof. move => P /=. rewrite -update_si_stronger_update_si_strong -bupd_stronger_update_si. iSteps. Qed.


  Global Instance elim_modal_bupd_update_si p (P Q : PROP) : 
    ElimModal True p false (|==> P) P (update_si Q) (update_si Q).
  Proof.
    rewrite /ElimModal bi.intuitionistically_if_elim /= => _.
    iStep as "HP HQ".
    iApply split_left.
    iApply bupd_stronger_update_si.
    iMod "HP". by iApply "HQ".
  Qed.

  Global Instance elim_modal_update_si p (P Q : PROP) : 
    ElimModal True p false (update_si P) P (update_si Q) (update_si Q).
  Proof.
    rewrite /ElimModal bi.intuitionistically_if_elim /= => _.
    iStep as "HP HQ".
    iApply split_left. iCombine "HP HQ" as "HM". rewrite modality_strong_frame_l.
    iStopProof. apply: util_classes.modality_mono. rewrite bi.wand_elim_r //.
  Qed.

  Global Instance elim_modal_bupd_update_si_strong p (P Q : PROP) π e  : 
    ElimModal True p false (|==> P) P (update_si_strong π e Q) (update_si_strong π e Q).
  Proof.
    rewrite /ElimModal bi.intuitionistically_if_elim /= => _.
    iStep as "HP HQ".
    iApply split_left.
    iApply update_si_stronger_update_si_strong.
    iMod "HP". iApply bupd_stronger_update_si. iIntros "!>". by iApply "HQ".
  Qed.

  Global Instance elim_modal_update_si_upd_si_strong p (P Q : PROP) π e : 
    ElimModal True p false (update_si P) P (update_si_strong π e Q) (update_si_strong π e Q).
  Proof.
    rewrite /ElimModal bi.intuitionistically_if_elim /= => _.
    iStep as "HP HQ".
    iApply split_left. iCombine "HP HQ" as "HM". rewrite modality_strong_frame_l.
    iApply update_si_stronger_update_si_strong.
    iStopProof. apply: util_classes.modality_mono. rewrite bi.wand_elim_r //.
  Qed.

  Global Instance elim_modal_update_si_strong_upd_si_strong p (P Q : PROP) π e : 
    ElimModal True p false (update_si_strong π e P) P (update_si_strong π e Q) (update_si_strong π e Q).
  Proof.
    rewrite /ElimModal bi.intuitionistically_if_elim /= => _.
    iStep as "HP HQ".
    iApply split_left. iCombine "HP HQ" as "HM". rewrite modality_strong_frame_l.
    iStopProof. apply: util_classes.modality_mono. rewrite bi.wand_elim_r //.
  Qed.


  Global Instance combined_safe_update_strong π e : CombinedModalitySafe (update_si_strong π e) (update_si_strong π e) (update_si_strong π e).
  Proof.
    move => P. apply (anti_symm _).
    - apply update_si_strong_introducable.
    - by iIntros ">P".
  Qed.

  Global Instance combined_safe_update_si  : CombinedModalitySafe (update_si) (update_si) (update_si).
  Proof.
    move => P. apply (anti_symm _).
    - apply update_si_introducable.
    - by iIntros ">P".
  Qed.

  Global Instance combined_safe_update_strong_si π e : CombinedModalitySafe (update_si_strong π e) (update_si) (update_si_strong π e).
  Proof.
    move => P. apply (anti_symm _).
    - iIntros ">P". iApply update_si_strong_introducable => /=. by iApply update_si_introducable.
    - iIntros ">>P". by iApply update_si_strong_introducable.
  Qed.

  Global Instance combined_safe_update_si_strong π e : CombinedModalitySafe (update_si) (update_si_strong π e) (update_si_strong π e).
  Proof.
    move => P. apply (anti_symm _).
    - apply update_si_introducable.
    - by iIntros ">P".
  Qed.

  Global Instance combined_safe_update_strong_bupd π e : CombinedModalitySafe (update_si_strong π e) (bupd) (update_si_strong π e).
  Proof.
    move => P. apply (anti_symm _).
    - iIntros ">P". iApply update_si_strong_introducable => /=. by iIntros "!>".
    - iIntros ">>P". by iApply update_si_strong_introducable.
  Qed.

  Global Instance combined_safe_bupd_update_strong π e : CombinedModalitySafe (bupd) (update_si_strong π e) (update_si_strong π e).
  Proof.
    move => P. apply (anti_symm _).
    - by iIntros "$".
    - by iIntros ">P".
  Qed.

  Global Instance combined_safe_update_bupd : CombinedModalitySafe (update_si) (bupd) (update_si).
  Proof.
    move => P. apply (anti_symm _).
    - iIntros ">P". iApply update_si_introducable => /=. by iIntros "!>".
    - iIntros ">>P". by iApply update_si_introducable.
  Qed.

  Global Instance combined_safe_bupd_update : CombinedModalitySafe (bupd) (update_si) (update_si).
  Proof.
    move => P. apply (anti_symm _).
    - by iIntros "$".
    - by iIntros ">P".
  Qed.
End modalities.


Class SimplSubst (e e' : expr) :=
  is_simpl_subst : e = e'.

Lemma W_tac_to_expr_subst' :
∀ (x : string) (v : val) (e : expr) (P : expr → Prop) (e' : W.expr) e'',
  e = W.to_expr e' → (∀ v, W.subst x v e' = e'' v) → P (W.to_expr $ e'' v) → P (subst x v e).
Proof.
  intros. specialize (H0 v).
  eapply W.tac_to_expr_subst => //.
Qed.

Lemma W_tac_to_expr_combine_subst_map' v :
∀ (e : val → W.expr) (P : expr → Prop) e', (∀ v, W.combine_subst_map [] (e v) = (e' v)) → P (W.to_expr $ e' v) → P (W.to_expr $ e v).
Proof.
  intros. specialize (H v).
  eapply W.tac_to_expr_combine_subst_map => //.
Qed. (* abstracts over v so that vm_compute does not unfold value computations *)

Ltac simpl_subst' :=
  repeat match goal with
    | |- context C [apply_func ?fn ?v] =>
      (* Unfold [apply_func] if the function's components are available *)
      let arg := open_constr:(_ : string) in
      let body := open_constr:(_ : expr) in
      unify fn (arg, body);
      change (apply_func fn v) with (subst arg v body)
    end;
  repeat match goal with
    | |- context C [subst ?x ?v ?e] =>
      lazymatch e with
      | subst _ _ _ => fail
      | _ => idtac
      end;
      pattern (subst x v e);
      let e' := W.of_expr e in
      simple refine (W_tac_to_expr_subst' _ _ _ _ e' _ _ _ _); [ shelve
      | simpl; rewrite ?list_to_map_to_list; reflexivity
      | intros ?; vm_compute W.subst; reflexivity
      |];
      simple refine (W_tac_to_expr_combine_subst_map' _ _ _ _ _ _); [ shelve
      | intros ?; vm_compute W.combine_subst_map; reflexivity
      | ];
      simpl
    end.

Global Hint Extern 4 (SimplSubst ?e ?e') =>
  simpl; simpl_subst'; reflexivity : typeclass_instances.


Section specs.
  Context `{!sheapGS Σ, sheapInv Σ}.

  Instance source_elim_bupd p e (Φ : expr → iProp Σ) P π : 
    ElimModal True p false (|==> P) P (source_red e π Φ) (source_red e π Φ).
  Proof.
    rewrite /ElimModal /= => _.
    rewrite bi.intuitionistically_if_elim.
    iStep as "HP HΦ".
    iApply source_red_update_si. iMod "HP". iApply mod_weaker => /=. iSteps.
  Qed.

  Instance source_elim_update_si p e (Φ : expr → iProp Σ) P π : 
    ElimModal True p false (update_si P) P (source_red e π Φ) (source_red e π Φ).
  Proof.
    rewrite /ElimModal => _.
    rewrite bi.intuitionistically_if_elim.
    iIntros "[H1 H2]".
    iApply source_red_update_si. iMod "H1". iApply mod_weaker => /=. iSteps.
  Qed.

  Lemma source_red_update_si_strong e π Φ :
    update_si_strong e π (source_red e π Φ) -∗ source_red e π Φ.
  Proof.
    iIntros "H1".
    iApply source_red_step.
    iIntros (P_t σ_t P_s σ_s T_s K_s) "[Hs [% %]]".
    iDestruct ("H1" with "Hs [%//]") as ">[Hs He]".
    iExists _, _. rewrite list_insert_id //. iFrame.
    iPureIntro. apply: no_forks_refl.
  Qed.

  Instance source_elim_update_si_strong p e (Φ : expr → iProp Σ) P π : 
    ElimModal True p false (update_si_strong e π P) P (source_red e π Φ) (source_red e π Φ).
  Proof.
    rewrite /ElimModal => _.
    rewrite bi.intuitionistically_if_elim.
    iIntros "[H1 H2]".
    iApply source_red_update_si_strong. iMod "H1". iApply update_si_stronger_update_si_strong. iApply mod_weaker => /=. iSteps.
  Qed.

  Instance target_elim_bupd p e (Φ : expr → iProp Σ) P : 
    ElimModal True p false (|==> P) P (target_red e Φ) (target_red e Φ).
  Proof.
    rewrite /ElimModal /= => _.
    rewrite bi.intuitionistically_if_elim.
    iStep.
    iApply (target_red_bind _ empty_ectx). iApply target_red_base. simpl.
    iSteps.
  Qed.

  Global Instance pure_wp_step_source_exec (e : expr) φ n e' e'' w :
    PureExec φ n e e' →
    SimplSubst e' e'' →
    SolveSepSideCondition φ →
    ReductionStep (source_red_cond, w) e ⊣ ⟨id⟩ True ; ε₀ =[▷^0]=> ⟨id⟩ e'' ⊣ emp | 10.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep => HS <- /=.
    iSteps.
    iApply source_red_lift_pure; first done. iSteps.
  Qed.

  Global Instance learn_safe_implication (e : expr) φ w :
    (∀ P σ, SafeImplies φ P e σ) →
    TCIf (SolveSepSideCondition φ) False TCTrue →
    ReductionStep (source_red_cond, w) e ⊣ ⟨id⟩ True ; ε₀ =[▷^0]=> ⟨id⟩ e ⊣ ⌜φ⌝ | 20.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep => HS _ /=.
    iStep 4.
    by iApply source_red_safe_implies.
  Qed.

  Global Instance pure_wp_step_source_exec_last_resort (e : expr) φ n e' e'' w :
    PureExec φ n e e' →
    SimplSubst e' e'' →
    ReductionStep (source_red_cond, w) e ⊣ ⟨id⟩ ⌜φ⌝ ; ε₀ =[▷^0]=> ⟨id⟩ e'' ⊣ emp | 30.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep => HS <- /=.
    iSteps.
    iApply source_red_lift_pure; first done. iSteps.
  Qed.

  Global Instance pure_wp_step_target_exec (e : expr) φ n e' e'' w :
    PureExec φ n e e' →
    SimplSubst e' e'' →
    ReductionStep (target_red_cond, w) e ⊣ ⟨id⟩ ⌜φ⌝ ; ε₀ =[▷^0]=> ⟨id⟩ e'' ⊣ emp.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep => HS <- /=.
    iSteps.
    iApply target_red_lift_pure; first done. iSteps.
  Qed.

  Global Instance allocn_step_source w n v `{!sheapInvSupportsAlloc} :
    SolveSepSideCondition (0 < n)%Z →
    ReductionStep (source_red_cond, w) (AllocN (Val $ LitV $ LitInt $ n) (Val v)) ⊣ ⟨id⟩ True ; ε₀ =[▷^0]=> 
      ∃ (l : loc), ⟨id⟩ #l ⊣ l ↦s∗ (replicate (Z.to_nat n) v) ∗ † l …s (Z.to_nat n) | 50.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /SolveSepSideCondition /= => Hn.
    iSteps. iApply source_red_allocN; first done.
    iSteps.
  Qed.

  Global Instance alloc_step_source w v `{!sheapInvSupportsAlloc} :
    ReductionStep (source_red_cond, w) (Alloc (Val v)) ⊣ ⟨id⟩ True ; ε₀ =[▷^0]=> 
      ∃ (l : loc), ⟨id⟩ #l ⊣ l ↦s v ∗ † l …s 1 | 40.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps. iApply source_red_alloc. iSteps.
  Qed.

  Global Instance allocn_step_target w n v :
    SolveSepSideCondition (0 < n)%Z →
    ReductionStep (target_red_cond, w) (AllocN (Val $ LitV $ LitInt $ n) (Val v)) ⊣ ⟨id⟩ True ; ε₀ =[▷^0]=> 
      ∃ (l : loc), ⟨id⟩ #l ⊣ l ↦t∗ (replicate (Z.to_nat n) v) ∗ † l …t (Z.to_nat n) | 50.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /SolveSepSideCondition /= => Hn.
    iSteps. iApply target_red_allocN; first done.
    iSteps.
  Qed.

  Global Instance alloc_step_target w v  :
    ReductionStep (target_red_cond, w) (Alloc (Val v)) ⊣ ⟨id⟩ True ; ε₀ =[▷^0]=> 
      ∃ (l : loc), ⟨id⟩ #l ⊣ l ↦t v ∗ † l …t 1 | 40.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps. iApply target_red_alloc. iSteps.
  Qed.

  Typeclasses Opaque heap_freeable.

  Global Instance freen_step_source w (l : loc) (n : Z) `{!sheapInvSupportsFree} :
    ReductionStep (source_red_cond, w) (FreeN #n #l) ⊣ ⟨id⟩ ∃ vs, l ↦s∗ vs ∗ ⌜n = length vs⌝ ∗ † l …s Z.to_nat n ; ε₁ =[▷^0]=>
      ⟨id⟩ #() ⊣ † l …s - | 50.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (vs n Φ) "Hl H† HΦ". iApply (source_red_freeN with "Hl H†") => //; iSteps.
  Qed.

  Global Instance free_step_source w (l : loc) `{!sheapInvSupportsFree} :
    ReductionStep (source_red_cond, w) (FreeN #1 #l) ⊣ ⟨id⟩ ∃ v, l ↦s v ∗ † l …s 1 ; ε₁ =[▷^0]=>
      ⟨id⟩ #() ⊣ † l …s - | 40.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (vs n Φ) "Hl H† HΦ". iApply (source_red_free with "Hl H†"); iSteps.
  Qed.

  Global Instance freen_step_target w (l : loc) (n : Z) :
    ReductionStep (target_red_cond, w) (FreeN #n #l) ⊣ ⟨id⟩ ∃ vs, l ↦t∗ vs ∗ ⌜n = length vs⌝ ∗ † l …t Z.to_nat n ; ε₁ =[▷^0]=>
      ⟨id⟩ #() ⊣ † l …t - | 50.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (vs n) "Hl H† HΦ". iApply (target_red_freeN with "Hl H†") => //; iSteps.
  Qed.

  Global Instance free_step_target w (l : loc) :
    ReductionStep (target_red_cond, w) (FreeN #1 #l) ⊣ ⟨id⟩ ∃ v, l ↦t v ∗ † l …t 1 ; ε₁ =[▷^0]=>
      ⟨id⟩ #() ⊣ † l …t - | 40.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (vs n) "Hl H† HΦ". iApply (target_red_free with "Hl H†"); iSteps.
  Qed.

  Lemma load_sc_step_source w (l : loc) `{!sheapInvSupportsLoad ScOrd} q v : 
    ReductionStep (source_red_cond, w) (!ˢᶜ #l)%E ⊣ ⟨id⟩ True ; l ↦s{#q} v =[▷^0]=> 
      ⟨id⟩ v ⊣ l ↦s{#q} v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (n Φ) "Hl HΦ".
    iApply (source_red_load_sc with "Hl"). iSteps.
  Qed.

  Lemma load_sc_step_target w (l : loc) q v : 
    ReductionStep (target_red_cond, w) (!ˢᶜ #l)%E ⊣ ⟨id⟩ True ; l ↦t{#q} v =[▷^0]=> 
      ⟨id⟩ v ⊣ l ↦t{#q} v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (Φ) "Hl HΦ".
    iApply (target_red_load_sc with "Hl"). iSteps.
  Qed.

  Lemma load_na_step_source w (l : loc) `{!sheapInvSupportsLoad Na1Ord} q v : 
    ReductionStep (source_red_cond, w) (! #l)%E ⊣ ⟨id⟩ True ; l ↦s{#q} v =[▷^0]=> 
      ⟨id⟩ v ⊣ l ↦s{#q} v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (n Φ) "Hl HΦ".
    iApply (source_red_load_na with "Hl"). iSteps.
  Qed.

  Lemma load_na_step_target w (l : loc) q v : 
    ReductionStep (target_red_cond, w) (! #l)%E ⊣ ⟨id⟩ True ; l ↦t{#q} v =[▷^0]=> 
      ⟨id⟩ v ⊣ l ↦t{#q} v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (Φ) "Hl HΦ".
    iApply (target_red_load_na with "Hl"). iSteps.
  Qed.

  Lemma store_sc_step_source w (l : loc) `{!sheapInvSupportsStore ScOrd} (v v' : val) : 
    ReductionStep (source_red_cond, w) (#l <-ˢᶜ v)%E ⊣ ⟨id⟩ True ; l ↦s v' =[▷^0]=> 
      ⟨id⟩ #() ⊣ l ↦s v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (n Φ) "Hl HΦ".
    iApply (source_red_store_sc with "Hl"). iSteps.
  Qed.

  Lemma store_sc_step_target w (l : loc) (v v' : val) : 
    ReductionStep (target_red_cond, w) (#l <-ˢᶜ v)%E ⊣ ⟨id⟩ True ; l ↦t v' =[▷^0]=> 
      ⟨id⟩ #() ⊣ l ↦t v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (Φ) "Hl HΦ".
    iApply (target_red_store_sc with "Hl"). iSteps.
  Qed.

  Lemma store_na_step_source w (l : loc) `{!sheapInvSupportsStore Na1Ord} (v v' : val) : 
    ReductionStep (source_red_cond, w) (#l <- v)%E ⊣ ⟨id⟩ True ; l ↦s v' =[▷^0]=> 
      ⟨id⟩ #() ⊣ l ↦s v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (n Φ) "Hl HΦ".
    iApply (source_red_store_na with "Hl"). iSteps.
  Qed.

  Lemma store_na_step_target w (l : loc) (v v' : val) : 
    ReductionStep (target_red_cond, w) (#l <- v)%E ⊣ ⟨id⟩ True ; l ↦t v' =[▷^0]=> 
      ⟨id⟩ #() ⊣ l ↦t v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (Φ) "Hl HΦ".
    iApply (target_red_store_na with "Hl"). iSteps.
  Qed.

  Global Instance if_step_bool_decide_source P `{Decision P} e1 e2 w :
    ReductionStep (source_red_cond, w) (if: #(bool_decide P) then e1 else e2)%E ⊣ ⟨id⟩ emp; ε₀ =[▷^0]=>
      ∃ b : bool, ⟨id⟩ (if b then e1 else e2)%V ⊣ ⌜b = true⌝ ∗ ⌜P⌝ ∨ ⌜b = false⌝ ∗ ⌜¬P⌝| 50.
  Proof.
    (* texan_to_red_cond does not work here, since (if b then e1 else e2) is not a value! *)
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (n Φ) "HΦ".
    case_bool_decide; iApply source_red_lift_pure => //.
    - iApply ("HΦ" $! true). eauto.
    - iApply ("HΦ" $! false). eauto.
  Qed.

  Global Instance if_step_bool_decide_source_neg_1 P `{Decision P} e1 e2 w :
    ReductionStep (source_red_cond, w) (if: #(bool_decide (¬ P)) then e1 else e2)%E ⊣ ⟨id⟩ emp; ε₀ =[▷^0]=>
      ∃ b : bool, ⟨id⟩ (if b then e1 else e2)%V ⊣ ⌜b = true⌝ ∗ ⌜¬ P⌝ ∨ ⌜b = false⌝ ∗ ⌜P⌝| 49.
  Proof.
    (* texan_to_red_cond does not work here, since (if b then e1 else e2) is not a value! *)
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (n Φ) "HΦ".
    case_bool_decide; iApply source_red_lift_pure => //.
    - iApply ("HΦ" $! true). eauto.
    - iApply ("HΦ" $! false). eauto.
  Qed.

  Global Instance if_step_bool_decide_source_neg_2 P `{Decision P} e1 e2 w :
    ReductionStep (source_red_cond, w) (if: #(negb $ bool_decide P) then e1 else e2)%E ⊣ ⟨id⟩ emp; ε₀ =[▷^0]=>
      ∃ b : bool, ⟨id⟩ (if b then e1 else e2)%V ⊣ ⌜b = true⌝ ∗ ⌜¬ P⌝ ∨ ⌜b = false⌝ ∗ ⌜P⌝| 49.
  Proof.
    (* texan_to_red_cond does not work here, since (if b then e1 else e2) is not a value! *)
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (n Φ) "HΦ".
    case_bool_decide; iApply source_red_lift_pure => //.
    - iApply ("HΦ" $! false). eauto.
    - iApply ("HΦ" $! true). eauto.
  Qed.
End specs.



Ltac find_reshape e K e' TC :=
  lazymatch e with
  | fill ?Kabs ?e_inner =>
    reshape_expr e_inner ltac:(fun K' e'' => 
      unify K (fill Kabs ∘ fill K'); unify e' e'';
      notypeclasses refine (ConstructReshape e (fill Kabs ∘ fill K') e'' _ (eq_refl) _); tc_solve )
  | _ =>
    reshape_expr e ltac:(fun K' e'' => 
      unify K (fill K'); unify e' e''; 
      notypeclasses refine (ConstructReshape e (fill K') e'' _ (eq_refl) _); tc_solve )
  end.

Global Hint Extern 4 (ReshapeExprAnd expr ?e ?K ?e' ?TC) => 
  find_reshape e K e' TC : typeclass_instances.

Global Hint Extern 4 (ReshapeExprAnd (language.expr ?L) ?e ?K ?e' ?TC) =>
  unify L simp_lang;
  find_reshape e K e' TC : typeclass_instances.



Section abducts.
  Context `{!sheapGS Σ, !sheapInv Σ}.

  Global Instance left_execution_abduct H P e R K e_in' T e_out' MT MT' R' :
    AsExecutionOf P left_execute e R →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep target_red_cond T H TargO e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition (mono_template_condition (TT := [tele_pair expr; thread_id; (expr → expr → iProp Σ)])) R MT R' MT' →
    Abduct (TT := [tele]) false H P id (MT' $ flip left_execute R' ∘ K ∘ e_out') | 30.
  Proof. intros. eapply execution_abduct_lem => //; [tc_solve| done]. Qed.

  Global Instance right_execution_abduct H P e R K e_in' T e_out' MT MT' R' :
    AsExecutionOf P right_execute e R →
    ReshapeExprAnd expr e K e_in' (ReductionTemplateStep source_red_cond T H TargO e_in' e_out' MT) →
    SatisfiesContextCondition context_as_item_condition K →
    SatisfiesTemplateCondition (mono_template_condition (TT := [tele_pair expr; thread_id; (expr → expr → iProp Σ)])) R MT R' MT' →
    Abduct (TT := [tele]) false H P id (MT' $ flip right_execute R' ∘ K ∘ e_out') | 20.
  Proof. intros. eapply execution_abduct_lem => //; [tc_solve| done]. Qed.

  Global Instance refines_values_abduct v1 v2 π Φ:
    Abduct (TT := [tele]) false (ε₀)%I (Val v1 ⪯{π} Val v2 [{Φ}])%I id (update_si_strong (Val v2) π $ Φ v1 v2) | 10.
  Proof.
    rewrite /Abduct empty_hyp_first_eq. iIntros "[_ HΦ]".
    iApply sim_update_si_strong. iMod "HΦ". iApply update_si_stronger_update_si_strong. iApply mod_weaker.
    by iApply sim_expr_base.
  Qed.
End abducts.



Lemma eval_quot_cond (x y : Z) (r : val) :
  y ≠ 0 →
  r = #(x `quot` y) →
  bin_op_eval QuotOp #x #y = Some r.
Proof.
  rewrite /bin_op_eval => Hx -> /=.
  rewrite decide_False //.
Qed.



Global Hint Extern 4 (vals_compare_safe _ _) => (left; pure_solver.trySolvePure || right; pure_solver.trySolvePure) : solve_pure_add. 
Global Hint Extern 4 (bin_op_eval QuotOp _ _ = Some _) => apply eval_quot_cond; pure_solver.trySolvePure : solve_pure_eq_add.


Global Typeclasses Opaque update_si update_si_strong.
Global Arguments update_si_strong : simpl never.
Global Arguments update_si : simpl never.


Section collect_modal.
  Context {PROP : bi} `{!BiBUpd PROP, !BiAffine PROP, !BiPureForall PROP}.
  Context {Λ : language}.
  Context {s : simulirisGS PROP Λ}.

  Global Instance collect_modal_update_si_strong (P : PROP) e π  :
    Collect1Modal (update_si_strong e π P) (update_si_strong e π) P | 45.
  Proof. rewrite /Collect1Modal //. Qed.

  Global Instance collect_modal_update_si (P : PROP) :
    Collect1Modal (update_si P) (update_si) P | 40.
  Proof using BiAffine0.
    by rewrite /Collect1Modal.
  Qed.
End collect_modal.


Section sim_specs.
  Context `{!sheapGS Σ, !sheapInv Σ}.

  Class SimultaneousSpec el er H (P : thread_id → (expr → expr → iProp Σ) → iProp Σ) := {
    simultaneous_spec Φ π : H ∗ P π Φ ⊢ el ⪯{π} er [{Φ}];
    simultaneous_spec_funex Φ Ψ π : (∀ e1 e2, Φ e1 e2 ⊣⊢ Ψ e1 e2) → P π Φ ⊣⊢ P π Ψ
  }.

  Global Instance sim_call_sim_spec π Φ el er Kl Kr el_f er_f H P :
    ReshapeExprAnd expr el Kl el_f (
      ReshapeExprAnd expr er Kr er_f (
        SimultaneousSpec el_f er_f H P
      )
    ) →
    SatisfiesContextCondition context_as_item_condition Kl →
    SatisfiesContextCondition context_as_item_condition Kr →
    Abduct (TT := [tele]) false H (el ⪯{π} er [{Φ}])%I id
        (update_si_strong er π $ P π $ λ e_t' e_s', Kl e_t' ⪯{π} Kr e_s' [{Φ}])%I | 10.
  Proof.
    rewrite /Abduct /=. iSteps as ([-> [-> Hspec]] HKl HKr) "??".
    destruct HKl as [Kl Kl' HKl'].
    destruct HKr as [Kr Kr' HKr'].
    rewrite -HKl' -HKr'.
    iApply sim_update_si_strong.
    iStopProof. rewrite modality_strong_frame_r.
    apply util_classes.modality_mono.
    iStep. iApply sim_expr_bind.
    destruct Hspec as [H1 H2].
    iApply H1. iFrame. iStopProof. simpl.
    erewrite H2; first done.
    move => e1 e2 /=. rewrite HKl' HKr' //.
  Qed.

  Global Instance call_sim_spec fname arg_l arg_lv arg_r arg_rv :
    TCEq (to_val arg_l) (Some arg_lv) →
    TCEq (to_val arg_r) (Some arg_rv) →
    SimultaneousSpec (Call f#fname arg_l) (Call f#fname arg_r) (ε₀)%I (λ π Φ,
      ext_rel π arg_lv arg_rv ∗ (∀ v_t' v_s', ext_rel π v_t' v_s' -∗ Φ v_t' v_s'))%I.
  Proof.
    move => Hl Hr; split.
    - move => Φ π. iStep as "Hs HΦ".
      iApply (sim_call with "Hs") => //; [rewrite Hl| rewrite Hr]; done.
    - move => Φ Ψ π HΦΨ. apply: anti_symm.
      * iSteps. iApply HΦΨ. iSteps.
      * iSteps. iApply HΦΨ. iSteps.
  Qed.
End sim_specs.

From simuliris.simulang Require Import gen_val_rel.

Section val_rel_hints.
  Context {Σ} (loc_rel : loc → loc → iProp Σ).
  Let val_rel := gen_val_rel loc_rel.

  Global Arguments gen_val_rel : simpl nomatch.

  Global Arguments gen_val_rel {_} _ !_ !_ : simpl nomatch.

  Global Instance simplify_right_Z_val_rel v (z : Z) : MergableConsume (val_rel v #z) true (λ p Pin Pout,
    TCAnd (TCEq p false) $
    TCAnd (TCEq Pin (ε₀)%I) $
           TCEq Pout ⌜v = #z⌝%I ).
  Proof.
    move => p Pin Pout [-> [-> ->]]. unfold val_rel.
    iStep as "Hv". by val_discr_source "Hv".
  Qed.

  Global Instance simplify_right_pair_val_rel vl v1 v2 : MergableConsume (val_rel vl (v1, v2)) true (λ p Pin Pout,
    TCAnd (TCEq p false) $
    TCAnd (TCEq Pin (ε₀)%I) $
           TCEq Pout (∃ vl1 vl2, ⌜vl = PairV vl1 vl2⌝ ∗ val_rel vl1 v1 ∗ val_rel vl2 v2)%I).
  Proof.
    move => p Pin Pout [-> [-> ->]]. unfold val_rel.
    iStep as "Hvs".
    iPoseProof (gen_val_rel_pair_source with "Hvs") as "(%x_t & %y_t & -> & Hx_r & Hy_r)".
    iSteps.
  Qed.

  Global Instance simplify_right_bool_val_rel v (b : bool) : MergableConsume (val_rel v #b) true (λ p Pin Pout,
    TCAnd (TCEq p false) $
    TCAnd (TCEq Pin (ε₀)%I) $
           TCEq Pout ⌜v = #b⌝%I ).
  Proof.
    move => p Pin Pout [-> [-> ->]]. unfold val_rel.
    iStep as "Hb". by val_discr_source "Hb".
  Qed.

  Global Instance simplify_right_loc_val_rel v (l : loc) : MergableConsume (val_rel v #l) true (λ p Pin Pout,
    TCAnd (TCEq p false) $
    TCAnd (TCEq Pin (ε₀)%I) $
           TCEq Pout (∃ (l' : loc), ⌜v = #l'⌝ ∗ loc_rel l' l)%I ).
  Proof.
    move => p Pin Pout [-> [-> ->]]. unfold val_rel.
    iStep as "Hl".
    iPoseProof (gen_val_rel_loc_source with "Hl") as (? ->) "H". iSteps.
  Qed.

  Global Instance simplify_right_fn_val_rel v (fn : fname) : MergableConsume (val_rel v f#fn) true (λ p Pin Pout,
    TCAnd (TCEq p false) $
    TCAnd (TCEq Pin (ε₀)%I) $
           TCEq Pout (⌜v = f#fn⌝%I)).
  Proof.
    move => p Pin Pout [-> [-> ->]]. unfold val_rel.
    iStep as "Hf". by val_discr_source "Hf".
  Qed.

End val_rel_hints.



















