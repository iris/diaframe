From iris.proofmode Require Import proofmode.
From simuliris.simulation Require Import slsls lifting.
From simuliris.simulang Require Import lang notation proofmode behavior.
From simuliris.simulang.simple_inv Require Import inv adequacy.
From iris.prelude Require Import options.

From diaframe.simuliris.simple Require Import proof_automation.

(** * Simple example for re-ordering two allocs and then passing the related locations to an external function. *)

Section reorder.
  Context `{!simpleGS Σ}.

  Definition alloc2_and_cont : expr :=
    let: "l1" := Alloc "v1" in
    let: "l2" := Alloc "v2" in
    Call "cont" ("l1", "l2").

  Definition alloc2_and_cont' : expr :=
    let: "l2" := Alloc "v2" in
    let: "l1" := Alloc "v1" in
    Call "cont" ("l1", "l2").

  Lemma alloc2_reorder :
    ⊢ log_rel alloc2_and_cont alloc2_and_cont'.
  Proof. log_rel. iSteps. Qed.

End reorder.

Section closed.
  (** Obtain a closed proof of [ctx_ref]. *)
  Lemma alloc2_reorder_ctx : ctx_ref alloc2_and_cont alloc2_and_cont'.
  Proof.
    set Σ := #[simpleΣ].
    apply (log_rel_adequacy Σ)=>?.
    apply alloc2_reorder.
  Qed.
End closed.
