From simuliris.simulang Require Import lang notation tactics class_instances.
From iris.proofmode Require Import proofmode.
From simuliris.simulation Require Import slsls lifting.
From simuliris.simulang.simple_inv Require Import inv.
From iris.prelude Require Import options.

From diaframe.simuliris.simple Require Import proof_automation.

(** * Examples of coinductive simulation proofs *)

Section fix_bi.
  Context `{!simpleGS Σ}.

  (** Trivial loop *)
  Definition loop_test n : expr :=
    let: "n" := Alloc #n in
    While (#0 < ! "n") ("n" <- ! "n" - #1).

  Lemma loop (n : nat) π :
    ⊢ loop_test n ⪯{π} #() {{ val_rel }}.
  Proof.
    rewrite /loop_test. iSteps as (l) "Hl Hdie".
    iInduction n as [|n] "IH" forall "Hl".
    - target_while. iApply target_red_base. iSteps.
    - target_while. iApply target_red_base. iSteps.
      assert (Z.sub (Z.of_nat (S n)) (Zpos xH) = Z.of_nat n) as -> by lia.
      iSteps.
  Qed.


  (** We can prove that [mul_loop] simulates primitive multiplication, and vice versa.
    The prove that it simulates primitive multiplication is based on our powerful unbounded stuttering support (the source program takes a single step!) . *)
  Definition mul_loop e1 e2 : expr :=
    let: "m" := e2 in
    let: "n" := Alloc e1 in
    let: "acc" := Alloc #0 in
    while: #0 < !"n" do "acc" <- !"acc" + "m";; "n" <- !"n" - #1 od ;;
    let: "mv" := !"acc" in
    Free "n";;
    Free "acc";;
    "mv".

  Lemma mul_sim (n m : nat) π :
    ⊢ mul_loop #n #m ⪯{π} #n * #m {{ val_rel }}.
  Proof.
    assert (∃ (v : Z), v = n ∧ True) as [v Hv] by eauto. (* needed because compute is used somewhere, unfolding everything *)
    replace (Z.of_nat n) with v; last apply Hv.
    assert (∃ (w : Z), w = m ∧ True) as [w Hw] by eauto.
    replace (Z.of_nat m) with w; last apply Hw.
    iSteps as (l1 l2) "Hl1 ? Hl2 ?". destruct Hv as [-> _]. destruct Hw as [-> _].
    iAssert (l2 ↦t #((n - n) * m))%I with "[Hl2]" as "Hl2".
    { by replace ((n - n) * m)%Z with Z0 by lia. }
    generalize n at 2 4 => n0.
    iInduction n as [|n] "IH" forall "Hl1 Hl2".
    - target_while. iApply target_red_base.
      iSteps. iPureIntro. f_equal. lia.
    - target_while. iApply target_red_base.
      iSteps. replace (S n - 1)%Z with (Z.of_nat n) by lia.
      iSteps. replace ((n0 - S n) * m + m)%Z with ((n0 - n) * m)%Z by lia.
      iSteps.
  Qed.

  Lemma mul_sim' (n m : nat) π :
    ⊢ #(n * m) ⪯{π} mul_loop #n #m  {{ val_rel }}.
  Proof.
    assert (∃ (v : Z), v = n ∧ True) as [v Hv] by eauto.
    replace (Z.of_nat n) with v; last apply Hv.
    assert (∃ (w : Z), w = m ∧ True) as [w Hw] by eauto.
    replace (Z.of_nat m) with w; last apply Hw.
    iSteps as (l1 l2) "Hl1 ? Hl2 ?". destruct Hv as [-> _]. destruct Hw as [-> _].
    iAssert (l2 ↦s #((n - n) * m))%I with "[Hl2]" as "Hl2".
    { by replace ((n - n) * m)%Z with Z0 by lia. }
    generalize n at 2 4 => n0.
    iInduction n as [|n] "IH" forall "Hl1 Hl2".
    - source_while. iApply source_red_base.
      iSteps. iPureIntro. f_equal. lia.
    - source_while. iApply source_red_base.
      iSteps. replace (S n - 1)%Z with (Z.of_nat n) by lia.
      iSteps. replace ((n0 - S n) * m + m)%Z with ((n0 - n) * m)%Z by lia.
      iSteps.
  Qed.


  Definition input_loop : expr :=
    let: "cont" := Alloc #true in
    while: !"cont" do
      "cont" <- Call f#"external" #()
    od.


  Definition input_rec : func :=
    λ: "cont",
      if: "cont" then
        let: "cont" := Call f#"external" #() in
        Call f#"rec" "cont"
      else #().

  Lemma loop_rec :
    "rec" @s input_rec -∗
    log_rel input_loop (Call f#"rec" #true).
  Proof. 
    iSteps as "Hrec". log_rel. iSteps as (π l) "Hl Hdie".
    iApply (sim_while_rec (λ v_s, ∃ v_t, l ↦t v_t ∗ val_rel v_t v_s)%I with "[-] Hrec"); [iSteps | ].
    iSteps as (b) "Hl". destruct b; iSteps.
    iApply sim_expr_base. iSteps.
  Qed.

  Lemma loop_rec' :
    "rec" @t input_rec -∗
    log_rel (Call f#"rec" #true) input_loop.
  Proof.
    iSteps as "Hrec". log_rel. iSteps as (π l) "Hl Hdie".
    iApply (sim_rec_while (λ v_t, ∃ v_s, l ↦s v_s ∗ val_rel v_t v_s)%I with "[-] Hrec"); [ iSteps | ].
    iSteps as (b) "Hl". destruct b; iSteps.
    iApply sim_expr_base. iSteps.
  Qed.
End fix_bi.