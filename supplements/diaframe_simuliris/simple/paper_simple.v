From simuliris.simulang Require Import lang notation tactics class_instances.
From iris.proofmode Require Import proofmode.
From simuliris.simulation Require Import slsls lifting .
From simuliris.simulang Require Import hoare behavior.
From simuliris.simulang.simple_inv Require Import inv adequacy.
From iris.prelude Require Import options.

From diaframe.simuliris.simple Require Import proof_automation.

(** * Examples from Section 2 of the paper *)
(** Here, we prove both the quadruples shown in the paper,
  and the logical relation (then deriving closed proofs of contextual refinement).

  Normally, we would not bother with the quadruples, since the statement we are really
   interested in is the contextual refinement.
 *)


Section examples.
  Context `{!simpleGS Σ}.

  (** Example from 2.1 *)
  Definition ex_2_1_unopt : expr := let: "y" := ref(#42) in !"y".
  Definition ex_2_1_opt : expr := let: "y" := ref(#42) in #42.

  Lemma ex_2_1 π :
    ⊢ {{{ True }}} ex_2_1_opt ⪯[π] ex_2_1_unopt {{{ lift_post (λ v_t v_s, ⌜v_t = v_s⌝) }}}.
  Proof. rewrite /ex_2_1_opt /ex_2_1_unopt. iSteps. Qed.

  (* for completeness : log_rel as described in Sec 4 *)
  Lemma ex_2_1_log :
    ⊢ log_rel ex_2_1_opt ex_2_1_unopt.
  Proof. log_rel. iSteps. Qed.

  (** First example from 2.2 *)
  Definition ex_2_2_1_unopt : expr :=
    let: "y" := ref(#42) in
    Call f#"f" #23;;
    !"y".
  Definition ex_2_2_1_opt : expr :=
    let: "y" := ref(#42) in
    Call f#"f" #23;;
    #42.

  Lemma ex_2_2_1 π :
    ⊢ {{{ True }}} ex_2_2_1_opt ⪯[π] ex_2_2_1_unopt {{{ lift_post (λ v_t v_s, ⌜v_t = v_s⌝) }}}.
  Proof. rewrite /ex_2_2_1_opt /ex_2_2_1_unopt. iSteps. Qed.

  Lemma ex_2_2_1_log :
    ⊢ log_rel ex_2_2_1_opt ex_2_2_1_unopt.
  Proof. log_rel. iSteps. Qed.

  (** Second example from 2.2 *)
  Definition ex_2_2_2_unopt : expr :=
    let: "y" := ref(#42) in
    let: "z" := !"y" in
    Call f#"f" "y";;
    !"y" + "z".
  Definition ex_2_2_2_opt : expr :=
    let: "y" := ref(#42) in
    let: "z" := #42 in
    Call f#"f" "y";;
    !"y" + "z".

  Lemma ex_2_2_2 π :
    ⊢ {{{ True }}} ex_2_2_2_opt ⪯[π] ex_2_2_2_unopt {{{ lift_post (λ v_t v_s, ⌜v_t = v_s⌝) }}}.
  Proof. rewrite /ex_2_2_2_opt /ex_2_2_2_unopt. iSteps. Qed.

  Lemma ex_2_2_2_log :
    ⊢ log_rel ex_2_2_2_opt ex_2_2_2_unopt.
  Proof. log_rel. iSteps. Qed.

  (** Example from 2.3 *)
  Definition ex_2_3_unopt : expr :=
    let: "p" := Call f#"f" #() in
    let: "x" := Fst "p" in
    let: "y" := Snd "p" in
    let: "z" := "x" `quot` "y" in
    if: "y" ≠ #0 then
      Call f#"g" "z"
    else Call f#"h" "z".

  Definition ex_2_3_opt : expr :=
    let: "p" := Call f#"f" #() in
    let: "x" := Fst "p" in
    let: "y" := Snd "p" in
    let: "z" := "x" `quot` "y" in
    Call f#"g" "z".

  Lemma ex_2_3 π :
    ⊢ {{{ True }}} ex_2_3_opt ⪯[π] ex_2_3_unopt {{{ lift_post (λ v_t v_s, True) }}}.
  Proof. rewrite /ex_2_3_opt /ex_2_3_unopt. iSteps. Qed.

  Lemma ex_2_3_log :
    ⊢ log_rel ex_2_3_opt ex_2_3_unopt.
  Proof. log_rel. iSteps. Qed.

  (** Example from 2.4 *)
  Definition ex_2_4_unopt : expr :=
    let: "x" := ref(#0) in
    while: (Call f#"f" (!"x")) do
      Call f#"g" (!"x")
    od.
  Definition ex_2_4_opt : expr :=
    let: "x" := ref(#0) in
    let: "r" := !"x" in
    while: (Call f#"f" "r") do
      Call f#"g" "r"
    od.

  Lemma ex_2_4 π :
    ⊢ {{{ True }}} ex_2_4_opt ⪯[π] ex_2_4_unopt {{{ lift_post (λ v_t v_s, True) }}}.
  Proof.
    rewrite /ex_2_4_opt /ex_2_4_unopt.
    iSteps as (x_s x_t) "????".
    iApply (sim_while_while (†x_s…s 1 ∗ †x_t…t 1 ∗ x_s ↦s #0 ∗ x_t ↦t #0)%I with "[-]"); 
      iSteps as (b) "????".
    destruct b; iSteps as (v1 v2) "??".
    iApply sim_expr_base. iSmash.
  Qed.

  Lemma ex_2_4_log :
    ⊢ log_rel ex_2_4_opt ex_2_4_unopt.
  Proof.
    log_rel. iSteps as (π x_s x_t) "????".
    iApply (sim_while_while (†x_s…s 1 ∗ †x_t…t 1 ∗ x_s ↦s #0 ∗ x_t ↦t #0)%I with "[-]"); iSteps as (b) "????".
    destruct b; iSteps as (v1 v2) "??".
    iApply sim_expr_base. iSmash.
  Qed.

End examples.


Section closed.
  (** Obtain a closed proof of [ctx_ref]. *)
  Lemma ex_2_1_ctx : ctx_ref ex_2_1_opt ex_2_1_unopt.
  Proof.
    set Σ := #[simpleΣ].
    apply (log_rel_adequacy Σ)=>?.
    apply ex_2_1_log.
  Qed.

  Lemma ex_2_2_1_ctx : ctx_ref ex_2_2_1_opt ex_2_2_1_unopt.
  Proof.
    set Σ := #[simpleΣ].
    apply (log_rel_adequacy Σ)=>?.
    apply ex_2_2_1_log.
  Qed.

  Lemma ex_2_2_2_ctx : ctx_ref ex_2_2_2_opt ex_2_2_2_unopt.
  Proof.
    set Σ := #[simpleΣ].
    apply (log_rel_adequacy Σ)=>?.
    apply ex_2_2_2_log.
  Qed.

  Lemma ex_2_3_ctx : ctx_ref ex_2_3_opt ex_2_3_unopt.
  Proof.
    set Σ := #[simpleΣ].
    apply (log_rel_adequacy Σ)=>?.
    apply ex_2_3_log.
  Qed.

  Lemma ex_2_4_ctx : ctx_ref ex_2_4_opt ex_2_4_unopt.
  Proof.
    set Σ := #[simpleΣ].
    apply (log_rel_adequacy Σ)=>?.
    apply ex_2_4_log.
  Qed.
End closed.
