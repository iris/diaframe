From simuliris.simulang Require Import lang notation tactics class_instances.
From iris.proofmode Require Import proofmode.
From simuliris.simulation Require Import slsls lifting .
From simuliris.simulang Require Import hoare behavior.
From simuliris.simulang.simple_inv Require Import inv adequacy.
From iris.prelude Require Import options.

From diaframe.simuliris Require Export proof_automation_base.


Global Existing Instance load_na_step_source.
Global Existing Instance load_na_step_target.
Global Existing Instance store_na_step_target.
Global Existing Instance store_na_step_source.

Section simple_autom.
  Context `{!simpleGS Σ}.

  Global Instance bij_insert l_t l_s v_t v_s :
    BiAbd (TTl := [tele]) (TTr := [tele]) false (l_t ↦t v_t) (l_t ↔h l_s)%I (update_si)
    (l_s ↦s v_s ∗ †l_t…t 1 ∗ †l_s…s 1 ∗ val_rel v_t v_s)%I (l_t ↔h l_s)%I.
  Proof.
    iStep as (Hlt_block Hls_block) "Hvs Hlt Hls Hlt_size Hls_size".
    rewrite /update_si.
    iIntros (?????) "(HP_t & HP_s & Hσ_t & Hσ_s & (%L&Hinv&#Hgs))".
    iMod (heapbij_insertN _ l_t l_s [v_t] [v_s] 1 with "Hinv [Hlt] [Hls] [Hvs] [$] [$]") as "[Hb #Ha]"; try (done || eauto).
    - by rewrite heap_pointsto_vec_singleton.
    - by rewrite heap_pointsto_vec_singleton.
    - simpl. iSteps.
    - iFrame "# ∗". iSteps.
  Qed.

  Typeclasses Opaque loc_rel heap_freeable.

  Global Instance sim_load_spec (l_t l_s : loc) :
    SimultaneousSpec (! #l_t) (! #l_s) (ε₁)%I (λ π Φ, 
      l_t ↔h l_s ∗ (∀ v_t v_s, val_rel v_t v_s -∗ Φ (of_val v_t) (of_val v_s)))%I.
  Proof.
    split.
    - iSteps. sim_load v_t v_s as "Hv".
      iSteps.
    - iSteps as (Φ Ψ π HΦΨ v1 v2) "Hl Hv HΦ" /
             as (Φ Ψ π HΦΨ v1 v2) "Hl Hv HΦ";
      iApply HΦΨ; iSteps.
  Qed.
End simple_autom.

Global Typeclasses Opaque loc_rel heap_freeable.