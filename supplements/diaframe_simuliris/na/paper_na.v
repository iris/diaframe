From iris Require Import bi.bi.
From iris.proofmode Require Import proofmode.
From simuliris.simulation Require Import slsls lifting.
From simuliris.simulang Require Import lang notation tactics
  proofmode log_rel_structural wf behavior hoare.
From simuliris.simulang.na_inv Require Export inv.
From simuliris.simulang.na_inv Require Import readonly_refl adequacy.
From iris.prelude Require Import options.

From diaframe.simuliris.na Require Import proof_automation.

Section data_race.
  Context `{naGS Σ}.

  (** ** Example from 3.2 *)

  Definition load_na_sc_unopt : expr :=
    "x" <- #42;;
    !ˢᶜ "y";;
    !"x".
  Definition load_na_sc_opt : expr :=
    "x" <- #42;;
    !ˢᶜ "y";;
    #42.

  (** For completeness: the triple shown in the paper.
    (really, we usually want to prove [load_na_sc_log] below).
  *)
  Lemma load_na_sc_sim π (x_t x_s y_s y_t : loc) :
    ⊢ {{{ x_t ↔h x_s ∗ y_t ↔h y_s ∗ na_locs π ∅ }}}
        subst "x" #x_t $ subst "y" #y_t $ load_na_sc_opt ⪯[π]
        subst "x" #x_s $ subst "y" #y_s $ load_na_sc_unopt
      {{{ lift_post (λ v_t v_s, ⌜v_t = v_s⌝ ∗ na_locs π ∅) }}}.
  Proof.
    rewrite /load_na_sc_opt /load_na_sc_unopt.
    simpl_subst. iSteps as (v1 v2) "H↔x H↔y Hv Hx_s Hx_t".
    destruct (decide (y_s = x_s)) as [ -> | Hneq].
    - rewrite lookup_insert.
      iPoseProof (heapbij_loc_inj with "H↔x H↔y") as "<-".
      iSteps.
    - (* don't alias *)
      rewrite lookup_insert_ne // lookup_empty.
      iSteps.
  Qed.

  (** The statement we really want (as described in Section 4): a [log_rel]. *)
  Lemma load_na_sc_log :
    ⊢ log_rel load_na_sc_opt load_na_sc_unopt.
  Proof.
    (* closes open variables with related values *)
    log_rel. iSteps as (π x_s x_t v2 v1 y_s y_t) "H↔x Hv H↔y Hx_s Hx_t".
    destruct (decide (y_s = x_s)) as [ -> | Hneq].
    - rewrite lookup_insert /=.
      iPoseProof (heapbij_loc_inj with "H↔x H↔y") as "<-".
      iSteps.
    - (* don't alias *)
      rewrite lookup_insert_ne // lookup_empty.
      iSteps.
  Qed.


  (** ** Example from 3.2 (arbitrary readonly expressions *)

  Definition load_na_unopt e : expr :=
    "x" <- #42;;
    e;;
    !"x".
  Definition load_na_opt e : expr :=
    "x" <- #42;;
    e;;
    #42.

  (** We currently do not have good automation when we don't know a upper bound
    on the set of variables used by [e], therefore there's a bit of manual work
    regarding substitution involved here.
   *)
  Import gen_log_rel.
  Lemma load_na_log e :
    gen_expr_wf readonly_wf e →
    ⊢ log_rel (load_na_opt e) (load_na_unopt e).
  Proof.
    iIntros (Hwf). rewrite /log_rel. iSteps as (π m) "Hsubst Hlocs".

    iPoseProof (subst_map_rel_lookup _ "x" with "Hsubst") as "(%vx_t & %vx_s & %Heq & Hx')".
    { set_solver. }
    rewrite !lookup_fmap. rewrite !Heq. simpl.

    iSteps as (x_t x_s v2 v1) "H↔x Hv Hlocs Hx_s Hx_t".

    sim_bind (subst_map _ _) (subst_map _ _).
    iPoseProof (subst_map_rel_weaken _ _ (free_vars e) with "Hsubst") as "Hsubst'"; first set_solver.

    iPoseProof (subst_map_rel_dom with "Hsubst'") as "%Hdom".
    iApply (sim_refl' with "[] [-]").
    { rewrite !dom_fmap_L //. }
    { rewrite dom_fmap_L //. }
    { apply (readonly_log_rel_structural [(x_s, x_t, #42, #42, 1%Qp)]). }
    { done. }
    { rewrite map_fmap_zip. rewrite map_zip_diag. rewrite -map_fmap_compose.
      erewrite (map_fmap_ext _ id). { rewrite map_fmap_id. done. }
      move => i [x' y] Hl //. }
    { rewrite /readonly_thread_own. iFrame.
      unfold pointsto_list, na_locs_in_pointsto_list.
      iSplit; [|iSplit; [ iSplit; done | done]].
      iPureIntro. intros ???. rewrite lookup_insert_Some.
      rewrite lookup_empty. intros [[<- [= <- <-]] | [_ [=]]].
      exists 0. eauto. }
    iSteps.
  Qed.


  (** ** Optimization from the intro and 3.3 *)
  (** (In file [theories/simulang/na_inv/examples/data_races.v], there is a version of this
      where the memory is actually freed in the end)
   *)

  Definition hoist_load_both_unopt : expr :=
     let: "i" := ref #0  in
     let: "r" := ref (!"y") in
     while: !"i" ≠ ! "x" do
       "i" <- !"i" + #1;;
       "r" <- !"r" + !"y"
     od;;
     !"r".

  Definition hoist_load_both_opt : expr :=
     let: "n" := !"x" in
     let: "m" := !"y" in
     let: "i" := ref #0  in
     let: "r" := ref "m" in
     while: !"i" ≠ "n" do
       "i" <- !"i" + #1;;
       "r" <- !"r" + "m"
     od;;
     !"r".

  (** We directly prove a [log_rel], as that is the statement we really want. *)
  Lemma hoist_load_both_log:
    ⊢ log_rel hoist_load_both_opt hoist_load_both_unopt.
  Proof.
    log_rel.
    iSteps as (v1 v2 π li_s l_s li_t qx vx_s vx_t lr_s) 
      "Hv12 H↔2 Hvx Hli_s Hli_s▮ Hli_t Hlocs Hl_s Hlr_s Hlr_s▮".
    destruct (if v2 is #(LitLoc _) then true else false) eqn:Heq. 2: {
      source_while.
      source_bind (! _)%E. iApply source_red_safe_implies.
      iIntros ([? ?]); simplify_eq.
    }
    destruct v2 as [[| | | |ly_s |]| | |] => //.
    iDecompose "Hv12" as (l_t) "H↔3".

    (* case analysis: do [x] and [y] alias? *)
    destruct (decide (ly_s = l_s)) as [-> | Hneq].
    - (* they do alias, so we do not need to exploit a second time. *)
      iPoseProof (heapbij_loc_inj with "H↔2 H↔3") as "->".
      iSteps as (li_t lr_t) "Hl_t Hli_t Hli_t▮ Hlr_t Hlr_t▮".
      set inv := (
        ∃ (z_i : Z) (vr_t vr_s : val),
        l_t ↦t{#qx} vx_t ∗
        l_s ↦s{#qx} vx_s ∗
        li_s ↦s #z_i ∗
        li_t ↦t #z_i ∗
        lr_t ↦t vr_t ∗
        lr_s ↦s vr_s ∗
        val_rel vr_t vr_s ∗
        na_locs π (<[l_s:=(l_t, NaRead qx)]> ∅))%I.

      sim_bind (While _ _) (While _ _).
      iApply (sim_while_while inv with "[-]"); iSteps.
      + rewrite bool_decide_false //.
        iSteps. iApply sim_expr_base. iSteps.
      + rewrite bool_decide_true //. iStep 4. iStepDebug. iLeft. iSteps.
    - (* the proof of this case is very similar to the one above, except that we carry around
        the additional locations in the invariant and have to release both in the end. *)
      iApply (sim_bij_exploit_load with "H↔3 Hlocs"); [| |]. {
        intros.
        safe_reach_fill (While _ _)%E => /=.
        apply: safe_reach_pure; [eapply pure_while | done | ].
        safe_reach_fill (Load _ _)%E.
        apply: safe_reach_safe_implies => ?.
        apply: safe_reach_refl. apply: post_in_ectx_intro. naive_solver.
      }
      { rewrite lookup_insert_ne; last done. apply lookup_empty. }
      iSteps as (qy vy_t vy_s li_t' lr_t)
          "Hvy Hly_s Hlocs Hl_t Hlx_t Hli_t Hli_t▮ Hlr_t Hlr_t▮". 
      rename li_t into lx_t. rename l_s into lx_s. rename l_t into ly_t.

      set inv := (
        ∃ (z_i : Z) (vr_t vr_s : val),
        lx_t ↦t{#qx} vx_t ∗ lx_s ↦s{#qx} vx_s ∗
        ly_t ↦t{#qy} vy_t ∗ ly_s ↦s{#qy} vy_s ∗
        li_s ↦s #z_i ∗      li_t' ↦t #z_i ∗
        lr_t ↦t vr_t ∗      lr_s ↦s vr_s ∗
        val_rel vr_t vr_s ∗
        na_locs π (<[ly_s:=(ly_t, NaRead qy)]> $ <[lx_s:=(lx_t, NaRead qx)]> ∅))%I.

      sim_bind (While _ _) (While _ _).
      iApply (sim_while_while inv with "[-] []"); first iSteps.
      rewrite /inv. clear inv. 
      iSteps as (z1 z2 Hz2 z3) "Hly_t Hlocs Hli_t Hly_s Hli_s Hlr_t Hlx_t Hlx_s Hlr_s" /
             as (z v1 v2) "Hv12 Hlx_t Hlx_s Hlr_t Hlr_s Hlocs Hli_t' Hli_s Hly_t Hly_s".
      (* TODO: coq var renaming breaks for let bindings *)
      + destruct (bool_decide (#z2 = vy_t)) eqn:Heq_vy_t.
        { (* contradiction *)
          apply bool_decide_eq_true_1 in Heq_vy_t. subst vy_t.
          val_discr_target "Hvy". done.
        }
        iSteps. iApply sim_expr_base. iSteps.
      + rewrite bool_decide_true //. do 4 iStep. iStepDebug. iLeft. 
        iSteps. rewrite delete_notin; last first.
        { rewrite lookup_insert_ne //. }
        iSteps.
    Qed.

End data_race.

(* Deriving contextual refinements *)
Section closed.
  Lemma load_na_sc_ctx :
    ctx_ref load_na_sc_opt load_na_sc_unopt.
  Proof.
    intros ??.
    set Σ := #[naΣ].
    apply (log_rel_adequacy Σ)=>?.
    by apply load_na_sc_log.
  Qed.

  Lemma load_na_ctx e :
    gen_expr_wf readonly_wf e →
    ctx_ref (load_na_opt e) (load_na_unopt e).
  Proof.
    intros ??.
    set Σ := #[naΣ].
    apply (log_rel_adequacy Σ)=>?.
    by apply load_na_log.
  Qed.

  Lemma hoist_load_both_ctx :
    ctx_ref hoist_load_both_opt hoist_load_both_unopt.
  Proof.
    intros ??.
    set Σ := #[naΣ].
    apply (log_rel_adequacy Σ)=>?.
    by apply hoist_load_both_log.
  Qed.
End closed.
