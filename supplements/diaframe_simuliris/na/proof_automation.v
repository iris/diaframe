From iris Require Import bi.bi.
From iris.proofmode Require Import proofmode.
From simuliris.simulation Require Import slsls lifting.
From simuliris.simulang Require Import lang notation tactics
  proofmode log_rel_structural wf behavior hoare.
From simuliris.simulang.na_inv Require Export inv.
From simuliris.simulang.na_inv Require Import readonly_refl adequacy.
From iris.prelude Require Import options.

From diaframe.symb_exec Require Import defs.
From diaframe.simuliris Require Export proof_automation_base.


Global Existing Instance load_na_step_source.
Global Existing Instance load_na_step_target.
Global Existing Instance store_na_step_target.
Global Existing Instance load_sc_step_source.
Global Existing Instance load_sc_step_target.

Global Typeclasses Opaque loc_rel heap_freeable.

Global Hint Extern 4 (ReshapeExprAnd _ _ _ _ _) => tc_solve : solve_pure_add.


(* make simpl unfold this *)
Global Arguments pointsto_list {_ _} _ /.

Section big_op_map_instances.
  Context {PROP : bi}.
  Context `{!EqDecision K, !Countable K} {A : Type}.
  Implicit Types k : K.
  Implicit Types m : gmap K A.
  Implicit Types P : K → A → PROP.

  Global Instance big_op_map_insert_hint m k v P :
    HINT ε₀ ✱ [- ; P k v ∗ big_opM bi_sep P $ delete k m] ⊫ [id]; big_opM bi_sep P $ insert k v m ✱ [True].
  Proof.
    iStep. rewrite -insert_delete_insert.
    rewrite big_sepM_insert //; first iSteps.
    apply lookup_delete.
  Qed.

  Class IsEmptyMap m :=
    is_empty_map : m = ∅.

  Global Instance empty_is_empty :
    IsEmptyMap (@empty (@gmap K EqDecision0 Countable0 A) _) | 10.
  Proof. done. Qed.

  Global Instance delete_empty_is_empty k m :
    IsEmptyMap m →
    IsEmptyMap (delete k m) | 20.
  Proof. move => ->. rewrite delete_empty. tc_solve. Qed.

  Global Instance big_op_map_on_empty_hint m P :
    IsEmptyMap m →
    HINT ε₀ ✱ [-; emp] ⊫ [id]; big_opM bi_sep P m ✱ [True].
  Proof.
    rewrite /BiAbd /= => ->.
    iStep. rewrite big_opM_empty. iSteps.
  Qed.

  Class TCIndexIn m k (ma : option A) :=
    tc_index_in : m !! k = ma.

  Global Instance index_in_empty_map m k :
    IsEmptyMap m → TCIndexIn m k None.
  Proof. rewrite /TCIndexIn => ->. rewrite lookup_empty //. Qed.

  Global Instance index_in_exactly m k v :
    TCIndexIn (<[k := v]> m) k (Some v).
  Proof. rewrite /TCIndexIn lookup_insert //. Qed.

  Global Instance index_in_skip m k v k2 mv:
    SolveSepSideCondition (k2 ≠ k) →
    TCIndexIn m k mv →
    TCIndexIn (<[k2 := v]> m) k mv.
  Proof.
    rewrite /TCIndexIn /SolveSepSideCondition => Hk2 <-.
    by apply lookup_insert_ne.
  Qed.
End big_op_map_instances.

Global Hint Extern 4 (TCIndexIn _ _ _) => tc_solve : solve_pure_add.

Section instance.
  Context `{!naGS Σ}.

  Global Instance load_na_step_source_lastresort w (l : loc) `{!sheapInvSupportsLoad Na1Ord}  : 
    ReductionStep (source_red_cond, w) (! #l)%E ⊣ ⟨id⟩ ∃ q v, l ↦s{#q} v ; ε₁ =[▷^0]=> 
      ⟨id⟩ v ⊣ l ↦s{#q} v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (π Φ q v) "Hl HΦ".
    iApply (source_red_load_na with "Hl"). iSteps.
  Qed.

  Global Instance store_na_step_source w (l : loc) (v : val) : 
    ReductionStep (source_red_cond, w) (#l <- v)%E ⊣ ⟨id⟩ ∃ v', l ↦s v' ; ε₁ =[▷^0]=> 
      ⟨id⟩ #() ⊣ l ↦s v.
  Proof.
    rewrite /ReductionStep' /ReductionTemplateStep /=.
    iSteps as (π Φ v') "Hl HΦ".
    iApply (source_red_store_na with "Hl"). iSteps.
  Qed.

  Instance sim_free_spec1 (l_t l_s : loc) col :
    SimultaneousSpec (FreeN #1 #l_t) (FreeN #1 #l_s) (l_t ↔h l_s) (λ π Φ, 
      na_locs π col ∗ ⌜col !! l_s = None⌝ ∗ (na_locs π col -∗ Φ #() #()))%I.
  Proof.
    split.
    - iStep 3 as (Φ π Hls) "Hl Hπ HΦ". iApply (sim_bij_free with "Hl Hπ"). 
      { intros. replace i with Z0 by lia. rewrite loc_add_0 //. }
      iSteps.
    - iSteps as (Φ Ψ π HΦΨ Hls) "Hπ HΦ" / 
             as (Φ Ψ π HΦΨ Hls) "Hπ HΦ";
      iApply HΦΨ; iSteps.
  Qed.

  Inductive StrongMemoryOpOn (l : loc) : expr → Prop :=
      store_strong_op (v : val) : StrongMemoryOpOn l (#l <- v)%E
    | free_strong_op (n : Z) : StrongMemoryOpOn l (FreeN #n #l).

  Existing Class StrongMemoryOpOn.
  Global Existing Instance store_strong_op.
  Global Existing Instance free_strong_op.

  Inductive MemoryOpOn (l : loc) : expr → bool → Prop :=
      strong_memory_op_is_op e : StrongMemoryOpOn l e → MemoryOpOn l e true
    | load_memory_op : MemoryOpOn l (! #l)%E false.
  Existing Class MemoryOpOn.
  Global Existing Instance strong_memory_op_is_op.
  Global Existing Instance load_memory_op.

  Global Instance bij_insert l_t l_s:
    HINT ε₁ ✱ [v_t v_s ;l_t ↦t v_t ∗ l_s ↦s v_s ∗ †l_t…t 1 ∗ †l_s…s 1 ∗ val_rel v_t v_s] ⊫ [update_si]; l_t ↔h l_s ✱ [l_t ↔h l_s].
  Proof.
  Typeclasses Transparent loc_rel heap_freeable.
    iStep 3 as (v_t v_s Hlt_block Hls_block) "Hvs Hlt Hls Hlt_size Hls_size". rewrite /update_si.
    iIntros (?????) "(HP_t & HP_s & Hσ_t & Hσ_s & Hinv)".
    iDestruct "Hinv" as (L cols ? ? HL) "(Hcols & Hbij & Hgs)".
    iMod (heapbij_insertN _ _ _ [_] [_] with "Hbij [Hlt] [Hls] [Hvs] [$] [$]") as "[Hbij #?]"; [lia|done..| | | | | ].
    - move => o ? HLs. rewrite /alloc_rel_pred combine_na_locs_list_None //.
      move => π' cols' Hcols.
      destruct (cols' !! (l_s +ₗ o)) eqn: Hk => //.
      exfalso. have /=[??]:= HL _ _ _ _ Hcols Hk. by apply: HLs.
    - rewrite heap_pointsto_vec_singleton. iSteps.
    - rewrite heap_pointsto_vec_singleton. iSteps.
    - simpl. iSteps.
    - iModIntro. iFrame "# ∗".
      iPureIntro. split_and!; [done..|].
      by apply: na_locs_in_L_extend.
  Typeclasses Opaque loc_rel heap_freeable.
  Qed.

  Instance sim_bij_exploit_store_biabd π l_t (l_s : loc) e_s :
    BiAbd (TTl := [tele_pair (gmap loc (loc * na_locs_state) ); expr → expr; val]) (TTr := [tele_pair val]) false (l_t ↔h l_s) (λ v_s, l_s ↦s v_s)%I (update_si_strong e_s π)
      (λ col K v', na_locs π col ∗ ⌜col !! l_s = None⌝ ∗ ⌜ReshapeExprAnd expr e_s K (#l_s <- v')%E 
      (TCAnd (SatisfiesContextCondition context_as_item_condition K) TCTrue)⌝)%I (λ col K _ v_s, ∃ v_t, l_t ↦t v_t ∗ val_rel v_t v_s ∗ na_locs π (<[l_s := (l_t, NaExcl)]> col))%I.
  Proof.
    rewrite /BiAbd /= => col K v'. Typeclasses Transparent loc_rel.
    iIntros "[#[Hbij %Hidx] [Hcol [%Hcol %HK]]]". Typeclasses Opaque loc_rel.
    destruct HK as [HK1 [HK2 Hls]]. destruct HK2. rewrite -H in HK1.
    assert ((∀ P_s σ_s, safe_reach P_s e_s σ_s (post_in_ectx (λ e' σ', ∃ v' : val, e' = Store Na1Ord #l_s v' ∧ ∃ v, σ_s.(heap) !! l_s = Some (RSt 0, v))))).
    { intros. rewrite HK1. apply: fill_safe_reach. apply: safe_reach_safe_implies => [[l [v [[= <-] Hlv2]]]].
      apply safe_reach_refl. apply post_in_ectx_intro. eauto. }
    destruct l_s as [b_s o], l_t as [b_t o']; simplify_eq/=. rewrite /update_si_strong.
    iIntros (??????) "($&$&$&Hσ_s&Hinv) [%HT %Hsafe]".
    iDestruct (heap_ctx_wf with "Hσ_s") as %?.
    have [?[?[[?[?[_ [?[_ [??]]]]]] _]]]:= pool_safe_safe_reach _ _ _ _ _ _ ltac:(done) ltac:(done) ltac:(done) ltac:(done).
    iDestruct (na_bij_access with "Hinv Hbij") as (cols ? Hwf) "(Hcols&Halloc&Hclose)".
    iDestruct (ghost_map.ghost_map_lookup with "Hcols Hcol") as %Hcols. rewrite lookup_map_seq_0 in Hcols.
    move: (Hcols) => /(lookup_lt_Some _ _ _) ?.
    set (cols' := <[π := (<[Loc b_s o := (Loc b_t o, NaExcl)]>col)]> cols).
    unshelve epose proof (na_locs_wf_combined_state_store _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _) as Hcom; shelve_unifiable; 
    [done|done |  |done|done| |]. 2: {
      move => ??. apply: safe_reach_mono; [done|]. move => ???. apply: post_in_ectx_mono; [done|]. naive_solver.
    } { done. } 
    rewrite Hcol /= in Hcom.
    iMod (alloc_rel_remove_frac (λ _, alloc_rel_pred cols') with "Halloc Hσ_s") as (v_t) "(H1&H2&H3&H4&H5)".
    { done. }
    { rewrite /alloc_rel_pred => q. by erewrite Hcom. }
    { by rewrite /alloc_rel_pred combine_na_locs_list_insert //= Hcom. }
    { move => q o' ? /=. rewrite /alloc_rel_pred combine_na_locs_list_partial_alter_ne // => -[?]. done. }
    { done. }
    { done. }
    iMod (ghost_map.ghost_map_update with "Hcols Hcol") as "[Hcols Hcol]". rewrite insert_map_seq_0 //.
    iModIntro. iFrame "H5 Hcol". iSplitR "H1 H2 H3"; last iSteps.
    iApply ("Hclose" with "[] [] [] [] [$] [$]"); iPureIntro.
    - by rewrite length_insert.
    - apply: na_locs_wf_insert_store; [done..| by rewrite list_insert_id | ].
      move => ?. apply: safe_reach_mono; [done|]. move => ???. apply: post_in_ectx_mono; [done|]. naive_solver.
    - move => ???? /list_lookup_insert_Some[[?[??]]|[??]] Hl'; simplify_eq; [|naive_solver].
      move: Hl' => /lookup_insert_Some[[??]|[??]]; simplify_map_eq; naive_solver.
    - move => ????. rewrite /alloc_rel_pred combine_na_locs_list_partial_alter_ne // => -[??]; simplify_eq.
  Qed.

  Instance sim_bij_exploit_store_biabd_no_λ π l_t (l_s : loc) e_s :
    BiAbd (TTl := [tele_pair (gmap loc (loc * na_locs_state) ); expr → expr; val]) (TTr := [tele_pair val]) false (l_t ↔h l_s) ((heap_pointsto sheapG_heap_source l_s (RSt 0) 1))%I (update_si_strong e_s π)
      (λ col K v', na_locs π col ∗ ⌜col !! l_s = None⌝ ∗ ⌜ReshapeExprAnd expr e_s K (#l_s <- v')%E (TCAnd (SatisfiesContextCondition context_as_item_condition K) TCTrue)⌝)%I (λ col K v' v_s, ∃ v_t, l_t ↦t v_t ∗ val_rel v_t v_s ∗ na_locs π (<[l_s := (l_t, NaExcl)]> col))%I.
  Proof.
    change (heap_pointsto sheapG_heap_source l_s (RSt 0) 1) with (λ v, l_s ↦s v)%I. 
    tc_solve.
  Qed.

  Global Instance sim_bij_exploit_load_biabd π l_t (l_s : loc) e_s :
    BiAbd (TTl := [tele_pair (gmap loc (loc * na_locs_state) ); expr → expr]) (TTr := [tele_pair Qp; val]) false (l_t ↔h l_s) (λ q v_s, l_s ↦s{#q} v_s)%I (update_si_strong e_s π)
      (λ col K, na_locs π col ∗ ⌜col !! l_s = None⌝ ∗ ⌜ReshapeExprAnd expr e_s K (! #l_s)%E (SatisfiesContextCondition context_as_item_condition K)⌝)%I (λ col K q v_s, ∃ v_t, l_t ↦t{#q} v_t ∗ val_rel v_t v_s ∗ na_locs π (<[l_s := (l_t, NaRead q)]> col))%I.
  Proof.
    rewrite /BiAbd /= => col K. Typeclasses Transparent loc_rel.
    iIntros "[#[Hbij %Hidx] [Hcol [%Hcol %HK]]]". Typeclasses Opaque loc_rel.
    destruct HK as [HK1 HK2]. destruct HK2. rewrite -H in HK1.
    assert ((∀ P_s σ_s, safe_reach P_s e_s σ_s (post_in_ectx (λ e' σ', e' = Load Na1Ord #l_s ∧ ∃ n v, σ_s.(heap) !! l_s = Some (RSt n, v))))).
    { intros. rewrite HK1. apply: fill_safe_reach. apply: safe_reach_safe_implies => [[l [v [n [[= <-] Hlv2]]]]].
      apply safe_reach_refl. apply post_in_ectx_intro. eauto. }
    destruct l_s as [b_s o], l_t as [b_t o']; simplify_eq/=. rewrite /update_si_strong.
    iIntros (??????) "($&$&$&Hσ_s&Hinv) [%HT %Hsafe]".
    iDestruct (heap_ctx_wf with "Hσ_s") as %?.
    have [?[?[[?[?[_ [_ [?[??]]]]]] _]]]:= pool_safe_safe_reach _ _ _ _ _ _ ltac:(done) ltac:(done) ltac:(done) ltac:(done).
    iDestruct (na_bij_access with "Hinv Hbij") as (cols ? Hwf) "(Hcols&Halloc&Hclose)".
    iDestruct (ghost_map.ghost_map_lookup with "Hcols Hcol") as %Hcols. rewrite lookup_map_seq_0 in Hcols.
    move: (Hcols) => /(lookup_lt_Some _ _ _) ?.
    unshelve epose proof (na_locs_wf_combined_state_load  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _) as Hcom; shelve_unifiable; 
    [done|done | |done|done | |]. 2: {
      move => ??. apply: safe_reach_mono; [done|]. move => ???. apply: post_in_ectx_mono; [done|]. naive_solver.
    } { done. }
    destruct Hcom as [q Hcom]. rewrite Hcol /= in Hcom.
    set (q1 := (default 1%Qp (q ≫= (λ q', 1 - q')%Qp))).
    set (cols' := <[π := (<[Loc b_s o := (Loc b_t o, NaRead (q1 / 2)%Qp)]>col)]> cols).
    iDestruct (alloc_rel_P_holds with "Halloc Hσ_s" ) as %[q'' Hcom']; [done|].
    rewrite /alloc_rel_pred Hcom in Hcom'.
    iMod (alloc_rel_remove_frac (λ _, alloc_rel_pred cols') _ q1 (Some (q1 / 2)%Qp) ((q1 / 2))%Qp with "Halloc Hσ_s") as (v_t) "(?&?&?&?&H5)".
    { done. }
    { rewrite /alloc_rel_pred => q'. rewrite Hcom /q1. destruct q, q' => //= Hx. symmetry in Hx.
      by move: Hx => /Qp.sub_Some ->. }
    { rewrite /alloc_rel_pred combine_na_locs_list_insert // Hcom /=. destruct q => /=.
      - unfold q1 => /=. simplify_eq/=. destruct q'' => //; simplify_eq/=. symmetry in Hcom'.
        move: Hcom' => /Qp.sub_Some Hq. rewrite Hq /=.
        rewrite [(_/2 + _)%Qp](comm) -assoc Qp.div_2. symmetry. by apply/Qp.sub_Some.
      - by rewrite /q1 /= Qp.div_2.
    }
    { move => q' o' ? /=. rewrite /alloc_rel_pred combine_na_locs_list_partial_alter_ne // => -[?]. done. }
    { by rewrite Qp.div_2. }
    { done. }
    iMod (ghost_map.ghost_map_update with "Hcols Hcol") as "[Hcols Hcol]". rewrite insert_map_seq_0 //.
    iModIntro. iFrame "H5 Hcol". iRename select (alloc_rel _ _ _) into "Ha". iSplitL "Hcols Hclose Ha"; last first. 
    { eauto with iFrame. }
    iApply ("Hclose" with "[] [] [] [] [$] [$]"); iPureIntro.
    - by rewrite length_insert.
    - apply: na_locs_wf_insert_load; [done..| by rewrite list_insert_id | ].
      move => ?. apply: safe_reach_mono; [done|]. move => ???. apply: post_in_ectx_mono; [done|]. naive_solver.
    - move => ???? /list_lookup_insert_Some[[?[??]]|[??]] Hl'; simplify_eq; [|naive_solver].
      move: Hl' => /lookup_insert_Some[[??]|[??]]; simplify_map_eq; naive_solver.
    - move => ????. rewrite /alloc_rel_pred combine_na_locs_list_partial_alter_ne // => -[??]; simplify_eq.
  Qed.

  Global Instance sim_bij_exploit_load_biabd_no_λ π l_t (l_s : loc) e_s :
    BiAbd (TTl := [tele_pair (gmap loc (loc * na_locs_state) ); expr → expr]) (TTr := [tele_pair Qp; val]) false (l_t ↔h l_s) (heap_pointsto sheapG_heap_source l_s (RSt 0))%I (update_si_strong e_s π)
      (λ col K, na_locs π col ∗ ⌜col !! l_s = None⌝ ∗ ⌜ReshapeExprAnd expr e_s K (! #l_s)%E (SatisfiesContextCondition context_as_item_condition K)⌝)%I (λ col K q v_s, ∃ v_t, l_t ↦t{#q} v_t ∗ val_rel v_t v_s ∗ na_locs π (<[l_s := (l_t, NaRead q)]> col))%I.
  Proof.
    change (heap_pointsto sheapG_heap_source l_s (RSt 0)) with (λ q v, l_s ↦s{#q} v)%I.
    tc_solve.
  Qed.

  Lemma sim_bij_release_update ns π e_s col l_s l_t v_t v_s:
    let q := if ns is NaRead q then q else 1%Qp in
    col !! l_s = Some (l_t, ns) →
    l_t ↔h l_s -∗
    na_locs π col -∗
    l_t ↦t{#q} v_t -∗
    l_s ↦s{#q} v_s -∗
    val_rel v_t v_s -∗
    update_si_strong e_s π (na_locs π (delete l_s col)).
  Proof.
    iIntros (q Hl_s). rewrite /loc_rel. iIntros "#[Hbij %Hidx] Hcol Hl_t Hl_s Hv".
    destruct l_s as [b_s o], l_t as [b_t o']; simplify_eq/=. rewrite /update_si_strong.
    iIntros (??????) "($&$&$&Hσ_s&Hinv) [%HT %Hsafe]".
    iDestruct (na_bij_access with "Hinv Hbij") as (cols ? Hwf) "(Hcols&Halloc&Hclose)".
    iDestruct (ghost_map.ghost_map_lookup with "Hcols Hcol") as %Hcols. rewrite lookup_map_seq_0 in Hcols.
    move: (Hcols) => /(lookup_lt_Some _ _ _) ?.
    set (cols' := <[π := (delete (Loc b_s o) col)]> cols).
    iMod (alloc_rel_add_frac (λ _, alloc_rel_pred cols') with "Halloc Hl_t Hl_s Hv Hσ_s") as "[? $]".
    { move => q' /=. rewrite /alloc_rel_pred (combine_na_locs_list_delete _ _ _ _ _ _ Hl_s Hcols) /=.
      rewrite -/cols' /q /combine_na_locs.
      repeat case_match; simplify_eq => //.
      - move => <-. rewrite assoc. f_equal. apply: comm.
      - by move => ->.
    }
    { move => q' o' ? /=. rewrite /alloc_rel_pred combine_na_locs_list_partial_alter_ne // => -[?]. done. }
    iMod (ghost_map.ghost_map_update with "Hcols Hcol") as "[Hcols Hcol]". rewrite insert_map_seq_0 //.
    iModIntro. iFrame "Hcol".
    iApply ("Hclose" with "[] [] [] [] [$] [$]"); iPureIntro.
    - by rewrite length_insert.
    - by apply: na_locs_wf_delete.
    - move => ???? /list_lookup_insert_Some[[?[??]]|[??]] Hl'; simplify_eq; [|naive_solver].
      move: Hl' => /lookup_delete_Some[??]; simplify_eq; naive_solver.
    - move => ????. rewrite /alloc_rel_pred combine_na_locs_list_partial_alter_ne // => -[??]; simplify_eq.
  Qed.

  Global Instance sim_bij_exploit_store_biabd_v2 π l_t (l_s : loc) e_s :
    BiAbd (TTl := [tele_pair (gmap loc (loc * na_locs_state) ); expr → expr; val; option (loc * na_locs_state)]) (TTr := [tele_pair val]) false (l_t ↔h l_s) (λ v_s, l_s ↦s v_s)%I (update_si_strong e_s π)
      (λ col K v' ma, na_locs π col ∗ ⌜TCIndexIn col l_s ma⌝ ∗ ⌜ReshapeExprAnd expr e_s K (#l_s <- v')%E 
      (TCAnd (SatisfiesContextCondition context_as_item_condition K) TCTrue)⌝
∗ (match ma with | None => True%I | Some (l_t', NaRead q) => ∃ vs vt, l_s ↦s{#q} vs ∗ ⌜l_t' = l_t⌝ ∗ l_t ↦t{#q} vt ∗ val_rel vt vs | _  => False%I end)
      )%I (λ col K _ ma v_s, ∃ v_t, l_t ↦t v_t ∗ val_rel v_t v_s ∗ na_locs π (<[l_s := (l_t, NaExcl)]> col))%I.
  Proof.
    rewrite /BiAbd /= => col K v' ma. destruct ma as [p0|]; last iSteps.
    destruct p0 as [l' [|q]]; first iStep.
    iStep as (HInd HK vl vs) "Hl Hvs Hπ Hls Hlt".
    iMod (sim_bij_release_update (NaRead q) _ e_s col l_s with "Hl Hπ Hlt Hls Hvs") as "H". { exact HInd. }
    iSteps. { iPureIntro. apply lookup_delete. }
    rewrite insert_delete_insert.
    iSteps.
  Qed.

  Global Instance sim_bij_exploit_store_biabd_no_λ_v2 π l_t (l_s : loc) e_s :
    BiAbd (TTl := [tele_pair (gmap loc (loc * na_locs_state) ); expr → expr; val; option (loc * na_locs_state)]) (TTr := [tele_pair val]) false (l_t ↔h l_s) ((heap_pointsto sheapG_heap_source l_s (RSt 0) 1))%I (update_si_strong e_s π)
      (λ col K v' ma, na_locs π col ∗ ⌜TCIndexIn col l_s ma⌝  ∗ ⌜ReshapeExprAnd expr e_s K (#l_s <- v')%E (TCAnd (SatisfiesContextCondition context_as_item_condition K) TCTrue)⌝
∗ (match ma with | None => True%I | Some (l_t', NaRead q) => ∃ vs vt, l_s ↦s{#q} vs ∗ ⌜l_t' = l_t⌝ ∗ l_t ↦t{#q} vt ∗ val_rel vt vs | _  => False%I end)
)%I (λ col K v' ma v_s, ∃ v_t, l_t ↦t v_t ∗ val_rel v_t v_s ∗ na_locs π (<[l_s := (l_t, NaExcl)]> col))%I.
  Proof.
    change (heap_pointsto sheapG_heap_source l_s (RSt 0) 1) with (λ v, l_s ↦s v)%I. 
    tc_solve.
  Qed.

  Global Instance dealloc_na_locs m1 π e : 
    BiAbd (TTl := [tele]) (TTr := [tele]) false (na_locs π m1) (na_locs π ∅) (update_si_strong e π) 
      ([∗ map] l ↦ pr ∈ m1, let q := if pr.2 is NaRead q then q else 1%Qp in ∃ v1 v2, l ↦s{#q} v1 ∗ pr.1 ↦t{#q} v2 ∗ pr.1 ↔h l ∗ val_rel v2 v1 )%I emp%I | 99.
  Proof.
    rewrite /BiAbd /=. revert m1.
    apply map_ind; iSteps as (l [l' s] m Hm H) "Hπ Hm".
    rewrite big_sepM_insert //.
    iDestruct "Hm" as "[Hl Hm]".
    iDestruct "Hl" as (v1 v2) "(Hlt & Hls & #Hlh & #Hlv)".
    set (q := match s with | NaExcl => 1%Qp | NaRead q' => q' end). unseal_diaframe => /=.
    iMod (sim_bij_release_update with "Hlh Hπ Hls Hlt Hlv") as "Hπ". { rewrite lookup_insert //. }
    rewrite delete_insert //.
    iCombine "Hπ Hm" as "H". rewrite H.
    iSteps.
  Qed.

  Global Instance dealloc_na_locs_one_insert m π e l_s l_t ns :
    TCIndexIn m l_s None →
    HINT (na_locs π (<[l_s := (l_t, ns)]> m)) ✱ [v_t v_s; l_t ↔h l_s ∗ (let q := if ns is NaRead q then q else 1%Qp in l_s ↦s{#q} v_s ∗ l_t ↦t{#q} v_t ∗ val_rel v_t v_s)] 
      ⊫ [update_si_strong e π]; na_locs π m ✱ [emp] | 70.
  Proof.
    move => Hml_s.
    iStep 3 as (v_t v_s) "Hl Hvs Hπ Hls Hlt".
    iPoseProof (sim_bij_release_update with "Hl Hπ Hlt Hls Hvs") as "H".
    { rewrite lookup_insert //. }
    iMod "H".
    rewrite delete_insert; last exact Hml_s.
    iSteps.
  Qed.

  Global Instance sim_free_spec2 (l_t l_s : loc) col ma :
    SimultaneousSpec (FreeN #1 #l_t) (FreeN #1 #l_s) (l_t ↔h l_s) (λ π Φ, 
      na_locs π col ∗ ⌜TCIndexIn col l_s ma⌝ ∗ 
        (match ma with | None => True
          |Some (l_t', NaRead q) => ∃ vs vt, l_s ↦s{#q} vs ∗ ⌜l_t = l_t'⌝ ∗ l_t' ↦t{#q} vt ∗ val_rel vt vs
          | _ => False end
        ) ∗
        ((match ma with | None => na_locs π col
          |Some _ => na_locs π (delete l_s col)
          end) -∗ Φ #() #()))%I.
  Proof.
    split.
    - destruct ma as [[p1 [|q]]|]; [ iSteps | | iSteps];
        iStep 3 as (Φ π Hind v_s v_t) "Hlh Hvs Hπ Hls Hp HΦ".
      match goal with 
      | |- environments.envs_entails _ (_ ⪯{?pi} ?es [{ _ }]) =>
        iAssert (update_si_strong es pi (na_locs π (delete l_s col)))%I with "[-HΦ]" as "Hπ" 
      end.
      * rewrite /TCIndexIn in Hind.
        set (col' := delete l_s col).
        erewrite <-(insert_delete col); last done.
        rewrite /col' {col'}.
        set (col' := delete l_s col). assert (col' !! l_s = None). { rewrite /col'. rewrite lookup_delete //. }
        assert (TCIndexIn col' l_s None); first exact H.
        iSteps.
      * iApply sim_update_si_strong.
        iMod "Hπ". iApply update_si_strong_introducable => /=. iSteps. rewrite lookup_delete //.
    - iSteps as (Φ Ψ π HΦΨ Hind) "HΦ Hπ" /
             as (Φ Ψ π HΦΨ Hind) "HΦ Hπ";
      iApply HΦΨ; iSteps.
  Qed.

  Global Instance sim_load_sc_spec (l_t l_s : loc) col :
    SimultaneousSpec (!ˢᶜ #l_t) (!ˢᶜ #l_s) (ε₁)%I (λ π Φ, 
      l_t ↔h l_s ∗ na_locs π col ∗ match col !! l_s with 
        | None => (∀ v_t v_s, val_rel v_t v_s ∗ na_locs π col -∗ Φ (of_val v_t) (of_val v_s))
        | Some pr => let q := if pr.2 is NaRead q then q else 1%Qp in 
            ∃ v1 v2, l_s ↦s{#q} v1 ∗ l_t ↦t{#q} v2 ∗ ⌜pr.1 = l_t⌝ ∗ (l_s ↦s{#q} v1 ∗ l_t ↦t{#q} v2 ∗ na_locs π col -∗ Φ (of_val v2) (of_val v1))
        end
    )%I.
  Proof.
    split.
    - iSteps as (Φ tid) "H↔h Hna_locs Hcol".
      destruct (col !! l_s) eqn:Hcol.
      * iDecompose "Hcol". iSteps.
      * iApply (sim_bij_load_sc with "H↔h Hna_locs"); [done| ].
        iSteps.
    - iStep 4 as (Φ1 Φ2 tid HΦ); iSteps; destruct (col !! l_s); iSteps; iApply HΦ; iSteps.
  Qed.
End instance.
