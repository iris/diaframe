From iris Require Import bi.bi.
From iris.proofmode Require Import proofmode.
From simuliris.simulation Require Import slsls lifting.
From simuliris.simulang Require Import lang notation tactics
  proofmode log_rel_structural wf behavior.
From simuliris.simulang.na_inv Require Export inv.
From simuliris.simulang.na_inv Require Import readonly_refl adequacy.
From iris.prelude Require Import options.

From diaframe.simuliris.na Require Import proof_automation.

Import bi.

(** * Examples for exploiting UB of data-races. *)

Section data_race.
  Context `{naGS Σ}.

  (** This optimization replaces multiple loads with a single load. *)
  Definition remove_load_s : expr :=
    "l" <- !"l" + !"l";;
    !"l".

  Definition remove_load_t : expr :=
    let: "v" := !"l" in
    let: "r" := "v" + "v" in
    "l" <- "r";;
    "r".

  Lemma remove_load_sim:
    ⊢ log_rel remove_load_t remove_load_s.
  Proof.
    log_rel. iSteps.
    rewrite delete_insert //.
    iSteps.
  Qed.

  (** This optimization hoists the read of "x" and "y" out of the loop. *)
  Definition hoist_load_both_s : expr :=
     let: "i" := ref #0  in
     let: "sum" := ref (!"y") in
     while: !"i" ≠ ! "x" do
       "i" <- !"i" + #1;;
       "sum" <- !"sum" + !"y"
     od;;
     let: "res" := !"sum" in
     Free "sum";;
     Free "i";;
     "res".

  Definition hoist_load_both_t : expr :=
     let: "n" := !"x" in
     let: "m" := !"y" in
     let: "i" := ref #0  in
     let: "sum" := ref "m" in
     while: !"i" ≠ "n" do
       "i" <- !"i" + #1;;
       "sum" <- !"sum" + "m"
     od;;
     let: "res" := !"sum" in
     Free "sum";;
     Free "i";;
     "res".


(*  Lemma int_is_unboxed (z : Z) : val_is_unboxed #z.
  Proof. done. Qed.*)
  Lemma hoist_load_both_sim:
    ⊢ log_rel hoist_load_both_t hoist_load_both_s.
  Proof.
    log_rel.
    iSteps as (v1 v2 π li_s l_s li_t qx vx_s vx_t lr_s) "Hvr1 Hlh Hvr2 Hli_s ? Hli_t Hπ Hl_s Hlr_s ?".
    destruct (if v2 is #(LitLoc _) then true else false) eqn:Heq. 2: {
      source_while.
      source_bind (! _)%E. iApply source_red_safe_implies.
      iIntros ([? ?]); simplify_eq.
    }
    destruct v2 as [[| | | |ly_s |]| | |] => //.
    iDecompose "Hvr1" as (l_t) "Hlh_xy".

    (* case analysis: do [x] and [y] alias? *)
    destruct (decide (ly_s = l_s)) as [-> | Hneq].
    - (* they do alias, so we do not need to exploit a second time. *)
      iPoseProof (heapbij_loc_inj with "Hlh Hlh_xy") as "->".
      iSteps as (li_t lr_t) "?????".
      set inv := (
        ∃ (z_i : Z) (vr_t vr_s : val),
        l_t ↦t{#qx} vx_t ∗
        l_s ↦s{#qx} vx_s ∗
        li_s ↦s #z_i ∗
        li_t ↦t #z_i ∗
        lr_t ↦t vr_t ∗
        lr_s ↦s vr_s ∗
        val_rel vr_t vr_s ∗
        na_locs π (<[l_s:=(l_t, NaRead qx)]> ∅) ∗
        †li_s…s 1 ∗ †lr_s…s 1 ∗ †li_t…t 1 ∗ †lr_t…t 1)%I.

      sim_bind (While _ _) (While _ _).
      iApply (sim_while_while inv with "[-]"); iSteps.
      + rewrite bool_decide_false //.
        iSteps. iApply sim_expr_base. iSteps.
      + rewrite bool_decide_true //. iStep 4.
        iStepDebug. iLeft. iSteps.
    - (* the proof of this case is very similar to the one above, except that we carry around
        the additional locations in the invariant and have to release both in the end. *)
      iApply (sim_bij_exploit_load with "Hlh_xy Hπ"); [| |]. {
        intros.
        safe_reach_fill (While _ _)%E => /=.
        apply: safe_reach_pure; [eapply pure_while | done | ].
        safe_reach_fill (Load _ _)%E.
        apply: safe_reach_safe_implies => ?.
        apply: safe_reach_refl. apply: post_in_ectx_intro. naive_solver.
      }
      { rewrite lookup_insert_ne; last done. apply lookup_empty. }
      rename li_t into lx_t. rename l_s into lx_s. rename l_t into ly_t.
      iSteps as (qy vy_t vy_s li_t lr_t) "Hvr3 ????????".

      set inv := (
        ∃ (z_i : Z) (vr_t vr_s : val),
        lx_t ↦t{#qx} vx_t ∗
        lx_s ↦s{#qx} vx_s ∗
        ly_t ↦t{#qy} vy_t ∗
        ly_s ↦s{#qy} vy_s ∗
        li_s ↦s #z_i ∗
        li_t ↦t #z_i ∗
        lr_t ↦t vr_t ∗
        lr_s ↦s vr_s ∗
        val_rel vr_t vr_s ∗
        na_locs π (<[ly_s:=(ly_t, NaRead qy)]> $ <[lx_s:=(lx_t, NaRead qx)]> ∅) ∗
        †li_s…s 1 ∗ †lr_s…s 1 ∗ †li_t…t 1 ∗ †lr_t…t 1)%I.

      sim_bind (While _ _) (While _ _).
      iApply (sim_while_while inv with "[-] []"); iSteps.
      + destruct (bool_decide (#x = vy_t)) eqn:Heq_vy_t.
        { (* contradiction *)
          apply bool_decide_eq_true_1 in Heq_vy_t. subst vy_t.
          val_discr_target "Hvr3". done.
        }
        iSteps. iApply sim_expr_base. iSteps.
      + rewrite bool_decide_true //. 
        iStep 4. iStepDebug. iLeft. iSteps. rewrite delete_notin; last first.
        { rewrite lookup_insert_ne //. }
        iSteps.
    Qed.

  (** This optimization hoists the read of "n" out of the loop. *)
  Definition hoist_load_s : expr :=
     let: "r" := ref #0  in
     let: "i" := ref #0  in
     while: !"i" ≠ ! "n" do
       "r" <- !"r" + !"i";;
       "i" <- !"i" + #1
     od;;
     let: "res" := !"r" in
     Free "r";;
     Free "i";;
     "res".

  Definition hoist_load_t : expr :=
     let: "r" := ref #0  in
     let: "i" := ref #0  in
     let: "nval" := ! "n" in
     while: !"i" ≠ "nval" do
       "r" <- !"r" + !"i";;
       "i" <- !"i" + #1
     od;;
     let: "res" := !"r" in
     Free "r";;
     Free "i";;
     "res".


  Lemma hoist_load_sim:
    ⊢ log_rel hoist_load_t hoist_load_s.
  Proof.
    log_rel. iSteps as (v_t v_s π li_s lr_s li_t lr_t) "Hvr1 Hπ ????????".
    destruct (if v_s is #(LitLoc _) then true else false) eqn:Heq. 2: {
      source_while.
      source_bind (! _)%E. iApply source_red_safe_implies.
      iIntros ([l_s ?]); simplify_eq.
    }
    destruct v_s as [[| | | |n_s |]| | |] => //.
    iDecompose "Hvr1" as (n_t) "Hvr1".
    iApply (sim_bij_exploit_load with "Hvr1 Hπ"); [|done|]. {
      intros.
      safe_reach_fill (While _ _)%E => /=.
      apply: safe_reach_base_step; [ by econstructor|].
      safe_reach_fill (! _)%E => /=.
      apply: safe_reach_safe_implies => ?.
      apply: safe_reach_refl. apply: post_in_ectx_intro. naive_solver.
    }
    iSteps as (q v_t v_s) "Hvr2 ???".
    sim_bind (While _ _) (While _ _).

    (* the invariant: we just thread through our ownership, with the invariant that
      [r] and [i] point to the same integer values.
      (retaining this information eases our live in the loop proof significantly)
    *)
    set (inv := (
      ∃ z_i z_r : Z,
      †lr_s…s 1 ∗ †li_s…s 1 ∗ †lr_t…t 1 ∗ †li_t…t 1 ∗
      lr_s ↦s #z_r ∗ li_s ↦s #z_i ∗ lr_t ↦t #z_r ∗ li_t ↦t #z_i ∗
      n_t ↦t{#q} v_t ∗ n_s ↦s{#q} v_s ∗ na_locs π (<[n_s:=(n_t, NaRead q)]> ∅))%I).
    iApply (sim_while_while inv with "[-]"); iSteps.
    - destruct (bool_decide (#x0 = v_t)) eqn:Heq_v_t.
      { (* contradiction *)
        apply bool_decide_eq_true_1 in Heq_v_t. subst v_t.
        val_discr_target "Hvr2". done.
      }
      iSteps. iApply sim_expr_base. iSteps.
    - rewrite bool_decide_true //. iStep 4. iStepDebug. iLeft. iSteps.
  Qed.

  (** This optimization hoists the read of "m" out of the loop. We
  need to unroll the loop once because the loop condition can be an
  arbitrary expression [e] that does not write to the heap. *)
  Definition hoist_load_unknown_s (e : expr) : expr :=
     let: "r" := ref #0  in
     let: "i" := ref #0  in
     while: e do
       "r" <- !"r" + !"m";;
       "i" <- !"i" + #1
     od;;
     Free "i";;
     let: "res" := !"r" in
     Free "r";;
     "res".

  Definition hoist_load_unknown_t (e : expr) : expr :=
    let: "i" := ref #0 in
    if: e then
      let: "mval" := !"m" in
      let: "r" := ref "mval" in
      "i" <- !"i" + #1;;
      while: e do
        "r" <- !"r" + "mval";;
        "i" <- !"i" + #1
      od;;
      Free "i";;
      let: "res" := !"r" in
      Free "r";;
      "res"
    else
      Free "i";;
      #0.

  Lemma hoist_load_unknown_sim e:
    free_vars e ⊆ list_to_set ["n"; "i"] →
    gen_expr_wf readonly_wf e →
    ⊢ log_rel (hoist_load_unknown_t e) (hoist_load_unknown_s e).
  Proof.
    move => He ?. log_rel. iSteps as (m_t m_s v1 v2 π lr_s li_s li_t) "Hvr_m ? Hπ ? Hlrs_die Hlis Hlis_die Hlit Hlit_die".
    iApply sim_update_si. iApply (sim_bij_freeable_ne_val with "Hvr_m Hlis_die"). iIntros (Hne3) "Hfi_s".
    iApply sim_update_si. iApply (sim_bij_freeable_ne_val with "Hvr_m Hlrs_die"). iIntros (Hne1) "Hfr_s".
    iApply (sim_bij_insert with "Hlit_die Hfi_s Hlit Hlis []"); [done..|]. iIntros "#Hbiji".
    source_while. to_sim.
    sim_bind (subst_map _ _) (subst_map _ _).
    iApply (sim_refl with "[] [Hπ]");
      [compute_done | etrans; [eassumption|compute_done]
       | apply: readonly_log_rel_structural [] ∅ | done | | |]. {
        rewrite !dom_insert_L. iApply big_sepS_intro. iIntros "!#" (s Hin).
        rewrite map_lookup_zip_with.
        destruct (decide (s = "n")); [| destruct (decide (s = "i")); [|exfalso; set_solver]]; by simplify_map_eq.
    }
    { iFrame. unfold pointsto_list, na_locs_in_pointsto_list. iSplit; [|done]. iPureIntro. set_solver. }
    iSteps as (_ b) "?". destruct b; [ | iSteps].
    iSteps as_anon / as (l_s l_t q lr_t m q' z v4 v3) "Hlh ? Hlrt_die Hlrt ?? Hlrs Hπ Hlis Hlit".
    { rewrite lookup_insert_ne //. contradict Hne3; subst => //. }
    sim_bind (While _ _) (While _ _).

    rewrite insert_insert.
    assert (li_s ≠ l_s). { contradict Hne3. by subst. }
    assert (lr_s ≠ l_s); first (contradict Hne1; by subst).
    iApply (sim_bij_insert with "Hlrt_die Hfr_s Hlrt Hlrs []"); [done..|]. iStep as "Hlrh".
    iPoseProof (sim_bij_release with "Hbiji Hπ") as "H". { rewrite lookup_insert //. }
    iApply ("H" with "Hlit Hlis []"); first iSteps. iStep.
    rewrite delete_insert; last rewrite lookup_insert_ne //.
    iApply (sim_while_while (l_t ↦t{#q} #m ∗ l_s ↦s{#q} #m ∗ na_locs π (<[l_s:=(l_t, NaRead q)]> ∅))%I with "[-]").
    { iSteps. }

    iSteps.
    sim_bind (subst_map _ _) (subst_map _ _).
    iApply (sim_refl with "[] [-]");
      [compute_done | etrans; [eassumption|compute_done]
       | apply (readonly_log_rel_structural [(l_t, l_s, #m ,#m , q)]) | done | | |]. {
      rewrite !dom_insert_L. iApply big_sepS_intro. iIntros "!#" (s Hin).
      rewrite map_lookup_zip_with.
      destruct (decide (s = "n")); [| destruct (decide (s = "i")); [|exfalso; set_solver]]; by simplify_map_eq.
    } {
      iFrame "∗ Hlh". iSteps. iPureIntro.
      move => ??? /lookup_insert_Some[[??]|[??]]; simplify_eq. by eexists 0, _, _.
    }
    iSteps as (Hna b) "? Hcomp". iDecompose "Hcomp". destruct b.
    - iStep 36 as (mz q'' z' v6 v5) "??? Hπ Hlrs Hlrt". rewrite insert_insert.
      iPoseProof (sim_bij_release with "Hlrh Hπ") as "H". { rewrite lookup_insert //. }
      iApply ("H" with "Hlrt Hlrs []"); first iSteps. iStep.
      rewrite delete_insert; last rewrite lookup_insert_ne //.
      iSteps as_anon / as (q''' zi v8 v7) "? Hπ Hlis Hlit". { rewrite lookup_insert_ne //. }
      rewrite insert_insert.
      iPoseProof (sim_bij_release with "Hbiji Hπ") as "H". { rewrite lookup_insert //. }
      iApply ("H" with "Hlit Hlis []"); first iSteps. iStep. rewrite delete_insert; last rewrite lookup_insert_ne //.
      iApply sim_expr_base. iSteps.
    - iStep 7. iStepDebug. iLeft. iSteps. { rewrite lookup_insert_ne //. }
      rewrite delete_insert; last rewrite lookup_insert_ne //.
      iSteps. Unshelve.
      rewrite lookup_insert_ne //.
  Qed.

End data_race.

Section closed.
  (** Obtain a closed proof of [ctx_ref]. *)
  Lemma hoist_load_ctx :
    ctx_ref hoist_load_t hoist_load_s.
  Proof.
    intros ??.
    set Σ := #[naΣ].
    apply (log_rel_adequacy Σ)=>?.
    by apply hoist_load_sim.
  Qed.

  (** Obtain a closed proof of [ctx_ref]. *)
  Lemma hoist_load_both_ctx :
    ctx_ref hoist_load_both_t hoist_load_both_s.
  Proof.
    intros ??.
    set Σ := #[naΣ].
    apply (log_rel_adequacy Σ)=>?.
    by apply hoist_load_both_sim.
  Qed.

  (** Obtain a closed proof of [ctx_ref]. *)
  Lemma hoist_load_unknown_ctx e :
    free_vars e ⊆ list_to_set ["n"; "i"] →
    gen_expr_wf readonly_wf e →
    ctx_ref (hoist_load_unknown_t e) (hoist_load_unknown_s e).
  Proof.
    intros ??.
    set Σ := #[naΣ].
    apply (log_rel_adequacy Σ)=>?.
    by apply hoist_load_unknown_sim.
  Qed.
End closed.
