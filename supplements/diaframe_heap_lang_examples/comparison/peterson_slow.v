From diaframe.heap_lang Require Import proof_automation wp_later_credits wp_auto_lob.
From diaframe.lib Require Import own_hints local_post.
From diaframe.symb_exec Require Import defs weakestpre.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import proofmode_classes excl csum auth.

(* true = left *)

Definition new_peterson : val :=
  λ: <>, 
    let: "wl" := ref #false in
    let: "wr" := ref #false in
    let: "first" := ref #true in
    ("first", "wl", "wr").

Definition wait_acquire_l : val :=
  rec: "wait_acquire" "first" "wr" :=
    if: (! "wr") && (~ (! "first")) then 
      "wait_acquire" "first" "wr"
    else 
      #().

Definition acquire_l : val :=
  λ: "p",
    let: "first" := Fst (Fst "p") in
    let: "wl" := Snd (Fst "p") in
    let: "wr" := Snd "p" in
    "wl" <- #true ;;
    "first" <- #false ;;
    wait_acquire_l "first" "wr".

Definition release_l : val :=
  λ: "p", (Snd (Fst "p")) <- #false.

Definition wait_acquire_r : val :=
  rec: "wait_acquire" "first" "wl" :=
    if: (! "wl") && ((! "first")) then 
      "wait_acquire" "first" "wl"
    else 
      #().

Definition acquire_r : val :=
  λ: "p",
    let: "first" := Fst (Fst "p") in
    let: "wl" := Snd (Fst "p") in
    let: "wr" := Snd "p" in
    "wr" <- #true ;;
    "first" <- #true ;;
    wait_acquire_r "first" "wl".

Definition release_r : val :=
  λ: "p", (Snd "p") <- #false.

Definition petersonR := authR $ optionUR $ csumR (exclR $ unitR) (exclR $ unitR).
Class petersonG Σ := PetersonG { #[local] peterson_tokG :: inG Σ petersonR }.
Definition petersonΣ : gFunctors := #[GFunctor petersonR].

Local Obligation Tactic := program_smash_verify.

Global Program Instance subG_petersonΣ {Σ} : subG petersonΣ Σ → petersonG Σ.

Section spec.
  Context `{!heapGS Σ, !petersonG Σ}.
  Let N := nroot.@"peterson".
  Set Default Proof Using "heapGS0 petersonG0".

  (* Cinl is waiting, Cinr is active *)

  Definition peterson_inv waitl waitr first γl γr γR R : iProp Σ :=
    ∃ (wl wr f ra la : bool), waitl ↦{#1/2} #wl ∗ waitr ↦{#1/2} #wr ∗ first ↦ #f ∗
      ((own γl (● (Some $ Cinr $ Excl ())) ∗ ⌜la = true⌝ ∗ ⌜wl = true⌝) ∨ (own γl (● (Some $ Cinl $ Excl ())) ∗ ⌜la = false⌝)) ∗
      ((own γr (● (Some $ Cinr $ Excl ())) ∗ ⌜ra = true⌝ ∗ ⌜wr = true⌝) ∨ (own γr (● (Some $ Cinl $ Excl ())) ∗ ⌜ra = false⌝)) ∗
      (if la && negb ra then ⌜f = ra⌝ else True) ∗ (* note that we don't know anything when both la and ra are true! *)
      (if ra && negb la then ⌜f = negb la⌝ else True) ∗ (* If left has it and first = false=right, then right must be inactive *)
      ((⌜f = ra⌝ ∗ own γl (◯ (Some $ Cinr $ Excl ())) ∗ own γR (● (Some $ Cinl $ Excl ()))) ∨
       (⌜f = negb la⌝ ∗ own γr (◯ (Some $ Cinr $ Excl ())) ∗ own γR (● (Some $ Cinr $ Excl ()))) ∨
       (own γR (● None) ∗ R) 
      ).
    (* wl and wr (wait-left, wait-right) just record the boolean flag value, while 
        la and ra (left-active, right-active) record whether the thread has executed 'first <- other-side' *)

  Definition is_peterson γl γr γR R p : iProp Σ :=
    ∃ (first waitl waitr : loc), ⌜p = (#first, #waitl, #waitr)%V⌝ ∗ inv N (peterson_inv waitl waitr first γl γr γR R).

  Definition left_released γl (γr γR : gname) p : iProp Σ :=
    ∃ (first waitl waitr : loc), ⌜p = (#first, #waitl, #waitr)%V⌝ ∗
    waitl ↦{#1/2} #false ∗ own γl (◯ (Some $ Cinl $ Excl ())).

  Definition left_acquired (γl γr : gname) γR p : iProp Σ :=
    ∃ (first waitl waitr : loc), ⌜p = (#first, #waitl, #waitr)%V⌝ ∗
    waitl ↦{#1/2} #true ∗ own γR (◯ (Some $ Cinl $ Excl ())).

  Definition right_released (γl : gname) γr (γR : gname) p : iProp Σ :=
    ∃ (first waitl waitr : loc), ⌜p = (#first, #waitl, #waitr)%V⌝ ∗
    waitr ↦{#1/2} #false ∗ own γr (◯ (Some $ Cinl $ Excl ())).

  Definition right_acquired (γl γr : gname) γR p : iProp Σ :=
    ∃ (first waitl waitr : loc), ⌜p = (#first, #waitl, #waitr)%V⌝ ∗
    waitr ↦{#1/2} #true ∗ own γR (◯ (Some $ Cinr $ Excl ())).

  Global Program Instance new_lock_spec :
    SPEC {{ True }}
      new_peterson #() 
    {{ (p : val), RET p; ∀ R, R ={⊤}=∗ ∃ γl γr γR,
      is_peterson γl γr γR R p ∗
      left_released γl γr γR p ∗
      right_released γl γr γR p }}.

  Global Instance if_step_abstract e1 e2 E s (b : bool) :
    ReductionStep (wp_red_cond, [tele_arg3 E; s]) (if: #b then e1 else e2)%E ⊣ ⟨id⟩ emp; ε₁ =[▷^1]=>
      ⟨id⟩ (if b then e1 else e2)%V ⊣ ⌜b = true⌝ ∨ ⌜b = false⌝| 999.
  Proof. destruct b; rewrite /ReductionStep' /ReductionTemplateStep /=; iSteps. Qed.

  Program Instance wait_acquire_l_spec (waitl waitr first : loc) γl γr γR R :
    SPEC {{ inv N (peterson_inv waitl waitr first γl γr γR R) ∗ own γl (◯ (Some $ Cinr $ Excl ())) ∗ waitl ↦{#1/2} #true }}
      wait_acquire_l #first #waitr
    {{ RET #(); waitl ↦{#1/2} #true ∗ own γR (◯ (Some $ Cinl $ Excl ())) ∗ R }}.

  Global Program Instance acquire_l_spec (p : val) γl γr γR R :
    SPEC {{ is_peterson γl γr γR R p ∗ left_released γl γr γR p }}
       acquire_l p
    {{ RET #(); left_acquired γl γr γR p ∗ R }}.

  Global Program Instance release_l_spec (p : val) γl γr γR R :
    SPEC {{ is_peterson γl γr γR R p ∗ left_acquired γl γr γR p ∗ R }}
      release_l p 
    {{ RET #(); left_released γl γr γR p }}.

  Definition acquired_mutual_excl (p : val) γl γr γR :
    left_acquired γl γr γR p ∗ right_acquired γl γr γR p ⊢ False := verify.

  Program Instance wait_acquire_r_spec (waitl waitr first : loc) γl γr γR R :
    SPEC {{ inv N (peterson_inv waitl waitr first γl γr γR R) ∗ own γr (◯ (Some $ Cinr $ Excl ())) ∗ waitr ↦{#1/2} #true }}
      wait_acquire_r #first #waitl
    {{ RET #(); waitr ↦{#1/2} #true ∗ own γR (◯ (Some $ Cinr $ Excl ())) ∗ R }} | 50.

  Global Program Instance acquire_r_spec (p : val) γl γr γR R :
    SPEC {{ is_peterson γl γr γR R p ∗ right_released γl γr γR p }}
       acquire_r p
    {{ RET #(); right_acquired γl γr γR p ∗ R }}.

  Global Program Instance release_r_spec (p : val) γl γr γR R :
    SPEC {{ is_peterson γl γr γR R p ∗ right_acquired γl γr γR p ∗ R }}
      release_r p 
    {{ RET #(); right_released γl γr γR p }}.
End spec.