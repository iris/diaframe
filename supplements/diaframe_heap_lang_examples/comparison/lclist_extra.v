From diaframe.heap_lang Require Import proof_automation.
From diaframe.heap_lang.examples.comparison Require Import spin_lock lclist.

Definition create_list : val := λ: "_", 
  let: "head" := ref (NONEV) in
    ("head", newlock #()).

Definition create_node : val := λ: "z" "n", 
  let: "new_node_loc" := AllocN #2 "n" in
  "new_node_loc" <- "z" ;;
  SOME ("new_node_loc", newlock #()).

Definition insert_into_nodelist : val :=
  rec: "insert" "z" "prev_loc" "prev_lk" :=
    match: ! "prev_loc" with
      NONE =>
        "prev_loc" <- create_node "z" NONEV ;;
        release "prev_lk"
    | SOME "pr" =>
      acquire (Snd "pr") ;;
      if: ! (Fst "pr") < "z" then
        release "prev_lk" ;;
        "insert" "z" ((Fst "pr") +ₗ #1) (Snd "pr")
      else 
        "prev_loc" <- create_node "z" (SOME "pr") ;;
        release "prev_lk" ;;
        release (Snd "pr")
    end.

Definition insert : val :=
  λ: "z" "lst",
    acquire (Snd "lst") ;;
    insert_into_nodelist "z" (Fst "lst") (Snd "lst").

Definition element_in_nodelist : val := 
  rec: "contains" "z" "prev_loc" "prev_lk" :=
    match: ! "prev_loc" with
      NONE => (* "z" was not in list: return false *)
        release "prev_lk";; #false
    | SOME "pr" =>
      acquire (Snd "pr");;
      let: "value" := ! (Fst "pr") in
      if: "value" < "z" then (* if "z" is in list, it is somewher further on *)
        release "prev_lk" ;;
        "contains" "z" ((Fst "pr") +ₗ #1) (Snd "pr")
      else 
        release "prev_lk" ;;
        release (Snd "pr") ;; "value" = "z"
    end.

Definition contains : val := 
  λ: "z" "lst",
    acquire (Snd "lst") ;;
    element_in_nodelist "z" (Fst "lst") (Snd "lst").

Definition read_first : val := 
  λ: "lst",
    acquire (Snd "lst") ;;
    match: ! (Fst "lst") with
      NONE => release (Snd "lst");; NONEV
    | SOME "pr" =>
      acquire (Snd "pr") ;;
      let: "value" := ! (Fst "pr") in
      release (Snd "lst") ;;
      release (Snd "pr") ;;
      SOME "value"
    end.

Definition pop_first : val :=
  λ: "lst",
    acquire (Snd "lst") ;;
    match: ! (Fst "lst") with
      NONE => release (Snd "lst");; NONEV
    | SOME "pr" =>
      acquire (Snd "pr") ;;
      let: "value" := ! (Fst "pr") in
      (Fst "lst") <- ! ((Fst "pr") +ₗ #1) ;;
      release (Snd "lst") ;;
      Free (Fst "pr") ;;
      SOME "value"
    end.

Definition read_last_from_nodelist : val :=
  rec: "read_last" "prev_loc" "prev_lk" "prev_val" :=
    match: ! "prev_loc" with
      NONE => release "prev_lk" ;; "prev_val"
    | SOME "pr" =>
      acquire (Snd "pr") ;;
      release "prev_lk" ;;
      "read_last" ((Fst "pr") +ₗ #1) (Snd "pr") (SOME (! (Fst "pr")))
    end.

Definition read_last : val :=
  λ: "lst",
    acquire (Snd "lst") ;;
    read_last_from_nodelist (Fst "lst") (Snd "lst") NONEV.

Definition pop_last_from_nodelist : val :=
  rec: "pop_last" "prev_loc" "prev_lk" "cur_loc" "cur_lk" "cur_val" :=
    match: ! "cur_loc" with
      NONE => (* cur_loc is last node: return cur_val and remove node from list *)
        "prev_loc" <- NONEV ;;
        release "prev_lk" ;;
        "cur_val"
    | SOME "pr" => (* cur_loc is not last node, but "pr" may be: recurse *)
        acquire (Snd "pr") ;;
        release "prev_lk" ;;
        "pop_last" "cur_loc" "cur_lk" ((Fst "pr") +ₗ #1) (Snd "pr") (SOME (! (Fst "pr")))
    end.

Definition pop_last : val :=
  λ: "lst",
    acquire (Snd "lst") ;;
    match: ! (Fst "lst") with
      NONE => release (Snd "lst") ;; NONEV (* nothing to pop *)
    | SOME "pr" =>
      acquire (Snd "pr") ;;
      pop_last_from_nodelist (Fst "lst") (Snd "lst") ((Fst "pr") +ₗ #1) (Snd "pr") (SOME (! (Fst "pr")))
    end.

  (* if we want to get really fancy we could maybe implement some higher-order list traverser/editor *)

Section proof.
  Context `{!heapGS Σ, !lockG Σ}.
  Obligation Tactic := program_verify.

  Global Program Instance create_list_spec :
    SPEC {{ True }} 
      create_list #() 
    {{ (ls : val), RET ls; is_list ls}}.

  Global Program Instance create_node_spec (z : Z) (nd : val) :
    SPEC z', {{ is_node z' nd ∗ ⌜z ≤ z'⌝%Z }}
      create_node #z nd
    {{ (v : val), RET v; is_node z v }}.

  Hint Extern 4 (_ ≤ _)%Z => solve [eassumption | done] : solve_pure_add.

  Global Program Instance insert_into_nodelist_spec γ (z z' z_n : Z) (nd : val) l_succ l prev_lk :
    SPEC [is_lock γ prev_lk (lock_inv z' l is_node)]
      {{ ⌜l_succ = l +ₗ 1⌝ ∗ l ↦ #z' ∗ ⌜z' ≤ z⌝%Z ∗ (l +ₗ 1) ↦ nd ∗ locked γ ∗ is_node z_n nd ∗ ⌜z' ≤ z_n⌝%Z }}
        insert_into_nodelist #z #l_succ prev_lk
      {{ RET #(); True }}.

  Global Program Instance insert_into_list_spec (ls : val) (z : Z) :
    SPEC {{ is_list ls }} insert #z ls {{ RET #(); True }}.

  Global Program Instance element_in_nodelist_spec γ (z z' z_n : Z) (nd : val) l_succ l prev_lk :
    SPEC [is_lock γ prev_lk (lock_inv z' l is_node)]
      {{ ⌜l_succ = l +ₗ 1⌝ ∗ l ↦ #z' ∗ ⌜z' ≤ z⌝%Z ∗ (l +ₗ 1) ↦ nd ∗ locked γ ∗ is_node z_n nd ∗ ⌜z' ≤ z_n⌝%Z }}
        element_in_nodelist #z #l_succ prev_lk
      {{ (b : bool), RET #b; True }}.

  Global Program Instance contains_list_spec (ls : val) (z : Z) :
    SPEC {{ is_list ls }} contains #z ls {{ (b : bool), RET #b; True }}.

  Global Program Instance read_first_spec (ls : val) :
    SPEC {{ is_list ls }} read_first ls {{ (v : val), RET v; ⌜v = NONEV⌝ ∨ ∃ (z : Z), ⌜v = SOMEV #z⌝ }}.

  Global Program Instance pop_first_spec (ls : val) :
    SPEC {{ is_list ls }} pop_first ls {{ (v : val), RET v; ⌜v = NONEV⌝ ∨ ∃ (z : Z), ⌜v = SOMEV #z⌝ }}.

  Global Program Instance read_last_from_nodelist_spec γ (z' z_n z_fallback : Z) (nd : val) l_succ l prev_lk :
    SPEC [is_lock γ prev_lk (lock_inv z' l is_node)]
      {{ ⌜l_succ = l +ₗ 1⌝ ∗ l ↦ #z' ∗ (l +ₗ 1) ↦ nd ∗ locked γ ∗ is_node z_n nd ∗ ⌜z' ≤ z_n⌝%Z }}
        read_last_from_nodelist #l_succ prev_lk (SOMEV #z_fallback)
      {{ (z : Z), RET SOMEV #z; True}}.

  Global Program Instance read_last_spec (ls : val) :
    SPEC {{ is_list ls }} read_last ls {{ (v : val), RET v; ⌜v = NONEV⌝ ∨ ∃ (z : Z), ⌜v = SOMEV #z⌝ }}.

  Global Program Instance pop_last_from_nodelist_spec prev_γ cur_γ (prevz curz next_z : Z) (nd : val) prevl_succ prevl (prev_lk : val) curl_succ (curl : loc) (cur_lk : val) :
    SPEC [is_lock prev_γ prev_lk (lock_inv prevz prevl is_node)]
      {{ ⌜prevl_succ = prevl +ₗ 1⌝ ∗ prevl ↦ #prevz ∗ locked prev_γ ∗ (prevl +ₗ 1) ↦ (SOMEV (#curl, cur_lk)) ∗ 
                 ⌜curl_succ = curl +ₗ 1⌝ ∗ curl ↦ #curz ∗ is_lock cur_γ cur_lk (lock_inv curz curl is_node) ∗ locked cur_γ ∗
                  (curl +ₗ 1) ↦ nd ∗ is_node next_z nd ∗ ⌜curz ≤ next_z⌝%Z ∗ ⌜prevz ≤ curz⌝%Z }}
        pop_last_from_nodelist #prevl_succ prev_lk #curl_succ cur_lk (SOMEV #curz)
      {{ (z : Z), RET SOMEV #z; True }}.

  Global Instance pop_last_spec (ls : val) :
    SPEC {{ is_list ls }} pop_last ls {{ (v : val), RET v; ⌜v = NONEV⌝ ∨ ∃ (z : Z), ⌜v = SOMEV #z⌝ }}.
  Proof.
    iStep 80 as (_ _ _ _ _ _ _ _ _ _ lk_nd lk_list cur_val cur_loc prev next_loc next_val γ_nd γ_lk Hvals _) 
                "Hnode Hlist IH Hl1 Hl2 Hnd Hl' Hγnd Hγlist".
    iClear "IH". 
    (* this breaks since the IH is bad for the first loop. previously, we did not have this IH *)
    iSteps. Unshelve. all: apply inhabitant.
  Qed.
End proof.


























