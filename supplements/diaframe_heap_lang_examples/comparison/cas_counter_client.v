From diaframe.heap_lang Require Import proof_automation.
From iris.algebra Require Import frac_auth numbers.
From diaframe.heap_lang.examples.comparison Require Import cas_counter fork_join.

Definition thread_incr : val :=
  λ: "c" "j",
    incr "c";;
    set "j" #().

Definition parallel_increment : val := 
  λ: <>, 
    let: "c" := make_counter #() in
    let: "j1" := make_join #() in
    Fork (thread_incr "c" "j1");;
    let: "j2" := make_join #() in
    Fork (thread_incr "c" "j2");;
    wait "j1";;
    wait "j2";;
    read "c".

Section client.
  Context `{!heapGS Σ, !counterG Σ, !forkjoinG Σ}.
  Let N := nroot .@ "counter".

  Obligation Tactic := program_verify.

  Global Program Instance thread_incr_spec γc (v : val) (l : loc) n : 
    SPEC γj, {{ is_counter γc l ∗ counter_contribution γc (1/2)%Qp n ∗
      is_join γj (λ _, counter_contribution γc (1/2)%Qp (S n)) v
    }}
      thread_incr #l v
    {{ RET #(); True }}.

  Global Program Instance parallel_increment_spec :
    SPEC {{ True }} 
      parallel_increment #() 
    {{ RET #2; True}}.
End client.