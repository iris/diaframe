From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import frac_auth numbers.

Definition make_counter : val := 
  λ: <>, ref #0.

Definition incr : val := 
  rec: "incr" "l" :=
    let: "n" := !"l" in
    if: CAS "l" "n" (#1 + "n") then 
      #() 
    else 
      "incr" "l".

Definition read : val := 
  λ: "l", !"l".

Class counterG Σ :=
  CounterG { #[local] counter_inG :: inG Σ (frac_authR natR) }.
Definition counterΣ : gFunctors :=
  #[GFunctor (frac_authR natR)].

Local Obligation Tactic := program_verify.

Global Program Instance subG_counterΣ {Σ} : subG counterΣ Σ → counterG Σ.

Section proof.
  Context `{!heapGS Σ, !counterG Σ}.
  Let N := nroot.@"counter".

  Definition is_counter (γ : gname) (l : loc) : iProp Σ :=
    inv N (∃ (z : Z), l ↦ #z ∗ ⌜0 ≤ z⌝%Z ∗ own γ (●F (Z.to_nat z))).

  Definition counter_contribution γ q n : iProp Σ := own γ (◯F{q} n).

  Global Program Instance newcounter_spec :
    SPEC {{ True }} 
      make_counter #()
    {{ γ (l : loc), RET #l; is_counter γ l ∗ counter_contribution γ 1 0 }}.

  Global Program Instance incr_spec γ (l : loc) q n :
    SPEC {{ is_counter γ l ∗ counter_contribution γ q n }}
      incr #l
    {{ RET #(); counter_contribution γ q (S n) }}.

  Global Program Instance read_spec_1 γ (l : loc) (n : nat) :
    SPEC [counter_contribution γ 1 n]
      {{ is_counter γ l }}
        read #l
      {{ (c : Z), RET #c; ⌜c = n⌝ ∗ counter_contribution γ 1 n }} | 30.

  Global Program Instance read_spec_frac γ (l : loc) q (n : nat) :
    SPEC [counter_contribution γ q n]
      {{ is_counter γ l }}
        read #l
      {{ (c : Z), RET #c; ⌜n ≤ c⌝%Z ∗ counter_contribution γ q n }} | 50.

  Global Program Instance is_counter_persistent_reuse γ l : Persistent (is_counter γ l).
End proof.


























