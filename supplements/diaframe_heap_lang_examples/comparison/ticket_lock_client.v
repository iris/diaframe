From diaframe.heap_lang Require Import proof_automation.
From iris.heap_lang.lib Require Import par.
From diaframe.heap_lang.examples.comparison Require Import ticket_lock.

Definition foo : val :=
  λ: "lk" "l" "z",
    acquire "lk";;
    "l" <- "z";;
    "l" +ₗ #1 <- "z";;
    release "lk".

Definition run_foo : val :=
  λ: <>, 
    let: "lk" := newlock #() in
    let:  "l" := AllocN #2 #0 in
    (foo "lk" "l" #3 ||| foo "lk" "l" # 5);;
    acquire "lk";;
    if: ! "l" = ! ("l" +ₗ #1) then 
      #()
    else 
      #() #() (* unsafe *).

Section spec.
  Context `{!heapGS Σ, tlockG Σ, spawnG Σ}.

  Obligation Tactic := program_verify.

  Definition same_vals l : iProp Σ := ∃ (z : Z), l ↦ #z ∗ (l +ₗ 1) ↦ #z.

  Global Program Instance foo_spec (lk : val) (l : loc) (z : Z) :
    SPEC γ1 γ2, {{ is_lock γ1 γ2 lk (same_vals l) }}
      foo lk #l #z
    {{ RET #(); True}}.

  Global Program Instance run_foo_spec :
    SPEC {{ True }}
      run_foo #()
    {{ RET #(); True}}.
End spec.







