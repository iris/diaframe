From diaframe.heap_lang Require Import proof_automation wp_auto_lob.

Definition new_stack : val := 
  λ: <>, ref NONEV.

Definition push_this : val :=
  rec: "push" "s" "l" :=
    let: "tail" := ! "s" in
    "l" +ₗ #1 <- "tail" ;; 
    if: CAS "s" "tail" (SOME "l") then 
      #() 
    else 
      "push" "s" "l".

Definition push : val :=
  λ: "s" "v",
    let: "l" := AllocN #2 "v" in
    push_this "s" "l". (* not inlined, since l ↦∗[v;v] not generalized to l ↦∗[v;w] *)

Definition pop : val :=
  rec: "pop" "s" :=
    match: !"s" with
      NONE => NONEV
    | SOME "l" =>
      let: "next" := !("l" +ₗ #1) in
      if: CAS "s" (SOME "l") "next" then 
        SOME (! "l")
      else 
        "pop" "s"
    end.

Section spec.
  Context `{!heapGS Σ}.
  Let N := nroot .@ "stack".

  Fixpoint is_list (P : val → iProp Σ) (xs : list val) (lv : val) : iProp Σ :=
    match xs with
    | [] => ⌜lv = NONEV⌝
    | x :: xs => ∃ (l : loc), ⌜lv = SOMEV #l⌝ ∗ l ↦□ x ∗ P x ∗ 
          ∃ lv', (l +ₗ1) ↦□ lv' ∗ is_list P xs lv'
    end.

  Fixpoint is_list_skel (xs : list val) (lv : val) : iProp Σ :=
    match xs with
    | [] => ⌜lv = NONEV⌝
    | x :: xs => ∃ (l : loc), ⌜lv = SOMEV #l⌝ ∗ l ↦□ x ∗ 
          ∃ lv', (l +ₗ1) ↦□ lv' ∗ is_list_skel xs lv'
    end.

  Lemma is_list_gives_skel P xs v : is_list P xs v ⊢ is_list_skel xs v.
  Proof. revert v; induction xs; iSteps. by iApply IHxs. Qed.

  Global Instance is_list_skel_pers xs v : Persistent (is_list_skel xs v). 
  Proof. revert v; induction xs; tc_solve. Qed.

  Definition stack_inv P l :=
    (∃ vl, l ↦ vl ∗ ∃ xs, is_list P xs vl ∗ ⌜val_is_unboxed vl⌝)%I.

  Definition is_stack (P : val → iProp Σ) v : iProp Σ :=
    ∃ (l : loc), ⌜v = #l⌝ ∗ inv N (stack_inv P l)%I.

  Obligation Tactic := program_verify.

  Global Program Instance biabd_islist_none xs P :
    HINT ε₀ ✱ [- ; ⌜xs = []⌝] ⊫ [id]; is_list P xs NONEV ✱ [⌜xs = []⌝].

  Global Program Instance biabd_islist_some P (l : loc) xs :
    HINT ε₁ ✱ [x xs' t ; l ↦□ x ∗ (l +ₗ 1) ↦□ t ∗ P x ∗ is_list P xs' t ∗ ⌜xs = x :: xs'⌝] ⊫
      [id]; is_list P xs (SOMEV #l) ✱ [⌜xs = x :: xs'⌝].

  Instance is_list_remember_skel P xs v :
    MergablePersist (is_list P xs v) (λ p Pin Pout,
        TCAnd (TCEq Pin (ε₀)%I)
              (TCEq Pout (is_list_skel xs v ∗ ⌜val_is_unboxed v⌝)))%I | 30.
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->] /=.
    rewrite is_list_gives_skel. destruct xs; iSteps.
  Qed.

  Global Instance biabd_islist_pop P l v E :
    HINT (l +ₗ 1) ↦□ v ✱ [x xs'; l ↦□ x ∗ is_list P xs' (SOMEV #l)] 
           ⊫ [fupd E E] xs; 
         is_list P xs v ✱ [⌜val_is_unboxed v⌝ ∗ P x ∗ ⌜xs' = x :: xs⌝] | 30.
  Proof.
    iSteps as (x xs) "Hl Hlist_skel Hl' Hlist".
    destruct xs; iDecompose "Hlist"; iSteps.
  Qed.

  Global Instance match_list xs v e1 e2 :
    SPEC [is_list_skel xs v] {{ True }} 
      Case v e1 e2 
    {{ [▷^0] RET Case v e1 e2; 
        ⌜v = NONEV⌝ ∗ ⌜xs = []⌝ ∨ ∃ (l : loc) h xs' t, ⌜v = SOMEV #l⌝ ∗ l ↦□ h ∗ (l +ₗ 1) ↦□ t ∗ ⌜xs = h :: xs'⌝ ∗ is_list_skel xs' t 
    }} | 50.
  Proof. iStep 4 as (Φ) "Hxs". destruct xs; iDecompose "Hxs"; iSteps. Qed.

  Global Program Instance new_stack_spec :
    SPEC {{ True }} 
      new_stack #() 
    {{ (s : val), RET s; ∀ P, |={⊤}=> is_stack P s }}.

  Context (P : val → iProp Σ).

  Global Program Instance push_this_spec (s : val) (l : loc) (v w : val) :
    SPEC {{ is_stack P s ∗ l ↦ v ∗ (l +ₗ 1) ↦ w ∗ P v }}
      push_this s #l
    {{ RET #(); True }}.

  Global Program Instance push_spec (s v : val) :
    SPEC {{ is_stack P s ∗ P v }} 
      push s v 
    {{ RET #(); True }}.

  Global Program Instance pop_spec (s : val) :
    SPEC {{ is_stack P s }} 
      pop s 
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ w, ⌜ov = SOMEV w⌝ ∗ P w }}.
End spec.





