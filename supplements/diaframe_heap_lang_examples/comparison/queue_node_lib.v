From diaframe.heap_lang Require Import proof_automation loc_map.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import excl.
From diaframe.lib Require Export own_hints.

Definition queueR := optionR $ exclR (leibnizO loc).
Class queueG Σ := QueueG { 
  #[local] queue_tokG :: inG Σ queueR ;
  #[local] queue_loc_mapG :: loc_mapG Σ gname
}.
Definition queueΣ : gFunctors := #[GFunctor queueR; loc_mapΣ gname].

Global Instance subG_queueΣ {Σ} : subG queueΣ Σ → queueG Σ.
Proof. solve_inG. Qed.

Section node.
  Context `{!heapGS Σ, !queueG Σ}.

  Let N := nroot .@ "nodequeue".

  Definition is_node_pre γm (P : val → iProp Σ) (F : loc -d> gname -d> iPropO Σ) :
     loc -d> gname -d> iPropO Σ := (λ l γ, 
      ((l +ₗ 1) ↦ NONEV ∗ own γ None) ∨
      (∃ (l2 : loc) v' γ', (l +ₗ 1) ↦□ SOMEV #l2 ∗ l2 ↦□ v' ∗
              (own γ (Excl' l) ∨ (P v' ∗ own γ' (Excl' l2))) ∗
               inv N (F l2 γ') ∗ loc_map_elem γm l2 DfracDiscarded γ'))%I.

  Local Instance is_node_contr γm (P : val → iProp Σ) : Contractive (is_node_pre γm P).
  Proof. rewrite /is_node_pre; repeat (intro); repeat (f_contractive || f_equiv). Qed.

  Definition is_node_def γm (P : val -> iProp Σ) := fixpoint (is_node_pre γm P).
  Definition is_node_aux P : seal (@is_node_def P). by eexists. Qed.
  Definition is_node P := unseal (is_node_aux P).
  Definition is_node_eq P : @is_node P = @is_node_def P := seal_eq (is_node_aux P).

  Lemma is_node_unfold γm (P : val → iProp Σ) l γ :
    is_node γm P l γ ⊣⊢ is_node_pre γm P (is_node γm P) l γ.
  Proof. rewrite is_node_eq. apply (fixpoint_unfold (is_node_pre _ _)). Qed.

  Global Instance is_node_from_unfold γm P l γ E : 
    HINT ε₁ ✱ [- ; ▷ (l +ₗ 1) ↦ NONEV ∗ own γ None ∨ 
        ∃ (l2 : loc) v' γ', ▷ (l +ₗ 1) ↦□ SOMEV #l2 ∗ l2 ↦□ v' ∗
          ((own γ (Excl' l)) ∨ (P v' ∗ own γ' (Excl' l2))) ∗
            inv N (is_node γm P l2 γ') ∗ loc_map_elem γm l2 DfracDiscarded γ' ] 
      ⊫ [fupd E E]; is_node γm P l γ ✱ [emp].
  Proof. iSteps; rewrite (is_node_unfold _ P l γ); iSteps. Qed.

  Global Instance is_node_into_exist γm (P : val → iProp Σ) (l : loc) γ : 
    AtomIntoExist (is_node γm P l γ) (λ tt : unit,
         (l +ₗ 1) ↦ NONEV ∗ own γ None ∨ 
        ∃ (l2 : loc) v' γ', (l +ₗ 1) ↦□ SOMEV #l2 ∗ l2 ↦□ v' ∗
          (own γ (Excl' l) ∨ (P v' ∗ own γ' (Excl' l2))) ∗ (bi_except_0 $ inv N (is_node γm P l2 γ')) ∗ loc_map_elem γm l2 DfracDiscarded γ')%I.
  Proof.
    rewrite /AtomIntoExist /IntoExistCareful2 /IntoExistCareful -(bi.exist_intro tt) is_node_unfold //.
    setoid_rewrite <-bi.except_0_intro. done.
  Qed.

  (* this hint relies on agreement properties of loc_map_elem, which is why disjunction stuff is not enough *)
  Global Instance queue_head_change (l l' : loc) γ γ' γm E P :
    HINT (l +ₗ 1) ↦□ SOMEV #l' ✱ [- ; own γ' (Excl' l) ∗ inv N (is_node γm P l γ') ∗ loc_map_elem γm l' DfracDiscarded γ ∗ ⌜↑N ⊆ E⌝]
           ⊫ [fupd E E]; 
         (▷ own γ (Excl' l')) ✱ [∃ v, l' ↦□ v ∗ ▷ P v].
  Proof.
    iSteps as (HE) "Hl HI Hγm Hγ". iInv N as "HN". 
    rewrite {2 3}is_node_unfold. iDecompose "HN" as (v) "Hl Hl' Hγm' HI' HP Hγ'".
    iSteps.
  Qed.

  Definition queue_head γm P l := (∃ γ, inv N (is_node γm P l γ) ∗ own γ (Excl' l))%I.
  (* the trick here is that the own γ forces the inside of is_node to have a P v, as soon as it has a next node.
     queue_head_change shows that if you wish to change the queue_head to the successor of l, you actually get
     P v for the node you removed from the queue *)

  Global Instance allocate_any_loc l : 
    (* ideally the sidecondition would be a forall. but that is not sound: we must be able to know l now *)
    HINT ε₁ ✱ [- ; emp] ⊫ [bupd] γ; own γ None ✱ [own γ $ Excl' l].
  Proof. iStep. iAssert (|==> ∃ γ, own γ (Excl' l))%I as ">[%γ Hγ]"; iSteps. Qed.
End node.






























