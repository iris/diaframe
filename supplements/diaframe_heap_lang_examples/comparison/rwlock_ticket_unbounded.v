From iris.algebra Require Import excl.
From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.heap_lang Require Import proofmode.
From diaframe.lib Require Import frac_token.
From diaframe.heap_lang.examples.comparison Require Import ticket_lock.

Definition new_rwlock : val :=
  λ: <>,
    let: "rdrs" := ref #0 in
    let: "tlk" := newlock #() in
    ("tlk", "rdrs").

Definition acquire_reader : val :=
  λ: "rwlk",
    acquire (Fst "rwlk") ;;
    FAA (Snd "rwlk") #1 ;;
    release (Fst "rwlk").

Definition release_reader : val :=
  λ: "rwlk", FAA (Snd "rwlk") #-1.

Definition duplicate_reader : val :=
  λ: "rwlk", FAA (Snd "rwlk") #1.

Definition wait_for_readers : val :=
  rec: "wait" "readers" :=
    if: ! "readers" = #0 then
      #()
    else
      "wait" "readers".

Definition acquire_writer : val :=
  λ: "rwlk",
    acquire (Fst "rwlk") ;;
    wait_for_readers (Snd "rwlk").

Definition release_writer : val :=
  λ: "rwlk", release (Fst "rwlk").

Definition downgrade_writer : val :=
  λ: "rwlk",
    (Snd "rwlk") <- #1;;
    release (Fst "rwlk").

Class rwlockG Σ := RwLockG {
  #[local] rwtlock_G :: tlockG Σ;
  #[local] rwfracToken_G :: fracTokenG Σ
}.
Definition rwlockΣ : gFunctors := #[ tlockΣ; fracTokenΣ ].

Local Obligation Tactic := program_verify.

Global Instance subG_rwlockΣ {Σ} : subG rwlockΣ Σ → rwlockG Σ := ltac:(solve_inG).
(* TODO: why is Program not sufficient here - it seems to split rwlockG before we have a chance.. *)

Section proof.
  Context `{!heapGS Σ, !rwlockG Σ}.
  Implicit Types P : Qp → iProp Σ.
  Let N := nroot .@ "rw_lock".

  Definition readers_inv P (γ γ' : gname) (readers : loc) : iProp Σ :=
    ∃ (z : Z), 
      readers ↦ #z ∗ 
        ((⌜0 < z⌝%Z ∗ token_counter P γ (Z.to_pos z)) ∨
          (⌜z = 0⌝%Z ∗ no_tokens P γ (1/2)%Qp ∗ ⌜Fractional P⌝ ∗ (locked γ' ∨ (no_tokens P γ (1/2)%Qp ∗ P 1%Qp)))).

  Definition is_rwlock P (γ1 γ2 γ2' : gname) (rwlk : val) : iProp Σ :=
    ∃ (tlk : val) (rdrs : loc),
      ⌜rwlk = (tlk, #rdrs)%V⌝ ∗ is_lock γ2 γ2' tlk True%I ∗ inv N (readers_inv P γ1 γ2' rdrs).

  Global Program Instance new_rwlock_spec  :
    SPEC {{ True }} 
      new_rwlock #() 
    {{ (rwlk : val), RET rwlk; ∀ P, ⌜Fractional P⌝ ∗ P 1%Qp ={⊤}=∗ ∃ γ1 γ2 γ2', is_rwlock P γ1 γ2 γ2' rwlk }}.

  Context P.

  Global Program Instance acquire_reader_spec (rwlk : val) γ1 γ2 γ2' :
    SPEC {{ is_rwlock P γ1 γ2 γ2' rwlk }} 
      acquire_reader rwlk 
    {{ RET #(); token P γ1}}.

  Global Program Instance release_reader_spec (rwlk : val) γ1 γ2 γ2' :
    SPEC {{ is_rwlock P γ1 γ2 γ2' rwlk ∗ token P γ1 }}
      release_reader rwlk 
    {{ (z : Z), RET #z; True }}.

  Global Program Instance duplicate_reader_spec (rwlk : val) γ1 γ2 γ2' :
    SPEC {{ is_rwlock P γ1 γ2 γ2' rwlk ∗ token P γ1 }}
      duplicate_reader rwlk 
    {{ (z : Z), RET #z; token P γ1 ∗ token P γ1 }}.

  Program Instance wait_for_readers_spec (γ γ' : gname) (readers : loc) :
    SPEC {{ inv N (readers_inv P γ γ' readers) ∗ locked γ' }}
      wait_for_readers #readers
    {{ RET #(); no_tokens P γ (1/2)%Qp ∗ P 1 }}.

  Global Program Instance acquire_writer_spec (rwlk : val) γ1 γ2 γ2' :
    SPEC {{ is_rwlock P γ1 γ2 γ2' rwlk }}
      acquire_writer rwlk 
    {{ RET #(); no_tokens P γ1 (1/2)%Qp ∗ P 1 }}.

  Global Program Instance release_writer_spec (rwlk : val) γ1 γ2 γ2' :
    SPEC {{ is_rwlock P γ1 γ2 γ2' rwlk ∗ no_tokens P γ1 (1/2)%Qp ∗ P 1 }}
      release_writer rwlk 
    {{ RET #(); True }}.

  Global Program Instance downgrade_writer_spec (rwlk : val) γ1 γ2 γ2' :
    SPEC {{ is_rwlock P γ1 γ2 γ2' rwlk ∗ no_tokens P γ1 (1/2)%Qp ∗ P 1 }}
      downgrade_writer rwlk
    {{ RET #(); token P γ1 }}.
End proof.




























