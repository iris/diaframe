From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.heap_lang.examples.comparison Require Import spin_lock.

(* a sorted concurrent list implemented using hand-over-hand concurrency/ a lock-coupling list *)

Definition remove_from_nodelist : val :=
  rec: "remove" "z" "prev_loc" "prev_lk" :=
    match: ! "prev_loc" with
      NONE => (* "z" was not in list: return false *)
        release "prev_lk";; #false
    | SOME "pr" =>
      acquire (Snd "pr");;
      let: "value" := ! (Fst "pr") in
      if: "value" < "z" then (* if "z" is in list, it is somewher further on *)
        release "prev_lk" ;;
        "remove" "z" ((Fst "pr") +ₗ #1) (Snd "pr")
      else 
        if: "value" = "z" then (* "z" is in list: remove it *)
          "prev_loc" <- (! ((Fst "pr") +ₗ #1)) ;;
          release "prev_lk" ;;
          Free (Fst "pr") ;;
          #true
          (* note that we can never release (Snd "pr")! *)
        else (* "z" is not in list: return false *)
          release "prev_lk" ;;
          release (Snd "pr") ;; #false
    end.

Definition remove : val :=
  λ: "z" "lst",
    acquire (Snd "lst") ;;
    remove_from_nodelist "z" (Fst "lst") (Snd "lst").

Section proof.
  Context `{!heapGS Σ, !lockG Σ}.

  Definition lock_inv (z : Z) l F : iProp Σ :=
    (∃ (z_n : Z) (n : val), l ↦ #z ∗ (l +ₗ 1) ↦ n ∗ F z_n n ∗ ⌜z ≤ z_n⌝%Z ).

  (* a fancier version would use two-party locks inside the nodes. this also allows deallocation of the locks *)
  Definition is_node_pre (F : Z -d> val -d> iPropO Σ) :
     Z -d> val -d> iPropO Σ := (λ z v, 
      ⌜v = NONEV⌝ ∨ (∃ (l : loc) (lk : val) γ, ⌜v = SOMEV (#l, lk)⌝ ∗ is_lock γ lk (lock_inv z l F)))%I.

  Local Instance is_node_contr : Contractive is_node_pre.
  Proof. rewrite /is_node_pre /lock_inv; repeat (intro); repeat (f_contractive || f_equiv). Qed.

  Definition is_node_def := fixpoint is_node_pre.
  Definition is_node_aux : seal (@is_node_def). by eexists. Qed.
  Definition is_node := unseal is_node_aux.
  Definition is_node_eq : @is_node = @is_node_def := seal_eq is_node_aux.

  Lemma is_node_unfold z v :
    is_node z v ⊣⊢ is_node_pre is_node z v.
  Proof. rewrite is_node_eq. apply (fixpoint_unfold is_node_pre). Qed.

  Instance is_node_persistent z v : Persistent $ is_node z v.
  Proof. rewrite is_node_unfold. tc_solve. Qed.

  Definition is_list (v : val) : iProp Σ := ∃ (l : loc) (lk : val) γ, ⌜v = (#l, lk)%V⌝ ∗ is_lock γ lk (∃ z v, l ↦ v ∗ is_node z v).
  Global Instance none_is_node z : 
    HINT ε₀ ✱ [- ; emp] ⊫ [id]; is_node z NONEV ✱ [emp].
  Proof. iStep. rewrite is_node_unfold. iSteps. Qed.

  Global Instance some_is_node z (l : loc) lk :
    HINT ε₁ ✱ [γ; is_lock γ lk (lock_inv z l is_node)] ⊫ [id]; is_node z (SOMEV (#l, lk)%V) ✱ [emp].
  Proof. iSteps as (γ) "Hlk". rewrite is_node_unfold. iSteps. Qed.

  Global Instance match_node (nd : val) e1 e2 z :
    SPEC [is_node z nd] {{ True }} Case nd e1 e2 {{ [▷^0] RET Case nd e1 e2; is_node_pre is_node z nd }} | 50.
  Proof. iStep 4 as (Φ) "Hnd". rewrite -is_node_unfold. iSteps. Qed.

  Obligation Tactic := program_verify.

  Global Program Instance remove_from_nodelist_spec γ (z z' z_n : Z) (nd : val) l_succ l prev_lk :
    SPEC [is_lock γ prev_lk (lock_inv z' l is_node)]
      {{ ⌜l_succ = l +ₗ 1⌝ ∗ l ↦ #z' ∗ ⌜z' ≤ z⌝%Z ∗ (l +ₗ 1) ↦ nd ∗ locked γ ∗ is_node z_n nd ∗ ⌜z' ≤ z_n⌝%Z }}
        remove_from_nodelist #z #l_succ prev_lk
      {{ (b : bool), RET #b; True }}.

  Global Program Instance remove_from_list_spec (ls : val) (z : Z) :
    SPEC {{ is_list ls }} remove #z ls {{ (b : bool), RET #b; True }}.
End proof.


















































