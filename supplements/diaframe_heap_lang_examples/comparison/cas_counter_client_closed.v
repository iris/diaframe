From diaframe.heap_lang Require Import proof_automation.
From diaframe.heap_lang.examples.comparison Require Import cas_counter cas_counter_client fork_join.
From iris.heap_lang Require Import adequacy.

Definition clientΣ : gFunctors := #[ heapΣ; counterΣ; forkjoinΣ ].

Lemma client_adequate σ : adequate NotStuck (parallel_increment #()) σ (λ v _, (v = #2)).
Proof. apply (heap_adequacy clientΣ)=> ?. iSteps. Qed.

(* Print Assumptions client_adequate. *)