From diaframe.heap_lang Require Import proof_automation.
From diaframe.lib Require Import frac_token.

Definition mk_arc : val :=
  λ: "_", ref #1.

Definition clone : val :=
  λ: "a", FAA "a" #1;; #().

Definition drop : val :=
  λ: "a", 
    let: "old_val" := FAA "a" #(-1) in
    ("old_val" = #1).

Section spec.
Context `{!heapGS Σ, !fracTokenG Σ}.
Local Notation iProp := (iProp Σ).
Local Notation no_tokens := (λ P γ, no_tokens P γ 1%Qp).

Context (P : Qp → iProp) `{!Fractional P}.

Let N := nroot .@ "arc".

Obligation Tactic := program_verify.

Definition arc_inv (γ : gname) (l : loc) : iProp := 
  ∃ (n : nat), l ↦ #n ∗ ((⌜n = 0⌝ ∗ no_tokens P γ)
                       ∨ (⌜0 < n⌝ ∗ token_counter P γ (Pos.of_nat n))).

Definition is_arc (γ : gname) (v : val) : iProp := 
  ∃ (l : loc), ⌜v = #l⌝ ∗ inv N (arc_inv γ l).

Program Instance mk_arc_spec :
  SPEC {{ P 1%Qp }} 
    mk_arc #() 
  {{ (v : val) (γ : gname), RET v; is_arc γ v ∗ token P γ }}.

Program Instance clone_arc_spec (γ : gname) (v : val) :
  SPEC {{ token P γ ∗ is_arc γ v }} 
    clone v 
  {{ RET #(); token P γ ∗ token P γ }}.

Program Instance drop_arc_spec (γ : gname) (v : val) :
  SPEC {{ token P γ ∗ is_arc γ v }} 
    drop v 
  {{ (b : bool), RET #b; (⌜b = true⌝ ∗ P 1) ∨ ⌜b = false⌝ }}.
End spec.


Definition drop_free : val :=
  λ: "a",
    let: "old_val" := FAA "a" #(-1) in
    if: "old_val" = #1 then
      Free "a";; #true
    else
      #false.

Section spec_free.
Context `{!heapGS Σ, !fracTokenG Σ}.
Local Notation iProp := (iProp Σ).
Local Notation no_tokens := (λ P γ, no_tokens P γ 1%Qp).

Context (P : Qp → iProp) `{!Fractional P}.

Let N := nroot .@ "arc".

Obligation Tactic := program_verify.

Definition arc_inv_free (γ : gname) (l : loc) : iProp := 
  no_tokens P γ ∨ (∃ p : positive, l ↦ #(Zpos p) ∗ token_counter P γ p).

Definition is_arc_free (γ : gname) (v : val) : iProp := 
  ∃ (l : loc), ⌜v = #l⌝ ∗ inv N (arc_inv_free γ l).

Program Instance mk_arc_free_spec :
  SPEC {{ P 1%Qp }} 
    mk_arc #() 
  {{ (v : val) (γ : gname), RET v; is_arc_free γ v ∗ token P γ }}.

Program Instance clone_arc_free_spec (γ : gname) (v : val) :
  SPEC {{ token P γ ∗ is_arc_free γ v }} 
    clone v 
  {{ RET #(); token P γ ∗ token P γ }}.

Program Instance drop_arc_free_spec (γ : gname) (v : val) :
  SPEC {{ token P γ ∗ is_arc_free γ v }} 
    drop v 
  {{ (b : bool), RET #b; (⌜b = true⌝ ∗ P 1) ∨ ⌜b = false⌝ }}.
End spec_free.












