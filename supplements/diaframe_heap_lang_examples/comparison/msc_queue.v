From diaframe.heap_lang Require Import proof_automation wp_auto_lob loc_map.
From diaframe.heap_lang.examples.comparison Require Import queue_node_lib.

(* Note that this file is shorter than the table states. We have included the linecounts of queue_node_lib.v in the table. *)
(* This queue is almost, _but not quite_ the Michael Scott queue. The consistent snapshots have been removed to simplify the verification. *)
Definition new_queue : val := 
  λ: <>, let: "n" := AllocN #2 NONEV in AllocN #2 "n".

Definition dequeue : val :=
  rec: "dequeue" "q" :=
    let: "head" := ! "q" in
    let: "next" := ! ("head" +ₗ #1) in
    match: "next" with
      NONE => NONEV
    | SOME "l" =>
      if: CAS "q" "head" "l" then 
        SOME ! "l"
      else 
        "dequeue" "q"
    end.

Definition enqueue : val :=
  λ: "q" "v",
    let: "node" := AllocN #2 NONEV in
    let: "tailptr" := "q" +ₗ #1 in
    "node" <- "v" ;;
    let: "newtail" := "node" in
    (rec: "set_tail" "_" :=
      let: "tail" := (! "tailptr") in
      let: "next" := ! ("tail" +ₗ #1) in
      match: "next" with
        NONE => 
          if: CAS ("tail" +ₗ #1) NONE (SOME "newtail") then 
            CAS "tailptr" "tail" "newtail";; #()
          else 
            "set_tail" #()
      | SOME "l" =>
          CAS "tailptr" "tail" "l";;
          "set_tail" #()
      end) #().

Section spec.
  Context `{!heapGS Σ, !queueG Σ}.

  Let N_node := nroot .@ "nodequeue".
  Let N_queue := nroot .@ "queue".

  Obligation Tactic := program_verify.
  Local Existing Instance queue_loc_mapG.

  Definition queue_head_inv γm P l : iProp Σ := ∃ (lh : loc), l ↦ #lh ∗ queue_head γm P lh.
  Definition queue_tail_inv γm P l : iProp Σ := ∃ (lt : loc) γ, l ↦ #lt ∗ inv N_node (is_node γm P lt γ).

  Definition is_queue γm P v : iProp Σ :=
    ∃ (l : loc), ⌜v = #l⌝ ∗ global_reg γm ∗ inv N_queue (queue_head_inv γm P l) ∗ inv N_queue (queue_tail_inv γm P (l +ₗ 1)).

  Global Program Instance new_queue_spec :
    SPEC {{ True }} 
      new_queue #() 
    {{ (v : val), RET v; ∀ P, |={⊤}=> ∃ γm, is_queue γm P v }}.

  Context (P : val → iProp Σ).

  Global Program Instance dequeue_spec γm (v : val) :
    SPEC {{ is_queue γm P v }} 
      dequeue v 
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ w, ⌜ov = SOMEV w⌝ ∗ P w }}.

  Global Program Instance enqueue_spec γm (v w : val) :
    SPEC {{ is_queue γm P v ∗ P w}} 
      enqueue v w 
    {{ RET #(); True }}.
End spec.

(* note that the 'lagging' state is not really mentioned anywhere!
   the invariant we currently have allows lagging behind several nodes.
    this is not strange: the definition of set_tail/enqueue will make the queue 'recover' from it

   indeed, we could add a lazy enqueue that does not try to update the tailpointer *)
















































