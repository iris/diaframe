From iris.heap_lang Require Import proofmode.
From diaframe.heap_lang Require Import proof_automation wp_auto_lob loc_map.
From diaframe.heap_lang.examples.comparison Require Import queue_node_lib.

(* Note that this file is shorter than the table states. We have included the linecounts of queue_node_lib.v in the table. *)

Definition new_queue : val := λ: "_", 
  let: "n" := AllocN #2 NONEV in ref "n".

Definition dequeue : val :=
  rec: "dequeue" "q" :=
    let: "head" := ! "q" in
    let: "next" := ! ("head" +ₗ #1) in
    match: "next" with
      NONE => NONEV
    | SOME "l" =>
      if: CAS "q" "head" "l" then 
        SOME ! "l"
      else 
        "dequeue" "q"
    end.

Definition get_tail : val :=
  rec: "get_tail" "head" :=
    let: "next" := ! ("head" +ₗ #1) in
    match: "next" with
      NONE => "head"
    | SOME "l" =>
      "get_tail" "l"
    end.

Definition set_tail : val :=
  rec: "set_tail" "head" "next" :=
    let: "tail" := get_tail "head" in
    if: CAS ("tail" +ₗ #1) NONE (SOME "next") then 
      #()
    else 
      "set_tail" "head" "next". 
  (* doing the recursive call with "head" is inefficient, but most similar to Capers implementation. 
     One can freely replace "head" with "tail" to get a more efficient version that can be verified without changes to the proof *)

Definition enqueue : val :=
  λ: "q" "v",
    let: "node" := AllocN #2 NONEV in
    "node" <- "v" ;;
    let: "head" := ! "q" in
    set_tail "head" "node". (* this is safe because dequeue'ing does not destroy the linked list structure *)

Section spec.
  Context `{!heapGS Σ, !queueG Σ}.

  Let N_node := nroot .@ "nodequeue".
  Let N_queue := nroot .@ "queue".

  Obligation Tactic := program_verify.
  Local Existing Instance queue_loc_mapG.

  Definition queue_inv γm P l : iProp Σ := ∃ (l' : loc), l ↦ #l' ∗ queue_head γm P l'.
  Definition is_queue γm P v : iProp Σ := ∃ (l : loc), ⌜v = #l⌝ ∗ global_reg γm ∗ inv N_queue (queue_inv γm P l).

  Global Program Instance new_queue_spec :
    SPEC {{ True }} 
      new_queue #() 
    {{ (v : val), RET v; ∀ P, |={⊤}=> ∃ γm, is_queue γm P v }}.

  Context (P : val → iProp Σ).

  Global Program Instance dequeue_spec γm (v : val) :
    SPEC {{ is_queue γm P v }} 
      dequeue v
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ w, ⌜ov = SOMEV w⌝ ∗ P w }}.

  Program Instance get_tail_spec γm (l : loc) γ :
    SPEC {{ inv N_node (is_node γm P l γ) }}
      get_tail #l
    {{ (l' : loc) γ', RET #l'; inv N_node (is_node γm P l' γ')}}.

  Program Instance set_tail_spec γm (l n : loc) γ v :
    SPEC {{ global_reg γm ∗ inv N_node (is_node γm P l γ) ∗ n ↦□ v ∗ (n +ₗ 1) ↦ NONEV ∗ P v ∗ meta_token n ⊤ }}
      set_tail #l #n
    {{ RET #(); True }}.

  Global Program Instance enqueue_spec γm (v w : val) :
    SPEC {{ is_queue γm P v ∗ P w}} 
      enqueue v w 
    {{ RET #(); True }}.
End spec.

















































