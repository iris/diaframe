From iris.algebra Require Import excl.
From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import own_hints.

Definition newlock : val := 
  λ: <>, ref #false.

Definition acquire : val :=
  rec: "acquire" "l" := 
    if: CAS "l" #false #true then 
      #() 
    else 
      "acquire" "l".

Definition release : val := 
  λ: "l", "l" <- #false.

Class lockG Σ := LockG { #[local] lock_tokG :: inG Σ (exclR unitO) }.
Definition lockΣ : gFunctors := #[GFunctor (exclR unitO)].

Local Obligation Tactic := program_verify.

Global Program Instance subG_lockΣ {Σ} : subG lockΣ Σ → lockG Σ.

Section proof.
  Context `{!heapGS Σ, !lockG Σ}.
  Let N := nroot .@ "spin_lock".

  Definition lock_inv (γ : gname) (l : loc) (R : iProp Σ) : iProp Σ :=
    ∃ b : bool, l ↦ #b ∗ (⌜b = true⌝ ∨ ⌜b = false⌝ ∗ own γ (Excl ()) ∗ R).

  Definition is_lock (γ : gname) (lk : val) (R : iProp Σ) : iProp Σ :=
    ∃ l: loc, ⌜lk = #l⌝ ∧ inv N (lock_inv γ l R).

  Definition locked γ := own γ (Excl ()).

  Global Program Instance newlock_spec :
    SPEC {{ True }}
      newlock #()
    {{ lk, RET (lk : val); ∀ R, R ={⊤}=∗ ∃ γ, is_lock γ lk R }}.

  Global Program Instance acquire_spec γ (lk : val) R :
    SPEC {{ is_lock γ lk R }} 
      acquire lk 
    {{ RET #(); proof.locked γ ∗ R }}.

  Global Program Instance release_spec γ (lk : val) R :
    SPEC {{ is_lock γ lk R ∗ locked γ ∗ R }} 
      release lk 
    {{ RET #(); True }}.

  Global Program Instance is_lock_persistent_reuse γ lk R : Persistent (is_lock γ lk R).
  Global Program Instance is_lock_ne_reuse γ l : NonExpansive (lock_inv γ l).
  Global Program Instance is_lock_contractive_reuse γ lk : Contractive (is_lock γ lk).
  Global Program Instance locked_timeless_reuse γ : Timeless (locked γ).
  Global Program Instance locked_exclusive_reuse γ : ExclusiveProp (locked γ).
End proof.

Global Opaque is_lock locked acquire release newlock.
