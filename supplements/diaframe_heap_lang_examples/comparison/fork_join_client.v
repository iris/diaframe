From diaframe.heap_lang Require Import proof_automation.
From diaframe.heap_lang.examples.comparison Require Import fork_join.

Definition client_thread : val :=
  λ: "l" "c", 
    "l" <- #1 ;;
    set "c" #().

Definition client : val :=
  λ: "_", 
    let: "x" := ref #0 in
    let: "c" := make_join #() in
    Fork (client_thread "x" "c") ;;
    wait "c" ;;
    ! "x".

Section spec.
  Context `{!heapGS Σ, forkjoinG Σ}.
  Obligation Tactic := program_verify.

  Global Program Instance thread_spec (v : val) (l : loc) :
    SPEC γ w, {{ is_join γ (λ _, l ↦ #1) v ∗ l ↦ w }}
      client_thread #l v
    {{ RET #(); True }}.

  Global Program Instance client_spec :
    SPEC {{ True }} 
      client #() 
    {{ RET #1; True }}.
End spec.