From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import frac_auth.
From diaframe.lib Require Import own_hints.

Definition make_counter : val := 
  λ: <>, ref #0.

Definition incr (max_state : positive) : val := 
  rec: "incr" "l" :=
    let: "n" := !"l" in
    let: "m" := 
      if: "n" < #(Zpos max_state) then 
        #1 + "n" 
      else 
        #0 
    in
    if: CAS "l" "n" "m" then 
      #() 
    else 
      "incr" "l".

Definition read : val := 
  λ: "l", !"l".

Class bcounterG Σ :=
  BCounterG { #[local] bcounter_inG :: inG Σ (frac_authR ZR) }.
Definition bcounterΣ : gFunctors :=
  #[GFunctor (frac_authR ZR)].

Local Obligation Tactic := program_verify.

Global Program Instance subG_bcounterΣ {Σ} : subG bcounterΣ Σ → bcounterG Σ.

(* TODO: investigate whether lia can actually help out here.
  I tried the stuff listed here https://coq.inria.fr/refman/addendum/micromega.html#coq:tacn.zify
  but it did not help *)


Section proof.
  Context `{!heapGS Σ, bcounterG Σ} (max_state : positive).
  Let N := nroot .@ "bnded_ctr".

  Definition counter_inv (γ : gname) (l : loc) : iProp Σ :=
    ∃ (z : Z), l ↦ #z ∗ ⌜0 ≤ z ≤ Zpos max_state⌝%Z ∗ own γ (●F z).

  Global Program Instance newcounter_spec :
    SPEC {{ True }}
      make_counter #()
    {{ γ (l : loc), RET #l; inv N (counter_inv γ l) ∗ own γ (◯F 0%Z) }}.

  Global Instance incr_spec γ (l : loc) q (z : Z) :
    SPEC {{ inv N (counter_inv γ l) ∗ own γ (◯F{q} z) }} 
      incr max_state #l
    {{ z', RET #(); own γ (◯F{q} z') ∗ ⌜eqm (Zpos max_state + 1) z' (z + 1)%Z⌝  }}.
  Proof.
    iSteps as (l' γ' q' z1 _ z2 Hz2 Hz2' Hz2'' _ _) "HI Hrec" /
           as (l' γ' q' z1 _ z2 Hz2 Hz2' _ _ _) "HI Hrec".
    - replace (0 + z1 - z2)%Z with (z1 + 1 - (Zpos max_state + 1))%Z by lia.
      by rewrite /eqm -(Z_mod_plus_full (z1 + 1)%Z (-1)%Z).
    - replace (1 + z2 + z1 - z2)%Z with (z1 + 1)%Z; last lia. done.
  Qed.

  Global Program Instance read_spec_1 γ (l : loc) (n : Z) :
    SPEC [own γ (◯F n)] {{ inv N (counter_inv γ l) }} 
      read #l
    {{ RET #n; ⌜0 ≤ n ≤ Zpos max_state⌝%Z ∗ own γ (◯F n) }}.

  Global Program Instance read_spec γ (l : loc) :
    SPEC [ε₁] {{ inv N (counter_inv γ l) }} 
      read #l
    {{ (c : Z), RET #c; ⌜0 ≤ c ≤ Zpos max_state⌝%Z}}.
End proof.















