From diaframe.heap_lang Require Import proof_automation.
From diaframe.lib Require Import frac_token.
From diaframe.heap_lang.examples.comparison Require Import spin_lock.

(* classic readers-writer lock from Courtois 1971 *)

Definition new_rwlock : val :=
  λ: <>, (ref #0, newlock #(), newlock #()).

Definition acquire_reader : val :=
  λ: "rwlk",
    acquire (Snd "rwlk") ;;
    let: "old_val" := ! (Fst (Fst "rwlk")) in
    (Fst (Fst "rwlk")) <- "old_val" + #1;;
    (if: "old_val" = #0 then
      acquire (Snd (Fst "rwlk"))
    else 
      #()
    ) ;;
    release (Snd "rwlk").

Definition release_reader : val :=
  λ: "rwlk",
    acquire (Snd "rwlk") ;;
    let: "old_val" := ! (Fst (Fst "rwlk")) in
    (Fst (Fst "rwlk")) <- "old_val" - #1;;
    (if: "old_val" = #1 then
      release (Snd (Fst "rwlk"))
    else 
      #()
    );;
    release (Snd "rwlk").

Definition upgrade_reader : val :=
  λ: "rwlk",
    acquire (Snd "rwlk") ;;
    let: "old_val" := ! (Fst (Fst "rwlk")) in
    (Fst (Fst "rwlk")) <- "old_val" - #1 ;;
    release (Snd "rwlk");;
    if: "old_val" = #1 then
      #()
    else
      acquire (Snd (Fst "rwlk")).

Definition acquire_writer : val :=
  λ: "rwlk", acquire (Snd (Fst "rwlk")).

Definition release_writer : val :=
  λ: "rwlk", release (Snd (Fst "rwlk")).

Section proof.
  Context `{!heapGS Σ, !lockG Σ, !fracTokenG Σ}.
  Implicit Types P : Qp → iProp Σ.
  Obligation Tactic := program_verify.

  Let N := nroot .@ "rw_lock".

  Definition lockA_inv P : iProp Σ := P 1%Qp.
  Definition lockB_inv P γA γt (reader_count : loc) : iProp Σ :=
    ∃ (z : Z), 
      reader_count ↦ #z ∗
        ((⌜0 < z⌝%Z ∗ token_counter P γt (Z.to_pos z) ∗ locked γA) ∨
          (⌜z = 0⌝%Z ∗ no_tokens P γt 1%Qp ∗ ⌜Fractional P⌝)).

  Definition is_rwlock P γA γB γt rwlk : iProp Σ :=
    ∃ (lkA lkB : val) (reader_count : loc), 
      ⌜rwlk = (#reader_count, lkA, lkB)%V⌝ ∗ is_lock γA lkA (lockA_inv P) ∗ is_lock γB lkB (lockB_inv P γA γt reader_count).

  Global Program Instance new_rwlock_spec :
    SPEC {{ True }} 
      new_rwlock #() 
    {{ (rwlk : val), RET rwlk; ∀ P, ⌜Fractional P⌝ ∗ P 1%Qp ={⊤}=∗ ∃ γt γA γB, is_rwlock P γA γB γt rwlk }}.

  Context P.

  Global Program Instance acquire_reader_spec γA γB γt (rwlk : val) :
    SPEC {{ is_rwlock P γA γB γt rwlk }} 
      acquire_reader rwlk 
    {{ RET #(); token P γt }}.

  Global Program Instance release_reader_spec γA γB γt (rwlk : val) :
    SPEC {{ is_rwlock P γA γB γt rwlk ∗ token P γt }}
      release_reader rwlk 
    {{ RET #(); True }}.

  Global Program Instance upgrade_reader_spec γA γB γt (rwlk : val) :
    SPEC {{ is_rwlock P γA γB γt rwlk ∗ token P γt }}
      upgrade_reader rwlk 
    {{ RET #(); P 1 ∗ locked γA }}.

  Global Program Instance acquire_writer_spec γA γB γt (rwlk : val) :
    SPEC {{ is_rwlock P γA γB γt rwlk }}
      acquire_writer rwlk 
    {{ RET #(); P 1 ∗ locked γA}}.

  Global Program Instance release_writer_spec γA γB γt (rwlk : val) :
    SPEC {{ is_rwlock P γA γB γt rwlk ∗ P 1 ∗ locked γA }}
      release_writer rwlk 
    {{ RET #(); True }}.
End proof.








