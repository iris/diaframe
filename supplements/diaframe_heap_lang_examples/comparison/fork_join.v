From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import excl.

Definition make_join : val :=
  λ: "_", ref NONE.

Definition set : val :=
  λ: "c" "v",
    "c" <- SOME ("v").

Definition wait : val :=
  rec: "join" "c" :=
    match: !"c" with
      SOME "x" => "x"
    | NONE => "join" "c"
    end.

Class forkjoinG Σ := ForkJoinG { #[local] spawn_tokG :: inG Σ (exclR unitO) }.
Definition forkjoinΣ : gFunctors := #[GFunctor (exclR unitO)].

Local Obligation Tactic := program_verify.

Global Program Instance subG_forkjoinΣ {Σ} : subG forkjoinΣ Σ → forkjoinG Σ.

Section proof.
  Context `{!heapGS Σ, !forkjoinG Σ}.
  Let N := nroot.@"spawn".

  Definition spawn_inv γ l (Ψ : val → iProp Σ) : iProp Σ :=
    ∃ lv, l ↦ lv ∗ (⌜lv = NONEV⌝ ∨
                    ∃ w, ⌜lv = SOMEV w⌝ ∗ (own γ (Excl ()) ∨ Ψ w)).

  Definition is_join γ Ψ (v : val) : iProp Σ :=
    ∃ (l : loc), ⌜v = #l⌝ ∗ inv N (spawn_inv γ l Ψ).

  Definition join_handle γ : iProp Σ := own γ (Excl ()).

  Global Program Instance make_join_spec :
    SPEC {{ True }} 
      make_join #() 
    {{ (v : val), RET v; ∀ Ψ, |={⊤}=> ∃ γ, join_handle γ ∗ is_join γ Ψ v }}.

  Global Program Instance fork_set_spec (Ψ : val → iProp Σ) γ (v w : val) :
    SPEC {{ is_join γ Ψ v ∗ Ψ w }} 
      set v w
    {{ RET #(); True }}.

  Global Program Instance wait_spec γ (Ψ : val → iProp Σ) (v : val) :
    SPEC {{ is_join γ Ψ v ∗ join_handle γ }} 
      wait v
    {{ w, RET (w : val); Ψ w }}.

  Global Program Instance is_join_persistent_reuse γ Ψ v : Persistent (is_join γ Ψ v).
End proof.

Global Opaque is_join join_handle make_join set wait.














