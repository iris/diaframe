From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.algebra Require Import frac_auth numbers excl.
From diaframe.lib Require Import int_as_nat_diff own_hints.

Definition make_counter : val := 
  λ: <>, ref #0.

Definition incr : val := 
  rec: "incr" "l" "k" :=
    let: "n" := !"l" in
    if: CAS "l" "n" ("n" + "k") then 
      #() 
    else 
      "incr" "l" "k".

Definition decr : val := 
  rec: "decr" "l" "k" :=
    let: "n" := !"l" in
    if: CAS "l" "n" ("n" - "k") then 
      #() 
    else 
      "decr" "l" "k".

Definition read : val := 
  λ: "l", !"l".

Definition inc_decR := authR $ prodUR (optionUR $ exclR natO) (optionUR $ exclR natO).
Class inc_decG Σ :=
  IncDecG { #[local] incdec_inG :: inG Σ inc_decR }.
Definition inc_decΣ : gFunctors :=
  #[GFunctor inc_decR].

Local Obligation Tactic := program_verify.

Global Program Instance subG_incdecΣ {Σ} : subG inc_decΣ Σ → inc_decG Σ.

Section proof.
  Context `{!heapGS Σ, inc_decG Σ}.
  Let N := nroot .@ "incdec".

  Definition inc_dec_inv (γ : gname) (l : loc) : iProp Σ :=
    ∃ (z : Z), l ↦ #z ∗ ∃ (n1 n2 : nat), int_as_nat_diff z n1 n2 ∗ own γ (● (Excl' n1, Excl' n2)).

  Global Program Instance newcounter_spec :
    SPEC {{ True }} 
      make_counter #()
    {{ γ (l : loc), RET #l; inv N (inc_dec_inv γ l) ∗ own γ (◯ (Excl' 0, Excl' 0)) }}.

  Global Program Instance incr_spec γ (l : loc) (k : nat) n :
    SPEC {{ inv N (inc_dec_inv γ l) ∗ own γ (◯ (Excl' n, ε)) ∗ ⌜0 < k⌝ }} 
      incr #l #k
    {{ RET #(); own γ (◯ (Excl' (n + k), ε)) }}.

  Global Program Instance decr_spec γ (l : loc) (k : nat) n :
    SPEC {{ inv N (inc_dec_inv γ l) ∗ own γ (◯ (ε, Excl' n)) ∗ ⌜0 < k⌝ }} 
      decr #l #k
    {{ RET #(); own γ (◯ (ε, Excl' (n + k))) }}.

  Global Program Instance read_spec_incr γ (l : loc) (n : nat) :
    SPEC [own γ (◯ (Excl' n, ε))] {{ inv N (inc_dec_inv γ l) }} 
      read #l
    {{ (c : Z), RET #c; ⌜c ≤ n⌝%Z ∗ own γ (◯ (Excl' n, ε)) }} | 30.

  Global Program Instance read_spec_decr γ (l : loc) (n : nat) :
    SPEC [own γ (◯ (ε, Excl' n))] {{ inv N (inc_dec_inv γ l) }} 
      read #l
    {{ (c : Z), RET #c; ⌜-n ≤ c⌝%Z ∗ own γ (◯ (ε, Excl' n)) }} | 30.

  Global Program Instance read_spec_exact γ (l : loc) (n m : nat) :
    SPEC [own γ (◯ (Excl' m, Excl' n))] {{ inv N (inc_dec_inv γ l) }} 
      read #l
    {{ RET #(m - n); own γ (◯ (Excl' m, Excl' n)) }} | 20.

  Global Program Instance read_spec γ (l : loc) :
    SPEC [ε₁] {{ inv N (inc_dec_inv γ l) }} 
      read #l
    {{ (c : Z), RET #c; True }} | 50.
End proof.




