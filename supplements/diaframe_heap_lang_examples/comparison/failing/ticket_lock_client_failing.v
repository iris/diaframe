From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.heap_lang.lib Require Import par.
From diaframe.heap_lang.examples.comparison Require Import ticket_lock.

Definition foo : val :=
  λ: "lk" "l" "z",
    acquire "lk";;
    "l" <- "z";;
    "l" +ₗ #1 <- "z";;
    release "lk".

Definition run_foo : val :=
  λ: <>, 
    let: "lk" := newlock #() in
    let:  "l" := AllocN #2 #0 in
    (foo "lk" "l" #3 ||| foo "lk" "l" # 5);;
    acquire "lk";;
    if: ! "l" = ! ("l" +ₗ #1) then 
      #()
    else 
      #() #() (* unsafe *).

Section spec.
  Context `{!heapGS Σ, tlockG Σ, spawnG Σ}.

  Obligation Tactic := program_verify.

  Definition same_vals l : iProp Σ := ∃ (z : Z), l ↦ #z ∗ (l +ₗ 1) ↦ #z.

  Definition bad_foo : val :=
    λ: "lk" "l" "z",
      acquire "lk";;
      "l" <- "z" + #1 ;;
      "l" +ₗ #1 <- "z";;
      release "lk".

  Global Instance foo_spec_bad_code (lk : val) (l : loc) (z : Z) :
    SPEC γ1 γ2, {{ is_lock γ1 γ2 lk (same_vals l) }}
      bad_foo lk #l #z
    {{ RET #(); True}}.
  Proof. Time Fail solve [iSteps]. Abort.

  Global Instance foo_spec_bad_post (lk : val) (l : loc) (z : Z) :
    SPEC γ1 γ2, {{ is_lock γ1 γ2 lk (same_vals l) }}
      bad_foo lk #l #z
    {{ RET #(); l ↦ #z }}.
  Proof. Time Fail solve [iSteps]. Abort.
End spec.







