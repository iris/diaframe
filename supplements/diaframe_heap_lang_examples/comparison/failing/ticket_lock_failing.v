From iris.algebra Require Import coPset excl auth.
From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.heap_lang Require Import proofmode.
From diaframe.lib Require Import ticket_cmra own_hints.

Definition wait_loop : val :=
  rec: "wait_loop" "x" "lk" :=
    let: "o" := !(Fst "lk") in
    if: "x" = "o" then 
      #() (* my turn *)
    else 
      "wait_loop" "x" "lk".

Definition newlock : val :=
  λ: <>, ((* owner *) ref #0, (* next *) ref #0).

Definition acquire : val :=
  rec: "acquire" "lk" :=
    let: "n" := !(Snd "lk") in
    if: CAS (Snd "lk") "n" ("n" + #1) then 
      wait_loop "n" "lk"
    else 
      "acquire" "lk".

Definition release : val :=
  λ: "lk", (Fst "lk") <- !(Fst "lk") + #1.

Definition tlockUR := authR $ optionUR $ exclR $ natO.
Class tlockG Σ := {
  #[local] tlock_ticketG :: inG Σ ticketR ;
  #[local] tlock_tlockG :: inG Σ tlockR;
}.
Definition tlockΣ : gFunctors :=
  #[ GFunctor ticketR; GFunctor tlockR ].

Local Obligation Tactic := program_verify.

Global Program Instance subG_tlockΣ {Σ} : subG tlockΣ Σ → tlockG Σ.

Section proof.
  Context `{!heapGS Σ, !tlockG Σ}.
  Let N := nroot .@ "ticket_lock".

  Definition lock_inv1 γ1 (ln : loc) : iProp Σ :=
    ∃ n : nat, ln ↦ #n ∗ own γ1 (CoPset $ tickets_geq n).

  Definition lock_inv2_bad (γ1 γ2 : gname) (lo : loc) (R : iProp Σ) : iProp Σ :=
    ∃ o : nat, lo ↦ #o ∗ own γ2 (● Excl' o) ∗ (
        (own γ2 (◯ (Excl' o)) ∗ R) ∨ (own γ1 (CoPset $ ticket o))).

  Definition is_lock_bad (γ1 γ2 : gname) (lk : val) (R : iProp Σ) : iProp Σ :=
    ∃ lo ln : loc,
      ⌜lk = (#lo, #ln)%V⌝ ∗ inv N (lock_inv1 γ1 ln) ∗ inv N (lock_inv2_bad γ1 γ2 lo R).

  Definition issued (γ : gname) (x : nat) : iProp Σ :=
    own γ (CoPset $ ticket x).

  Definition locked (γ : gname) : iProp Σ := ∃ o, own γ (◯ Excl' o).

  Global Program Instance newlock_spec :
    SPEC {{ True }} 
      newlock #() 
    {{ (lk : val), RET lk; ∀ R, R ={⊤}=∗ ∃ γ1 γ2, is_lock_bad γ1 γ2 lk R }}.

  Instance wait_loop_spec_bad_inv γ1 γ2 (lk : val) (x : nat) R :
    SPEC {{ is_lock_bad γ1 γ2 lk R ∗ issued γ1 x }} 
      wait_loop #x lk 
    {{ RET #(); locked γ2 ∗ R }}.
  Proof.
    Time Fail solve [iSteps]. 
    (* fails since the order of disjunctions in lock_inv2 should be swapped *)
  Abort.

  Definition lock_inv2_good (γ1 γ2 : gname) (lo : loc) (R : iProp Σ) : iProp Σ :=
    ∃ o : nat, lo ↦ #o ∗ own γ2 (● Excl' o) ∗ (
        (own γ1 (CoPset $ ticket o)) ∨ (own γ2 (◯ (Excl' o)) ∗ R)).

  Definition is_lock (γ1 γ2 : gname) (lk : val) (R : iProp Σ) : iProp Σ :=
    ∃ lo ln : loc,
      ⌜lk = (#lo, #ln)%V⌝ ∗ inv N (lock_inv1 γ1 ln) ∗ inv N (lock_inv2_good γ1 γ2 lo R).

  Instance wait_loop_spec_bad_post γ1 γ2 (lk : val) (x : nat) R :
    SPEC {{ is_lock γ1 γ2 lk R ∗ issued γ1 x }} 
      wait_loop #x lk 
    {{ RET #(); own γ2 (◯ Excl' (S x)) ∗ R }}.
  Proof.
    Time Fail solve [iSteps]. (* NB: final state is helpful *)
  Abort.

  Definition bad_wait_loop : val :=
    rec: "wait_loop" "x" "lk" :=
      let: "o" := !(Fst "lk") in
      if: "x" + #1 = "o" then 
        #() (* my turn *)
      else 
        "wait_loop" "x" "lk".

  Instance wait_loop_spec_bad_code γ1 γ2 (lk : val) (x : nat) R :
    SPEC {{ is_lock γ1 γ2 lk R ∗ issued γ1 x }} 
      bad_wait_loop #x lk 
    {{ RET #(); own γ2 (◯ Excl' x) ∗ R }}.
  Proof.
    Time Fail solve [iSteps]. (* final state is moderately helpful: shows we have ticket but need Excl *)
  Abort.
End proof.




















