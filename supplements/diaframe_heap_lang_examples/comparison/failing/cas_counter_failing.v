From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import frac_auth numbers.

Definition make_counter : val := 
  λ: <>, ref #0.

Definition incr : val := 
  rec: "incr" "l" :=
    let: "n" := !"l" in
    if: CAS "l" "n" (#1 + "n") then 
      #() 
    else 
      "incr" "l".

Definition read : val := 
  λ: "l", !"l".

Class counterG Σ :=
  CounterG { #[local] counter_inG :: inG Σ (frac_authR natR) }.
Definition counterΣ : gFunctors :=
  #[GFunctor (frac_authR natR)].

Local Obligation Tactic := program_verify.

Global Program Instance subG_counterΣ {Σ} : subG counterΣ Σ → counterG Σ.

Section proof.
  Context `{!heapGS Σ, counterG Σ}.
  Let N := nroot.@"counter".

  Definition counter_inv (γ : gname) (l : loc) : iProp Σ :=
    ∃ (z : Z), l ↦ #z ∗ ⌜0 ≤ z⌝%Z ∗ own γ (●F (Z.to_nat z)).

  Global Program Instance newcounter_spec :
    SPEC {{ True }} 
      make_counter #()
    {{ γ (l : loc), RET #l; inv N (counter_inv γ l) ∗ own γ (◯F 0) }}.

  Global Instance incr_spec_bad_post γ (l : loc) q n :
    SPEC {{ inv N (counter_inv γ l) ∗ own γ (◯F{q} n) }} 
      incr #l
    {{ RET #(); own γ (◯F{q} (S $ S n)) }}.
  Proof.
    Time Fail solve [iSteps].
  Abort.

  Definition bad_incr : val := 
    rec: "incr" "l" :=
      let: "n" := !"l" in
      if: CAS "l" (#1 + "n") "n" then 
        #() 
      else 
        "incr" "l".

  Global Instance incr_spec_bad_code γ (l : loc) q n :
    SPEC {{ inv N (counter_inv γ l) ∗ own γ (◯F{q} n) }} 
      bad_incr #l
    {{ RET #(); own γ (◯F{q} (S n)) }}.
  Proof.
    Time Fail solve [iSteps].
  Abort.
End proof.


























