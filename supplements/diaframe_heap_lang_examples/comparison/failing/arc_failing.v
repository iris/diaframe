From diaframe.heap_lang Require Import proof_automation.
From diaframe.lib Require Import frac_token.

Definition mk_arc : val :=
  λ: "_", ref #1.

Definition clone : val :=
  λ: "a", FAA "a" #1.

Definition count : val :=
  λ: "a", ! "a".

Definition drop : val :=
  λ: "a", 
    let: "old_val" := FAA "a" #(-1) in
    if: "old_val" = #1 then
      Free "a";; 
      #true
    else 
      #false.

Section spec.
  Context `{!heapGS Σ, !fracTokenG Σ}.
  Let N := nroot .@ "arc".

  Obligation Tactic := program_verify.

  Context (P : Qp → iProp Σ).
  Context {HP : Fractional P}.

  Definition arc_inv γ l : iProp Σ := 
    (no_tokens P γ (1/2)%Qp) ∨ (∃ (z : Z), l ↦ #z ∗ ⌜0 < z⌝%Z ∗ token_counter P γ (Z.to_pos z)).

  Global Program Instance trivial_or_as_forall γ R q E : (* the fupd is required to eleminate ⋄ from ⋄False *)
    IntoWand2 false (no_tokens P γ q ∨ R)%I (token P γ) (|={E}=> token P γ ∗ R)%I.

  Program Instance mk_arc_spec :
    SPEC {{ P 1 }} 
      mk_arc #() 
    {{ (l : loc) γ, RET #l; inv N (arc_inv γ l) ∗ token P γ }}.

  Global Instance clone_arc_spec_bad_post γ (l : loc) :
    SPEC {{ token P γ ∗ inv N (arc_inv γ l) }} 
      clone #l 
    {{ (z : Z), RET #z; ⌜0 < z⌝%Z ∗ token P γ ∗ token P γ ∗ token P γ }}.
  Proof.
    Fail Time solve [iSteps].
  Abort.

  Definition bad_clone : val :=
    λ: "a", FAA "a" #-1.

  Global Instance clone_arc_spec_bad_post γ (l : loc) :
    SPEC {{ token P γ ∗ inv N (arc_inv γ l) }} 
      bad_clone #l 
    {{ (z : Z), RET #z; ⌜0 < z⌝%Z ∗ token P γ ∗ token P γ }}.
  Proof.
    Fail Time solve [iSteps].
  Abort.
End spec.












