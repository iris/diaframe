From diaframe.heap_lang Require Import proof_automation.
From diaframe.lib Require Import frac_token.

Definition mk_arc : val :=
  λ: "_", ref #1.

Definition clone : val :=
  λ: "a", FAA "a" #1.

Definition count : val :=
  λ: "a", ! "a".

Definition drop : val :=
  λ: "a", 
    let: "old_val" := FAA "a" #(-1) in
    if: "old_val" = #1 then
      Free "a";; 
      #true
    else 
      #false.

Section spec.
  Context `{!heapGS Σ, !fracTokenG Σ}.
  Let N := nroot .@ "arc".

  Obligation Tactic := program_verify.

  Definition arc_inv P γ l : iProp Σ := 
    (no_tokens P γ (1/2)%Qp) ∨ (∃ (p : positive), l ↦ #(Zpos p) ∗ token_counter P γ p).

  Global Program Instance mk_arc_spec :
    SPEC {{ True }} 
      mk_arc #() 
    {{ (l : loc), RET #l; ∀ P, ⌜Fractional P⌝ ∗ P 1%Qp ={⊤}=∗ ∃ γ, inv N (arc_inv P γ l) ∗ token P γ }}.

  Context (P : Qp → iProp Σ).

  Global Program Instance clone_arc_spec γ (l : loc) :
    SPEC {{ token P γ ∗ inv N (arc_inv P γ l) }} 
      clone #l 
    {{ (z : Z), RET #z; ⌜0 < z⌝%Z ∗ token P γ ∗ token P γ }}.

  Global Program Instance count_arc_spec γ (l : loc) :
    SPEC {{ token P γ ∗ inv N (arc_inv P γ l) }} 
      count #l 
    {{ (z : Z), RET #z; ⌜0 < z⌝%Z ∗ token P γ }}.

  Global Program Instance drop_arc_spec γ (l : loc) :
    SPEC {{ token P γ ∗ inv N (arc_inv P γ l) }} 
      drop #l 
    {{ (b : bool), RET #b; 
        ⌜b = true⌝ ∗ no_tokens P γ (1/2)%Qp ∗ P 1 ∨ ⌜b = false⌝ }}.
End spec.












