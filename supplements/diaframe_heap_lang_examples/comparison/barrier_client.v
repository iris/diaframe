From diaframe.heap_lang Require Import proof_automation.
From iris.algebra Require Import frac_auth numbers csum agree.
From iris.heap_lang Require Import proofmode.
From diaframe.lib Require Import frac_token int_as_nat_diff own_hints.
From diaframe.heap_lang.examples.comparison Require Import barrier.

Definition increment : val := 
  rec: "inc" "c" := 
    let: "v" := ! "c" in
    if: CAS "c" "v" ("v" + #1) then 
      #()
    else 
      "inc" "c".

Definition decrement : val := 
  rec: "dec" "c" := 
    let: "v" := ! "c" in
    if: CAS "c" "v" ("v" - #1) then 
      #()
    else 
      "dec" "c".

Definition check_rising : val := 
  λ: "c",
    let: "a" := ! "c" in
    let: "b" := ! "c" in
    if: "b" < "a" then 
      #() #() (* unsafe *)
    else 
      #().

Definition check_falling : val := 
  λ: "c",
    let: "a" := ! "c" in
    let: "b" := ! "c" in
    if: "a" < "b" then 
      #() #() (* unsafe *)
    else 
      #().

Definition client_thread p : val := 
  λ: "c" "b",
    increment "c";;
    check_rising "c" ;;
    sync_up p "b" ;; 
    decrement "c" ;;
    check_falling "c".

Definition client_run p : val := 
  rec: "spawn_clients" "c" "b" "n" :=
    if: "n" = #0 
    then 
      #()
    else 
      Fork (client_thread p "c" "b") ;;
      "spawn_clients" "c" "b" ("n" - #1).

Definition client p : val := 
  λ: <>, 
    let: "c" := ref #0 in
    let: "b" := make_barrier #() in
    client_run p "c" "b" #(Zpos p).

Definition barrier_clientR := frac_authR $ prodR (csumR max_natR max_natR) $ agreeR natO.
Class barrier_clientG Σ :=
  BarrierClientG { 
    #[local] barrier_client_clientG :: inG Σ barrier_clientR;
    #[local] barrier_client_barrierG :: barrierG Σ
  }.
Definition barrier_clientΣ : gFunctors :=
  #[GFunctor barrier_clientUR; barrierΣ].

Local Obligation Tactic := program_verify.

Global Program Instance subG_barrier_clientΣ {Σ} : subG barrier_clientΣ Σ → barrier_clientG Σ.

Section proof.
  Context `{!heapGS Σ, barrier_clientG Σ}.
  Let N := nroot.@"client".

  Definition counter_inv γ (l : loc) : iProp Σ :=
    ∃ (z : Z), l ↦ #z ∗ ∃ (n1 n2 : nat), int_as_nat_diff z n1 n2 ∗
      (own γ (●F (Cinl $ MaxNat n1, to_agree n2)) ∨ own γ (●F (Cinr $ MaxNat n2, to_agree n1))).

  Definition P1 γ : Qp → iProp Σ := λ q,
    (∃ (n1 n2 : nat), own γ (◯F{q} (Cinl $ MaxNat n1, to_agree n2)))%I.

  Definition P2 γ : Qp → iProp Σ := λ q,
    (∃ (n1 n2 : nat), own γ (◯F{q} (Cinr $ MaxNat n1, to_agree n2)))%I.

  Instance p1_fractional γ : Fractional (P1 γ).
  Proof. rewrite /Fractional => p q. apply (anti_symm _); iSteps. Qed.
  Instance p2_fractional γ : Fractional (P2 γ).
  Proof. rewrite /Fractional => p q. apply (anti_symm _); iSteps. Qed.

  Definition is_counter_barrier threads γp1 γp2 γt γb (v : val) γc : iProp Σ :=
    is_barrier threads (P1 γc) (P2 γc) emp%I γp1 γp2 γt γb (v : val).

  Program Instance increment_spec γc (l : loc) :
    SPEC q, {{ P1 γc q ∗ inv N (counter_inv γc l) }}
      increment #l
    {{ RET #(); P1 γc q }}.

  Instance check_rising_spec γc (l : loc) :
    SPEC q, {{ P1 γc q ∗ inv N (counter_inv γc l) }}
      check_rising #l
    {{ RET #(); P1 γc q }}.
  Proof.
    iStep 7 as (q n m k Hnm) "HI H£ Hcl H● H◯".
    assert (m = m `max` n) as Hnm' by lia. rewrite {2}Hnm'.
    iSteps.
  Qed.

  Program Instance decrement_spec γc (l : loc) :
    SPEC q, {{ P2 γc q ∗ inv N (counter_inv γc l) }}
      decrement #l
    {{ RET #(); P2 γc q }}.

  Instance check_falling_spec γc (l : loc) :
    SPEC q, {{ P2 γc q ∗ inv N (counter_inv γc l) }}
      check_falling #l
    {{ RET #(); P2 γc q }}.
  Proof.
    iStep 7 as (q n m k Hnk) "HI H£ Hcl H● H◯".
    assert (k = k `max` n) as Hnk' by lia. rewrite {2}Hnk'.
    iSteps.
  Qed.

  Program Instance client_thread_spec threads γp1 γp2 γt γb (v : val) γc (l : loc) :
    SPEC {{ is_counter_barrier threads γp1 γp2 γt γb v γc ∗ token (P1 γc) γp1 ∗ inv N (counter_inv γc l) }}
      client_thread threads #l v
    {{ RET #(); token (P2 γc) γp2 }}.

  Program Instance client_run_spec threads (v : val) (l : loc) (z : Z) :
    SPEC γp1 γp2 γt γb γc, {{ inv N (counter_inv γc l) ∗ ⌜0 ≤ z⌝%Z ∗ is_counter_barrier threads γp1 γp2 γt γb v γc ∗ 
          token_iter (P1 γc) (Z.to_nat z) γp1 }}
      client_run threads #l v #z
    {{ RET #(); True }}.

  Global Instance client_spec threads :
    SPEC {{ True }}
      client threads #()
    {{ RET #(); True }}.
  Proof.
    iSteps as_anon / as (l v γ) "HI H£ H◯" /
                     as (l v γ) "HI H£"; [iPureIntro; tc_solve..| ].
    unseal_diaframe => /=.
    iSplitR; [ | iSplitR; [ | iSteps]].
    all: iStep 2 as (n m) "H◯"; iInv N as "HN"; iDecompose "HN" as "H● Hl"; iSmash.
  Qed.
End proof.



