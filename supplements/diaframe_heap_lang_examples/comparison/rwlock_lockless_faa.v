From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import frac_token.

Definition new_rwlock : val :=
  λ: "_", ref (#0).

Definition acquire_reader : val :=
  rec: "acquire_rdr" "rwlk" :=
    let: "z" := ! "rwlk" in
    if: "z" = #-1 then
      "acquire_rdr" "rwlk"
    else 
      if: CAS "rwlk" "z" ("z" + #1) then 
        #() 
      else 
        "acquire_rdr" "rwlk".

Definition release_reader : val :=
  λ: "rwlk", (FAA "rwlk" #-1) ;; #().

Definition acquire_writer : val :=
  rec: "acquire_wrtr" "rwlk" :=
    if: CAS "rwlk" (#0) #-1 then 
      #() 
    else 
      "acquire_wrtr" "rwlk".

Definition release_writer : val :=
  λ: "rwlk", "rwlk" <- #0.

Section proof.
  Context `{!heapGS Σ, fracTokenG Σ}.
  Let N := nroot .@ "rw_lock".

  Obligation Tactic := program_verify.

  Definition rwlock_inv P γt (rw_loc : loc) : iProp Σ :=
    ∃ (z : Z), 
      rw_loc ↦ #z ∗
        ((⌜0 < z⌝%Z ∗ token_counter P γt (Z.to_pos z)) ∨
         (⌜z = 0⌝%Z ∗ no_tokens P γt 1 ∗ ⌜Fractional P⌝ ∗ P 1%Qp) ∨
         (⌜z = -1⌝%Z ∗ no_tokens P γt (1/2)%Qp ∗ ⌜Fractional P⌝)).

  Definition is_rwlock P γt rwlk : iProp Σ :=
    ∃ (rw_loc : loc), 
      ⌜rwlk = #rw_loc⌝ ∗ inv N (rwlock_inv P γt rw_loc).

  Global Program Instance new_rwlock_spec :
    SPEC {{ True }} 
      new_rwlock #() 
    {{ (rwlk : val), RET rwlk; ∀ P, ⌜Fractional P⌝ ∗ P 1%Qp ={⊤}=∗ ∃ γt, is_rwlock P γt rwlk }}.

  Context (P : Qp → iProp Σ).

  Global Program Instance acquire_reader_spec γt (rwlk : val) :
    SPEC {{ is_rwlock P γt rwlk }} 
      acquire_reader rwlk 
    {{ RET #(); token P γt }}.

  Global Program Instance release_reader_spec γt (rwlk : val) :
    SPEC {{ is_rwlock P γt rwlk ∗ token P γt }}
      release_reader rwlk
    {{ RET #(); True }}.

  Global Program Instance acquire_writer_spec γt (rwlk : val) :
    SPEC {{ is_rwlock P γt rwlk }}
      acquire_writer rwlk
    {{ RET #(); P 1 ∗ no_tokens P γt (1/2)%Qp }}.

  Global Program Instance release_writer_spec γt (rwlk : val) :
    SPEC {{ is_rwlock P γt rwlk ∗ P 1 ∗ no_tokens P γt (1/2)%Qp }}
      release_writer rwlk 
    {{ RET #(); True }}.
End proof.



















