From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob wp_later_credits.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import frac_auth numbers excl.
From diaframe.lib Require Import ticket_cmra frac_token own_hints later_credits.

Definition make_barrier : val := 
  λ: <>, ref #0.

Definition peek : val :=
  λ: "b", ! "b".

Definition sync_up_enter : val := rec: "sync_enter" "b" :=
  let: "z" := peek "b" in
  if: #0 ≤ "z" then
    if: CAS "b" "z" ("z" + #1) then 
      "z" 
    else 
      "sync_enter" "b"
  else "sync_enter" "b".

Definition sync_up_exit threads : val := rec: "sync_exit" "b" "v" :=
  let: "w" := 
    if: "v" = #0 then 
      #(Zpos threads) 
    else 
      "v" 
    in
  let: "z" := ! "b" in
  if: "z" = "w" then
      "b" <- "z" - #1
  else
    "sync_exit" "b" "v".

Definition sync_up threads : val := λ: "b",
  sync_up_exit threads "b" (sync_up_enter "b").

Definition sync_down_enter : val := rec: "sync_enter" "b" :=
  let: "z" := peek "b" in
  if: "z" ≤ #0 then
    if: CAS "b" "z" ("z" - #1) then
     - "z" 
    else 
      "sync_enter" "b"
  else 
    "sync_enter" "b".

Definition sync_down_exit threads : val := rec: "sync_exit" "b" "v" :=
  let: "w" := 
    if: "v" = #0 then 
      #(Zneg threads) 
    else 
      -"v" 
    in
  let: "z" := ! "b" in
  if: "z" = "w" then 
    "b" <- "z" + #1
  else 
    "sync_exit" "b" "v".

Definition sync_down threads : val := λ: "b",
  sync_down_exit threads "b" (sync_down_enter "b").

Definition barrierR := authR $ optionUR $ exclR $ leibnizO Z.
Class barrierG Σ :=
  BarrierG { 
    #[local] barrier_ticketG :: inG Σ coPset_disjR;
    #[local] barrier_tokenG :: fracTokenG Σ;
    #[local] barrier_barrierG :: inG Σ barrierR;
  }.
Definition barrierΣ : gFunctors :=
  #[GFunctor ticketUR; fracTokenΣ; GFunctor barrierR].

Local Obligation Tactic := program_verify.

Global Program Instance subG_barrierΣ {Σ} : subG barrierΣ Σ → barrierG Σ.

Section proof.
  Context `{!heapGS Σ} `{!barrierG Σ}.
  Context (threads : positive).
  Implicit Types P : Qp → iProp Σ.
  Let N := nroot .@ "barrier".
  Set Default Proof Using "threads heapGS0 barrierG0".

  Definition barrier_inv P1 P2 R γp1 γp2 γt γb (l : loc) : iProp Σ :=
    ∃ (z : Z), l ↦ #z ∗ own γt (CoPset $ tickets_geq (S (Z.abs_nat z))) ∗ own γb (● Excl' z) ∗ (
      (⌜0 < z⌝%Z ∗ (
          (⌜z < Zpos threads⌝%Z ∗ 
            ((own γt (CoPset $ ticket (Z.abs_nat z)) 
              ∗ ((⌜Fractional P1⌝ ∗ no_tokens P1 γp1 1%Qp ∗ own γt (CoPset $ ticket 0) ∗ token_counter P2 γp2 (Z.to_pos (Zpos threads - z)))
                  ∨
                 (own γb (◯ Excl' z) ∗ token_counter P1 γp1 (Z.to_pos (Zpos threads - z)) ∗ no_tokens P2 γp2 1%Qp ∗ ⌜Fractional P2⌝)
                ))
                ∨ 
            ((own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket 0) ∗ no_tokens P1 γp1 1%Qp ∗ ⌜Fractional P1⌝ ∗ token_counter P2 γp2 (Z.to_pos (Zpos threads - z))))
          )
                ∨
          (⌜z = Zpos threads⌝ ∗ no_tokens P1 γp1 1%Qp ∗ no_tokens P2 γp2 1%Qp ∗ P1 1%Qp ∗ ⌜Fractional P1⌝ ∗ ⌜Fractional P2⌝ ∗ (own γt (CoPset $ ticket 0) ∨ own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket $ Pos.to_nat threads))))
        ∨ 
      (⌜z < 0⌝%Z ∗ (
          (⌜Zneg threads < z⌝%Z ∗ 
            ((own γt (CoPset $ ticket (Z.abs_nat z)) 
              ∗ ((⌜Fractional P2⌝ ∗ no_tokens P2 γp2 1%Qp ∗ own γt (CoPset $ ticket 0) ∗ token_counter P1 γp1 (Z.to_pos (Zpos threads + z)))
                  ∨
                 (own γb (◯ Excl' z) ∗ token_counter P2 γp2 (Z.to_pos (Zpos threads + z)) ∗ no_tokens P1 γp1 1%Qp ∗ ⌜Fractional P1⌝)
                ))
                ∨ 
            ((own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket 0) ∗ no_tokens P2 γp2 1%Qp ∗ ⌜Fractional P2⌝ ∗ token_counter P1 γp1 (Z.to_pos (Zpos threads + z))))
          )
                ∨
          (⌜z = Zneg threads⌝ ∗ no_tokens P1 γp1 1%Qp ∗ no_tokens P2 γp2 1%Qp ∗ P1 1%Qp ∗ ⌜Fractional P1⌝ ∗ ⌜Fractional P2⌝ ∗ (own γt (CoPset $ ticket 0) ∨ own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket (Pos.to_nat threads)))))
        ∨
      (⌜z = 0⌝%Z ∗ (
        own γt (CoPset $ ticket 0) ∗ own γb (◯ Excl' z) ∗ (
          (⌜Fractional P2⌝ ∗ no_tokens P2 γp2 1%Qp ∗ token_counter P1 γp1 threads)
          ∨
          (⌜Fractional P1⌝ ∗ no_tokens P1 γp1 1%Qp ∗ token_counter P2 γp2 threads)
        )
      ))
    ) ∗ R.

  Definition atomic_wand L H : iProp Σ := L -∗ H.
  Notation "L '-A∗' H" := (atomic_wand L H) (at level 99, H at level 200, right associativity).
  Typeclasses Opaque atomic_wand.
  Global Instance atomic_wand_as_wand L H : AtomIntoConnective (L -A∗ H) (L -∗ H).
  Proof. rewrite /atomic_wand /AtomIntoConnective //. Qed.

  Definition is_barrier P1 P2 R γp1 γp2 γt γb (v : val) : iProp Σ :=
    ∃ (l : loc), ⌜v = #l⌝ ∗ □ (R ∗ P1 1%Qp -A∗ |={⊤∖↑N}=> P2 1%Qp ∗ ◇R) ∗ □ (R ∗ P2 1%Qp -A∗ |={⊤∖↑N}=> P1 1%Qp ∗ ◇R) ∗ inv N (barrier_inv P1 P2 R γp1 γp2 γt γb l).

  Global Instance newbarrier_spec :
    SPEC {{ True }} 
      make_barrier #()
    {{ (v : val), RET v; ∀ P1 P2 R, 
        ⌜Fractional P1⌝ ∗ ⌜Fractional P2⌝ ∗ P1 1%Qp ∗ R ∗ □ (R ∗ P1 1%Qp ={⊤∖↑N}=∗ P2 1%Qp ∗ ◇R) ∗ 
          □ (R ∗ P2 1%Qp ={⊤∖↑N}=∗ P1 1%Qp ∗ ◇ R) ={⊤}=∗ 
            ∃ γp1 γp2 γt γb, is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token_iter P1 (Pos.to_nat threads) γp1 }}.
  Proof. iSteps. unseal_diaframe => /=; rewrite /atomic_wand. iFrame "#". iSteps. Qed.

  Context P1 P2 (R : iProp Σ).
  Set Default Proof Using "threads heapGS0 barrierG0 P1 P2 R".

  Program Instance peek_spec γp1 γp2 γt γb (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v }}
      peek v
    {{ (z : Z), RET #z; ⌜Zneg threads ≤ z ≤ Zpos threads⌝%Z }}.
(* 170 seconds ---> 75 seconds *)
  Program Instance sync_up_enter_spec γp1 γp2 γt γb (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P1 γp1 }}
      sync_up_enter v
    {{ (z : Z), RET #z; ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket (Z.to_nat z)) }}.
  (* we use ⌜Fractional _⌝ as the guarding hypotheses. Using ticket 0 or no_tokens P1 is problematic:
      for no_tokens, the problematic transition is aborting the Zpos threads -1 -> Zpos threads shift, caused by [token_dealloc_hint]
      for ticket 0, the problematic transition is 0 -> 1 with threads greater than 1. *)
(* ?? --> 106 seconds *)

  Program Instance sync_up_exit_spec γp1 γp2 γb γt (z : Z) (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket $ Z.to_nat z) }}
      sync_up_exit threads v #z
    {{ RET #(); token P2 γp2 }}.
(* 211 seconds --> 202 seconds 
   498 seconds --> 143 seconds *)

  Global Program Instance sync_up_spec γp1 γp2 γt γb (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P1 γp1 }}
      sync_up threads v
    {{ RET #(); token P2 γp2 }}.

  Program Instance sync_down_enter_spec γp1 γp2 γt γb (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P2 γp2 }}
      sync_down_enter v
    {{ (z : Z), RET #z; ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket (Z.to_nat z)) }}.

  Program Instance sync_down_exit_spec γp1 γp2 γb γt (z : Z) (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket $ Z.to_nat z) }}
      sync_down_exit threads v #z
    {{ RET #(); token P1 γp1 }}.

  Global Program Instance sync_down_spec γp1 γp2 γt γb (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P2 γp2 }}
      sync_down threads v
    {{ RET #(); token P1 γp1 }}.

  Global Program Instance barrier_persistent_reuse γp1 γp2 γt γb (v : val) :
    Persistent (is_barrier P1 P2 R γp1 γp2 γt γb v).
End proof.

Global Opaque is_barrier make_barrier sync_up sync_down.

