From iris.algebra Require Import auth excl numbers.
From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From diaframe.lib Require Import frac_token int_as_nat_diff own_hints.
From diaframe.heap_lang.examples.comparison Require Import ticket_lock.

Definition new_rwlock : val :=
  λ: <>,
    let: "rdrs" := ref #0 in
    let: "tlk" := newlock #() in
    ("tlk", "rdrs").

Definition acquire_reader_int (BOUND : positive) : val :=
  rec: "acq_reader" "readers" :=
    if: ! "readers" < #(Z.pos BOUND) then
      FAA "readers" #1
    else
      "acq_reader" "readers".

Definition acquire_reader (BOUND : positive) : val :=
  λ: "rwlk",
    acquire (Fst "rwlk") ;;
    acquire_reader_int BOUND (Snd "rwlk") ;;
    release (Fst "rwlk").

Definition release_reader : val :=
  λ: "rwlk", FAA (Snd "rwlk") #-1.
(* cannot have upgrade: if a writer has an earlier ticket, it will wait indefinitely on this reader, while this reader
   will wait indefinitely on the writer to release the lock *)

Definition wait_for_readers : val :=
  rec: "wait" "readers" :=
    if: ! "readers" = #0 then
      #()
    else
      "wait" "readers".

Definition acquire_writer : val :=
  λ: "rwlk",
    acquire (Fst "rwlk") ;;
    wait_for_readers (Snd "rwlk").

Definition release_writer : val :=
  λ: "rwlk", release (Fst "rwlk"). (* this rwlock allows writer downgrades, but we do not verify this since starling does not *)

Definition rwlockR := authR $ prodUR max_natUR $ optionUR $ exclR $ natO.
Class rwlockG Σ := { 
  #[local] rwlock_rwlockG :: inG Σ rwlockR ;
  #[local] rwlock_tlockG :: tlockG Σ ;
  #[local] rwlock_fracToken_G :: fracTokenG Σ
}.
Definition rwlockΣ : gFunctors := #[ GFunctor rwlockR ; tlockΣ; fracTokenΣ ].

Local Obligation Tactic := program_verify.

Global Instance subG_rwlockΣ {Σ} : subG rwlockΣ Σ → rwlockG Σ := ltac:(solve_inG).
(* TODO: why is Program not sufficient here - it seems to split rwlockG before we have a chance.. *)

Section proof.
  Context `{!heapGS Σ, !rwlockG Σ}.
  Context (BOUND : positive).
  Implicit Types P : Qp → iProp Σ.
  Let N := nroot .@ "rw_lock".

  Definition readers_inv P (γi γl γp : gname) (readers : loc) : iProp Σ :=
    ∃ (z : Z), readers ↦ #z ∗ 
      ∃ (i d : nat), int_as_nat_diff z i d ∗ own γi (● (MaxNat d, Excl' i)) ∗ 
        ((⌜d < i⌝%Z ∗ ∃ (p : positive), ⌜(Z.pos p + d)%Z = i⌝ ∗ ⌜p ≤ BOUND⌝%positive ∗ token_counter P γp p) ∨
          (⌜i = d⌝%Z ∗ no_tokens P γp (1/2)%Qp ∗ ⌜Fractional P⌝ ∗ (locked γl ∨ no_tokens P γp (1/2)%Qp ∗ P 1%Qp))).

  Definition tlock_inv (γi : gname) : iProp Σ :=
    ∃ (i : nat), own γi (◯ (ε, Excl' i)).

  Definition is_rwlock P (γi γl γl' γp : gname) (rwlk : val) : iProp Σ :=
    ∃ (tlk : val) (rdrs : loc),
      ⌜rwlk = (tlk, #rdrs)%V⌝ ∗ is_lock γl γl' tlk (tlock_inv γi) ∗ inv N (readers_inv P γi γl' γp rdrs).

  Global Instance new_rwlock_spec  :
    SPEC {{ True }} 
      new_rwlock #() 
    {{ (rwlk : val), RET rwlk; ∀ P, ⌜Fractional P⌝ ∗ P 1%Qp ={⊤}=∗ ∃ γp γi γl γl', is_rwlock P γi γl γl' γp rwlk }}.
  Proof.
    iSteps as (l v P HP) "Hl HP". (* we cannot guess a good coallocation target here *)
    iAssert (|==> ∃ γ, own γ (● (MaxNat 0, Excl' 0)) ∗ own γ (◯ (MaxNat 0, Excl' 0)))%I as "H";
    iSteps.
  Qed.

  Context P.

  Program Instance acquire_reader_int_spec (γi γl γp : gname) (readers : loc) i :
    SPEC {{ inv N (readers_inv P γi γl γp readers) ∗ locked γl ∗ own γi (◯ (ε, Excl' i)) }}
      acquire_reader_int BOUND #readers
    {{ (z : Z), RET #z; locked γl ∗ token P γp ∗ own γi (◯ (ε, Excl' (S i)))}}.

  Global Program Instance acquire_reader_spec (rwlk : val) γi γl γl' γp :
    SPEC {{ is_rwlock P γi γl γl' γp rwlk }} 
      acquire_reader BOUND rwlk 
    {{ RET #(); token P γp }}.

  Global Program Instance release_reader_spec (rwlk : val) γi γl γl' γp :
    SPEC {{ is_rwlock P γi γl γl' γp rwlk ∗ token P γp }}
      release_reader rwlk 
    {{ (z : Z), RET #z; True }}.

  Global Program Instance wait_for_readers_spec (γi γl γp : gname) (readers : loc) i :
    SPEC {{ inv N (readers_inv P γi γl γp readers) ∗ locked γl ∗ own γi (◯ (ε, Excl' i)) }}
      wait_for_readers #readers
    {{ RET #(); own γi (◯ (MaxNat i, Excl' i)) ∗ P 1 ∗ no_tokens P γp (1/2)%Qp }}.

  Global Program Instance acquire_writer_spec (rwlk : val) γi γl γl' γp :
    SPEC {{ is_rwlock P γi γl γl' γp rwlk }}
      acquire_writer rwlk 
    {{ (i : nat), RET #(); own γi (◯ (MaxNat i, Excl' i)) ∗ P 1 ∗ no_tokens P γp (1/2)%Qp}}.

  Global Program Instance release_writer_spec (rwlk : val) γi γl γl' γp i :
    SPEC {{ is_rwlock P γi γl γl' γp rwlk ∗ own γi (◯ (MaxNat i, Excl' i)) ∗ P 1 ∗ no_tokens P γp (1/2)%Qp }}
      release_writer rwlk 
    {{ RET #(); True }}.
End proof.

