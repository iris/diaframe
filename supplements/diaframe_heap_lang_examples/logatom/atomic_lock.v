From diaframe.heap_lang Require Import proof_automation atomic_specs.
From iris.program_logic Require Import atomic.
From iris.heap_lang Require Import proofmode notation lib.lock.

From iris.algebra Require Import auth excl.
From diaframe.lib Require Import own_hints.


Module atomic_lock.
  Class atomic_lock Σ `{!heapGS Σ} := AtomicLock {
    (** * Operations *)
    newlock : val;
    acquire : val;
    release : val;
    (** * Predicates *)
    (** [name] is used to associate locked with [is_lock] *)
    name : Type;
    raw_lock : Type;
    lock_mask : coPset;
    client_namespace : namespace;
    as_lock : raw_lock → val;
    lock_inv (γ : name) (lock : raw_lock) : iProp Σ;
    locked_at (γ : name) (v : val) : iProp Σ; 
    locked_at_private (γ : name) (v : val) : iProp Σ;
    can_have_clients : ↑client_namespace ⊆ lock_mask;
    locked_at_exclusive γ v1 v2 lk : ▷ lock_inv γ lk -∗ locked_at γ v1 -∗ ▷ locked_at γ v2 -∗ ◇ False;
    (** * Program specs *)
    #[global] newlock_spec ::
      SPEC {{ True }} newlock #() {{ (lk : raw_lock) γ, RET (as_lock lk); lock_inv γ lk }} ;
    #[global] acquire_spec γ (lk : raw_lock) ::
      SPEC ⟨⊤ ∖ lock_mask⟩ << ▷ lock_inv γ lk >> 
          acquire (as_lock lk) 
      << (v : val), RET v; locked_at_private γ v ¦  ▷ lock_inv γ lk ∗ locked_at γ v >>;
    #[global] release_spec γ (lk : raw_lock) (v : val) :: (* note that v must be know beforehand! *)
      SPEC ⟨⊤ ∖ lock_mask⟩ << locked_at_private γ v ¦ ▷ lock_inv γ lk ∗ ▷ locked_at γ v >> 
          release (as_lock lk) v 
      << RET #(); ▷ lock_inv γ lk >>;
  }.

  Global Arguments newlock {_ _} _.
  Global Arguments acquire {_ _} _.
  Global Arguments release {_ _} _.
  Global Arguments name {_ _} _.
  Global Arguments client_namespace {_ _} _.
  Global Arguments lock_mask {_ _} _.
  Global Arguments raw_lock {_ _} _.
  Global Arguments lock_inv {_ _} _ _ _.
  Global Arguments locked_at {_ _} _ _ _.
  Global Arguments locked_at_private {_ _} _ _ _.
  Global Arguments AtomicLock {_ _} _ _ _ _ _ _ _ _ _ _ _.
End atomic_lock.


Definition boolLockR := authR $ optionUR $ prodR fracR $ agreeR valO.
Class boolLockG Σ := BoolLockG { #[local] bool_lock_tokG :: inG Σ boolLockR }.
Definition boolLockΣ : gFunctors := #[GFunctor boolLockR].

Local Obligation Tactic := program_smash_verify.

Section lock_with_param.
  Import atomic_lock.
  Context `{!heapGS Σ, boolLockG Σ} {L : atomic_lock Σ}.

  Definition lock_inv' (γ : name L) (γ' : gname) (lock : raw_lock L) (b : bool) : iProp Σ :=
    lock_inv L γ lock ∗ ((∃ v, locked_at L γ v ∗ own γ' (● (Some (1%Qp, to_agree v))) ∗ ⌜b = true⌝) ∨ own γ' (● None) ∗ ⌜b = false⌝).

  Definition locked_at' (γ' : gname) (v : val) : iProp Σ := own γ' (◯ (Some ((2/3)%Qp, to_agree v))).

  Definition locked_at_private' γ (γ' : gname) (v : val) : iProp Σ := 
    locked_at_private L γ v ∗ own γ' (◯ (Some ((1/3)%Qp, to_agree v))).

  Lemma locked_at_exclusive' γ' : ∀ v1 v2, locked_at' γ' v1 -∗ locked_at' γ' v2 -∗ False.
  Proof. iSteps. Qed.

  Lemma newlock_spec'' :
    SPEC {{ True }} newlock L #() {{ (lk : raw_lock L) γ γ', RET (as_lock lk); lock_inv' γ γ' lk false }}.
  Proof. iSmash. Qed.

  Lemma acquire_spec'' (lk : raw_lock L) γ γ':
    SPEC ⟨⊤ ∖ lock_mask L⟩ (b : bool), << ▷ lock_inv' γ γ' lk b >> 
        acquire L (as_lock lk) 
    << (v : val), RET v; locked_at_private' γ γ' v ¦  ▷ lock_inv' γ γ' lk true ∗ ⌜b = false⌝ ∗ locked_at' γ' v >>.
  Proof.
    iStep 11 as_anon / as (Φ v1 v2) "Hlocked1 H● HΦ HL Hlocked2" /
             as_anon / as (Φ v) "H● HΦ HL Hlocked".
    - iSteps.
    - iDestruct (locked_at_exclusive with "HL Hlocked2 Hlocked1") as ">[]".
    - iSmash.
    - iSteps as "H◯".
      replace 1%Qp with (2/3 + 1/3)%Qp; last first.
      { by rewrite -Qp.div_add_distr pos_to_Qp_add /= Qp.div_diag. }
      iDestruct "H◯" as "(H◯1 & H◯2)".
      iSteps.
  Qed.

  Lemma release_spec'' (lk : raw_lock L) γ γ' (v : val) :
    SPEC ⟨⊤ ∖ lock_mask L⟩ (b : bool), << locked_at_private' γ γ' v ¦ ▷ lock_inv' γ γ' lk b ∗ ▷ locked_at' γ' v >> 
        release L (as_lock lk) v 
    << RET #(); ▷ lock_inv' γ γ' lk false ∗ ⌜b = true⌝ >>.
  Proof.
    iSteps --safe / as_anon / as (v Φ _ _ _ _ Hthirds) "H● H◯".
    - iSmash.
    - rewrite Hthirds. iSmash.
  Qed.
End lock_with_param.


Definition iCapLockR := authR $ optionUR $ exclR $ leibnizO val.
Class iCapLockG Σ := ICapLockG { #[local] lock_tokG :: inG Σ iCapLockR }.
Definition iCapLockΣ : gFunctors := #[GFunctor iCapLockR].

Local Obligation Tactic := program_verify.

Global Program Instance subG_iCapLockΣ {Σ} : subG iCapLockΣ Σ → iCapLockG Σ.

Section atomic_lock_icap_lock.
  Import atomic_lock.
  Context `{!heapGS Σ, iCapLockG Σ} {L : atomic_lock Σ}.
  Let N  := client_namespace L.
  Let HL := can_have_clients.

  Definition locked (γ : name L) (γ' : gname) (v : val) : iProp Σ := own γ' (◯ (Excl' v)) ∗ locked_at_private L γ v.

  Definition icap_lock_inv (γ : name L) (γ' : gname) (l : raw_lock L) (R : iProp Σ) : iProp Σ :=
    lock_inv L γ l ∗ ∃ (v : val), 
        ((locked_at L γ v ∗ own γ' (● (Excl' v))) ∨ (R ∗ ⌜v = NONEV⌝ ∗ own γ' (● None))).

  Definition is_lock (γ : name L) (γ' : gname) (lk : val) (R : iProp Σ) : iProp Σ := 
    ∃ (l : raw_lock L), ⌜lk = as_lock l⌝ ∗ inv N (icap_lock_inv γ γ' l R).

  Global Program Instance newlock_spec' :
    SPEC {{ True }}
      newlock L #()
    {{ lk, RET (lk : val); ∀ R E, R ={E}=∗ ∃ γ γ', is_lock γ γ' lk R }}.

  Global Instance acquire_spec' γ γ' (lk : val) R :
    SPEC {{ is_lock γ γ' lk R }} 
      acquire L lk 
    {{ (v : val), RET v; locked γ γ' v ∗ ▷ R }}.
  Proof.
    iStep 11 as_anon / as (rlk v1 v2) "HI Hcl Hlocked1 H● HL Hlocked2" / as_anon / as_anon;
             [iSteps |                                                 | iSteps..].
    iDestruct (locked_at_exclusive with "HL Hlocked2 Hlocked1") as ">[]".
  Qed.

  Global Program Instance release_spec' γ γ' (lk v : val) R :
    SPEC {{ is_lock γ γ' lk R ∗ locked γ γ' v ∗ ▷ R }} 
      release L lk v
    {{ RET #(); True }}.

  (* cant do lock_from_atomic : lock.lock Σ, because of the release on values *)
End atomic_lock_icap_lock.

(* It is possible to prove the iCap lock spec with a weaker release spec,
  where locked_at γ v is the private precondition, not at the linearization point.
  But this spec cannot be used in the following way, which is desirable: *)

Definition atomizeR := λ A, authR $ optionUR $ exclR $ leibnizO (val * A).
Class atomizeG A Σ := AtomizeG { #[local] atomize_inG :: inG Σ $ atomizeR A}.
Definition AtomizeΣ : Type → gFunctors := λ A, #[GFunctor iCapLockR].

Local Obligation Tactic := program_smash_verify.

Section atomic_lock_guard.
  (* We want to show that operations on a coarse grained data structure are also logically atomic.*)
  Context `{!heapGS Σ}.
  Context (A : Type) (ν : Type) (P : ν → A → iProp Σ) (Pret : ν → A → val → Prop).

  Context (f_pure : A → A) (f_stateful : val) (handle : ν → val).

  Import atomic_lock.
  Context {L : atomic_lock Σ} `{!atomizeG A Σ}.

  Definition guarded_op : val :=
    λ: "lk" "hndl",
      let: "lkval" := acquire L "lk" in
      let: "ret" := f_stateful "hndl" in
      release L "lk" "lkval" ;;
      "ret".

  Definition frozen_at (v : val) (a : A) : atomizeR A :=
      (auth_auth (A := optionUR $ exclR $ leibnizO (val * A)) (DfracOwn 1%Qp) (Excl' (v, a))).
  (* Using just '● (Excl' (v, a)))' breaks for some reason *)

  Definition P' (γ : name L) (γ' : gname) (lk : raw_lock L) (nameA : ν) (a : A) : iProp Σ :=
     lock_inv L γ lk ∗ ((∃ v, locked_at L γ v ∗ own γ' (frozen_at v a)) ∨ (P nameA a ∗ own γ' (● None))).

  Lemma regular_to_logatom γ γ' lk (nameA : ν) :
    (∀ a, P nameA a -∗ WP f_stateful (handle nameA) {{ λ v, P nameA (f_pure a) ∗ ⌜Pret nameA (f_pure a) v⌝ }})
      ⊢ 
    <<{ ∀∀ (a : A), ▷ P' γ γ' lk nameA a }>> 
        guarded_op (as_lock lk) (handle nameA) @ ⊤ ∖ lock_mask L
    <<{ ∃∃ (v : val), ▷ P' γ γ' lk nameA (f_pure a) ∗ ⌜Pret nameA (f_pure a) v⌝ | RET v }>>.
  Proof.
    iStep 18 as_anon / as (Φ a v1 v2) "Hf HΦ Hlocked1 Hγ HL Hlocked2" / as_anon / as_anon;
            [iSmash  |                                                | iSmash..].
    iDestruct (locked_at_exclusive with "HL Hlocked2 Hlocked1") as ">[]".
  Qed.
  (* this would not be provable with the weaker release spec, since we would have to hand back locked_at to soon:
    we could do the atomic commit before executing release, BUT this means we no longer have access to
    lock_inv, so cannot execute release! *)
End atomic_lock_guard.


Module atomic_stateless_lock.
  Class atomic_stateless_lock Σ `{!heapGS Σ} := AtomicStatelessLock {
    (** * Operations *)
    newlock : val;
    acquire : val;
    release : val;
    (** * Predicates *)
    (** [name] is used to associate locked with [is_lock] *)
    name : Type;
    raw_lock : Type;
    lock_mask : coPset;
    client_namespace : namespace;
    as_lock : raw_lock → val;
    lock_inv (γ : name) (lock : raw_lock) : iProp Σ;
    locked (γ : name) : iProp Σ; 
    locked_private (γ : name) : iProp Σ;
    can_have_clients : ↑client_namespace ⊆ lock_mask;
    locked_at_exclusive γ lk : ▷ lock_inv γ lk -∗ locked γ -∗ ▷ locked γ -∗ ◇ False;
    (** * Program specs *)
    #[global] newlock_spec ::
      SPEC {{ True }} newlock #() {{ (lk : raw_lock) γ, RET (as_lock lk); lock_inv γ lk }} ;
    #[global] acquire_spec γ (lk : raw_lock) ::
      SPEC ⟨⊤ ∖ lock_mask⟩ << ▷ lock_inv γ lk >> 
          acquire (as_lock lk) 
      << RET #(); locked_private γ ¦  ▷ lock_inv γ lk ∗ locked γ >>;
    #[global] release_spec γ (lk : raw_lock) ::
      SPEC ⟨⊤ ∖ lock_mask⟩ << locked_private γ ¦ ▷ lock_inv γ lk ∗ ▷ locked γ >> 
          release (as_lock lk)
      << RET #(); ▷ lock_inv γ lk >>;
  }.

  Global Arguments newlock {_ _} _.
  Global Arguments acquire {_ _} _.
  Global Arguments release {_ _} _.
  Global Arguments name {_ _} _.
  Global Arguments client_namespace {_ _} _.
  Global Arguments lock_mask {_ _} _.
  Global Arguments raw_lock {_ _} _.
  Global Arguments as_lock {_ _} _.
  Global Arguments lock_inv {_ _} _ _ _.
  Global Arguments locked {_ _} _ _.
  Global Arguments locked_private {_ _} _ _.
  Global Arguments AtomicStatelessLock {_ _} _ _ _ _ _ _ _ _ _ _ _.

  Global Program Instance stateless_lock_is_lock `{!heapGS Σ} (L : atomic_stateless_lock Σ) : atomic_lock.atomic_lock Σ :=
    {| atomic_lock.newlock := newlock L;
       atomic_lock.acquire := acquire L;
       atomic_lock.release := λ: "lk" "v", release L "lk";
       atomic_lock.name := name L;
       atomic_lock.raw_lock := raw_lock L;
       atomic_lock.lock_mask := lock_mask L;
       atomic_lock.client_namespace := client_namespace L;
       atomic_lock.as_lock := as_lock L;
       atomic_lock.lock_inv := lock_inv L;
       atomic_lock.locked_at := (λ γ v, locked L γ ∗ ⌜v = #()⌝)%I;
       atomic_lock.locked_at_private := (λ γ v, locked_private L γ ∗ ⌜v = #()⌝)%I;
       atomic_lock.can_have_clients := can_have_clients;
    |}.
  Next Obligation. iStep 3 as "HL Hlocked1 Hlocked2". by iApply (locked_at_exclusive with "HL Hlocked1 Hlocked2"). Qed.
End atomic_stateless_lock.


Definition icap_to_atomicR := exclR unitO.
Class iCapToAtomicG Σ := ICapToAtomicG { #[local] to_icap_atomicG :: inG Σ icap_to_atomicR }.
Definition iCapToAtomicΣ : gFunctors := #[GFunctor icap_to_atomicR].


Section icap_lock_atomic_lock.
  Context `{!lock.lock, !heapGS Σ, !iCapLockG Σ, !lockG Σ}.
  Import atomic_stateless_lock.

  Let NC := nroot.@ "client".
  Let NE := nroot.@ "extra".
  (* TODO: use ghost_var *)

  Program Definition atomic_from_icap_lock : atomic_stateless_lock Σ :=
    {| newlock := lock.newlock;
       acquire := (λ: "lk", #() ;; lock.acquire "lk"); (* this is ugly and only to strip a later from invariants *)
       release := (λ: "lk", #() ;; lock.release "lk");
       name := lock.lock_name * gname;
       raw_lock := val;
       lock_mask := ⊤ ∖ ↑ NE;
       client_namespace := NC;
       as_lock := id;
       lock_inv := (λ γpr lk, lock.is_lock γpr.1 lk (own γpr.2 (●{#3/4} None)) ∗ inv NE (own γpr.2 (●{#1/4} None) ∨ own γpr.2 (●{#1/4} (Excl' #0)) ∗ lock.locked γpr.1 ∗ own γpr.2 (◯ (Excl' #0))))%I;
       locked := λ γpr, own γpr.2 (●{#3/4} Excl' #0);
       locked_private := λ _, emp%I
     |}.
  Next Obligation.
    iStep.
    iAssert (|==> ∃ γ, own γ (● None))%I as ">[%γ [Hγ1 [Hγ2 Hγ3]]]"; first iSteps.
    iCombine "Hγ1 Hγ2" as "Hγ".
    replace (1/2 + 1/2/2)%Qp with (3/4)%Qp; last first.
    { rewrite -{1}(Qp.div_2_mul 1 2) Qp.div_div.
      rewrite pos_to_Qp_mul -!Qp.div_add_distr !pos_to_Qp_add.
      done. }
    replace (1/2/2)%Qp with (1/4)%Qp; last first.
    { rewrite Qp.div_div // pos_to_Qp_mul. done. }
    iApply wp_fupd.
    iApply (lock.newlock_spec with "Hγ").
    iSteps as (v lk) "HL". iExists (lk, γ). iSmash.
  Qed.
  Next Obligation.
    iStep 2 as (Φ) "HAU".
    iApply fupd_wp. iMod "HAU" as "[#[Hi1 Hi2] [Hr _]]".
    iMod ("Hr" with "[$Hi1 $Hi2]") as "HAU".
    iSteps as "Hi1 Hi2 Hlocked H●".
    iMod (inv_acc with "Hi2") as "[Hi Hcl]"; first done.
    iDecompose "Hi" as (_) "H●".
    iAssert (|==> own γ.2 (●{#1/4 + 3/4} Excl' #0) ∗ own γ.2 (◯ Excl' #0))%I with "[H●]" as ">[[H●1 H●2] H◯]".
    { replace (1/4 + 3/4)%Qp with 1%Qp; first iSteps.
      rewrite Qp.quarter_three_quarter //. }
    iMod ("Hcl" with "[H●1 Hlocked H◯]") as "_"; first iSteps.
    iMod "HAU" as "[_ [_ Hcl]]".
    iMod ("Hcl" with "[H●2]") as "HΦ"; first iSteps.
    iSteps.
  Qed.
  Next Obligation.
    iStep 2 as (Φ) "HAU".
    iApply fupd_wp. iMod "HAU" as "[[#Hi Hl] [Hr _]]".
    iMod ("Hr" with "[$Hi $Hl]") as "HAU".
    iSteps as "HL HI Hcl H● H◯" / as "HL HI H● Hcl";
    iMod "HAU" as "[[_ >Hl] [_ HAUcl]]"; iDecompose "Hl" as (_) "H●".
    rewrite Qp.three_quarter_quarter.
    iAssert (|==> own γ.2 (●{#3/4 + 1/4} None))%I with "[H● H◯]" as ">[H●1 H●2]".
    { rewrite Qp.three_quarter_quarter. iSteps. }
    iStep. iMod ("HAUcl" with "[]") as "HΦ"; iSteps.
  Qed.
End icap_lock_atomic_lock.
























