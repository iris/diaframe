From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From iris.heap_lang Require Import proofmode notation atomic_heap.

Import bi notation.

Local Obligation Tactic := program_smash_verify.


Definition make_counter `{!atomic_heap} : val := 
    λ: <>, ref #0.

Definition incr `{!atomic_heap} (max_state : positive) : val := 
  rec: "incr" "l" :=
    let: "n" := !"l" in
    let: "m" := 
      if: "n" < #(Zpos max_state) then 
        #1 + "n" 
      else 
        #0 
    in
    if: CAS "l" "n" "m" then 
      "n" 
    else 
      "incr" "l".

Definition read `{!atomic_heap} : val := 
  λ: "l", !"l".


Section proof.
  Context  `{!atomic_heap, !heapGS Σ,!atomic_heapGS Σ}.

  (* exact physical effect of incr *)
  Local Program Instance incr_spec (l : loc) (max_state : positive) :
    SPEC (z : Z), << l ↦ #z >>
      incr max_state #l
    << RET #z; ∃ (z' : Z), l ↦ #z' ∗ (⌜z < Zpos max_state⌝%Z ∗ ⌜z' = (z + 1)%Z⌝%Z ∨ ⌜Zpos max_state ≤ z⌝%Z ∗ ⌜z' = 0⌝) >>.

  Definition bounded_ref max_state (l : loc) (z : Z) : iProp Σ :=
    l ↦ #z ∗ ⌜0 ≤ z ≤ Zpos max_state⌝%Z%I.

  Global Program Instance newbounded_ref max_state :
    SPEC {{ True }}
      make_counter #()
    {{ (l : loc), RET #l; bounded_ref max_state l 0 }}.

  (* library effect of incr *)
  Global Instance incr_counter_spec (l : loc) (max_state : positive) :
    SPEC (z : Z), << bounded_ref max_state l z >>
      incr max_state #l
    << RET #z; bounded_ref max_state l ((z + 1) `mod` (Zpos max_state + 1)) >>.
  Proof.
    iSteps --safe / as_anon /
                    as (Φ z Hz1 _ Hz2) /
                    as (Φ z _ Hz1 Hz2);
      [ iRight| ..]; iSteps as (Hz3).
    - contradict Hz3. rewrite Zmod_small //. lia.
    - contradict Hz3. assert (z = Z.pos max_state)%Z as -> by lia.
      rewrite Z_mod_same_full //.
    Unshelve. all: apply inhabitant.
  Qed.

  Global Program Instance read_counter_spec (l : loc) (max_state : positive) :
    SPEC (z : Z), << bounded_ref max_state l z >>
      read #l
    << RET #z; bounded_ref max_state l z ∗ ⌜0 ≤ z ≤ Zpos max_state⌝%Z >>.
End proof.


































