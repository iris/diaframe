From diaframe.heap_lang Require Import proof_automation.
From diaframe.lib Require Export max_prefix_list_hints.


Section queue_resources.
  Context `{!heapGS Σ}.

  Fixpoint is_queue_base l (ls : list loc) (vs : list val) : iProp Σ :=
    match ls, vs with
    | [], [] => (l +ₗ 1) ↦ NONEV
    | l':: ls', v :: vs' => (l +ₗ 1) ↦□ SOMEV #l' ∗ l' ↦□ v ∗ is_queue_base l' ls' vs'
    | _, _ => False
    end.

  Lemma is_queue_base_to_mapsto i (l' : loc) l (ls : list loc) (vs : list val) :
    (l :: ls) !! i = Some l' →
    is_queue_base l ls vs ⊢ ∃ v dq, (l' +ₗ 1) ↦{dq} v ∗ 
      (⌜v = NONEV⌝ ∗ ⌜dq = DfracOwn 1⌝ ∗ ⌜i = length ls⌝ ∗ 
        ((∀ v (olv : option (loc * val)), 
            (l' +ₗ 1) ↦ v -∗ (⌜v = NONEV⌝ ∗ ⌜olv = None⌝ 
                            ∨ ∃ (lt : loc), ⌜v = SOMEV #lt⌝ ∗ (lt +ₗ 1) ↦ NONEV ∗ ∃ v', lt ↦□v' ∗ ⌜olv = Some (lt, v')⌝) ==∗
              match olv with None => ⌜default l (last ls) = l'⌝ ∗ is_queue_base l ls vs | Some (lt, v') => is_queue_base l (ls ++ [lt]) (vs ++ [v']) end ))
      ∨ ∃ (lr : loc), ⌜ls !! i = Some lr⌝ ∗ ⌜v = SOMEV #lr⌝ ∗ ⌜dq = DfracDiscarded⌝ ∗ ⌜i < length ls⌝ ∗ is_queue_base l ls vs).
  Proof.
    elim: i ls vs l l'.
    - move => ls vs l l' /= Hl. simplify_eq. destruct vs; destruct ls; iSteps --safest. iLeft. unseal_diaframe =>/=. iSteps.
    - move => i IH ls vs l l' /= Hl.
      destruct ls; destruct vs; simplify_eq; iStep as "Hl Hl0 Hq".
      rewrite {1}IH //. iDecompose "Hq" as "Hl' Hcl" / as_anon; iSteps --safest.
      iLeft. unseal_diaframe=> /=. iStep 4 as "Hl'" / as (l'' v') "Hl'' Hl' Hl''2".
      * iMod ("Hcl" with "Hl' []") as "HN"; first iSteps. iDecompose "HN". iSteps.
        rewrite last_lookup /= Hl//=.
      * iMod ("Hcl" with "Hl' [Hl''2]"); iSteps.
  Qed. (* stated with ∈, this lemma is not strong enough! *)

  Lemma is_queue_base_extract_next_and_wit i l' l ls vs (lt : loc) :
    ((l :: ls) !! i = Some l') →
    is_queue_base l ls vs ⊢ (l' +ₗ 1) ↦□ SOMEV #lt -∗ ⌜ls !! i = Some lt⌝ ∗ ∃ v, lt ↦□ v ∗ ⌜vs !! i = Some v⌝ ∗ is_queue_base l ls vs.
  Proof.
    elim: ls i l l' vs lt.
    - move => i l l' vs lt /list_lookup_singleton_Some. case; intros; simplify_eq.
      destruct vs; iSteps.
    - move => lh ls IH i l l' vs lt.
      destruct i => /= Hl; simplify_eq.
      * destruct vs; iSteps.
      * destruct vs; iStep as "Hl Hlh Hq". rewrite {1}IH //.
        iStep as "Hl'". iSpecialize ("Hq" with "Hl'"). iDecompose "Hq" as (Hlt v' Hv') "Hlt Hq". iSteps.
  Qed.

  Lemma is_queue_base_extract_next l ls vs l' (lt : loc) :
    (l' ∈ l :: ls) →
    is_queue_base l ls vs ⊢ (l' +ₗ 1) ↦□ SOMEV #lt -∗ ⌜lt ∈ ls⌝ ∗ is_queue_base l ls vs.
  Proof.
    move => Hls. apply elem_of_list_lookup_1 in Hls as [i Hi].
    apply bi.wand_intro_l.
    rewrite {1}is_queue_base_extract_next_and_wit //.
    erewrite bi.wand_elim_r.
    iSteps as (Hlt v Hv) "Hlt Hq". iPureIntro. by eapply elem_of_list_lookup_2.
  Qed.

  Lemma is_queue_root_not_in l ls vs : is_queue_base l ls vs ⊢ ⌜l ∉ ls⌝.
  Proof.
    elim: ls l vs.
    - iSteps as (l vs Hin) "Hvs". by apply elem_of_not_nil in Hin.
    - move => lh ls IH l vs.
      destruct vs; iStep as "Hl Hlh Hq".
      destruct (decide (l ∈ lh :: ls)); last done.
      rewrite is_queue_base_extract_next //.
      iDestruct ("Hq" with "Hl") as "[% Hq]".
      rewrite IH. iDecompose "Hq".
  Qed.

  Lemma is_queue_no_dup l ls vs : is_queue_base l ls vs ⊢ ⌜NoDup (l :: ls)⌝.
  Proof.
    elim: ls l vs.
    - iSteps as (l ls) "Hls". iPureIntro. apply NoDup_singleton.
    - move => lh ls IH l vs.
      destruct vs; first iStep.
      iIntros "Hq". iAssert (⌜l ∉ lh :: ls⌝)%I as %Hl.
      { by rewrite is_queue_root_not_in. }
      iDecompose "Hq" as "Hl Hlh Hq". rewrite IH. iDecompose "Hq" as (Hnodup).
      iPureIntro. by apply NoDup_cons.
  Qed.

  Global Instance remember_no_dup l ls vs : MergablePersist (is_queue_base l ls vs) (λ p Pin Pout,
      TCAnd (TCEq Pin (ε₀)%I) (TCEq Pout ⌜NoDup (l :: ls)⌝%I)).
  Proof.
    move => p Pin Pout [-> ->].
    rewrite bi.intuitionistically_if_elim.
    rewrite is_queue_no_dup. iSteps.
  Qed.

  Global Instance is_queue_base_timeless l ls vs : Timeless (is_queue_base l ls vs).
  Proof.
    elim: ls l vs; intros => //=; destruct (_ : list val); tc_solve.
  Qed.
End queue_resources.


Ltac prove_lookup :=
  match goal with
  | H : ?rs !! ?i = _ |- ?ls !! ?i = _ =>
    (* try to prove with prefix_lookup -> this one will not work if i is evar! *)
    refine (prefix_lookup_Some _ _ _ _ H _); prove_prefix 
  | H : ?rs !! _ = ?rhs |- ?ls !! _ = ?rhs =>
    (* try to prove with prefix_lookup -> this one will not work if rhs is evar! *)
    refine (prefix_lookup_Some _ _ _ _ H _); prove_prefix 
  | |- (?l :: ?ls) !! ?i = Some ?x =>
    (* see if ls is cons, and (it occurs in the rhs, or is the head) *)
    match l with
    | x => unify i O; reflexivity
    | _ =>
      let i' := open_constr:((_ : nat)) in
      unify i (S i'); simpl;
      prove_lookup
    end
  end.

Global Hint Extern 4 (_ !! _ = Some _) => prove_lookup : solve_pure_eq_add.



Class FindLookupIndex {A : Type} (ls : list A) (i : nat) (l : A) :=
  find_lookup_index : ls !! i = Some l.
Global Hint Mode FindLookupIndex + + - + : typeclass_instances.
Global Hint Extern 4 (FindLookupIndex _ _ _) => unfold FindLookupIndex; prove_lookup : typeclass_instances.
(* cant use SolveSepSideCondition, wrong hint mode *)

Definition queue_base_R : cmra := authR $ max_prefix_listUR $ locO.

Section queue_hints.
  Context `{!heapGS Σ, !inG Σ queue_base_R}.

  Global Instance lookup_queue_loc γll lsk i l' :
    FindLookupIndex lsk i l' →
    HINT □⟨true⟩ own γll (◯ to_max_prefix_list lsk) ✱ [ls vs root; is_queue_base root ls vs ∗ own γll (● to_max_prefix_list (root :: ls))] 
      ⊫ [id] v dq ; (l' +ₗ 1) ↦{dq} v ✱ 
        [own γll (● to_max_prefix_list (root :: ls)) ∗ ⌜lsk `prefix_of` (root :: ls)⌝ ∗ (
          ⌜v = NONEV⌝ ∗ ⌜dq = DfracOwn 1⌝ ∗ ⌜i = length ls⌝ ∗ 
          (∀ (ls' : list loc) (vs' : list val) (v0 : val) (olv : option (loc * val)),
            (l' +ₗ 1) ↦ v0 ∗ (⌜v0 = InjLV #()⌝ ∗ ⌜olv = None⌝ ∗ ⌜ls = ls'⌝ ∗ ⌜vs = vs'⌝
               ∨ (∃ (lt : loc) (v' : val), ⌜v0 = InjRV #lt⌝ ∗ (lt +ₗ 1) ↦ InjLV #() ∗ lt ↦□ v' ∗ 
                       ⌜olv = Some (lt, v')⌝ ∗ ⌜ls' = ls ++ [lt]⌝ ∗ ⌜vs' = vs ++ [v']⌝)) ==∗
            is_queue_base root ls' vs' ∗ ⌜NoDup (root :: ls')⌝ ∗
            match olv with
            | Some (lt, v') => ⌜ls' = ls ++ [lt]⌝ ∗ ⌜vs' = vs ++ [v']⌝
            | None => ⌜default root (last ls) = l'⌝ ∗ ⌜ls = ls'⌝ ∗ ⌜vs = vs'⌝
            end)
        ∨ (∃ lr : loc, ⌜ls !! i = Some lr⌝ ∗ ⌜v = InjRV #lr⌝ ∗
            ⌜dq = DfracDiscarded⌝ ∗ ⌜i < length ls⌝ ∗ ∃ v, lr ↦□ v ∗ ⌜vs !! i = Some v⌝ ∗
            is_queue_base root ls vs))].
  Proof.
    rewrite /FindLookupIndex => Hlsk /=. iSteps as (ls vs l Hls Hpref) "H◯ Hq H●".
    rewrite {1}(is_queue_base_to_mapsto i l'); last prove_lookup.
    iDecompose "Hq" as "Hl' Hcl" / as (lt Hlt Hi _) "Hl' Hq"; iSteps --safest / as "".
    - iLeft. unseal_diaframe => /=.
      iSteps as (ls vs) "Hl' Hcl" / as (l'' v'') "Hl'' Hl' Hl''2".
      * iSpecialize ("Hcl" $! NONEV None). iSteps.
      * iSpecialize ("Hcl" $! (SOMEV #l'') (Some (l'', v''))).
        unseal_diaframe => /=.
        iMod ("Hcl" with "Hl' [-]") as "HN"; first iSteps. iDecompose "HN". iSteps.
    - rewrite {1}(is_queue_base_extract_next_and_wit i l'); last prove_lookup. 
      iSpecialize ("Hq" with "Hl'").
      iDecompose "Hq". iSteps.
  Qed.
End queue_hints.


Ltac prove_nodup :=
  lazymatch goal with
  | |- NoDup ?ls =>
    try assumption;
    lazymatch ls with
    | [?x] => apply NoDup_singleton
    | longest ?l ?r =>
      lazymatch goal with
      | H : ?r' `prefix_of` longest ?l' ?r' |- NoDup (longest ?l' ?r') =>
        apply right_prefix_of_longest_then_one_eq in H; destruct H as [H|H]; rewrite H; prove_nodup
      end
    end
  end.

Global Hint Extern 4 (NoDup _) => prove_nodup : solve_pure_add.







