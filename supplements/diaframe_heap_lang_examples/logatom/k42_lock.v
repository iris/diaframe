From iris.heap_lang Require Import proofmode.
From diaframe.heap_lang Require Import proof_automation loc_map atomic_specs wp_auto_lob.
From iris.algebra Require Import agree frac excl auth.
From diaframe.lib Require Import own_hints.
Set Default Proof Using "Type".

Definition new_lock : val :=
  λ: "_", AllocN #2 NONE.

Definition new_node : val :=
  λ: <>, AllocN #2 NONE.

Definition enqueue_node : val :=
  rec: "enqueue" "lk" "node" :=
    let: "pred" := ! "lk" in
    if: "pred" = NONEV then
      if: CAS "lk" "pred" (SOME "lk") then
        NONEV
      else
        "enqueue" "lk" "node"
    else
      if: CAS "lk" "pred" (SOME "node") then 
        "pred"
      else 
        "enqueue" "lk" "node".

Definition wait_on : val :=
  rec: "wait_on" "l" :=
    if: ! "l" then 
      "wait_on" "l"
    else 
      #().

Definition wait_for_succ : val :=
  rec: "wait_for" "node" :=
    match: !("node" +ₗ #1) with
      NONE => "wait_for" "node" (* after reading SOME, we gain ownership of the location *)
    | SOME "l" => #()
    end.

Definition acquire_with : val :=
  λ: "lk" "node",
    ("node" +ₗ #1) <- NONE;;
    let: "pred" := enqueue_node "lk" "node" in (* this transfers ownership of "node" +ₗ 1 to "lk" *)
    match: "pred" with
      NONE => #() (* we obtained the lock, we dont own the right to change (lk + 1), 
                    but we know that if (lk + 1) ↦ SOME n then this will be a head_signal_loc  *)
    | SOME "l" => (* in this case, we have received of "l" +ₗ 1 ↦{1/2} NONEV from enqueue_node "lk" *)
      "node" <- #true ;;
      ("l" +ₗ #1) <- SOME "node";; (* this transfers ownership of "node" +ₗ 0 to "l" *)
      wait_on "node";; (* we are not done: change the lock so that release will not require the node explicitly *)
      (* here we do have the right to change (lk + 1), and the right to change what (lk + 1) ↦ SOME means *)
      match: ! ("node" +ₗ #1) with
        NONE => (* no one has enqueued in the mean time: try to decouple *)
        ("lk" +ₗ #1) <- NONE ;; (* the head/tail of the list has access to the lk.next..? *)
        if: CAS "lk" (SOME "node") (SOME "lk") then
          #() (* same result as enqueue returning NONE *)
        else (* somebody got in between*)
          wait_for_succ "node";;
          ("lk" +ₗ #1) <- (! ("node" +ₗ #1)) (* we know at release that !("lk" +ₗ #1) = SOME _ *)
      | SOME "l" => (* someone enqueued: store this next node in the lock *)
        ("lk" +ₗ #1) <- SOME "l" (* we know at release that !("lk" +ₗ #1) = SOME _, *and* that this means its a signal inv *)
      end
    end.

Definition acquire : val :=
  λ: "lk",
    let: "node" := new_node #() in
    acquire_with "lk" "node" ;;
    Free "node" ;;
    Free ("node" +ₗ #1).

Definition release : val :=
  λ: "lk",
    let: "free" := 
      if: !("lk" +ₗ #1) = NONE then (* after reading SOME, we gain ownership of the location *)
        if: CAS "lk" (SOME "lk") NONE then (* This CAS, if succesful gives us almost the ownership back *)
          #true
        else (* someone is in the process of claiming the lock, wait until it has made itself our tail *)
          wait_for_succ "lk";;
          #false
      else
        #false
      in
    if: "free" then
      #()
    else
      match: !("lk" +ₗ #1) with (* here we should have gained ownership and knowledge that it points to SOME *)
        NONE => #() #() (* unsafe *)
      | SOME "l" =>
        "l" <- #false (* returns ownership to "l", and relinquishes lock resource R *)
      end.

Definition k42_lock1R := prodR (agreeR $ prodO (leibnizO loc) (leibnizO bool)) fracR.
Definition k42_lock2R := authR $ prodUR (optionUR $ exclR $ boolO) $ optionUR $ exclR $ optionO $ prodO (leibnizO loc) $ leibnizO gname.
Class k42_lockG Σ := K42LockG { 
  #[local] k42_lock_tok1G :: inG Σ k42_lock1R;
  #[local] k42_lock_tok2G :: inG Σ k42_lock2R;
  #[local] k42_lock_locmapG :: loc_mapG Σ (option gname);
  #[local] k42_lock_exclG :: inG Σ $ exclR unitO
}.
Definition k42_lockΣ : gFunctors := #[
  GFunctor k42_lock1R; 
  GFunctor k42_lock2R;
  GFunctor $ exclR unitO; 
  loc_mapΣ (option gname)
].

Local Obligation Tactic := program_verify.

Global Program Instance subG_k42_lockΣ {Σ} : subG k42_lockΣ Σ → k42_lockG Σ.

Section spec.
  Context `{!heapGS Σ, k42_lockG Σ}.

  Let N := nroot .@ "k42lock".
  Definition N_signal := N .@ "signal".
  Definition N_node := N .@ "node".
  (* l, false is about the boolean, l, true is about the next location *)

  Definition signal_loc γh γe l γb R : iProp Σ :=
    own γb (to_agree (l, false), 1%Qp) ∨ (∃ (b : bool), l ↦ #b ∗ 
      (⌜b = true⌝
          ∨ 
        ⌜b = false⌝ ∗ R ∗ own γe (Excl ()) ∗ own γb (to_agree (l, false), (1/2)%Qp) ∗ ∃ (pred : loc), own γh (◯ (Excl' false, Excl' (Some (pred, γb)))))).

  Definition waiting_loc_inv γh γm γn γe (n : loc) R : iProp Σ :=
    own γn (to_agree (n, true), 1%Qp) ∨ (∃ v, (n +ₗ 1) ↦{# 1/2} v ∗ ⌜val_is_unboxed v⌝ ∗
        (⌜v = NONEV⌝ ∨ 
        (∃ (n' : loc) γ, ⌜v = SOMEV #n'⌝ ∗ (n +ₗ 1) ↦{# 1/2} v ∗ own γn (to_agree (n, true), (1/2)%Qp) ∗ loc_map_elem γm n (DfracOwn $ 1/2)%Qp (Some γn) ∗ 
              own γ (to_agree (n', false), (1/2)%Qp) ∗ inv N_signal (signal_loc γh γe n' γ R)))).

  Definition head_signal_loc γh l γb R : iProp Σ :=
    own γb (to_agree (l, false), 1%Qp) ∨ (∃ (b : bool), l ↦ #b ∗ 
      (⌜b = true⌝
          ∨ 
        ⌜b = false⌝ ∗ R ∗ own γh (●{#1/2} (Excl' true, Excl' (Some (l, γb)))) ∗ own γb (to_agree (l, false), (1/2)%Qp) ∗ own γh (◯ (Excl' true, ε)))).

  Definition head_inv γh γe (l : loc) R : iProp Σ :=
    ∃ v, (l +ₗ 1) ↦ v ∗ ⌜val_is_unboxed v⌝ ∗
      (own γh (● (Excl' true, Excl' None)) ∗ ⌜v = NONEV⌝
            ∨                            (* this vv resource should not be in the invariant *)
      (∃ (n' : loc) γ, ⌜v = SOMEV #n'⌝ (* ∗ own γ (to_agree (n', false), (1/2)%Qp)*) ∗
           ((inv N_signal (signal_loc γh γe n' γ R) ∗ own γh (● (Excl' false, Excl' (Some (n', γ))))) ∨ 
          ((own γ (to_agree (n', false), 1/2)%Qp ∗ own γh (●{#1/2} (Excl' true, Excl' (Some (n', γ)))) ∗ inv N_signal (head_signal_loc γh n' γ R) 
                    ∨ 
          own γe (Excl()) ∗ inv N_signal (head_signal_loc γh n' γ R))
                   ∗ own γh (●{#1/2} (Excl' true, Excl' (Some (n', γ)))))))).

  Definition free_node γm n : iProp Σ :=
    ∃ v1 v2, n ↦ v1 ∗ (n +ₗ 1) ↦ v2 ∗ loc_map_elem γm n (DfracOwn 1) None.

  Definition lock_inv γh γm γe lk R : iProp Σ :=
    ∃ (v : val), lk ↦ v ∗ ⌜val_is_unboxed v⌝ ∗ loc_map_elem γm lk DfracDiscarded None ∗ (
      ⌜v = NONEV⌝ ∗ own γe (Excl ()) ∗ R ∗ own γh (◯ (Excl' true, Excl' None))
        ∨
      ∃ (l : loc), ⌜v = SOMEV #l⌝ ∗ 
        ((l +ₗ 1) ↦{# 1/2} NONEV ∗ (∃ γn, own γn (to_agree (l, true), (1/2)%Qp) ∗ 
            loc_map_elem γm l (DfracOwn $ 1/2)%Qp (Some γn) ∗ inv N_node (waiting_loc_inv γh γm γn γe l R))
          ∨
        (⌜l = lk⌝ ∗ own γh (◯ (ε, Excl' None)))) (* and what..? *)
    ).

  Definition is_lock γh γm γe R v : iProp Σ :=
    ∃ (lk : loc), ⌜v = #lk⌝ ∗ global_reg γm ∗ inv N (lock_inv γh γm γe lk R) ∗ inv N_node (head_inv γh γe lk R).

  Global Program Instance new_lock_spec :
    SPEC {{ True }} 
      new_lock #()          (* note that global_reg is not a difficult premise, since one has ⊢ |={E}=> ∃ γm, global_reg γm *)
    {{ (lk : val), RET lk; (∀ R γm, R ∗ global_reg γm ={⊤}=∗ ∃ γe γh, is_lock γh γm γe R lk) }}.
    (* having global_reg γm on the left side of the fupd means multiple k42_locks can share the same global_reg γm,
       which means free_nodes can be used in any of these locks, as desired. *)

  Global Program Instance new_node_spec :
    SPEC {{ True }} 
      new_node #() (* global_reg or is_lock? *)
    {{ (n : loc), RET #n; ∀ γm, global_reg γm ={⊤}=∗ free_node γm n }}.

  Global Instance mergable_persist_mapsto_different γm dq1 dq2 l1 l2 v1 v2 :
    MergablePersist (loc_map_elem γm l1 dq1 v1)%I (λ p Pin Pout, 
      TCAnd (TCEq Pin (loc_map_elem γm l2 dq2 v2)) $
      TCAnd (SolveSepSideCondition (v1 ≠ v2)) $
            (TCEq Pout ⌜l1 ≠ l2⌝))%I | 150.
  Proof.
    rewrite /MergablePersist /SolveSepSideCondition => p Pin Pout [-> [Hv ->]].
    rewrite bi.intuitionistically_if_elim.
    destruct (decide (l1 = l2)) as [-> | Hneq]; iSteps.
  Qed.

  Section enqueue_node.
    Instance enqueue_node_phys_spec (lk : loc) (n : val) :
      SPEC (v : val), << ▷ lk ↦ v ∗ ⌜val_is_unboxed v⌝ >>
        enqueue_node #lk n
      << RET v; (⌜v = NONEV⌝ ∗ lk ↦ SOMEV #lk) ∨ (⌜v ≠ NONEV⌝ ∗ lk ↦ SOMEV n) >>.
    Proof. time iSmash. Qed.

    Instance enqueue_node_spec (lk n : loc) γh γm γe R :
      SPEC {{ inv N (lock_inv γh γm γe lk R) ∗ (n +ₗ 1) ↦ NONEV ∗ loc_map_elem γm n (DfracOwn 1) None }}
        enqueue_node #lk #n
      {{ (p : val), RET p; 
        ⌜p = NONEV⌝ ∗ own γe (Excl ()) ∗ ▷ R ∗ (n +ₗ 1) ↦ NONEV ∗ loc_map_elem γm n (DfracOwn 1) None ∗ own γh (◯ (Excl' true, ε))
            ∨
        ∃ (l : loc) γn, ⌜p = SOMEV #l⌝ ∗ 
          inv N_node (waiting_loc_inv γh γm γn γe n R) ∗ own γn (to_agree (n, true), (1/2))%Qp ∗ loc_map_elem γm n (DfracOwn $ 1/2)%Qp (Some γn) ∗ (
          ((l +ₗ 1) ↦{# 1/2} NONEV ∗ (∃ γl, ▷ loc_map_elem γm l (DfracOwn $ 1/2)%Qp (Some γl) ∗ 
              own γl (to_agree (l, true), (1/2)%Qp) ∗ ▷ inv N_node (waiting_loc_inv γh γm γl γe l R))
           ∨ (⌜l = lk⌝ ∗ own γh (◯ (ε, Excl' None))))) }}.
    Proof. iSteps. Qed.
  End enqueue_node.
  Existing Instance enqueue_node_spec.

  Section wait_on.
    Program Instance wait_on_phys_spec (l : loc) :
      SPEC (b : bool) q, << ▷ l ↦{q} #b >>
        wait_on #l
      << RET #(); ⌜b = false⌝ ∗ l ↦{q} #false >>.

    Program Instance wait_on_spec (n : loc) γh γe γ R :
      SPEC [inv N_signal (signal_loc γh γe n γ R)] {{ own γ (to_agree (n, false), 1/2)%Qp }}
        wait_on #n
      {{ RET #(); ▷ R ∗ own γe (Excl ()) ∗ n ↦ #false ∗ ∃ (pred : loc), own γh (◯ (Excl' false, Excl' (Some (pred, γ)))) }}.

    Program Instance wait_on_spec' (n : loc) γh γ R :
      SPEC [inv N_signal (head_signal_loc γh n γ R)] {{ own γ (to_agree (n, false), 1/2)%Qp }}
        wait_on #n
      {{ RET #(); ▷ R ∗ n ↦ #false ∗ own γh (◯ (Excl' true, ε)) ∗ own γh (●{#1/2} (Excl' true, Excl' (Some (n, γ)))) }}.
  End wait_on.
  Existing Instance wait_on_spec.
  Existing Instance wait_on_spec'.

  Section wait_for_succ.
    Program Instance wait_for_succ_phys_spec (l : loc) :
      SPEC (v : val) q, << ▷ (l +ₗ 1) ↦{q} v ∗ (⌜v = NONEV⌝ ∨ ∃ v2, ⌜v = SOMEV v2⌝ ∗ ε₀) >>
        wait_for_succ #l
      << RET #(); ⌜∃ v2, v = SOMEV v2⌝ ∗ (l +ₗ 1) ↦{q} v >>.

    Program Instance wait_for_succ_spec γh γm γn γe (n : loc) R :
      SPEC [inv N_node (waiting_loc_inv γh γm γn γe n R)] {{ own γn (to_agree (n, true), (1/2))%Qp }}
        wait_for_succ #n
      {{ (l : loc) γ, RET #(); 
          (n +ₗ 1) ↦ SOMEV #l ∗ own γ (to_agree (l, false), (1/2)%Qp) ∗ ▷ loc_map_elem γm n (DfracOwn $ 1/2)%Qp (Some γn) ∗ ▷ inv N_signal (signal_loc γh γe l γ R) }}.

    Instance wait_for_succ_spec' γh γe (l : loc) R :
      SPEC [inv N_node (head_inv γh γe l R)] {{ own γe (Excl ()) ∗ own γh (◯ (Excl' true, ε)) }}
        wait_for_succ #l
      {{ (n' : loc) γ, RET #(); 
          own γh (◯ (Excl' true, ε)) ∗ own γ (to_agree (n', false), 1/2)%Qp ∗ own γh (●{#1/2} (Excl' true, Excl' (Some (n', γ)))) ∗ ▷ inv N_signal (head_signal_loc γh n' γ R) }}.
    Proof. iSmash. Qed.
  End wait_for_succ.
  Existing Instance wait_for_succ_spec.
  Existing Instance wait_for_succ_spec'.

  Definition locked γh γe R : iProp Σ := 
    own γe (Excl ()) ∗ 
    (own γh (◯ (Excl' true, ε)) ∨ 
    ∃ (s : loc) γ, own γh (◯ (Excl' false, Excl' (Some (s, γ)))) ∗ own γ (to_agree (s, false), 1/2)%Qp ∗ inv N_signal (signal_loc γh γe s γ R)).

  Global Instance acquire_with_spec R (lk : val) (n : loc) γh γm γe :
    SPEC {{ is_lock γh γm γe R lk ∗ free_node γm n }}
      acquire_with lk #n
    {{ RET #(); R ∗ free_node γm n ∗ locked γh γe R }}.
  Proof. time iSmash. Qed. (* about 450 seconds now *)

  Global Program Instance acquire_spec R (lk : val) (n : loc) γh γm γe :
    SPEC {{ is_lock γh γm γe R lk }}
      acquire lk
    {{ RET #(); R ∗ locked γh γe R }}.

  Global Instance release_spec R γh γm γe (lk : val) :
    SPEC {{ is_lock γh γm γe R lk ∗ R ∗ locked γh γe R }}
      release lk
    {{ RET #(); emp }}.
  Proof. time iSmash. Qed.

  Program Instance acquired_mutual_exclusion γh γe R : ExclusiveProp (locked γh γe R).
End spec.




















