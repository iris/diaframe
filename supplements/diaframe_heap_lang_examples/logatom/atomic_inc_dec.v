From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From iris.heap_lang Require Import proofmode notation atomic_heap.

Import bi notation.

Local Obligation Tactic := program_smash_verify.


Definition make_counter `{!atomic_heap} : val := 
  λ: <>, ref #0.

Definition incr `{!atomic_heap} : val := 
  rec: "incr" "l" "k" :=
    let: "n" := !"l" in
    if: CAS "l" "n" ("n" + "k") then 
      "n"
    else 
      "incr" "l" "k".

Definition decr `{!atomic_heap} : val := 
  rec: "decr" "l" "k" :=
    let: "n" := !"l" in
    if: CAS "l" "n" ("n" - "k") then 
      "n" 
    else 
      "decr" "l" "k".

Definition read `{!atomic_heap} : val := 
  λ: "l", !"l".


Section proof.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ}.

  Program Instance make_counter_spec (l : loc) :
    SPEC {{ True }}
      make_counter #()
    {{ (l : loc), RET #l; l ↦ #0 }}.

  Program Instance incr_spec (l : loc) (k : Z) : 
    SPEC (z : Z), << l ↦ #z >>
      incr #l #k
    << RET #z; l ↦ #(z + k) >>.

  Program Instance decr_spec (l : loc) (k : Z) : 
    SPEC (z : Z), << l ↦ #z >>
      decr #l #k
    << RET #z; l ↦ #(z - k) >>.

  Program Instance read_spec (l : loc) :
    SPEC (z : Z), << l ↦ #z >>
      read #l
    << RET #z; l ↦ #z >>.
End proof.


































