From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From iris.heap_lang Require Import proofmode notation atomic_heap.

Set Default Proof Using "Type".

Import bi notation.

Local Obligation Tactic := program_smash_verify.


Definition new_stack `{!atomic_heap} : val := 
  λ: <>,  ref NONEV.

Definition push_this `{!atomic_heap} : val :=
  rec: "push" "s" "l" :=
    let: "val" := Fst (! "l") in
    let: "tail" := ! "s" in
    "l" <- ("val", "tail") ;; 
    if: CAS "s" "tail" (SOME "l") then 
      #() 
    else 
      "push" "s" "l".

Definition push `{!atomic_heap} : val :=
  λ: "s" "v",
    let: "l" := ref ("v", NONE) in
    push_this "s" "l".

Definition pop `{!atomic_heap} : val :=
  rec: "pop" "s" :=
    match: !"s" with
      NONE => NONEV
    | SOME "l" =>
      let: "next" := Snd (! "l") in
      if: CAS "s" (SOME "l") "next" then 
        SOME (Fst ! "l")
      else 
        "pop" "s"
    end.


Section spec.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ}.
  Let N := nroot .@ "stack".

  Fixpoint is_list (xs : list val) (lv : val) : iProp Σ :=
    match xs with
    | [] => ⌜lv = NONEV⌝
    | x :: xs => ∃ (l : loc), ⌜lv = SOMEV #l⌝ ∗ 
          ∃ lv', l ↦□ (x, lv') ∗ is_list xs lv'
    end.

  Global Instance is_list_timeless xs lv : Timeless (is_list xs lv).
  Proof. revert lv. induction xs as [| x xs]; tc_solve. Qed.

  Global Instance is_list_persistent xs lv : Persistent (is_list xs lv).
  Proof. revert lv. induction xs as [| x xs]; tc_solve. Qed.

  Definition is_stack l xs : iProp Σ :=
    ∃ vl, l ↦ vl ∗ is_list xs vl ∗ ⌜val_is_unboxed vl⌝%I.

  Global Program Instance biabd_islist_none xs :
    HINT ε₀ ✱ [- ; ⌜xs = []⌝] ⊫ [id]; is_list xs NONEV ✱ [⌜xs = []⌝].

  Global Program Instance biabd_islist_some (l : loc) xs :
    HINT ε₁ ✱ [x xs' t; l ↦□ (x, t) ∗ is_list xs' t ∗ ⌜xs = x :: xs'⌝] ⊫ [id]; 
         is_list xs (SOMEV #l) ✱ [⌜xs = x :: xs'⌝].

  Lemma is_list_agree xs ys v : is_list xs v ∗ is_list ys v ⊢ ⌜xs = ys⌝.
  Proof.
    revert ys v. induction xs.
    { move => [| y ys]; iSteps. }
    move => [| y ys]; iSteps as (v l) "Hxs Hl Hys".
    iAssert (⌜xs = ys⌝)%I as "->"; last done.
    iApply (IHxs ys v). iSteps.
  Qed.

  Lemma is_list_unboxed xs v : is_list xs v ⊢ ⌜val_is_unboxed v⌝.
  Proof. revert v. induction xs as [|x xs]; iSteps. Qed.

  Global Instance is_list_merge xs v ys :
    MergableConsume (is_list xs v) true (λ p Pin Pout,
      TCAnd (TCEq p true) $
      TCAnd (TCEq Pin (is_list ys v)) $
             TCEq Pout (is_list xs v ∗ ⌜xs = ys⌝)%I).
  Proof.
    rewrite /MergableConsume /= => p Pin Pout [-> [-> ->]] /=.
    iStep 2 as "Hxs Hys". iApply (is_list_agree _ _ (v)). iSteps.
  Qed.

  Global Instance is_list_decompose xs (l : loc) :
    MergableConsume (is_list xs (SOMEV #l)) true (λ p Pin Pout,
      TCAnd (TCEq p false) $
      TCAnd (TCEq Pin (ε₀)%I) $
             TCEq Pout (∃ x xs' v, ⌜xs = x :: xs'⌝ ∗ l ↦□ (x, v) ∗ is_list xs' v)%I).
  Proof.
    rewrite /MergableConsume /= => p Pin Pout [-> [-> ->]] /=.
    destruct xs; iSteps.
  Qed.

  Global Instance is_list_get_unboxed xs v :
    MergablePersist (is_list xs v) (λ p Pin Pout,
      TCAnd (TCEq p false) $
      TCAnd (TCEq Pin (ε₁)%I) $
             TCEq Pout ⌜val_is_unboxed v⌝)%I.
  Proof.
    rewrite /MergablePersist /= => p Pin Pout [-> [-> ->]] /=.
    iStep 2 as "Hxs". by iApply is_list_unboxed.
  Qed.

  Global Instance match_node (v : val) e1 e2 xs :
    SPEC [is_list xs v] {{ True }} Case v e1 e2 {{ [▷^0] RET Case v e1 e2; 
        (⌜xs = []⌝ ∗ ⌜v = NONEV⌝ ∨ (∃ (l' : loc) x' xs' v', ⌜xs = x'::xs'⌝ ∗ ⌜v = SOMEV #l'⌝ ∗ l' ↦□ (x', v') ∗ is_list xs' v' ∗ ⌜val_is_unboxed v'⌝ )) }} | 50.
  Proof.
    destruct xs; [(iStep 3 as (Φ) "HΦ") | iStep 3 as (l Φ v Hv) "Hl Hxs HΦ"].
    - iApply "HΦ". iSteps.
    - iApply "HΦ". iSteps.
  Qed.

  Global Program Instance new_stack_spec :
    SPEC {{ True }} 
      new_stack #() 
    {{ (s : loc), RET #s; is_stack s [] }}.

  Global Program Instance push_this_spec' (s l : loc) (v w : val) :
    SPEC (xs : list val), << l ↦ (v, w) ¦ is_stack s xs >> 
      push_this #s #l
    << RET #(); is_stack s (v :: xs) >>.

  Global Program Instance push_spec (s : loc)  (v : val) :
    SPEC xs, << is_stack s xs >>
      push #s v
    << RET #(); is_stack s (v :: xs) >>.

  Global Program Instance pop_spec (s : loc) :
    SPEC xs, << is_stack s xs >>
      pop #s
    << (v : val), RET v; ⌜xs = []⌝ ∗ is_stack s [] ∗ ⌜v = NONEV⌝ ∨ ∃ w xs', is_stack s xs' ∗ ⌜xs = w :: xs'⌝ ∗ ⌜v = SOMEV w⌝ >>.
End spec.





