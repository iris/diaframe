From diaframe.heap_lang Require Import proof_automation wp_auto_lob atomic_specs.
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import auth excl numbers.

From diaframe.heap_lang.examples.logatom Require Import queue_lib.

(* This queue is almost, _but not quite_ the Michael Scott queue. The consistent snapshots have been removed to simplify the verification. *)
Definition new_queue : val := 
  λ: <>, let: "n" := AllocN #2 NONEV in AllocN #2 "n".

Definition dequeue : val :=
  rec: "dequeue" "q" :=
    let: "head" := ! "q" in
    let: "next" := ! ("head" +ₗ #1) in
    match: "next" with
      NONE => NONEV
    | SOME "l" =>
      if: CAS "q" "head" "l" then 
        SOME ! "l"
      else 
        "dequeue" "q"
    end.

Definition enqueue : val :=
  λ: "q" "v",
    let: "node" := AllocN #2 NONEV in
    let: "tailptr" := "q" +ₗ #1 in
    "node" <- "v" ;;
    let: "newtail" := "node" in
    (rec: "set_tail" <> :=
      let: "tail" := (! "tailptr") in
      let: "next" := ! ("tail" +ₗ #1) in
      match: "next" with
        NONE => 
          if: CAS ("tail" +ₗ #1) NONE (SOME "newtail") then 
            CAS "tailptr" "tail" "newtail";; #()
          else 
            "set_tail" #()
      | SOME "l" =>
          CAS "tailptr" "tail" "l";;
          "set_tail" #()
      end) #().

Definition queue_value_list_R : cmra := authR $ max_prefix_listUR $ valO.
Definition queue_head_index_R : cmra := authR $ max_natUR.
Class queueAltG Σ := QueueAltG { 
  #[local] queueG1 :: inG Σ queue_value_list_R ;
  #[local] queueG2 :: inG Σ queue_head_index_R;
  #[local] queueG3 :: inG Σ queue_base_R ;
}.
Definition queueAltΣ : gFunctors := 
  #[GFunctor queue_value_list_R; GFunctor queue_head_index_R; GFunctor queue_base_R].

Global Instance subG_queueAltΣ {Σ} : subG queueAltΣ Σ → queueAltG Σ.
Proof. solve_inG. Qed.

Section queue_atomic.
  Context `{!heapGS Σ, !queueAltG Σ}.
  Set Default Proof Using "Type*".

  Definition is_queue_c_all γlv γll γih (l : loc) (ls : list loc) (vs : list val) (i : nat) : iProp Σ :=
    ∃ (l' : loc) (root : loc), 
      l ↦ #l' ∗ is_queue_base root ls vs ∗ own γlv (● to_max_prefix_list vs) ∗
        ⌜(root :: ls) !! i = Some l'⌝ ∗ own γih (● MaxNat i) ∗ own γll (● to_max_prefix_list (root :: ls)) ∗ ⌜length ls = length vs⌝.

  Definition is_queue_c γlv γll γih (l : loc) (cvs : list val) : iProp Σ :=
    ∃ (ls : list loc) (vs : list val) (i : nat), is_queue_c_all γlv γll γih l ls vs i ∗ ⌜cvs = drop i vs⌝.

  Definition queue_tail_inv (l : loc) γll γit : iProp Σ :=
    ∃ (l'' : loc) (ls : list loc) j, (l +ₗ 1) ↦ #l'' ∗ own γll (◯ to_max_prefix_list ls)
      ∗ ⌜ls !! j = Some l''⌝ ∗ own γit (● MaxNat j) ∗ ⌜NoDup ls⌝.

  Let queue_N := nroot.@"msc_queue".

  Definition is_queue_tail (l : loc) γll γit : iProp Σ := inv queue_N (queue_tail_inv l γll γit).

  Global Instance new_queue_spec :
    SPEC {{ True }} 
      new_queue #() 
    {{ (l : loc) γll γlv γih γit, RET #l; 
      is_queue_c γlv γll γih l [] ∗ is_queue_tail l γll γit }}.
  Proof. (* root is logically separate from the current head, so we need to give an explicit witness *)
    iSteps as (root l) "Hroot Hl". iExists [],[], root. iSteps.
  Qed.

  Hint Extern 4 (_ ≤ _)%nat => done : solve_pure_add.

  Lemma is_cons_not_nil (A : Type) (xs : list A) x xs' : xs = x :: xs' → xs ≠ [].
  Proof. by move ->. Qed.

  Hint Resolve is_cons_not_nil : solve_pure_add.
  Hint Immediate eq_sym : solve_pure_eq_add.

  Global Instance dequeue_spec γll γlv γih γit (l : loc) :
    SPEC vs, << is_queue_tail l γll γit ¦ is_queue_c γlv γll γih l vs >>
      dequeue #l 
    << (ov : val), RET ov; ⌜ov = NONEV⌝ ∗ ⌜vs = []⌝ ∗ is_queue_c γlv γll γih l []
                 ∨ ∃ v vs', ⌜vs = v :: vs'⌝ ∗ ⌜ov = SOMEV v⌝ ∗ is_queue_c γlv γll γih l vs' >>.
  Proof.
    iSteps --safe / as (_ l' Φ γlv' γll' γih' γit' Hlöb ls1 vs1 i hd root Hnd_ls1 Hi_hd Hls1_vs1) "Hqt IH Hq H●vs H●i H●ls Hl'".
   (* peek at head *) iRight. iSteps --safe / 
      as (root ls2 vs2 j hd' Hvs1_pref Hj_hd' Hj_ls Hls1_pref _ Hls2_vs2 _ _ _) 
          "H◯vs H◯ls H◯j Hl' H●vs H●j H●ls Hq Hhd" /
      as (root ls2 vs2 j hd' Hvs1_pref Hj_hd' Hij Hls1_pref _ Hls2_vs2 _ _ hd'' Hi_hd' Hi_ls2 v' Hi_v Hnd_ls2)
          "H◯vs H◯i H◯ls Hhd'' Hhd Hl' H●vs H●j H●ls Hq".
    - (* queue was empty *) 
      iSteps as (v'' vs3 Hjvs3 Hnd_ls2) "H◯vs H◯ls H◯j" / as (v'' vs3 Hjvs3 Hnd_ls2) "H◯vs H◯ls H◯j HΦ".
      all: contradict Hjvs3; rewrite drop_ge //; lia.
    - (* queue was not empty: abort, try to CAS *)iRight.
      iStep 16 as (root ls3 vs3 k Hnd_ls3 Hvs2_pref Hk_hd Hjk Hls2_pref _ Hls3_vs3)
                   "H◯vs H◯j H◯ls HΦ Hq H●vs H●j H●ls Hl" / as_anon; last iSmash. (* failing CAS is easy *)
      (* commit, we need to establish some pure facts first *)
      assert (i = k); last subst. (* not strictly needed to proceed, but this pure fact is needed for multiple subgoals *)
      { eapply NoDup_lookup. exact Hnd_ls3. prove_lookup. prove_lookup. }
      assert (drop k vs3 = v' :: drop (S k) vs3).
      { erewrite drop_S; last prove_lookup; done. }
      iSteps.
  Qed.

  Global Instance enqueue_spec γll γlv γih γit (l : loc) (v : val) :
    SPEC vs, << is_queue_tail l γll γit ¦ is_queue_c γlv γll γih l vs >>
      enqueue #l v
    << RET #(); is_queue_c γlv γll γih l (vs ++ [v]) >>.
  Proof.
    iSteps --safe / (* allocate node, start recursion, peek at tail and at next of tail *)
      as (_ _ nd l' Φ v' γlv' γll' γih' γit' Hlöb tl ls1 ls2 Hls1_tl Hnd_ls1 vs2 i el root Hi_el Hls1_pref Hls2_vs2 _ _) 
          "Hqt IH H◯ls H◯i1 Hnd1 Hnd2 H●vs H●i H●ls Hq Htl" /
      as (_ _ nd l' Φ v' γlv' γll' γih' γit' Hlöb tl ls1 i Hi_tl Hnd_ls1 ls2 vs2 j el1 
            root Hj_el1 Hls1_pref Hls2_vs2 _ _ el2 Hi_el2 Hi_ls2 v'' Hi_vs2 Hnd_ls2)
          "Hqt IH H◯i1 H◯ls Hel2 Hel1 Hnd1 Hnd2 H●vs H●i H●ls Hq".
    - (* next was empty: try to add our element here *)iRight. iStep 2 as (Hnd_ls2) "H◯vs H◯i2 H◯ls HAU".
      iSteps --safe / 
        as (root ls3 vs3 j el3 Hvs2_pref Hj_el3 Hij Hls2_pref _ Hls3_vs3 _ _ Hls2_ls3)
            "H◯ls H●vs H●i H●ls Hq Hlast" / as_anon; last iSmash. (* failing CAS is easy *)
      (* TODO: something is iffy here: need to rename both an existing coq hypothesis! 
        Note at 20-10-23: still iffy after removing delete_tac stuff. 
        Note at 01-11-23: It's because [prove_prefix] is terrible and calls [clear]. See test in [pure_solver.v] *)
      iSteps as_anon / as_anon /
        as (Hvs2_pref Hnd_ls3_nd ls4 m Hls4_pref_longest Hls3_pref_longest Hm_last_ls2 Hls2_ls4 Hnd_ls4)
            "Hnd H◯vs H◯i2 H◯ls HΦ H●it".
      { iPureIntro. rewrite !length_app /=; lia. }
      { iPureIntro. rewrite drop_app_le //. apply lookup_lt_Some in Hj_el3. simpl in Hj_el3. lia. }
      (* CAS succeeded and committed. We need manual help for the second CAS, to establish some pure facts *)
      iExists (S (length ls3)). iSteps.
      * iPureIntro.
        assert ((root :: ls3 ++ [nd]) !! S (length ls3) = Some nd) as Hd; last by prove_lookup.
        simpl. by apply list_lookup_middle.
      * iPureIntro. enough (length ls2 = m); first lia.
        eapply (NoDup_lookup (longest ls4 (root :: ls3 ++ [nd]))).
        prove_nodup. prove_lookup. prove_lookup.
    - (* next was SOME: abort and swing tail pointer forward *) iRight.
      iSteps as (ls3 k Hls3_pref_longest Hls2_pref_longest Hk_tl Hik Hnd_ls3) "H◯vs H◯j H◯ls HAU H●it".
      iExists (S i). iSteps; iPureIntro.
      * assert ((root :: ls2) !! S i = Some el2) as Hd; prove_lookup.
      * enough (i = k); first lia.
        eapply (NoDup_lookup (longest ls3 (root :: ls2))).
        prove_nodup. prove_lookup. prove_lookup.
  Qed.
End queue_atomic.

















