From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From diaframe.lib Require Import own_hints.
From iris.algebra Require Import agree frac excl auth.
From iris.heap_lang Require Import atomic_heap.
From diaframe.heap_lang.examples.logatom Require Import atomic_lock.


Definition new_lock : val :=
    λ: "_", ref (ref #false).

Definition new_node : val :=
  λ: "_", 
    let: "ret" := AllocN #2 NONE in
    "ret" <- ref #false;;
    "ret".

Definition wait_on : val :=
  rec: "wait_on" "l" :=
    if: ! "l" then 
      "wait_on" "l"
    else 
      #().

Definition acquire_with : val :=
  λ: "lk" "node",
    let: "l" := ! "node" in
    "l" <- #true;;
    let: "pred" := Xchg "lk" "l" in  (* also called fetch_and_store sometimes *)
    ("node" +ₗ #1) <- "pred";;
    wait_on "pred".

Definition acquire : val :=
  λ: "lk",
    let: "node" := new_node #() in
    acquire_with "lk" "node";;
    "node".

Definition release_with : val :=
  λ: "lk" "node",
    ! "node" <- #false;;
    "node" <- ! ("node" +ₗ #1).

Definition release : val :=
  λ: "lk" "node",
    release_with "lk" "node" ;;
    Free (! "node") ;; Free "node" ;; Free ("node" +ₗ #1).


Definition clh_lock_firstR := prodR (agreeR $ leibnizO loc) fracR.
Definition clh_lock_secondR := authR $ optionUR $ exclR $ leibnizO loc.
Class clh_lockG Σ := CLHLockG { 
  #[local] clh_lock_firstG :: inG Σ clh_lock_firstR;
  #[local] clh_lock_secondG :: inG Σ clh_lock_secondR;
}.
Definition clh_lockΣ : gFunctors := #[GFunctor clh_lock_firstR; GFunctor clh_lock_secondR].

Local Obligation Tactic := program_smash_verify.

Global Program Instance subG_clh_lockΣ {Σ} : subG clh_lockΣ Σ → clh_lockG Σ.


Section spec.
  Context `{!heapGS Σ, clh_lockG Σ}. 
  (* not specified on top of logically atomic heap, since current definition does not come with array allocation *)

  Definition N_node := nroot .@ "clh_node".

  Definition free_node n : iProp Σ :=
    ∃ (l : loc) v, n ↦ #l ∗ (n +ₗ 1) ↦ v ∗ l ↦ #false.

  Program Instance wait_on_phys (l : loc) :
    SPEC q (b : bool), << ▷ l ↦{q} #b >>
      wait_on #l
    << RET #(); ⌜b = false⌝ ∗ l ↦{q} #false >>.

  Definition holds_at_loc (l : loc) P : iProp Σ :=
    ∃ (l' : loc), l ↦ #l' ∗ P l'.

  Definition queued_loc γe l γ : iProp Σ := 
    own γ (to_agree l, 1%Qp) ∨ (∃ (b : bool), l ↦{#1/2} #b ∗ (⌜b = true⌝ ∨ (⌜b = false⌝ ∗ own γe (to_agree l, 1/2 + 1/2/2)%Qp ∗ own γ (to_agree l, 1/2)%Qp ∗ l ↦{#1/2} #b))).

  Definition is_queued_loc γe l' : iProp Σ :=
      ∃ γ, own γ (to_agree l', 1/2)%Qp ∗ inv N_node (queued_loc γe l' γ).

  Definition is_queue_head γe (p : loc) : iProp Σ := own γe (to_agree p, 1/2/2)%Qp.

  Definition locked_at γe (p : loc) : iProp Σ := own γe (to_agree p, 1/2 + 1/2/2)%Qp.
  Definition locked_at_private γe n (p : loc) : iProp Σ :=
    (n +ₗ 1) ↦ #p ∗ p ↦ #false ∗ ∃ (l' : loc), n ↦ #l' ∗ is_queued_loc γe l' ∗ l' ↦{#1/2} #true.

  Instance acquire_with_phys (l n : loc) γe :
    SPEC ⟨↑N_node⟩ (p : loc), << free_node n ¦ ▷ holds_at_loc l (is_queued_loc γe) ∗ ▷ is_queue_head γe p >>
      acquire_with #l #n
    << RET #(); ▷ holds_at_loc l (is_queued_loc γe) ∗ is_queue_head γe p ∗ locked_at γe p ∗ locked_at_private γe n p >>.
  Proof.
    iSteps --safe / as (Φ p v l1 l2 γ) "HN Hn1 Hn Hp Hl1 Hγ".
    iRight. iSteps as (γ' _ _) "HNp Hγ' Hp HAU Hn2 Hγe Hl2". iMod "HAU". 
    iDecompose "HAU" as (l3 γl3 _) "HIl3 Hl Hγl3 Hγe Hcl".
    iMod (diaframe_close_right with "Hcl [-]") as "[_ HΦ]"; iSteps.
  Qed.

  Instance release_with_phys (l n : loc) γe p' :
    SPEC ⟨↑N_node⟩ (p : loc), << ▷ locked_at_private γe n p' ¦ ▷ holds_at_loc l (is_queued_loc γe) ∗ ▷ is_queue_head γe p ∗ ▷ locked_at γe p' >>
      release_with #l #n
    << (p'' : loc), RET #(); free_node n ¦ ▷ holds_at_loc l (is_queued_loc γe) ∗ is_queue_head γe p'' >>.
  Proof.
    iSteps as (Φ p γ) "HN Hn1 Hp' Hγ HAU Hn2 Hp". iMod "HAU" as "HAU".
    iDecompose "HAU" as (l1 l2 γ' _) "HNl2 Hl Hγ' Hl1 Hn1 Hγe Hcl".
    replace (1/2 + 1/2/2 + 1/2/2)%Qp with 1%Qp; last first.
    { rewrite -assoc !Qp.div_2 //. }
    iMod (own_update _ _ (to_agree p, 1)%Qp with "Hγe") as "[Hγe1 [Hγe2 Hγe3]]"; first by apply cmra_update_exclusive.
    iCombine "Hγe1 Hγe2" as "Hγe".
    iMod (diaframe_close_right_quant with "Hcl [Hl Hγ' Hγe3]") as "[_ HΦ]"; iSteps.
  Qed.

  Global Program Instance new_node_spec :
    SPEC {{ True }} 
      new_node #() 
    {{ (n : loc), RET #n; free_node n }}.

  Global Instance new_lock_spec :
    SPEC {{ True }} 
      new_lock #() 
    {{ (l p : loc) γe, RET #l; holds_at_loc l (is_queued_loc γe) ∗ is_queue_head γe p }}.
  Proof.
    iSteps as (l p γe) "Hγe Hl".
    iMod (own_alloc (to_agree l, 1)%Qp) as "[%γ [Hγ [Hγ' Hγ'']]]"; first done.
    iCombine "Hγ Hγ'" as "Hγ".
    iSteps.
  Qed.

  Global Program Instance acquire_spec (l : loc) γe :
    SPEC ⟨↑N_node⟩ (p : loc), << ▷ holds_at_loc l (is_queued_loc γe) ∗ ▷ is_queue_head γe p >>
      acquire #l
    << (n : loc), RET #n; ▷ holds_at_loc l (is_queued_loc γe) ∗ is_queue_head γe p ∗ locked_at γe p ∗ locked_at_private γe n p >>.

  Global Program Instance release_spec (l n : loc) γe p' :
    SPEC ⟨↑N_node⟩ (p : loc), << ▷ locked_at_private γe n p' ¦ ▷ holds_at_loc l (is_queued_loc γe) ∗ ▷ is_queue_head γe p ∗ ▷ locked_at γe p' >>
      release #l #n
    << (p'' : loc), RET #(); ▷ holds_at_loc l (is_queued_loc γe) ∗ is_queue_head γe p'' >>.
  (* seems like absence of shared parameter between locked_at and locked_at_private is cumbersome.
     can usually be alleviated by awkward ghost state like here and in ticket lock. *)
End spec.


(* Additionally, we show clh_lock inhabits the atomic_lock interface. *)

Section clh_lock_is_atomic_lock.
  Context `{!heapGS Σ, !clh_lockG Σ}.
  Import atomic_lock.

  Global Program Instance clh_lock_atomic_lock : atomic_lock Σ :=
    {| newlock := new_lock;
       acquire := atomic_clhlock.acquire;
       release := atomic_clhlock.release;
       name := gname;
       raw_lock := loc;
       lock_mask := ⊤ ∖ ↑ N_node;
       client_namespace := (nroot.@ "free");
       as_lock := (λ l, #l);
       lock_inv := (λ γ l, ∃ p, holds_at_loc l (is_queued_loc γ) ∗ is_queue_head γ p)%I;
       locked_at := (λ γ _, ∃ (p : loc), own γ (to_agree p, 1/2 + 1/2/2/2)%Qp)%I;
       locked_at_private := (λ γ v, ∃ (n : loc), ⌜v = #n⌝ ∗ ∃ (p : loc), atomic_clhlock.locked_at_private γ n p ∗ own γ (to_agree p, 1/2/2/2)%Qp)%I;
    |}.
  Next Obligation.
    iSteps --safe / as_anon / 
      as (Φ n p γp l' l γl _ l'' γl'') "HNp _ HNl HNl'' Hγl Hγ Hl1 Hn Hl2 Hγl'' Hl''";
                first iSmash.
    iStep as "_' [Hγ1 [Hγ2 Hγ3]]".
    iCombine "Hγ1 Hγ2" as "Hγ". iSteps. iExists _. iSteps.
  Qed.
  Next Obligation.
    iSteps --safe / 
      as (_ Φ n p γp _ _ l1 γl1 l2 γl2 _) "HNp HNl1 _ HNl2 Hγl2 Hγ" / as_anon;
                                                    last iSmash.
    iRight. iStep as "_' [Hγ1 [Hγ2 Hγ3]]".
    iCombine "Hγ1 Hγ2" as "Hγ". iSteps.
  Qed.
End clh_lock_is_atomic_lock.




















