From iris.algebra Require Import excl.
From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From diaframe.lib Require Import own_hints.
From iris.heap_lang Require Import proofmode notation atomic_heap.
From diaframe.heap_lang.examples.logatom Require Import atomic_lock.
Set Default Proof Using "Type".

Import bi notation.


Definition newlock `{!atomic_heap} : val := 
  λ: <>, ref #false.

Definition acquire `{!atomic_heap} : val :=
  rec: "acquire" "l" := 
    if: CAS "l" #false #true then 
      #() 
    else 
      "acquire" "l".

Definition release `{!atomic_heap} : val := 
  λ: "l", "l" <- #false.


Class lockG Σ := LockG { #[local] lock_tokG :: inG Σ (exclR unitO) }.
Definition lockΣ : gFunctors := #[GFunctor (exclR unitO)].

Local Obligation Tactic := program_smash_verify.

Global Program Instance subG_lockΣ {Σ} : subG lockΣ Σ → lockG Σ.

Section proof.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ, !lockG Σ}.

  Definition lock_state (γ : gname) (l : loc) (b : bool) : iProp Σ :=
    l ↦ #b ∗ (⌜b = true⌝ ∨ ⌜b = false⌝ ∗ own γ (Excl ())).

  Definition locked (γ : gname) : iProp Σ :=
    own γ (Excl ()).

  Global Program Instance newlock_spec :
    SPEC {{ True }}
      newlock #()
    {{ (l : loc) γ, RET #l; lock_state γ l false}}.

  Global Program Instance acquire_spec (l : loc) γ :
    SPEC b, << ▷ lock_state γ l b >>
        acquire #l
    << RET #(); lock_state γ l true ∗ locked γ ∗ ⌜b = false⌝ >>.

  Global Program Instance release_spec (l : loc) γ :
    SPEC b, << ▷ lock_state γ l b ∗ ▷ locked γ >>
        release #l
    << RET #(); lock_state γ l false ∗ ⌜b = true⌝ >>.

  Global Program Instance locked_timeless γ : Timeless (locked γ).
  Global Program Instance locked_exclusive γ : ExclusiveProp (locked γ).
End proof.

Global Opaque lock_state locked acquire release newlock.


(* Additionally, we show spin_lock inhabits the atomic_lock interface. *)

Section spin_lock_is_atomic_lock.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ, !lockG Σ}.
  Import atomic_stateless_lock.

  Global Program Instance spin_lock_atomic_stateless_lock : atomic_stateless_lock Σ :=
    {| newlock := atomic_spinlock.newlock;
       acquire := atomic_spinlock.acquire;
       release := atomic_spinlock.release;
       name := gname;
       raw_lock := loc;
       lock_mask := ⊤;
       client_namespace := nroot;
       as_lock := (λ l, #l);
       lock_inv := (λ γ l, ∃ b, lock_state γ l b)%I;
       locked := atomic_spinlock.locked;
       locked_private := λ _, emp%I;
    |}.
End spin_lock_is_atomic_lock.
