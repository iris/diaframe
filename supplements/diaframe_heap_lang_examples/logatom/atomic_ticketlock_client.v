From iris.algebra Require Import coPset excl auth.
From diaframe.heap_lang Require Import proof_automation atomic_specs.
From iris.heap_lang Require Import proofmode notation atomic_heap.
From diaframe.lib Require Import ticket_cmra own_hints.
From diaframe.heap_lang.examples.logatom Require Import atomic_ticketlock.


Definition foo `{!atomic_heap} : val :=
  λ: "lk" "left" "right" "z",
    acquire "lk";;
    "left" <- "z";;
    "right" <- "z";;
    release "lk".

Definition run_foo `{!atomic_heap} : val :=
  λ: <>, 
    let: "lk" := newlock #() in
    let: "left" := ref #0 in
    let: "right" := ref #0 in
    Fork (foo "lk" "left" "right" #3);;
    Fork (foo "lk" "left" "right" #5);;
    acquire "lk";;
    if: ! "left" = ! "right" then 
      #()
    else 
      #() #() (* unsafe *).


Section proof.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ, !tlockG Σ}.

  Definition has_state γ1 γ2 lk1 lk2 l1 l2 (z : Z) : iProp Σ :=
      ∃ (n : nat) b, lock_state γ1 γ2 lk1 lk2 n b ∗
        (⌜b = true⌝ ∨ (l1 ↦ #z ∗ l2 ↦ #z ∗ ⌜b = false⌝)).

  Obligation Tactic := program_smash_verify.

  Program Instance thread_spec_1 γ1 γ2 (lk1 lk2 l1 l2 : loc) (z : Z) :
    SPEC (m : Z), << ▷ has_state γ1 γ2 lk1 lk2 l1 l2 m >>
      foo (#lk1, #lk2)%V #l1 #l2 #z
    << RET #(); has_state γ1 γ2 lk1 lk2 l1 l2 z >>.
    (* Voila only verifies until here. We go one step further, using it in a client *)

  (* Below verifications have some struggle with has_state since it underdetermines Z.
     One could introduce an exclR ZO ghost state, tracking the value of the locations.
     This is also not perfect since it requires us to give the update hint, since 
     the hint library is not smart enough to figure that one out. *)

  Opaque has_state.
  Global Program Instance thread_spec_2 γ1 γ2 (lk1 lk2 l1 l2 : loc) (z : Z) :
    SPEC {{ inv nroot (∃ z, has_state γ1 γ2 lk1 lk2 l1 l2 z) }}
      foo (#lk1, #lk2)%V #l1 #l2 #z
    {{ RET #(); True }}.
    (* thread_spec_1 shows the state transition is like we want. but this spec cannot be used
        in Fork (foo _) statements. So we derive a regular spec from the atomic one *)
  Transparent has_state.

  Global Instance run_foo_spec :
    SPEC {{ True }} run_foo #() {{ RET #(); True }}.
  Proof. iSteps; iExists Z0; iSteps. Qed.
  (* underdetermination of the Z thing prevents iSmash from working *)
End proof.

















