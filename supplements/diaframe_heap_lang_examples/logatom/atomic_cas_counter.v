From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From iris.heap_lang Require Import proofmode notation atomic_heap.

Import bi notation.

Local Obligation Tactic := program_smash_verify.


Definition make_counter `{!atomic_heap} : val := 
  λ: <>, ref #0.

Definition incr `{!atomic_heap} : val := 
  rec: "incr" "l" :=
    let: "n" := !"l" in
    if: CAS "l" "n" (#1 + "n") then 
      "n" 
    else 
      "incr" "l".

Definition weak_incr `{!atomic_heap} : val :=
  λ: "l", 
    let: "n" := !"l" in
    "l" <- "n" + #1;;
    "n".

Definition read `{!atomic_heap} : val := 
  λ: "l", !"l".


Section proof.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ}.

  (* physical specification *)
  Global Program Instance incr_spec (l : loc) :
    SPEC (z : Z), << ▷ l ↦ #z >>
        incr #l
    << RET #z; l ↦ #(z + 1) >>.

  Global Program Instance weakincr_spec (l : loc) (z : Z) :
    SPEC v, << ▷ l ↦ v ∗ ⌜v = #z⌝ >> (* this can actually be quite a useful specification, as we will see in ticket_lock *)
        weak_incr #l
    << RET #z; l ↦ #(z + 1) >>.

  Global Program Instance read_spec (l : loc) :
    SPEC (z : Z), << l ↦ #z >>
        read #l
    << RET #z; l ↦ #z >>.
End proof.


































