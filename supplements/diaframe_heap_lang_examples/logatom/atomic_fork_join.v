From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From iris.heap_lang Require Import proofmode notation atomic_heap.

Import bi notation.

Local Obligation Tactic := program_smash_verify.


Definition make_join `{!atomic_heap} : val :=
  λ: <>, ref NONE.

Definition set `{!atomic_heap} : val :=
  λ: "c" "v",
    "c" <- SOME "v".

Definition wait `{!atomic_heap} : val :=
  rec: "join" "c" :=
    match: !"c" with
      SOME "x" => "x"
    | NONE => "join" "c"
    end.


Section proof.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ}.

  Definition fork_join_state l (ov : option val) : iProp Σ := 
    (* alternatively:    ∃ v, l ↦ v ∗ (⌜v = NONEV⌝ ∗ ⌜ov = None⌝ ∨ ∃ v', ⌜v = SOMEV v'⌝ ∗ ⌜ov = Some v'⌝). *)
     (l ↦ NONEV ∗ ⌜ov = None⌝) ∨ (∃ v', l ↦ SOMEV v' ∗ ⌜ov = Some v'⌝).

  Global Program Instance make_join_spec : 
    SPEC {{ True }} 
      make_join #()
    {{ (l : loc), RET #l; fork_join_state l None }}.

  Global Program Instance set_spec' (l : loc) (v : val) :
    SPEC (ov : option val), << ▷ fork_join_state l ov >>
      set #l v
    << RET #(); fork_join_state l (Some v) >>.

  Global Program Instance wait_spec' (l : loc) :
    SPEC (ov : option val), << ▷ fork_join_state l ov >>
      wait #l
    << (v : val), RET v; fork_join_state l (Some v) ∗ ⌜ov = Some v⌝ >>.
End proof.
Global Opaque fork_join_state.
































