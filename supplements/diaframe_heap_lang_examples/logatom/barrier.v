From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob wp_later_credits.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import frac_auth numbers excl.
From diaframe.lib Require Import ticket_cmra frac_token own_hints.


(* TODO: which version of barrier to keep? *)

Definition make_barrier : val := 
  λ: <>, ref #0.

Definition sync_up_enter : val := rec: "sync_enter" "b" :=
  let: "z" := ! "b" in
  if: #0 ≤ "z" then
    if: CAS "b" "z" ("z" + #1) then 
      "z" 
    else 
      "sync_enter" "b"
  else "sync_enter" "b".

Definition wait_for : val :=
  rec: "wait_for" "b" "w" := 
    let: "z" := ! "b" in
    if: "z" = "w" then
      #()
    else
      "wait_for" "b" "w".

Definition sync_up_exit threads : val := 
  λ: "b" "v",
    let: "w" := 
      if: "v" = #0 then 
        #(Zpos threads) 
      else 
        "v" 
      in
    wait_for "b" "w";;
    "b" <- "w" - #1.

Definition sync_up threads : val := λ: "b",
  sync_up_exit threads "b" (sync_up_enter "b").

Definition sync_down_enter : val := rec: "sync_enter" "b" :=
  let: "z" := ! "b" in
  if: "z" ≤ #0 then
    if: CAS "b" "z" ("z" - #1) then
     - "z" 
    else 
      "sync_enter" "b"
  else 
    "sync_enter" "b".

Definition sync_down_exit threads : val := 
  λ: "b" "v",
    let: "w" := 
      if: "v" = #0 then 
        #(Zneg threads) 
      else 
        - "v" 
      in
    wait_for "b" "w";;
    "b" <- "w" + #1.

Definition sync_down threads : val := λ: "b",
  sync_down_exit threads "b" (sync_down_enter "b").

Definition barrierR := authR $ optionUR $ exclR $ leibnizO Z.
Class barrierG Σ :=
  BarrierG { 
    #[local] barrier_ticketG :: inG Σ coPset_disjR;
    #[local] barrier_tokenG :: fracTokenG Σ;
    #[local] barrier_barrierG :: inG Σ barrierR;
  }.
Definition barrierΣ : gFunctors :=
  #[GFunctor ticketUR; fracTokenΣ; GFunctor barrierR].

Local Obligation Tactic := program_smash_verify.

Global Program Instance subG_barrierΣ {Σ} : subG barrierΣ Σ → barrierG Σ.

Section proof.
  Context `{!heapGS Σ}.
  Set Default Proof Using "heapGS0".

  Program Instance sync_up_wait_for_spec (l : loc) (zw : Z) :
    SPEC (dq : dfrac) (z : Z), << ▷ l ↦{dq} #z >>
      wait_for #l #zw
    << RET #(); ⌜z = zw⌝ ∗ l ↦{dq} #zw ∗ £ 2 >>. (* other order does not work because the hint for abstract dfrac is missing *)

  Context (threads : positive).
  Context `{!barrierG Σ}.
  Implicit Types P : Qp → iProp Σ.
  Let N := nroot .@ "barrier".
  Set Default Proof Using "threads heapGS0 barrierG0".

  Definition barrier_inv P1 P2 R γp1 γp2 γt γb (l : loc) : iProp Σ :=
    ∃ (z : Z), l ↦ #z ∗ own γt (CoPset $ tickets_geq (S (Z.abs_nat z))) ∗ own γb (● Excl' z) ∗ (
      (⌜0 < z⌝%Z ∗ (
          (⌜z < Zpos threads⌝%Z ∗ 
            ((own γt (CoPset $ ticket (Z.abs_nat z)) 
              ∗ ((⌜Fractional P1⌝ ∗ no_tokens P1 γp1 1%Qp ∗ own γt (CoPset $ ticket 0) ∗ token_counter P2 γp2 (Z.to_pos (Zpos threads - z)))
                  ∨
                 (own γb (◯ Excl' z) ∗ token_counter P1 γp1 (Z.to_pos (Zpos threads - z)) ∗ no_tokens P2 γp2 1%Qp ∗ ⌜Fractional P2⌝)
                ))
                ∨ 
            ((own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket 0) ∗ no_tokens P1 γp1 1%Qp ∗ ⌜Fractional P1⌝ ∗ token_counter P2 γp2 (Z.to_pos (Zpos threads - z))))
          )
                ∨
          (⌜z = Zpos threads⌝ ∗ no_tokens P1 γp1 1%Qp ∗ no_tokens P2 γp2 1%Qp ∗ P1 1%Qp ∗ ⌜Fractional P1⌝ ∗ ⌜Fractional P2⌝ ∗ (own γt (CoPset $ ticket 0) ∨ own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket $ Pos.to_nat threads))))
        ∨ 
      (⌜z < 0⌝%Z ∗ (
          (⌜Zneg threads < z⌝%Z ∗ 
            ((own γt (CoPset $ ticket (Z.abs_nat z)) 
              ∗ ((⌜Fractional P2⌝ ∗ no_tokens P2 γp2 1%Qp ∗ own γt (CoPset $ ticket 0) ∗ token_counter P1 γp1 (Z.to_pos (Zpos threads + z)))
                  ∨
                 (own γb (◯ Excl' z) ∗ token_counter P2 γp2 (Z.to_pos (Zpos threads + z)) ∗ no_tokens P1 γp1 1%Qp ∗ ⌜Fractional P1⌝)
                ))
                ∨ 
            ((own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket 0) ∗ no_tokens P2 γp2 1%Qp ∗ ⌜Fractional P2⌝ ∗ token_counter P1 γp1 (Z.to_pos (Zpos threads + z))))
          )
                ∨
          (⌜z = Zneg threads⌝ ∗ no_tokens P1 γp1 1%Qp ∗ no_tokens P2 γp2 1%Qp ∗ P1 1%Qp ∗ ⌜Fractional P1⌝ ∗ ⌜Fractional P2⌝ ∗ (own γt (CoPset $ ticket 0) ∨ own γb (◯ Excl' z)) ∗ own γt (CoPset $ ticket (Pos.to_nat threads)))))
        ∨
      (⌜z = 0⌝%Z ∗ (
        own γt (CoPset $ ticket 0) ∗ own γb (◯ Excl' z) ∗ (
          (⌜Fractional P2⌝ ∗ token_counter P1 γp1 threads ∗ no_tokens P2 γp2 1%Qp)
          ∨
          (⌜Fractional P1⌝ ∗ no_tokens P1 γp1 1%Qp ∗ token_counter P2 γp2 threads)
        )
      ))
    ) ∗ R.

  Definition as_atom {PROP : bi} (P : PROP) : PROP := P.
  Typeclasses Opaque as_atom.
  Notation "'𝒜{' P '}'" := (as_atom P).
  Global Instance as_atom_into_connective {PROP : bi} (G : PROP) : AtomIntoConnective (𝒜{ G }) G.
  Proof. rewrite /as_atom /AtomIntoConnective //. Qed.

  Definition is_barrier P1 P2 R γp1 γp2 γt γb (v : val) : iProp Σ :=
    ∃ (l : loc), ⌜v = #l⌝ ∗ □ (𝒜{ R ∗ P1 1%Qp ={⊤∖↑N}=∗ P2 1%Qp ∗ ◇R}) ∗ □ (𝒜{ R ∗ P2 1%Qp ={⊤∖↑N}=∗ P1 1%Qp ∗ ◇R}) ∗ inv N (barrier_inv P1 P2 R γp1 γp2 γt γb l).

  Global Instance newbarrier_spec :
    SPEC {{ True }} 
      make_barrier #()
    {{ (v : val), RET v; ∀ P1 P2 R, ⌜Fractional P1⌝ ∗ ⌜Fractional P2⌝ ∗ P1 1%Qp ∗ R ∗ □ (R ∗ P1 1%Qp ={⊤∖↑N}=∗ P2 1%Qp ∗ ◇R) ∗ □ (R ∗ P2 1%Qp ={⊤∖↑N}=∗ P1 1%Qp ∗ ◇R) ={⊤}=∗ 
            ∃ γp1 γp2 γt γb, is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token_iter P1 (Pos.to_nat threads) γp1 }}.
  Proof. iSteps. unseal_diaframe => /=. rewrite /as_atom. iFrame "#". iSteps. Qed.

  Context P1 P2 (R : iProp Σ).

  Section sync_up.
    Local Program Instance sync_up_enter_phys_spec (l : loc) :
      SPEC (z : Z), << ▷ l ↦ #z >>
        sync_up_enter #l
      << RET #z; l ↦ #(z + 1) ∗ ⌜0 ≤ z⌝%Z ∗ £ 2 >>.

    Program Instance sync_up_enter_spec γp1 γp2 γt γb (v : val) :
      SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P1 γp1 }}
        sync_up_enter v
      {{ (z : Z), RET #z; ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket (Z.to_nat z)) }}.
    (* we use ⌜Fractional _⌝ as the guarding hypotheses. Using ticket 0 or no_tokens P1 is problematic:
        for no_tokens, the problematic transition is aborting the Zpos threads -1 -> Zpos threads shift, caused by [token_dealloc_hint]
        for ticket 0, the problematic transition is 0 -> 1 with threads greater than 1. *)
  End sync_up.
  Local Existing Instance sync_up_enter_spec.

  Program Instance sync_up_exit_spec γp1 γp2 γb γt (z : Z) (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket $ Z.to_nat z) }}
      sync_up_exit threads v #z
    {{ RET #(); token P2 γp2 }}.

  Global Program Instance sync_up_spec γp1 γp2 γt γb (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P1 γp1 }}
      sync_up threads v
    {{ RET #(); token P2 γp2 }}.

  Section sync_down.
    Local Program Instance sync_down_enter_phys_spec (l : loc) :
      SPEC (z : Z), << ▷ l ↦ #z >>
        sync_down_enter #l
      << RET #-z; l ↦ #(z - 1) ∗ ⌜z ≤ 0⌝%Z ∗ £ 2 >>.

    Instance sync_down_enter_spec γp1 γp2 γt γb (v : val) :
      SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P2 γp2 }}
        sync_down_enter v
      {{ (z : Z), RET #z; ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket (Z.to_nat z)) }}.
    Proof.
      iStep 2. replace (⊤ ∖ ∅) with (⊤ : coPset); last set_solver.
      (* TODO: address this strange mask stuff. *)
      iSteps. apply inhabitant.
    Qed.
  End sync_down.
  Local Existing Instance sync_down_enter_spec.

  Program Instance sync_down_exit_spec γp1 γp2 γb γt (z : Z) (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ ⌜0 ≤ z⌝%Z ∗ own γt (CoPset $ ticket $ Z.to_nat z) }}
      sync_down_exit threads v #z
    {{ RET #(); token P1 γp1 }}.

  Global Program Instance sync_down_spec γp1 γp2 γt γb (v : val) :
    SPEC {{ is_barrier P1 P2 R γp1 γp2 γt γb v ∗ token P2 γp2 }}
      sync_down threads v
    {{ RET #(); token P1 γp1 }}.

  Global Program Instance barrier_persistent_reuse γp1 γp2 γt γb (v : val) :
    Persistent (is_barrier P1 P2 R γp1 γp2 γt γb v).
End proof.

Global Opaque is_barrier make_barrier sync_up sync_down.

