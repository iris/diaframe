From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From diaframe.lib Require Import own_hints max_prefix_list_hints.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import auth excl numbers.

From diaframe.heap_lang.examples.logatom Require Import queue_lib.

Definition new_queue : val := λ: "_", 
  let: "n" := AllocN #2 NONEV in ref "n".

Definition dequeue : val :=
  rec: "dequeue" "q" :=
    let: "head" := ! "q" in
    let: "next" := ! ("head" +ₗ #1) in
    match: "next" with
      NONE => NONEV
    | SOME "l" =>
      if: CAS "q" "head" "l" then 
        SOME ! "l"
      else 
        "dequeue" "q"
    end.

Definition get_tail : val :=
  rec: "get_tail" "head" :=
    let: "next" := ! ("head" +ₗ #1) in
    match: "next" with
      NONE => "head"
    | SOME "l" =>
      "get_tail" "l"
    end.

Definition set_tail : val :=
  rec: "set_tail" "head" "next" :=
    let: "tail" := get_tail "head" in
    if: CAS ("tail" +ₗ #1) NONE (SOME "next") then 
      #()
    else 
      "set_tail" "head" "next". 
  (* doing the recursive call with "head" is inefficient, but most similar to Capers implementation. 
     One can freely replace "head" with "tail" to get a more efficient version that can be verified without changes to the proof *)

Definition enqueue : val :=
  λ: "q" "v",
    let: "node" := AllocN #2 NONEV in
    "node" <- "v" ;;
    let: "head" := ! "q" in
    set_tail "head" "node".

Definition queue_value_list_R : cmra := authR $ max_prefix_listUR $ valO.
Definition queue_head_index_R : cmra := authR $ max_natUR.
Class queueAltG Σ := QueueAltG { 
  #[local] queueG1 :: inG Σ queue_value_list_R ;
  #[local] queueG2 :: inG Σ queue_head_index_R;
  #[local] queueG3 :: inG Σ queue_base_R ;
}.
Definition queueAltΣ : gFunctors := 
  #[GFunctor queue_value_list_R; GFunctor queue_head_index_R; GFunctor queue_base_R].

Global Instance subG_queueAltΣ {Σ} : subG queueAltΣ Σ → queueAltG Σ.
Proof. solve_inG. Qed.

Section queue_atomic.
  Context `{!heapGS Σ, !queueAltG Σ}.

  Definition is_queue_c_all γlv γll γi (l : loc) (ls : list loc) (vs : list val) (i : nat) : iProp Σ :=
    ∃ (l' : loc) (root : loc), 
      l ↦ #l' ∗ is_queue_base root ls vs ∗ own γlv (● to_max_prefix_list vs) ∗
        ⌜(root :: ls) !! i = Some l'⌝ ∗ own γi (● MaxNat i) ∗ own γll (● to_max_prefix_list (root :: ls)) ∗ ⌜length ls = length vs⌝.

  Definition is_queue_c γlv γll γi (l : loc) (cvs : list val) : iProp Σ :=
    ∃ (ls : list loc) (vs : list val) (i : nat), is_queue_c_all γlv γll γi l ls vs i ∗ ⌜cvs = drop i vs⌝.

  Lemma new_queue_spec :
    {{{ True }}} new_queue #() {{{ (l : loc) γll γlv γm, RET #l; is_queue_c γll γlv γm l [] }}}.
  Proof. (* root is logically separate from the current head, so we need to give an explicit witness *)
    iSteps as (Φ l γll) "Hl". iExists [],[], l. iSteps.
  Qed.

  Hint Extern 4 (_ ≤ _)%nat => done : solve_pure_add.

  Lemma is_cons_not_nil (A : Type) (xs : list A) x xs' : xs = x :: xs' → xs ≠ [].
  Proof. by move ->. Qed.

  Hint Resolve is_cons_not_nil : solve_pure_add.
  Hint Immediate eq_sym : solve_pure_eq_add.

  Global Instance dequeue_spec γll γlv γm (l : loc) :
    SPEC vs, << is_queue_c γlv γll γm l vs >>
      dequeue #l 
    << (ov : val), RET ov; ⌜vs = []⌝ ∗ ⌜ov = NONEV⌝ ∗ is_queue_c γlv γll γm l []
                 ∨ ∃ v vs', ⌜vs = v :: vs'⌝ ∗ ⌜ov = SOMEV v⌝ ∗ is_queue_c γlv γll γm l vs' >>.
  Proof.
    iSteps --safe / as (_ l' Φ γlv' γll' γm' Hl ls vs i hd root Hnd Hi_hd Hls_vs) "IH Hq H●vs H●i H●ls Hl".
    clear l γll γlv γm.
     (* peek at head *)
    iRight. iSteps --safe /
     as (root ls' vs' j hd' Hvs_pref Hj_hd' Hj_last Hls_pref _ Hls_vs' _ _ _)
        "H◯vs H◯ls H◯j Hl' H●vs H●j H●ls Hq Hhd" /
     as (root ls' vs' j hd' Hvs_pref Hj_hd' Hij Hls_pref _ Hls_vs' _ _ hd_suc Hi_hd_suc' Hi_ls' hd_suc_val Hi_hd_suc_val _)
        "H◯vs H◯i H◯ls Hhd_suc Hhd Hl' H●vs H●j H●ls Hq".
    - (* queue was empty: commit and done*) 
      iSteps as (v' vs'' Hv' Hls') "H◯vs H◯ls H◯j" / as (v' vs'' Hv' Hls') "H◯vs H◯ls H◯j HΦ".
      all: contradict Hv'; rewrite drop_ge //; lia.
    - (* queue was not empty: abort, try to CAS *)iRight.
      iStep 16 (* --until goal-matches (fupd (∅) _ (WP _ {{ _ }}))%I. / *)
            as (root ls'' vs'' k Hnd_ls'' Hvs'_pref Hk Hjik Hls'_pref _ Hls_vs'')
                   "H◯vs H◯j H◯ls Hcl Hq H●vs H●k H●ls Hl" / as_anon;
          last iSmash. (* failing CAS is easy *)
      (* commit, we need to establish some pure facts first *)
      assert (i = k); last subst. (* not strictly needed to proceed, but this pure fact is needed for multiple subgoals *)
      { eapply NoDup_lookup. exact Hnd_ls''. prove_lookup. prove_lookup. }
      assert (drop k vs'' = hd_suc_val :: drop (S k) vs'') as Hdrop.
      { erewrite drop_S; last prove_lookup; done. }
      iSteps.
  Qed.

  Global Instance get_tail_spec γll γlv γm (l lq : loc) root ls' j :
    SPEC ls vs i, << own γll (◯ (to_max_prefix_list (root :: ls'))) ∗ ⌜(root :: ls') !! j = Some lq⌝ 
                   ¦ is_queue_c_all γlv γll γm l ls vs i >>
      get_tail #lq 
    << (lt : loc), RET #lt; ⌜lt = default root (last ls)⌝ ∗ is_queue_c_all γlv γll γm l ls vs i >>.
  Proof. iStep as (Φ). iLöb as "IH" forall (j lq ls'). iSmash. Qed.
    (* TODO: auto-löb doesnt generalize on j! *)

  Global Instance set_tail_spec γll γlv γm (l lq : loc) root (lt' : loc) ls' j :
    SPEC ls vs i v, << own γll (◯ (to_max_prefix_list (root :: ls'))) ∗ ⌜(root :: ls') !! j = Some lq⌝ 
                     ¦ is_queue_c_all γlv γll γm l ls vs i ∗ lt' ↦ v ∗ (lt' +ₗ 1) ↦ NONEV >>
      set_tail #lq #lt'
    << RET #(); is_queue_c_all γlv γll γm l (ls ++ [lt']) (vs ++ [v]) i >>.
  Proof.
    iSteps --safe / as_anon /
        as (_ Hlq lt'' lq' Φ γlv' γll' γm' l' ls1 root' Hlq' Hlöb ls2 vs2 i v tl Hi_tl Hnd_ls2 Hls1_pref _ Hls2_vs2 tl' _ _ Htl' _ _ _ _)
           "IH H◯vs H◯i H◯ls Hlt1 Hlt2 Hq H●vs H●i H●ls";
                    first iSmash.
    assert (tl' = tl); last (subst; clear Htl').
    { rewrite Htl' in Hi_tl. by simplify_eq. }
    (* iSmash deals with AU abort sidecondition *) iRight. (* abort, try to CAS *) iSteps as "H◯vs H◯i H◯ls HAU".
    (* Hints are not smart enough to see that the following is always true, so (default x16 (last x1) ∈ (x16 :: x1)) *)
    assert ((root' :: ls2) !! length ls2 = Some (default root' (last ls2))) as Hls2_last.
    { rewrite last_lookup. destruct ls2 as [|l0 ls2] => //=.
      assert (is_Some ((l0 :: ls2) !! length ls2)) as [w ->]; last done.
      apply lookup_lt_is_Some_2. simpl; lia. }
    iSteps --safe / 
      as (root' ls3 vs3 k v' tl' Hvs2_pref Hk_tl' Hik Hls2_pref _ Hls3_vs3 _ _ Hls2_ls3)
          "H◯ls Hlt1 Hlt2 H●vs H●k H●ls Hq Hlast" / as_anon;
                                                        last iSmash. (* failing CAS handled by iSmash *)
    iSteps as (Hvs2_pref Hnd_ls3) "Hlt H◯vs H◯i H◯ls".
    iPureIntro. rewrite !length_app //=. lia.
  Qed.

  Global Instance enqueue_spec γll γlv γm (l : loc) (v : val) :
    SPEC vs, << is_queue_c γll γlv γm l vs >>
      enqueue #l v
    << RET #(); is_queue_c γll γlv γm l (vs ++ [v]) >>.
  Proof.
    iSteps --safe / as (Φ l' ls1 vs1 i tl root Hnd_ls1 Hi_tl Hvs1_ls1) "Hl1 Hl2 Hq H●vs H●i H●ls".
    iRight. iSteps --safe / as_anon /
       as (root ls2 vs2 j tl' Hnd_ls2 Hvs1_pref Hj_tl' Hij Hls1_pref _ Hvs2_ls2 tl'' Hnd_ls2_app Hvs2_pref Hj_tl'' _ Hls2_pref _ Hlen)
          "H◯vs H◯k H◯ls Hq H●vs H●k H●ls";
                            first iSmash.
    iSteps as "H◯vs H◯k H◯ls".
    iPureIntro. apply eq_sym, drop_app_le.
    apply lookup_lt_Some in Hj_tl'. simpl in Hj_tl'. lia.
  Qed.
End queue_atomic.

















