From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From iris.algebra Require Import auth excl numbers.

From diaframe.heap_lang.examples.logatom Require Import queue_lib.


Definition new_queue : val := 
  λ: <>, let: "n" := AllocN #2 NONEV in AllocN #2 "n".

Definition dequeue : val :=
  rec: "dequeue" "q" :=
    let: "head" := ! "q" in
    let: "tail" := ! ("q" +ₗ #1) in
    let: "p" := NewProph in
    let: "next" := ! ("head" +ₗ #1) in

    let: "consistent":= "head" = ! "q" in
    resolve_proph: "p" to: "consistent";;
    if: "consistent" then
      if: "head" = "tail" then
        match: "next" with
          NONE => NONEV
        | SOME "l" => (* tail has fallen behind, help out *)
          CAS ("q" +ₗ #1) "tail" "l";;
          "dequeue" "q"
        end
      else
        match: "next" with
          NONE => #() #() (* error, should be unreachable *)
        | SOME "l" =>
          if: CAS "q" "head" "l" then 
            SOME ! "l"
          else 
            "dequeue" "q"
        end
    else "dequeue" "q".

Definition enqueue : val :=
  λ: "q" "v",
    let: "node" := AllocN #2 NONEV in
    let: "tailptr" := "q" +ₗ #1 in
    "node" <- "v" ;;
    (rec: "set_tail" <> :=
      let: "tail" := (! "tailptr") in
      let: "next" := ! ("tail" +ₗ #1) in
      if: ! "tailptr" = "tail" then
        match: "next" with
          NONE => 
            if: CAS ("tail" +ₗ #1) NONE (SOME "node") then 
              CAS "tailptr" "tail" "node";; #()
            else 
              "set_tail" #()
        | SOME "l" =>
            CAS "tailptr" "tail" "l";;
            "set_tail" #()
        end
      else "set_tail" #()
    ) #().

Definition queue_value_list_R : cmra := authR $ max_prefix_listUR $ valO.
Definition queue_head_index_R : cmra := authR $ max_natUR.
Class queueAltG Σ := QueueAltG { 
  #[local] queueG1 :: inG Σ queue_value_list_R ;
  #[local] queueG2 :: inG Σ queue_head_index_R;
  #[local] queueG3 :: inG Σ queue_base_R ;
}.
Definition queueAltΣ : gFunctors := 
  #[GFunctor queue_value_list_R; GFunctor queue_head_index_R; GFunctor queue_base_R].

Global Instance subG_queueAltΣ {Σ} : subG queueAltΣ Σ → queueAltG Σ.
Proof. solve_inG. Qed.


Section queue_atomic.
  Context `{!heapGS Σ, !queueAltG Σ}.
  Set Default Proof Using "Type*".

  Definition queue_head_inv γlv γll γih γit (l : loc) (ls : list loc) (vs : list val) (i : nat) : iProp Σ :=
    ∃ (l' : loc) (root : loc), 
      l ↦ #l' ∗ is_queue_base root ls vs ∗ own γlv (●{#1/2} to_max_prefix_list vs) ∗
        ⌜(root :: ls) !! i = Some l'⌝ ∗ own γih (●{#1/2} MaxNat i) ∗ own γit (◯ MaxNat i) ∗ own γll (● to_max_prefix_list (root :: ls)) ∗ ⌜length ls = length vs⌝.
        (* own γih (● MaxNat i) ∗ own γit (◯ MaxNat i) enforces that ● MaxNat i ≼ ● MaxNat j,
           which means that the tail pointer cannot lag further behind than the head pointer *)

  Definition is_queue γlv γih cvs : iProp Σ := ∃ (vs : list val) (i : nat), 
        own γlv (●{#1/2} to_max_prefix_list vs) ∗ own γih (●{#1/2} MaxNat i) ∗ ⌜cvs = drop i vs⌝.

  Definition queue_tail_inv (l : loc) γll γit : iProp Σ :=
    ∃ (l'' : loc) (ls : list loc) j, (l +ₗ 1) ↦ #l'' ∗ own γll (◯ to_max_prefix_list ls)
         ∗ ⌜ls !! j = Some l''⌝ ∗ own γit (● MaxNat j) ∗ ⌜NoDup ls⌝.

  Let queue_N := nroot.@"msc_queue".

  Definition is_queue_tail (l : loc) γll γit : iProp Σ := inv queue_N (queue_tail_inv l γll γit).
  Definition is_queue_head (l : loc) γlv γll γih γit := inv queue_N (∃ ls vs i, queue_head_inv γlv γll γih γit l ls vs i).
  Definition is_queue_inv (l : loc) γlv γll γih γit : iProp Σ := is_queue_head l γlv γll γih γit ∧ is_queue_tail l γll γit.

  (* lib.own_hints is not smart enough to figure these out, so we add them manually *)
  Instance max_nat_better γ n m :
    SolveSepSideCondition (n ≤ m) →
    HINT own γ (● MaxNat n) ✱ [- ; emp] ⊫ [bupd] ; own γ (● MaxNat m) ✱ [own γ (◯ MaxNat m)] | 15.
  Proof.
    rewrite /SolveSepSideCondition => Hnm; iSteps.
  Qed.

  Instance core_id_auth_auth_remember γ {A : ucmra} dq (a : A) `{!inG Σ' $ authUR A} :
    CoreId a →
    HINT own γ (●{dq} a) ✱ [-; emp] ⊫ [bupd]; own γ (●{dq} a) ✱ [own γ (◯ a)] | 20.
  Proof.
    move => Ha; iStep as "H●".
    iMod (own_update with "H●") as "H●".
    eapply auth_update_dfrac_alloc; last reflexivity. done.
    by iDestruct "H●" as "[$ $]".
  Qed.

  Global Instance new_queue_spec :
    SPEC {{ True }} 
      new_queue #() 
    {{ (l : loc) γll γlv γih γit, RET #l; 
      is_queue γlv γih [] ∗ is_queue_inv l γlv γll γih γit }}.
  Proof.
    iSteps as (root l) "Hroot Hl".
    iMod (own_alloc (● to_max_prefix_list ([]: list val))) as (?) "?"; first pure_solver.trySolvePure.
    iStep as "H●ls".
    do 2 (iMod (own_alloc (● MaxNat 0)) as (?) "?"; first pure_solver.trySolvePure).
    iSteps as "H●i Hl".
    iExists [],[], root. iSteps.
  Qed.

  Hint Extern 4 (_ ≤ _)%nat => done : solve_pure_add.

  Definition will_be_consistent (xs : list (val * val)) : bool :=
    match xs with (* this kind of construction should be put in a specific format, so that we can add automatic rewriting for it. *)
    | [] => true  (* something like prophesize_to_resolve_to (val) *)
    | (v1, v2) :: xs =>
      bool_decide (v2 = #true)
    end.

  Lemma is_cons_not_nil (A : Type) (xs : list A) x xs' : xs = x :: xs' → xs ≠ [].
  Proof. by move ->. Qed.

  Hint Resolve is_cons_not_nil : solve_pure_add.
  Hint Immediate eq_sym : solve_pure_eq_add.

  Global Instance dequeue_spec γll γlv γih γit (l : loc) :
    SPEC ⟨↑queue_N⟩ vs, << is_queue_inv l γlv γll γih γit ¦ is_queue γlv γih vs >>
      dequeue #l 
    << (ov : val), RET ov; ⌜vs = []⌝ ∗ ⌜ov = NONEV⌝ ∗ is_queue γlv γih []
                 ∨ ∃ v vs', ⌜vs = v :: vs'⌝ ∗ ⌜ov = SOMEV v⌝ ∗ is_queue γlv γih vs' >>.
  Proof.
    iStep 51 
      as (_ l' Φ γlv' γih' γll' γit' Hlöb ls1 vs1 hd1 root Hnd_ls1 ls2 Hls2_hd Hls1_vs1 tl1 ls3 i 
            Hls3_pref Hls1_pref Hi_tl1 Hls2_i Hnd_ls3 pvs p vs2 j hd2 root' Hvs1_pref Hj_hd2 Hvs2_j Hls3_pref' Hls2_vs2 _ _ _) 
          "Hqt Hqh IH H◯vs H◯ls H◯it H◯ih HAU Hp Hcl Hl' H●vs H●i H●ls Hq Htl" /
      as (_ l' Φ γlv' γih' γll' γit' Hlöb ls1 vs1 n hd1 root Hnd_ls1 Hn_hd Hls1_vs1 tl1 ls3 i 
            Hls3_pref Hls1_pref Hi_tl1 Hln_i Hnd_ls3 pvs p ls2 vs2 j hd2 root' Hvs1_pref Hj_hd2 
            Hn_j Hls3_pref' Hls2_vs2 _ _ nxt Hn_nxt Hn_ls2 v_nxt Hn_v_next Hnd_ls2)
          "Hqt Hqh IH H◯vs H◯it H◯ih H◯ls Hnxt1 Hhd1 HAU Hp Hcl Hl' H●vs H●i H●ls Hq".
    (* stop after succesful load of next *)
    - (* next is NONE: linearize if head is consistent with our earlier load *)
      destruct (will_be_consistent pvs) eqn:Hcons.
      * (* We need to prove: hd1 = tl1 for the if that occurs later *)
        assert (i = length ls2); last subst.
        { apply lookup_lt_Some in Hi_tl1. apply prefix_length in Hls3_pref'.
          rewrite longest_length in Hls3_pref'. simpl in Hls3_pref'.
          change (ofe_car locO) with loc in *. (* bloody hell *) lia. }
        assert (root = root'); last subst.
        { eapply prefix_cons_inv_1. by etrans. }
        assert (ls1 = ls2); last subst.
        { apply list_eq_from_prefix_length_ineq.
          + eapply prefix_cons_inv_2. etrans; done.
          + apply lookup_lt_Some in Hls2_hd. simpl in Hls2_hd. lia. }
        assert (hd1 = tl1); last subst.
        { enough (Some hd1 = Some tl1) by by simplify_eq. rewrite -Hls2_hd.
          prove_lookup. }
        (* force committing the linearization point *)
        iMod "HAU". iDecompose "HAU" as (_ _ _ _) "H●vs H●j HAUcl".
        iPoseProof (diaframe_close_right_quant with "HAUcl") as "HAUcl".
        (* kill second way of closing HAUcl. Keep equality as it shows up somewhere *)
        assert (drop j vs2 = []) as Hjvs2 by (rewrite drop_ge //; lia). rewrite Hjvs2.
        iStep 3 as (_ _ _) "H◯vs H◯jh H◯jt H◯ls HΦ".
        (* finish, making sure to use our prophecy that head will be consistent *)
        iStep 22 as (root nxt' pvs ls4 vs4 k Hnd_ls4 Hvs2_pref' Hk_nxt' Hjk Hls2_pref' _ Hls4_vs4)
                    "H◯vs H◯jh H◯it H◯ls Hp". simpl in Hcons.
        apply bool_decide_eq_true_1 in Hcons. rewrite Hcons.
        iSteps.
      * (* head will be inconsistent, do nothing and recurse *)
        iStep 23 as (root' nxt' pvs Hnd_ls2 ls4 vs4 k Hnd_ls4 Hvs2_pref' Hk_nxt' Hjk Hls2_pref' _ Hls4_vs4)
                    "H◯vs H◯jh H◯it H◯ls Hp".
        simpl in Hcons. apply bool_decide_eq_false_1 in Hcons.
        iRevert (Hcons). iStep as (Hcons_neq). rewrite Hcons_neq.
        iSteps.
    - (* next is SOME. skip case where head is inconsistent, then compare head with tail,
         inspect cases where a CAS was succesful *)
      iStep 47 
        as (root' pvs ls4 vs4 k Hnd_ls4 Hvs2_pref' Hk_nxt' Hjnk Hls2_pref' _ Hls4_vs4 nxt' 
            ls5 k' Hls5_pref Hls4_pref Hk_nxt Hjnk_l Hnd_ls5)
            "H◯vs H◯jh H◯it Htl1 H◯ls Hp Hcl H●it" /
        as (nxt' root' ps ls4 vs4 k Hnd_ls4 Hvs2_pref' Hk_nxt' Hjnk Hls2_pref' _ Hls4_vs4 Hnxt_neq_tl1
            ls5 vs5 k' Hnd_ls5 Hvs4_pref Hk_nxt'' Hjnk_l Hls4_pref _ Hls5_vs5)
            "H◯vs H◯jh Hnxt' H◯it H◯ls Hp Hcl Hq H●vs H●ih H●ls Hl'" /
        as_anon; last iSteps.
      * iStep 3 as "Hl'" / as_anon; last iSteps.
        (* head = tail, but tail pointer is lagging, since next is SOME. 
          The following equalities also hold, but are irrelevant:   k = n and n = i *)
        assert (k' = n); last subst.
        { eapply (NoDup_lookup (longest ls5 (root' :: ls4))). prove_nodup. prove_lookup. prove_lookup. }
        iStep. iExists (S n). iSteps.
        iPureIntro. assert ((root' :: ls4) !! S n = Some nxt); prove_lookup.
      * (* succesful dequeue of non empty queue. Open AU and commit *) 
        iMod "HAU"; iDecompose "HAU" as (_ _ _ _) "H●vs H●ih HAUcl".
        iPoseProof (diaframe_close_right_quant with "HAUcl") as "HAUcl".
        assert (k = n); last subst.
        { eapply NoDup_lookup. exact Hnd_ls5. prove_lookup. prove_lookup. }
        assert (k' = n); last subst.
        { eapply NoDup_lookup. exact Hnd_ls5. prove_lookup. prove_lookup. }
        iDecompose "Hcl" as "Hcl". (* forces priority of closing queue over closing AU *)
        erewrite drop_S; last prove_lookup. (* rewrites HAUcl to have an easier side condition *)
        iSteps. iPureIntro.
        enough (n ≠ i); first lia.
        contradict Hnxt_neq_tl1. subst.
        enough (Some nxt' = Some tl1) by by simplify_eq.
        rewrite -Hk_nxt''. prove_lookup.
  Qed.

  Global Instance enqueue_spec γll γlv γih γit (l : loc) (v : val) :
    SPEC ⟨↑queue_N⟩ vs, << is_queue_inv l γlv γll γih γit ¦ is_queue γlv γih vs >>
      enqueue #l v
    << RET #(); is_queue γlv γih (vs ++ [v]) >>.
  Proof.
    iStep 90 
      as (_ _ lq nd Φ v' γlv' γih' γll' γit' Hlöb ls1 ls2 tl1 Hls1_tl1 Hnd_ls1 
          vs2 i tl2 Hi_tl2 Hls1_pref Hls2_vs2 _ _ Hnd_ls2 ls3 j Hls3_pref Hls2_pref
          Hj_last_ls2 Hj_i Hnd_ls3)
          "Hqt Hqh IH H◯vs H◯it H◯ls H◯ih Hn1 Hn2 HAU" /
      as (_ _ lq nd Φ v' γlv' γih' γll' γit' Hlöb tl1 ls1 i Hi_tl1 Hnd_ls1 ls2 vs2 j 
          tl2 root Hj_tl2 Hls1_pref Hls2_vs2 _ _ tl3 Hj_tl3 Hj_ls2 v_tl3 Hj_v_tl3 
          Hnd_ls2 ls3 k Hls3_pref Hls2_pref Hk_tl1 Hij_k Hnd_ls3 ls4 m Hls4_pref Hls2_pref'
          Hn_tl1 Hij_n Hnd_ls4)
          "Hqt Hqh IH Htl3 Htl1 H◯vs H◯ih H◯ls H◯it Hn1 Hn2 HAU Hcl H●it Hlq" /
      as_anon; last iSteps.
    (* allocates the node, performs Löb induction. Two cases: just after succesful help of tail pointer swing, and just before trying to enqueue. *)
    - iStep 8 
        as (ls4 vs4 k tl3 root Hvs2_pref Hk_tl3 Hi_k Hls3_pref' Hls4_vs4 _ _ Hnd_ls4)
            "H◯ih H◯ls Hnd H●vs H●ih H●ls" /
        as_anon; last iSteps. (* if CAS fails, we recurse, this is easy *)
      iMod "HAU"; iDecompose "HAU" as (_ _ _ _) "H●vs H●ih HAUcl".
      (* suppose CAS succeeds: need to commit AU *)
      iPoseProof (diaframe_close_right with "HAUcl") as "HAUcl".
      iSteps as_anon / as_anon / (* failing swing is easy, iSteps stops for succesful swing *)
        as (_ _ _ ls5 n Hls5_pref Hls4_pref Hk_last Hki_n Hnd_ls5)
            "H◯it H◯vs H◯ih H◯ls HΦ H●it".
      { iPureIntro. rewrite !length_app /=. lia. }
      { iPureIntro. rewrite drop_app_le //. apply lookup_lt_Some in Hk_tl3. simpl in Hk_tl3. lia. }
      assert (ls1 = tl1 :: ls2); last subst.
      { apply list_eq_from_prefix_length_ineq; first done.
        apply lookup_lt_Some in Hls1_tl1. simpl. lia. }
      assert (length ls4 = length ls2) as Hls4_ls2.
      { eapply NoDup_lookup. exact Hnd_ls4. prove_lookup. prove_lookup. }
      assert (tl1 = root); last subst.
      { eapply prefix_cons_inv_1. etrans; done. }
      assert (ls4 = ls2); last subst.
      { apply eq_sym, list_eq_from_prefix_length_ineq; last lia.
        apply (prefix_cons_inv_2 root root). prove_prefix. }
      assert (n = length ls2); last subst.
      { eapply (NoDup_lookup (longest ls5 (root :: ls2 ++ [nd]))). 
        prove_nodup. prove_lookup. prove_lookup. }
      iExists (S (length ls2)). iSteps.
      iPureIntro.
      assert ((root :: ls2 ++ [nd]) !! S (length ls2) = Some nd); last prove_lookup.
      simpl. by apply list_lookup_middle.
    - (* succesfully helped swing tail pointer forward *)
      iStep as "".
      iExists (S i). iSteps; iPureIntro.
      * eapply (prefix_lookup_Some (root :: ls2)). prove_lookup. prove_prefix.
      * enough (m = i); first lia.
        eapply (NoDup_lookup (longest ls4 (longest ls3 (root :: ls2)))).
        prove_nodup. prove_lookup. prove_lookup.
  Qed.
End queue_atomic.

















