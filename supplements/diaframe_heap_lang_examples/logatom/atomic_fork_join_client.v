From diaframe.heap_lang Require Import proof_automation atomic_specs.
From diaframe.lib Require Import own_hints.
From iris.heap_lang Require Import proofmode notation atomic_heap.
From iris.algebra Require Import frac.
From diaframe.heap_lang.examples.logatom Require Import atomic_fork_join.

Import bi notation.

Local Obligation Tactic := program_smash_verify.

Definition forkjoinR := fracR.
Class forkjoinG Σ := ForkJoinG { #[local] spawn_tokG :: inG Σ forkjoinR }.
Definition forkjoinΣ : gFunctors := #[GFunctor forkjoinR].

Global Program Instance subG_forkjoinΣ {Σ} : subG forkjoinΣ Σ → forkjoinG Σ.

Section proof.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ, !forkjoinG Σ}.

  Definition client_thread : val :=
    λ: "l" "c", 
      "l" <- #1 ;;
      set "c" #().

  Definition client : val :=
    λ: "_", 
      let: "x" := ref #0 in
      let: "c" := make_join #() in
      Fork (client_thread "x" "c") ;;
      wait "c" ;;
      ! "x".

  Definition join_inv γ l1 l2 : iProp Σ :=
    ∃ ov, fork_join_state l2 ov ∗ ((⌜ov = None⌝ ∗ own γ (1/2)%Qp) ∨
                    ⌜ov = Some #()⌝ ∗ (own γ 1%Qp ∨ l1 ↦ #1 ∗ own γ (1/2)%Qp)).

  Global Program Instance thread_spec (l1 l2 : loc) :
    SPEC γ w, {{ inv nroot (join_inv γ l1 l2) ∗ l1 ↦ w }}
      client_thread #l1 #l2
    {{ RET #(); True}}.

  Global Program Instance client_spec :
    SPEC {{ True }} 
      client #() 
    {{ RET #1; True }}.
End proof.