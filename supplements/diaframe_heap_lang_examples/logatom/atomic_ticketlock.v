From iris.algebra Require Import coPset frac_auth agree.
From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.
From diaframe.lib Require Import ticket_cmra own_hints.
From diaframe.heap_lang.examples.logatom Require Import atomic_lock atomic_cas_counter.
From iris.heap_lang Require Import proofmode notation atomic_heap.

Import bi atomic_heap.notation.


Definition wait_equal `{!atomic_heap} : val :=
  rec: "wait_loop" "x" "l" :=
    let: "o" := ! "l" in
    if: "x" = "o" then 
      #() (* my turn *)
    else 
      "wait_loop" "x" "l".

Definition newlock `{!atomic_heap} : val :=
  λ: <>, ((* owner *) ref #0, (* next *) ref #0).

Definition acquire `{!atomic_heap} : val :=
  λ: "lk",
    let: "ticket" := incr (Snd "lk") in
    wait_equal "ticket" (Fst "lk").

Definition release `{!atomic_heap} : val :=
  λ: "lk", weak_incr (Fst "lk");; #().


Definition tlockR := frac_authR $ optionR $ agreeR $ natO.
Class tlockG Σ := {
  #[local] tlock_ticketG :: inG Σ ticketR ;
  #[local] tlock_tlockG :: inG Σ tlockR;
}.
Definition tlockΣ : gFunctors :=
  #[ GFunctor ticketR; GFunctor tlockR ].

Local Obligation Tactic := program_smash_verify.

Global Program Instance subG_tlockΣ {Σ} : subG tlockΣ Σ → tlockG Σ.

Section proof.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ, !tlockG Σ}.

  Program Instance wait_equal_spec (l : loc) (z : Z) :
    SPEC w, << ▷ l ↦ w >>
      wait_equal #z #l
    << RET #(); l ↦ #z ∗ ⌜w = #z⌝ >>.

  Definition ticket_inv (l : loc) γ : iProp Σ := ∃ (n : nat), l ↦ #n ∗ own γ (CoPset $ tickets_geq n).
  Definition issued (γ : gname) (x : nat) : iProp Σ := own γ (CoPset $ ticket x).
  Definition locked_at γ1 (n : nat) : iProp Σ := own γ1 (◯F (Some $ to_agree n)).
  Definition lock_state γ1 γ2 l1 l2 (n : nat) (b : bool) : iProp Σ :=
    l1 ↦ #n ∗ own γ1 (●F (Some $ to_agree n)) ∗ ticket_inv l2 γ2 ∗ (issued γ2 n ∗ ⌜b = true⌝ ∨ locked_at γ1 n ∗ ⌜b = false⌝).

  Global Program Instance newlock_spec :
    SPEC {{ True }}
      newlock #() 
    {{ (l1 l2 : loc) γ1 γ2, RET (#l1, #l2)%V; lock_state γ1 γ2 l1 l2 O false }}.

  Global Program Instance acquire_spec (l1 l2 : loc) γ1 γ2 :
    SPEC n b, << ▷ lock_state γ1 γ2 l1 l2 n b >>
      acquire (#l1, #l2)%V
    << RET #(); lock_state γ1 γ2 l1 l2 n true ∗ ⌜b = false⌝ ∗ locked_at γ1 n >>.

  Global Program Instance release_spec (l1 l2 : loc) γ1 γ2 (m : nat) :
    SPEC n b, << ▷ lock_state γ1 γ2 l1 l2 n b ∗ ▷ locked_at γ1 m >>
      release (#l1, #l2)%V
    << RET #(); lock_state γ1 γ2 l1 l2 (S n) false ∗ ⌜b = true⌝ ∗ ⌜n = m⌝ >>.
End proof.

Global Opaque lock_state.


(* Additionally, we show ticket_lock inhabits the atomic_lock interface. *)

Section ticket_lock_is_atomic_lock.
  Context  `{!atomic_heap, !heapGS Σ, !atomic_heapGS Σ, !tlockG Σ}.
  Import atomic_stateless_lock.

  Global Program Instance ticket_lock_atomic_stateless_lock : atomic_stateless_lock Σ :=
    {| newlock := atomic_ticketlock.newlock;
       acquire := atomic_ticketlock.acquire;
       release := atomic_ticketlock.release;
       name := gname * gname;
       raw_lock := loc * loc;
       lock_mask := ⊤;
       client_namespace := nroot;
       as_lock := (λ ls, (#ls.1, #ls.2)%V);
       lock_inv := (λ γs ls, ∃ n b, lock_state γs.1 γs.2 ls.1 ls.2 n b)%I;
       locked := (λ γs, ∃ n, own γs.1 (◯F{1/2 + 1/2/2} (Some $ to_agree n)))%I;
       locked_private := (λ γs, ∃ n, own γs.1 (◯F{1/2/2} (Some $ to_agree n)))%I;
    |}.
  Next Obligation.
    iSteps as (l1 l2 γ1 γ2) "H1". simpl.
    iExists (γ1, γ2). iSteps.
  Qed.
  Next Obligation.
    iSteps --safe / as_anon / as (Φ n) "Hlocked"; [ iRight | ]; iSteps.
    (* TODO: stops, since fractional arithmetic is very incomplete *)
    iDestruct "Hlocked" as "[Hγ1 [Hγ2 Hγ3]]".
    iCombine "Hγ1 Hγ2" as "Hγ". iSteps.
  Qed.
  Next Obligation.
    iSteps as (Φ n1 _ n2 b) "HΦ Hγ". replace (1/2 + 1/2/2 + 1/2/2)%Qp with 1%Qp; last first.
    { rewrite -assoc. rewrite !Qp.div_2 //. }
    iSteps --safe / as "Hlocked" / as_anon; last iSmash. (* TODO: stops, since fractional arithmetic is very incomplete *)
    iDestruct "Hlocked" as "[Hγ1 [Hγ2 Hγ3]]".
    iCombine "Hγ1 Hγ2" as "Hγ". iSmash.
  Qed.
End ticket_lock_is_atomic_lock.































