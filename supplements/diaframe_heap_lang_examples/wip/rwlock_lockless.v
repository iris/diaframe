From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.heap_lang Require Import proofmode.

From diaframe.lib Require Import frac_token.


Definition new_rwlock : val :=
  λ: "_", ref (SOME #0).


Definition acquire_reader : val :=
  rec: "acquire_rdr" "rwlk" :=
    match: ! "rwlk" with
      NONE => "acquire_rdr" "rwlk"
    | SOME "l" => if: CAS "rwlk" (SOME "l") (SOME ("l" + #1)) then #() else "acquire_rdr" "rwlk"
    end.

Definition release_reader : val :=
  rec: "release_rdr" "rwlk" :=
    match: ! "rwlk" with
      NONE => #() #() (* unsafe, but will never be executed *)
    | SOME "l" => if: CAS "rwlk" (SOME "l") (SOME ("l" + #-1)) then #() else "release_rdr" "rwlk"
    end.

Definition duplicate_reader : val :=
  rec: "dup_rdr" "rwlk" :=
    match: ! "rwlk" with
      NONE => #() #() (* unsafe, but will never be executed *)
    | SOME "l" => if: CAS "rwlk" (SOME "l") (SOME ("l" + #1)) then #() else "dup_rdr" "rwlk"
    end.


Definition acquire_writer : val :=
  rec: "acquire_wrtr" "rwlk"  :=
    if: CAS "rwlk" (SOME #0) NONEV then #() else "acquire_wrtr" "rwlk".

Definition release_writer : val :=
  λ: "rwlk",
    "rwlk" <- SOME #0.

Definition downgrade_writer : val :=
  λ: "rwlk",
    "rwlk" <- SOME #1.


Definition upgrade_reader : val :=
  λ: "rwlk",
    if: CAS "rwlk" (SOME #1) (NONE) then #() 
    (* can't loop on this: will cause deadlocks for readers competing for upgrade *)
    else
      release_reader "rwlk" ;;
      acquire_writer "rwlk".

Section proof.
  Context `{!heapGS Σ, !fracTokenG Σ}. 
  Context (P : Qp → iProp Σ).
  Context {HP : Fractional P}.

  Let N := nroot .@ "rw_lock".

  Obligation Tactic := program_smash_verify.

  Definition rwlock_inv γt (rw_loc : loc) : iProp Σ :=
    ∃ v, 
      rw_loc ↦ v ∗
        ((∃ (z : Z), ⌜v = InjRV #z⌝ ∗ 
            ((⌜0 < z⌝%Z ∗ token_counter P γt (Z.to_pos z)) ∨
            (⌜z = 0⌝%Z ∗ no_tokens P γt 1 ∗ P 1))
          ) ∨
        (⌜v = InjLV #()⌝ ∗ no_tokens P γt (1/2)%Qp)).

  Definition is_rwlock γt rwlk : iProp Σ :=
    ∃ (rw_loc : loc), 
      ⌜rwlk = #rw_loc⌝ ∗ inv N (rwlock_inv γt rw_loc).

  Program Instance new_rwlock_spec :
    SPEC {{ P 1 }} 
      new_rwlock #() 
    {{ (rwlk : val) γt, RET rwlk; is_rwlock γt rwlk }}.

  Global Program Instance acquire_reader_spec γt (rwlk : val) :
    SPEC {{ is_rwlock γt rwlk }} 
      acquire_reader rwlk 
    {{ RET #(); token P γt }}.

  Global Program Instance release_reader_spec γt (rwlk : val) :
    SPEC {{ is_rwlock γt rwlk ∗ token P γt }}
      release_reader rwlk 
    {{ RET #(); True }}.

  Program Instance duplicate_reader_spec γt (rwlk : val) :
    SPEC {{ is_rwlock γt rwlk ∗ token P γt }}
      duplicate_reader rwlk 
    {{ RET #(); token P γt ∗ token P γt }}.

  Global Program Instance acquire_writer_spec γt (rwlk : val) :
    SPEC {{ is_rwlock γt rwlk }}
      acquire_writer rwlk 
    {{ RET #(); P 1 ∗ no_tokens P γt (1/2)%Qp }}.

  Global Program Instance release_writer_spec γt (rwlk : val) :
    SPEC {{ is_rwlock γt rwlk ∗ P 1 ∗ no_tokens P γt (1/2)%Qp }}
      release_writer rwlk 
    {{ RET #(); True }}.

  Global Program Instance downgrade_writer_spec γt (rwlk : val) :
    SPEC {{ is_rwlock γt rwlk ∗ P 1 ∗ no_tokens P γt (1/2)%Qp }}
      downgrade_writer rwlk
    {{ RET #(); token P γt }}.

  Global Program Instance upgrade_reader_spec γt (rwlk : val) :
    SPEC {{ is_rwlock γt rwlk ∗ token P γt }}
      upgrade_reader rwlk 
    {{ RET #(); P 1 ∗ no_tokens P γt (1/2)%Qp }}.
End proof.


















