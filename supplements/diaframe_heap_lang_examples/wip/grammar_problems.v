From diaframe.heap_lang Require Import proof_automation.
From diaframe.lib Require Import own_hints.
From iris.heap_lang Require Import proofmode.
From iris.bi Require Import fractional.
From iris.algebra Require Import excl auth.

Section problems.
  Context `{!heapGS Σ}.

  Lemma exist_wand_true : ⊢ |==> (∃ v, WP (ref v) {{ λ v, ∃ (l : loc), ⌜v = #l⌝ ∗ l ↦ #42 }})%I.
  Proof.
    iExists #42; iSteps.
  Qed.

  Hint Mode defs.ReshapeExprAnd + ! - - ! : typeclass_instances.

  Lemma exist_wand_autom : ⊢ |==> (∃ (v : val), WP (ref v) {{ λ v, ∃ (l : loc), ⌜v = #l⌝ ∗ l ↦ #42 }})%I.
  Proof.
    iStepDebug.
    solveStep with empty_hyp_first.
    find_abd_initial_step.
    simple eapply abdhyp_from_unfold_goal.
    abd_step_goal_exist ltac:(idtac).
    eapply search_abd.abd_unfoldgoal_from_base.
    eapply weakestpre.abduct_from_execution. tc_solve. split.
    match goal with
    | |- ?el = ?K ?er =>
      idtac el; idtac K; idtac er;
      unify K (fill (Λ := heap_ectxi_lang) []); by simpl
    end.
    notypeclasses refine (defs.as_template_step).
    eapply specs.alloc_step_wp.
    all: tc_solve || simpl.
    Fail iStep. (* cannot step since there is an update on the RHS.. but even if that were gone, there would be a ▷ and a forall *)
    iExists _; iSteps.
  Qed.

  Context `{!Fractional (PROP := iPropI Σ) P}.
  Context `{!inG Σ $ authUR $ optionUR $ exclR $ leibnizO Qp}.

  Instance p_as_frac q : AsFractional (P q) P q.
  Proof. done. Qed.

  Definition co_frac_P q : iProp Σ := ∃ q', ⌜(q + q')%Qp = 1⌝%Qp ∗ P q'.

  Lemma strange_frac_lem_true (A1 A2 : iProp Σ) γ : 
    A1 ∗ A2 
      ∗ (A1 -∗ ∃ q, co_frac_P q ∗ own γ (◯ (Excl' q))) 
      ∗ (A2 -∗ ∃ q q', P q ∗ ⌜(q + q')%Qp = 1⌝%Qp ∗ own γ (● Excl' q)) 
    ⊢ P 1.
  Proof.
    iSteps.
    iSpecialize ("H4" with "H2"); iRevert "H4"; iStep.
    iSpecialize ("H3" with "H1"); iRevert "H3"; iStep.
    iCombine "H1 H2" as "HP". rewrite Qp_add_comm H1.
    iSteps.
  Qed.

  Instance frac_abd q :
    HINT P q ✱ [- ; (⌜q < 1⌝%Qp ∗ ∃ q', ⌜(q + q')%Qp = 1%Qp⌝ ∗ P q') ∨ ⌜q = 1⌝%Qp] ⊫ [id]; P 1 ✱ [True].
  Proof.
    iSteps.
    iCombine "H1 H2" as "H".
    rewrite H1. iSteps.
  Qed.

  Instance co_frac_abd q : HINT co_frac_P q ✱ [- ; P q] ⊫ [id]; P 1 ✱ [emp].
  Proof.
    iStep.
    iCombine "H1 H2" as "HP". rewrite Qp_add_comm H0.
    iSteps.
  Qed.

  Opaque co_frac_P.

  Lemma strange_frac_lem_true_biabd (A1 A2 R : iProp Σ) γ : 
    A1 ∗ A2 ∗ R
      ∗ (A1 -∗ ∃ q, co_frac_P q ∗ own γ (◯ (Excl' q))) 
      ∗ (A2 -∗ ∃ q q', P q ∗ ⌜(q + q')%Qp = 1⌝%Qp ∗ own γ (● Excl' q)) 
    ⊢ P 1 ∗ R.
  Proof. iStep. iRevert "H4". iStep. iStep. (* this is hopeless *) Admitted.

  Lemma strange_frac_lem_autom_abd (A1 A2 : iProp Σ) γ : 
    A1 ∗ A2 
      ∗ (A1 -∗ ∃ q, co_frac_P q ∗ own γ (◯ (Excl' q))) 
      ∗ (A2 -∗ ∃ q q', P q ∗ ⌜(q + q')%Qp = 1⌝%Qp ∗ own γ (● Excl' q)) 
    ⊢ P 1.
  Proof.
    iSteps. (* we are stuck now! *)
(* or are we..? *)
iLeft.
unshelve iStep. { eapply Qp_lt_sum. by eexists. }
iStep. 
iSpecialize ("H3" with "H1"). iReIntro "H3".
Transparent co_frac_P.
rewrite /co_frac_P. iReIntro "H3". 
enough (x0 = x1) as -> => //.
enough (Some x0 = Some x1) as H2; first by case: H2.
assert ((1 - x) = Some x0)%Qp as <-;
by apply Qp_sub_Some.
  Qed.

  Lemma strange_frac_lem_autom_biabd (A1 A2 R : iProp Σ) γ : 
    A1 ∗ A2 ∗ R
      ∗ (A1 -∗ ∃ q, co_frac_P q ∗ own γ (◯ (Excl' q))) 
      ∗ (A2 -∗ ∃ q q', P q ∗ ⌜(q + q')%Qp = 1⌝%Qp ∗ own γ (● Excl' q)) 
    ⊢ P 1 ∗ R.
  Proof.
    iSteps. (* we are stuck now, in a worse position! *)
  Admitted.

End problems.





















