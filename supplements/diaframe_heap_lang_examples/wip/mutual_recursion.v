From diaframe.heap_lang Require Import proof_automation wp_auto_lob.
From iris.proofmode Require Import coq_tactics reduction tactics.
From iris.heap_lang Require Import derived_laws notation proofmode.

Obligation Tactic := program_verify.

Section list_functions.
  Context `{!heapGS Σ}.

  Fixpoint is_list (l : val) (Ps : list (val → iProp Σ)) : iProp Σ :=
    match Ps with
    | nil => ⌜l = InjLV #()⌝
    | P :: Ps => ∃ (hd:loc) (v : val) l', ⌜l = InjRV #hd⌝ ∗ hd ↦ (v, l') ∗ P v ∗ is_list l' Ps
    end%I.

  Global Program Instance is_list_nil_abd Ps :
    HINT ε₀ ✱ [⌜Ps = []⌝] ⊫ [id]; is_list (InjLV #()) Ps ✱ [⌜Ps = []⌝] | 55.

  Global Instance match_node (nd : val) Ps e1 e2 :
    SPEC [is_list nd Ps]
     {{ True }}
       Case nd e1 e2
     {{ [▷^0] RET Case nd e1 e2; (⌜Ps = []⌝ ∨ (∃ P Ps', ⌜Ps = P :: Ps'⌝)) ∗ is_list nd Ps }} | 50.
  Proof. iSteps. destruct Ps; eauto. Qed.

  Inductive tree (A : Type) :=
      leaf : A → tree A
    | node : list (tree A) → tree A.
  Arguments leaf {_} _.
  Arguments node {_} _.

  Fixpoint tree_size {A : Type} (t : tree A) : nat :=
    match t with
    | leaf _ => 1
    | node ts =>
      list_sum (map (tree_size) ts)
    end.

  Fixpoint is_tree (l : val) (t : tree Z) : iProp Σ :=
    match t with
    | leaf a => ⌜l = InjLV $ #a⌝
    | node ts => ∃ (l' : val), ⌜l = InjRV l'⌝ ∗
      is_list l' (map (λ t v, is_tree v t) ts)
    end.

  Definition hl_tree_size : val :=
    rec: "size" "l" :=
      let: "lsizef" :=
        rec: "lsize" "ts" :=
          match: "ts" with
            NONE => #0
          | SOME "lh" =>
            let: "lv" := ! "lh" in
            "size" (Fst "lv") + "lsize" (Snd "lv")
          end
      in
      match: "l" with
        InjL "a" => #1
      | InjR "ts" =>
        "lsizef" "ts"
      end.

  Global Instance match_node_tree l t e1 e2:
    SPEC [is_tree l t]
     {{ True }}
       Case l e1 e2
     {{ [▷^0] RET Case l e1 e2; 
        ((∃ (a : Z), ⌜t = leaf a⌝) ∨ (∃ (ts : list $ tree Z), ⌜t = node ts⌝)) ∗ is_tree l t }} | 50.
  Proof. iSteps. destruct t as [a|ts]; eauto. Qed.

  Hint Extern 4 (@map ?A _ ?f ?l = []) =>
    cut (l = []); [ move => -> // | pure_solver.trySolvePure] : solve_pure_eq_add.

  Lemma tree_size_spec (l : val) (t : tree Z) :
    SPEC {{ is_tree l t }} hl_tree_size l {{ RET #(tree_size t); is_tree l t }}.
  Proof. 
    (* we are missing procedures for pure simplification, rest is automatic *)
    do 37 iStep.
    { apply map_eq_nil in H1. subst.
      iReIntro "H3".
      iSteps. }
    apply map_eq_cons in H1 as [vs [vss' [-> [Hvs Hvss']]]]. subst.
    iReIntro "H3".
    iSteps. repeat f_equal.
    rewrite Nat2Z.inj_add //.
  Qed.
End list_functions.
































