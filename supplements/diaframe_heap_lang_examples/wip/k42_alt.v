From iris.heap_lang Require Import proofmode.
From diaframe.heap_lang Require Import proof_automation loc_map.
From iris.algebra Require Import agree frac excl auth.
From diaframe.lib Require Import own_hints.

From diaframe.heap_lang.examples.logatom Require Import k42_lock.
Set Default Proof Using "Type".

Definition wait_for_succ' : val :=
  rec: "wait_for" "node" :=
    match: !("node" +ₗ #1) with
      NONE => "wait_for" "node" (* after reading SOME, we gain ownership of the location *)
    | SOME "l" => "l"
    end.

Definition acquire_with' : val :=
  λ: "lk" "node",
    ("node" +ₗ #1) <- NONE;;
    let: "pred" := enqueue_node "lk" "node" in (* this transfers ownership of "node" +ₗ 1 to "lk" *)
    match: "pred" with
      NONE => #() (* we obtained the lock, we dont own the right to change (lk + 1), 
                    but we know that if (lk + 1) ↦ SOME n then this will be a head_signal_loc  *)
    | SOME "l" => (* in this case, we have received of "l" +ₗ 1 ↦{1/2} NONEV from enqueue_node "lk" *)
      "node" <- #true ;;
      ("l" +ₗ #1) <- SOME "node";; (* this transfers ownership of "node" +ₗ 0 to "l" *)
      wait_on "node";; (* we are not done: change the lock so that release will not require the node explicitly *)
      (* here we do have the right to change (lk + 1), and the right to change what (lk + 1) ↦ SOME means *)
      ("lk" +ₗ #1) <- NONE ;;
      if: CAS "lk" (SOME "node") (SOME "lk") then
        #()
      else
        ("lk" +ₗ #1) <- SOME (wait_for_succ' "node")
    end.

Definition acquire' : val :=
  λ: "lk",
    let: "node" := new_node #() in
    acquire_with' "lk" "node" ;;
    Free "node" ;;
    Free ("node" +ₗ #1).

Definition release' : val :=
  λ: "lk",
    if: CAS "lk" (SOME "lk") NONE then
      #()
    else
      (wait_for_succ' "lk") <- #false.

Local Obligation Tactic := program_smash_verify.

Section spec.
  Context `{!heapGS Σ, k42_lockG Σ}.

  Let N := nroot .@ "k42lock".
  Definition N_signal := N .@ "signal".
  Definition N_node := N .@ "node".

  Program Instance wait_for_succ_spec γh γm γn γe (n : loc) R :
    SPEC [inv N_node (waiting_loc_inv γh γm γn γe n R)] {{ own γn (to_agree (n, true), (1/2))%Qp }}
      wait_for_succ' #n
    {{ (l : loc) γ, RET #l; (n +ₗ 1) ↦ SOMEV #l ∗ own γ (to_agree (l, false), (1/2)%Qp) ∗ loc_map_elem γm n (DfracOwn $ 1/2)%Qp (Some γn) ∗ inv N_signal (signal_loc γh γe l γ R) }}.

  Program Instance wait_for_succ_spec' γh γe (l : loc) R :
    SPEC [inv N_node (head_inv γh γe l R)] b p γ1, {{ ⌜b = true⌝ ∗ own γe (Excl ()) ∗ own γh (◯ (Excl' true, ε)) ∗ ⌜p = l ∧ γ1 = γh⌝ ∨ ⌜b = false⌝ ∗ own γh (◯ (Excl' false, Excl' (Some (p, γ1)))) }}
      wait_for_succ' #l
    {{ (n' : loc), RET #n'; 
        if b then ∃ γ, own γ (to_agree (n', false), 1/2)%Qp ∗ own γh (●{#1/2} (Excl' b, Excl' (Some (n', γ)))) ∗ own γh (◯ (Excl' true, ε)) ∗ inv N_signal (head_signal_loc γh n' γ R)
             else ⌜n' = p⌝ ∗ own γh (◯ (Excl' false, Excl' (Some (p, γ1)))) }}.

  Existing Instance enqueue_node_spec.
  Existing Instance wait_on_spec.
  Existing Instance wait_on_spec'.

  Global Program Instance acquire_with_spec R (lk : val) (n : loc) γh γm γe :
    SPEC {{ is_lock γh γm γe R lk ∗ free_node γm n }}
      acquire_with' lk #n
    {{ RET #(); R ∗ free_node γm n ∗ locked γh γe R }}.

  Global Program Instance acquire_spec R (lk : val) (n : loc) γh γm γe :
    SPEC {{ is_lock γh γm γe R lk }}
      acquire' lk
    {{ RET #(); R ∗ locked γh γe R }}.

  Global Program Instance release_spec R γh γm γe (lk : val) :
    SPEC {{ is_lock γh γm γe R lk ∗ R ∗ locked γh γe R }}
      release' lk
    {{ RET #(); emp }}.
End spec.




















