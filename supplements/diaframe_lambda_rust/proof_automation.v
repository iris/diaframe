From iris.proofmode Require Export base coq_tactics reduction tactics.
From diaframe Require Export proofmode_base.
From diaframe.symb_exec Require Export defs weakestpre spec_notation.
From diaframe.lib Require Import persistently intuitionistically iris_hints.
From diaframe.steps Require Export verify_tac.

From iris.bi Require Export bi telescopes derived_laws.
From iris.base_logic Require Export lib.invariants. 

From lrust.lang Require Export lang proofmode notation.
From lrust.lang Require Import new_delete lifting heap tactics.

Set Default Proof Using "Type".

Section instances.
  Context `{!lrustGS Σ}.

  Section execution.
    Open Scope expr_scope.

    Global Instance write_sc_step (l : loc) E1 E2 e v:
      IntoVal e v →
      SPEC ⟨E1, E2⟩ v', {{ ▷ l ↦ v' }}
        #l <-ˢᶜ e
      {{ RET #☠; l ↦ v}}.
    Proof.
      move => <- /=. (* without simpl atomicity breaks for some reason *)
      iSteps as (v') "Hl".
      iApply (wp_write_sc with "Hl").
      iSteps.
    Qed.

    Global Instance write_na_step (l : loc) E e v :
      IntoVal e v →
      SPEC ⟨E,E⟩ (v' : val), {{ ▷ l ↦ v' }} #l <- e {{ RET (#☠)%E; l ↦ v }}.
    Proof.
      move => <- /=.
      iSteps as (v') "Hl".
      iApply (wp_write_na with "Hl").
      iSteps.
    Qed.

    Global Instance read_sc_step (l : loc) E1 E2 :
      SPEC ⟨E1,E2⟩ (q : Qp)(v : val), 
        {{ ▷ l ↦{q} v }} 
          !ˢᶜ(# l) 
        {{ RET v; l ↦{q} v }}.
    Proof.
      iSteps as (q v) "Hl".
      iApply (wp_read_sc with "Hl").
      iSteps.
    Qed.

    Global Instance read_na_step (l : loc) E :
      SPEC ⟨E,E⟩ (q : Qp)(v : val), 
        {{ ▷ l ↦{q} v }} 
          !#l 
        {{ RET v; l ↦{q} v }}.
    Proof.
      iSteps as (q v) "Hl".
      iApply (wp_read_na with "Hl").
      iSteps.
    Qed.

    Global Opaque loc. (* loc is a pair underneath, but we don't want to see that *)
    Global Opaque heap_pointsto_vec.

    Global Instance alloc_step E1 E2 (n : Z) :
      SPEC ⟨E1,E2⟩ {{ ⌜0 < n⌝%Z }} 
        Alloc #n 
      {{ (l : loc)(sz : nat), RET #l; ⌜n = sz⌝ ∗ † l … sz ∗ l ↦∗ repeat (LitV ☠%V) sz }}.
    Proof.
      iSteps.
      iApply wp_alloc => //. iSteps.
    Qed.

    Close Scope expr_scope.

    Global Instance cas_step_int (l : loc) E1 E2 (vo : Z) er (vn : base_lit) :
      IntoVal er (LitV vn) →
      SPEC ⟨E1,E2⟩ (va : Z),
       {{ ▷ l ↦ #va }}
         CAS #l #vo er
       {{ (z : Z), RET #z;
            ⌜z = 1⌝ ∗ ⌜va = vo⌝ ∗ l ↦ #vn ∨ ⌜z = 0⌝ ∗ ⌜va ≠ vo⌝ ∗ l ↦ #va }}.
    Proof.
      move => <- /=.
      iSteps as (va) "Hl".
      destruct (decide (vo = va)) as [->|Hneq].
      - iApply (wp_cas_int_suc with "Hl").
        iSteps.
      - iApply (wp_cas_int_fail with "Hl") => //.
        iSteps.
    Qed.

    (* we should also add the specification for cas on locations.
      the difficulty there is that lit_eq is 'overloaded' -> any unallocated location is equal to another. This means that
       when CAS succeeds, we only learn that the two locations are equal IF they point to some value. not sure how that should best be encoded
    *)

  End execution.

  Section biabds.

    Global Instance mapsto_biabd l (z1 z2 : Z) q :
      HINT l ↦{q} #z1 ✱ [- ; ⌜z1 = z2⌝] ⊫ [id]; l ↦{q} #z2 ✱ [⌜z1 = z2⌝] | 51.
    Proof. iSteps. Qed.

  End biabds.

  Section purestuff.

    Global Instance pure_wp_step_exec_inst1 `(e : expr) b el φ n e' E s f xl erec :
      PureExec φ n e e' →
      TCIf (TCAnd (TCEq e (App b el)) (AsRec b f xl erec)) False $
        TCAnd (TCEq f BAnon) $
        TCAnd (TCEq xl []) $
        TCAnd (TCEq erec #LitPoison) $
        TCAnd (TCEq b #LitPoison)
              (TCEq el []) →
      ReductionStep (wp_red_cond, [tele_arg3 E; s]) e ⊣ ⟨id⟩ ⌜φ⌝ ; ε₀ =[▷^n]=> ⟨id⟩ e' ⊣ emp.
    Proof.
      intros Hφ _. rewrite empty_hyp_first_eq. apply pure_wp_step_exec => //.
      tc_solve.
    Qed.

    Fixpoint occurs_in (s : string) (body : expr) : bool :=
      match body with
      | Lit _ => false
      | Var s' => if decide (s = s') then true else false
      | Rec b xs e => if decide (BNamed s ≠ b ∧ BNamed s ∉ xs) then occurs_in s e else false
      | BinOp _ l r => (occurs_in s l) || (occurs_in s r)
      | App f xs => (occurs_in s f) || (fold_right orb false $ map (occurs_in s) xs)
      | Read _ e => occurs_in s e
      | Write _ l e => (occurs_in s l) || (occurs_in s e)
      | CAS l e1 e2 => (occurs_in s l) || (occurs_in s e1) || (occurs_in s e2)
      | Alloc e => (occurs_in s e)
      | Free e1 e2 => (occurs_in s e1) || (occurs_in s e2)
      | Case c ls => (occurs_in s c) || (fold_right orb false $ map (occurs_in s) ls)
      | Fork e => (occurs_in s e)
      end.

    Definition is_recursive (b : binder) (body : expr) :=
      match b with
      | BNamed s => occurs_in s body
      | BAnon => false
      end.

    Global Instance pure_wp_step_exec_inst_last (e : expr) el φ n e' E s f xl erec :
      PureExec φ n (App e el) e' →
      TCIf (AsRec e f xl erec) (SolveSepSideCondition (is_recursive f erec = false)) False →
      ReductionStep (wp_red_cond, [tele_arg3 E; s]) (App e el) ⊣ ⟨id⟩ ⌜φ⌝ ; ε₁ =[▷^n]=> ⟨id⟩ e' ⊣ emp.
    Proof.
      intros Hφ _. rewrite empty_hyp_last_eq. apply pure_wp_step_exec => //. tc_solve.
    Qed.

  End purestuff.

End instances.

Ltac find_reshape e K e' TC :=
  lazymatch e with
  | fill ?Kabs ?e_inner =>
    reshape_expr e_inner ltac:(fun K' e'' => 
      unify K (fill (K' ++ Kabs)); unify e' e''; 
      notypeclasses refine (ConstructReshape e (fill (K' ++ Kabs)) e'' _ (eq_refl) _); tc_solve )
  | _ =>
    reshape_expr e ltac:(fun K' e'' => 
      unify K (fill K'); unify e' e''; 
      notypeclasses refine (ConstructReshape e (fill K') e'' _ (eq_refl) _); tc_solve )
  end.

Global Hint Extern 4 (ReshapeExprAnd expr ?e ?K ?e' ?TC) => 
  find_reshape e K e' TC : typeclass_instances.

Global Hint Extern 4 (ReshapeExprAnd (language.expr ?L) ?e ?K ?e' ?TC) =>
  unify L lrust_lang;
  find_reshape e K e' TC : typeclass_instances.









