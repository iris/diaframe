From diaframe.lambda_rust Require Import proof_automation.

From lrust.lang.lib Require Import swap.


Lemma wp_swap `{!lrustGS Σ} E l1 l2 vl1 vl2 (n : Z):
  {{{ l1 ↦∗ vl1 ∗ l2 ↦∗ vl2 ∗ ⌜Z.of_nat (length vl1) = n⌝ ∗ ⌜Z.of_nat (length vl2) = n⌝ }}}
    swap [ #l1; #l2; #n] @ E
  {{{ RET #☠; l1 ↦∗ vl2 ∗ l2 ↦∗ vl1 }}}.
Proof.
  iInduction vl1 as [|v1 vl1] "IH" forall (n vl2 l1 l2).
  - iSteps. destruct vl2 => //=.
    rewrite !heap_pointsto_vec_nil.
    wp_lam.
    iSteps.
  - iStep as (Φ); destruct vl2; simplify_eq/=; try lia; iStep.
    rewrite !heap_pointsto_vec_cons. iStep.
    wp_lam.
    iSteps. iExists Φ. iSplitL; iSteps.
Qed.
