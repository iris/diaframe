From diaframe.lambda_rust Require Import proof_automation.
From diaframe.lib Require Import frac_token own_hints local_post.
From diaframe.symb_exec Require Import weakestpre_logatom atom_spec_notation.

From iris.algebra Require Import excl csum frac auth numbers.
From iris.bi Require Import fractional.
From lrust.lang.lib Require Import arc.

Set Default Proof Using "Type".

Definition arc_stR :=
  prodUR (optionUR (csumR (prodR positiveR (optionR (exclR unitO)))
                          (exclR unitO))) natUR.
Class arcG Σ := {
  #[local] RcG :: inG Σ (authR arc_stR);
  #[local] rc_fractokG :: fracTokenG Σ
}.
Definition arcΣ : gFunctors := #[GFunctor (authR arc_stR); fracTokenΣ].

Local Obligation Tactic := program_smash_verify.

Global Program Instance subG_arcΣ {Σ} : subG arcΣ Σ → arcG Σ.

Section specs.
  Context `{!lrustGS Σ, !arcG Σ} (P2 : iProp Σ) (N : namespace).
  Context (P1 : Qp → iProp Σ).
  Context {HP : Fractional P1}.

  Definition arc_inv_base' (γ γp : gname) (l : loc) (z1 z2 : Z) : iProp Σ :=
    (own γ (● (None, 0)) ∗ ⌜z1 = -1⌝%Z ∗ ⌜z2 = -2⌝%Z)%I ∨ (
       l ↦ #z1 ∗ (l +ₗ 1) ↦ #z2 ∗ (( ⌜0 < z1⌝%Z ∗
          token_counter P1 γp (Z.to_pos z1) ∗
          ((
              ⌜z2 = -1⌝%Z ∗ own γ (● (Some (Cinl (Z.to_pos z1, Excl' ())), 0))
           ) ∨ (
              ⌜0 < z2⌝%Z ∗ own γ (● (Some (Cinl (Z.to_pos z1, None)), Z.to_nat $ Z.pred z2))
           ))
    ) ∨ (
      ⌜z1 = 0⌝ ∗ ⌜0 < z2⌝%Z ∗ no_tokens P1 γp 1%Qp ∗ ((
          own γ (● (Some (Cinr (Excl ())), Z.to_nat $ Z.pred z2))
      ) ∨ (
          P2 ∗ own γ (● (None, Z.to_nat z2))
      ))
  )))%I.

  Definition arc_inv' (γ γp : gname) (l : loc) : iProp Σ :=
    own γ (● (None, 0)) ∨ (
    ∃ (z1 z2 : Z), l ↦ #z1 ∗ (l +ₗ 1) ↦ #z2 ∗ (( ⌜0 < z1⌝%Z ∗
          token_counter P1 γp (Z.to_pos z1) ∗
          ((
              ⌜z2 = -1⌝%Z ∗ own γ (● (Some (Cinl (Z.to_pos z1, Excl' ())), 0))
           ) ∨ (
              ⌜0 < z2⌝%Z ∗ own γ (● (Some (Cinl (Z.to_pos z1, None)), Z.to_nat $ Z.pred z2))
           ))
    ) ∨ (
      ⌜z1 = 0⌝ ∗ ⌜0 < z2⌝%Z ∗ no_tokens P1 γp 1%Qp ∗ ((
          own γ (● (Some (Cinr (Excl ())), Z.to_nat $ Z.pred z2))
      ) ∨ (
          P2 ∗ own γ (● (None, Z.to_nat z2))
      ))
  ))).

  Definition is_arc' (γ γp : gname) (l : loc) : iProp Σ :=
    inv N (arc_inv' γ γp l).

  Definition arc_tok γ γp : iProp Σ := own γ (◯ (Some (Cinl (1%positive, None)), 0)) ∗ token P1 γp.
  Definition weak_tok γ : iProp Σ := own γ (◯ (None, 1%nat)).

  Definition arc_tok_acc' (γ γp : gname) P E : iProp Σ :=
    (□ (P =c{⊤}={⊤,E}=∗ arc_tok γ γp ∗ (arc_tok γ γp ={E,⊤}=∗ ◇ P ∗ emp)))%I.

  Definition weak_tok_acc' γ P E : iProp Σ :=
    □ (P =c{⊤}={⊤, E}=∗ weak_tok γ ∗ (weak_tok γ ={E,⊤}=∗ ◇ P ∗ emp))%I.

  Typeclasses Transparent is_arc arc_tok weak_tok.

  Definition create_arc E l :
    l ↦ #1 -∗ (l +ₗ 1) ↦ #1 -∗ P1 1 ={E}=∗ ∃ γ γp, is_arc' γ γp l ∗ token P1 γp
    := smash_verify. (* these are abstraction breaking..? *)

  Definition create_weak E l :
    l ↦ #0 -∗ (l +ₗ 1) ↦ #1 -∗ P2 ={E}=∗ ∃ γ γp, is_arc' γ γp l ∗ weak_tok γ
    := smash_verify.


  (* this is ugly and currently required since atomic specs above do not expose the facts
     that the linearization point happens at a physical step, where we can strip a later.
     TODO: figure out how to combine later credits with Diaframe *)
  Instance token_counter_timeless P γ p : Timeless (token_counter P γ p).
  Proof. Admitted.
  Instance token_timeless P γ : Timeless (token P γ).
  Proof. Admitted.

  Instance p2_timeless : Timeless P2.
  Proof. Admitted.

  Instance fupd_intro_minus_empty_1 `{!BiFUpd PROP} E : IntroducableModality (fupd (PROP := PROP) (E ∖ ∅) E) | 50.
  Proof. rewrite difference_empty_L. tc_solve. Qed.

  Instance fupd_intro_minus_empty_2 `{!BiFUpd PROP} E : IntroducableModality (fupd (PROP := PROP) E (E ∖ ∅)) | 50.
  Proof. rewrite difference_empty_L. tc_solve. Qed.


  Program Instance strong_count_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << arc_tok γ γp ∗ arc_inv_base' γ γp l z1 z2 >>
      strong_count ([ #l])%E
    << RET #z1; arc_inv_base' γ γp l z1 z2 ∗ ⌜(0 < z1)%Z⌝ ∗ arc_tok γ γp >>.

  Global Instance strong_count_spec (γ γp : gname) (l : loc) (P : iProp Σ) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok_acc' γ γp P (↑N) ∗ P }} 
      strong_count ([ #l])%E
    {{ (z : Z), RET #z; P ∗ ⌜(0 < z)%Z⌝ }}.
  Proof. iSmash. Qed.

  Program Instance weak_count_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << arc_tok γ γp ∗ arc_inv_base' γ γp l z1 z2 >>
      weak_count ([ #l])%E
    << RET #z2; arc_inv_base' γ γp l z1 z2 ∗ ⌜(-1 ≤ z2)%Z⌝ ∗ arc_tok γ γp >>.

  Global Instance weak_count_spec (γ γp : gname) (l : loc) (P : iProp Σ) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok_acc' γ γp P (↑N) ∗ P }} 
      weak_count [ #l] %E
    {{ (c : Z), RET #c; P ∗ ⌜-1 ≤ c⌝%Z }}.
  Proof. iSmash. Qed.

  Instance clone_arc_phys_spec (l : loc) :
    SPEC (z : Z), << ▷ l ↦ #z >> clone_arc ([ #l])%E << RET #☠; l ↦ #(z + 1) >> | 100.
  Proof. iSteps. iLöb as "IH". wp_lam. iSteps. Qed.

  Program Instance clone_arc_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << arc_tok γ γp ∗ arc_inv_base' γ γp l z1 z2 >> 
      clone_arc ([ #l])%E
    << RET #☠; arc_inv_base' γ γp l (z1 + 1) z2 ∗ arc_tok γ γp ∗ arc_tok γ γp >> | 50.

  Global Instance clone_arc_spec (γ γp : gname) (l : loc) (P : iProp Σ) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok_acc' γ γp P (↑N) ∗ P }} 
      clone_arc ([ #l])%E
    {{ RET #☠; P ∗ arc_tok γ γp }}.
  Proof. iSmash. Qed.

  Instance downgrade_phys_spec (l : loc) :
    SPEC (z : Z), << ▷ (l +ₗ 1) ↦ #z >> downgrade [ #l]%E << RET #☠; (l +ₗ 1) ↦ #(z + 1) ∗ ⌜z ≠ -1⌝%Z >> | 100.
  Proof.
    iSteps. iLöb as "IH". wp_lam.
    iSteps. case_bool_decide; iSteps.
  Qed.

  Instance downgrade_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << arc_tok γ γp ∗ arc_inv_base' γ γp l z1 z2 >> 
      downgrade ([ #l])%E
    << RET #☠; arc_inv_base' γ γp l z1 (z2 + 1) ∗ arc_tok γ γp ∗ weak_tok γ >> | 50.
  Proof. iSmash. Qed.

  Instance downgrade_spec' (γ γp : gname) (l : loc) (P : iProp Σ) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok_acc' γ γp P (↑ N) ∗ P }} 
      downgrade ([ #l])%E
    {{ RET #☠; P ∗ weak_tok γ }}.
  Proof. iSmash. Qed.

  Instance clone_weak_phys_spec (l : loc) :
    SPEC (z : Z), << ▷ (l +ₗ 1) ↦ #z >> clone_weak ([ #l])%E << RET #☠; (l +ₗ 1) ↦ #(z + 1) >> | 100.
  Proof. iSteps. iLöb as "IH". wp_lam. iSteps. Qed.

  Instance clone_weak_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << weak_tok γ ∗ arc_inv_base' γ γp l z1 z2 >> 
      clone_weak ([ #l])%E
    << RET #☠; arc_inv_base' γ γp l z1 (z2 + 1) ∗ weak_tok γ ∗ weak_tok γ >> | 50.
  Proof. iSmash. Qed.

(*  Hint Extern 4 (_ ≤ _)%nat => eapply Nat.le_refl : solve_pure_add. *)

  Instance clone_weak_spec' (γ γp : gname) (l : loc) (P : iProp Σ) :
    SPEC {{ is_arc' γ γp l ∗ weak_tok_acc' γ P (↑N) ∗ P }} 
      clone_weak ([ #l])%E
    {{ RET #☠; P ∗ weak_tok γ }}.
  Proof. iSmash. Qed.

  Instance upgrade_phys_spec (l : loc) :
    SPEC (z : Z), << ▷ l ↦ #z >> 
      upgrade ([ #l])%E 
    << (b : bool), RET #b; 
        (⌜z = Z0⌝ ∗ l ↦ #0 ∗ ⌜b = false⌝) ∨ 
        (l ↦ #(z + 1) ∗ ⌜z ≠ Z0⌝ ∗ ⌜b = true⌝) >> | 100.
  Proof. iSteps. iLöb as "IH". wp_lam. iSteps. case_bool_decide; first lia. iSmash. Qed.

  Instance upgrade_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << weak_tok γ ∗ arc_inv_base' γ γp l z1 z2 >> 
      upgrade ([ #l])%E
    << (b : bool), RET #b; 
          ((⌜z1 = Z0⌝ ∗ arc_inv_base' γ γp l 0 z2 ∗ ⌜b = false⌝) ∨ 
          (⌜z1 ≠ Z0⌝ ∗ arc_inv_base' γ γp l (z1 + 1) z2 ∗ arc_tok γ γp ∗ ⌜b = true⌝)) ∗ 
           weak_tok γ >> | 50.
  Proof. iSmash. Qed.

  Instance upgrade_spec (γ γp : gname) (l : loc) (P : iProp Σ) :
    SPEC {{ is_arc' γ γp l ∗ weak_tok_acc' γ P (↑N) ∗ P }} 
      (upgrade [ #l])%E
    {{ (b : bool), RET #b; P ∗ if b then arc_tok γ γp else True }}.
  Proof. iSmash. Qed.

  Instance drop_weak_phys_spec (l : loc) :
    SPEC (z : Z), << ▷ (l +ₗ 1) ↦ #z >> 
      drop_weak ([ #l])%E 
    << (b : bool), RET #b; (l +ₗ 1) ↦ #(z - 1) ∗ (* TODO: IntoPure is too aggressive, prevents normal spec *)
        ((⌜z = 1%Z⌝ ∗ ⌜b = true⌝) ∨ (⌜z ≠ 1%Z⌝ ∗ ⌜b = false⌝ ∗ ε₀)) >> | 100.
  Proof. iSteps. iLöb as "IH". wp_lam. iSteps. case_bool_decide; first lia. iSteps. Qed.

  Instance drop_weak_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << weak_tok γ ∗ arc_inv_base' γ γp l z1 z2 >> 
      drop_weak ([ #l])%E
    << (b : bool), RET #b;
          ((⌜z2 = 1%Z⌝ ∗ arc_inv_base' γ γp l (-1) (-2) ∗ ⌜b = true⌝ ∗ P2 ∗ no_tokens P1 γp 1 ∗ l ↦ #0 ∗ (l +ₗ 1) ↦ #0 ) ∨ 
          (⌜z2 ≠ 1%Z⌝ ∗ arc_inv_base' γ γp l z1 (z2 - 1) ∗ ⌜b = false⌝)) >> | 50.
  Proof. iSmash. Qed.

  Instance drop_weak_spec (γ γp : gname) (l : loc) :
    SPEC {{ is_arc' γ γp l ∗ weak_tok γ }} 
      (drop_weak [ #l])%E
    {{ (b : bool), RET #b ; if b then P2 ∗ l ↦ #0 ∗ (l +ₗ 1) ↦ #0 else True }}.
  Proof. iSmash. Qed.

  Instance drop_arc_phys_spec (l : loc) :
    SPEC (z : Z), << ▷ l ↦ #z >> 
      drop_arc ([ #l])%E 
    << (b : bool), RET #b; l ↦ #(z - 1) ∗ (* TODO: IntoPure is too aggressive, prevents normal spec *)
        ((⌜z = 1%Z⌝ ∗ ⌜b = true⌝) ∨ (⌜z ≠ 1%Z⌝ ∗ ⌜b = false⌝ ∗ ε₀)) >> | 100.
  Proof. iSteps. iLöb as "IH". wp_lam. iSteps. case_bool_decide; first lia. iSteps. Qed.

  Instance drop_arc_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << arc_tok γ γp ∗ arc_inv_base' γ γp l z1 z2 >> 
      drop_arc ([ #l])%E
    << (b : bool), RET #b; arc_inv_base' γ γp l (z1 - 1) z2 ∗
          ((⌜z1 = 1%Z⌝ ∗ ⌜0 < z2⌝%Z ∗ ⌜b = true⌝ ∗ P1 1 ∗ own γ (◯ (Some (Cinr (Excl ())), 0))) ∨ 
          (⌜z1 ≠ 1%Z⌝ ∗ ⌜b = false⌝)) >> | 50.
  Proof.
    iSteps as (Φ z1 _ _ Hz1 _ _) "H◯" / as (Φ z1 z2 _ Hz2 _ Hz1 _ _) "H◯".
    all: replace (Z.to_pos z1 - (Z.to_pos (z1 - 1)))%positive with xH by lia; iSteps.
  Qed.

  Instance get_weak_tok γ γp l E n :
    SolveSepSideCondition (n = 0) → (* only relevant when n = 0; if > 0, we get it for free from CmraSubtract *)
    HINT own γ (◯ (Some (Cinr $ Excl ()), n)) ✱ [- ; is_arc' γ γp l ∗ P2 ∗ ⌜↑N ⊆ E⌝] ⊫
            [cfupd (↑N) E E]; 
         weak_tok γ ✱ [emp] | 60. (* after assumption! *)
  Proof.
    move => ->. iSteps. rewrite cfupd_eq.
    iInv N as "HN"; iDecompose "HN"; iSmash.
  Qed.

  Instance drop_arc_spec (γ γp : gname) (l : loc) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok γ γp }} 
      (drop_arc  [ #l])%E
    {{ (b : bool), RET #b ; if b then P1 1 ∗ (P2 ={⊤}=∗ weak_tok γ) else True }}.
  Proof. iSmash. Qed.

  Hint Extern 4 (?l = #(lit_of_bool ?b)) => 
    match l with
    | #(LitInt Z0) => by unify b false
    | #(LitInt (Zpos xH)) => by unify b true
    end : solve_pure_eq_add.

  Instance try_unwrap_aspec (γ γp : gname) (l : loc) :
    SPEC z1 z2, << arc_tok γ γp ∗ arc_inv_base' γ γp l z1 z2 >> 
      try_unwrap ([ #l])%E
    << (v : val) (b : bool), RET v; 
          ((⌜z1 = 1%Z⌝ ∗ ⌜0 < z2⌝%Z ∗ ⌜b = true⌝ ∗ arc_inv_base' γ γp l 0 z2 ∗ P1 1 ∗ own γ (◯ (Some (Cinr (Excl ())), 0))) ∨ 
          (⌜z1 ≠ 1%Z⌝ ∗ ⌜b = false⌝ ∗ arc_inv_base' γ γp l z1 z2 ∗ arc_tok γ γp)) ∗ ⌜v = #b⌝ >> | 50.
  Proof. iSteps. Qed.

  Instance try_unwrap_spec (γ γp : gname) (l : loc) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok γ γp }} 
      (try_unwrap [ #l])%E
    {{ (v : val) (b : bool), RET v; ⌜v = #b⌝ ∗ 
        if b then P1 1 ∗ (P2 ={⊤}=∗ weak_tok γ) else arc_tok γ γp }}.
  Proof. iSmash. Qed.

  (* is_unique consists of more than one atomic operations *)

  Instance is_unique_spec (γ γp : gname) (l : loc) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok γ γp }}
      (is_unique [ #l])%E
    {{ (b : bool), RET #b ;
        ⌜b = true⌝ ∗ l ↦ #1 ∗ (l +ₗ 1) ↦ #1 ∗ P1 1  ∨
        ⌜b = false⌝ ∗ arc_tok γ γp }}.
  Proof.
    iSteps --safest / as_anon /
      as (z Hz1 _ _ Hz2 _ _ _ _) "??????" / as_anon /
      as (z Hz1 _ _ Hz2 _ _ _ _) "??????" / as_anon;
      [iSteps | | iSteps | | iSteps].
    - iStep. replace (Z.to_pos z + 1 - Z.to_pos z)%positive with xH by lia. 
      iSteps --safest / as_anon / as (z' Hz'1 _ Hz'2 _) "???".
      * iSteps. all: apply inhabitant.
      * assert (z' = 1) as -> by lia. iSteps.
    - assert (z = 1) as -> by lia. iSteps.
    Unshelve. all: apply inhabitant.
  Qed.

  (* as does try_unwrap_full *)

  Instance try_unwrap_full_spec (γ γp : gname) (l : loc) :
    SPEC {{ is_arc' γ γp l ∗ arc_tok γ γp }} 
      (try_unwrap_full [ #l])%E
    {{ (v : val) (x : Z), RET v ; ⌜v = #x⌝ ∗ ⌜x < 3⌝%Z ∗ ⌜0 ≤ x⌝%Z ∗
        match (Z.to_nat x) : nat with
        (* No other reference : we get everything. *)
        | 0%nat => l ↦ #1 ∗ (l +ₗ 1) ↦ #1 ∗ P1 1
        (* No other strong, but there are weaks : we get P1,
           plus the option to get a weak if we pay P2. *)
        | 1%nat => P1 1 ∗ (P2 ={⊤}=∗ weak_tok γ)
        (* There are other strong : we get back what we gave at the first place. *)
        | _ (* 2 *) => arc_tok γ γp
        end }}.
  Proof.
    iSteps --safe / as_anon / as_anon / 
          as (z1 _ Hz1 _ _ _ _ _) "Ha Htok Hl2 Hcount Hl1"; [apply inhabitant.. | ].
    iRight. iStep as (_ _ HP1) "HP1 H◯". iSteps --safe / as (z2 Hz2 _ ) "Hl1 Hno_tok Hl2".
    destruct (decide (z2 = 1)) as [Heq|Hne'].
    * subst. iSteps.
    * iRight. iSteps. case_bool_decide; first done. iSteps.
  Qed.
End specs.
















