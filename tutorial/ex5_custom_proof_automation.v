(** Welcome to the Diaframe tutorial, part 5.

  In this part of the tutorial, we will build some proof automation from scratch.
  That is, we will import the Diaframe infrastructure with its hint definitions, 
  but without any preprovided hints. We will aim to build some minimal proof
  automation for proving weakest preconditions. We will provide our own
  _abduction hints_ to make this possible.
*)

(** This is the minimal import for a basic functional version of Diaframe. *)
From diaframe Require Import proofmode_base.

(** We will be proving weakest preconditions for the heap_lang language, so import that. *)
From iris.heap_lang Require Import notation proofmode.

(** Our goal is to automatically prove a spec to the following function *)
Definition allocate_zero : val :=
  λ: <>, ref #0.


Section allocate_zero_automation.
Context `{!heapGS Σ}.

(** Let us first look at the specification and its manual proof for the [allocate_zero] method. *)
Lemma allocate_zero_spec :
  {{{ True }}} allocate_zero #() {{{ (l : loc), RET #l; l ↦ #0 }}}.
Proof.
  iIntros (Φ) "_ HΦ". 
  (** This step just unfolds the definition of texan triples {{{ .. }}} .. {{{ ..}}} so poses no problem *)
  wp_lam. (** This requires work. It is a pure reduction, let us agree for now that they should always be performed. *)
  wp_alloc l as "Hl". (** This applies the specification of [ref], and also requires work. *)
  (** At this point, we need to apply "HΦ". The proof automation will do this out of the box, since it notices the similar
     terms in hypothesis and goal. In a manual proof this requires a bit more effort: *)
  iApply "HΦ". iExact "Hl".
Qed.


(** To kick off, let us add a rule for pure reduction. We leverage Iris's [PureExec] typeclass:
  [PureExec φ n e e'] can be used when we are looking for a pure reduction for some [e].
  [φ], [n], and [e'] are all outputs of this search: the pure reduction takes [n] steps,
  [e] reduces to [e'], and this is only true if you can prove [φ]. *)
Global Instance automate_pure_exec n e e' φ Φ:
  PureExec φ n e e' → (** The pure reduction provided by typeclass search *)
  HINT1 (** HINT1 stands for abduction hints*)
        ε₀ ✱ (** The hypothesis we look for in our context. ε₀ is always the first hypothesis, 
              so this abduction hint will always applied directly when a [PureExec] can be found. *)
        [⌜φ⌝ ∗ ▷^n (WP e' {{ Φ }})] ⊫ (** The new goal: prove [φ] and then continue with [▷^n (WP ...)] *)
        [id]; (** Modality under which this holds: the [id]entity modality, can be disregarded. *)
        WP e {{ Φ }}. (** Goal for which we want to provide proof automation. *)
Proof.
  iSteps. (** We can ofcourse bootstrap ourselves with a bit of known proof automation. *)
  (** The relevant rule is [lifting.wp_pure_step_later]. This can be found with [Search wp PureExec] *)
  iApply lifting.wp_pure_step_later.
  - done.
  - (** This is an easy goal. Note that we also get the later credits [£ n]. 
        We will discard and disregard these for now: you can use and mention these in 
        the definition of the abduction hint, but we will not do so in this file. *)
    iSteps.
Qed.

(** Note that we could have also written [▷^n (WP e' {{ Φ }}) ∗ ⌜φ⌝] as the sidecondition:
  the abduction hint would still be provable. HOWEVER, this is a bad choice here: [⌜φ⌝] is easy,
  while [WP .. ] is hard. By 'hard', we mean: its proof might feature unfolding to a [∀] or a [-∗].
  The rule of thumb is: put all the easy things on the left-hand side of the [∗],
  put the one hard thing on the right-hand side. If your goal features two hard goals, Diaframe might not
  be able to provide proof automation for your setting. An example where this occurs is in [wp_par]:
  there, one needs to prove two [WP]s, and Diaframe will not be able to guess how to distribute resources over these. *)

Lemma allocate_zero_spec_autom1 : (** Let us make sure above hint [automate_pure_exec] works, and determine our next goal. *)
  {{{ True }}} allocate_zero #() {{{ (l : loc), RET #l; l ↦ #0 }}}.
Proof.
  iSteps. (** Oops! [allocate_zero] is not unfolded. Let us check if this is the problem. *)
  unfold allocate_zero.
  iSteps. (** Alright, that works. So somehow the [PureExec] instance is not found for [allocate_zero]. *)
  (** Let us go back to our previous goal, and debug this. *)
  Undo 2.
  iStepDebug. (** This opens an intermediate goal format, not usually shown to the user. No hints have yet been sought. *)
  Set Typeclasses Debug. (** We want to investigate the typeclass search trace for the [automate_pure_exec] hint, so enable debugging. *)
  solveStep with ε₀. (** The [solveStep with H] tactic (where H is a name of a hypothesis, [ε₀] or [ε₁]) is used for debugging.
    It produces two new goals, and we are interested in the first one. It has shape
    [AbductModInput] and states that Diaframe is looking for an _abduction hint_ from the specified hypothesis to the previous goal. 
    The second goal will be whatever remains after applying the found abduction hint. Let us now launch typeclass search: *)
  Fail tc_solve. (** After some chatter, note that [automate_pure_exec] is found and applied. However,
    typeclass search gets stuck on a goal [AsRecV allocate_zero ...], and so no [PureExec] instance is found.
    If one investigates this further, one sees that during the [wp_rec]/[wp_lam] tactics, an additional typeclass instance
    [AsRecV_recv] is temporarily assumed. This typeclass is there to prevent eager unfolding of recursive functions.
    For this file, we will assume we will always want to do such eager unfolding. This means the [AsRecV_recv] typeclass instance
    is what is missing. So, we add it, and disable typeclass debugging. *)
  Global Existing Instance AsRecV_recv.
  Unset Typeclasses Debug.
  tc_solve. (** This now succeeds. After simplification, the remaining goal becomes: *)
  simpl. (** which is precisely the shape from our abduction hint [automate_pure_exec]. *)
  iSteps. (** Support for the later modality [▷] is included by default. We are left with [WP [ref #0]]. *)
  (** Let us now check we end up at the same goal after adding [AsRecV_recv]. *)
Abort.

Lemma allocate_zero_spec_autom2 : 
  {{{ True }}} allocate_zero #() {{{ (l : loc), RET #l; l ↦ #0 }}}.
Proof.
  iSteps. (** Indeed, pure reductions are now done automatically. Let us now add the symbolic execution rule for [ref]. *)
Abort.


Global Instance automate_sym_exec_ref (v : val) Φ :
  HINT1 ε₀ ✱ 
      [▷ (∀ (l : loc), l ↦ v -∗ Φ #l)] (** The new goal: obtain some newly allocated location [l],
        for which you learn that [l ↦ v]. Then prove the postcondition [Φ #l] *)
      ⊫ [id]; WP (ref v) {{ Φ }}.
Proof.
  iSteps. (** The relevant rule is [wp_alloc], found with [Search wp AllocN]. *)
  iApply wp_alloc. (** These two goals are both easy. Note that a [meta_token] is also referenced, we will not use it. *)
  - iSteps.
  - iSteps.
Qed.

(** Let us see this in action: *)
Lemma allocate_zero_spec_autom3 : 
  {{{ True }}} allocate_zero #() {{{ (l : loc), RET #l; l ↦ #0 }}}.
Proof. iSteps. Qed. (** It works! *)

(** An important caveat for the [automate_sym_exec_ref] hint is that it is not applicable when [ref] is part of some larger expression!
  I.e., if your goal is [WP (let: "l" := ref #0 in ...) {{ Φ }}], it cannot be used.
  The default proof automation for [WP] relies on some additional typeclass machinery to address this.
  We will now give an ad-hoc solution for this problem, that works only for let bindings: *)

Global Instance automate_let_in (e1 e2 : expr) (x : binder) (Φ : val → iProp Σ) :
  HINT1 ε₁ ✱ (** We use [ε₁] here as key hypothesis: only apply this rule if no other can be found. *)
    [WP e1 {{ v', ▷ WP (λ: x, e2)%V (of_val v') {{ Φ }} }} ] 
    (** New goal: the same expression, but after applying [wp_bind], and doing one additional pure reduction. *)
    ⊫ [id];
    WP (let: x := e1 in e2) {{ Φ }} (** Goal: an expression that features a let binding *).
Proof. iSteps as "HWP". wp_bind e1. iApply (wp_mono with "HWP"). iSteps. by wp_pure. Qed.
End allocate_zero_automation.


(** Let us now verify a slightly more interesting function. We will use our [allocate_zero] function,
  and use the allocated reference in the returned closure, which will just call [FAA] on the provided location. *)
Definition get_incrementer : val :=
  λ: <>,
    let: "l" := allocate_zero #() in
    (λ: <>, FAA "l" #1).


Section incrementer_automation.
Context `{!heapGS Σ}.

Definition incrementer_spec_type : Prop :=
  (** We will give this [get_incrementer] a somewhat tricky specification, to demonstrate a key feature of 
      abduction hints: they can can also be used when the key hypothesis occurs beneath other connectives. *)
  {{{ True }}}
    get_incrementer #()
  {{{ (closure : val) (l : loc), RET closure; l ↦ #0 ∗
      (** incrementer returns a closure, and an anonymous location [l] (since we dont get access to it,
        yet still learn that one must exist). The closure satisfies the following: *)
      □ (∀ z : Z, l ↦ #z -∗ WP closure #() {{ w, ⌜w = #z⌝ ∗ l ↦ #(z + 1) }} )
      (** I.e. as long as we know [l ↦ #z], we know calling the closure will increment the stored value by 1. *)
  }}}.
  

Lemma incrementer_spec_proof_try1 : incrementer_spec_type.
Proof. (** Let us try proving this: *)
  iStep 3. (** unfold and introduce texan triple *)
  iStep. (** pure reduction *)
  iStep 2. iStep. (**  [automate_let_in] kicks in *)
  iStep 3. (** purely reduces to [ref] *)
  iStep 5. (** symbolically executes [ref] *)
  iStep 6. (** more pure reductions *)
  Fail iStep. (** We are stuck now! We need to first register [wp_value]. *)
Abort.

Global Instance automate_wp_value (v : val) (Φ : val → iProp Σ) :
  HINT1 ε₀ ✱ [Φ v] ⊫ [id]; WP of_val v {{ Φ }}.
Proof. iSteps. by iApply wp_value. Qed.

Lemma incrementer_spec_proof_try2 : incrementer_spec_type.
Proof. (** Let us resume our proof after adding this abduction hint. *)
  iStep 21 as (Φ l) "HΦ Hl". (** this was our state before [automate_wp_value]. But now: *)
  iStep. (** We can proceed. At this point, "HΦ" is relevant and used: *)
  iStep. (** And this goal looks like "Hl" should be used. *)
  iStep. (** Now, we are stuck at a goal of shape [□ P]. Adding support for such goals requires
    adding transformer hints. In this file, we will just import the relevant file from the library:
    [lib/intuitionistically.v]. If you want to know more about transformer hints, please see that file. *)
Abort.

From diaframe.lib Require Import intuitionistically.

Lemma incrementer_spec_proof_try3 : incrementer_spec_type.
Proof.
  iStep 24 as (Φ l). (** our previous state. *)
  iStep. (** now with the □ introduced. *)
  iStep 2 as (z) "Hl". (** introduce forall, wands *)
  iStep 3. (** pure reductions *)
  Fail iStep. (** We are missing a hint for [WP FAA] here! 

    Exercise: provide an appropriate hint. Remember to put simple resources on the left-hand side of the [∗]

  For now, we will just use [iApply] to proceed manually. *)
  iApply (wp_faa with "Hl"). (** After using the [FAA] specification, the goal is easy *)
  iSteps.
Qed.

(** Proving [incrementer_spec_proof_try3] has not yet registered it as a usable specification.
  We will now prove an abduction hint that does so. *)
Global Instance incrementer_spec_proof_hint Φ : 
  HINT1 ε₀ ✱ [ ▷ (∀ (closure : val) ( l : loc),
    l ↦ #0 ∗ □ (∀ z : Z, l ↦ #z -∗ WP closure #() {{ w, ⌜w = #z⌝ ∗ l ↦ #(z + 1) }} )
    -∗ Φ closure)] 
    ⊫ [id];
    WP get_incrementer #() {{ Φ }}.
Proof. iStep. by iApply incrementer_spec_proof_try3. Qed.
End incrementer_automation.

(** Let us now try to use [get_incrementer] in the function below: *)
Definition increment_twice : val :=
  λ: <>,
    let: "i" := get_incrementer #() in
    "i" #();;
    "i" #();;
    "i" #().
(** The third call to ["i"] should return [2], since the incrementer was called twice before this. Let us prove that: *)

Section increment_twice_automation.
Context `{!heapGS Σ}.

Lemma increment_twice_spec_1 :
  {{{ True }}} increment_twice #() {{{ RET #2; True }}}.
Proof.
  iStep 3. (** Unfold texan triple *)
  iStep 3. (** pure reduction *)
  iStep. (** use let_in rule. *)
  iStep 6 as (closure l) "Hclosure Hl". (** apply the specification of [get_incrementer] *) 
  iStep 3. (** pure reduction *)
  iStep. (** sequencing [;;] is just notation for an anonymous let in binding *)
  Fail iStep. (** We are stuck now: note that "Hclosure" talks about [WP closure #()] - but the postconditions do not match!
    It seems obvious that we want to use something like [wp_mono], but this is not directly applicable.
    We can once again use an abduction hint to fix this. *)
Abort.

Global Instance automation_wp_mono Φ1 Φ2 e :
  HINT1 (WP e {{ Φ1 }}) ✱  (** <-- the hypothesis *)
    [∀ v, Φ1 v -∗ Φ2 v] (** <-- remaining goal *)
    ⊫ [id]; 
    WP e {{ Φ2 }}. (** <-- the goal *)
Proof. iSteps as "HWP HΦ". iApply (wp_strong_mono with "HWP"); try done. iSteps. Qed.

(** [automation_wp_mono] might look pretty useless, but let us now retry the verification of [increment_twice]. *)

Lemma increment_twice_spec_2 :
  {{{ True }}} increment_twice #() {{{ RET #2; True }}}.
Proof.
  iStep 17 as (Φ closure l) "Hclosure HΦ Hl". (** result at our previous goal. *)
  iStep. (** We can make progress now! Note that the wand and quantification from "Hclosure" have been turned into
      a [∗] and existential quantification in our remaining goal, automatically!
    We are now in shape to make progress once more *)
  iStep 4. (** pure reduction.. *)
  iStep 3. (** now do a let-in binding *)
  iStep. (** then use "Hclosure" with [automation_wp_mono] again *)
  iStep 5. (** and the rest is easy :) *)
  iSteps.
Qed.
End increment_twice_automation.


















