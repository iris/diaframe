(** Welcome to the Diaframe tutorial, part 4.

  In this example, we will prove a _logically atomic triple_.
  We will continue with the example from part 3 of the tutorial.
*)

From iris.heap_lang Require Import proofmode.

(** We will now also import [atomic_specs] and [wp_auto_lob].
   This will add proof automation for logically atomic triples, and a proof rule to
   perform automatic Löb induction.
*)
From diaframe.heap_lang Require Import proof_automation atomic_specs wp_auto_lob.


(** In part 3 of the tutorial, we saw the following [xchg_cas] function. *)
Definition xchg_cas : val :=
  (rec: "xchg" "l" "nv" := 
    let: "ov" := ! "l" in
    if: CAS "l" "ov" "nv" then
      "ov"
    else
      "xchg" "l" "nv").
(** We then proved that this function is contextually equivalent to the primitive Xchg function.
  This is a nice result, but we cannot use this fact when proving regular Hoare triples.
  A stronger result would be to prove a logically atomic triple for [xchg_cas],
  the same triple that the primitive [Xchg] method satisfies. This is what we'll do here.
*)

Section xchg_cas_logatom.
  Context `{!heapGS Σ}.

  Lemma xchg_cas_logatom_spec1 (z' : Z) (l : loc) :
    SPEC (z : Z), (** We give a SPECification to [xchg_cas].. *)
    (** a logically atomic specification, indicated by the << >> *)
      << l ↦ #z >> (** that at the linearization point, l ↦ #z is atomically changed to *)
        xchg_cas #l #z' 
      << RET #z; l ↦ #z' >>. (** l ↦ #z', where #z' is the argument to [xchg_cas], and the old value #z is returned *)
  Proof.
    (** Now let's see how the proof automation proceeds.*)
    iStep 2 as (Φ) "HAU". (** Unfolds the SPEC notation, introduces [Φ : val → iProp Σ] and an _atomic update_ *)
    iStep. (** Since our goal is a WP of a recursive function, proof automation deduces the need for Löb induction. *)
    (** [do_lob.do_löb] is an intermediate representation, that specifies Löb induction should be performed after 
        generalizing over the arguments and spatial hypotheses. *)
    iStep. (** Löb induction has been performed: reintroduce everything. *)
    iStep 7 as (z'' l' Φ') "IH HAU". 
      (** Goal now has shape [do_lob.post_löb arg]. If [arg] is a WP of a recursive function, 
                proof automation will start executing it. *)
    iStep 3. (** We are in a familiar setting once again: symbolic execution of a WP. *)
    iStep 5. (** To symbolically execute the load instruction, we need access to a [↦] connective. *)
    iStep 2 as (z) "H2 Hl'". (** This is obtained by using the access rule of atomic updates. *)
    (** Take note of the [|={∅,⊤]=>] update in front of the WP, and the hypothesis "H2". 
        "H2" is the 'closing resource', needed to restore the mask. Check that ["H2" true] corresponds to
        peeking at the AU, deferring the linearization point, while ["H2" false] corresponds to committing
        to the linearization point. *)
    (** Diaframe tries to be clever here, and thinks it should commit precisely when ⌜x2 = x5⌝. *)
    iStep --safe. (** But with the [--safe] option, Diaframe will not be daring enough to take that step.
       It thus asks the user to confirm making the choice based on the derived case distinction.
       The goal has shape [SolveOneFoc], notation for a disjunction. Since we do not want Diaframe to do that (yet): *)
    iRight. (** after which we can resume automation with [iStep]. *) iStep 2 as "HAU".
    iStep. (** notice that we still have access to the atomic update "H2" *)
    iStep 3. (** To symbolically execute the [CmpXchg] operation, we need a [↦] resource once again. *)
    iStep 3. (** The atomic update has been used to get the [↦], and we will now obtain the postcondition of [CmpXchg].
                     Note that there are two cases to consider: success and failure. *)
    iStep. (** We get two subgoals: *)
    - (** The [CmpXchg] succeeds, now we need to close the atomic update. *)
      (** We need to make the same choice as before: defer or commit.
        Now, Diaframe can see the appropriate way to continue by itself. *)
      iStep 2 as "HΦ'". (** Note that we now have ["HΦ'": True -∗ Φ' #z], which we need to prove the postcondition: *)
      iSteps.
    - (** The [CmpXchg] fails, and again we need to close the atomic update. *)
      iStep --safe. (** The linearization point should be deferred: it will be commited in a recursive call. *)
      iRight. iStep 2. (** Now after some pure reduction.. *)
      iStep 3. (** We see the recursive call will be executed. We will now use our induction hypothesis. *)
      iStep. (** The induction hypothesis has some requirements: mainly, we need to hand back the atomic update.
                This is a relatively simple goal, and can be proved with just: *)
      iSteps.
  Qed.

  (** Looking back at the proof of [xchg_cas_logatom_spec1]: it just consisted of calls to [iStep] and [iStepSafe], and a couple of
      choices for disjunctions with [iLeft]/[iRight]. This becomes more obvious if we use [iSteps], 
      which runs the automation until it finishes or gets stuck. *)
  Lemma xchg_cas_logatom_spec2 (z' : Z) (l : loc) :
    SPEC (z : Z), (** We give a SPECification to [xchg_cas].. *)
    (** a logically atomic specification, indicated by the << >> *)
      << l ↦ #z >> (** that at the linearization point, l ↦ #z is atomically changed to *)
        xchg_cas #l #z' 
      << RET #z; l ↦ #z' >>. (** l ↦ #z', where #z' is the argument to [xchg_cas], and the old value #z is returned *)
  Proof.
    iSteps --safe. iRight. iSteps --safe. (** defer linearization point at load. *)
    - iSteps. (** commit at succesful [CmpXchg] *)
    - iRight. iSteps. (** defer to recursive call at failing [CmpXchg] *)
  Qed.

  (** For proofs which follow above pattern, we can alternatively use the [iSmash] tactic. *)

  (**  We will now prove a slightly different, but equivalent specification of [xchg_cas], which turns out to be 
    friendlier in combination with relocs [lref lrel_int] typing. If [(ref lrel_int)%lrel l1 l2] holds,
    then [l1] and [l2] are locations, whose resources are governed by an invariant. The invariant states that
    the locations point to values [v1] and [v2], for which we have [lrel_int v1 v2] -- i.e. [v1] and [v2] are
    the same integers.

    The trouble is that from just the [↦] in the invariant, it is not apparent that the value is an integer.
    For Diaframe, this information becomes available 'too late' with above SPEC triple. But when stated as follows,
    this is no longer a problem. *)
  Global Instance xchg_cas_logatom_spec3 (z' : Z) (l : loc) :
    SPEC (v : val) (z : Z), (** We give a SPECification to [xchg_cas].. *)
    (** a logically atomic specification, indicated by the << >> *)
      << l ↦ v ∗ ⌜v = #z⌝ >> (** that at the linearization point, l ↦ #z is atomically changed to *)
        xchg_cas #l #z' 
      << RET #z; l ↦ #z' >>. (** l ↦ #z', where #z' is the argument to [xchg_cas], and the old value #z is returned *)
  Proof. iSmash. Qed.
End xchg_cas_logatom.


(** Logical atomicity is stronger than contextual refinement, and we will demonstrate this by proving
  a contextual refinement for [xchg_cas] using just the specification [xchg_cas_logatom_spec3]. To ensure
  we only use this specification, we make [xchg_cas] opaque for Coq. *)
Opaque xchg_cas.

(** Note that you need to have ReLoC installed to proceed! *)

(** We now import the proof automation for ReLoC. We need to import [symb_exec_logatom] to be able to use
  logically atomic triples for proving ReLoC's refinement judgment *)
From diaframe.reloc Require Import proof_automation symb_exec_logatom.

Section xchg_cas_refinement.
  Context `{!relocG Σ}.

  Lemma xchg_cas_refines vl1 vl2 va1 va2 : 
     (ref lrel_int)%lrel vl1 vl2 ∗ (lrel_int)%lrel va1 va2 ⊢ REL xchg_cas vl1 va1 << Xchg vl2 va2 : lrel_int.
  Proof.
    iStep. (** Note the invariant "H1", and see the discussion in lines 99-103 *)
    do 2 iStep. (** Applies the specification [xchg_cas_logatom_spec3]. We now need to prove a (slightly altered version of an)
               atomic update: we need to be able to access [x ↦ v] atomically and non-destructively, 
                and be able to swap it with the logically atomic postcondition [x ↦ #x1] *)
    iStep. (** Atomic updates are defined in terms of greatest fixpoints, briefly shown in this intermediate goal *)
    iStep. (** We now need to prove we can atomically access something.. *)
    iStep. (** which becomes a familiar [|={⊤, ?}=> ∃ v, l ↦ v ∗ ...] goal *)
    iStep. (** After accessing, we get two subgoals: one that the access was non-destructive, and one where we need to show
              that its okay to swap [x ↦ v] with the logically atomic postcondition [x ↦ #x1] *) 
    do 2 iStep.
    - (** The atomic access of [x ↦ v] was non destructive: *)
      do 2 iStep. (** Since our spatial context was empty upon needing to prove the atomic update, this just comes down 
                       to being able to restore the invariant when we receive [x ↦ #x5] back, which we can. *)
      iSteps.
    - (** And it is okay to swap: *)
      iStep. (** Get logically atomic postcondition *)
      do 2 iStep. (** Since the LHS is now a value, start symbolic execution of the RHS. Note that the invariant is (and stays) open. *)
      do 3 iStep. (** Now conclude the refinement and reinstate the invariant. *)
      iSteps.
  Qed.
End xchg_cas_refinement.













