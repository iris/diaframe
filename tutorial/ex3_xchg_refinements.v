(** Welcome to the Diaframe tutorial, part 3.

  In this example, we will prove a _contextual refinement_ as provided by ReLoC.
  You may want to return to part 1 or part 2 if you do not grasp pieces of this tutorial.
  Some familiarity with ReLoC's refinement judgments is also assumed.
*)

From iris.heap_lang Require Import proofmode.

(** We will now import the proof automation for ReLoC. This requires you to have
  ReLoc installed! *)
From diaframe.reloc Require Import proof_automation.


(** We will give multiple implementations of an [Xchg] function,
   which unconditionally eXCHanGes the stored value of a location,
   with the provided value.

   The first one implements [Xchg] with a CAS loop. *)

Definition xchg_cas : val :=
  (rec: "xchg" "l" "nv" := 
    let: "ov" := ! "l" in
    if: CAS "l" "ov" "nv" then
      "ov"
    else
      "xchg" "l" "nv").

(** [xchg_lam] is just a wrapper around the primitive [Xchg] expression *)
Definition xchg_lam : val :=
  λ: "l" "nv", Xchg "l" "nv".


Section xchg_refinements_1.
  Context `{!relocG Σ}.

  (** We will now show that '[xchg_cas] implements [Xchg]' or 'all behavior of [xchg_cas] is behavior of [Xchg]'.
      Specifically, when given passed:
      - references pointing to the same integer as the first argument
      - equal integers as second argument
      - then the two functions will return the same integer,
        as well as possibly change the reference in the same manner. *)
  Lemma xchg_cas_refines_xchg : 
     ⊢ REL xchg_cas << xchg_lam : ref lrel_int → lrel_int → lrel_int.
  Proof.
    (** Let us now see how Diaframe's proof automation operates on this goal. *)
    iStep. (** Both [xchg_cas] and [xchg_lam] are values, so we need to show these values are related at type [.. → .. → ..] *)
    iStep. (** Apply the rule for values related at an arrow type *)
    do 3 iStep. (** Introduce wands and foralls *)
    Fail iStep. 
    (** Diaframe does not continue, since both expressions are not values, and left is not reducible.
       This awkwardness rises from the fact that the next steps are β-reductions, which will
       reduce both sides to closures (which are values). First, we manually enforce β-reductions on the right hand side: *)
    iStepR. (** Performs a single pure reduction on the RHS *)
    do 3 iStepR. (** Prove all (trivial) sideconditions *)
    do 4 iStepR. (** Reduces the closure to a value, and prove sideconditions *)
    Fail iStep. (** This still does not reduce, because the LHS is a recursive function. 
      For this example, we will perform manual Löb induction. *)
    iIntros "!>". iLöb as "IH". rel_rec_l. 
    (** We use ReLoC's [rel_rec_l] tactic to β-reduce the LHS. 
            This only works after we remove the [|={⊤}=>] modality *)
    do 2 iStep. (** Diaframe resumes pure reductions, both expressions are values again. *)
    iStep. (** Apply the rule for values related at an arrow type. *)
    do 4 iStep. (** Now we start actual symbolic execution of the body of [xchg_cas] *)
    do 2 iStep. (** Some pure reduction *)
    iStep. (** Need a [↦] to symbolically execute the load. *)
    iStep. (** Open the invariant for the related locations *)
    iStep. (** Get back the [↦] after having executed load. *)
    do 4 iStep. (** Perform additional pure executions *)
    Fail iStep. (** The next thing to execute is a CAS, but note that the mask is not [⊤]!*)
    (** Additionally, the RHS is not a value. This means we need to choose whether to execute the RHS.
        In this case, the entire effect of the RHS [Xchg] happens when [CmpXchg] is succesful. So, we
        refrain from executing the RHS, and use the [iRestore] tactic to close the mask. *)
    iRestore. (** Our spatial context is empty once again *)
    iStep. (** To execute [CmpXchg], we need a [↦] *)
    iStep. (** Opens the invariant again *)
    do 3 iStep. (** Obtain postcondition of [CmpXchg]: *)
    - (** Either it succeeds, and the values were the same. We perform pure reductions on the LHS: *)
      do 4 iStep. (** Now the LHS is a value, which means Diaframe will start RHS execution. *)
      do 8 iStep. (** Executing [Xchg] works, and the proof is finished after closing the invariant *)
      iSteps.
    - (** Here, [CmpXchg] fails. After some pure reductions on the LHS: *)
      do 4 iStep. (** Here, note that our goal becomes similar (but not equal!) to our induction hypotheses "IH"*)
      (** Diaframe also notices this, and produces the following remaining goal: *)
      iStep. (** This states: close the invariant, and then show that the arguments are related *)
      iStep. (** This closes the invariant *)
      do 4 iStep. (** Now we need to show that the arguments are related, which they are *)
      iSteps.
  Qed.

  (** [xchg_cas] and [Xchg] are actually contextually _equivalent_, since we can also show this refinement in the other direction: *)
  Lemma xchg_refines_xchg_cas : 
     ⊢ REL xchg_lam << xchg_cas : ref lrel_int → lrel_int → lrel_int.
  Proof.
    iSteps. (** Stops because the RHS is not a value. Manually unfold it to proceed: *) 
    unfold xchg_cas. (** Now Diaframe can finish the proof automatically *) iSteps. 
  Qed.
End xchg_refinements_1.

(** We now use ReLoC's adequacy theorem to obtain a closed proof of contextual equivalence: *)
Theorem xchg_cas_xchg_equiv :
  ∅ ⊨ xchg_cas =ctx= xchg_lam : ref TNat → TNat → TNat.
Proof.
  split; eapply (refines_sound relocΣ); intros.
  - exact xchg_cas_refines_xchg.
  - exact xchg_refines_xchg_cas.
Qed.

Print Assumptions xchg_cas_xchg_equiv.
(** This shows two used axioms:
  - [refines_xchg_r], an Admitted lemma in the ReLoC source code. 
        Issue reported here https://gitlab.mpi-sws.org/iris/reloc/-/issues/11
        Proposed fix available here https://gitlab.mpi-sws.org/iris/reloc/-/merge_requests/6/diffs
  - Functional Extensionality, which is used throughout the Autosubst library, which is employed by ReLoC.

We advise you to use the following commands to inspect the [xchg_cas_xchg_equiv] statement: *)
About xchg_cas_xchg_equiv. (** If you disable Notations, you see that the [∅ ⊨ .. =ctx= ..] is notation for [ctx_equiv] *)
Print ctx_equiv. (** Which is defined in terms of [ctx_refines] *)
Print ctx_refines. (** Which is a relatively simple statement that only involves program steps, no separation logic! *)



(** As an exercise, you could investigate the behavior of the following proposed implementation of [Xchg]: *)

Definition xchg_faa : val :=
  λ: "l" "nz",
    let: "oz" := ! "l" in
    let: "dz" := "nz" - "oz" in
    FAA "l" "dz" ;;
    "oz".
(** This implementation is _bad_: a change between the load and the [FAA] will make [xchg_faa] not perform the
    desired exchange. However, [xchg_lam] does contextually refine [xchg_faa], as does [xchg_cas]. *)

(** A second (harder) exercise would be to establish that the following function logically refines [xchg_faa]: *)
Definition xchg_cas_proph : val :=
  (rec: "xchg" "l" "nv" := 
    let: "p" := NewProph in
    let: "ov" := ! "l" in
    if: Snd (Resolve (CmpXchg "l" "ov" "nv") "p" #()) then
      "ov"
    else
      "xchg" "l" "nv").
(** [xchg_cas_proph] is just [xchg_cas], but with prophecy variables to predict the success of the CAS.
  This is necessary for establishing the logical refinement directly: the RHS load needs to be executed at the same time
  as the LHS load, but only when the CAS will succeed later. 

  A hint for this exercise: you will need to use the following tactic:
  [destruct (match x with
        | ((_, #true)%V, _ ) :: xs => true
        | _ => false
        end) eqn:Heqn. ]
  where [x : list (val * val)] is the list of prophesied outcomes, to distinguish the cases where the CAS fails or succeeds
*)

