(** Welcome to the Diaframe tutorial, part 1!

  In this example, we will be verifying a modified version of the 'oneshot' example from the
  'Iris from the ground up' paper (%\url{https://doi.org/10.1017/S0956796818000151}% #<a href="https://doi.org/10.1017/S0956796818000151">link</a>#), section 2.
  Although the program itself is not very useful, it is relatively simple, 
  and can illustrate the core reasoning principles of Iris and the automation of Diaframe.

  Before we dive into the verification, we need to get access to Iris and Diaframe. *)

From iris.heap_lang Require Import proofmode.
From diaframe.heap_lang Require Import proof_automation.

(** The first imports give us access to Iris's heap_lang language, along with its notation and tactics.
  The second import loads the Diaframe tactics, as well as a collection of hints for the heap_lang language. 

  To verify this example, we will be needing some ghost resources. An in-depth explanation of 
  these is beyond the scope of this tutorial, more information can be found in the 
  %Iris documentation: \url{https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/resource_algebras.md}.%
  #<a href="https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/resource_algebras.md">Iris documentation.</a>#

  The next two lines give us access to the required resources, and load additional Diaframe hints concerning these.
*)

From iris.algebra Require Import agree csum excl.
From diaframe.lib Require Import own_hints.


(** We now define the methods of our example. 
  These are all written in heap_lang, a lambda-calculus with access to a heap.
  Relevant heap operations for this example are:
  - [ref v] allocates and returns a new location that initially maps to value [v].
  - [! l] performs a 'load': it returns the value [v] for which [l ↦ v].
  - [CAS l v1 v2] performs a Compare And Swap operation. 
      It checks whether currently [l ↦ v1], and only if so, writes [v2] to location [l].
      It returns a boolean indicating whether this swap happened.
*)

Definition mk_oneshot : val := 
  λ: "_", ref NONE.

(** [mk_oneshot] simply allocates a location which initially mapsto to [NONE], or nil.
  The idea of this example is to allow multiple threads to try to write [SOME] number to this location,
  but only one may succeed. By this agreement, any thread is allowed to read the location.
  Moreover, any thread that witnesses that this location now maps to [SOME] number [z], 
  knows that this location will forever map to this number [z].

  We define the following operations on such a 'oneshot' location:
 *)

Definition tryset : val :=
  λ: "c" "n", CAS "c" NONE (SOME "n") ;; #().

(** [tryset l z] will _try_ to _set_ oneshot-location [l] to the number [z]. 
  The method does not report on its success.

  Note that the binders of heap_lang are strings. This is done because heap_lang is a deeply embedded language.
  The notation [#()] stands for the unit value.
*)

Definition check : val :=
  λ: "c", 
    match: ! "c" with
      NONE => λ: "_", #()
    | SOME "n" => λ: "_", 
      match: ! "c" with
        NONE => #() #() (* unsafe *)
      | SOME "m" => 
          if: "m" = "n" then #() else #() #() 
      end
    end.

(** [check l] returns a closure [f]. This closure remembers the value once read from location [l].
   If the closure is called, it will compare the newly read value ["m"] to the previously read value ["n"].
   If these are unequal, the closure will (try to) execute [#() #()], which applies the unit value to the unit value.
   This would be unsafe. We aim to show that the closure returned by [check l] is, in fact, safe, 
    so the branches with [#() #()] are unreachable.

  What follows now are some incantations to modularly get access to our desired ghost state.
  We will discuss the properties of this ghost state further on.
*)

Definition oneshotR := csumR (exclR unitO) (agreeR ZO).
Class oneshotG Σ := OneShotG {
  #[local] oneshot_tokG :: inG Σ oneshotR
}.
Definition oneshotΣ : gFunctors := #[GFunctor oneshotR].

Global Instance subG_oneshotΣ {Σ} : subG oneshotΣ Σ → oneshotG Σ := verify.


Section verification.
  (** We are now ready to write down the desired invariants and specifications. 
    To enable modular verification, the specifications are parametrized over a [Σ : gFunctors].
    Think of this [Σ] as a kind of ghost-state register. To perform the verification, we need
    access to the ↦-connective, and to our desired 'oneshot' ghost state. To express that
    the ghost-state register [Σ] should contains these, we write: *)
  Context `{!heapGS Σ, !oneshotG Σ}.
  (** The idea is now that this verification works for all registers having at least these two kinds of ghost state. 

    We will need an invariant for this verification, which also needs a name.
  *)
  Let N := nroot.@"oneshot".

  (** We are now ready to write down the invariant: *)
  Definition oneshot_inv γ (l : loc) : iProp Σ :=
    ∃ (v : val), l ↦ v ∗ 
      (⌜v = NONEV⌝ ∗ own γ (Cinl $ Excl ()) ∨
       ∃ (z : Z), ⌜v = SOMEV #z⌝ ∗ own γ (Cinr $ to_agree z)).
  (** There are several things to note here. We could have written [oneshot_inv] as 
<<
    Definition oneshot_inv' γ (l : loc) : iProp Σ :=
      (l ↦ NONEV ∗ own γ (Cinl $ Excl ())) ∨
      (∃ (z : Z), l ↦ SOMEV #z ∗ own γ (Cinr $ to_agree z)).
>>
    We do not do this, since Diaframe's hint search cannot recurse into disjunctions.
    [l ↦ v] is taken out of the disjunction to ensure that Diaframe can find it when needed.
    Another thing to note is the order of the resources. By setting [l ↦ v] first,
    the physical value [v] can be used to guide the proof search, indicating which side 
    of the disjunction to try to prove.

    Another important part is, of course, the ghost state [own γ _]. The relevant rules of our ghost state are:
    - allocation: [⊢ |==> ∃ γ, own γ (Cinl $ Excl ())]
    - 'shooting': [own γ (Cinl $ Excl ()) ⊢ |==> own γ (Cinr $ to_agree n)]
    - shot-duplication: [own γ (Cinr $ to_agree n) ⊣⊢ own γ (Cinr $ to_agree n) ∗ own γ (Cinr $ to_agree n)]
    - shot-agreement: [own γ (Cinr $ to_agree n) ∗ own γ (Cinr $ to_agree m) ⊢ ⌜m = n⌝]
    By importing [own_hints], Diaframe is aware of all these rules, except the shooting rule.


    To present a clean interface for clients, we hide away the details of the invariant.
    We do this by defining an [is_oneshot] predicate, as below, and use this in all our specifications.
  *)

  Definition is_oneshot γ (v : val) : iProp Σ := ∃ (l : loc), ⌜v = #l⌝ ∗ inv N (oneshot_inv γ l).

  (** Alright, let's do our first verification!
    The following lemma is our desired specification of [mk_oneshot]: no preconditions,
    and the returned value [is_oneshot].
    This is a 'regular' Iris Texan Triple -- mostly equivalent to a standard hoare triple, but
    easier to use in interactive proofs. 
    [mk_oneshot_spec_manual] shows how one could verify this in regular Iris.
  *)
  Lemma mk_oneshot_spec_manual :
    {{{ True }}} mk_oneshot #() {{{ γ v, RET v; is_oneshot γ v }}}.
  Proof.
    iStartProof. (** This shows what a Texan Triple unfolds to. *)
    iIntros (Φ) "_ HΦ".
    wp_lam.
    wp_alloc l as "Hl".
    iMod (own_alloc _) as (γ) "Hγ"; last first.
    - iApply "HΦ".
      iExists l. iSplitR => //.
      iApply inv_alloc.
      iExists NONEV. eauto with iFrame.
    - done.
  Qed.

  (** We will now tackle the same goal, but using Diaframe. *)
  Lemma mk_oneshot_spec_1 :
    {{{ True }}} mk_oneshot #() {{{ γ v, RET v; is_oneshot γ v }}}.
  Proof.
  (** To prove our specification, we first enter the Iris Proof Mode. *)
    iStartProof.
  (** Diaframe's most relevant tactic is [iStep]. It performs a single _chunk of steps_ of the
      automation described in the paper. Let's see what happens.
      We will explain the actions of [iStep] after each call, as well as a comparable IPM command.
     *)
    iStep as (Φ). (** Introduce the predicate [Φ].                 [iIntros (Φ)] *)
    iStep. (** Introduce the trivial hypothesis [True]             [iIntros "_"] *)
    iStep as "HΦ". (** Introduce the hypothesis on [Φ]             [iIntros "HΦ"] *)
    iStep. (** Use pure reduction to unfold [mk_oneshot]           [wp_lam], roughly *)
    iStep as "HΦ". (** Introduce the later modality, strip it off ["HΦ"] *)
    iStep. (** pure_reduction: [InjL] → [InjLV]                    [wp_pures], roughly *)
    iStep. (** Introduce the later modality                        ... *)
    iStep. (** Apply the specification of [ref]                    [wp_alloc x as "?"], roughly *)
    iStep. (** Prove the precondition of [ref]: trivial            ... *)
    iStep. (** Introduce the newly allocated location [x0]         ... *)
    iStep. (** Introduce the later modality                        ... *)
    iStep. (** Acquire resource [_ : x ↦ InjLV #()]             [iIntros "?"] *)
    iStep. (** Apply the wp-value rule                             ... *)
    iStep. (** This does not correspond to a single IPM tactic. 
          Diaframe finds a hint from "HΦ" to our goal, which notices that the [γ] argument to "HΦ" need
          not be instantiated right away. The resulting goal thus existentially quantifies over [γ], which will be crucial later.
            To reach this goal with regular IPM tactics, one can use
          [iAssert (|={⊤}=> ∃ γ, is_oneshot γ #x0)%I with "[$]" as ">[%γ Hγ]"; last by iApply "HΦ".] *)
    unfold is_oneshot. (** <-  This line can be omitted, is just to demonstrate what is going on more clearly *)
    iStep. (** Instantiates _only_ the second existentials with [x]. This, again, does not correspond to a single IPM tactic *)
    iStep. (** Applies the invariant allocation rule, but beneath the binder [x0]. We are now asked to show the invariant resource holds,
              after which we must show that the invariant is enough to prove (the trivial goal) emp. *)
    assert_succeeds (solve [do 3 iStep]). (** [iStep] can solve this, but let us take a closer look at what happens *)
    iStepDebug. (** nothing has happened yet, but note the ‖ bars that indicate we are looking at internal Diaframe goals. *)
    solveStep.  (** nothing much happens, the [∗] gets put between parentheses to [(∗)], 
                meaning Diaframe now knows it is proving a separating conjunction  *)
    solveStep. (** Diaframe looks beneath [oneshot_inv] to find an existentially quantified statement, which it will now prove *)
    solveStep. (** Under the existentially quantified statement is a separating conjunction, whose left-hand side will first be proven *)
    solveStep. (** [_ : x ↦ InjLV #()] can prove this *)
    do 6 solveStep. (** After some simplifications, we now need to prove a disjunction. *)
    solveStep. (** This disjunction will first have its pure components simplified. *)
    solveStep. (** Now, if the left-hand side can be proven completely, Diaframe assumes this will be the correct side. *)
    solveSteps. (** Diaframe will notice it can use the own allocation rule, and prove the left-hand side completely. *)
    iSteps. (** Solves the remaining goal. *)
  Qed.

  (** The repetition of [iStep] in the proof above is not required or recommended, but illustrates the individual steps taken by Diaframe.
    You can let Diaframe take more steps by using the [iSteps] command. 

    Texan Triples as above are nice, but such specifications are not automatically available to Diaframe. To make specifications
    available to Diaframe, write them as SPEC triples. For example: *)

  Lemma mk_oneshot_spec_2 :
    SPEC {{ True }} mk_oneshot #() {{ γ (v : val), RET v; is_oneshot γ v }}.
  Proof.
    iSteps. (** Runs [iStep] until it gets stuck. Note that this also performs [iStartProof] if necessary. *)
  Qed.


  (** Since the proofs become just a single tactic, we are in shape to generate proofs automatically with [Program].
    Diaframe provides a tweaked version of both [iSmash] and [iSteps] for automatic proof generation with [Program].
    Enable this as follows: *)
  Local Obligation Tactic := program_verify.

  Global Program Instance mk_oneshot_spec_3 :
    SPEC {{ True }} mk_oneshot #() {{ γ (v : val), RET v; is_oneshot γ v }}.

  (* We want to verify [tryset] next. We need the following lemma for ghost state to proceed.
    This is the 'shooting' ghost rule mentioned above. The proof need not be understood. *)
  Lemma shoot_update γ n : 
    own γ (Cinl $ Excl ()) ⊢ |==> own γ (Cinr $ to_agree n) ∗ own γ (Cinr $ to_agree n).
  Proof. rewrite -own_op -Cinr_op agree_idemp. by apply own_update, cmra_update_exclusive. Qed.

  (** We will now verify that tryset is _safe_. The specification is simply: *)
  Lemma tryset_spec_1 γ (v : val) (z : Z) :
    SPEC {{ is_oneshot γ v }} tryset v #z {{ RET #(); True }}.
  Proof.  (** Let's run the automation and see what happens. *)
    iSteps as (l) "HN Hγ". (** We get stuck! Diaframe does not know how to proceed.
      We ended up here as follows. To perform the CAS, we needed access to [l ↦ v]. 
      Diaframe opened the invariant accordingly. Now consider the case where the CAS succeeds (the other case poses no problem).
      We need to restore the invariant [oneshot_inv]. Diaframe sees that the left-hand side of the disjunction is contradictory,
      so tries to prove the right-hand side. It gets stuck at our current goal, where we need to perform a ghost-update to proceed.

      What now? One approach is to return to a regular IPM goal, and continue with a manual proof.
     *)
    unseal_diaframe => /=. (** Now we are back to a regular Iris goal, and finish using [shoot_update]. *)
    iSplitL.
    - iMod (shoot_update with "Hγ") as "[$ #?]". done.
    - iSteps. (** This goal poses no problems for Diaframe. *)
  Qed.

  (** Instead of reverting to a manual proof, one can also register a hint for Diaframe.
      In this case, this can be done as follows. *)
  Instance shoot_hint γ z :
    HINT own γ (Cinl $ Excl ()) ✱ [- ; emp] ⊫ [bupd]; own γ (Cinr $ to_agree z) ✱ [own γ (Cinr $ to_agree z)].
  Proof. iStep. iApply shoot_update. iSteps. Qed.
  (** Note that we use [bupd] as the modality, instead of [fupd _ _] as in the paper.
    Diaframe uses the fact that [|==> P ⊢ |={E,E}=> P] for all [P] and [E] to apply such hints. *)

  (** Now try running the Diaframe automation again for the verification of [tryset]! *)
  Global Instance tryset_spec_2 γ (v : val) (z : Z) :
    SPEC {{ is_oneshot γ v }} tryset v #z {{ RET #(); True }}.
  Proof. Admitted.

  (** Again, try running the Diaframe automation for the verification of [check].
    An interesting part of [check] is that it returns a closure,
    and the specification of this closure is part of the postcondition of [check]: 
    the [□ WP f #() {{ w, ⌜w = #()⌝] part. Prefixing this with [□], means that the specification keeps holding,
    i.e. that we can execute the closure more than once. *)
  Global Instance check_spec γ (v : val) :
    SPEC {{ is_oneshot γ v }} check v {{ (f : val), RET f; □ WP f #() {{ w, ⌜w = #()⌝ }}}}.
  Proof. Admitted.

  (* We need the following once we make is_oneshot Opaque *)
  Global Program Instance oneshot_persistent γ v : Persistent (is_oneshot γ v).
End verification.

(** This is to prevent anyone (including Diaframe) to look inside the following definitions.
   That should no longer be necessary: the specifications are all one should need. *)
Global Opaque is_oneshot mk_oneshot tryset check.


(** We have verified our oneshot library -- let's use it to verify some clients! 
  These clients do not do anything fancy, but they make use of the following facts:
  - [is_oneshot _ _] can be shared between threads
  - closures returned by [check] can be used multiple times, and concurrently
*)
Definition client_thread : val :=
  λ: "c" "n", 
    let: "f_pre" := check "c" in
    Fork ("f_pre" #());;
    tryset "c" "n";;
    let: "f_post" := check "c" in
    Fork ("f_post" #());;
    "f_post" #() ;;
    "f_pre" #().

Definition oneshot_client : val :=
  λ: <>,
    let: "c" := mk_oneshot #() in
    Fork (client_thread "c" #5) ;;
    Fork (client_thread "c" #3) ;;
    check "c" #().


Section client_verification.
  (** We start the verification of clients. Once again, we parametrize over appropriate [Σ]s. *)
  Context `{!heapGS Σ, !oneshotG Σ}.

  (** The verifications just show the client programs are safe. *)
  Global Instance client_thread_spec γ (v : val) (z : Z) :
    SPEC {{ is_oneshot γ v }} client_thread v #z {{ RET #(); True }}.
  Proof. Admitted.

  Global Instance oneshot_client_spec :
    SPEC {{ True }} oneshot_client #() {{ RET #(); True }}.
  Proof. Admitted.
End client_verification.


(** To conclude this example, we generate a closed proof of safety of [oneshot_client]. 
  For a more thorough explanations, see the 
  %Iris documentation: \url{https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/resource_algebras.md#obtaining-a-closed-proof}.%
  #<a href="https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/resource_algebras.md##obtaining-a-closed-proof">Iris documentation.</a># 
*)
From iris.heap_lang Require Import adequacy.

(** We need a specific [Σ] for a closed proof - the 'ghost-state register' which needs to satisfy [heapGS Σ] and [oneshotG Σ]. 
  This can be constructed as follows. *)
Definition clientΣ : gFunctors := #[ heapΣ; oneshotΣ ].

(** The statement '[oneshot_client #()] is safe' in Coq becomes the following: *)
Lemma client_adequate σ : adequate NotStuck (oneshot_client #()) σ (λ _ _, True).
Proof. apply (heap_adequacy clientΣ)=> ?. iSteps. Qed.

(** We can ask Coq to check if any Axioms were used in the proof of [client_adequate].
  If you have completed the proofs correctly, this should print: 'Closed under the global context' *)
Print Assumptions client_adequate.
