(** Welcome to the Diaframe tutorial, part 2.

  In this example, we will be verifying an implementation of the Treiber stack.
  This tutorial assumes that you have finished part 1 of the tutorial. You may want to
  return to part 1 if you do not grasp pieces of this tutorial.
*)

From iris.heap_lang Require Import proofmode.
From diaframe.heap_lang Require Import proof_automation.


Definition new_stack : val := 
  λ: <>, ref NONEV.

Definition push_this : val :=
  rec: "push" "s" "l" :=
    let: "tail" := ! "s" in
    "l" +ₗ #1 <- "tail" ;; 
    if: CAS "s" "tail" (SOME "l") then 
      #() 
    else 
      "push" "s" "l".

Definition push : val :=
  λ: "s" "v",
    let: "l" := AllocN #2 "v" in
    push_this "s" "l".

Definition pop : val :=
  rec: "pop" "s" :=
    match: !"s" with
      NONE => NONEV
    | SOME "l" =>
      let: "next" := !("l" +ₗ #1) in
      if: CAS "s" (SOME "l") "next" then 
        SOME (! "l")
      else 
        "pop" "s"
    end.

(** This example features two new heap operations:
  - The store operation [l <- v] writes value [v] to location [l]
  - The [AllocN n v] allocates an array of length [n], initialized with value [v]
  The [+ₗ] operation is used to add an offset to a pointer, i.e. to index in an array.
  Additionally, the [push_this] and [pop] methods feature the [rec:] keyword.
  This indicates the definition of a _recursive_ function. The first argument to [rec:] 
  is a binder, containing the recursive reference to the current function.

  We will not be needing any ghost-state for this example. What we will need, 
  is a recursive definition of what it means to be a stack.
*)

Section verification.
  Context `{!heapGS Σ}.

  (** The algorithm described above operates on memory that has a specific layout:
   locations [l] for which [l ↦ v], and [l +ₗ 1 ↦ ml], where either [ml = NONEV] or [ml = SOMEV l'], 
   with [l'] the again the same layout. We want to indicate that [SOMEV l] describes a 
   list of values, with [v] being the head of the list, and [ml] describing the tail of the list.
   In the spirit of iCAP, we may want these values to satisfy some predicate P. For example,
   if the stored values are locations, the predicate P may require them to map to a certain value.
   In Iris, such a definition can be given as follows:
  *)

  Fixpoint is_list (P : val → iProp Σ) (xs : list val) (lv : val) : iProp Σ :=
    match xs with
    | [] => ⌜lv = NONEV⌝
    | x :: xs => ∃ (l : loc), ⌜lv = SOMEV #l⌝ ∗ l ↦□ x ∗ P x ∗ 
          ∃ lv', (l +ₗ 1) ↦□ lv' ∗ is_list P xs lv'
    end.
  (** We make use of the persistent mapsto [l ↦□ _] here. This is necessary, since 
    a very slow [pop] may try to load a location that has been [pop]'ed concurrently
    a while ago. The relevant rule for this persistent mapsto is [l ↦{#q} v ⊢ |==> l ↦□ v].

    The stack just contains a pointer to a value which [is_list], so we have:
  *)

  Definition stack_inv P l :=
    (∃ vl, l ↦ vl ∗ ∃ xs, is_list P xs vl)%I.

  (** And as before, we wrap this in an [is_stack] predicate to hide away the implementation details *)

  Let N := nroot .@ "stack".

  Definition is_stack (P : val → iProp Σ) v : iProp Σ :=
    ∃ (l : loc), ⌜v = #l⌝ ∗ inv N (stack_inv P l)%I.

  (** Our ideal specification for [new_stack] is as follows. *)
  Global Instance new_stack_spec :
    SPEC {{ True }} 
      new_stack #() 
    {{ (s : val), RET s; ∀ P, |={⊤}=> is_stack P s }}.
  Proof.
    iSteps. 
  (** We get stuck here! Diaframe does not have native support for recursive definitions like [is_list]. 
    We need an appropriate hint. Remember that we got stuck on a goal of the form:
    [∃ xs, is_list P xs NONEV]  -- [InjLV #()] is notation for [NONEV]
  *)
  Abort.

  (** Exercise: change the hint to have proper side-conditions and frame, then prove the hint. *)
  Global Instance biabd_islist_none xs P :
    HINT ε₀ ✱ [- ; emp (* replace me *)] ⊫ [id]; is_list P xs NONEV ✱ [False (* replace me *)].
  Proof.
    (** This hint has key hypothesis [ε₀]. This is another special proposition that is treated 
      differently by the proof search strategy, like [ε₁]. The semantics of
      [ε₀] is just [True], but this hypothesis is always the first hypothesis in every environment Δ.
      It can be used for hints to add simplification rules.
     *)
  Admitted.
  (** For this hint, we use [id] as the modality. Diaframe automatically uses the fact that
    [id P ⊢ |={E,E}=> P] for all [E] and [P] when applying such hints. *)

  Obligation Tactic := verify_tac.

  Global Program Instance new_stack_spec :
    SPEC {{ True }} 
      new_stack #() 
    {{ (s : val), RET s; ∀ P, |={⊤}=> is_stack P s }}.

  (** Now we can get on with the verification of [push_this]. The specification we want is as follows. *)
  Global Instance push_this_spec P (s : val) (l : loc) (v w : val) :
    SPEC {{ is_stack P s ∗ l ↦ v ∗ (l +ₗ 1) ↦ w ∗ P v }}
      push_this s #l
    {{ RET #(); True }}.
  Proof.
    iSteps as (ls) "HI Hl1 Hl2 HP".
    (** Like before, we just try [iSteps]. This time, it stops at just [WP push_this ..], and
      does not unfold [push_this]. This is because Diaframe's heap_lang support is set up to 
      _not_ unfold recursive functions. Verifications of such functions usually require 
      a form of induction.

      In this case, we use Löb induction, as follows. *)
    iLöb as "IH" forall (w).
    (** We now skip ahead to the part of the verification that currently poses problems.
        We start with [wp_lam] to force unfolding [push_this] - as noted above, Diaframe won't do that *)
    wp_lam. iStep 24 as (l' xs) "Hl2".
    (** Stepping again shows us the specification of [CAS]/[CmpXchg] *)
    iStep. (* But Diaframe cannot currently prove this: *) Fail solve [iSteps].
    (** There is a problem with the [vals_compare_safe] proposition.
      As heap_lang is a lambda-calculus, lambdas are also values. The current semantics
      of heap_lang allow storing any value in a location, including lambdas. Such locations
      can also be CAS'ed, which would require a comparison of lambdas. This has been deemed
      unrealistic, so the specification of CAS requires the two compared values to satisfy
      [vals_compare_safe]. This boils down to requiring that either value is [val_is_unboxed],
      meaning a 'regular' literal value.

      In our situation, we do not have this information available, so the [vals_is_unboxed]
      becomes an unsolvable proof obligation.
     *)
    iStep. (* Diaframe will shelve pure goals that can be postponed. To show the shelved goal, do: *)
    Unshelve. 3:{   
    (** This is problematic. What to do? Well, [vals_compare_safe v] actually follows from
      [is_list _ _ v]: either [v = NONEV] or there is a location [l] such that [v = SOMEV #l],
      and in both cases we get [vals_is_unboxed v].

      We need a way to make sure Diaframe picks up this information. *)
  Abort.

  (** This is what the [MergablePersist] typeclass is responsible for. It is written in CPS for
      efficiency reasons. [MergablePersist H1 C] means that for all [p] [H2] [Hout], 
      [C p H2 Hout] implies [H1 ∗ □?p H2 ⊢ □ Hout]. Diaframe uses this information to
      additionally learn [Hout] when introducing [H1] into a context that already contains [H2].
      We can again take [H2] to be [ε₀], to learn [Hout] everytime we introduce [H1]
      into a context.
      The following instance makes Diaframe obtain ⌜val_is_unboxed v⌝ whenever it introduces
      (is_list P xs v) into the context.
  *)
  Instance is_list_remember_1 P xs v :
    MergablePersist (is_list P xs v) (λ p Pin Pout,
        TCAnd (TCEq Pin (ε₀)%I)
              (TCEq Pout (⌜val_is_unboxed v⌝)))%I | 30.
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->] /=.
    destruct xs; iSteps.
  Qed.

  (** We now retry the verification of [push_this] *)
  Lemma push_this_spec_1 P (s : val) (l : loc) (v w : val) :
    SPEC {{ is_stack P s ∗ l ↦ v ∗ (l +ₗ 1) ↦ w ∗ P v }}
      push_this s #l
    {{ RET #(); True }}.
  Proof.
    iSteps. iLöb as "IH" forall (w).
    wp_lam. iSteps.
  (** We get stuck once again, on a goal of shape
    [∃ xs, is_list P xs (SOMEV #l)]. All spatial hypotheses are relevant.

    One way to proceed is as follows:
  *)
    iExists (v :: x5). iSteps.
  Qed.

  (** Exercise: Create and prove a hint so that above proof can be done automatically.
    The skeleton of the hint has been provided below.
    Some comments and tips:
    - [ε₁] has been taken as key hypothesis, to only apply this hint if we cannot find anything else to do
    - The side conditions are basically taken from the definition of [is_list].
    - The variables [x xs' v;] are quantifiers, as is [xs]!
  *)
  Global Instance biabd_islist_some P (l : loc) :
    HINT ε₁ ✱ [x xs' v; l ↦□ x ∗ P x ∗ (l +ₗ 1) ↦□ v ∗ is_list P xs' v] ⊫ 
          [id] xs; is_list P xs (SOMEV #l) ✱ [⌜xs' = []⌝ (* replace me: what do we learn about [xs]? *)].
  Proof.
    iSteps. iExists (x :: x0). iSteps.
  Admitted.

  (** If you have completed above exercise, the proof of [push_this_spec_2] below should be valid: *)
  Lemma push_this_spec_2 P (s : val) (l : loc) (v w : val) :
    SPEC {{ is_stack P s ∗ l ↦ v ∗ (l +ₗ 1) ↦ w ∗ P v }}
      push_this s #l
    {{ RET #(); True }}.
  Proof. iSteps. iLöb as "IH" forall (w). wp_lam. iSteps. Qed.
  (** Note that apart from Löb induction, the proof is fully automatic. 
      We can additionally add automation for Löb induction with the following import: *)
  From diaframe.heap_lang Require Import wp_auto_lob.


  Lemma push_this_spec_3 P (s : val) (l : loc) (v w : val) :
    SPEC {{ is_stack P s ∗ l ↦ v ∗ (l +ₗ 1) ↦ w ∗ P v }}
      push_this s #l
    {{ RET #(); True }}.
  Proof.
    iSteps as (_ _ s' _ v' P' Hlöb _ _ _ _ _ _ _) "HN IH HP'".
    (** The Löb induction is now performed automatically, and does some additional generalization.
      See also the induction hypothesis "H1". We get stuck on the current goal because the goal was automatically
      generalized over [P]. We now need to find and prove some [x], for which [x x5]. However, since
      [x] is in the head position, there are, in a sense, too many choices. One could choose for example
      the trivial [x = λ _, emp]. To avoid making such bad choices, Diaframe stops. 
      We could finish the proof like so: *)
    iExists P'. (** explicitly choosing the correct witness [P'], chosen by inspecting ["HP"] *)
    iSteps. (** and the rest is easy. *) 
  Admitted.

  (** Alternatively, one can instruct the automatic Löb induction not to generalize on some variables,
    by making them Section variables. *)
  Context (P : val → iProp Σ).
  (** Now, [push_this_spec] can be proven fully automatically: *)

  Program Instance push_this_spec_4 (s : val) (l : loc) (v w : val) :
    SPEC {{ is_stack P s ∗ l ↦ v ∗ (l +ₗ 1) ↦ w ∗ P v }}
      push_this s #l
    {{ RET #(); True }}.

  Global Instance push_spec (s v : val) :
    SPEC {{ is_stack P s ∗ P v }} 
      push s v 
    {{ RET #(); True }}.
  Proof. Admitted.
  (** If you get '1 obligation remaining: ... defs.ReductionTemplateStep', when using [Program] to run the
    verification go back and add appropriate hints! *)

  (** Now let us consider [pop]. We want to prove the following specification for [pop]: *)
  Lemma pop_spec_1 (s : val) :
    SPEC {{ is_stack P s }} 
      pop s 
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ w, ⌜ov = SOMEV w⌝ ∗ P w }}.
  Proof.
    (** Start automation again, remember that we now do Löb induction automatically! *)
    iStep 10 as (_ l Hlöb) "HN IH".
    (** We almost immediately run into a problem. We do a load from the stack, 
      and only learn that this value [is_list]. After the load, [pop] pattern-matches
      to determine how to continue. But the information that the loaded value matches 
      one of the branches is lost. *)
    iStep.
    iStep 2 as (v vs Hv) "Hcl Hlist Hl".
    (** If you do [iStep] in this proofstate, it becomes unprovable!

      One option is to manually perform a destruct here, then reintroduce some hypotheses. 
    *)
    destruct vs; iDecompose "Hlist".
    - iSteps. exact [].
    - (** We need to resort to a manual proof here. *)
      iStep; iSteps as (vs' Hx) "Hx Hlist".
      (** Once again, Diaframe gets stuck on an [is_list] goal, this time:
        [∃ x2, ▷ is_list x2 x1]. The clue here is the goal [(x +ₗ 1) ↦□ x0] together with hypothesis
        [is_list P vs' (SOMEV #x)]. *)
      destruct vs'; iDecompose "Hlist".
      iStep. iSteps.
  Admitted.
  (** So, the verification of [pop] poses two problems:
    - We are missing a hint for the pop-manipulation of [is_list]
    - At some point, Diaframe does not perform a case distinction that, as becomes apparent later in 
      the proof, is required.
    The first problem is fixable by just adding an appropriate hint. The second problem is a bit more tricky.

    For the second problem, one solution is to do a case distinction every time an [is_list _ _ _]
    gets introduced into the environment. This works (although care must be taken not to keep doing this,
    since the case distinction produces another, nested [is_list _ _ _]), but is inefficient: the 
    case distinction is done even in cases where it is not necessary (i.e. throughout the verification of
    [push]).
    A cleaner approach can be found, when one realises that in the definition of [is_list],
    the only non-persistent resource are the [P _]. Additionally, the persistent part contains
    enough information to finish the proof!

    The idea is now to give a separate definition of the persistent part of [is_list], and
    make sure Diaframe remembers this information. Then, we make Diaframe perform the 
    required case distinction precisely when necessary. *)

  Fixpoint is_list_skel (xs : list val) (lv : val) : iProp Σ :=
    match xs with
    | [] => ⌜lv = NONEV⌝
    | x :: xs => ∃ (l : loc), ⌜lv = SOMEV #l⌝ ∗ l ↦□ x ∗ 
          ∃ lv', (l +ₗ1) ↦□ lv' ∗ is_list_skel xs lv'
    end.

  (** Now we prove that [is_list_skel] follows from [is_list] ... *)
  Lemma is_list_gives_skel P xs v : is_list P xs v ⊢ is_list_skel xs v.
  Proof. revert v; induction xs; iSteps. by iApply IHxs. Qed.

  (** And indeed that [is_list_skel] is persistent. *)
  Global Instance is_list_skel_pers xs v : Persistent (is_list_skel xs v). 
  Proof. revert v; induction xs; tc_solve. Qed.

  (** Exercise: change and prove [is_list_remember_2] so that Diaframe will
    remember [is_list_skel] on introduction of [is_list]. Tip: use [is_list_gives_skel] *)
  Instance is_list_remember_2 P xs v :
    MergablePersist (is_list P xs v) (λ p Pin Pout,
        TCAnd (TCEq Pin (ε₀)%I)
              (TCEq Pout ((* replace me*)is_list_skel xs v ∗ ⌜val_is_unboxed v⌝)))%I | 20.
  Proof.
    rewrite /MergablePersist => p Pin Pout [-> ->] /=. 
    rewrite is_list_gives_skel. destruct xs; iSteps.
  Admitted.

  (** Exercise: 
    Construct and prove the [is_list] hint required for the verification of [pop].
    That is, write a Coq [Instance] with type an appropriate HINT type. See also
    the related [biabd_islist_some] hint.
  *)

  Lemma pop_spec_2 P (s : val) :
    SPEC {{ is_stack P s }} 
      pop s 
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ w, ⌜ov = SOMEV w⌝ ∗ P w }}.
  Proof.
    iSteps as (_ l' Hlöb v vs Hv) "HN IH H□vs".
    (** Alright, so we have access to [is_list_skel], but get stuck on executing the
      pattern match. We could do a manual proof: *)
    destruct vs. (* then iDecompose "H□vs"; iSteps. *)
    Undo 1.
    (** but this is unsatisfactory. Can we do better? 

      It turns out we can. Diaframe comes with _keyed_ specifications of programs/expressions. 
      Such specifications are not always applied, but only when the keyed hypothesis 
      can be found. In this case, we can provide an additional specification of 
      [match: x2] where [is_list_skel _ x2] is the keyed hypothesis.
      Regular SPEC triples implicitly feature [ε₀] as keyed hypothesis.
    *)
  Abort.

  Global Instance match_list xs v e1 e2 :
      (** The keyed hypothesis in a SPEC triple is provided between square brackets: *)
    SPEC [is_list_skel xs v] {{ True }} 
      Case v e1 e2 (** [match:] desugars into the more primitive [Case] instruction *)
      (** Note that term after RET is now an expression, of type [expr], not [val]!
          So this is not a regular specification specifying return values, but something 
          that specifies how this expression reduces. It says that the [Case] expression
          reduces to itself, and that additionally one learns some information on the
          first argument (i.e. [v = NONEV] or [v = SOMEV #l] for some [l].
       *)
    {{ [▷^0] RET Case v e1 e2; 
        ⌜v = NONEV⌝ ∗ ⌜xs = []⌝ ∨ ∃ (l : loc) h xs' t, ⌜v = SOMEV #l⌝ ∗ l ↦□ h ∗ (l +ₗ 1) ↦□ t ∗ ⌜xs = h :: xs'⌝ ∗ is_list_skel xs' t 
    }} | 50.
  Proof.
    iStep 2 as (Φ) "H□vs". destruct xs; iDecompose "H□vs"; iStep as "HWP".
    - iApply "HWP". iSteps.
    - iApply "HWP". iSteps.
  Qed.

  (** At this point, Diaframe can verify [pop] automatically! 
    That is, only if you have completed the exercise for the missing [is_list] hint for the verification of [pop] *)
  Global Instance pop_spec_3 P (s : val) :
    SPEC {{ is_stack P s }} 
      pop s 
    {{ (ov : val), RET ov; ⌜ov = NONEV⌝ ∨ ∃ w, ⌜ov = SOMEV w⌝ ∗ P w }}.
  Proof.
  Admitted.

End verification.

