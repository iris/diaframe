# Makefile to create the CoqMakefile from _CoqProject

-include CoqMakefile.conf
ifeq ($(origin COQMF_COQLIB), undefined)
COQ_MAKEFILE = $(shell which coq_makefile || echo '"'"$$COQBIN"coq_makefile'"')
else
COQ_MAKEFILE = $(COQBIN)coq_makefile
endif


BASE_SRC_DIRS := diaframe diaframe_heap_lang supplements/diaframe_heap_lang_examples
ALL_SRC_DIRS := $(BASE_SRC_DIRS) supplements/diaframe_reloc supplements/diaframe_simuliris supplements/diaframe_actris supplements/diaframe_iris_examples supplements/diaframe_lambda_rust
SRC_FIND_OPTS := -not \( -path diaframe/legacy -prune \)
COQMF_ARGS = INSTALLDEFAULTROOT = diaframe
CAMLPKGS = -package coq-core.plugins.ltac2


# We don't want to manually specify all buildable vfiles, so we use coq_makefile to bootstrap a list of buildable files
# hilarious, sort order depends on locale config, which differs between Make-recipe and make shell :)))
._CoqProject_make: _CoqProject Makefile
	@find $(ALL_SRC_DIRS) $(SRC_FIND_OPTS) -name "*.v" | LC_ALL=en_US.UTF-8 sort > .allvfiles.d # records the list of vfiles at generation of ._CoqProject_make
	@"$(COQ_MAKEFILE)" $(COQMF_ARGS) -f $< -o .CoqMakefile.tmp && \
	coqlib=$$(grep COQMF_COQLIB= .CoqMakefile.tmp.conf | sed -e 's/[^=]*=//' ); \
	srcdirs="$(BASE_SRC_DIRS)"; \
	cp $< $@; \
	if [ ! -d "$$coqlib""user-contrib/simuliris" ]; then \
	  sed -i.old '/diaframe_simuliris/ s/^#*/#/' $@ ; \
	else\
	  srcdirs="$$srcdirs supplements/diaframe_simuliris"; \
	fi; \
	if [ ! -d "$$coqlib""user-contrib/reloc" ]; then \
	  sed -i.old '/diaframe_reloc/ s/^#*/#/' $@; \
	else\
	  srcdirs="$$srcdirs supplements/diaframe_reloc"; \
	fi; \
	if [ ! -d "$$coqlib""user-contrib/iris_examples" ]; then \
	  sed -i.old '/diaframe_iris_examples/ s/^#*/#/' $@; \
	else\
	  srcdirs="$$srcdirs supplements/diaframe_iris_examples"; \
	fi; \
	if [ ! -d "$$coqlib""user-contrib/lrust" ]; then \
	  sed -i.old '/diaframe_lambda_rust/ s/^#*/#/' $@; \
	else\
	  srcdirs="$$srcdirs supplements/diaframe_lambda_rust"; \
	fi; \
	if [ ! -d "$$coqlib""user-contrib/actris" ]; then \
	  sed -i.old '/diaframe_actris/ s/^#*/#/' $@; \
	else\
	  srcdirs="$$srcdirs supplements/diaframe_actris"; \
	fi; \
	echo >> $@; \
	echo >> $@; \
	find $$srcdirs $(SRC_FIND_OPTS) -name "*.v" | sort >> $@

CoqMakefile: ._CoqProject_make Makefile
	@"$(COQ_MAKEFILE)" $(COQMF_ARGS) -f $< -o $@

# stuff which forces recalculation of dependencies if new .v files are created
ifneq ("$(wildcard CoqMakefile.conf)", "")
# test if CoqMakefile.conf and .allvfiles.d both exist
ifneq ("$(wildcard .allvfiles.d)", "")
SEARCHVFILES?=1
ifeq ("$(SEARCHVFILES)", "1")
# allow this check to be skipped via command line argument

CURRENT_VFILES := $(shell find $(ALL_SRC_DIRS) $(SRC_FIND_OPTS) -name "*.v" | LC_ALL=en_US.UTF-8 sort > .curvfiles.d)
VFILES_EQ := $(shell diff -q .allvfiles.d .curvfiles.d; echo $$?)
ifneq ("$(VFILES_EQ)", "0")
$(info Detected change in .v files..)
# new vfiles detected: make CoqProject_make phony for this run to force remaking dependencies
PHONY_REQ=._CoqProject_make
endif

endif
endif
endif

.PHONY: ${PHONY_REQ}


BUILDDEPFILES=$(shell find . ./supplements -maxdepth 1 -type f -name "*.opam" | grep -v 'builddep' | sed -e 's_/\([^/]*\)\.opam_/builddep/\1-builddep.opam_')

builddep-opamfiles: $(BUILDDEPFILES)
	@true
.PHONY: builddep-opamfiles

builddep/%-builddep.opam: %.opam Makefile
	@echo "# Creating $*-builddep package."
	@mkdir -p builddep
# this sed performs 3 actions: remove build actions, make builddep package name obvious, fix requirements to other packages from this repository
	@sed <$< -E 's/^(build|install|remove):.*/\1: []/; s/^name: *"(.*)" */name: "\1-builddep"/; s/"(.*)"(.*= *version.*)$$/"\1-builddep"\2/;' >$@
	@fgrep builddep $@ >/dev/null || (echo "sed failed to fix the package name" && exit 1) # sanity check

supplements/builddep/%-builddep.opam: supplements/%.opam Makefile
	@echo "# Creating $*-builddep package."
	@mkdir -p supplements/builddep
# this sed performs 3 actions: remove build actions, make builddep package name obvious, fix requirements to other packages from this repository
	@sed <$< -E 's/^(build|install|remove):.*/\1: []/; s/^name: *"(.*)" */name: "\1-builddep"/; s/"(.*)"(.*= *version.*)$$/"\1-builddep"\2/;' >$@
	@fgrep builddep $@ >/dev/null || (echo "sed failed to fix the package name" && exit 1) # sanity check

builddep/%-cijobdep.opam: supplements/builddep/%-builddep.opam
	@mkdir -p builddep
	@ln -sf ../$< $@


EXAMPLES_DIR := supplements/diaframe_heap_lang_examples
BASE := diaframe_heap_lang/proof_automation.vo diaframe/lib/own_hints.vo
TESTEXAMPLES := $(EXAMPLES_DIR)/comparison/spin_lock.vo $(EXAMPLES_DIR)/comparison/clh_lock.vo $(EXAMPLES_DIR)/comparison/arc.vo
ACTRIS := $(patsubst %.v,%.vo,$(wildcard supplements/diaframe_actris/*.v))
IRIS_EXAMPLES := $(patsubst %.v,%.vo,$(wildcard supplements/diaframe_iris_examples/*.v))
RELOC := $(patsubst %.v,%.vo,$(wildcard supplements/diaframe/reloc/*.v) $(wildcard supplements/diaframe_reloc/examples/*.v))
SIMULIRIS := $(patsubst %.v,%.vo,$(wildcard supplements/diaframe_simuliris/*.v) $(wildcard supplements/diaframe_simuliris/simple/*.v) $(wildcard supplements/diaframe_simuliris/na/*.v))
LRUST := $(patsubst %.v,%.vo,$(wildcard supplements/diaframe_lambda_rust/*.v))
ATOMIC_VERY_SLOW := $(EXAMPLES_DIR)/logatom/queue.vo $(EXAMPLES_DIR)/logatom/msc_queue_simplified.vo $(EXAMPLES_DIR)/logatom/msc_queue_faithful.vo
ATOMIC_SLOW := $(EXAMPLES_DIR)/logatom/atomic_clhlock.vo $(EXAMPLES_DIR)/logatom/atomic_lock.vo $(EXAMPLES_DIR)/logatom/atomic_spinlock.vo $(EXAMPLES_DIR)/logatom/atomic_ticketlock_client.vo $(EXAMPLES_DIR)/logatom/atomic_ticketlock.vo
ATOMIC := $(filter-out $(ATOMIC_VERY_SLOW),$(filter-out $(ATOMIC_SLOW),$(patsubst %.v,%.vo,$(wildcard $(EXAMPLES_DIR)/logatom/atomic_*.v))))
COMPARISON_VERY_SLOW := $(EXAMPLES_DIR)/comparison/barrier.vo $(EXAMPLES_DIR)/comparison/barrier_client.vo $(EXAMPLES_DIR)/comparison/peterson.vo $(EXAMPLES_DIR)/comparison/peterson_slow.vo
COMPARISON_SLOW := $(EXAMPLES_DIR)/comparison/lclist_extra.vo $(EXAMPLES_DIR)/comparison/mcs_lock.vo $(EXAMPLES_DIR)/comparison/msc_queue.vo $(EXAMPLES_DIR)/comparison/queue.vo
COMPARISON := $(filter-out $(COMPARISON_SLOW),$(filter-out $(COMPARISON_VERY_SLOW),$(patsubst %.v,%.vo, $(wildcard $(EXAMPLES_DIR)/comparison/*.v))))
RWLOCKS := $(patsubst %.v,%.vo,$(wildcard $(EXAMPLES_DIR)/comparison/rwlock*.v))
ALL_SLOW_AND_VERY_SLOW := $(ATOMIC_VERY_SLOW) $(ATOMIC_SLOW) $(COMPARISON_VERY_SLOW) $(COMPARISON_SLOW)

ifeq (,$(shell which $(COQ_MAKEFILE)))
# coq is probably not accessible yet! so don't build the CoqMakefile
else
include CoqMakefile
endif


# CoqMakefile HELPFULLY overrides the default target
.DEFAULT_GOAL := default

default: $(BASE)

testexamples: $(BASE) $(TESTEXAMPLES)

actris: $(BASE) $(ACTRIS)

reloc: $(BASE) $(RELOC)

simuliris: $(BASE) $(SIMULIRIS)

lambda-rust: $(BASE) $(LRUST)

lrust: lambda-rust

comparison-quick: $(COMPARISON)

comparison: $(COMPARISON) $(COMPARISON_SLOW) $(ATOMIC) $(ATOMIC_SLOW)

comparison-all: $(COMPARISON) $(COMPARISON_SLOW) $(COMPARISON_VERY_SLOW) $(ATOMIC) $(ATOMIC_SLOW) $(ATOMIC_VERY_SLOW)

logatom: $(BASE) $(ATOMIC) $(ATOMIC_SLOW) $(ATOMIC_VERY_SLOW)

logatom-quick: $(ATOMIC)

irisexamples: $(BASE) $(IRIS_EXAMPLES)

rwlocks: $(BASE) $(RWLOCKS)

all: no-user-bail

no-user-bail:
	@echo
	@echo "This is really all, which is quite alot!"
	@echo "Giving you 5 seconds to bail..."
	@sleep 5
.PHONY: no-user-bail

clean::
	@rm -f CoqMakefile
	@rm -f CoqMakefile.conf
	@rm -f CoqMakefile.old
	@rm -f CoqMakefile.conf.old
	@rm -f ._CoqProject_make
	@rm -f ._CoqProject_make.old
	@rm -f .CoqMakefile.tmp
	@rm -f .CoqMakefile.tmp.conf
	@rm -f .allvfiles.d
	@rm -f .curvfiles.d
	@rm -f .CoqMakefile.d
	@rm -rf builddep
	@rm -rf supplements/builddep
	@rm -f .lia.cache


wrong-install:
	@echo "make install does not work, use make install-<PACKAGENAME>, ie make install-diaframe"
	@exit 1

install: wrong-install

PACKAGE_PREFIX_diaframe=diaframe/
PACKAGE_PREFIX_diaframe-heap-lang=diaframe_heap_lang/
PACKAGE_PREFIX_diaframe-heap-lang-examples=supplements/diaframe_heap_lang_examples/
PACKAGE_PREFIX_diaframe-reloc=supplements/diaframe_reloc/
PACKAGE_PREFIX_diaframe-simuliris=supplements/diaframe_simuliris/
PACKAGE_PREFIX_diaframe-actris=supplements/diaframe_actris/
PACKAGE_PREFIX_diaframe-iris-examples=supplements/diaframe_iris_examples/
PACKAGE_PREFIX_diaframe-lambda-rust=supplements/diaframe_lambda_rust/

PACKAGES=diaframe diaframe-heap-lang diaframe-heap-lang-examples diaframe-reloc diaframe-simuliris diaframe-actris diaframe-iris-examples diaframe-lambda-rust

diaframe-heap-lang-examples-light-ci: $(filter-out $(ALL_SLOW_AND_VERY_SLOW),$(shell grep $(PACKAGE_PREFIX_diaframe-heap-lang-examples) installfiles.d | sed -e 's/\.v/.vo/'))

.SECONDEXPANSION:

$(PACKAGES): %: $$(shell grep $$(PACKAGE_PREFIX_%) installfiles.d | sed -e 's/\.v/.vo/')

INSTALL_PACKAGES=$(addprefix install-,$(PACKAGES))

$(INSTALL_PACKAGES):: install-%: $$(shell grep $$(PACKAGE_PREFIX_%) installfiles.d | sed -e 's/\.v/.vo/')
	@echo "INSTALL $*"
	$(HIDE)code=0; for ext in ".v" ".vo" ".glob"; do\
	 for f in $$(grep $(PACKAGE_PREFIX_$*) installfiles.d | sed -e "s/\.v/$$ext/"); do\
	  if ! [ -f "$$f" ]; then >&2 echo "$$f does not exist" ; code=1; fi \
	 done; \
	done; exit $$code
	$(HIDE)code=0; for ext in ".v" ".vo" ".glob"; do\
	 for f in $$(grep $(PACKAGE_PREFIX_$*) installfiles.d | sed -e "s/\.v/$$ext/"); do\
	  df="`$(COQMKFILE) -destination-of "$$f" $(COQLIBS)`";\
	  if [ "$$?" != "0" -o -z "$$df" ]; then\
	   echo SKIP "$$f" since it has no logical path;\
	  else\
	   install -d "$(COQLIBINSTALL)/$$df" &&\
	   install -m 0644 "$$f" "$(COQLIBINSTALL)/$$df" &&\
	   echo INSTALL "$$f" "$(COQLIBINSTALL)/$$df";\
	  fi;\
	 done; \
	done

# we wanted to be fancy and add ocaml stuff. look where that got us
install-diaframe:: META
	@echo "INSTALL diaframe-ocaml"
	$(HIDE)code=0; for f in $(CMXSFILES); do\
	  if ! [ -f "$$f" ]; then >&2 echo "$$f does not exist" ; code=1; fi \
	done; exit $$code
	$(HIDE)code=0; for f in $(CMXSFILES); do\
	  df="`$(COQMKFILE) -destination-of "$$f" $(COQLIBS)`";\
	  if [ "$$?" != "0" -o -z "$$df" ]; then\
	   echo SKIP "$$f" since it has no logical path;\
	  else\
	   install -d "$(COQLIBINSTALL)/$$df" &&\
	   install -m 0644 "$$f" "$(COQLIBINSTALL)/$$df" &&\
	   echo INSTALL "$$f" "$(COQLIBINSTALL)/$$df";\
	  fi;\
	done
	$(call findlib_remove)
	$(call findlib_install, META $(FINDLIBFILESTOINSTALL))
